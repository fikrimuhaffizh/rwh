<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['rwhgate']='administrator';
$route['administrator']='error';
$route['sitemap.xml']='sitemap';
$route['default_controller'] = 'guest';
$route['login'] = 'auth';

$route['profil'] = 'guest/halaman/profil';
$route['muthawif'] = 'guest/halaman/muthawif';
$route['tafahum'] = 'guest/halaman/tafahum';

$route['haji'] = 'guest/paket/haji';
$route['umroh'] = 'guest/paket/umroh';
$route['umroh/(:num)'] = 'guest/paket/umroh/$1';
$route['wisata'] = 'guest/paket/wisata';
$route['status'] = 'guest/status';
$route['manasik'] = 'guest/manasik';
$route['merchant'] = 'guest/merchant';
$route['member'] = 'member';
$route['member/detail/(:num)'] = 'member/detail/$1';
$route['verif_pendaftaran/(:num)'] = 'member/verifikasi_pendaftaran/$1';

$route['konfirmasi'] = 'guest/konfirmasi';
$route['konfirmasi/add'] = 'guest/konfirmasi/add';

// $route['daftar-tafahum'] = 'guest/daftar_tafahum';
// $route['daftar-tafahum/add'] = 'guest/daftar_tafahum/add';

$route['detail/cekKodePromo/(:any)'] = 'guest/detail/cekKodePromo/$1';
$route['keberangkatan/(:any)'] = 'guest/keberangkatan/$1';

$route['kantor'] = 'guest/kantor';
$route['kantor/getDetailKantor/(:any)'] = 'guest/kantor/getDetailKantor/$1';

$route['kontak'] = 'guest/kontak';
$route['kontak/send_email'] = 'guest/kontak/send_email';
$route['kontak/getWhatsappReady'] = 'guest/kontak/getWhatsappReady';

$route['booking'] = 'guest/detail/booking';
$route['detail/form_booking/(:num)'] = 'guest/detail/form_booking/$1';
$route['detail/form_booking/(:num)/(:num)'] = 'guest/detail/form_booking/$1/$2';
$route['detail/form_booking/(:num)/(:num)/(:num)'] = 'guest/detail/form_booking/$1/$2/$3';

$route['daftar'] = 'guest/daftar';
$route['daftar/add'] = 'guest/daftar/add';

$route['paket'] = 'guest/paket';
$route['paket/detail/(:num)/(:any)'] = 'guest/detail/$1';
$route['paket/detail/haji/(:num)/(:any)'] = 'guest/detail/haji/$1/$2';
$route['umroh/cari'] = 'guest/paket/umroh';
$route['wisata/cari'] = 'guest/paket/wisata';

$route['artikel'] = 'guest/artikel';
$route['artikel/(:num)'] = 'guest/artikel';
$route['artikel/detail/(:num)/(:any)'] = 'guest/artikel/detail/$1';

$route['galeri'] = 'guest/galeri';
$route['galeri/(:num)'] = 'guest/galeri';
$route['galeri/detail/(:num)'] = 'guest/galeri/detail/$1';
$route['galeri/detail/(:num)/(:num)'] = 'guest/galeri/detail/$1';

$route['oauthcallback'] = 'Auth/oauth';

$route['404_override'] = 'guest/error';
$route['translate_uri_dashes'] = FALSE;

$route['sitemap.xml']='sitemap';
$route['halaman-sitemap.xml']='sitemap/halaman';
$route['artikel-sitemap.xml']='sitemap/artikel';

$route['(:any)'] = 'guest/halaman/$1';
