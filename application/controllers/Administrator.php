<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Administrator extends CI_Controller {

    function __construct() {
        parent::__construct();
        // $this->output->enable_profiler(TRUE);
        
        $this->load->model('md_notifikasi');
        $this->load->model('md_artikel');
        $this->load->model('md_galeri_album');
        $this->load->model('md_galeri_item');
        $this->load->model('md_halaman');
        $this->load->model('md_jadwal_manasik');
        $this->load->model('md_jenis_paket');
        $this->load->model('md_keberangkatan');
        $this->load->model('md_paket_waktu');
        $this->load->model('md_paketwaktu_kelas');
        $this->load->model('md_kelas');
        $this->load->model('md_barang');
        $this->load->model('md_kontak');
        $this->load->model('md_bank');
        $this->load->model('md_maskapai');
        $this->load->model('md_transaksi');
        $this->load->model('md_hotel');
        $this->load->model('md_merchant');
        $this->load->model('md_perlengkapan');
        // $this->load->model('md_label');
        // $this->load->model('md_label_artikel');
        $this->load->model('md_keberangkatan_tour');
        $this->load->model('md_kota');
        $this->load->model('md_log');
        $this->load->model('md_paket_maskapai');
        $this->load->model('md_media');
        $this->load->model('md_media_detail');
        $this->load->model('md_member');
        $this->load->model('md_jemaah');
        $this->load->model('md_notifikasi');
        $this->load->model('md_paket');
        $this->load->model('md_paket_akomodasi');
        $this->load->model('md_pembayaran');
        $this->load->model('md_pendaftaran');
        $this->load->model('md_pendaftaran_detail');
        $this->load->model('md_pendaftaran_detail_biaya');
        $this->load->model('md_pengguna');
        $this->load->model('md_kantor_cabang');
        $this->load->model('md_slide');
        $this->load->model('md_syarat_status');
        $this->load->model('md_syarat_umroh');
        $this->load->model('md_konfirmasi');
        $this->load->model('md_testimoni');
        $this->load->model('md_tour_leader');
        $this->load->model('md_barang_kantor');
        $this->load->model('md_company');
        $this->load->model('md_kurs_dollar');

        $this->load->helper('datetime');
        $this->load->helper('notif');
        $this->load->helper('file');
        $this->load->helper('rwh');
        $this->load->helper('userLog');
        $this->load->helper('encrypt');
        $this->load->helper('upload');
        $this->load->helper('format');
        $this->load->helper('email');

        $this->load->library('email');
        $this->load->library('image_lib');  
        $this->load->library('googlefunction');

        $this->session->unset_userdata('login_page');
        $this->session->set_userdata('login_page','admin');
        
        $this->load->library('datatables');
        date_default_timezone_set('Asia/Jakarta');
    }

//#------------------------------------------------------------------------------------------------#//  
    public function index($param='') {
        //Penanda jika user sedang berada di page admin
        $this->session->set_userdata('admin_page','1');

        if($this->session->userdata('login_type')==""){
            $page_data = array('loginUrl' => $this->googlefunction->getLoginUrl());
            $this->session->set_userdata('loginUrl',$page_data['loginUrl']);
            $page_data['page_name'] = 'login';
            $page_data['page_title'] = 'Login Administrator';
            $this->load->view('login/index', $page_data); 
        }
        else {
            redirect(base_url().'administrator/dashboard');
        }     
    }


//#------------------------------------------------------------------------------------------------#//  
    public function dashboard($param1="",$param2=""){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        $login_type = $this->session->userdata('login_type');
        $page_data['show_jemaah'] = FALSE;

        if($param1 == 'show_jemaah'){
            $keberangkatan_id = decrypt($param2);
            $page_data['show_jemaah'] = TRUE;
            //JEmaah yang diambil hanya yang valid dan siap untuk diberangkatkan
            $x = $this->md_pendaftaran_detail->getPendaftaraDetailByKeberangkatanId($keberangkatan_id);
            $x = getMember($x);
            $page_data['show_jemaah'] = $x;
        } 
        else if($param1 == 'paginationTglKeberangkatan'){
            $dt = $this->md_keberangkatan->getLatestStatusSeat_pagination();
            $no=0;
            $data =array();
            $listbulan=all_bulan();
            foreach($dt['data'] as $row){
                $id = encrypt($row->keberangkatan_id);  
                $kuota = $row->kuota > 0 ? $row->kuota : 'Belum diset';
                $terisi = $kuota > 0 ? "<span class='label label-sm bg-green-jungle'>".($row->kuota-$row->terisi)."</span>" : '-';
                $disabled = '';
                if($row->terisi==0)
                    $disabled = 'disabled';
                $th1 = ++$no;
                $th2 = $row->nama_paket;
                $th3 = strtoupper(substr($listbulan[$row->bulan_berangkat],0,3)).' '.$row->tahun_berangkat;
                $th4 = date('d-F-Y',strtotime($row->tgl_keberangkatan));
                $th5 = $kuota;
                $th6 = '<a class="font-red"><b>'.$row->terisi.'</b></a>';
                $th7 = $terisi;
                $th8 = '<a href="'.base_url().'administrator/dashboard/show_jemaah/'.$id.'" class="btn btn-xs green btn_detail<?php echo $id ?>" '.$disabled.'>Cek Jemaah &nbsp;<i class="fa fa-search"></i></a>';
                $data[] = gathered_data(array($th1,$th2,$th3,$th4,$th5,$th6,$th7,$th8));
            }
            $dt['data'] = $data;
            echo json_encode($dt);
            die;
        }

        $jmh_berangkat = $this->md_pendaftaran_detail->countJemaahByStatus('berangkat');
        $jmh_proses    = $this->md_pendaftaran_detail->countJemaahByStatus('proses');
        $jmh_cancel    = $this->md_pendaftaran_detail->countJemaahByStatus('cancel');
        $jml_kntr_cbg  = $this->md_kantor_cabang->countKantorCabang();

        $page_data['jml_jamaah_berangkat'] = $jmh_berangkat[0]->total;
        $page_data['jml_jamaah_proses']    = $jmh_proses[0]->total;
        $page_data['jml_jamaah_cancel']    = $jmh_cancel[0]->total;
        $page_data['jml_kantor_cabang']    = $jml_kntr_cbg[0]->total;
        $page_data['jml_member']           = $this->md_member->getJmlMember();

        /*chart jml jamaah*/
        $page_data['bulan']       = $this->md_paket_waktu->getTop12PaketWaktuBulan();
        $page_data['kota']        = $this->md_kota->getAllKota();
        $page_data['log']         = $this->md_log->getTop30Activity($this->session->userdata('login_type'));
        $page_data['page_name']   = 'dashboard';
        $page_data['page_title']  = 'Dashboard';
        $this->load->view('login/index', $page_data);        
    }
//#------------------------------------------------------------------------------------------------#//  
    public function pengguna($param1='',$param2=''){
        $login_type = $this->session->userdata('login_type');
        if(!$login_type || $login_type == 'Perwakilan Daerah')
            redirect(base_url().'rwhgate');

        if($param1 == 'add'){
            $data['nama_lengkap'] = $this->input->post('nama_lengkap');
            $data['email'] = $this->input->post('email');
            $data['no_hp'] = $this->input->post('no_hp');
            $data['password'] = hash('sha512',$this->input->post('password'));
            $data['level'] = $this->input->post('level');
            $data['kantorcabang_id'] = decrypt($this->input->post("kantorcabang_id"));
            $data['status'] = 1;

            $this->md_pengguna->addPengguna($data);
            $this->md_log->addLog(adminLog('Menambah Pengguna','Menambah pengguna '.$data['nama_lengkap'].' sebagai '.$data['level']));
            echo json_encode('add_success');
            die;
        }
        else if($param1 == 'update'){
            $pengguna_id = decrypt($param2);
            $data['nama_lengkap'] = $this->input->post('nama_lengkap');
            $data['email'] = $this->input->post('email');
            $data['kantorcabang_id'] = decrypt($this->input->post("kantorcabang_id"));
            $data['no_hp'] = $this->input->post('no_hp');
            $data['level'] = $this->input->post('level');

            $this->md_pengguna->updatePengguna($pengguna_id,$data);
            $this->md_log->addLog(adminLog('Memperbaharui Pengguna','Memperbaharui data pengguna '.$data['nama_lengkap']));
            echo json_encode('update_success');
            die;
        }
        if($param1 == 'delete'){
            $pengguna_id = decrypt($param2);
            $temp = $this->md_pengguna->getPenggunaById($pengguna_id);
            $this->md_log->addLog(adminLog('Menghapus Pengguna','Menghapus pengguna '.$temp[0]->nama_lengkap));

            $data['status'] = 2;
            $this->md_notifikasi->updateNotifikasiByPenggunaId($pengguna_id,$data);
            $this->md_pengguna->updatePengguna($pengguna_id,$data);
            echo json_encode('update_success');
            die;
        }
        if($param1 == 'reset_akun'){
            $pengguna_id = decrypt($param2);
            $temp = $this->md_pengguna->getPenggunaById($pengguna_id);
            $uname = $this->input->post('update_email');
            $pass = $this->input->post('update_password');
            if($pass){
                $data['password'] = hash('sha512',$pass);
                $this->md_log->addLog(adminLog('Reset Password Pengguna','Melakukan reset password terhadap akun '.$temp[0]->email));
            } else {
                $data['email'] = $uname;
                $this->md_log->addLog(adminLog('Ubah Username Pengguna','Merubah username "'.$temp[0]->email.'" menjadi "'.$uname.'"'));
            }
            $this->md_pengguna->updatePengguna($pengguna_id,$data);

            echo json_encode('update_success');
            die;
        }
        if($param1 == 'getPenggunaKantorCabang'){
            $id = decrypt($param2);
            $x = $this->md_pengguna->getPenggunaByKantocabangId($id);
            foreach($x as $row){
                $row->pengguna_id = encrypt($row->pengguna_id);
            }
            echo json_encode($x);
            die;
        }   
        $page_data['kantor_cabang'] = getListKantorCabang($this->session->userdata('login_type'));
        $page_data['pengguna'] = $this->md_pengguna->getPenggunaAll();
        $page_data['page_name'] = 'pengguna';
        $page_data['page_title'] = 'Pengguna';
        $this->load->view('login/index', $page_data);        
    }
//#------------------------------------------------------------------------------------------------#//  
    public function bank($param1='',$param2=''){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');


        if($param1 == 'add'){
            $data['nama_bank']      = $this->input->post('nama_bank');
            $data['jenis']          = $this->input->post('jenis');
            $data['nomor_rekening'] = $this->input->post('nomor_rekening');
            $data['status']         = 1;

            if(empty($data['nama_bank']) || empty($data['jenis'])){
                $err['empty_val'] = 'Nama Bank dan Jenis harus diisi';
                echo json_encode($err);
                die;
            }

            $this->md_bank->addBank($data);

            //Log
                $this->md_log->addLog(adminLog('Menambah Bank','Menambah data bank '.$data['nama_bank']));
                

            echo json_encode('add_success');
            die;
        }
        else if($param1 == 'update'){
            $bank_id = decrypt($param2);
            $data['nama_bank']      = $this->input->post('nama_bank');
            $data['jenis']          = $this->input->post('jenis');
            $data['nomor_rekening'] = $this->input->post('nomor_rekening');
            $data['status']          = 1;
            $this->md_bank->updateBank($bank_id,$data);
            $this->md_log->addLog(adminLog('Memperbaharui Bank','Memperbaharui bank '.$data['nama_bank']));            
            echo json_encode('update_success');
            die;
        }
        
        else if($param1 == 'delete'){
            $bank_id = decrypt($param2);
            $data['status'] = 2;
            $nama_bank = $this->input->post('nama_bank');
            $this->md_bank->updateBank($bank_id,$data);
            $this->md_log->addLog(adminLog('Menghapus Bank','Menghapus Bank '.$nama_bank));            
            echo json_encode('update_success');
            die;
        }
        
        else if($param1 == 'pagination'){
            //List yang mucul hanya jemaah yang terverfikasi
                $dt = $this->md_bank->getAllBank();

            $no=0;
            $data =array();
            foreach($dt['data'] as $row){
                $id = encrypt($row->bank_id);

                //Aksi
                    $btn_edit    = '<li><a href="javascript:;" onClick="edit_function(\'edit\','. $id.');"><i class="fa fa-pencil"></i>&nbsp;Edit</a></li>';
                    $btn_delete  = '<li><a href="javascript:;" onClick="edit_function(\'delete\','. $id.')"><i class="fa fa-trash"></i>&nbsp;Hapus</a></li>';


                    if($row->dependency > 0){
                        $btn_delete = '';
                    }

                    $list_btn = $btn_edit.$btn_delete;
                    $aksi = '<div class="btn-group">
                                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    '.$list_btn.'
                                </ul>
                                <input type="hidden" id="nama_bank_'.$id.'" value="'.$row->nama_bank.'">
                                <input type="hidden" id="nomor_rekening_'.$id.'" value="'.$row->nomor_rekening.'">
                                <input type="hidden" id="jenis_'.$id.'" value="'.$row->jenis.'">
                                <input type="hidden" id="bank_id'.$id.'" value="'.$id.'">
                            </div>';

                $th1 = ++$no;
                $th2 = $row->nama_bank;
                $th3 = $row->nomor_rekening ? $row->nomor_rekening : '-';
                $th4 = $row->jenis;
                $th5 = $aksi;
                $data[] = gathered_data(array($th1,$th2,$th3,$th4,$th5));
            }
            $dt['data'] = $data;
            echo json_encode($dt);
            die;
        }

        
        $page_data['page_name'] = 'bank';
        $page_data['page_title'] = 'Bank';
        $this->load->view('login/index', $page_data);        
    }  
//#------------------------------------------------------------------------------------------------#//  
    public function barang($param1='',$param2=''){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        $page_data['riwayat'] = FALSE;
        $page_data['list_kantor'] = FALSE;
        $page_data['kantor_cabang'] = getListKantorCabang($this->session->userdata('login_type'));

        if($param1 == 'add'){

            $data['nama_barang']     = ucwords(strtolower($this->input->post('nama_barang')));
            $cek = $this->md_barang->getBarangByNamaBarang($data['nama_barang']);
            if($cek){
                $this->session->set_flashdata("error",$data['nama_barang'].' sudah tersedia !');          
            } else {
                $data['kode_barang']     = $this->input->post('kode_barang');
                $data['satuan']          = $this->input->post('satuan');
                $data['harga_terakhir']  = $this->input->post('harga_terakhir');
                $data['status']          = 1;
                $this->md_barang->addBarang($data);
                //Log
                    $this->md_log->addLog(adminLog('Menambah Barang','Menambah data barang '.$data['nama_barang']));
                    $this->session->set_flashdata("success",$data['nama_barang'].' berhasil ditambahkan');          

            }

            redirect('administrator/barang');
        }
        else if($param1 == 'update'){
            $bkantor_id      = decrypt($param2);
            $barang_id       = decrypt($this->input->post('barang_id'));
            //Update data barang terlebih dahulu
                $data['nama_barang']     = $this->input->post('nama_barang');
                $data['kode_barang']     = $this->input->post('kode_barang');
                $data['harga_terakhir']  = $this->input->post('harga_terakhir');
                $data['satuan']          = $this->input->post('satuan');
                $data['status']          = 1;
                $this->md_barang->updateBarang($barang_id,$data);
                
            $this->md_log->addLog(adminLog('Memperbaharui Barang','Memperbaharui Barang '.$data['nama_barang']));            
            echo json_encode('update_success');
            die;
        }
        else if($param1 =='updateLokasiBarang'){
            $error     = array();
            $success   = array();
            $result    = array();
            $barang_id = decrypt($param2);
            $bkNew = $this->input->post('kantor_cabang');

            if(count($bkNew)>0){
                $temp = $this->md_barang_kantor->getBarangKantorByBarangId($barang_id);
                foreach($temp as $row){
                    $bkOld[]=encrypt($row->kantorcabang_id);
                }

                //Barang sudah pernah ada pada kantor cabang namun terjadi penambahan atau pengurangan
                    if($temp){
                        //Cek yang baru dimasukkan
                            $bkNeedAdd = array_diff($bkNew,$bkOld);
                            foreach($bkNeedAdd as $row){
                                $id = decrypt($row);
                                $data2['barang_id']       = $barang_id;
                                $data2['kantorcabang_id'] = $id;
                                $data2['stok_akhir']      = 0;
                                $data2['status']          = 1;
                                $bkantor_id = $this->md_barang_kantor->addBarangKantor($data2);
                                $temp = $this->md_barang_kantor->getBarangKantorById($bkantor_id);
                                $info = $temp[0]->nama_barang.' ditambahkan ke kantor '.$temp[0]->nama_cabang;
                                $success[] = $info;
                                    //Log
                                        $this->md_log->addLog(adminLog('Menambah Lokasi Barang',$info));
                            }

                        //Cek Yang dihapus
                            $bkNeedDelete = array_diff($bkOld,$bkNew);
                            foreach($bkNeedDelete as $row){
                                $id = decrypt($row);
                                $cek = $this->md_transaksi->getBarangTransaksiByBarangByKantorCabang($barang_id,$id);
                                if($cek){
                                    $error[] = $cek[0]->nama_barang.' tidak bisa dihapus dari '.$cek[0]->nama_cabang.' karena sudah ada transaksi';
                                } else {
                                    $temp = $this->md_barang_kantor->getBarangKantorByBarangIdByKantorcabangId($barang_id,$id);
                                    $this->md_barang_kantor->deleteBarangKantor($temp[0]->bkantor_id);
                                    $info = $temp[0]->nama_barang.' berhasil dihapus dari '.$temp[0]->nama_cabang;
                                    $success[] = $info;
                                    //Log
                                        $this->md_log->addLog(adminLog('Menghapus Lokasi Barang',$info));
                                }
                                
                            }
                    }
                    //Barang belum pernah ada disalah satu kantor cabang sehingga harus ditambahkan baru
                    else {
                        foreach($bkNew as $row){
                            $id = decrypt($row);
                            $data2['barang_id']       = $barang_id;
                            $data2['kantorcabang_id'] = $id;
                            $data2['stok_akhir']      = 0;
                            $data2['status']          = 1;
                            $bkantor_id = $this->md_barang_kantor->addBarangKantor($data2);
                            $temp = $this->md_barang_kantor->getBarangKantorById($bkantor_id);
                            $info = $temp[0]->nama_barang.' ditambahkan ke kantor '.$temp[0]->nama_cabang;
                            $success[] = $info;
                                //Log
                                    $this->md_log->addLog(adminLog('Menambah Lokasi Barang',$info));
                        }
                    }

            } else {
                //Menghapus barang pada semua kantor cabang
                    $temp = $this->md_barang_kantor->getBarangKantorByBarangId($barang_id);
                    foreach($temp as $row){
                        $id = $row->kantorcabang_id;
                        $cek = $this->md_transaksi->getBarangTransaksiByBarangByKantorCabang($barang_id,$id);
                        if($cek){
                            $error[] = $cek[0]->nama_barang.' tidak bisa dihapus dari '.$cek[0]->nama_cabang.' karena sudah ada transaksi';
                        } else {
                                $temp = $this->md_barang_kantor->getBarangKantorByBarangIdByKantorcabangId($barang_id,$id);
                                $this->md_barang_kantor->deleteBarangKantor($temp[0]->bkantor_id);
                                $info = $temp[0]->nama_barang.' berhasil dihapus dari '.$temp[0]->nama_cabang;
                                $success[] = $info;
                                //Log
                                    $this->md_log->addLog(adminLog('Menghapus Lokasi Barang',$info));
                            }
                    }
            }

            array_push($result,$success);
            array_push($result,$error);
            echo json_encode($result);
            die;
        }
        else if($param1 == 'update'){
            $bkantor_id      = decrypt($param2);
            $barang_id       = decrypt($this->input->post('barang_id'));
            //Update data barang terlebih dahulu
                $data['nama_barang']     = $this->input->post('nama_barang');
                $data['kode_barang']     = $this->input->post('kode_barang');
                $data['harga_terakhir']  = $this->input->post('harga_terakhir');
                $data['satuan']          = $this->input->post('satuan');
                $data['status']          = 1;
                $this->md_barang->updateBarang($barang_id,$data);
                
            $this->md_log->addLog(adminLog('Memperbaharui Barang','Memperbaharui Barang '.$data['nama_barang']));            
            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'reset_stock'){
            $bkantor_id         = decrypt($this->input->post('bkantor_id'));
            $data['stok_akhir'] = $this->input->post('stok');
            $keterangan         = $this->input->post('keterangan');
            $this->md_barang_kantor->updateBarangKantorId($bkantor_id,$data);

            $temp = $this->md_barang_kantor->getBarangKantorById($bkantor_id);

            $this->md_log->addLog(adminLog('Reset Stok Barang','Reset Stok '.$temp[0]->nama_barang.' di kantor '.$temp[0]->nama_cabang.' menjadi '.$data['stok_akhir'].'. Keterangan : '.$keterangan));  
            $this->session->set_flashdata("update_success",'Reset stok '.$temp[0]->nama_barang.' di kantor '.$temp[0]->nama_cabang.' menjadi '.$data['stok_akhir'].' berhasil ');          
            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'delete_barangkantor'){
            $bkantor_id = decrypt($param2);
            $data['status'] = 2;
            $temp = $this->md_barang_kantor->getBarangKantorById($bkantor_id);
            $this->md_barang_kantor->updateBarangKantorId($bkantor_id,$data);
            $this->md_log->addLog(adminLog('Menghapus Barang Kantor','Menghapus Barang "'.$temp[0]->nama_barang.'" dari kantor '.$temp[0]->nama_cabang));            
            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'delete'){
            $barang_id = decrypt($param2);
            $data['status'] = 2;
            $temp = $this->md_barang->getBarangById($barang_id);
            $this->md_barang->updatebarang($barang_id,$data);
            $this->md_log->addLog(adminLog('Menghapus Barang Kantor','Menghapus Barang "'.$temp[0]->nama_barang.'"'));            
            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'list_kantor'){
            $barang_id = decrypt($param2);
            $x = $this->md_barang_kantor->getBarangKantorByBarangId($barang_id);
            $page_data['dt'] = $x;
            $page_data['brg'] = $this->md_barang->getBarangById($barang_id);
            $page_data['list_kantor'] = TRUE;
        }
        else if($param1 == 'riwayat'){
            $bkantor_id           = decrypt($param2);
            $x                    = $this->md_transaksi->getTop200RiwayatTransaksiBarang($bkantor_id);
            $page_data['dt']      = $x;
            $page_data['riwayat'] = TRUE;
        }
        else if($param1 == 'getListBarangByKantorCabang'){
            if($this->input->post('kantorcabang_id')=='Semua Kantor'){
                $x = $this->md_barang->getAllNamaBarangDistinct();
            } else {
                $kantorcabang_id = decrypt($this->input->post('kantorcabang_id'));
                $x = $this->md_barang->getBarangByKantorCabangId($kantorcabang_id);
            }

            foreach($x as $row){
                $row->bkantor_id = encrypt($row->bkantor_id);
            }
            echo json_encode($x);
            die;
        }
        else if($param1 == 'getListKantorCabangByBarang'){
            $barang_id    = decrypt($this->input->post('barang_id'));
            $allCabang    = $this->md_kantor_cabang->getAllKantorCabang();
            $barangKantor = $this->md_barang_kantor->getBarangKantorByBarangId($barang_id);
            foreach($allCabang as $ac){

                  $param=0;
                  $ac->bkantor_id = 0;
                   foreach($barangKantor as $row){
                        if($ac->kantorcabang_id == $row->kantorcabang_id){
                            $param++;
                            $ac->bkantor_id = encrypt($row->bkantor_id);
                        }
                   }

                   $ac->selected = 0;
                   if($param>0){
                    $ac->selected = 1;
                   }
                  $ac->kantorcabang_id = encrypt($ac->kantorcabang_id);

            }

            echo json_encode($allCabang);
            die;
        }
        else if($param1 == 'getAllNamaBarangDistinct'){

            $x = $this->md_barang->getAllNamaBarangDistinct();
            echo json_encode($x);
            die;
        }
        else if($param1 == 'pagination2'){
            //List yang mucul hanya jemaah yang terverfikasi
                $dt = $this->md_barang->getAllBarangOnly();

            $no=0;
            $data =array();
            foreach($dt['data'] as $row){
                $id = encrypt($row->barang_id);

                //Aksi
                    $btn_detail  = '<li><a href="'.base_url().'/administrator/barang/list_kantor/'.$id.'" ><i class="fa fa-search"></i>&nbsp;Detail</a></li>';
                    $btn_edit    = '<li><a href="javascript:;" onClick="edit_function(\'edit\','. $id.');"><i class="fa fa-pencil"></i>&nbsp;Edit</a></li>';
                    $btn_delete  = '<li><a href="javascript:;" onClick="edit_function(\'delete\','. $id.')"><i class="fa fa-trash"></i>&nbsp;Hapus</a></li>';
                    $btn_add_to_kantor = '<li><a href="javascript:;" onClick="edit_function(\'show_add_to_kantor\','. $id.')"><i class="fa fa-plus"></i>&nbsp;Lokasi</a></li>';

                    if($row->total_kantor > 0){
                        $btn_delete = '';
                    }

                    $list_btn = $btn_add_to_kantor.$btn_detail.$btn_edit.$btn_delete;
                    $aksi = '<div class="btn-group">
                                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    '.$list_btn.'
                                </ul>
                                <input type="hidden" id="nama_barang_'.$id.'" value="'.$row->nama_barang.'">
                                <input type="hidden" id="kode_barang_'.$id.'" value="'.$row->kode_barang.'">
                                <input type="hidden" id="harga_terakhir_'.$id.'" value="'.$row->harga_terakhir.'">
                                <input type="hidden" id="satuan_'.$id.'" value="'.$row->satuan.'">
                                <input type="hidden" id="barang_id_'.$id.'" value="'.encrypt($row->barang_id).'">
                            </div>';

                $th1 = ++$no;
                $th2 = $row->nama_barang;
                $th3 = $row->satuan;
                $th4 = $row->harga_terakhir ? '<span class="pull-left">Rp.</span><span class="pull-right">'.number_format($row->harga_terakhir).'</span>' : '-';
                $th5 = $row->total_kantor. ' Kantor';
                $th6 = $aksi;
                $data[] = gathered_data(array($th1,$th2,$th3,$th4,$th5,$th6));
            }
            $dt['data'] = $data;
            echo json_encode($dt);
            die;
        }
        
        $page_data['page_name'] = 'barang';
        $page_data['page_title'] = 'Barang';
        $this->load->view('login/index', $page_data);        
    } 
//#------------------------------------------------------------------------------------------------#//  
    public function company($param1='',$param2=''){
        if(!$this->session->userdata('login_type') || $this->session->userdata('login_type')=='Perwakilan Daerah')
            redirect(base_url().'rwhgate');


        else if($param1 == 'update'){
            $company_id = decrypt($param2);

            $media_id = ImageUploadToMedia('media','perusahaan_logo','logo_company');
            if($media_id != NULL){
                $data['media_id'] = $media_id;
                //Delete Old Media and Old Media Detail
                    $temp = $this->md_company->getCompanyById($id);
                    deleteMedia($temp[0]->media_id);
            }

            $data['nama_usaha'] = $this->input->post('nama_usaha');
            $data['nama_brand'] = $this->input->post('nama_brand');
            $data['kontak']     = $this->input->post('kontak');
            $data['alamat']       = $this->input->post('alamat');
            $data['kota']       = $this->input->post('kota');
            $data['provinsi']   = $this->input->post('provinsi');
            $data['keterangan'] = $this->input->post('keterangan');
            $this->md_company->updateCompany($company_id,$data);
            $this->md_log->addLog(adminLog('Memperbaharui Company','Memperbaharui Data Company '.$data['nama_usaha']));            
            $this->session->set_flashdata('update_success',1);
            redirect(base_url().'administrator/company');
        }

        $page_data['company'] = $this->md_company->getCompany();
        $page_data['page_name'] = 'company';
        $page_data['page_title'] = 'Data Perusahaan';
        $this->load->view('login/index', $page_data);        
    }
//#------------------------------------------------------------------------------------------------#//  
    public function hotel($param1='',$param2=''){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        if($param1 == 'add'){
            $media_id = ImageUploadToMedia('media','hotel_pic','logo_hotel');
            if($media_id != NULL)
                $data['media_id'] = $media_id;

            $data['nama_hotel'] = $this->input->post('nama_hotel');
            $data['kota'] = $this->input->post('kota');
            $data['deskripsi'] = $this->input->post('deskripsi');
            $data['bintang'] = $this->input->post('bintang');
            $data['status'] = 1;

            $this->md_hotel->addHotel($data);
            $this->md_log->addLog(adminLog('Menambah Hotel','Menambah hotel '.$data['nama_hotel']));
            echo json_encode('add_success');
            die;
        }
        else if($param1 == 'update'){
            $id = decrypt($this->input->post('hotel_id'));

            $media_id = ImageUploadToMedia('media','hotel_pic','logo_hotel');
            if($media_id != NULL){
                $data['media_id'] = $media_id;
                //Delete Old Media and Old Media Detail
                    $temp = $this->md_hotel->getHotelById($id);
                    deleteMedia($temp[0]->media_id);
            }
            $data['nama_hotel'] = $this->input->post('nama_hotel');
            $data['kota'] = $this->input->post('kota');            
            $data['deskripsi'] = $this->input->post('deskripsi');   
            $data['bintang'] = $this->input->post('bintang');

            $this->md_hotel->updateHotel($id,$data);
            $this->md_log->addLog(adminLog('Memperbaharui Hotel','Memperbaharui hotel '.$data['nama_hotel']));
            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'edit'){
            $id = decrypt($param2);
            $data['dt'] = $this->md_hotel->getHotelById($id);
            foreach($data['dt'] as $row){
                $row->hotel_id = encrypt($row->hotel_id);
                $temp = $this->md_media_detail->getMediaDetailByMediaId($row->media_id);
                if($temp){
                    $row->media_id = base_url().$temp[0]->media_link;
                }
            }
            echo json_encode($data);
            die;
        }
        else if($param1 == 'delete'){
            $id = decrypt($param2);
            $x = $this->md_hotel->getHotelById($id);
            deleteMedia($x[0]->media_id);
            $data['status'] = 2;
            $this->md_hotel->updateHotel($id,$data);
            $this->md_log->addLog(adminLog('Menghapus Hotel','Menghapus hotel '.$x[0]->nama_hotel));
            echo json_encode('update_success');
            die;
        }

        $page_data['hotel'] = $this->md_hotel->getAllHotel();
        $page_data['page_name'] = 'hotel';
        $page_data['page_title'] = 'Hotel';
        $this->load->view('login/index', $page_data);        
    }  
//#------------------------------------------------------------------------------------------------#//  
    public function jadwal_manasik($param1='',$param2=''){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        $login_type = $this->session->userdata('login_type');
        if($param1 == 'add'){
            $data['tgl_manasik']     = dateConvert2($this->input->post('tgl_manasik'));
            $data['pembicara']       = $this->input->post('pembicara');
            $data['tempat']          = $this->input->post('tempat');
            $data['alamat_tempat']   = $this->input->post('alamat_tempat');
            $data['keterangan']      = $this->input->post('keterangan');
            $data['status']          = 1;
            $this->md_jadwal_manasik->addJadwalManasik($data);

            $this->md_log->addLog(adminLog('Menjadwalkan Manasik','Menjadwalkan manasik pada tanggal '.date('d-M-Y H:i',strtotime($data['tgl_manasik'])).' di '.$data['tempat']));
            echo json_encode('add_success');
            die;
        }
        else if($param1 == 'edit'){
            $id = decrypt($param2);
            $x= $this->md_jadwal_manasik->getJadwalManasikById($id);
            foreach($x as $row){
                $row->jadwalmanasik_id = encrypt($row->jadwalmanasik_id);
            }
            echo json_encode($x);
            die;
        }
        else if($param1 == 'update'){
            $jadwalmanasik_id        = decrypt($this->input->post('jadwalmanasik_id'));
            $data['tgl_manasik']     = dateConvert2($this->input->post('tgl_manasik'));
            $data['tempat']          = $this->input->post('tempat');
            $data['pembicara']       = $this->input->post('pembicara');
            $data['alamat_tempat']   = $this->input->post('alamat_tempat');
            $data['keterangan']      = $this->input->post('keterangan');
            $data['status']          = 1;
            $this->md_jadwal_manasik->updateJadwalManasik($jadwalmanasik_id,$data);
            $this->md_log->addLog(adminLog('Memperbaharu Jadwali Manasik','Memperbaharui jadwal manasik '.date('d-M-Y H:i',strtotime($this->input->post('tgl_manasik')))));
            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'delete'){
            $jadwalmanasik_id = decrypt($param2);
            $data['status']   = 2;
            $this->md_jadwal_manasik->updateJadwalManasik($jadwalmanasik_id,$data);
            echo json_encode('update_success');
            die;
        }

        $page_data['jadwal_manasik'] = $this->md_jadwal_manasik->getAllJadwalManasik();
        $page_data['page_name'] = 'jadwal_manasik';
        $page_data['page_title'] = 'Jadwal Manasik';
        $this->load->view('login/index', $page_data);        
    }
//#------------------------------------------------------------------------------------------------#//  
    public function kantor_cabang($param1='',$param2=''){
        if(!$this->session->userdata('login_type') || $this->session->userdata('login_type')=='Perwakilan Daerah')
            redirect(base_url().'rwhgate');

        $login_type = $this->session->userdata('login_type');
        $page_data['list_jemaah'] = FALSE;
        $page_data['list_sahabat'] = FALSE;

        if($param1 == 'add'){

            $jenis = $this->input->post('jenis');
            //Generate Kode Kantor Cabang baru
            if($jenis == 'Kantor Cabang'){
                    $data = generateKodeKantorCabang();
            }

            $media_id = ImageUploadToMedia('media','kantorcabang_pic','avatar');
            if($media_id != NULL)
                $data['media_id'] = $media_id;

            $data['nama_cabang'] = $this->input->post('nama_cabang');
            $data['nama_daerah'] = $this->input->post('nama_daerah');
            $data['provinsi'] = $this->input->post('provinsi');
            $data['kota'] = $this->input->post('kota');
            $data['kode_kota'] = $this->input->post('kode_kota');
            $data['alamat'] = $this->input->post('alamat');
            $data['telp'] = $this->input->post('telp');
            $data['jenis'] = $this->input->post('jenis');
            $data['tgl_input'] = date('Y-m-d H:i:s');
            $data['author'] = $this->session->userdata('pengguna_id');
            $data['status'] = 1;
 
            $this->md_kantor_cabang->addKantorCabang($data);
            $this->md_log->addLog(adminLog('Menambah '.$jenis,'Menambah '.$jenis.' '.$data['nama_cabang']));
            echo json_encode('add_success');
            die;
        }
        else if($param1 == 'update'){
            $id = decrypt($this->input->post('kantorcabang_id'));
            $jenis = $this->input->post('jenis');
            $media_id = ImageUploadToMedia('media','kantorcabang_pic','avatar');

            if($media_id != NULL){
                $data['media_id'] = $media_id;
                //Delete Old Media and Old Media Detail
                    $temp = $this->md_kantor_cabang->getKantorCabangById($id);
                    deleteMedia($temp[0]->media_id);
            }

            $data['nama_cabang'] = $this->input->post('nama_cabang');
            $data['nama_daerah'] = $this->input->post('nama_daerah');
            $data['provinsi'] = $this->input->post('provinsi');
            $data['kota'] = $this->input->post('kota');
            $data['kode_kota'] = $this->input->post('kode_kota');
            $data['alamat'] = $this->input->post('alamat');
            $data['telp'] = $this->input->post('telp');
            $data['jenis'] = $jenis;
            $this->md_kantor_cabang->updateKantorCabang($id,$data);

            $this->md_log->addLog(adminLog('Memperbaharui '.$jenis,'Memperbaharui data '.$data['nama_cabang']));
            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'edit'){
            $id = decrypt($param2);
            $x = $this->md_kantor_cabang->getKantorCabangById($id);
            //Melengkapi array sebelum dilempar ke view
                for($i=0;$i<count($x);$i++){
                    //Pengambilan nama parent jika ada
                        $y = $this->md_kantor_cabang->getKantorCabangById($x[$i]->parent);
                        if($y)
                            $x[$i]->nama_parent = $y[0]->kode_afiliasi.'-'.$y[0]->nama_cabang;
                        else
                            $x[$i]->nama_parent = '-';

                    //Encrypt kantrocabang_id
                        $x[$i]->kantorcabang_id = encrypt($x[$i]->kantorcabang_id);
                        if($x[$i]->parent){
                            $temp = $this->md_kantor_cabang->getKantorCabangById($x[$i]->parent);
                            $x[$i]->parent = encrypt($temp[0]->kantorcabang_id);
                        }

                    //Pengambilan URL media
                        if($x[$i]->media_id){
                            $y = $this->md_media_detail->getMediaDetailByMediaId($x[$i]->media_id);
                            $x[$i]->media_id = base_url().$y[0]->media_link;
                        }
                }

            echo json_encode($x);
            die;       
        }
        else if($param1 == 'delete'){
            $id = decrypt($param2);
            $data['status'] = 2;
            
            //Delete Image Kantor Cabang
                $temp = $this->md_kantor_cabang->getKantorCabangById($id);
                    deleteMedia($temp[0]->media_id);

                if($temp[0]->parent){
                    $jenis = 'Perwakilan Daerah';
                }
                else {
                    $jenis = 'Kantor Cabang';
                }
                $nama_cabang = $temp[0]->nama_cabang;

            //Delete Pengguna
                $this->md_pengguna->updatePenggunaByKantorcabangId($id,$data);

            $this->md_kantor_cabang->updateKantorCabang($id,$data);

            $this->md_log->addLog(adminLog('Menghapus '.$jenis,'Menghapus data '.$nama_cabang));
            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'getAllKantorCabang'){
            $x = getListKantorCabang($this->session->userdata('login_type'));
            echo json_encode($x);
            die;
        }

        $page_data['kantor_cabang'] = getListKantorCabang($this->session->userdata('login_type'));
        $page_data['page_name'] = 'kantor_cabang';
        $page_data['page_title'] = 'Kantor Cabang';
        $this->load->view('login/index', $page_data);        
    }
//#------------------------------------------------------------------------------------------------#//  
    public function kelas($param1='',$param2=''){
        if(!$this->session->userdata('login_type') || $this->session->userdata('login_type')=='Perwakilan Daerah')
            redirect(base_url().'rwhgate');

        if($param1 == 'add'){
            $data['kelas'] = $this->input->post('kelas');
            $data['jumlah_sekamar'] = $this->input->post('jumlah_sekamar');
            $data['status'] = 1;
            $this->md_kelas->addKelas($data);
            $this->md_log->addLog(adminLog('Menambah Kelas','Menambah Kelas '.$data['kelas']));
            echo json_encode('add_success');

            die;
        }
        if($param1 == 'update'){
            $kelas_id = decrypt($param2);
            $data['kelas'] = $this->input->post('kelas');
            $data['jumlah_sekamar'] = $this->input->post('jumlah_sekamar');
            $this->md_kelas->updateKelas($kelas_id,$data);
            $this->md_log->addLog(adminLog('Memperbaharui Kelas','Memperbaharui Kelas '.$data['kelas']));            
            echo json_encode('update_success');
            die;
        }
        if($param1 == 'delete'){
            $kelas_id = decrypt($param2);
            $data['status'] = 2;
            $this->md_kelas->updateKelas($kelas_id,$data);
            $this->md_log->addLog(adminLog('Menghapus Kelas','Menghapus Kelas '.$data['kelas']));            
            echo json_encode('update_success');
            die;
        }

        $page_data['kelas'] = $this->md_kelas->getAllKelas();
        $page_data['page_name'] = 'kelas';
        $page_data['page_title'] = 'Kelas';
        $this->load->view('login/index', $page_data);        
    }
//#------------------------------------------------------------------------------------------------#//  
    public function kota($param1='',$param2=''){
        if(!$this->session->userdata('login_type') || $this->session->userdata('login_type')=='Perwakilan Daerah')
            redirect(base_url().'rwhgate');

        if($param1 == 'add'){
            $data['nama_kota']     = $this->input->post('nama_kota');
            $data['kode_kota']     = $this->input->post('kode_kota');
            $data['status']          = 1;
            $kota_id = $this->md_kota->addKota($data);

            $this->md_log->addLog(adminLog('Menambah Kota','Menambah kota '.$data['nama_kota']));
            echo json_encode('add_success');
            die;
        }
        else if($param1 == 'update'){
            $kota_id = decrypt($param2);
            $data['nama_kota']     = $this->input->post('nama_kota');
            $data['kode_kota']     = $this->input->post('kode_kota');
            $this->md_kota->updateKota($kota_id,$data);
            $this->md_log->addLog(adminLog('Memperbaharui Kota','Memperbaharui Kota '.$data['nama_kota']));            
            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'delete'){
            $kota_id = decrypt($param2);
            $data['status'] = 2;
            $temp = $this->md_kota->getKotaById($kota_id);
            $this->md_kota->updateKota($kota_id,$data);
            $this->md_log->addLog(adminLog('Menghapus Kota','Menghapus Kota '.$temp[0]->nama_kota));            
            echo json_encode('update_success');
            die;
        }

        else if($param1 == 'pagination'){
            //List yang mucul hanya jemaah yang terverfikasi
                $dt = $this->md_kota->getAllKota();

            $no=0;
            $data =array();
            foreach($dt['data'] as $row){
                $id = encrypt($row->kota_id);

                //Aksi
                    $btn_edit = '<li><a href="javascript:;" onClick="edit_function(\'edit\','. $id.');"><i class="fa fa-pencil"></i>&nbsp;Edit</a></li>';
                    $btn_delete = '<li><a href="javascript:;" onClick="edit_function(\'delete\','. $id.')"><i class="fa fa-trash"></i>&nbsp;Hapus</a></li>';

                    if($row->dependency > 0)
                        $btn_delete = '';

                    $list_btn = $btn_edit.$btn_delete;
                    $aksi = '<div class="btn-group">
                                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    '.$list_btn.'
                                </ul>
                                <input type="hidden" id="nama_kota_'.$id.'" value="'.$row->nama_kota.'">
                                <input type="hidden" id="kode_kota_'.$id.'" value="'.$row->kode_kota.'">
                            </div>';

                $th1 = ++$no;
                $th2 = $row->nama_kota;
                $th3 = $row->kode_kota;
                $th4 = $aksi;
                $data[] = gathered_data(array($th1,$th2,$th3,$th4));
            }
            $dt['data'] = $data;
            echo json_encode($dt);
            die;
        }
        $page_data['kota'] = $this->md_kota->getAllKota();
        $page_data['page_name'] = 'kota';
        $page_data['page_title'] = 'Kota';
        $this->load->view('login/index', $page_data);        
    }
//#------------------------------------------------------------------------------------------------#//  
    public function maskapai($param1='',$param2=''){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        if($param1 == 'add'){
            $media_id = ImageUploadToMedia('media','maskapai_pic','logo_maskapai');
            if($media_id != NULL)
                $data['media_id'] = $media_id;

            $data['nama_maskapai'] = $this->input->post('nama_maskapai');
            $data['kode_maskapai'] = $this->input->post('kode_maskapai');
            $data['deskripsi'] = $this->input->post('deskripsi');
            $data['tgl_post'] = date('Y-m-d H:i:s');
            $data['status'] = 1;

            $this->md_maskapai->addMaskapai($data);
            $this->md_log->addLog(adminLog('Menambah Maskapai','Menambah maskapai '.$data['nama_maskapai']));
            echo json_encode('add_success');
            die;
        }
        else if($param1 == 'update'){
            $id = decrypt($this->input->post('maskapai_id'));

            $media_id = ImageUploadToMedia('media','maskapai_pic','logo_maskapai');
            if($media_id != NULL){
                $data['media_id'] = $media_id;
                //Delete Old Media and Old Media Detail
                    $temp = $this->md_maskapai->getMaskapaiById($id);
                    deleteMedia($temp[0]->media_id);
            }

            $data['nama_maskapai'] = $this->input->post('nama_maskapai');
            $data['kode_maskapai'] = $this->input->post('kode_maskapai');
            $data['deskripsi'] = $this->input->post('deskripsi');            
            $this->md_maskapai->updateMaskapai($id,$data);
            $this->md_log->addLog(adminLog('Memperbaharui Maskapai','Memperbaharui maskapai '.$data['nama_maskapai']));
            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'edit'){
            $id = decrypt($param2);
            $data['dt'] = $this->md_maskapai->getMaskapaiById($id);
            foreach($data['dt'] as $row){
                $row->maskapai_id = encrypt($row->maskapai_id);
                $temp = $this->md_media_detail->getMediaDetailByMediaId($row->media_id);
                if($temp){
                    $row->media_id = base_url().$temp[0]->media_link;
                }
            }
            echo json_encode($data);
            die;
        }
        else if($param1 == 'delete'){
            $id = decrypt($param2);
            //Delete Image Kantor Cabang
                $temp = $this->md_maskapai->getMaskapaiById($id);
                deleteMedia($temp[0]->media_id);
            $data['status'] = 2;
            $this->md_maskapai->updateMaskapai($id,$data);
            $this->md_log->addLog(adminLog('Menghapus Maskapai','Menghapus maskapai '.$temp[0]->nama_maskapai));
            echo json_encode('update_success');
            die;
        }

        $page_data['maskapai'] = $this->md_maskapai->getAllMaskapai();
        $page_data['page_name'] = 'maskapai';
        $page_data['page_title'] = 'Maskapai';
        $this->load->view('login/index', $page_data);        
    }
//#------------------------------------------------------------------------------------------------#//  
    public function merchant($param1='',$param2=''){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        if($param1 == 'add'){
            $media_id = ImageUploadToMedia('media','merchant_pic','logo_merchant');
            if($media_id != NULL)
                $data['media_id'] = $media_id;

            $data['merchant'] = $this->input->post('merchant');
            $data['deskripsi'] = $this->input->post('deskripsi');
            $data['tgl_post'] = date('Y-m-d H:i:s');
            $data['status'] = 1;

            $this->md_merchant->addMerchant($data);
            $this->md_log->addLog(adminLog('Menambah Merchant','Menambah merchant '.$data['merchant']));
            echo json_encode('add_success');
            die;
        }
        else if($param1 == 'update'){
            $id = decrypt($this->input->post('merchant_id'));

            $media_id = ImageUploadToMedia('media','merchant_pic','logo_merchant');
            if($media_id != NULL){
                $data['media_id'] = $media_id;
                //Delete Old Media and Old Media Detail
                    $temp = $this->md_merchant->getMerchantById($id);
                    deleteMedia($temp[0]->media_id);
            }

            $data['merchant'] = $this->input->post('merchant');
            $data['deskripsi'] = $this->input->post('deskripsi');            
            $this->md_merchant->updateMerchant($id,$data);
            $this->md_log->addLog(adminLog('Memperbaharui Merchant','Memperbaharui data merchant '.$data['merchant']));
            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'edit'){
            $id = decrypt($param2);
            $data['dt'] = $this->md_merchant->getMerchantById($id);
            foreach($data['dt'] as $row){
                $row->merchant_id = encrypt($row->merchant_id);
                $temp = $this->md_media_detail->getMediaDetailByMediaId($row->media_id);
                if($temp){
                    $row->media_id = base_url().$temp[0]->media_link;
                }
            }
            echo json_encode($data);
            die;
        }
        else if($param1 == 'delete'){
            $id = decrypt($param2);
            //Delete Image Merchant
                $temp = $this->md_merchant->getMerchantById($id);
                deleteMedia($temp[0]->media_id);
            $data['status'] = 2;
            $this->md_merchant->updateMerchant($id,$data);
            $this->md_log->addLog(adminLog('Menghapus Merchant','Menghapus merchant '.$temp[0]->merchant));
            echo json_encode('update_success');
            die;
        }

        $page_data['merchant'] = $this->md_merchant->getAllMerchant();
        $page_data['page_name'] = 'merchant';
        $page_data['page_title'] = 'Merchant';
        $this->load->view('login/index', $page_data);        
    }
//#------------------------------------------------------------------------------------------------#//  
    public function paket($param1='',$param2=''){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        $page_data['form_paket']       = FALSE;
        $page_data['form_haji']        = FALSE;
        $page_data['edit_paket']       = FALSE;
        $page_data['edit_paket_haji']  = FALSE;

        if($param1 == 'add'){

            //Add media First
                $media_id        = ImageUploadToMedia('Paket','paket_pic','paket');
                $media_mobile_id = ImageUploadToMedia('Paket Mobile','paket_mobile_pic','paket_mobile');


            if($this->input->post('nama_paket_haji')){
                //Add Paket
                    $nama                    = $this->input->post('nama_paket_haji');
                    $data['nama_paket']      = $nama;
                    $data['media_id']        = $media_id;
                    $data['media_mobile_id'] = $media_mobile_id;
                    $data['jenispaket_id']   = 2;
                    $data['info_plus']       = $this->input->post('info_plus');
                    $data['jenis_quota']     = $this->input->post('jenis_kuota');
                    $data['masa_tunggu']     = $this->input->post('masa_tunggu');
                    
                    $data['tgl_submit']      = date('Y-m-d H:i:s');
                    $data['last_update']     = date('Y-m-d H:i:s');
                    $data['author']          = 'Administrator';
                    $data['status']          = 1;

                    $harga_terakhir          = remove_number_format($this->input->post('harga_terakhir'));
                    $harga_terakhir_usd_kurs = remove_number_format($this->input->post('add_paket_kurs'));
                    //Jika kurs_dollar berisi, berarti harga rupiah berdasarkan harga_usd * kurs_dollar
                        if($harga_terakhir_usd_kurs){
                            $data['harga_terakhir_usd']      = $harga_terakhir;
                            $data['harga_terakhir']          = $harga_terakhir * $harga_terakhir_usd_kurs;
                            $data['harga_terakhir_usd_kurs'] = $harga_terakhir_usd_kurs;
                        } else {
                            $data['harga_terakhir_usd']      = null;
                            $data['harga_terakhir']          = $harga_terakhir;
                            $data['harga_terakhir_usd_kurs'] = null;
                        }
                    $this->md_paket->addPaket($data);
                //Log
                    $this->md_log->addLog(adminLog('Menambah Paket Haji','Menambah Paket'.$nama));
            }
            else {
                //Add Paket
                    $nama                      = $this->input->post('nama_paket');
                    $data['nama_paket']        = $nama;
                    $data['media_id']          = $media_id;
                    $data['media_mobile_id']   = $media_mobile_id;
                    $data['jenispaket_id']     = decrypt($this->input->post('jenispaket_id'));
                    $data['berangkat_awal']    = decrypt($this->input->post('berangkat_awal'));
                    $data['berangkat_transit'] = $this->input->post('berangkat_transit');
                    $data['berangkat_landing'] = $this->input->post('berangkat_landing');
                    $data['info_plus']         = $this->input->post('info_plus');
                    $data['lama_hari']         = $this->input->post('lama_hari');
                    $data['mulai_publish']     = dateConvert2($this->input->post('mulai_publish'));
                    $data['akhir_publish']     = dateConvert2($this->input->post('akhir_publish'));
                    $data['tgl_submit']        = date('Y-m-d H:i:s');
                    $data['last_update']       = date('Y-m-d H:i:s');
                    $data['author']            = 'Administrator';
                    $data['status']            = 1;

                    $this->md_paket->addPaket($data);

                //Add Maskapai,Hotel, Paket_Waktu, Kelas dan Harga
                    $paket    = $this->md_paket->getLatestPaket();
                    $maskapai = $this->input->post('maskapai');

                    for($i=0;$i<count($maskapai);$i++){
                        $data0['paket_id']    = $paket[0]->paket_id;
                        $data0['maskapai_id'] = decrypt($maskapai[$i]);
                        $data0['status']      = 1;
                        $this->md_paket_maskapai->addPaketMaskapai($data0);
                    }


                    for($i=1;;$i++){
                        $a = $this->input->post('bulan_berangkat_'.$i);
                        $b = $this->input->post('tahun_berangkat_'.$i);
                        if(!$a && !$b)
                            break; 

                        $data2['paket_id']        = $paket[0]->paket_id;
                        $data2['bulan_berangkat'] = $a;
                        $data2['tahun_berangkat'] = $b;
                        $data2['status']          = 1;
                        $this->md_paket_waktu->addPaketWaktu($data2);

                        $paketwaktu  = $this->md_paket_waktu->getLatestPaketWaktu();
                        $kurs_dollar = str_replace('.',"",$this->input->post('add_paket_kurs'));
                        for($j=1;;$j++){
                            $d = $this->input->post('kelas_'.$i.'_'.$j);
                            $e = str_replace('.',"",$this->input->post('harga_'.$i.'_'.$j));

                            if(!$d && !$e)
                                break;

                            //Jika kurs_dollar berisi, berarti harga rupiah berdasarkan harga_usd * kurs_dollar
                            if($kurs_dollar){
                                $data3['harga_usd']  = $e;
                                $data3['harga']      = (int)$e * (int)$kurs_dollar;
                                $data3['kurs_dollar']   = $kurs_dollar;
                            } else {
                                $data3['harga_usd']   = null;
                                $data3['harga']       = $e;
                                $data3['kurs_dollar'] = null;
                            }

                            $data3['paketwaktu_id'] = $paketwaktu[0]->paketwaktu_id;
                            $data3['kelas_id']      = $d;                            
                            $data3['harga_awal']    = $data3['harga'];
                            $data3['status']        = 1;

                            $this->md_paketwaktu_kelas->addPaketWaktuKelas($data3);
                        }
                    }
                    
                    $hotel =$this->input->post('hotel');
                    for($i=0;$i<count($hotel);$i++){
                        $data4['paket_id'] = $paket[0]->paket_id;
                        $data4['hotel']    = decrypt($hotel[$i]);
                        $data4['status']   = 1;
                        $this->md_paket_akomodasi->addPaketAkomodasi($data4);
                    }

                $this->md_log->addLog(adminLog('Menambah Paket','Menambah Paket '.$nama));
            }

            echo json_encode('add_success');
            die;
        }
        else if($param1 == 'detail' || $param1 == 'edit'){

            $id   = decrypt($param2);
            $data = array();
            $x    = $this->md_paket->getPaketByPaketId($id);
            foreach($x as $row){
                $a        = $this->md_jenis_paket->getJenisPaketByJenisPaketId($row->jenispaket_id);
                $b        = $this->md_media_detail->getMediaDetailByMediaId($row->media_id);
                $b_mobile = $this->md_media_detail->getMediaDetailByMediaId($row->media_mobile_id);

                $dt['paket_id']  = encrypt($x[0]->paket_id);
                $dt['paket_pic'] = base_url().$b[0]->media_link;

                if($b_mobile)
                    $dt['paket_mobile_pic'] = base_url().$b_mobile[0]->media_link;
                else 
                    $dt['paket_mobile_pic'] = base_url().'assets/frontend/img/noimagefound.jpg';

                if($a[0]->jenis_paket=='Haji'){
                    $dt['jenispaket_id']  = encrypt($a[0]->jenispaket_id);
                    $dt['jenis_paket']    = $a[0]->jenis_paket;
                    $dt['nama_paket']     = $x[0]->nama_paket;
                    $dt['masa_tunggu']    = $x[0]->masa_tunggu;
                    $dt['jenis_quota']    = $x[0]->jenis_quota;
                    $dt['harga_terakhir'] = number_format($x[0]->harga_terakhir);
                    $dt['harga_terakhir_usd'] = number_format($x[0]->harga_terakhir_usd);
                    $dt['harga_terakhir_usd_kurs'] = number_format($x[0]->harga_terakhir_usd_kurs);
                    $dt['info_plus']      = $x[0]->info_plus;
                } 
                else {
                    //Maskapai (d)
                        $temp = $this->md_paket_maskapai->getPaketMaskapaiByPaketId($id);
                        for($j=0;$j<count($temp);$j++){
                            $_temp                  = $this->md_maskapai->getMaskapaiById($temp[$j]->maskapai_id);
                            $d[$j]['nama_maskapai'] = $_temp[0]->nama_maskapai;
                            $d[$j]['maskapai_id']   = encrypt($_temp[0]->maskapai_id);
                            $d[$j]['deskripsi']     = $_temp[0]->deskripsi;
                            //Ambil Foto
                                $__temp = $this->md_media_detail->getMediaDetailByMediaId($_temp[0]->media_id);
                                $d[$j]['logo'] = base_url().$__temp[1]->media_link;
                        }
                        
                    //Paket Akomodasi (e)
                        $temp = $this->md_paket_akomodasi->getpaketAkomodasiByPaketId($id);
                        for($j=0;$j<count($temp);$j++){
                            $_temp               = $this->md_hotel->getHotelById($temp[$j]->hotel);
                            $e[$j]['hotel_id']   = encrypt($_temp[0]->hotel_id);
                            $e[$j]['nama_hotel'] = $_temp[0]->nama_hotel;
                            $e[$j]['kota']       = $_temp[0]->kota;
                            $e[$j]['bintang']    = $_temp[0]->bintang;
                            $e[$j]['deskripsi']  = $_temp[0]->deskripsi;
                            //Ambil Foto
                                $__temp = $this->md_media_detail->getMediaDetailByMediaId($_temp[0]->media_id);
                                $e[$j]['foto'] = base_url().$__temp[1]->media_link;
                        }

                    //Paket Waktu
                        $temp = $this->md_paket_waktu->getPaketWaktuByPaketIdV2($id);
                        $bulan = all_bulan();
                        for($j=0;$j<count($temp);$j++){
                            $__temp                          = $this->md_kelas->getKelasById($temp[$j]->kelas_id);
                            $f[$j]['pwk_id']                 = encrypt($temp[$j]->pwk_id);
                            $f[$j]['paketwaktu_id']          = encrypt($temp[$j]->paketwaktu_id);
                            $f[$j]['bulan_berangkat']        = $temp[$j]->bulan_berangkat;
                            $f[$j]['bulan_berangkat_format'] = $bulan[$temp[$j]->bulan_berangkat];
                            $f[$j]['tahun_berangkat']        = $temp[$j]->tahun_berangkat;
                            $f[$j]['kelas_id']               = encrypt($temp[$j]->kelas_id);
                            $f[$j]['kelas']                  = $__temp[0]->kelas;
                            $f[$j]['jumlah_sekamar']         = $__temp[0]->jumlah_sekamar;
                            $f[$j]['harga']                  = number_format($temp[$j]->harga);
                            $f[$j]['harga_usd']              = $temp[$j]->harga_usd ? $temp[$j]->harga_usd : null;
                            $f[$j]['kurs_dollar']            = $temp[$j]->kurs_dollar ? $temp[$j]->kurs_dollar : null;
                            $f[$j]['promo']                  = $temp[$j]->promo;
                            $f[$j]['diskon']                 = $temp[$j]->diskon ? number_format($temp[$j]->diskon) : null;
                            $f[$j]['total_jemaah']           = $this->md_pendaftaran_detail->getCountPendaftaranDetailByPwkId($temp[$j]->pwk_id)[0]->total;
                            $f[$j]['berangkat']              = $bulan[$temp[$j]->bulan_berangkat].' '.$temp[$j]->tahun_berangkat;
                            
                            //Keberangkatan setiap paketwaktu_id
                                $kbr = $this->md_keberangkatan->getKeberangkatanByPaketWaktuId($temp[$j]->paketwaktu_id);
                                $tgl_berangkat=''; 
                                if($kbr){
                                    foreach($kbr as $_kbr){
                                        $tgl_berangkat = $tgl_berangkat.date('d-M-Y',strtotime($_kbr->tgl_keberangkatan)).'<br>';
                                    }
                                } else {
                                    $tgl_berangkat = '-';
                                }
                            $f[$j]['tgl_berangkat'] = $tgl_berangkat;

                        }

                    //Gabung Semua data paket
                        $c                       = $this->md_kota->getKotaById($row->berangkat_awal);
                        $dt['nama_paket']        = $x[0]->nama_paket;
                        $dt['jenis_paket']       = $a[0]->jenis_paket;
                        $dt['jenispaket_id']     = encrypt($a[0]->jenispaket_id);
                        $dt['berangkat_awal']    = '('.$c[0]->kode_kota.') '.$c[0]->nama_kota;
                        $dt['kota_id']           = encrypt($c[0]->kota_id);
                        $dt['berangkat_transit'] = $x[0]->berangkat_transit;
                        $dt['berangkat_landing'] = $x[0]->berangkat_landing;
                        $dt['info_plus']         = $x[0]->info_plus;
                        $dt['lama_hari']         = $x[0]->lama_hari;
                        $dt['mulai_publish']     = date('d-F-Y H:i:s',strtotime($x[0]->mulai_publish));
                        $dt['akhir_publish']     = date('d-F-Y H:i:s',strtotime($x[0]->akhir_publish));
                        $dt['maskapai']          = $d;
                        $dt['paket_akomodasi']   = $e;
                        $dt['paket_waktu']       = $f;
                }
            }

            if($param1=='edit' && $dt['jenis_paket'] != 'Haji'){
                $page_data['edit_paket']=$dt;
            }
            else if($param1=='edit' && $dt['jenis_paket'] == 'Haji'){
                $page_data['edit_paket_haji']=$dt;
            }
            else {
                echo json_encode($dt);
                die;
            }
        }
        else if($param1 == 'simpanDataPaket'){

            //Data Paket
                $paket_id = decrypt($this->input->post('paket_id'));
                $media_id = ImageUploadToMedia('Paket','paket_pic','paket');
                if($media_id != NULL){
                    $data['media_id'] = $media_id;
                    $temp             = $this->md_paket->getPaketByPaketId($paket_id);
                    deleteMedia($temp[0]->media_id);
                }  

                $media_mobile_id = ImageUploadToMedia('Paket_Mobile','paket_mobile_pic','paket_mobile');
                if($media_mobile_id != NULL){
                    $data['media_mobile_id'] = $media_mobile_id;
                    $temp                    = $this->md_paket->getPaketByPaketId($paket_id);
                    deleteMedia($temp[0]->media_mobile_id);
                }   

                $data['jenispaket_id'] = decrypt($this->input->post('jenispaket_id'));
                //Edit paket Haji
                if($data['jenispaket_id'] == 2){
                    $data['nama_paket']     = $this->input->post('nama_paket');
                    $data['masa_tunggu']    = $this->input->post("masa_tunggu");
                    $data['jenis_quota']    = $this->input->post("jenis_quota");
                    $data['info_plus']      = $this->input->post('info_plus');
                    $data['last_update']    = date('Y-m-d H:i:s');
                    
                    $harga_terakhir          = remove_number_format($this->input->post('harga_terakhir'));
                    $harga_terakhir_usd_kurs = remove_number_format($this->input->post('add_paket_kurs'));
                    //Jika kurs_dollar berisi, berarti harga rupiah berdasarkan harga_usd * kurs_dollar
                        if($harga_terakhir_usd_kurs){
                            $data['harga_terakhir_usd']      = $harga_terakhir;
                            $data['harga_terakhir']          = $harga_terakhir * $harga_terakhir_usd_kurs;
                            $data['harga_terakhir_usd_kurs'] = $harga_terakhir_usd_kurs;
                        } else {
                            $data['harga_terakhir_usd']      = null;
                            $data['harga_terakhir']          = $harga_terakhir;
                            $data['harga_terakhir_usd_kurs'] = null;
                        }
                    $this->md_paket->updatePaket($paket_id,$data);
                } 
                //Edit paket Reguler
                else {            
                    $data['lama_hari']         = $this->input->post('lama_hari');
                    $data['mulai_publish']     = dateConvert2($this->input->post('mulai_publish'));
                    $data['akhir_publish']     = dateConvert2($this->input->post('akhir_publish'));
                    $data['berangkat_awal']    = decrypt($this->input->post('berangkat_awal'));
                    $data['berangkat_transit'] = $this->input->post('berangkat_transit');
                    $data['berangkat_landing'] = $this->input->post('berangkat_landing');    
                    $data['nama_paket']        = $this->input->post('nama_paket');
                    $data['info_plus']         = $this->input->post('info_plus');
                    $data['last_update']       = date('Y-m-d H:i:s');
                    $this->md_paket->updatePaket($paket_id,$data);

                    //Hapus semua paket_maskapai baru masukkan yang baru
                        $this->md_paket_maskapai->deletePaketMaskapai($paket_id);
                        $maskapai =$this->input->post('maskapai');
                        for($i=0;$i<count($maskapai);$i++){
                            $data0['paket_id'] = $paket_id;
                            $data0['maskapai_id'] = decrypt($maskapai[$i]);
                            $data0['status'] = 1;
                            $this->md_paket_maskapai->addPaketMaskapai($data0);
                        }

                    //Hapus semua paket_akomodasi
                        $this->md_paket_akomodasi->deletePaketAkomodasiByPaketId($paket_id);
                        $hotel =$this->input->post('hotel');
                        for($i=0;$i<count($hotel);$i++){
                            $data4['paket_id'] = $paket_id;
                            $data4['hotel'] = decrypt($hotel[$i]);
                            $data4['status'] = 1;
                            $this->md_paket_akomodasi->addPaketAkomodasi($data4);
                        }
            }

            $x = $this->md_paket->getPaketByPaketId($paket_id);
            $this->md_log->addLog(adminLog('Memperbaharui Data Paket','Memperbaharui Data Paket '.$x[0]->nama_paket));

            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'delete'){
            $id = decrypt($param2);

            $data['status'] = 2;
            $x = $this->md_paket->getPaketByPaketId($id);

            deleteMedia($x[0]->media_id);
            deleteMedia($x[0]->media_mobile_id);


            $this->md_paket_akomodasi->updatePaketAkomodasiByPaketId($id,$data);

            $y = $this->md_paket_waktu->getPaketWaktuByPaketId($id);
            foreach($y as $row){
                $this->md_paketwaktu_kelas->updatePaketWaktuKelasByPaketWaktuId($row->paketwaktu_id,$data);
            }

            $this->md_paket_waktu->updatePaketWaktuByPaketId($id,$data);
            $this->md_paket_maskapai->deletePaketMaskapai($id);
            $this->md_paket->updatePaket($id,$data);

            $this->md_log->addLog(adminLog('Menghapus Paket','Menghapus Paket '.$x[0]->nama_paket));

            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'getDetailHotel'){
            $id = decrypt($param2);
            $x = $this->md_hotel->getHotelById($id);
            echo json_encode($x);
            die;
        }
        else if($param1 == 'getAutoCompleteData'){

            $maskapai['maskapai'] = array();
            $transit['transit']=array();
            $landing['landing'] = array();
            $berangkat['berangkat'] = array();
            $hotel['hotel'] = array();

            //Ambil data yang dibutuhkan
                $v = $this->md_paket_akomodasi->getAllHotel();

                $x = $this->md_paket->getAllLanding();    
                $y = $this->md_paket->getAllTransit();

            //Ambil bagian yang dibutuhkan pada data yang tersedia
                foreach($v as $row){array_push($hotel['hotel'], $row->hotel);}
                foreach($x as $row2){array_push($landing['landing'],$row2->berangkat_landing);}
                foreach($y as $row3){array_push($transit['transit'],$row3->berangkat_transit);}

            //Gabung bagian jadi satu untuk di kirim ke halaman
                $datax = array();
                array_push($datax, $maskapai);
                array_push($datax, $landing);
                array_push($datax, $transit);
                array_push($datax, $hotel);

            
            echo json_encode($datax);
            die;
        }
        else if($param1 == 'setPromoPaket'){
            $pwk_id = decrypt($this->input->post('pwk_id'));
            $data['promo'] = $this->input->post('val');
            $this->md_paketwaktu_kelas->updatePaketWaktuKelasById($pwk_id,$data);
            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'simpanKeberangkatan'){
            $task=$this->input->post('task');
            $pwk_id = decrypt($this->input->post('pwk_id'));
            $paketwaktu_id = decrypt($this->input->post('paketwaktu_id'));
            
            if($task=='update'){
                $diskon      = $this->input->post('diskon') ? remove_number_format($this->input->post('diskon')) : null;
                $harga       = remove_number_format($this->input->post('harga_idr'));
                $harga_usd   = remove_number_format($this->input->post('harga_usd'));
                $kurs_dollar = remove_number_format($this->input->post('kurs_dollar'));

                if($kurs_dollar && $harga_usd){
                    $data['harga_usd']   = $harga_usd;
                    $data['kurs_dollar'] = $kurs_dollar;
                }

                $data['kelas_id']   = decrypt($this->input->post('kelas_id'));
                $data['harga']      = $harga;
                $data['harga_awal'] = $harga;
                $data['diskon']     = $diskon;
                $this->md_paketwaktu_kelas->updatePaketWaktuKelasById($pwk_id,$data);

                $data2['bulan_berangkat'] = $this->input->post('bulan');
                $data2['tahun_berangkat'] = $this->input->post('tahun');
                $this->md_paket_waktu->updatePaketWaktuById($paketwaktu_id,$data2);
            } 
            else if($task=='add'){
                $data2['paket_id'] = decrypt($this->input->post('paket_id'));
                $data2['bulan_berangkat'] = $this->input->post('bulan');
                $data2['tahun_berangkat'] = $this->input->post('tahun');
                $data2['status'] = 1;

                //cek apakah keberangkatan sudah ada di tabel paketwaktu 
                $cek = $this->md_paket_waktu->getPaketWaktuByPaketIdByBulanByTahun($data2['paket_id'],$data2['bulan_berangkat'],$data2['tahun_berangkat']);
                if($cek)
                    $data['paketwaktu_id'] = $cek[0]->paketwaktu_id;
                else{
                    $this->md_paket_waktu->addPaketWaktu($data2);
                    $x = $this->md_paket_waktu->getLatestPaketWaktu();
                    $data['paketwaktu_id'] = $x[0]->paketwaktu_id;
                }

                $data['kelas_id'] = decrypt($this->input->post('kelas_id'));
                $data['harga'] = remove_rupiah_format($this->input->post('harga'));
                $data['harga_awal'] = remove_rupiah_format($this->input->post('harga'));
                $data['status'] = 1;
                $this->md_paketwaktu_kelas->addPaketWaktuKelas($data);

            }
            else if($task=='delete'){
                $data['status']=2;
                $this->md_paketwaktu_kelas->updatePaketWaktuKelasById($pwk_id,$data);

                $cek = count($this->md_paketwaktu_kelas->getPaketWaktuKelasByPaketWaktuId($paketwaktu_id));
                if($cek==0){
                    $this->md_paket_waktu->updatePaketWaktuById($paketwaktu_id,$data);
                }
            }


            echo json_encode($task.'_success');
            die;
        }        
        else if($param1 == 'form_add_paket'){
            $page_data['form_paket'] = TRUE;
            $page_data['caption'] = "Tambah Paket";
        }
        else if($param1 == 'form_add_haji'){
            $page_data['form_haji'] = TRUE;
            $page_data['caption'] = "Tambah Paket Haji";
        }
        
        $page_data['kota_keberangkatan'] = $this->md_kota->getAllKota();
        $page_data['maskapai_penerbangan'] = $this->md_maskapai->getAllMaskapai();
        $page_data['hotel'] = $this->md_hotel->getAllHotel();
        $page_data['bulan']=all_bulan();
        $year = date('Y');
        for($i=0;$i<50;$i++) $tahun[] = $year++;
        
        $page_data['tahun']=$tahun;
        $page_data['paket_akomodasi'] = $this->md_paket_akomodasi->getAllPaketAkomodasi();
        $page_data['paketwaktu_kelas'] = $this->md_paketwaktu_kelas->getAllPaketWaktuKelas();
        $page_data['paketwaktu'] = $this->md_paket_waktu->getAllPaketWaktu();
        $page_data['transit'] = $this->md_paket->getAllTransit();
        $page_data['landing'] = $this->md_paket->getAllLanding();

        //Paket Reguler. Cek apakah paket sudah dipesan, jika sudah, pembaharuan data terkait keberangkatan,kelas,harga tidak bisa dirubah
        $x = $this->md_paket->getAllPaketReguler();
        for($i=0;$i<count($x);$i++){
            $cek = $this->md_pendaftaran_detail->getPendaftaranDetailByPaketId($x[$i]->paket_id);
            if($cek)
                $x[$i]->pendaftar = count($cek);
            else
                $x[$i]->pendaftar = 0;
        }
        $page_data['paket'] = $x;

        $y = $this->md_paket->getAllPaketHaji();
        //Paket Haji. Cek apakah paket sudah dipesan, jika sudah, pembaharuan data terkait keberangkatan,kelas,harga tidak bisa dirubah
        for($i=0;$i<count($y);$i++){
            $cek = $this->md_pendaftaran_detail->getPendaftaranDetailByPaketIdV2($y[$i]->paket_id);
            if($cek)
                $y[$i]->pendaftar = count($cek);
            else
                $y[$i]->pendaftar = 0;
        }
        $page_data['paket_haji'] = $y;

        $page_data['jenis_paket'] = $this->md_jenis_paket->getAllJenisPaket();
        $page_data['kelas'] = $this->md_kelas->getAllKelas();
        $page_data['page_name'] = 'paket';
        $page_data['page_title'] = 'Paket';
        $this->load->view('login/index', $page_data);        
    }
//#------------------------------------------------------------------------------------------------#//  
    public function syarat_umroh($param1='',$param2=''){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        if($param1 == 'add'){
            if($this->input->post('jenis')=='all'){
                $jenis = array('Umroh','Haji');
                for($i=0;$i<count($jenis);$i++){
                    $data['persyaratan'] = $this->input->post('syarat');
                    $data['jenis'] = $jenis[$i];
                    $data['tipe'] = $this->input->post('tipe');
                    $data['status'] = 1;
                    $this->md_syarat_umroh->addSyaratUmroh($data);
                }
            } else {
                $data['persyaratan'] = $this->input->post('syarat');
                $data['jenis'] = $this->input->post('jenis');
                $data['tipe'] = $this->input->post('tipe');
                $data['status'] = 1;
                $this->md_syarat_umroh->addSyaratUmroh($data);
            }
            echo json_encode('add_success');
            die;
        }
        if($param1 == 'update'){
            $kelas_id = decrypt($param2);
            $data['persyaratan'] = $this->input->post('syarat');
            $data['tipe'] = $this->input->post('tipe');
            $this->md_syarat_umroh->updateSyaratUmroh($kelas_id,$data);
            echo json_encode('update_success');
            die;
        }
        if($param1 == 'delete'){
            $syaratumroh_id = decrypt($param2);
            $data['status'] = 2;
            $this->md_syarat_umroh->updateSyaratUmroh($syaratumroh_id,$data);
            // $this->md_syarat_status->removeSyaratStatusBySyaratUmrohId($syaratumroh_id);
            echo json_encode('update_success');
            die;
        }
        if($param1 == 'get_all_syarat'){
            $syarat = $this->md_syarat_umroh->getAllSyaratUmroh();
            foreach($syarat as $row){
                $row->syaratumroh_id = encrypt($row->syaratumroh_id);
            }
            echo json_encode($syarat);
            die;
        }
        if($param1 == 'get_all_syarat_haji'){
            $syarat = $this->md_syarat_umroh->getAllSyaratHaji();
            foreach($syarat as $row){
                $row->syaratumroh_id = encrypt($row->syaratumroh_id);
            }
            echo json_encode($syarat);
            die;
        }
        if($param1 == 'setDefaultSyarat'){
            $jenis = $this->input->post('jenis');
            if($jenis=='Umroh')
                $syarat = $this->md_syarat_umroh->getAllSyaratUmroh();

            if($jenis=='Haji')
                $syarat = $this->md_syarat_umroh->getAllSyaratHaji();

            foreach($syarat as $row){
                $id = encrypt($row->syaratumroh_id);
                $x = $this->input->post('syarat_'.$id);
                if($x == 'on')
                    $data['aktif'] = 1;
                else 
                    $data['aktif'] = 0;
                
                $this->md_syarat_umroh->updateSyaratUmroh($row->syaratumroh_id,$data);
            }
            echo json_encode("update_success");
            die;
        }

        //Hitung jumlah pendaftar reguler yang memiliki syarat 
        $x = $this->md_syarat_umroh->getAllSyaratUmroh();
        for($i=0;$i<count($x);$i++){
            $x[$i]->status_default = '-';
            $x[$i]->total = 0;
            $y = $this->md_syarat_status->getSyaratStatusBySyaratUmrohId($x[$i]->syaratumroh_id);
            if($y>0)
                $x[$i]->total = count($y);

            if($x[$i]->aktif)
                $x[$i]->status_default = '<button class="btn btn-xs green"><i class ="fa fa-check"></i>Default</button>';

        }
        $page_data['syarat'] = $x;

        //Hitung jumlah pendaftar haji yang memiliki syarat 
        $y = $this->md_syarat_umroh->getAllSyaratHaji();
        for($i=0;$i<count($y);$i++){
            $y[$i]->status_default = '-';
            $y[$i]->total = 0;
            $t = $this->md_syarat_status->getSyaratStatusBySyaratUmrohId($y[$i]->syaratumroh_id);
            if($t>0)
                $y[$i]->total = count($t);

            if($y[$i]->aktif)
                $y[$i]->status_default = '<button class="btn btn-xs blue"><i class ="fa fa-check"></i>Default</button>';

        }
        $page_data['syarat_haji'] = $y;
        $page_data['page_name'] = 'syarat_umroh';
        $page_data['page_title'] = 'Syarat Umroh';
        $this->load->view('login/index', $page_data);        
    }  
//#------------------------------------------------------------------------------------------------#//  
    public function testimoni($param1='',$param2=''){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        if($param1 == 'add'){
            $media_id = ImageUploadToMedia('media','testimoni_pic','avatar_testimoni');
            if($media_id != NULL)
                $data['media_id'] = $media_id;

            $data['nama_lengkap'] = $this->input->post('nama_lengkap');
            $data['asal_daerah'] = $this->input->post('asal_daerah');
            $data['testimoni'] = $this->input->post('testimoni');
            $data['jabatan'] = $this->input->post('jabatan');
            $data['tgl_post'] = date('Y-m-d',strtotime($this->input->post('tgl_post')));
            $data['status'] = 1;

            $this->md_testimoni->addTestimoni($data);
            $this->md_log->addLog(adminLog('Menambah Testimoni','Menambah testimoni dari '.$data['nama_lengkap']));
            echo json_encode('add_success');
            die;
        }
        else if($param1 == 'update'){
            $id = decrypt($this->input->post('testimoni_id'));

            $media_id = ImageUploadToMedia('media','testimoni_pic','avatar_testimoni');
            if($media_id != NULL){
                $data['media_id'] = $media_id;
                //Delete Old Media and Old Media Detail
                    $temp = $this->md_testimoni->getTestimoniById($id);
                    deleteMedia($temp[0]->media_id);
            }

            $data['nama_lengkap'] = $this->input->post('nama_lengkap');
            $data['asal_daerah'] = $this->input->post('asal_daerah');
            $data['testimoni'] = $this->input->post('testimoni');
            $data['jabatan'] = $this->input->post('jabatan');
            $data['tgl_post'] = date('Y-m-d',strtotime($this->input->post('tgl_post')));        
            $this->md_testimoni->updateTestimoni($id,$data);
            $this->md_log->addLog(adminLog('Memperbaharui Testimoni','Memperbaharui testimoni '.$data['nama_lengkap']));
            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'edit'){
            $id = decrypt($param2);
            $data['dt'] = $this->md_testimoni->getTestimoniById($id);
            foreach($data['dt'] as $row){
                $row->testimoni_id = encrypt($row->testimoni_id);
                $temp = $this->md_media_detail->getMediaDetailByMediaId($row->media_id);
                if($temp){
                    $row->media_id = base_url().$temp[0]->media_link;
                }
                $row->tgl_post = date('d-M-Y',strtotime($row->tgl_post));
                if(!$row->jabatan)
                    $row->jabatan = '-';
            }
            echo json_encode($data);
            die;
        }
        else if($param1 == 'delete'){
            $id = decrypt($param2);
            //Delete Image Kantor Cabang
                $temp = $this->md_testimoni->getTestimoniById($id);
                deleteMedia($temp[0]->media_id);
            $data['status'] = 2;
            $this->md_testimoni->updateTestimoni($id,$data);
            $this->md_log->addLog(adminLog('Menghapus Testimoni','Menghapus testimoni '.$temp[0]->nama_lengkap));
            echo json_encode('update_success');
            die;
        }

        $page_data['testimoni'] = $this->md_testimoni->getAllTestimoni();
        $page_data['page_name'] = 'testimoni';
        $page_data['page_title'] = 'Testimoni';
        $this->load->view('login/index', $page_data);        
    }  
//#------------------------------------------------------------------------------------------------#//  
    public function tour_leader($param1='',$param2=''){
        if(!$this->session->userdata('login_type') || $this->session->userdata('login_type')=='Perwakilan Daerah')
            redirect(base_url().'rwhgate');

        if($param1 == 'add'){
            $data['nama']          = $this->input->post('nama');
            $data['alamat']        = $this->input->post('alamat');
            $data['no_hp']         = $this->input->post('no_hp');
            $data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
            $data['status']        = 1;

            $this->md_tour_leader->addTourLeader($data);
            $this->md_log->addLog(adminLog('Menambah Tour Leader','Menambah  Tour Leader bernama '.$data['nama']));
            echo json_encode('add_success');
            die;
        }
        else if($param1 == 'update'){
            $id = decrypt($param2);
            $data['nama']    = $this->input->post('nama');
            $data['alamat']    = $this->input->post('alamat');
            $data['no_hp']         = $this->input->post('no_hp');
            $data['jenis_kelamin']         = $this->input->post('jenis_kelamin');
            $data['status']         = 1;
            $this->md_tour_leader->updateTourLeader($id,$data);
            $this->md_log->addLog(adminLog('Memperbaharui Tour Leader','Memperbaharui Data Tour Leader '.$data['nama']));            
            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'delete'){
            $id = decrypt($param2);
            $data['status'] = 2;
            $temp = $this->md_tour_leader->getTourLeaderById($id);
            $this->md_tour_leader->updateTourLeader($id,$data);
            $this->md_log->addLog(adminLog('Menghapus Tour Leader','Menghapus Data Tour Leader '.$temp[0]->nama));            
            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'pagination'){
            //List yang mucul hanya jemaah yang terverfikasi
                $dt = $this->md_tour_leader->getAllTourLeader();

            $no=0;
            $data =array();
            foreach($dt['data'] as $row){
                $id = encrypt($row->tourleader_id);

                //Aksi
                    $btn_edit = '<li><a href="javascript:;" onClick="edit_function(\'edit\','. $id.');"><i class="fa fa-pencil"></i>&nbsp;Edit</a></li>';
                    $btn_delete = '<li><a href="javascript:;" onClick="edit_function(\'delete\','. $id.')"><i class="fa fa-trash"></i>&nbsp;Hapus</a></li>';
                    $aksi = '<div class="btn-group">
                                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    '.$btn_edit.$btn_delete.'
                                </ul>
                                <input type="hidden" id="nama_'.$id.'" value="'.$row->nama.'">
                                <input type="hidden" id="jenis_kelamin_'.$id.'" value="'.$row->jenis_kelamin.'">
                                <input type="hidden" id="no_hp_'.$id.'" value="'.$row->no_hp.'">
                                <input type="hidden" id="alamat_'.$id.'" value="'.$row->alamat.'">
                            </div>';
                
                $th1 = ++$no;
                $th2 = $row->nama;
                $th3 = $row->jenis_kelamin;
                $th4 = $row->no_hp;
                $th5 = $row->alamat;
                $th6 = $aksi;
                $data[] = gathered_data(array($th1,$th2,$th3,$th4,$th5,$th6));
            }
            $dt['data'] = $data;
            echo json_encode($dt);
            die;
        }

        $page_data['page_name'] = 'tour_leader';
        $page_data['page_title'] = 'Tour Leader';
        $this->load->view('login/index', $page_data);        
    }



//#------------------------------------------------------------------------------------------------#//  
    public function halaman($param1='',$param2=''){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        $page_data['show_form'] = FALSE;
        $page_data['fill_form'] = FALSE;

        if($param1 == 'add'){
            $data['judul'] = $this->input->post('judul');
            $data['link_halaman'] = $this->input->post('link');
            $data['tipe'] = 'Tambahan';
            $data['author'] = 'Administrator';
            $data['isi'] = $this->input->post('isi');
            $data['meta_desc'] = $this->input->post('meta_desc');
            $data['meta_keyword'] = $this->input->post('meta_key');
            $data['tgl_post'] = date('Y-m-d H:i:s');
            $data['status'] = 1;
            $this->md_halaman->addHalaman($data);
            echo json_encode('add_success');
            die;
        } else if($param1 == 'update'){
            $halaman_id = decrypt($param2);
            $data['judul'] = $this->input->post('judul');
            $data['link_halaman'] = $this->input->post('link');
            $data['author'] = 'Administrator';
            $data['meta_desc'] = $this->input->post('meta_desc');
            $data['meta_keyword'] = $this->input->post('meta_key');
            $data['isi'] = $this->input->post('isi');
            $data['tgl_post'] = date('Y-m-d H:i:s');
            $data['status'] = 1;
            $this->md_halaman->updateHalaman($halaman_id,$data);

            echo json_encode('update_success');
            die;
        } else if($param1 == 'delete'){
            $halaman_id = decrypt($param2);
            $data['status'] = 2;
            $this->md_halaman->updateHalaman($halaman_id,$data);
            echo json_encode('update_success');
            die;
        } else if($param1 == 'show_add_form') {
            $page_data['show_form'] = TRUE;
        } else if($param1 == 'show_edit_form') {
            $page_data['show_form'] = TRUE;
            $page_data['fill_form'] = TRUE;
            $id = decrypt($param2);
            $page_data['dt_halaman'] = $this->md_halaman->getHalamanById($id);
        }

        $page_data['halaman'] = $this->md_halaman->getAllHalaman();
        $page_data['page_name'] = 'halaman';
        $page_data['page_title'] = 'Halaman';
        $this->load->view('login/index', $page_data);        
    }
//#------------------------------------------------------------------------------------------------#//  
    public function artikel($param1='',$param2=''){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');
        $page_data['show_form'] = FALSE;
        $page_data['fill_form'] = FALSE;

        if($param1=='add'){
            $media_id = ImageUploadToMedia('Thumbnail Artikel','thumb','media');
            $data['media_id'] = $media_id;
            $data['judul'] = $this->input->post('judul');
            $data['isi'] = $this->input->post('isi');
            $data['author'] = 'Administrator';
            $data['tgl_post'] = date("Y-m-d", strtotime($this->input->post('tgl_post')));
            $data['tgl_perubahan'] = date('Y-m-d H:i:s');
            $data['post_status'] = $this->input->post('post_status');
            $data['meta_desc'] = $this->input->post('meta_desc');
            $data['meta_keyword'] = $this->input->post('meta_key');
            $data['jenisartikel_id'] = 1;
            $data['status'] = 1;
            $this->md_artikel->addArtikel($data);
            echo json_encode('add_success');
            die;
        } 
        else if($param1=='update'){
            $id = decrypt($param2);
            $media_id = ImageUploadToMedia('Thumbnail Artikel','thumb','media');
            if($media_id != NULL)
                $data['media_id'] = $media_id;
            
            $data['judul'] = $this->input->post('judul');
            $data['isi'] = $this->input->post('isi');
            $data['author'] = $this->input->post('author');
            $data['tgl_post'] = date("Y-m-d", strtotime($this->input->post('tgl_post')));
            $data['tgl_perubahan'] = date('Y-m-d H:i:s');
            $data['post_status'] = $this->input->post('post_status');
            $data['meta_desc'] = $this->input->post('meta_desc');
            $data['meta_keyword'] = $this->input->post('meta_key');
            $this->md_artikel->updateArtikel($id,$data);
            // echo_array($data);
            echo json_encode('update_success');
            die;
        } 
        else if($param1=='delete'){
            $id = decrypt($param2);
            $temp = $this->md_artikel->getArtikelById($id);
            deleteMedia($temp[0]->media_id);

            $data['status'] = 2;
            $this->md_artikel->updateArtikel($id,$data);
            echo json_encode('update_success');
            die;

        } else if($param1 == 'show_add_form') {
            $page_data['show_form'] = TRUE;
        } else if($param1 == 'show_edit_form') {
            $page_data['show_form'] = TRUE;
            $page_data['fill_form'] = TRUE;
            $id = decrypt($param2);
            $page_data['dt_artikel'] = $this->md_artikel->getArtikelById($id);
        }

        $page_data['artikel'] = $this->md_artikel->getAllArtikel();
        $page_data['page_name'] = 'artikel';
        $page_data['page_title'] = 'Artikel';
        $this->load->view('login/index', $page_data);        
    }
//#------------------------------------------------------------------------------------------------#//  
    public function slideshow($param1='',$param2=''){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        if($param1 == 'add'){
            $dt['judul'] = $this->input->post('judul');
            $dt['link_slide'] = $this->input->post('link_slide');
            $dt['tgl_post'] =  date('Y-m-d',strtotime($this->input->post('tgl_post')));
            $dt['keterangan'] =  $this->input->post('keterangan');
            $dt['author'] =  $this->input->post('author');

            $result = ImageUploadToSlide($dt,'slideshow','add');
            $this->md_log->addLog(adminLog('Menambah Slideshow','Menambah Slideshow '.$dt['judul']));
            echo json_encode($result);
            die;
        }
        else if($param1 == 'update'){
            $dt['slide_id'] = decrypt($param2); 
            $dt['judul'] = $this->input->post('judul');
            $dt['link_slide'] = $this->input->post('link_slide');
            $dt['tgl_post'] =  date('Y-m-d',strtotime($this->input->post('tgl_post')));
            $dt['keterangan'] =  $this->input->post('keterangan');
            $dt['author'] =  $this->input->post('author');
            $result = ImageUploadToSlide($dt,'slideshow','update');
            $this->md_log->addLog(adminLog('Memperbaharui Slideshow','Memperbaharui Slideshow '.$dt['judul']));
            echo json_encode($result);
            die;
        }
        else if($param1 == 'delete'){
            $slide_id = decrypt($param2);
            $link = $this->md_slide->getSlideBySlideId($slide_id);
            unlink($link[0]->url_slide);
            unlink($link[0]->url_thumbnail);
            $data['status'] = 2;
            $this->md_slide->updateSlide($slide_id,$data);
            $this->md_log->addLog(adminLog('Menghapus Slideshow','Menghapus Slideshow '.$link[0]->judul));
            echo json_encode('update_success');
            die;
        }

        $page_data['slide'] = $this->md_slide->getAllSlide();
        $page_data['page_name'] = 'slideshow';
        $page_data['page_title'] = 'Slideshow';
        $this->load->view('login/index', $page_data);        
    }
//#------------------------------------------------------------------------------------------------#//  
    public function galeri($param1='',$param2=''){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        $page_data['lihat'] = FALSE;
        $page_data['galerialbum_id'] = FALSE;
        if($param1 == 'add_album'){

            $media_id = ImageUploadToMedia('Album','album','album',$this->input->post('alt_teks'));

            $data2['nama_album'] = $this->input->post('nama_album');
            $data2['author'] = 'Administrator';
            $data2['alt_teks'] = $this->input->post('alt_teks');
            $data2['tgl_post'] = date('Y-m-d',strtotime($this->input->post('tgl_post')));
            $data2['keterangan'] = $this->input->post('keterangan');
            $data2['media_id'] =$media_id;
            $data2['status'] = 1;
            $this->md_galeri_album->addGaleriAlbum($data2);

 
            $data3['alt_teks'] = $this->input->post('alt_teks');
            $this->md_media->updateMedia($media_id,$data3);

            $this->md_log->addLog(adminLog('Menambah Album','Menambah Album berjudul '.$data2['nama_album']));
            echo json_encode('add_success');
            die;
        }
        else if($param1 == 'add_photo'){

            $media_id = ImageUploadToMedia('Album_Item','photo','galeri');
            $data2['galerialbum_id'] = decrypt($this->input->post('galerialbum_id'));
            $data2['author'] = 'Administrator';
            $data2['tgl_post'] = date('Y-m-d H:i:s');
            $data2['media_id'] =$media_id;
            $data2['status'] = 1;
            $this->md_galeri_item->addGaleriItem($data2);
            $x = $this->md_galeri_album->getGaleriAlbumById($data2['galerialbum_id']);
            $this->md_log->addLog(adminLog('Menambah Photo','Menambah photo baru ke album '.$x[0]->nama_album));
            echo json_encode('add_success');
            die;
        }
        else if($param1 == 'update_album'){
            $id = decrypt($this->input->post('galerialbum_id'));
            $media_id = ImageUploadToMedia('Album','album','album',$this->input->post('alt_teks'));
            if($media_id != NULL)
                $data['media_id'] = $media_id;
            
            $data['nama_album'] = $this->input->post('nama_album');
            $data['tgl_post'] = date('Y-m-d',strtotime($this->input->post('tgl_post')));
            $data['keterangan'] = $this->input->post('keterangan');
            $this->md_galeri_album->updateGaleriAlbum($id,$data);
            
            $id2=decrypt($this->input->post('media_id'));
            $data2['alt_teks'] = $this->input->post('alt_teks');
            $data2['tgl_perubahan'] = date("Y-m-d H:i:s");
            $this->md_media->updateMedia($id2,$data2);

            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'delete_album'){
            $id = decrypt($param2);
            $x = $this->md_galeri_album->getGaleriAlbumById($id);
            $this->md_log->addLog(adminLog('Menghapus Album','Menghapus album '.$x[0]->nama_album));
            $media_id_album = $x[0]->media_id;

            //Hapus Album Item dulu
                $temp = $this->md_galeri_item->getGaleriItemByGaleriAlbumId($id);
                foreach($temp as $row){
                    deleteMedia($row->media_id);
                }
            //Baru Hapus Albumnya
                deleteMedia($x[0]->media_id);

            $data['status'] = 2;
            $this->md_galeri_album->updateGaleriAlbum($id,$data);
            $this->md_galeri_item->updateGaleriItemByGaleriAlbumId($id,$data);
            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'delete_item'){

            $media_id = decrypt($this->input->post('media_id'));
            deleteMedia($media_id);

            $galeriitem_id = decrypt($param2);

            $x = $this->md_galeri_item->getGaleriItemById($galeriitem_id);
            $temp = $this->md_galeri_album->getGaleriAlbumById($x[0]->galerialbum_id);
            $this->md_log->addLog(adminLog('Menghapus Photo','Menghapus photo pada album '.$temp[0]->nama_album));

            $data['status'] = 2;
            $this->md_galeri_item->updateGaleriItem($galeriitem_id,$data);

            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'lihat_album'){
            $page_data['lihat']=TRUE;
            $page_data['galerialbum_id'] = $param2;
            $album = $this->md_galeri_album->getGaleriAlbumById(decrypt($param2));
            $page_data['nama_album'] = $album[0]->nama_album;
            $page_data['item'] = $this->md_galeri_item->getGaleriItemByGaleriAlbumId(decrypt($param2));
        }

        $page_data['album'] = $this->md_galeri_album->getAllGaleriAlbum();
        $page_data['page_name'] = 'galeri';
        $page_data['page_title'] = 'Galeri';
        $this->load->view('login/index', $page_data);        
    }
//#------------------------------------------------------------------------------------------------#//  
    public function media($param1='',$param2=''){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        if($param1=='add'){
            $config['upload_path'] = './assets/file/';
            $config['allowed_types'] = 'jpg|png|jpeg|JPG|PNG|JPEG|docx|doc|txt|xls|xlsx|rar|zip|pdf|PDF|DOCX|DOC|TXT|XLS|XLSX|RAR|ZIP';
            $config['max_size']       = '2000';
            ini_set('memory_limit', '-1');
            $this->load->library('upload', $config);

            if($this->upload->do_upload('media')){
                $dt = $this->upload->data();
                //Jika judul tidak diisi, nama file akan digunakan
                $judul = $this->input->post('judul');
                if($judul)
                    $judul = $judul.$dt['file_ext'];
                else
                    $judul = $dt['file_name'];

                if ($dt['file_ext'] == '.gif' OR $dt['file_ext'] == '.GIF' OR 
                    $dt['file_ext'] == '.JPG' OR $dt['file_ext'] == '.jpg' OR 
                    $dt['file_ext'] == '.png' OR $dt['file_ext'] == '.PNG' OR 
                    $dt['file_ext'] == '.jpeg' OR $dt['file_ext'] == '.JPEG') {
                        //Add media First
                            $data = array(
                                'judul' => $judul,
                                'tipe' => 'gambar',
                                'tgl_upload' => date("Y-m-d H:i:s"),
                                'tgl_perubahan' => date("Y-m-d H:i:s"),
                                'jenismedia_id' => '1',
                                'status' => '1'
                            );
                            $this->md_media->addMedia($data);
                            
                        //Add Media detail and resized file
                            $x = $this->md_media->getLatestMedia();
                            $data = $this->upload->data();
                            $source_image = './assets/file/' .  $dt['file_name'];
                            $new_image = array(
                                                'assets/file/large/large_'. $judul,
                                                'assets/file/medium/medium_' .  $judul,
                                                'assets/file/small/small_' .  $judul);
                            $new_width = array(1024,650,128);
                            $ukuran=array('large','medium','small');
                            for($i=0;$i<3;$i++){
                                //resize for large,medium,small
                                    $config = array(
                                        'source_image' => $source_image,
                                        'new_image' => $new_image[$i],
                                        'maintain_ratio' => true,
                                        'width' => $new_width[$i]
                                    );
                                    $this->image_lib->initialize($config);
                                    $this->image_lib->resize();

                                //Add large,medium,small media_detail
                                    $data = array(
                                        'media_id' => $x[0]->media_id,
                                        'jenis_ukuran' => $ukuran[$i],
                                        'media_link' => $config['new_image'],
                                        'width' => $config['width'],
                                        'status' => '1'
                                    );
                                    $this->md_media_detail->addMediaDetail($data);
                            }
                            unlink($source_image);
                        
                        echo json_encode('add_success');
                        die;
                }
                else {
                    //Add media First
                    $data = array(
                        'judul' =>$judul,
                        'tipe' => 'file',
                        'tgl_upload' => date("Y-m-d H:i:s"),
                        'tgl_perubahan' => date("Y-m-d H:i:s"),
                        'jenismedia_id' => '1',
                        'status' => '1'
                    );
                    $this->md_media->addMedia($data);

                //Add Media detail and resized file
                    $x = $this->md_media->getLatestMedia();
                    $data = $this->upload->data();
                    $data2 = array(
                        'media_id' => $x[0]->media_id,
                        'media_link' => 'assets/file/'.$data['file_name'],
                        'status' => '1'
                    );
                    $this->md_media_detail->addMediaDetail($data2);
                    echo json_encode('add_success');
                    die;
                }

                /*
                
                */
            } else {
                $pesan = "add_fail";
                echo json_encode($pesan);
                die;
            }
        }
        else if($param1 == 'delete'){
            $media_id = decrypt($param2);
            deleteMedia($media_id);
            echo json_encode('update_success');
            die;
        }
        $page_data['media'] = $this->md_media->getAllMediaByMedia();
        $page_data['page_name'] = 'media';
        $page_data['page_title'] = 'Media';
        $this->load->view('login/index', $page_data);        
    }
//#------------------------------------------------------------------------------------------------#//  
    public function pendaftaran($param1='',$param2='',$param3=''){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        $login_type = $this->session->userdata('login_type');
        $page_data['show_detail'] = FALSE;
        $page_data['show_form'] = FALSE;

        if($param1 == 'add'){
            $this->db->trans_start();
            $temp                 = explode('_',$this->input->post('pwk_id')[0]);
            $paketwaktu_id        = $temp[0];
            $pwk_id               = $temp[1];
            $paket_id             = $this->input->post('paket_id');
            $kurs_dollar_hari_ini = $this->input->post('kurs_dollar_hari_ini');

            //Cek Daftar dengan Member Baru
                $insert_member = FALSE;
                if($this->input->post('member_id')){
                    $member_id         = $this->input->post('member_id');
                    $data['member_id'] = decrypt($member_id);
                } 
                //Pendaftar menggunakan data baru
                else if($this->input->post('nama_member')){
                    $dataMember['nama_lengkap'] = $this->input->post('nama_member');
                    $dataMember['tgl_lahir']    = dateConvertToDatabase($this->input->post('tgl_lahir_member'));
                    $dataMember['alamat']       = $this->input->post('alamat_member');
                    $dataMember['email']        = $this->input->post('email_member');
                    $dataMember['no_hp']        = $this->input->post('no_hp_member');
                    $dataMember['tgl_regis']    = date('Y-m-d H:i:s');
                    $dataMember['status']       = 1;
                    $this->md_member->addMember_transact($dataMember);
                    $data['member_id'] = $this->db->insert_id();
                } 
                //Pendaftar menggunakan data jemaah nomor 1 sebagai pendaftar
                else {
                    $dataMember['nama_lengkap'] = $this->input->post('nama_lengkap')[0];
                    $dataMember['tgl_lahir']    = dateConvertToDatabase($this->input->post('tgl_lahir')[0]);
                    $dataMember['alamat']       = $this->input->post('alamat')[0];
                    $dataMember['email']        = $this->input->post('email')[0];
                    $dataMember['no_hp']        = $this->input->post('no_hp')[0];
                    $dataMember['tgl_regis']    = date('Y-m-d H:i:s');
                    $dataMember['status']       = 1;
                    $this->md_member->addMember_transact($dataMember);
                    $data['member_id'] = $this->db->insert_id();
                }

            $data['tgl_daftar']        = date('Y-m-d H:i:s');
            $data['jenis_pendaftaran'] = 'Web';
            $data['status_aktif']      = 'Aktif';
            $data['pengguna_id']       = $this->session->userdata('pengguna_id');
            $data['status']            = 1;
            $this->md_pendaftaran->addPendaftaran_transact($data);
            $pendaftaran_id = $this->db->insert_id();

            if($pendaftaran_id){
                $data2['status_verifikasi'] = 'Valid';
                $data2['pengguna_id'] = $this->session->userdata('pengguna_id');

                //Booking Paket reguler
                    if($this->input->post('pwk_id')){
                        $pwk_id         = decrypt($pwk_id);
                        $x              = $this->md_paketwaktu_kelas->getPaketWaktuKelasByIdV2($pwk_id);
                        $syarat_default = $this->md_syarat_umroh->getAllSyaratUmrohDefault();
                        
                        $data2['harus_bayar']          = $x[0]->harga;
                        $data2['harus_bayar_usd']      = $x[0]->harga_usd;
                        $data2['kurs_dollar']          = $x[0]->kurs_dollar;
                        $data2['pwk_id']               = $pwk_id;
                        $data2['tgl_submit']           = date('Y-m-d H:i:s');
                        $data2['status_keberangkatan'] = 'Dalam Proses';
                        $data2['kota_keberangkatan']   = $x[0]->berangkat_awal;
                        $data2['status']               = 1;
                        //Infant mendapat potongan 75% dari harga paket
                        $biaya_infant                  = $x[0]->harga*0.75;

                    }

                //Booking Paket haji
                    else if($paket_id){
                        $paket_id       = decrypt($paket_id[0]);
                        $x              = $this->md_paket->getPaketByPaketId($paket_id);
                        $syarat_default = $this->md_syarat_umroh->getAllSyaratHajiDefault();
                        $data2['harus_bayar']         = $x[0]->harga_terakhir;
                        $data2['harus_bayar_usd']     = $x[0]->harga_terakhir_usd;
                        $data2['kurs_dollar']         = $x[0]->harga_terakhir_kurs;
                        $data2['paket_id']            = $paket_id;
                        $data2['tahun_est_berangkat'] = date('Y')+$x[0]->masa_tunggu;
                        $data2['status']              = 1;
                        $data2['tgl_submit']          = date('Y-m-d H:i:s');
                    }

                //Data Jemaah
                    $nama_lengkap        = $this->input->post('nama_lengkap');
                    $no_ktp              = $this->input->post('no_ktp');
                    $jenis_kelamin       = $this->input->post('jenis_kelamin');
                    $tempat_lahir        = $this->input->post('tempat_lahir');
                    $tgl_lahir           = $this->input->post('tgl_lahir');
                    $alamat              = $this->input->post('alamat');
                    $no_hp               = $this->input->post('no_hp');
                    $no_hp_2             = $this->input->post('no_hp_2');
                    $email               = $this->input->post('email');
                    $mahram              = $this->input->post('mahram');                    
                    $progresive          = $this->input->post('progresive');
                    $infant              = $this->input->post('infant');
                    $biaya_sharing_bed   = $this->input->post('biaya_sharing_bed');
                    $kelas_pesawat       = $this->input->post('kelas_pesawat');
                    $biaya_kelas_pesawat = $this->input->post('biaya_kelas_pesawat');
                    $riwayat_penyakit    = $this->input->post('riwayat_penyakit');
                    $jenis_jemaah        = $this->input->post('jenis_jemaah');
                    $catatan             = $this->input->post('catatan');

                    for($i=0;$i<count($nama_lengkap);$i++){

                        //Cek Sequence No Registrasi Jemaah
                            $x                      = getLatestSequence('Registrasi Jemaah');
                            $data2['seq']           =  $x['new_seq'];
                            $data2['no_registrasi'] =  $this->session->userdata('kode_kota_pengguna').date('Y').date('m').$x['new_seq'];

                        $data2['pendaftaran_id']      = $pendaftaran_id;
                        $data2['nama_jemaah']         = $nama_lengkap[$i];
                        $data2['no_ktp']              = $no_ktp[$i];
                        $data2['tempat_lahir']        = $tempat_lahir[$i];
                        $data2['tgl_lahir']           = dateConvertToDatabase($tgl_lahir[$i]);
                        $data2['alamat']              = $alamat[$i];
                        $data2['no_hp']               = $no_hp[$i];
                        $data2['no_hp_2']             = $no_hp_2[$i];
                        $data2['email']               = $email[$i];
                        $data2['ada_mahram']          = $mahram[$i];
                        $data2['progressive']         = $progresive[$i];
                        $data2['infant']              = $infant[$i]=='Ya' ? $biaya_infant : 0;        
                        $data2['sharing_bed']         = $biaya_sharing_bed[$i] ? $biaya_sharing_bed[$i] : 0; 
                        $data2['kelas_pesawat']       = $kelas_pesawat[$i];
                        $data2['biaya_kelas_pesawat'] = $biaya_kelas_pesawat[$i] ? $biaya_kelas_pesawat[$i] : 0;
                        $data2['riwayat_penyakit']    = $riwayat_penyakit[$i];
                        $data2['catatan']             = $catatan[$i];
                        $data2['jenis_kelamin']       = $jenis_kelamin[$i];
                        $data2['jenis_jemaah']        = $jenis_jemaah[$i];
                        $data2['status_verifikasi']   = 'Valid';
                        $data2['keberangkatan_id']    = $this->input->post('tgl_keberangkatan_'.$paketwaktu_id) ? decrypt($this->input->post('tgl_keberangkatan_'.$paketwaktu_id)) : null;
                        $this->md_pendaftaran_detail->addPendaftaranDetail_transact($data2);
                        $pendaftarandetail_id = $this->db->insert_id();
                        
                        if($pendaftarandetail_id){
                            //Syarat default Jemaah
                                foreach($syarat_default as $row){
                                    $data3['syaratumroh_id']       = $row->syaratumroh_id;
                                    $data3['pendaftarandetail_id'] = $pendaftarandetail_id;
                                    $data3['status_syarat']        = 'Belum Lengkap';
                                    $data3['status']               = 1;
                                    $this->md_syarat_status->addSyaratStatus_transact($data3);
                                }

                            //Biaya Tambahan
                                $nama_tambahan                 = $this->input->post('nama_biaya_tambahan_'.($i+1));
                                $biaya_tambahan                = $this->input->post('biaya_tambahan_'.($i+1));
                                $biaya_tambahan_gunakan_dollar = $this->input->post('biaya_tambahan_kurs_dollar_'.($i+1));
                                for($j=0;$j<count($nama_tambahan);$j++){

                                    if($biaya_tambahan_gunakan_dollar){
                                        $data4['kurs_dollar'] = $kurs_dollar_hari_ini;
                                        $data4['jumlah_usd']  = $biaya_tambahan[$j];
                                        $data4['jumlah']      = $data4['jumlah_usd'] * $data4['kurs_dollar'];
                                    } else {
                                        $data4['jumlah']      = $biaya_tambahan[$j];
                                    }

                                    $data4['pendaftarandetail_id'] = $pendaftarandetail_id;
                                    $data4['jenis']                = 'Tambahan';
                                    $data4['biaya']                = $nama_tambahan[$j];
                                    $data4['status']               = 1;
                                    $this->md_pendaftaran_detail_biaya->addPendaftaranDetailBiaya_transact($data4);
                                }

                            //Biaya Pengurang
                                $nama_pengurang                 = $this->input->post('nama_biaya_pengurang_'.($i+1));
                                $biaya_pengurang                = $this->input->post('biaya_pengurang_'.($i+1));
                                $biaya_pengurang_gunakan_dollar = $this->input->post('pengurang_kurs_dollar_'.($i+1));
                                for($j=0;$j<count($nama_pengurang);$j++){
                                    
                                    if($biaya_pengurang_gunakan_dollar){
                                        $data5['kurs_dollar'] = $kurs_dollar_hari_ini;
                                        $data5['jumlah_usd']  = $biaya_pengurang[$j];
                                        $data5['jumlah']      = $data5['jumlah_usd'] * $data5['kurs_dollar'];
                                    } else {
                                        $data5['jumlah']      = $biaya_pengurang[$j];
                                    }

                                    $data5['pendaftarandetail_id'] = $pendaftarandetail_id;
                                    $data5['jenis']                = 'Pengurang';
                                    $data5['biaya']                = $nama_pengurang[$j];
                                    $data5['status']               = 1;
                                    $this->md_pendaftaran_detail_biaya->addPendaftaranDetailBiaya_transact($data5);
                                }
                        }

                        //Log
                            $this->md_log->addLog_transact(adminLog('Menambah Jemaah','Menambah jemaah baru bernama '.$data2['nama_jemaah']));
                    }

                if($this->db->trans_status() === FALSE){
                    // $temp['error'] = $error;
                    // $this->session->set_flashdata('err_add',$error);
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                    $this->session->set_flashdata('add_success',$data2['nama_jemaah']);
                    if($data2['keberangkatan_id']){
                        $this->session->set_flashdata('redirect_room',encrypt($data2['keberangkatan_id']).'/'.encrypt($data2['kota_keberangkatan']));
                    }
                    redirect('administrator/pendaftaran/daftar');
                }

            }
        }
        else if($param1 == 'edit'){
            $id = decrypt($param2);
            $list_bulan = all_bulan();
            $x = $this->md_pendaftaran_detail->getPendaftaranDetailByPendaftarandetailIdV2($id);

            //Booking Paket Haji
                if($x[0]->nama_paket_haji){
                    foreach($x as $row){
                        $row->member_id = $row->member_id ? encrypt($row->member_id) : NULL;
                        $row->paket_id             = encrypt($x[0]->paket_id);
                        $row->pendaftarandetail_id = encrypt($row->pendaftarandetail_id);
                        $row->pendaftaran_id       = encrypt($row->pendaftaran_id);
                        $row->nama_paket           = $x[0]->nama_paket_haji;
                        $row->tipe_pendaftaran     = $row->tipe_pendaftar;
                        $row->tgl_lahir            = date('d/m/y',strtotime($row->tgl_lahir));
                        $row->harga                = rupiah_format($row->harus_bayar);
                        $row->jenis                = 'Haji';
                        $row->jenis_quota          = $x[0]->jenis_quota;
                        $row->list_biaya_tambahan = $this->md_pendaftaran_detail_biaya->getPendaftaranDetailBiayaByPendaftaranDetailIdByJenis($id,'Tambahan');
                        $row->list_biaya_pengurang = $this->md_pendaftaran_detail_biaya->getPendaftaranDetailBiayaByPendaftaranDetailIdByJenis($id,'Pengurang');
                    }                
                } 
            
            //Booking Paket Reguler
                else {
                    foreach($x as $row){
                        $row->member_id = $row->member_id ? encrypt($row->member_id) : NULL;
                        $row->pendaftarandetail_id  = encrypt($row->pendaftarandetail_id);
                        $row->pendaftaran_id        = encrypt($row->pendaftaran_id);
                        $row->pwk_id                = encrypt($row->pwk_id);
                        $row->id_kota_keberangkatan = encrypt($row->id_kota_keberangkatan);
                        $row->bulan_berangkat       = $list_bulan[$row->bulan_berangkat];
                        $row->harga                 = rupiah_format($row->harga_reguler);
                        $row->tgl_lahir             = date('d/m/Y',strtotime($row->tgl_lahir));
                        $row->jenis                 = 'Reguler';
                        $row->kota_keberangkatan    = $row->kota_keberangkatan;
                        $row->berangkat_awal        = '('.$row->kode_kota_paket.') '.$row->kota_paket;

                        if($row->kurs_dollar_reguler){
                            $row->harga_usd   = '$'.number_format($row->harga_usd_reguler);
                            $row->kurs_dollar = number_format($row->kurs_dollar_reguler);
                        }

                        $row->list_biaya_tambahan = $this->md_pendaftaran_detail_biaya->getPendaftaranDetailBiayaByPendaftaranDetailIdByJenis($id,'Tambahan');
                        $row->list_biaya_pengurang = $this->md_pendaftaran_detail_biaya->getPendaftaranDetailBiayaByPendaftaranDetailIdByJenis($id,'Pengurang');
                    }
                }


            echo json_encode($x);
            die;
        }
        else if($param1 == 'update'){

     
            $pendaftarandetail_id = decrypt($this->input->post('pendaftarandetail_id'));
            $pendaftaran_id       = decrypt($this->input->post('pendaftaran_id'));

            //Cek Member
                $member_id = $this->input->post('member_id');
                $data['member_id'] = $member_id ? decrypt($member_id) : NULL;
                $data['status'] = 1;
                $this->md_pendaftaran->updatePendaftaran($pendaftaran_id,$data);
        
            //Data Jemaah
                $data2['pendaftaran_id']     = $pendaftaran_id;
                $data2['nama_jemaah']        = $this->input->post('nama_lengkap');
                $data2['no_ktp']             = $this->input->post('no_ktp');
                $data2['tempat_lahir']       = $this->input->post('tempat_lahir');
                $data2['tgl_lahir']          = dateConvertToDatabase($this->input->post('tgl_lahir'));
                $data2['jenis_kelamin']      = $this->input->post('jenis_kelamin');
                $data2['jenis_jemaah']       = $this->input->post('jenis_jemaah');
                $data2['email']              = $this->input->post('email');
                $data2['no_hp']              = $this->input->post('no_hp');
                $data2['no_hp_2']            = $this->input->post('no_hp_2');
                $data2['alamat']             = $this->input->post('alamat');
                $data2['riwayat_penyakit']   = $this->input->post('riwayat_penyakit');
                $data2['catatan']            = $this->input->post('catatan');
                $data2['kota_keberangkatan'] = decrypt($this->input->post('kota_keberangkatan'));
                $harus_bayar                 = remove_rupiah_format($this->input->post('harus_bayar'));

            //Paket yang dipilih
                //pwk_id dikirim bersamaan dengan paketwaktu_id sehingga harus di explode untuk memisahkan mereka. Jika mengganti paket, maka $temp akan berisi 2 array
                $temp          = explode('_',$this->input->post('pwk_id')[0]);
                if(count($temp)==2){
                    $paketwaktu_id = $temp[0];
                    $pwk_id        = $temp[1];
                    $paket_id      = $this->input->post('paket_id');

                    //Booking Paket Reguler
                        if($this->input->post('pwk_id')){
                            $pwk_id = decrypt($pwk_id);
                            $x = $this->md_paketwaktu_kelas->getPaketWaktuKelasByIdV2($pwk_id);
                            if($x[0]->kurs_dollar){
                                $data2['harus_bayar']     = $x[0]->harga;
                                $data2['harus_bayar_usd'] = $x[0]->harga_usd;
                                $data2['kurs_dollar']     = $x[0]->kurs_dollar;

                            } else {
                                $data2['harus_bayar']     = $x[0]->harga;
                                $data2['harus_bayar_usd'] = null;
                                $data2['kurs_dollar']     = null;
                            }
                            $data2['pwk_id']              = $pwk_id;
                            $data2['paket_id']            = NULL;
                            $data2['tahun_est_berangkat'] = NULL;
                            $data2['keberangkatan_id']    = $this->input->post('tgl_keberangkatan_'.$paketwaktu_id) ? decrypt($this->input->post('tgl_keberangkatan_'.$paketwaktu_id)) : null;
                            $harus_bayar                  = $data2['harus_bayar'];
                        }

                    //Booking Paket Haji
                        else if($paket_id){
                            $paket_id                     = decrypt($paket_id[0]);
                            $x                            = $this->md_paket->getPaketByPaketId($paket_id);
                            $data2['pwk_id']              = NULL;
                            $data2['harus_bayar']         = $x[0]->harga_terakhir;
                            $data2['paket_id']            = $paket_id;
                            $data2['tahun_est_berangkat'] = date('Y')+$x[0]->masa_tunggu;
                        }
                }

                //Atribut Jemaah
                    $data2['ada_mahram']          = $this->input->post('mahram');
                    $data2['progressive']         = $this->input->post('progressive');

                    //Infant berarti dapat diskon 75%
                    $data2['infant']              = $this->input->post('infant')=='Ya' ? $harus_bayar*0.75 : 0;        

                    $data2['sharing_bed']         = $this->input->post('biaya_sharing_bed') > 0 ? remove_number_format($this->input->post('biaya_sharing_bed')) : 0; 
                    $data2['kelas_pesawat']       = $this->input->post('kelas_pesawat');
                    $data2['biaya_kelas_pesawat'] = $this->input->post('biaya_kelas_pesawat') ? remove_number_format($this->input->post('biaya_kelas_pesawat')) : 0;


                $kurs_dollar_hari_ini = $this->input->post('kurs_dollar_hari_ini_modal');
                $this->md_pendaftaran_detail_biaya->removePendaftaranDetailBiayaByJemaah($pendaftarandetail_id);
                //Biaya Tambahan dan Pengurang
                    $nama_biaya_tambahan           = $this->input->post('nama_biaya_tambahan_0');
                    $biaya_tambahan                = $this->input->post('biaya_tambahan_0');
                    $biaya_tambahan_gunakan_dollar = $this->input->post('biaya_tambahan_kurs_dollar_0');
                    $kurs_dollar_biaya_tambahan    = $this->input->post('kurs_dollar_biaya_tambahan_0');
                    for($i=0;$i<count($biaya_tambahan);$i++){

                        if(!$kurs_dollar_hari_ini)
                            $kurs_dollar_hari_ini = $kurs_dollar_biaya_tambahan[$i];

                        if($biaya_tambahan_gunakan_dollar[$i]=='on'){
                            $bt['kurs_dollar'] = $kurs_dollar_hari_ini;
                            $bt['jumlah_usd']  = $biaya_tambahan[$i];
                            $bt['jumlah']      = $bt['jumlah_usd']*$bt['kurs_dollar'];
                        } else {
                            $bt['jumlah']      = $biaya_tambahan[$i];
                            $bt['jumlah_usd']  = 0;
                            $bt['kurs_dollar'] = 0;
                        }

                        $bt['pendaftarandetail_id'] = $pendaftarandetail_id;
                        $bt['biaya']                = $nama_biaya_tambahan[$i];
                        $bt['jenis']                = 'Tambahan';
                        $bt['status']               = 1;
                        $this->md_pendaftaran_detail_biaya->addPendaftaranDetailBiaya($bt);
                    }

                    $nama_biaya_pengurang           = $this->input->post('nama_biaya_pengurang_0');
                    $biaya_pengurang                = $this->input->post('biaya_pengurang_0');
                    $biaya_pengurang_gunakan_dollar = $this->input->post('pengurang_kurs_dollar_0');
                    $kurs_dollar_pengurang          = $this->input->post('kurs_dollar_pengurang_0');
                    for($i=0;$i<count($biaya_pengurang);$i++){

                        if(!$kurs_dollar_hari_ini)
                            $kurs_dollar_hari_ini = $kurs_dollar_pengurang[$i];

                        if($biaya_pengurang_gunakan_dollar[$i]=='on'){
                            $bp['kurs_dollar'] = $kurs_dollar_hari_ini;
                            $bp['jumlah_usd']  = $biaya_pengurang[$i];
                            $bp['jumlah']      = $bp['jumlah_usd']*$bp['kurs_dollar'];
                        } else {
                            $bp['jumlah']      = $biaya_pengurang[$i];
                            $bp['jumlah_usd']  = 0;
                            $bp['kurs_dollar'] = 0;
                        }

                        $bp['pendaftarandetail_id'] = $pendaftarandetail_id;
                        $bp['biaya']                = $nama_biaya_pengurang[$i];
                        $bp['jenis']                = 'Pengurang';
                        $bp['status']               = 1;
                        $this->md_pendaftaran_detail_biaya->addPendaftaranDetailBiaya($bp);
                    }

            $this->md_pendaftaran_detail->updatePendaftaranDetail($pendaftarandetail_id,$data2);
            
            //Log 
                $this->md_log->addLog(adminLog('Memperbaharui Jemaah','Memperbaharui data jemaah '.$data2['nama_jemaah']));
                echo json_encode('update_success');

            die;
        }        
        else if($param1 == 'detail_jemaah'){
            $id=decrypt($param2);
            $page_data['show_detail'] = TRUE;

            //Data Paket
                $x = $this->md_pendaftaran_detail->getPendaftaranDetailByPendaftarandetailIdV2($id);
                $x = getMember($x);
                $page_data['dt_jemaah'] = $x;

            //Data Syarat Jemaah
                $page_data['dt_syarat'] = $this->md_syarat_status->getSyaratStatusByPendaftarandetailIdV2($id);

            //Data Perlengkapan Jemaah
                $page_data['dt_perlengkapan'] = $this->md_perlengkapan->getPerlengkapanByPendaftarandetailId($id);

            //Data Pembayaran Jemaah
                $x = $this->md_pembayaran->getPembayaranByJemaah($id);
                foreach($x as $row){
                    if($row->status_verifikasi){
                        $row->kantor_pusat_nama = $row->nama_lengkap;
                        $row->kantor_pusat_tgl_verif = date('d-M-Y H:i',strtotime($row->tgl_update));
                    }
                }
                $page_data['dt_pembayaran'] = $x;
        }
        else if($param1 == 'getPaketByProvinsi'){
            $id=decrypt($param2);
            $x = $this->md_paket->getPaketByProvinsi($id);
            echo json_encode($x);
            die;
        }
        else if($param1 == 'delete'){
            $id=decrypt($param2);
            $data['status'] = 2;
            //Hapus Notifikasi
                $this->md_notifikasi->updateNotifikasiByPendaftaranDetailId($param2,$data);

            //Hapus Pembayaran
                $this->md_pembayaran->updatePembayaranByPendaftaranDetailId($id,$data);

            //Hapus Syarat Jemaah
                $this->md_syarat_status->updateAllSyaratStatusByJemaah($data,$id);

            //Cek tabel pendaftaran
                $x = $this->md_pendaftaran_detail->getPendaftaranDetailByPendaftarandetailIdV2($id);
                $pendaftaran_id = $x[0]->pendaftaran_id;

            //Jika jemaah atas pendaftaran X masih ada, table pendaftaran tidak perlu status=2
                $cek = $this->md_pendaftaran_detail->getPendaftaranDetailByPendaftaranId($pendaftaran_id);
                if(count($cek)>1){
                    $this->md_pendaftaran_detail->updatePendaftaranDetail($id,$data);
                }
                else if(count($cek) == 1){
                    $pendaftaran_id=$cek[0]->pendaftaran_id;
                    $this->md_pendaftaran_detail->updatePendaftaranDetail($id,$data);
                    $this->md_pendaftaran->updatePendaftaran($pendaftaran_id,$data);
                }


            $this->md_log->addLog(adminLog('Menghapus Jemaah','Menghapus data jemaah '.$x[0]->nama_jemaah));
            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'cancel'){
            $id=decrypt($param2);
            $x = $this->md_pendaftaran_detail->getPendaftaranDetailByPendaftarandetailIdV2($id);
            $data['status_keberangkatan'] = 'Cancel';
            $this->md_pendaftaran_detail->updatePendaftaranDetail($id,$data);
            $this->md_log->addLog(adminLog('Pembatalan Jemaah','Keberangkatan Jemaah '.$x[0]->nama_jemaah.' dibatalkan'));
            echo json_encode('update_success');
            die;
        }    
        else if($param1 == 'process_again'){
            $id=decrypt($param2);
            $x = $this->md_pendaftaran_detail->getPendaftaranDetailByPendaftarandetailIdV2($id);
            $data['status_keberangkatan'] = 'Dalam Proses';
            $this->md_pendaftaran_detail->updatePendaftaranDetail($id,$data);
            $this->md_log->addLog(adminLog('Proses Jemaah','Memproses kembali jemaah '.$x[0]->nama_jemaah.' yang sebelumnya dibatalkan'));
            echo json_encode('update_success');
            die;
        }     
        else if($param1 == 'daftar'){
            $page_data['tour_leader'] = $this->md_tour_leader->getAllTourLeaderV2();
            $page_data['show_form'] = TRUE;
        }
        else if($param1 == 'cekPaket'){
            //Jenis Paket
                $jenispaket = decrypt($this->input->post('jenis_paket'));

            //Berangkat Awal
                $prov = $this->input->post('provinsi');
                if($prov != 'Semua Keberangkatan')
                    $prov = decrypt($prov);

            //Berangkat Transit
                 $trans = $this->input->post('berangkat_transit');

            //Maskapai
                $maskapai = $this->input->post('maskapai');
                if($maskapai != 'Semua Maskapai')
                    $maskapai = decrypt($maskapai);

            //Bintang Hotel
                $bintang = $this->input->post('bintang');

            //Bulan dan Tahun
                $list_bulan = all_bulan();
                $bln = $this->input->post('bulan');
                $thn = $this->input->post('tahun');

            //Begin Search
                $x = $this->md_paket->getPaketBySearchAdmin($prov,$bln,$thn,"",$jenispaket,$trans,$maskapai,$bintang);
                // echo_array($x);
                foreach($x as $row){
                    //cek apakah tanggal keberangkatan sudah di set
                    $row->keberangkatan = "";
                    $cek = $this->md_keberangkatan->getKeberangkatanByPaketWaktuId($row->paketwaktu_id);
                    if($cek){
                        foreach($cek as $row2){
                            $row2->keberangkatan_id  = encrypt($row2->keberangkatan_id);
                            $row2->tgl_keberangkatan = date('d-M-Y',strtotime($row2->tgl_keberangkatan));
                        }
                        $row->keberangkatan = $cek;
                    }
                        
                    $row->bulan_berangkat = $list_bulan[$row->bulan_berangkat];
                    $row->paketwaktu_id   = encrypt($row->paketwaktu_id);
                    $row->pwk_id          = encrypt($row->pwk_id);
                    $row->paket_id        = encrypt($row->paket_id);
                    $row->berangkat_awal  = '('.$row->kode_kota.') '.$row->nama_kota;
                    $row->harga           = rupiah_format($row->harga);
                    $row->harga_awal      = $row->harga_awal ? rupiah_format($row->harga_awal) : NULL;
                    $row->diskon          = $row->diskon ? rupiah_format($row->diskon) : NULL;
                    
                    if($row->kurs_dollar){
                        $row->harga_usd   = '$'.number_format($row->harga_usd);
                        $row->kurs_dollar = number_format($row->kurs_dollar);
                    }

                }
            // echo_array($x);
            echo json_encode($x);
            die;
        }
        else if($param1 == 'filterFormPaket'){
            $jenispaket_id = decrypt($param2);
            echo json_encode(getAtributFormPaket($jenispaket_id));
            die;
        }
        else if($param1 == 'setVerifikasi'){
            $temp = explode('_',$this->input->post('status_verifikasi'));
            $pendaftarandetail_id = decrypt($temp[0]);
            $data['status_verifikasi'] = $temp[1];
            $this->md_pendaftaran_detail->updatePendaftaranDetail($pendaftarandetail_id,$data);

            $x = $this->md_pendaftaran_detail->getPendaftaranDetailByPendaftarandetailIdV2($pendaftarandetail_id);
            $this->md_log->addLog(adminLog('Verifikasi Jemaah','Merubah status verifikasi jemaah '.$x[0]->nama_jemaah.' menjadi '.$data['status_verifikasi']));
            echo json_encode('update_success');
            die;    
        }
        else if($param1 == 'pagination'){
            //List yang mucul hanya jemaah yang terverfikasi
                $dt = $this->md_pendaftaran_detail->getListJemaah($param2);

            $no=0;
            $list_bulan = all_bulan();
            $data =array();
            foreach($dt['data'] as $row){
                $id = encrypt($row->pendaftarandetail_id);

                if($row->jenis_paket=='Haji'){
                    $paket = '';
                    $tgl_keberangkatan = $row->tahun_est_berangkat;
                } else {
                    $paket = $list_bulan[$row->bulan_berangkat].' '.$row->tahun_berangkat;


                    //Tgl Keberangkatan
                        if($row->tgl_keberangkatan)
                            $tgl_keberangkatan = '<label class="label bg-green-jungle label-sm">'.date('d-M-Y',strtotime($row->tgl_keberangkatan)).'</label>';
                        else 
                            $tgl_keberangkatan = '<label class="label label-warning label-sm">Belum Diset</label>';

                }
                    //No registrasi
                        $no_regis = '<small style="font-size:11px"><a href="'.base_url().'administrator/pendaftaran/detail_jemaah/'.$id.'">'.$row->no_registrasi.'</a><span class="pull-right" id="label_jemaah_'.$id.'">';
                        
                    //Atribut Jemaah
                        $bc      = AtributJemaah($row,'Kelas Pesawat');
                        $sb      = AtributJemaah($row,'Sharing Bed');
                        $inft    = AtributJemaah($row,'Infant');
                        $progrv  = AtributJemaah($row,'Visa');
                        $mahrm   = AtributJemaah($row,'Mahram');
                        $tourled = AtributJemaah($row,'Tour Leader');
                        $atribut = ''.$no_regis.$bc.$sb.$inft.$mahrm.$progrv.$tourled;

                    //Verifikasi
                        $status = $row->status_verifikasi;
                        $select_valid = $status=='Valid' ? 'selected' : '';
                        $select_tidak_valid = $status=='Tidak Valid' ? 'selected' : '';
                        $verif = '<select onChange="gantiStatusVerif('.$id.')"  name="status_verifikasi_'.$id.'" id="status_verifikasi_'.$id.'">
                                    <option value="">Menunggu</option>
                                    <option value="'.$id.'_Valid" '.$select_valid.'>Valid</option>
                                    <option value="'.$id.'_Tidak Valid" '.$select_tidak_valid.'>Tidak Valid</option>
                                </select>';

                    //Aksi
                        $btn1 = '<li><a href="'.base_url().'administrator/pendaftaran/detail_jemaah/'.$id.'"><i class="fa fa-search"></i>&nbsp;Detail</a></li>';
                        $btn2 = '<li><a href="'.base_url().'administrator/pendaftaran/detail_jemaah/'.$id.'/cetak'.'" target="_blank" ><i class="fa fa-print"></i>&nbsp;Cetak</a></li>';
                        $btn3 = '<li><a href="javascript:;" onClick="edit_function(\'edit\','. $id.');"><i class="fa fa-pencil"></i>&nbsp;Edit</a></li>';
                        $btn4 = '<li><a href="javascript:;" onClick="edit_function(\'delete\','. $id.')"><i class="fa fa-trash"></i>&nbsp;Hapus</a></li>';
                        $btn5 = '<li><a href="javascript:;" onClick="edit_function(\'process_again\','. $id.')"><i class="icon-reload"></i>&nbsp;Proses Kembali</a></li>';
                        $btn6 = '<li><a href="javascript:;" onClick="edit_function(\'cancel\','. $id.')"><i class="icon-ban"></i>&nbsp;Cancel</a></li>';
      
                        switch ($row->status_keberangkatan) {
                            case 'Cancel':
                                $list_btn = $btn1.$btn2.$btn5; 
                                break;
                            case 'Sudah Berangkat':
                                $list_btn = $btn1.$btn2; 
                                break;
                            default:
                                if($row->status_verifikasi=='Valid')
                                    $list_btn = $btn1.$btn2.$btn3.$btn4.$btn6;
                                else 
                                    $list_btn = $btn1;
                                break;
                        }
                    $aksi = '<div class="btn-group">
                                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    '.$list_btn.'
                                </ul>
                            </div>';

                $th1 = ++$no;
                $th2 = $row->nama_jemaah.'<br><small style="font-size:11px">'.$atribut.'</span></small>';
                $th3 = $row->nama_paket ? $row->nama_paket.' <br><small>'.$paket.'</small>' : '-';
                $th4 = $row->nama_kota ? $row->nama_kota :'-' ;
                $th5 = $tgl_keberangkatan;
                $th6 = $row->nama_cs.'<br><small style="font-size:11px">'.date('d-M-Y H:i',strtotime($row->tgl_submit)).'</span></small>';
                $th7 = $verif;
                $th8 = $aksi;
                $data[] = gathered_data(array($th1,$th2,$th3,$th4,$th5,$th6,$th7,$th8));
            }
            $dt['data'] = $data;
            echo json_encode($dt);
            die;
        }

        $page_data['list_bulan']     = all_bulan();
        $page_data['member']         = $this->md_member->getAllMember();
        $page_data['berangkat_awal'] = $this->md_kantor_cabang->getAllKantorCabangOnly();
        $page_data['bulan']          = $this->md_paket_waktu->getAllPaketWaktuBulan();
        $page_data['tahun']          = $this->md_paket_waktu->getAllPaketWaktuTahun();
        $page_data['kota']           = $this->md_kota->getAllKota();

        if($param3=='cetak'){
            $this->load->view('login/page/cetak_pendaftaran',$page_data);
            die;
        }

        $page_data['page_name'] = 'pendaftaran';
        $page_data['page_title'] = 'Pendaftaran';
        $this->load->view('login/index', $page_data);        
    }
//#------------------------------------------------------------------------------------------------#//  
    public function perlengkapan($param1='',$param2=''){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        $login_type=$this->session->userdata('login_type');
        $page_data['show_detail'] = FALSE;
        $page_data['list_barang'] =  FALSE;

        if($param1=='add'){
            $error = array();
            $this->db->trans_start();
            $pendaftarandetail_id = decrypt($param2);
            $bkantor_id           = $this->input->post('bkantor_id');
            $qty                  = $this->input->post('qty');
            $tgl_penyerahan       = $this->input->post('tgl_penyerahan');

            if(count($bkantor_id)==0){
                $this->session->set_flashdata('err_add','Pastikan form penyerahan diisi dengan benar !');
                redirect(base_url().'administrator/perlengkapan/show_detail/'.$param2);
            }

            //Insert ke table faktur, jenis faktur = Perlengkapan Jemaah
                //Cek Sequence No Perlengkapan Jemaah
                    $x                 = getLatestSequence('Perlengkapan Jemaah');
                    $data['seq']       =  $x['new_seq'];
                    $data['no_faktur'] =  'PJ'.substr(date('Y'),2).date('m').$x['new_seq'];

                $data['jenis_faktur']    = 'Perlengkapan Jemaah';
                $data['tgl_transaksi']   = date("Y-m-d H:i");
                $data['pengguna_id']     = $this->session->userdata('pengguna_id');
                $data['total_qty']       = $this->input->post('total_qty');
                $data['kantorcabang_id'] = decrypt($this->input->post('kantorcabang_id'));
                $data['keterangan']      = $this->input->post('catatan');
                $data['status']          = 1;
                $this->md_transaksi->addFaktur_transact($data);
                $faktur_id = $this->db->insert_id();

            for($i=0;$i<count($bkantor_id);$i++){
                $id = decrypt($bkantor_id[$i]);
                $brg       = $this->md_barang_kantor->getBarangKantorById($id);
                $stok_awal = $brg[0]->stok_akhir;

                //Update jumlah stok di tabel barang_kantor. Penyerahan ke jemaah dianggap keluar, kurangkan
                    $stok_akhir = $stok_awal - $qty[$i];
                    if($stok_akhir<0){
                        $error[] = 'Stok <b>"'.$brg[0]->nama_barang.'"</b> tidak mencukupi ! (Sisa stok : '.$stok_awal.' , Jumlah penyerahan : '.$qty[$i].')';
                    }
                    $data3['stok_akhir'] = $stok_akhir;
                    $this->md_barang_kantor->updateBarangKantorId($id,$data3);

                //Input ke barang_transaksi
                    $data4['stok_awal']   = $stok_awal;
                    $data4['stok_akhir']  = $stok_akhir;
                    $data4['bkantor_id']  = $id;
                    $data4['jenis_trans'] = 'Keluar';
                    $data4['qty']         = $qty[$i];
                    $data4['pengguna_id'] = $this->session->userdata('pengguna_id');
                    $data4['faktur_id']   = $faktur_id;
                    $data4['status']   = 1;
                    $this->md_transaksi->addBarang_Transaksi_transact($data4);
                    $btrans_id = $this->db->insert_id();

                //Perlengkapan
                    $data2['pendaftarandetail_id'] = $pendaftarandetail_id;
                    $data2['btrans_id']            = $btrans_id;
                    $data2['tgl_penyerahan']       = date('Y-m-d',strtotime($tgl_penyerahan[$i]));
                    $data2['tgl_input']            = date('Y-m-d H:i');
                    $data2['status']               = 1;
                    $this->md_perlengkapan->addPerlengkapan_transact($data2);

                //Data jemaah
                    $dt = $this->md_pendaftaran_detail->getPendaftaranDetailByPendaftarandetailIdV2($pendaftarandetail_id);

                //Log
                    $this->md_log->addLog_transact(adminLog('Penyerahan Perlengkapan Jemaah','Diserahkan kepada '.$dt[0]->nama_jemaah.' : '.$data4['qty'].' '.$brg[0]->nama_barang.'. Stok awal : '.$stok_awal.', Stok akhir :'.$stok_akhir));
            }

            if(count($error)>0 || $this->db->trans_status() === FALSE){
                $temp['error'] = $error;
                $this->session->set_flashdata('err_add',$error);
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
                $this->session->set_flashdata('add_success',TRUE);        
            }

            redirect(base_url().'administrator/perlengkapan/show_detail/'.$param2);
        }
        else if($param1 == 'pembatalan'){
            $faktur_id            = decrypt($this->input->post('faktur_id'));
            $kantorcabang_id      = decrypt($this->input->post('kantorcabang_id'));
            $pendaftarandetail_id = decrypt($this->input->post('pendaftarandetail_id'));
            // $x = $this->md_transaksi->getFakturByIdByKantorcabangId($faktur_id,$kantorcabang_id);

            $data2['status'] = 2;
            $x = $this->md_transaksi->getBarangTransaksiByFakturId($faktur_id);
            // echo_array($x);die;
            foreach($x as $row){
                //Pembatalan akan kembali menambah stock barang di barang_kantor
                    $y = $this->md_barang_kantor->getBarangKantorByBarangIdByKantorcabangId($row->barang_id,$kantorcabang_id);
                    $stok_awal          = $y[0]->stok_akhir;
                    $stok_akhir         = $stok_awal + $row->qty;
                    $data['stok_akhir'] = $stok_akhir;
                    $this->md_barang_kantor->updateBarangKantorbyBarangIdByKantorcabangId($row->barang_id,$kantorcabang_id,$data);

                $this->md_perlengkapan->updatePerlengkapanByPendaftaranDetailIdByBtransId($pendaftarandetail_id,$row->btrans_id,$data2);
                
            }


            $this->md_transaksi->updateFaktur($faktur_id,$data2);
            $this->md_transaksi->updateBarangTransaksiByFakturId($faktur_id,$data2);

            //Pembatalan akan menghapus transaksi. jika sudah tidak ada transaksi maka faktur diahpus juga ????????????????
            //Log
                $z = $this->md_pendaftaran_detail->getPendaftaranDetailByPendaftarandetailIdV2($pendaftarandetail_id);
                $this->md_log->addLog(adminLog('Pembatalan Penyerahan Perlengkapan Jemaah','Membatalkan penyerahan '.$x[0]->qty.' '.$y[0]->satuan.' '.$y[0]->nama_barang.' atas jemaah '.$z[0]->nama_jemaah.'. Stok awal '.$stok_awal.', Stok akhir '.$stok_akhir));

            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'get_list_perlengkapan'){
            $id = decrypt($param2);
            $x = $this->md_perlengkapan->getPerlengkapanByFakturId($id);
            foreach($x as $row){
                $row->tgl_penyerahan = date('d-M-Y',strtotime($row->tgl_penyerahan));
            }
            echo json_encode($x);
            die;
        }
        else if($param1 =='show_detail'){
            $id = decrypt($param2);
            $page_data['dt_jemaah']            = $this->md_pendaftaran_detail->getPendaftaranDetailByPendaftarandetailIdV2(decrypt($param2));
            $page_data['list_barang']          = $this->md_barang->getAllBarang();
            $page_data['list_faktur']          = $this->md_transaksi->getFakturByPendaftaranDetailId($id);
            $page_data['show_detail']          = TRUE;
            $page_data['pendaftarandetail_id'] = $param2;
        }
        else if($param1 == 'cetak'){
            $id = decrypt($param2);
            $x = $this->md_perlengkapan->getDetailJemaahByPerlengkapanByFakturId($id);
            $page_data['perlengkapan'] = $x;
            $this->load->view('login/page/cetak_perlengkapan',$page_data);
            die;
        }
        else if($param1 == 'pagination'){
            //List yang mucul hanya jemaah yang terverfikasi
                $dt = $this->md_perlengkapan->getListJemaahWithPerlengkapan($param2);

            $no=0;
            $data =array();
            foreach($dt['data'] as $row){
                $id = encrypt($row->pendaftarandetail_id);

                //Tanggal Keberangkatan
                if($row->jenis_paket=='Haji'){
                    $tgl_keberangkatan = 'Estimasi Tahun '.$row->tahun_est_berangkat;
                } else {
                    if($row->tgl_keberangkatan)
                        $tgl_keberangkatan = '<label class="label bg-green-jungle label-sm">'.date('d-M-Y',strtotime($row->tgl_keberangkatan)).'</label>';
                    else 
                        $tgl_keberangkatan = '<label class="label label-warning label-sm">Belum Diset</label>';
                    
                }
                
                //No registrasi
                    $no_regis = '<small style="font-size:11px"><a href="'.base_url().'administrator/pendaftaran/detail_jemaah/'.$id.'">'.$row->no_registrasi.'</a><span class="pull-right" id="label_jemaah_'.$id.'">';
                
                //Atribut Jemaah
                    $bc     = AtributJemaah($row,'Kelas Pesawat');
                    $sb     = AtributJemaah($row,'Sharing Bed');
                    $inft   = AtributJemaah($row,'Infant');
                    $progrv = AtributJemaah($row,'Visa');
                    $mahrm  = AtributJemaah($row,'Mahram');
                    $tourled = AtributJemaah($row,'Tour Leader');
                    $atribut = ''.$no_regis.$bc.$sb.$inft.$mahrm.$progrv.$tourled;


                //Totla barang yang telah diserahkan
                    $status = $row->total_penyerahan > 0 ? '<i class="fa fa-check font-green-jungle"></i> '.$row->total_penyerahan. ' Item' : '-';

                //Aksi
                    $btn1 = '<li><a href="'.base_url().'administrator/perlengkapan/show_detail/'.$id.'"><i class="fa fa-search"></i>&nbsp;Cek Perlengkapan</a></li>';
                    $btn2 = '<li><a href="'.base_url().'administrator/pendaftaran/detail_jemaah/'.$id.'"><i class="fa fa-user"></i>&nbsp;Detail Jemaah</a></li>';
  
                    switch ($row->status_keberangkatan) {
                        case 'Cancel':
                            $list_btn = $btn2; 
                            break;
                        case 'Sudah Berangkat':
                            $list_btn = $btn2; 
                            break;
                        default:
                            $list_btn = $btn1.$btn2;
                            break;
                    }

                    $aksi = '<div class="btn-group">
                                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu" role="menu">'.$list_btn.'</ul>
                            </div>';

                $th1 = ++$no;
                $th2 = $row->nama_jemaah.'<br><small style="font-size:11px">'.$atribut.'</span></small>';
                $th3 = $tgl_keberangkatan;
                $th4 = $status;
                $th5 = $aksi;
                $data[] = gathered_data(array($th1,$th2,$th3,$th4,$th5));
            }

            $dt['data'] = $data;
            echo json_encode($dt);
            die;
        }        

        $page_data['kantor_cabang'] = getListKantorCabang($this->session->userdata('login_type'));

        $page_data['syarat'] = $this->md_syarat_umroh->getAllSyaratUmroh();
        $page_data['page_name'] = 'perlengkapan';
        $page_data['page_title'] = 'Perlengkapan Jemaah';
        $this->load->view('login/index', $page_data);        
    }
//#------------------------------------------------------------------------------------------------#//  
    public function persyaratan($param1='',$param2=''){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        $login_type=$this->session->userdata('login_type');

        if($param1=='add_syarat_to_jamaah'){
            for($i=0;;$i++){
                $syaratumroh = $this->input->post('syaratumroh_'.$i);
                if($syaratumroh){
                    $temp = explode('_',$syaratumroh);
                    $status = $temp[1];
                    $data['syaratumroh_id'] = $temp[0];
                    $data['pendaftarandetail_id'] =  decrypt($param2);
                    $data['status_syarat'] = 'Belum Lengkap';
                    $data['status'] = 1;

                    //True berarti akan ditambahkan
                    if($status=='true'){

                        //Syarat akan ditambahkan jika belum ada
                        $cek = $this->md_syarat_status->cekSyaratStatusPendaftar($data['syaratumroh_id'],$data['pendaftarandetail_id']);
                        if(!$cek)
                            $this->md_syarat_status->addSyaratStatus($data);
                    } else if ($status=='false'){
                        $this->md_syarat_status->removeSyaratStatusByJemaah($data['syaratumroh_id'],$data['pendaftarandetail_id']);
                    }
                }
                else
                    break;
            }
            $temp = $this->md_pendaftaran_detail->getPendaftaranDetailByPendaftarandetailIdV2(decrypt($param2));
            $this->md_log->addLog(adminLog('Memperbaharui List Syarat Jemaah','Memperbaharui List Syarat Jemaah '.$temp[0]->nama_jemaah));            
            echo json_encode('update_success');
            die;
        }
        else if($param1=='update_syarat_jemaah'){
            $pendaftarandetail_id = decrypt($param2);
            for($i=0;;$i++){
                $syaratumroh = $this->input->post('syaratjemaah_'.$i);
                $tgl_penyerahan = $this->input->post('tgl_penyerahan_'.$i);
                if($syaratumroh){
                    $temp = explode('_',$syaratumroh);
                    $status = $temp[1];
                    $data['syaratumroh_id'] = decrypt($temp[0]);
                    $data['pendaftarandetail_id'] =  $pendaftarandetail_id;


                    //True berarti Lengkap
                    if($status=='true'){

                        $data['tgl_penyerahan'] = $tgl_penyerahan > 0 ? dateConvertToDatabase($tgl_penyerahan) : NULL; 
                        $data['status_syarat'] = 'Lengkap';
                        $data['approve'] = 'Ya';
                        $this->md_syarat_status->updateSyaratStatusByJemaah($data,$data['syaratumroh_id'],$data['pendaftarandetail_id']);
                    } else if ($status=='false'){
                        $data['tgl_penyerahan'] = NULL;
                        $data['status_syarat'] = 'Belum Lengkap';
                        $this->md_syarat_status->updateSyaratStatusByJemaah($data,$data['syaratumroh_id'],$data['pendaftarandetail_id']);
                    }
                }
                else
                    break;
            }

            $temp = $this->md_pendaftaran_detail->getPendaftaranDetailByPendaftarandetailIdV2($pendaftarandetail_id);
            $this->md_log->addLog(adminLog('Memperbaharui Syarat Jemaah','Memperbaharui Syarat Jemaah '.$temp[0]->nama_jemaah));
            
            echo json_encode('update_success');
            die;
        }
        else if($param1=='show_syarat'){
            $id=decrypt($param2);
            $x = $this->md_syarat_status->getSyaratStatusByPendaftarandetailIdV2($id);

            for($i=0;$i<count($x);$i++){
                if($x[$i]->media_id){
                    $y = $this->md_media_detail->getMediaDetailByMediaId($x[$i]->media_id);
                    $x[$i]->media_link = base_url().$y[0]->media_link;
                }
                    $x[$i]->syaratumroh_id = encrypt($x[$i]->syaratumroh_id);
                    $x[$i]->syaratstatus_id = encrypt($x[$i]->syaratstatus_id);
                    $x[$i]->tgl_penyerahan = $x[$i]->tgl_penyerahan ? date('d/m/Y',strtotime($x[$i]->tgl_penyerahan)) : '';
            }

            echo json_encode($x);
            die;
        }
        else if($param1=='tolak_file'){
            $id = decrypt($param2);
            $email = $this->input->post('email');
            $pendaftarandetail_id = $this->input->post('pendaftarandetail_id');
            $pendaftaran_id = $this->input->post('pendaftaran_id');
            $nama_jemaah = $this->input->post('nama_jemaah');
            $syarat = $this->input->post('syarat');

            $data['approve'] = 'Tidak';
            $this->md_syarat_status->updateAllSyaratStatusBySyaratStatusId($data,$id);

            $link = base_url().'member/detail/pendaftar/'.$pendaftaran_id.'#'.$pendaftarandetail_id;
            emailTolakSyarat($nama_jemaah,$email,$link,$syarat);

            echo json_encode("update_success");
            die;
        }
        else if($param1=='get_all_syarat'){
            $id=decrypt($param2);
            $syarat = $this->md_syarat_umroh->getAllSyaratUmroh();
            $cek_syarat = $this->md_syarat_status->getSyaratStatusByPendaftarandetailIdV2($id);
            

            $i=0;
            foreach($syarat as $row){
                $list_syarat[$i]['syaratumroh_id'] = $row->syaratumroh_id;
                $list_syarat[$i]['persyaratan'] = $row->persyaratan;
                foreach($cek_syarat as $row2){
                    if($row->persyaratan == $row2->persyaratan){
                        $list_syarat[$i]['status'] = 'Ada';
                    }
                    

                }
                $i++;
            }
            echo json_encode($list_syarat);
            die;
        }
        else if($param1=='cetak'){
            $id = decrypt($param2);
            $x = $this->md_syarat_status->getSyaratStatusByPendaftarandetailIdV2($id);

            $page_data['persyaratan'] = $x;
            $this->load->view('login/page/cetak_persyaratan',$page_data);
            die;
        }
        else if($param1 == 'pagination'){
            //List yang mucul hanya jemaah yang terverfikasi
                $dt = $this->md_syarat_status->getListJemaahWithPersyaratan($param2);

            $no=0;
            $data =array();
            foreach($dt['data'] as $row){
                $id = encrypt($row->pendaftarandetail_id);

                //Tanggal Keberangkatan
                if($row->jenis_paket=='Haji'){
                    $tgl_keberangkatan = 'Estimasi Tahun '.$row->tahun_est_berangkat;
                } else {
                    if($row->tgl_keberangkatan)
                        $tgl_keberangkatan = '<label class="label bg-green-jungle label-sm">'.date('d-M-Y',strtotime($row->tgl_keberangkatan)).'</label>';
                    else 
                        $tgl_keberangkatan = '<label class="label label-warning label-sm">Belum Diset</label>';   
                }

                //No registrasi
                    $no_regis = '<small style="font-size:11px"><a href="'.base_url().'administrator/pendaftaran/detail_jemaah/'.$id.'">'.$row->no_registrasi.'</a><span class="pull-right" id="label_jemaah_'.$id.'">';

                //Atribut Jemaah
                    $bc     = AtributJemaah($row,'Kelas Pesawat');
                    $sb     = AtributJemaah($row,'Sharing Bed');
                    $inft   = AtributJemaah($row,'Infant');
                    $progrv = AtributJemaah($row,'Visa');
                    $mahrm  = AtributJemaah($row,'Mahram');
                    $tourled = AtributJemaah($row,'Tour Leader');
                    $atribut = ''.$no_regis.$bc.$sb.$inft.$mahrm.$progrv.$tourled;


                //Status PemSyarat
                    if($row->total_syarat > 0){
                        $btn_cetak = TRUE;
                        if($row->syarat_belum_lengkap==0){
                            $status = '<button class="btn btn-xs green"><i class="fa fa-check"></i></button> Lengkap';
                        } else {
                            $status = '<button class="btn btn-xs red"><i class="fa fa-info"></i></button>'.$row->syarat_belum_lengkap.' Syarat belum lengkap';
                        }
                    } else {
                        $status = '<button class="btn btn-xs red"><i class="fa fa-exclamation"></i></button> Syarat belum ditentukan';
                        $btn_cetak = FALSE;
                    }


                //Aksi
                    $btn1 = '<li><a href="javascript:;" onClick="edit_function(\'show_syarat\','.$id.');" class="btn_edit'.$id.'"><i class="fa fa-search"></i>&nbsp;Cek Persyaratan</a></li>';
                    $btn2 = '<li><a href="'.base_url().'administrator/pendaftaran/detail_jemaah/'.$id.'"><i class="fa fa-user"></i>&nbsp;Detail Jemaah</a></li>';
                    $btn3 = '<li><a href="'.base_url().'administrator/persyaratan/cetak/'.$id.'" target="_blank" ><i class="fa fa-print"></i>&nbsp;Cetak</a></li>';

                    if($row->status_keberangkatan == 'Sudah Berangkat' || $row->status_keberangkatan == 'Cancel'){
                        $btn1 = '';
                    }

                    if(!$btn_cetak)
                        $btn3 = '';


                    $aksi = '<div class="btn-group">
                                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    '.$btn1.$btn2.$btn3.'
                                </ul>
                            </div>';

                $th1 = ++$no;
                $th2 = $row->nama_jemaah.'<br><small style="font-size:11px">'.$atribut.'</span></small>';
                $th3 = $tgl_keberangkatan;
                $th4 = $status;
                $th5 = $aksi;
                $data[] = gathered_data(array($th1,$th2,$th3,$th4,$th5));
            }

            $dt['data'] = $data;
            echo json_encode($dt);
            die;
        }        

        $page_data['bulan'] = all_bulan();
        $page_data['syarat'] = $this->md_syarat_umroh->getAllSyaratUmroh();
        $page_data['page_name'] = 'persyaratan';
        $page_data['page_title'] = 'Persyaratan Jemaah';
        $this->load->view('login/index', $page_data);        
    }
//#------------------------------------------------------------------------------------------------#//  
    public function pembayaran($param1='',$param2='',$param3=''){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        $login_type                            = $this->session->userdata('login_type');
        $page_data['show_jemaah']              = FALSE;
        $page_data['show_verif']               = FALSE;
        $page_data['dt_pembayaran']            = FALSE;
        $page_data['dt_pengembalian']          = FALSE;
        $page_data['show_detail_pembayaran']   = FALSE;
        $page_data['show_detail_pengembalian'] = FALSE;
        $page_data['dt_jemaah']                = FALSE;
        $page_data['pendaftarandetail_id']     = FALSE;
        $page_data['dt_bank_transfer']         = $this->md_bank->getBankByJenis('Transfer');
        $page_data['dt_bank_edc']              = $this->md_bank->getBankByJenis('EDC');
        
        if($param1 == 'add'){
            $pendaftarandetail_id = decrypt($this->input->post('pendaftarandetail_id'));
            $jenis_transaksi = $this->input->post('jenis_transaksi');
            $data['metode_transaksi']     = $this->input->post('metode_transaksi');

            if($jenis_transaksi == 'Pembayaran'){
                $data['tgl_bayar']         = date('Y-m-d',strtotime($this->input->post('tgl_bayar')));
                $data['jumlah_bayar']      = remove_number_format($this->input->post('jumlah_bayar'));
                $data['status_pembayaran'] = $this->input->post('status_pembayaran');

                if($data['metode_transaksi']=='Transfer'){
                    $data['nama_rek_pengirim'] = $this->input->post('nama_pengirim');
                    $data['bank_pengirim']     = $this->input->post('bank_pengirim');
                    $data['bank_id']           = decrypt($this->input->post('bank_penerima'));
                    $data['tgl_bayar_bank']    = date('Y-m-d',strtotime($this->input->post('tgl_bayar_bank')));
                } else if($data['metode_transaksi']=='EDC'){
                    $biaya_tambahan = $this->input->post('biaya_tambahan');
                    $data['bank_id']        = decrypt($this->input->post('bank_penerima'));
                    $data['biaya_tambahan'] = $biaya_tambahan;
                    $data['jumlah_bayar']   = $data['jumlah_bayar'] + $biaya_tambahan;
                }
                $this->session->set_flashdata('sucess','Pembayaran');
            }
            else if($jenis_transaksi == 'Pengembalian'){
                $data['tgl_bayar']          = date('Y-m-d',strtotime($this->input->post('tgl_pengembalian')));
                $data['jumlah_bayar']       = remove_number_format($this->input->post('jumlah_pengembalian'));
                $data['jenis_pengembalian'] = $this->input->post('jenis_pengembalian');

                if($data['metode_transaksi']=='Transfer'){
                    $data['bank_id']           = decrypt($this->input->post('bank_penerima'));
                    $data['kembali_rek_penerima']  = $this->input->post('kembali_rek_penerima');
                    $data['kembali_bank_penerima'] = $this->input->post('kembali_bank_penerima').' - '.$this->input->post('kembali_no_rek_penerima');
                    $data['kembali_tgl_transfer']  = date('Y-m-d',strtotime($this->input->post('kembali_tgl_transfer')));
                }

                if($data['jenis_pengembalian'] == 'Cancel'){
                    $_data['status_keberangkatan'] = 'Cancel';
                    $this->md_pendaftaran_detail->updatePendaftaranDetail($pendaftarandetail_id,$_data);
                } else {
                    $this->session->set_flashdata('sucess','Pengembalian');
                }
            }

            $data['catatan']              = $this->input->post('catatan');
            $data['jenis_transaksi']      = $jenis_transaksi;
            $data['pendaftarandetail_id'] = $pendaftarandetail_id;
            $data['status']               = 1;

            //Cek Sequence No Kwitansi
                $x = getLatestSequence('Kwitansi');
                $data['seq']         =  $x['new_seq'];
                $data['no_kwitansi'] =  'K'.date('y').date('m').$x['new_seq'];
                $data['pengguna_id'] = $this->session->userdata('pengguna_id');
                $data['status_verifikasi'] = 'Accept';
                $data['tgl_update'] = date('Y-m-d H:i:s');

            $this->md_pembayaran->addPembayaran($data);

        
            //Log
                $dt_jemaah   = $this->md_pendaftaran_detail->getPendaftaranDetailByPendaftarandetailIdV2($pendaftarandetail_id);
                $nama_jemaah = $dt_jemaah[0]->nama_jemaah;
                $this->md_log->addLog(adminLog('Memproses '.$jenis_transaksi,$this->session->userdata('nama'). ' memproses '.$jenis_transaksi.' jemaah '.$nama_jemaah.' sebesar '.rupiah_format($data['jumlah_bayar'])));
            
            redirect(base_url().'administrator/pembayaran/detail_jemaah/'.encrypt($pendaftarandetail_id).'/'.strtolower($jenis_transaksi));
            die;
        }
        else if($param1 =='total_pembayaran'){
            $id=decrypt($param2);
            $x = $this->md_pembayaran->getTotalPembayaranByJemaah($id);
            $y = $this->md_pendaftaran_detail->getPendaftaranDetailByPendaftarandetailIdV2($id);

            $a['total_bayar'] = rupiah_format($x[0]->total_pembayaran);
            $b['harus_bayar'] = rupiah_format($y[0]->harus_bayar);
            $sisa = $y[0]->harus_bayar-$x[0]->total_pembayaran;
            if($sisa==0)
                $sisa = '<span class="font-green"><i class="fa fa-check"></i>&nbsp;LUNAS</span>';
            else
                $sisa = rupiah_format($sisa);

            $c['sisa'] = $sisa;

            $data=array();
            array_push($data, $a);
            array_push($data, $b);
            array_push($data, $c);
            echo json_encode($data);
            die;
        }
        else if($param1 == 'detail_jemaah' || $param1 == 'get_list_pembayaran' || $param1 == 'cetak'){
            $id = decrypt($param2);

            if($param1 == 'detail_jemaah' ){
                $x = $this->md_pembayaran->getPembayaranByJemaah($id);
                $pendaftarandetail_id = encrypt($id);
            }
            else if($param1 == 'cetak' || $param1 == 'get_list_pembayaran'){
                $x = $this->md_pembayaran->getPembayaranById($id);
                $pendaftarandetail_id = encrypt($x[0]->pendaftarandetail_id);
            }
            
            $pembayaran   = array();
            $pengembalian = array();
            $total_setoran_awal = 0;
            foreach($x as $row){
                $row->pembayaran_id        = encrypt($row->pembayaran_id);
                $row->pendaftarandetail_id = encrypt($row->pendaftarandetail_id);
                $row->catatan = $row->catatan ? $row->catatan : '-';
                $row->tgl_update = date('d-M-Y H:i',strtotime($row->tgl_update));
                // $row->jumlah_bayar = number_format($row->jumlah_bayar);

                if(!$row->status_verifikasi){
                    $row->status_verifikasi_style ='Dalam Proses</span>';
                } else {
                    $h = $this->md_pengguna->getPenggunaById($row->pengguna_id);
                    $row->kantor_pusat_nama      = $h[0]->nama_lengkap;
                    $row->kantor_pusat_hp        = $h[0]->no_hp;
                    $row->kantor_pusat_tgl_verif = date('d-M-Y H:i',strtotime($row->tgl_update));

                    if($row->status_verifikasi == 'Accept'){
                        $row->status_verifikasi_style ='<span class="font-green-jungle"><i class="fa fa-check"></i>&nbsp;'.$row->status_verifikasi.'</span>';
                    }
                    else if($row->status_verifikasi == 'Decline'){
                        $row->status_verifikasi_style ='<span class="font-red"><i class="fa fa-times"></i>&nbsp;'.$row->status_verifikasi.'</span>';
                    }

                    //Hitung Setoran Awal
                    if($row->status_pembayaran == 'Setoran Awal'){
                        $total_setoran_awal+=$row->jumlah_bayar;
                    }
                }
                if($row->jenis_transaksi == 'Pembayaran'){
                    $pembayaran[] = $row;
                }
                else if($row->jenis_transaksi == 'Pengembalian'){
                    $pengembalian[] = $row;
                }
            }   

            if($param1 == 'detail_jemaah' ){

                if($param3=='pembayaran'){
                    $page_data['show_detail_pembayaran'] =TRUE;
                    $page_data['dt_pembayaran']          = $pembayaran;
                }

                if($param3=='pengembalian'){
                    $page_data['show_detail_pengembalian'] =TRUE;
                    $page_data['total_setoran_awal']       = $total_setoran_awal;
                    $page_data['dt_pengembalian']          = $pengembalian;
                }

            } else if($param1 == 'get_list_pembayaran'){
                echo json_encode($x);
                die;

            } else if($param1 == 'cetak'){
                $page_data['pembayaran'] = $x;
                $this->load->view('login/page/cetak_pembayaran',$page_data);
                die;
            }
            $page_data['dt_jemaah'] = $this->md_pendaftaran_detail->getPendaftaranDetailByPendaftarandetailIdV2(decrypt($pendaftarandetail_id));
            $page_data['pendaftarandetail_id'] = $pendaftarandetail_id;
        }
        else if($param1 == 'verif' && $login_type == 'Kantor Pusat'){
            $pembayaran_id = decrypt($param2);
            $x = $this->md_pembayaran->getPembayaranById($pembayaran_id);
            for($i=0;$i<count($x);$i++){
                $y = $this->md_pendaftaran_detail->getPendaftaranDetailByPendaftarandetailIdV2($x[0]->pendaftarandetail_id);
                $v = $this->md_paketwaktu_kelas->getPaketWaktuKelasByIdV2($y[0]->pwk_id);
                $w = $this->md_paket->getPaketByPaketId($v[0]->paket_id);
                $z = $this->md_kantor_cabang->getKantorCabangById($x[$i]->kantorcabang_id);
                $x[$i]->nama_kantor = $z[0]->nama_cabang;
                $x[$i]->alamat = $z[0]->alamat.', '.$z[0]->nama_daerah ;
                $x[$i]->telp = $z[0]->telp;
                $x[$i]->hp = $z[0]->hp;
                $x[$i]->nama_paket = $w[0]->nama_paket;
                $x[$i]->pembayaran_id = encrypt($x[$i]->pembayaran_id);
                $x[$i]->nama_jemaah = $y[0]->nama_jemaah;
                $x[$i]->no_hp = $y[0]->no_hp;
                $x[$i]->harus_bayar = rupiah_format($y[0]->harus_bayar);
                if($x[0]->status_verifikasi){
                    $u = $this->md_pengguna->getPenggunaById($x[0]->pengguna_id);
                    $x[0]->kantor_pusat_nama = $u[0]->nama_lengkap;
                    $x[0]->kantor_pusat_hp = $x[0]->no_hp;
                    $x[0]->status_verifikasi = $x[0]->status_verifikasi;
                    $x[0]->tgl_input = $x[0]->tgl_update; 
                }
            }
            $page_data['dt_byr'] = $x;
            $page_data['show_verif'] = TRUE;
        }
        else if($param1 == 'update_verif'){
            $pembayaran_id = decrypt($this->input->post('pembayaran_id'));
            $verif = $this->input->post('btn_verif');
            if($verif == 'terima') {
                $data['status_verifikasi'] = 'Accept';
            }
            else 
                $data['status_verifikasi'] = 'Decline';

            $data['pengguna_id'] = $this->session->userdata('pengguna_id');
            $data['tgl_update'] = date('Y-m-d H:i:s');

            $this->md_pembayaran->updatePembayaran($pembayaran_id,$data);
            $this->session->set_flashdata('verif',$verif);

            $x = $this->md_pembayaran->getPembayaranById($pembayaran_id);
            $temp = $this->md_kantor_cabang->getKantorCabangById($x[0]->kantorcabang_id);
            $this->md_log->addLog(adminLog('Verifikasi Pembayaran','Menetapkan untuk '.$data['status_verifikasi'].' terhadap pengajuan pembayaran pada tanggal '.date('d-M-Y H:i',strtotime($x[0]->tgl_bayar)).' oleh '.$temp[0]->nama_cabang));
            redirect(base_url().'administrator/pembayaran');
        }
        else if($param1 == 'delete'){
            $id = decrypt($param2);
            $temp = $this->md_pembayaran->getPembayaranById($id);
            $data['status'] = 2;
            $this->md_pembayaran->updatePembayaran($id,$data);
            $this->md_log->addLog(adminLog('Hapus '.$param3,'Menghapus data '.$param3.' jemaah '.$temp[0]->nama_jemaah.' sebesar '.rupiah_format($temp[0]->jumlah_bayar)));
            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'update_harga_paket'){
            $pendaftarandetail_id = decrypt($this->input->post('pendaftarandetail_id'));
            $kurs_hari_ini = str_replace(',','',$this->input->post('kurs_hari_ini'));

            //Update Harus Bayar Jemaah
                $x = $this->md_pendaftaran_detail->getPendaftaranDetailByPendaftarandetailIdV2($pendaftarandetail_id);
                $new_harga           = $x[0]->harus_bayar_usd * $kurs_hari_ini;
                $data['kurs_dollar'] = $kurs_hari_ini;
                $data['harus_bayar'] = $new_harga;
                $this->md_pendaftaran_detail->updatePendaftaranDetail($pendaftarandetail_id,$data);

            //Update Biaya Tambahan Jemaah yang USD
                $biaya_tambahan = $this->md_pendaftaran_detail_biaya->getPendaftaranDetailBiayaByPendaftaranDetailIdByJenis($pendaftarandetail_id,'Tambahan');
                foreach($biaya_tambahan as $bt){
                    if($bt->jumlah_usd>0){
                        $data2['jumlah']      = $bt->jumlah_usd * $kurs_hari_ini;
                        $data2['kurs_dollar'] = $kurs_hari_ini;
                        $this->md_pendaftaran_detail_biaya->updatePendaftaranDetailBiaya($bt->pdbiaya_id,$data2);
                    }
                }

            //Update Biaya Pengurang Jemaah yang USD
                $biaya_pengurang = $this->md_pendaftaran_detail_biaya->getPendaftaranDetailBiayaByPendaftaranDetailIdByJenis($pendaftarandetail_id,'Pengurang');
                foreach($biaya_pengurang as $bp){
                    if($bp->jumlah_usd>0){
                        $data2['jumlah']      = $bt->jumlah_usd * $kurs_hari_ini;
                        $data3['kurs_dollar'] = $kurs_hari_ini;
                        $this->md_pendaftaran_detail_biaya->updatePendaftaranDetailBiaya($bt->pdbiaya_id,$data3);
                    }
                }

            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'pagination'){
            //List yang mucul hanya jemaah yang terverfikasi
                $dt = $this->md_pembayaran->getListJemaahWithPembayaran($param2);

            $no=0;
            $data =array();
            foreach($dt['data'] as $row){
                $id = encrypt($row->pendaftarandetail_id);

                //No registrasi
                    $no_regis = '<small style="font-size:11px"><a href="'.base_url().'administrator/pendaftaran/detail_jemaah/'.$id.'">'.$row->no_registrasi.'</a><span class="pull-right" id="label_jemaah_'.$id.'">';
                //Atribut Jemaah
                    $bc     = AtributJemaah($row,'Kelas Pesawat');
                    $sb     = AtributJemaah($row,'Sharing Bed');
                    $inft   = AtributJemaah($row,'Infant');
                    $progrv = AtributJemaah($row,'Visa');
                    $mahrm  = AtributJemaah($row,'Mahram');
                    $tourled = AtributJemaah($row,'Tour Leader');
                    $atribut = ''.$no_regis.$bc.$sb.$inft.$mahrm.$progrv.$tourled;

                //Tanggal Keberangkatan
                    $tgl_keberangkatan = $row->tgl_keberangkatan ? '<label class="label bg-green-jungle label-sm">'.date('d-M-Y',strtotime($row->tgl_keberangkatan)).'</label>' : '<label class="label label-warning label-sm">Belum Diset</label>';
    
                //Harus bayar
                    $infant              = $row->infant ? $row->infant : 0;
                    $sharing_bed         = $row->sharing_bed ? $row->sharing_bed : 0;
                    $biaya_kelas_pesawat = $row->biaya_kelas_pesawat ? $row->biaya_kelas_pesawat : 0;
                    $harus_bayar         = $row->harus_bayar-($row->infant+$row->sharing_bed)+$row->biaya_kelas_pesawat;
                    $harus_bayar         = ($harus_bayar+$row->total_biaya_tambahan)-$row->total_biaya_pengurang;
                    
                    
                //Total Bayar : total bayar dihitung dari setiap pembayaran yang telah terverifikasi
                    $total_pembayaran = $row->total_pembayaran ? $row->total_pembayaran - $row->total_pengembalian : '-';
                    
                //Pengembalian
                    $refund = $row->total_pengembalian ? '<br><small class="pull-right" style="font-size:10px"><i>(refund '.rupiah_format($row->total_pengembalian).')</i></small>' : '';
                    
                //Sisa Pembayaran
                    $sisa = ($row->sisa_tanpa_tambahan_pengurang+$row->total_biaya_tambahan) - $row->total_biaya_pengurang;
                    $sisa_pembayaran = $sisa ;

                //Status Pembayaran

                    if($row->status_bayar=='Lunas')
                        $status = '<label class="label bg-green-jungle label-sm">Lunas</label>';
                    else if($row->status_bayar=='Belum Lunas')
                        $status = '<label class="label label-danger label-sm">Belum Lunas</label>';
                    else if($row->status_bayar=='Belum Bayar')
                        $status = '<label class="label bg-dark label-sm">Belum Bayar</label>';
                    else
                        $status = 'Error !';


                //Aksi
                    $btn1 = '<li><a href="'.base_url().'administrator/pembayaran/detail_jemaah/'.$id.'/pembayaran"><i class="fa fa-dollar"></i>&nbsp;Pembayaran</a></li>';
                    $btn2 = '<li><a href="'.base_url().'administrator/pembayaran/detail_jemaah/'.$id.'/pengembalian"><i class="fa fa-refresh"></i>&nbsp</i>&nbsp;Pengembalian</a></li>';
                    $btn3 = '<li><a href="'.base_url().'administrator/pendaftaran/detail_jemaah/'.$id.'"><i class="fa fa-user"></i>&nbsp;Detail Jemaah</a></li>';
      

                if($row->jenis_jemaah=='Tour Leader'){
                    $status = '<label class="label bg-yellow label-sm"><i class="fa fa-trophy"></i> Tour Leader</label>';
                    $btn1 = '';
                    $btn2 = '';
                    $th4 = '-';
                    $th5 = '-';
                    $th6 = '-';
                } else {
                    $th4 = 'Rp <span class="pull-right">'.number_format_decimal($harus_bayar).'</span>';
                    $th5 = 'Rp <span class="pull-right">'.number_format_decimal($row->total_pembayaran).'</span>'.$refund;
                    $th6 = 'Rp <span class="pull-right">'.number_format($sisa_pembayaran).'</span>' ;
                }

                $list_btn = $btn1.$btn2.$btn3;
                $aksi = '<div class="btn-group">
                            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                <i class="fa fa-angle-down"></i>
                            </button>
                            <ul class="dropdown-menu" role="menu">'.$list_btn.'</ul>
                        </div>';


                $th1 = ++$no;
                $th2 = $row->nama_jemaah.'<br><small style="font-size:11px">'.$atribut.'</span></small>';
                $th3 = $tgl_keberangkatan;
                $th7 = $status;
                $th8 = $aksi;
                $data[] = gathered_data(array($th1,$th2,$th3,$th4,$th5,$th6,$th7,$th8));
            }
            $dt['data'] = $data;
            echo json_encode($dt);
            die;
        }
        
        $page_data['page_name'] = 'pembayaran';
        $page_data['page_title'] = 'Pembayaran';
        $this->load->view('login/index', $page_data);        
    }
//#------------------------------------------------------------------------------------------------#//  
    public function keberangkatan($param1 = '', $param2 = ''){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        $page_data['show_detail'] = FALSE;
        $page_data['show_jemaah'] = FALSE;
        $page_data['_paketwaktu_id'] = FALSE;

        if($param1 == 'add'){
            $id = decrypt($param2);
            //Kosongkan keberangkatan di pendaftarandetail jika sudah ada yang mengeset tanggal keberangkatan jemaah
                $x = $this->md_keberangkatan->getKeberangkatanByPaketWaktuId($id);
                foreach($x as $row){
                    $data['keberangkatan_id'] = NULL;
                    $this->md_pendaftaran_detail->updatePendaftaranDetailByKeberangkatanId($row->keberangkatan_id,$data);
                }
            //Pisah tanggal yang gabung
                $tgl = explode(',',$this->input->post('tgl_berangkat'));

                if($tgl[0]){
                //Hapus semua, lalu masukan lagi
                    $this->md_keberangkatan->deleteKeberangkatanByPaketWaktuId($id);
                    for($i=0;$i<count($tgl);$i++){
                        $data['paketwaktu_id'] = $id;
                        $data['tgl_keberangkatan'] = dateConvertToDatabase($tgl[$i]);
                        // $data['kuota'] = $this->input->post('kuota');
                        $data['status'] = 1;
                        $this->md_keberangkatan->addKeberangkatan($data);
                    }
                } else {
                    //Hapus semua
                        $this->md_keberangkatan->deleteKeberangkatanByPaketWaktuId($id);                   
                }


            $temp = $this->md_paket_waktu->getPaketWaktuByPaketWaktuIdV2($id);
            
            $this->md_log->addLog(adminLog('Set Tanggal Keberangkatan','Menetapkan tanggal '.$this->input->post('tgl_berangkat').' untuk keberangkatan '.$temp[0]->nama_paket));
            echo json_encode('add_success');
            die;
        }
        else if($param1 == 'add_kuota'){
            $kuota = $this->input->post('kuota');
            $keberangkatan_id = $this->input->post('keberangkatan_id');
            for($i=0;$i<count($kuota);$i++){

                $data['kuota'] = $kuota[$i];
                $id = decrypt($keberangkatan_id[$i]);
                $this->md_keberangkatan->updateKeberangkatan($id,$data);

                $temp = $this->md_keberangkatan->getKeberangkatanByKeberangkatanId($id);
                $this->md_log->addLog(adminLog('Set Kuota Keberangkatan','Menetapkan '.$data['kuota'].' untuk kuota keberangkatan '.date("d-M-Y",strtotime($temp[0]->tgl_keberangkatan))));            
            }
            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'add_tourLead'){
            $kbrngtn = $this->input->post('keberangkatan_id');

            for($j=0;$j<count($kbrngtn);$j++){

                $tourlead = $this->input->post($kbrngtn[$j].'_tour_leader');
                    $temp2='';
                    $keberangkatan_id = decrypt($kbrngtn[$j]);

                    //Hapus semua tourleader sebelumnya
                        $this->md_keberangkatan_tour->deleteKeberangkatanTourByKeberangkatanId($keberangkatan_id);

                if($tourlead){
                    for($i=0;$i<count($tourlead);$i++){
                        $temp = explode('_',$tourlead[$i]);
                        $nama_tour_lead           = $temp[1];
                        $data['tourleader_id']    = decrypt($temp[0]);
                        $data['keberangkatan_id'] = $keberangkatan_id;
                        $data['status']           = 1;
                        $this->md_keberangkatan_tour->addKeberangkatanTour($data);
                        $temp2 = $temp2.$nama_tour_lead.',';
                    }

                    $temp  = $this->md_keberangkatan->getKeberangkatanByKeberangkatanId($keberangkatan_id);
                    $temp2 = substr_replace($temp2, "", -1);
                    $this->md_log->addLog(adminLog('Set Tour Leader Keberangkatan','Menetapkan Tour Leader '.$temp2.' untuk keberangkatan '.date("d-M-Y",strtotime($temp[0]->tgl_keberangkatan))));            
                } 
            }
            echo json_encode('update_success');
            die;
        }          
        else if($param1 == 'SetKeberangkatanPerTanggal'){
            $keberangkatan_id = decrypt($this->input->post('multi_tgl_berangkat'));
            $pendaftaran_id   = $this->input->post('multi_jemaah');

            for($i=0;$i<count($pendaftaran_id);$i++){
                $data['keberangkatan_id'] = $keberangkatan_id;
                $this->md_pendaftaran_detail->updatePendaftaranDetail(decrypt($pendaftaran_id[$i]),$data);
            }

            $temp = $this->md_keberangkatan->getKeberangkatanByKeberangkatanId($keberangkatan_id);
            $this->md_log->addLog(adminLog('Set Keberangkatan Banyak Jemaah','Menetapkan keberangkatan '.date("d-M-Y",strtotime($temp[0]->tgl_keberangkatan)).' kepada banyak jemaah '));

            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'setTglBerangkat'){
            $id = decrypt($param2);

            $data['keberangkatan_id'] = decrypt($this->input->post('keberangkatan_id'));
            $this->md_pendaftaran_detail->updatePendaftaranDetail($id,$data);

            $temp = $this->md_keberangkatan->getKeberangkatanByKeberangkatanId($data['keberangkatan_id']);
            $temp2 = $this->md_pendaftaran_detail->getPendaftaranDetailByPendaftarandetailIdV2($id);
            $this->md_log->addLog(adminLog('Set Keberangkatan Jemaah','Menetapkan keberangkatan '.date("d-M-Y",strtotime($temp[0]->tgl_keberangkatan)).' untuk jemaah '.$temp2[0]->nama_jemaah));

            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'setStatusKeberangkatan'){
            $id = decrypt($param2);

            $data['status_keberangkatan'] = $this->input->post('status_keberangkatan');
            $this->md_pendaftaran_detail->updatePendaftaranDetail($id,$data);

            $temp2 = $this->md_pendaftaran_detail->getPendaftaranDetailByPendaftarandetailIdV2($id);
            $this->md_log->addLog(adminLog('Set Status Keberangkatan Jemaah','Menetapkan status keberangkatan untuk jemaah '.$temp2[0]->nama_jemaah.' menjadi '.$data['status_keberangkatan']));

            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'setStatusKeberangkatanPerTanggal'){
            $paketwaktu_id                = $this->input->post('paketwaktu_id');
            $data['status_keberangkatan'] = $this->input->post('status_keberangkatan');
            $list_keberangkatan           = $this->input->post('multi_tgl');
 
            $y='';
            foreach($list_keberangkatan as $row){
                $temp = explode('_',$row);
                $x[]  = decrypt($temp[0]);
                $y    = $y.$temp[1].', ';
            }
            // echo_array($x);die;
            $this->md_pendaftaran_detail->updatePendaftaranDetailByKeberangkatanId($x,$data);
            $this->md_log->addLog(adminLog('Set Status Keberangkatan Jemaah','Menetapkan status keberangkatan untuk keberangkatan  '.substr($y, 0, -2).' menjadi '.$data['status_keberangkatan']));
            echo json_encode('update_success');
            die;
        }
        else if ($param1 == "getTglBerangkat"){
            $id = decrypt($param2);
            $temp = $this->md_keberangkatan->getKeberangkatanByPaketWaktuId($id);
            if($temp){
                foreach($temp as $row){
                    $keberangkatan_id = encrypt($row->keberangkatan_id);
                    $x[] = date('d-F-Y',strtotime($row->tgl_keberangkatan));
                    $y[] = $keberangkatan_id;
                    $z[] = $row->kuota;
                    

                    $e = $this->md_keberangkatan_tour->getKeberangkatanTourByKeberangkatanId($row->keberangkatan_id);
                    $f[$keberangkatan_id][] = $e;
                      
                }
                $data['tourleader'] = $f;
                $data['data']       = $x;
                $data['id']         = $y;
                $data['kuota']      = $z;
                

                $w = $this->md_tour_leader->getAllTourLeader();
                foreach($w['data'] as $row){
                    $row->tourleader_id = encrypt($row->tourleader_id);
                }
                $data['tourlead'] = $e;
                echo json_encode($data);
            } else {
                echo json_encode("");
            }
            die;
        }
        else if($param1 == 'getDetailJemaah'){
            $id = decrypt($param2);
            
            //List Pembayaran
                $list_pembayaran = $this->md_pembayaran->getPembayaranByJemaah($id);
                foreach($list_pembayaran as $row){
                    $row->jumlah_bayar = rupiah_format($row->jumlah_bayar);
                    $row->tgl_bayar = date('d-M-Y H:i',strtotime($row->tgl_bayar));
                    $row->tgl_bayar_bank = date('d-M-Y',strtotime($row->tgl_bayar_bank));
                }
                $data['pembayaran'] = $list_pembayaran;


            //List Syarat
                $data['syarat'] = $this->md_syarat_status->getSyaratStatusByPendaftarandetailIdV2($id);


            echo json_encode($data);
            die;
        }
        else if($param1 == 'show_jemaah'){
            $id=decrypt($param2);
            $page_data['show_jemaah'] = TRUE;
            //JEmaah yang diambil hanya yang valid dan siap untuk diberangkatkan
            $x = $this->md_pendaftaran_detail->getPendaftaranDetailValidProsesPaketWaktuId($id);
            $x = getMember($x);
            $page_data['jemaah'] = $x;

            $page_data['keberangkatan'] = $this->md_paket_waktu->getPaketWaktuByPaketWaktuIdV2($id);
            $x = $this->md_keberangkatan->getKeberangkatanByPaketWaktuId($id);
            foreach ($x as $row){
                $row->keberangkatan_id  = encrypt($row->keberangkatan_id);
                $row->tgl_keberangkatan = date('d-F-Y',strtotime($row->tgl_keberangkatan));
            }
            $page_data['tgl_berangkat']  = $x;
            $page_data['_paketwaktu_id'] = $param2;
        }

        $page_data['bulan']            = all_bulan_short();
        $page_data['list_tour_leader'] = $this->md_tour_leader->getAllTourLeader();
        $page_data['list_paket']       = $this->md_paket->getAllPaketV2();
        $page_data['page_name']        = 'keberangkatan';
        $page_data['page_title']       = 'Keberangkatan Jemaah';
        $this->load->view('login/index', $page_data);        
    }
//#------------------------------------------------------------------------------------------------#//  
    public function konfirmasi($param1='',$param2=''){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');
        $page_data['show_detail'] = FALSE;

        if($param1=='detail'){
            $konfirmasi_id = decrypt($param2);
            $x = $this->md_konfirmasi->getKonfirmasiById($konfirmasi_id);
            $page_data['show_detail'] = TRUE;
            $page_data['dt_konfirmasi'] = $x;
        }
        else if($param1 == 'setStatusValidasi'){
            $detail = FALSE;
            $verif = $this->input->post('btn_verif');
            if($verif) {
                $status = $verif;
                $detail = TRUE;
            } else {
                $temp = explode('_',$this->input->post('status_validasi'));
                $status = $temp[1];
            }

            $data['status_validasi'] = $status;
            $id = decrypt($this->input->post('konfirmasi_id'));
            $this->md_konfirmasi->updateKonfirmasi($id,$data);

            $temp=$this->md_konfirmasi->getKonfirmasiById($id);
            $this->md_log->addLog(adminLog('Memperbaharui Status Validasi Konfirmasi Pembayaran','Set status "'.$data['status_validasi'].'" untuk Konfirmasi Pembayaran '.$temp[0]->nama));
            if($detail)
                redirect(base_url().'administrator/konfirmasi/detail/'.$param2);
            else{
                echo json_encode('update_success');
                die;
            }
        }
        else if($param1 == 'delete'){
            $id = decrypt($param2);
            $temp = $this->md_konfirmasi->getKonfirmasiById($id);
            $data['status'] = 2;
            $this->md_konfirmasi->updateKonfirmasi($id,$data);
            $this->md_log->addLog(adminLog('Menghapus Konfirmasi Pembayaran','Menghapus Konfirmasi Pembayaran '.$temp[0]->nama));
            echo json_encode('update_success');
            die;
        }

        $page_data['konfirmasi'] = $this->md_konfirmasi->getAllKonfirmasi();
        $page_data['page_name'] = 'konfirmasi';
        $page_data['page_title'] = 'Konfirmasi Pembayaran';
        $this->load->view('login/index', $page_data);        
    }
//#------------------------------------------------------------------------------------------------#//  
    public function room($param1='',$param2='',$param3=''){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        $page_data['jemaah_d']    = 0;
        $page_data['jemaah_t']    = 0;
        $page_data['jemaah_q']    = 0;
        $page_data['max_d']       = 0;
        $page_data['max_t']       = 0;
        $page_data['max_q']       = 0;
        $page_data['show_detail'] = FALSE;

        if($param1=='show_detail'){
            
            $this->session->set_userdata('body_close_sidebar','page-sidebar-menu-closed page-sidebar-closed');
            $this->session->set_userdata('menu_close_sidebar','page-sidebar-menu-closed');

            $keberangkatan_id = decrypt($param2);
            $kota_keberangkatan = decrypt($param3);
            //Data jemaah yang tampil hanya yang terverifikasi dan status keberangkatan nya Dalam Proses atau Sudah berangkat
                $x = $this->md_pendaftaran_detail->getPendaftaranDetailByKeberangkatanIdMember($keberangkatan_id,$kota_keberangkatan);
                $page_data['tgl_keberangkatan']     = date('d-F-Y',strtotime($x[0]->tgl_keberangkatan));
                $page_data['kota_keberangkatan']    =  '('.$x[0]->kode_kota.') '.$x[0]->nama_kota;
                $page_data['keberangkatan_id']      = encrypt($keberangkatan_id);
                $page_data['kota_keberangkatan_id'] = encrypt($kota_keberangkatan);
            //Cek list room tertinggi dari semua keberangkatan
                $D=[1];
                $T=[1];
                $Q=[1];
                $list_d=[];
                $list_t=[];
                $list_q=[];
                foreach($x as $row){
                    $id          = encrypt($row->pendaftarandetail_id);
                    $sex         = $row->jenis_kelamin == 'Pria' ? '<b class="font-dark">M</b>' : '<b class="font-red">F</b>';
                    $sharing_bed = $row->sharing_bed>0 ? '<a class="tooltips" data-placement="top" title="Jemaah Sharing Bed"><i class="fa fa-bed"></i></a> | ' : '';
                    $infant      = $row->infant>0 ? '<a class="tooltips font-yellow-crusta" data-placement="top" title="Jemaah Infant"><i class="fa fa-child"></i></a> | ' : '';
                    $tourled     = $row->jenis_jemaah=='Tour Leader' ? '<a class="tooltips font-yellow" data-placement="top" title="Tour Leader"><i class="fa fa-trophy"></i></a> |' : '';
                    $atribut     = $sex.' | '.$sharing_bed.$infant.$tourled;

                    //Pengambilan angka untuk setiap room pada jemaah yang telah memiliki room
                    switch (substr($row->room,0,1)) {
                        case 'D':
                            $D[]=substr($row->room,1);
                            $list_d[] = array(
                            'id'          =>$id,
                            'nama_jemaah' =>$row->nama_jemaah,
                            'room'        =>$row->room,
                            'room_count'  =>substr($row->room,1),
                            'content'     => '<li class="dd-nochildren dd-item" data-nama="'.$row->nama_jemaah.'" data-id="'. $id .'" title="'.$row->nama_jemaah.'">
                                                <div class="dd-handle tooltips"  style="text-overflow: ellipsis;overflow: hidden;white-space: nowrap;" data-placement="bottom" title="['.$row->nama_paket.'] '.$row->nama_jemaah.' memilih kelas '.$row->kelas.'">
                                                    <label>'.$atribut.$row->nama_jemaah.'</label><label class="pull-right"></label>;
                                                </div>
                                            </li>'
                            );
                            break;
                        case 'T':
                            $T[]=substr($row->room,1);
                            $list_t[] = array(
                            'id'          =>$id,
                            'nama_jemaah' =>$row->nama_jemaah,
                            'room'        =>$row->room,
                            'room_count'  =>substr($row->room,1),
                            'content'     => '<li class="dd-nochildren dd-item" data-nama="'.$row->nama_jemaah.'" data-id="'. $id .'" title="'.$row->nama_jemaah.'">
                                                <div class="dd-handle tooltips"  style="text-overflow: ellipsis;overflow: hidden;white-space: nowrap;" data-placement="bottom" title="['.$row->nama_paket.'] '.$row->nama_jemaah.' memilih kelas '.$row->kelas.'">
                                                    <label>'.$atribut.$row->nama_jemaah.'</label><label class="pull-right"></label>;
                                                </div>
                                            </li>'
                            );
                            break;
                        case 'Q':
                            $Q[]=substr($row->room,1);
                            $list_q[] = array(
                            'id'          =>$id,
                            'nama_jemaah' =>$row->nama_jemaah,
                            'room'        =>$row->room,
                            'room_count'  =>substr($row->room,1),       
                            'content'     => '<li class="dd-nochildren dd-item" data-nama="'.$row->nama_jemaah.'" data-id="'. $id .'" title="'.$row->nama_jemaah.'">
                                                <div class="dd-handle tooltips"  style="text-overflow: ellipsis;overflow: hidden;white-space: nowrap;" data-placement="bottom"  title="['.$row->nama_paket.'] '.$row->nama_jemaah.' memilih kelas '.$row->kelas.'">
                                                    <label>'.$atribut.$row->nama_jemaah.'</label><label class="pull-right"></label>;
                                                </div>
                                            </li>'
                            );
                            break;
                    }
                }

            $page_data['jemaah_d'] = json_encode($list_d);
            $page_data['jemaah_t'] = json_encode($list_t);
            $page_data['jemaah_q'] = json_encode($list_q);
            $page_data['max_d'] = max($D);
            $page_data['max_t'] = max($T);
            $page_data['max_q'] = max($Q);

            $y = $this->md_pendaftaran_detail->getPendaftaranDetailByKeberangkatanIdMember($keberangkatan_id,$kota_keberangkatan,TRUE);
            $page_data['show_detail'] = TRUE;
            $page_data['jemaah_room'] = $y;
            $page_data['total_room'] = $this->md_pendaftaran_detail->getTotalRoomJemaahByKeberangkatanIdByKotaBerangkat($keberangkatan_id,$kota_keberangkatan);
        } 
        else if($param1=='update'){
            $list = $this->input->post('list');
            echo_array($list);
            die;
        }
        else if($param1 == 'set'){
            $id = decrypt($param2);
            $data['room'] = $this->input->post('room') ? $this->input->post('room') : null;
            $this->md_pendaftaran_detail->updatePendaftaranDetail($id,$data);
            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'export'){
            $keberangkatan_id = decrypt($param2);
            $kota_keberangkatan = decrypt($param3);
            $rows = array();
            $header = ['No Registrasi','Nama','Gender','Room'];
            $dt = $this->md_pendaftaran_detail->getPendaftaranDetailByKeberangkatanIdByKotaKeberangkatan($keberangkatan_id,$kota_keberangkatan);
            foreach($dt as $row){
                $x = array();
                $x[] = $row->no_registrasi;
                $x[] = $row->nama_jemaah;
                $x[] = $row->jenis_kelamin == 'Pria' ? 'M' : 'F';
                $x[] = $row->room ? $row->room : '-' ;
                $rows[]= $x;
            }

            $ttl = $this->md_pendaftaran_detail->getTotalRoomJemaahByKeberangkatanIdByKotaBerangkat($keberangkatan_id,$kota_keberangkatan);
            $rows[]= array();
            $x = array();
            $x[] = 'Total : ';
            $x[] = $ttl[0]->total_q.' Quad';
            $x[] = $ttl[0]->total_t.' Triple';
            $x[] = $ttl[0]->total_d.' Double';
            $rows[]= $x;
            // echo_array($rows);die;
            // echo_array($rows);die;
            $this->exportExcel($header,$rows,'list_room');
            // $query = $this->db->get('log');
            
            die;
        }



        $page_data['list_keberangkatan'] = $this->md_keberangkatan->getAvailableKeberangkatanJemaah();
        $page_data['page_name'] = 'room';
        $page_data['page_title'] = 'Room Management';
        $this->load->view('login/index', $page_data);        
    }
//#------------------------------------------------------------------------------------------------#//  
    public function sms($param1='',$param2='',$param3=''){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        $page_data['show_detail'] = FALSE;

        if($param1 == 'send'){
            $error = [];
            $success = [];
            $isi                   = $this->input->post('isi_sms');
            $list_keberangkatan_id = $this->input->post('keberangkatan_id');
            $list_no_jemaah        = $this->input->post('no_hp');

            //Lakukan pengecekan isi SMS
                if($isi=='')
                    $error[] = 'Isi SMS tidak boleh kosong !';

            //Pengambilan jemaah berdasarkan paket dan keberangkatan
                if(count($list_keberangkatan_id)>0){
                    foreach($list_keberangkatan_id as $row){
                        $temp = explode('_',$row);
                        $paket_id =  decrypt($temp[0]);
                        $keberangkatan_id = decrypt($temp[1]);
                        $dt = $this->md_pendaftaran_detail->getPendaftaranDetailByPaketIdByKeberangkatanId($paket_id,$keberangkatan_id);
                        foreach($dt as $dt){

                            if(strlen($dt->no_hp)<9 || !is_numeric($dt->no_hp))
                                $error[] = 'No Hp. <b>'.$dt->nama_jemaah.' ('.$dt->no_hp.')</b> tidak valid !';     
                            else 
                                $list_no_jemaah[] = $dt->no_hp;
                            
                            if($dt->no_hp_2){
                                if(strlen($dt->no_hp_2)<9 || !is_numeric($dt->no_hp_2))
                                    $error[] = 'No Hp. ke 2 <b>'.$dt->nama_jemaah.' ('.$dt->no_hp_2.')</b> tidak valid !';   
                                else 
                                    $list_no_jemaah[] = $dt->no_hp_2;
                            }
                            
                        }
                    }
                }

            //Pengecekan
            if(count($list_no_jemaah)>0){
                foreach($list_no_jemaah as $row){
                    if(strlen($row)<9 || !is_numeric($row))
                        $error[] = 'No Hp. <b>'.$row.'</b> tidak valid !';                        
                }
            } else {
                $error[] = 'No tujuan tidak boleh kosong !';
            }    


            if(count($error)>0){
                $data['error'] = $error;
            } else {
                $data['success'] = 'SMS siap dikirim, namun fitur berlum tersedia';
            }
            // echo_array($isi);
            // echo_array($list_no_jemaah);
            // echo_array($list_tgl_berangkat);

            echo json_encode($data);
            die;
        } 
        else if($param1 == 'pagination'){
            //List yang mucul hanya jemaah yang terverfikasi
                $dt = $this->md_pendaftaran_detail->getListJemaah($param2);

            $no=0;
            $list_bulan = all_bulan();
            $data =array();
            foreach($dt['data'] as $row){
                $id = encrypt($row->pendaftarandetail_id);

                if($row->jenis_paket=='Haji'){
                    $paket = '';
                    $tgl_keberangkatan = $row->tahun_est_berangkat;
                } else {
                    $paket = $list_bulan[$row->bulan_berangkat].' '.$row->tahun_berangkat;
                }
                    //No registrasi
                        $no_regis = '<small style="font-size:11px"><a href="'.base_url().'administrator/pendaftaran/detail_jemaah/'.$id.'" target="_blank">'.$row->no_registrasi.'</a><span class="pull-right" id="label_jemaah_'.$id.'">';
                        
                    //Atribut Jemaah
                        $bc      = AtributJemaah($row,'Kelas Pesawat');
                        $sb      = AtributJemaah($row,'Sharing Bed');
                        $inft    = AtributJemaah($row,'Infant');
                        $progrv  = AtributJemaah($row,'Visa');
                        $mahrm   = AtributJemaah($row,'Mahram');
                        $tourled = AtributJemaah($row,'Tour Leader');
                        $atribut = $no_regis.'<br>'.$bc.$sb.$inft.$mahrm.$progrv.$tourled;


  
                $th1 = '<input type="checkbox" id="jemaah_'.$id.' value='.$row->pendaftarandetail_id.' name="no_hp_jemaah[]">';
                $th2 = $row->nama_jemaah;
                $th3 = $row->nama_paket ? $row->nama_paket.' <br><small>'.$paket.'</small>' : '-';
                $th4 = $row->no_hp;
                $th5 = $row->no_hp_2 ? $row->no_hp_2 : '-' ;
                $th6 = $atribut;
                $data[] = gathered_data(array($th1,$th2,$th3,$th4,$th5,$th6));
            }
            $dt['data'] = $data;
            echo json_encode($dt);
            die;
        } 
        else if($param1 == 'paginationTglKeberangkatan'){
            $dt = $this->md_keberangkatan->getLatestStatusSeat_pagination();
            $no=0;
            $data =array();
            $listbulan=all_bulan();
            foreach($dt['data'] as $row){
                $id = encrypt($row->keberangkatan_id);  
                $kuota = $row->kuota > 0 ? $row->kuota : 'Belum diset';
                $terisi = $kuota > 0 ? "<span class='label label-sm bg-green-jungle'>".($row->kuota-$row->terisi)."</span>" : '-';
                
                $th1 = '<input type="checkbox" id="keberangkatan_'.$id.' value='.$id.' name="list_keberangkatan[]">';
                $th2 = $row->nama_paket;
                $th3 = encrypt($row->paket_id).'_'.$id;
                $th4 = date('d-F-Y',strtotime($row->tgl_keberangkatan));
                $th5 = '';
                $th6 = $row->terisi.' Jemaah';
                $th7 = '';
                $th8 = '';
                $data[] = gathered_data(array($th1,$th2,$th3,$th4,$th5,$th6,$th7,$th8));
            }
            $dt['data'] = $data;
            echo json_encode($dt);
            die;
        }

        $page_data['page_name'] = 'sms';
        $page_data['page_title'] = 'SMS Blast';
        $this->load->view('login/index', $page_data);        
    }
//#------------------------------------------------------------------------------------------------#//  
    public function transaksi($param1='',$param2=''){
        if(!$this->session->userdata('login_type') || $this->session->userdata('login_type')=='Perwakilan Daerah')
            redirect(base_url().'rwhgate');

        $page_data['show_form'] = FALSE;
        $page_data['show_detail'] = FALSE;
        $page_data['list_barang'] = FALSE;

        if($param1 == 'add'){

            $bkantor_id = $this->input->post('bkantor_id');
            $qty        = $this->input->post('qty');
            if(count($bkantor_id)==0){
                $this->session->set_flashdata('err_add','Pastikan form transaksi diisi dengan benar !');
                redirect(base_url().'administrator/transaksi/form/');
            }

            $jenis_faktur = $this->input->post('jenis_faktur');
            if($jenis_faktur == 'Masuk')
                $data['no_faktur']    = $this->input->post('no_faktur');    
            else{
                //Cek Sequence No Faktur Barang Keluar
                    $x = getLatestSequence('Keluar');
                    $data['seq']       =  $x['new_seq'];
                    $data['no_faktur'] =  'BK'.date('y').date('m').$x['new_seq'];    
            }

            $data['jenis_faktur']    = $jenis_faktur;
            $data['tgl_transaksi']   = date("Y-m-d H:i");
            $data['pengguna_id']     = $this->session->userdata('pengguna_id');
            $data['keterangan']      = $this->input->post('keterangan');
            $data['total_qty']       = $this->input->post('total_qty');
            $data['kantorcabang_id'] = decrypt($this->input->post('kantorcabang_id'));
            $data['status']          = 1;
            $faktur_id = $this->md_transaksi->addFaktur($data);

            if($faktur_id){
                $jns_faktur = $data['jenis_faktur'];
                for($i=0;$i<count($bkantor_id);$i++){
                    $id = decrypt($bkantor_id[$i]);
                    $brg       = $this->md_barang_kantor->getBarangKantorById($id);
                    $stok_awal = $brg[0]->stok_akhir;

                    //Update jumlah stok di tabel barang_kantor. Jika masuk, tambahkan. Jika keluar, kurangkan
                        $stok_akhir = $jns_faktur == 'Masuk' ? $stok_awal + $qty[$i] : $stok_awal - $qty[$i];
                        $data3['stok_akhir'] = $stok_akhir;
                        $this->md_barang_kantor->updateBarangKantorId($id,$data3);

                    //Input ke barang_transaksi
                        $data2['stok_awal']   = $stok_awal;
                        $data2['stok_akhir']  = $stok_akhir;
                        $data2['bkantor_id']  = $id;
                        $data2['jenis_trans'] = $jns_faktur;
                        $data2['qty']         = $qty[$i];
                        $data2['pengguna_id'] = $this->session->userdata('pengguna_id');
                        $data2['faktur_id']   = $faktur_id;
                        $data2['status']      = 1;
                        $this->md_transaksi->addBarang_Transaksi($data2);

                    //Log
                        $this->md_log->addLog(adminLog('Transaksi Barang',$jns_faktur.' '.$data2['qty'].' '.$brg[0]->nama_barang.'. Stok awal : '.$stok_awal.' , Stok akhir : '.$stok_akhir));
                }
            }
  
            $this->session->set_flashdata('add_success','Transaksi Barang '.$jns_faktur.' berhasil disimpan');
            redirect(base_url().'administrator/transaksi');
        }
        else if($param1 == 'delete'){
            $faktur_id = decrypt($this->input->post('faktur_id'));
            $jenis_faktur = $this->input->post('jenis_faktur');

            $x = $this->md_transaksi->getFakturById($faktur_id);
            $w = $this->md_transaksi->getBarangTransaksiByFakturId($faktur_id);

            foreach($w as $row){
                //Update setiap barang 
                $qty       = $row->qty;
                $r         = $this->md_barang_kantor->getBarangKantorByBarangIdByKantorcabangId($row->barang_id,$row->kantorcabang_id);
                $stok_awal = $r[0]->stok_akhir;

                if($jenis_faktur == 'Masuk'){
                    $data['stok_akhir'] = $stok_awal - $qty;
                } else {
                    $data['stok_akhir'] = $stok_awal + $qty;
                }
                $this->md_barang_kantor->updateBarangKantorbyBarangIdByKantorcabangId($row->barang_id,$row->kantorcabang_id,$data);
                $this->md_log->addLog(adminLog('Membatalkan Transaksi','Membatalkan Transaksi '.$jenis_faktur.' '.$row->nama_barang.' pada faktur '.$row->no_faktur.'. Stok awal :'.$stok_awal.', Stok akhir : '.$data['stok_akhir']));            
            }


            $data2['status'] = 2;
            $this->md_transaksi->updateBarangTransaksiByFakturId($faktur_id,$data2);
            $this->md_transaksi->updateFaktur($faktur_id,$data2);
            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'detail'){
            $faktur_id                = decrypt($param2);
            $page_data['transaksi']   = $this->md_transaksi->getBarangTransaksiByFakturId($faktur_id);
            $page_data['show_detail'] = TRUE;
        }
        else if($param1 == 'form'){
            $page_data['list_barang']   = $this->md_barang->getAllBarang();
            $page_data['kantor_cabang'] = getListKantorCabang($this->session->userdata('login_type'));
            $page_data['show_form']     = TRUE;
        }
        else if($param1 == 'pagination'){
            //List yang mucul hanya jemaah yang terverfikasi
            $kantorcabang_id = FALSE;
            if($this->session->userdata('login_type')!='Administrator' && $this->session->userdata('login_type')!='Keuangan'){
                $kantorcabang_id = $this->session->userdata('id_kantor');
            }
            $dt = $this->md_transaksi->getAllFaktur($kantorcabang_id);
       
            $no=0;
            $data =array();
            foreach($dt['data'] as $row){
                $id = encrypt($row->faktur_id);

                $jenis_faktur  = $row->jenis_faktur == 'Masuk' ? '<span class="font-green-jungle"><i class="fa fa-download"></i> Barang Masuk</span>' : '<span class="font-red"><i class="fa fa-upload"></i> Barang Keluar</span>';
                $tgl_transaksi = '<i class="fa fa-calendar"></i> '.date('d-M-Y H:i',strtotime($row->tgl_transaksi));
                $total_qty     = '<i class="icon-social-dropbox"></i> '.$row->total_qty.' Barang';
                $aksi          = '<div class="btn-group">
                                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="'.base_url().'administrator/transaksi/detail/'.$id.'" target="_blank"><i class="fa fa-search"></i>&nbsp;Detail</a></li>
                                            <li><a href="javascript:;" onClick="edit_function(\'delete\','. $id.',\''.$row->jenis_faktur.'\')"><i class="fa fa-times"></i>&nbsp;Batalkan</a></li>
                                        </ul>
                                    </div>';

                $th1 = ++$no;
                $th2 = '<a href="'.base_url().'administrator/transaksi/detail/'.$id.'" target="_blank">'.$row->no_faktur.'</a>';
                $th3 = $jenis_faktur;
                $th4 = $total_qty;
                $th5 = $tgl_transaksi;
                $th6 = $row->nama_lengkap.' <small>('.$row->nama_cabang.')</small>';
                $th7 = $aksi;
                $data[] = gathered_data(array($th1,$th2,$th3,$th4,$th5,$th6,$th7));
            }
            $dt['data'] = $data;
            echo json_encode($dt);
            die;
        }
        else if($param1 == 'pagination_history'){
            //List yang mucul hanya jemaah yang terverfikasi
            $kantorcabang_id = FALSE;
            if($this->session->userdata('login_type')!='Administrator' && $this->session->userdata('login_type')!='Keuangan'){
                $kantorcabang_id = $this->session->userdata('id_kantor');
            }

            $dt = $this->md_transaksi->getAllBarangTransaksi($kantorcabang_id);

            $no=0;
            $data =array();
            foreach($dt['data'] as $row){
                $id = encrypt($row->faktur_id);
                $jenis_faktur = $row->jenis_faktur == 'Masuk' ? '<span class="font-green-jungle"><i class="fa fa-download"></i> Barang Masuk</span>' : '<span class="font-red"><i class="fa fa-upload"></i> Barang Keluar</span>';
                $tgl_transaksi = '<i class="fa fa-calendar"></i> '.date('d-M-Y H:i',strtotime($row->tgl_transaksi));
                // $total_qty = '<i class="icon-social-dropbox"></i> '.$row->total_qty.' Barang';

                $th1 = ++$no;
                $th2 = $row->nama_barang;
                $th3 = $jenis_faktur;
                $th4 = $row->nama_cabang;
                $th5 = $row->qty;
                $th6 = $row->stok_awal;
                $th7 = $row->stok_akhir;
                $th8 = $tgl_transaksi;
                $th9 = '<a href="'.base_url().'administrator/transaksi/detail/'.$id.'" target="_blank">'.$row->no_faktur.'</a>';
                $data[] = gathered_data(array($th1,$th2,$th3,$th4,$th5,$th6,$th7,$th8,$th9));
            }
            $dt['data'] = $data;
            echo json_encode($dt);
            die;
        }
        $page_data['page_name'] = 'transaksi';
        $page_data['page_title'] = 'Transaksi';
        $this->load->view('login/index', $page_data);        
    }
//#------------------------------------------------------------------------------------------------#//  
    public function member($param1='',$param2=''){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        if($param1 == 'add'){
            //Add media First
                $nama = $this->input->post('nama_lengkap');
                $media_id = ImageUploadToMedia('media','avatar','avatar_member');
                if($media_id != NULL)
                    $data['media_id'] = $media_id;

            $data['nama_lengkap'] = $this->input->post('nama_lengkap');
            $data['email']        = $this->input->post('email');
            $data['no_hp']        = $this->input->post('no_hp');
            $data['tgl_lahir']    = date('Y-m-d',strtotime($this->input->post('tgl_lahir')));
            $data['alamat']       = $this->input->post('alamat');
            $data['password']     = $this->input->post('password');
            $data['kota']         = $this->input->post('kota');
            $data['registrasi']   = 'Web';
            $data['ip_addr']      = $_SERVER['REMOTE_ADDR'];
            $data['media_id']     = $media_id;
            $data['tgl_regis']    = date('Y-m-d H:i:s');
            $data['status_aktif'] ='aktif';
            $data['status']       = 1;
            $this->md_member->addMember($data);
            $this->md_log->addLog(adminLog('Menambah Member','Menambah member bernama '.$data['nama_lengkap']));
            echo json_encode('add_success');
            die;
        }

        if($param1 == 'update'){
            $nama = $this->input->post('nama_lengkap');
            $id = decrypt($this->input->post('member_id'));

            $media_id = ImageUploadToMedia('media','avatar','avatar_member');
            if($media_id != NULL){
                $data['media_id'] = $media_id;
                //Delete Old Media and Old Media Detail
                    $temp = $this->md_member->getMemberByMemberId($id);
                    deleteMedia($temp[0]->media_id);
            }
            
            $data['nama_lengkap'] = $this->input->post('nama_lengkap');
            $data['email'] = $this->input->post('email');
            $data['no_hp'] = $this->input->post('no_hp');
            $data['tgl_lahir'] = date('Y-m-d',strtotime($this->input->post('tgl_lahir')));
            $data['alamat'] = $this->input->post('alamat');
            $data['kota'] = $this->input->post('kota');
            $this->md_member->updateMember($id,$data);
            $this->md_log->addLog(adminLog('Memperbaharui Member','Memperbaharui data member '.$data['nama_lengkap']));
            echo json_encode('update_success');
            die;
        }

        if($param1 == 'update_status'){

            $data['status_aktif'] = $this->input->post('status_aktif');
            $member_id = decrypt($param2);

            $this->md_member->updateMember($member_id,$data);
            echo json_encode('update_success');
            die;
        }
        
        if($param1 == 'delete'){
            $id = decrypt($param2);
            
            //Delete Media and Media Detail
                $temp = $this->md_member->getMemberByMemberId($id);
                deleteMedia($temp[0]->media_id);

            $this->md_log->addLog(adminLog('Menghapus Member','Menghapus member '.$temp[0]->nama_lengkap));

            $v = $this->md_pendaftaran->getPendaftaranByMemberId($id);
            if($v){
                $pendaftaran_id = $v[0]->pendaftaran_id;
                $this->md_pendaftaran->updatePendaftaran($pendaftaran_id,$data);
                
                $w = $this->md_pendaftaran_detail->getPendaftaranDetailByPendaftaranId($pendaftaran_id);
                $pendaftarandetail_id = $w[0]->pendaftarandetail_id;
                $this->md_pendaftaran_detail->updatePendaftaranDetailByPendaftaranId($pendaftaran_id,$data);

                $cek = $this->md_pembayaran->getPembayaranByJemaah($pendaftarandetail_id);
                if($cek){
                    $this->md_pembayaran->updatePembayaranByPendaftaranDetailId($pendaftarandetail_id,$data);
                }
                
                $cek = $this->md_syarat_status->getSyaratStatusByPendaftarandetailId($pendaftarandetail_id);
                if($cek){
                    $this->md_syarat_status->updateAllSyaratStatusByJemaah($data,$pendaftarandetail_id);
                }
                
            }

            $data['status'] = 2;
            $this->md_member->updateMember($id,$data);
            echo json_encode('update_success');
            die;
        }

        if($param1 == 'getListJemaah'){
            $id = decrypt($param2);
            $x = $this->md_pendaftaran_detail->getPendaftaranDetailByMemberId($id);
            echo json_encode($x);
            die;
        }

        $x = $this->md_member->getAllMember();
        for($i=0;$i<count($x);$i++){
            $y = $this->md_pendaftaran_detail->getPendaftaranDetailByMemberId($x[$i]->member_id);
            $x[$i]->total_jemaah = count($y);
            $x[$i]->tgl_regis = date('d-M-Y H:i:s',strtotime($x[$i]->tgl_regis));
            $x[$i]->tgl_lahir = date('d-M-Y',strtotime($x[$i]->tgl_lahir));
            $cek = $this->md_media_detail->getMediaDetailByMediaId($x[$i]->media_id);
            if($cek)
                $x[$i]->avatar = $cek[0]->media_link;
            else
                $x[$i]->avatar = 'assets/frontend/img/no-avatar.png';
        }

        $page_data['member'] = $x;
        $page_data['page_name'] = 'member';
        $page_data['page_title'] = 'Member';
        $this->load->view('login/index', $page_data);        
    } 
//#------------------------------------------------------------------------------------------------#//  
    public function laporan_transaksi($param1=""){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        if($param1=='cetak'){
            $kantorcabang_id          = $this->input->post('kantorcabang_id') != 'Semua Kantor' ? decrypt($this->input->post('kantorcabang_id')) : 'Semua Kantor';
            $barang_id                = $this->input->post('barang_id') != 'Semua Barang' ? decrypt($this->input->post('barang_id')) : 'Semua Barang' ;
            $page_data['output']      = $this->input->post('output');
            $page_data['dt']          = $this->md_transaksi->getLaporanRiwayatTransaksiBarang($kantorcabang_id,$barang_id);
            
            $dt_kantor = $kantorcabang_id != 'Semua Kantor' ? $this->md_kantor_cabang->getKantorCabangById($kantorcabang_id) : 'Semua Kantor';
            $page_data['nama_cabang'] = $dt_kantor != 'Semua Kantor' ? $dt_kantor[0]->nama_cabang : $dt_kantor;

            $dt_barang = $barang_id != 'Semua Barang' ? $this->md_barang->getBarangById($barang_id) : 'Semua Barang';
            $page_data['nama_barang'] = $dt_barang != 'Semua Barang' ? $dt_barang[0]->nama_barang : $dt_barang;
            $this->load->view('login/page/cetak_laporan_transaksi', $page_data); 
        } else {
            $page_data['kantor']     = getListKantorCabang($this->session->userdata('login_type'));
            $page_data['page_name']  = 'laporan_transaksi';
            $page_data['page_title'] = 'Laporan Transaksi Barang';
            $this->load->view('login/index', $page_data);        
        }
    }
//#------------------------------------------------------------------------------------------------#//  
    public function laporan_barang($param1=""){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        if($param1=='cetak'){
            $kantorcabang_id          = $this->input->post('kantorcabang_id') != 'Semua Kantor' ? decrypt($this->input->post('kantorcabang_id')) : 'Semua Kantor';
            $page_data['output']      = $this->input->post('output');
            $page_data['dt']          = $this->md_barang_kantor->getBarangKantorByKantorcabangId($kantorcabang_id);
            $page_data['dt_kantor']   = $this->md_kantor_cabang->getKantorCabangById($kantorcabang_id);
            $page_data['nama_cabang'] = $kantorcabang_id != 'Semua Kantor' ? $page_data['dt_kantor'][0]->nama_cabang : 'Semua Kantor';
            $this->load->view('login/page/cetak_laporan_barang', $page_data); 
        } else {
            $page_data['kantor']     = getListKantorCabang($this->session->userdata('login_type'));
            $page_data['page_name']  = 'laporan_barang';
            $page_data['page_title'] = 'Laporan Barang';
            $this->load->view('login/index', $page_data);        
        }
    }
//#------------------------------------------------------------------------------------------------#//  
    public function laporan_jemaah($param1=""){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        $page_data['tour_leader'] = FALSE;
        if($param1=='cetak'){
            $paket_id                  = $this->input->post('paket_id');
            $jenis                     = $this->input->post('jenis');
            $status_keberangkatan      = $this->input->post('status_keberangkatan');
            $page_data['output']       = $this->input->post('output');
            $page_data['total_header'] = 0;
            //Tampilkan data jemaah per Paket
                if($jenis=='paket'){
                    
                    if($paket_id!="Semua Paket"){
                        $paket_id = decrypt($paket_id);
                        $x        = $this->md_paket->getPaketByPaketId($paket_id);
                        $page_data['nama_paket'] = $x[0]->nama_paket;

                        if($x[0]->jenispaket_id == 1 || $x[0]->jenispaket_id == 3){
                            $page_data['dt'] = $this->md_pendaftaran_detail->getPendaftaranDetailByPaketId($paket_id,$status_keberangkatan);
                        }
                        else if($x[0]->jenispaket_id == 2){
                            $page_data['dt'] = $this->md_pendaftaran_detail->getPendaftaranDetailHajiByPaketId($paket_id);
                        }

                    } else {
                        $page_data['nama_paket'] = "Semua Paket";
                        $page_data['dt'] = $this->md_pendaftaran_detail->getPendaftaranDetailByPaketId("Semua Paket",$status_keberangkatan);
                    }

                    
                    $page_data['tgl_akhir'] = date('Y-m-d',strtotime($this->input->post('tgl_akhir')));
                } 

            //Tampilkan data jemaah per tanggal keberangkatan
                else {
                    $tgl_keberangkatan        = date('Y-m-d',strtotime($this->input->post('tgl_keberangkatan')));
                    $page_data['dt']          = $this->md_pendaftaran_detail->getPendaftaraDetailByTglKeberangkatan($tgl_keberangkatan);
                    $page_data['tour_leader'] = $this->md_keberangkatan_tour->getKeberangkatanTourByTglKeberangkatan($tgl_keberangkatan);
                    $page_data['nama_paket']  = "Keberangkatan ".date('d-F-Y',strtotime($this->input->post('tgl_keberangkatan')));
                }

            //Menghitung header table per pembayaran
                $total_header_b = 0;
                $total_header_k = 0;
                foreach($page_data['dt'] as $row){
                    $b = 0;
                    $k = 0;
                    $z = $this->md_pembayaran->getPembayaranByJemaah($row->pendaftarandetail_id);
                    if($z){
                        foreach($z as $row2){
                            if($row2->jenis_transaksi == 'Pembayaran')
                                $b++;
                            if($row2->jenis_transaksi == 'Pengembalian')
                                $k++;
                        }
                        $total_header_b = $total_header_b < $b ? $b : $total_header_b;
                        $total_header_k = $total_header_k < $k ? $k : $total_header_k;
                    }
                }
            $page_data['total_header_b'] = $total_header_b;
            $page_data['total_header_k'] = $total_header_k;

            $this->load->view('login/page/cetak_laporan_jemaah', $page_data); 
        } else {
            $page_data['paket']      = $this->md_paket->getAllPaket();
            $page_data['page_name']  = 'laporan_jemaah';
            $page_data['page_title'] = 'Laporan Jemaah';
            $this->load->view('login/index', $page_data);        
        }
    }
//#------------------------------------------------------------------------------------------------#//  
    public function laporan_pendaftaran($param1=""){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        if($param1=='cetak'){
            $page_data['tgl_awal'] = date('Y-m-d',strtotime($this->input->post('tgl_awal')));
            $page_data['tgl_akhir'] = date('Y-m-d',strtotime($this->input->post('tgl_akhir')));
            $page_data['output'] = $this->input->post('output');
            $page_data['dt'] = $this->md_pendaftaran_detail->getAllPendaftaranDetailV2($page_data['tgl_awal'],$page_data['tgl_akhir']);
            $this->load->view('login/page/cetak_laporan_pendaftaran', $page_data); 
        } else {
            $page_data['tahun'] = $this->md_pendaftaran->getAllPendaftarTahun();
            $page_data['page_name'] = 'laporan_pendaftaran';
            $page_data['page_title'] = 'Laporan Pendaftaran';
            $this->load->view('login/index', $page_data);        
        }
    }
//#------------------------------------------------------------------------------------------------#//  
    public function laporan_pembayaran($param1=""){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        if($param1=='cetak'){
            $tgl_awal  = date('Y-m-d',strtotime($this->input->post('tgl_awal')));
            $tgl_akhir = date('Y-m-d',strtotime($this->input->post('tgl_akhir')));
            $paket     = $this->input->post('paket_id') != 'Semua Paket' ? decrypt($this->input->post('paket_id')) : 'Semua Paket';
            $jenis_transaksi = $this->input->post('jenis_transaksi');

            $page_data['dt']        = $this->md_pembayaran->getAllPembayaran($tgl_awal,$tgl_akhir,$paket,$jenis_transaksi);
            $page_data['output']    = $this->input->post('output');
            $page_data['tgl_awal']  = $tgl_awal;
            $page_data['tgl_akhir'] = $tgl_akhir;

            $this->load->view('login/page/cetak_laporan_pembayaran', $page_data); 
        } else {
            $page_data['paket']      = $this->md_paket->getAllPaket();
            $page_data['page_name']  = 'laporan_pembayaran';
            $page_data['page_title'] = 'Laporan Pembayaran';
            $this->load->view('login/index', $page_data);        
        }      
    }
//#------------------------------------------------------------------------------------------------#//  
    public function notifikasi($param1 = ''){
        if (!$this->session->userdata('login_type'))
            redirect(base_url(), 'refresh');

        switch ($param1) {
            case 'baca':
                $id = decrypt($this->input->post('notifikasi_id'));
                $link = $this->input->post('link');
                $data['baca']='1';
                $this->md_notifikasi->updateNotif($id,$data);
                echo json_encode('update_success');
                die;        
                break;
            case 'read_all':
                $id=$this->session->userdata("pengguna_id");
                $data['baca']='1';
                $this->md_notifikasi->updateNotifReadAllByPengguna($id,$data);
                redirect(base_url().'rwhgate');
                die;        
                break;
            case 'cek_notif':
                $data = $this->md_notifikasi->getAllNotifBelumBaca();
                $new_id = array();
                $i=0;
                foreach($data as $dt){
                    $data[$i]->notifikasi_id =  encrypt($dt->notifikasi_id);
                    $i++;
                }

                echo json_encode($data);
                die;        
                break;
            case 'pagination':
                $dt = $this->md_notifikasi->getNotifbyPengguna($this->session->userdata('pengguna_id'));
                $no=0;
                $data =array();
                foreach($dt['data'] as $row){
                    $baca = $row->baca == 0 ? '<button class="btn btn-xs blue">Read</button>' : '<button class="btn btn-xs red">Unread</button>';
                    $th1 = ++$no;
                    $th2 = '<a href="'.$row->link.'">'.$row->judul.'</a>';
                    $th3 = $row->keterangan;
                    $th4 = '<i class="fa fa-clock-o"></i> '.date('d-M-Y H:i',strtotime($row->tgl));
                    $th5 = $baca;
                    $data[] = gathered_data(array($th1,$th2,$th3,$th4,$th5));
                }
                $dt['data'] = $data;
                echo json_encode($dt);
                die;                          
        }
                               
        $page_data['notifikasi'] = $this->md_notifikasi->getNotifbyPengguna($this->session->userdata('pengguna_id'));
        $page_data['page_name'] = 'notifikasi';
        $page_data['page_title'] = 'Notifikasi';
        $this->load->view('login/index', $page_data);
    }   
//#------------------------------------------------------------------------------------------------#//  
    public function log($param1=""){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        if($param1 == 'pagination'){
            $dt = $this->md_log->getAllLogByLoginType($this->session->userdata('login_type'));
            $no=0;
            $data =array();
            foreach($dt['data'] as $row){
                $th1 = ++$no;
                $th2 = $row->nama_lengkap;
                $th3 = $row->jenis_aksi;
                $th4 = $row->keterangan;
                $th5 = '<i class="fa fa-clock-o"></i> '.date('d-M-Y H:i',strtotime($row->tgl));
                $data[] = gathered_data(array($th1,$th2,$th3,$th4,$th5));
            }
            $dt['data'] = $data;
            echo json_encode($dt);
            die;
        }   
        else if($param1 == 'export'){
            $count = 0;
            $header = [
                'No',
                'Pengguna',
                'Jenis',
                'Aksi',
                'Tanggal'
            ];
            $dt = $this->md_log->getAllLogByLoginType($this->session->userdata('login_type'));
            foreach($dt['data'] as $row){
                $x = array();
                $x[] = ++$count;
                $x[] = $row->nama_lengkap;
                $x[] = $row->jenis_aksi;
                $x[] = $row->keterangan;
                $x[] = $row->tgl;
                $rows []= $x;
            }
            $this->exportExcel($header,$rows,'list_log');
        }     
        $page_data['page_name'] = 'log';
        $page_data['page_title'] = 'Log';
        $this->load->view('login/index', $page_data);
    }
//#------------------------------------------------------------------------------------------------#//  
    public function kontak($param1='',$param2=''){
        if(!$this->session->userdata('login_type'))
            redirect(base_url().'rwhgate');

        if($param1 == 'add'){
            $data['hp'] = $this->input->post('hp');
            $data['whatsapp_chat'] = 'Ready';
            $data['status'] = 1;
            $this->md_kontak->addKontak($data);
            $this->md_log->addLog(adminLog('Menambah No Whatsapp','Menambah No Whatsapp '.$data['hp']));
            echo json_encode('add_success');
            die;
        }
        else if($param1 == 'update'){
            $kontak_id = decrypt($param2);
            if($kontak_id==1){
                $data['alamat'] = $this->input->post('alamat');
                $data['telp'] = $this->input->post('telp');
                $data['hp'] = $this->input->post('hp');
                $data['email'] = $this->input->post('email');
                $data['faks'] = $this->input->post('faks');
                $this->md_log->addLog(adminLog('Memperbaharui Kontak Utama','Memperbaharui data Kontak Utama'));
            } else {
                $data['hp'] = $this->input->post('hp');
                $this->md_log->addLog(adminLog('Memperbaharui No WA','Memperbaharui No WA '.$data['hp']));
            }
            $this->md_kontak->updateKontak($kontak_id,$data);
            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'delete'){
            $kontak_id = decrypt($param2);
            $temp = $this->md_kontak->getKontakById($kontak_id);
            $data['status'] = 2;
            $this->md_kontak->updateKontak($kontak_id,$data);
            $this->md_log->addLog(adminLog('Menghapus No WA','Menghapus No WA '.$temp[0]->hp));
            echo json_encode('update_success');
            die;
        }
        else if($param1 == 'wa_ready'){
            $kontak_id = decrypt($param2);
            $temp = $this->input->post('wa_ready');
            if($temp=='null')
                $data['whatsapp_chat'] = NULL;
            else
                $data['whatsapp_chat'] = $temp;

            $this->md_kontak->updateKontak($kontak_id,$data);
            echo json_encode('update_success');
            die;
        }

        $page_data['kontak_utama'] = $this->md_kontak->getKontakUtama();
        $page_data['kontak_wa'] = $this->md_kontak->getKontakForWhatsapp();
        $page_data['page_name'] = 'kontak';
        $page_data['page_title'] = 'Kontak';
        $this->load->view('login/index', $page_data);        
    }
//#------------------------------------------------------------------------------------------------#// 
    public function kurs_dollar($param1=""){
        if($param1=='update'){
            $data['harga_rupiah'] = $this->input->post('kurs_dollar');
            $data['tgl_perubahan'] = date('Y-m-d H:i');
            $data['status'] = 1;
            $this->md_kurs_dollar->addKursDollar($data);
            $this->md_log->addLog(adminLog('Kurs Dollar','Memperbaharui kurs dollar '.date('d-F-Y').' menjadi '.$data['harga_rupiah']));
            echo json_encode('update_success');
            die;
        }
    }
//#------------------------------------------------------------------------------------------------#//  
    public function toggle(){
        if($this->session->userdata('body_close_sidebar') && $this->session->userdata('menu_close_sidebar')){
            $this->session->unset_userdata('body_close_sidebar');
            $this->session->unset_userdata('menu_close_sidebar');
        } else {
            $this->session->set_userdata('body_close_sidebar','page-sidebar-menu-closed page-sidebar-closed');
            $this->session->set_userdata('menu_close_sidebar','page-sidebar-menu-closed');
        }
        echo json_encode("done");
        die;
    }
//#------------------------------------------------------------------------------------------------#// 
    public function exportExcel($header="",$rows="",$filename=""){
        $this->load->library('PHPExcel');
        $this->load->library('PHPExcel/IOFactory');
 
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

        $col = 0;
        foreach ($header as $header){
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++, 1, $header);
        }
 
        $row = 2;
        foreach($rows as $dt)
        {
            for($i=0;$i<count($dt);$i++){
               $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($i, $row, $dt[$i]);
            }
            $row++;
        }
 
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'_'.date('dMy').'.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');        
    }    
}