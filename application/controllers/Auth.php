<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->library('googlefunction');
		$this->load->model('md_pengguna');
		$this->load->model('md_member');
		$this->load->model('md_kantor_cabang');
		$this->load->model('md_log');
		$this->load->model('md_media_detail');

		$this->load->helper('email');
		$this->load->helper('userLog');
		$this->load->helper('encrypt');

		date_default_timezone_set('Asia/Jakarta');
	}
	public function index(){
		$page_data['meta_desc'] = 'RWH memberikan artikel menraik seputar islam, umroh, haji, dan informasi umum yang akan menambah wawasan pengunjung website rwh.com';
        $page_data['meta_keyword'] = 'Umroh, Riau Wisasta Hati, Riau, Umroh Riau, Umroh aman';
        $page_data['page_name'] = 'login';
        $page_data['page_title'] = 'Riau Wisasta Hati';
        $this->session->set_userdata('loginUrl',$this->googlefunction->getLoginUrl());
        $this->session->set_userdata('login_page','guest');
        $this->load->view('index', $page_data);
	}

	public function oauth(){
		$admin_page = FALSE;
		$guest_page = FALSE;
		if($this->session->userdata('login_page')=='admin')
			$admin_page = TRUE;
		else
			$guest_page = TRUE;

		$code = $this->input->get('code',TRUE);
		if($code){
			$login_with_google = $this->googlefunction->login($code);
			if($login_with_google){
				$user = $this->googlefunction->getUserInfo();
				$nama = $user->name;
				$picture = $user->picture;
				$email = $user->email;
				$this->session->set_userdata('url',$this->googlefunction->getLoginUrl());
				$cek = $this->md_member->getMemberByEmail($email);

		        foreach($cek as $row){
		            if($row->media_id){
		                $temp = $this->md_media_detail->getMediaDetailByMediaId($row->media_id);
		                $picture = base_url().$temp[1]->media_link;
		            }
		        }
			}
		}
		else {
			
			$password = hash('sha512', $this->input->post('password'));
			$picture = base_url()."assets/frontend/img/no-avatar.png";
			if($admin_page){
				$uname = $this->input->post('username');
				$cek = $this->md_pengguna->getPenggunaByEmailByPass($uname,$password);
				if(!$cek){
					$this->session->set_flashdata('err_login','<i class="fa fa-times"></i> Username atau Password tidak terdaftar. Informasi lebih lanjut hubungi Kantor Pusat');
					redirect(base_url().'rwhgate');
				}
			}
			else{
				$email = $this->input->post('email');
				$cek = $this->md_member->getMemberByEmailByPass($email,$password);
				if(!$cek){
					$this->session->set_flashdata('err_login','Username atau Password salah');
					redirect(base_url().'login');
				}
			}
		}

		if($cek && $guest_page){
			if($cek[0]->status_aktif!='Aktif'){
				$this->session->set_flashdata('err_login','Akun member belum melakukan klik link verifikasi pada email yang dikirimkan.
												 Silahkan cek kembali email verifikasi RWH di <b>'.$cek[0]->email.'</b>
												 atau <b><a href="javascript:;" onClick="kirim_verif('.encrypt($cek[0]->member_id).')">Klik disini</a></b> untuk mengirimkan kembali email verifikasi akun RWH');
				redirect(base_url().'login');
			}
			if($cek[0]->media_id){
				$temp=$this->md_media_detail->getMediaDetailByMediaId($cek[0]->media_id);
				$picture = base_url().$temp[1]->media_link;
			}
		    $this->session->set_userdata('say_hello','1');
		 	$this->session->set_userdata('member_login', '1');
			$this->session->set_userdata('member_email',$cek[0]->email);
			$this->session->set_userdata('member_id',encrypt($cek[0]->member_id));
			$this->session->set_userdata('nama',$cek[0]->nama_lengkap);
			$this->session->set_userdata('picture',$picture);
		    
		    //Pengecekan jika sebelum nya login dari tombol booking
				$paket_id = $this->session->userdata('paket_id');
				$pwk_id = $this->session->userdata('pwk_id');
				$this->session->unset_userdata('paket_id');
				$this->session->unset_userdata('pwk_id');
				$this->session->unset_userdata('login_first');
				//Paket Reguler
				if($paket_id && $pwk_id)
					redirect(base_url().'detail/form_booking/'.$paket_id.'/'.$pwk_id);
				//Paket Haji
				else if($paket_id)
					redirect(base_url().'detail/form_booking/'.$paket_id);
				else
					redirect(base_url().'member','refresh');
		} 

		//User login menggunakan google account namun bukan member dan diarahkan untuk mendaftar 
		else if($guest_page){
				$this->session->set_flashdata('new-sign-in','1');
				$this->session->set_userdata('member_email',$email);
				$this->session->set_userdata('nama',$nama);
				$this->session->set_userdata('picture',$picture);
				redirect(base_url().'daftar');
		}

		else if($admin_page){

			// cek apakah email termasuk pengguna kantor cabang atau perwakilan daerah
			$cek = $this->md_pengguna->getPenggunaByEmail($cek[0]->email);

			if($cek){						
				$this->session->set_userdata('body_close_sidebar','page-sidebar-menu-closed page-sidebar-closed');
        		$this->session->set_userdata('menu_close_sidebar','page-sidebar-menu-closed');
        		$this->session->set_userdata('login_type',$cek[0]->level);
        		$this->session->set_userdata('kode_kota_pengguna',$cek[0]->kode_kota);
        		$this->session->set_userdata('kota_pengguna',$cek[0]->kota);
				$this->session->set_userdata('pengguna_id',$cek[0]->pengguna_id);
				$this->session->set_userdata('pengguna_email',$cek[0]->email);
				$this->session->set_userdata('nama',$cek[0]->nama_lengkap);
				$this->session->set_userdata('nama_kantor',$cek[0]->nama_cabang);
				$this->session->set_userdata('alamat_kantor',$cek[0]->alamat);
				$this->session->set_userdata('kota_kantor',$cek[0]->kota);
				$this->session->set_userdata('telp_kantor',$cek[0]->telp);
				$this->session->set_userdata('id_kantor',$cek[0]->kantorcabang_id);
				$this->session->set_userdata('picture',$picture);

				$this->md_log->addLog(adminLog('Login','Login ke RWH System'));
				redirect(base_url().'rwhgate','refresh');
			} 

			//Email user tidak terdaftar sebagai Admin
			else {

				if($this->session->userdata('admin_page')){
					$this->session->set_flashdata('err_login','Ooppss... Anda tidak memiliki hak akses untuk halaman ini. Silahkan Hubungi Kantor Pusat RWH');
					redirect(base_url().'rwhgate');
				} 
			}
		}
		
	}


	public function logout($param=''){
		if($this->session->userdata('pengguna_id')){
			$this->md_log->addLog(adminLog('Logout','Logout dari RWH System'));	
		}
	
		$this->googlefunction->revokeToken();
		$this->session->sess_destroy();
		if($param=='member')
			redirect(base_url());
		else
			redirect(base_url().'rwhgate');
	}

	function reset_password(){
		$email = $this->input->post('email_reset');
		$password = generateRandomString(8);
		$data['password'] = hash('sha512', $password);
		$cek = $this->md_member->getMemberByEmail($email);
		if($cek){
			$this->md_member->updateMember($cek[0]->member_id,$data);
            $result = emailResetPassword($cek[0]->nama_lengkap,$email,$password);
            if($result=='success'){
            	$this->session->set_flashdata('reset_success',$email);
            	echo json_encode('Reset Password Success');
            }
		} else {
			$this->session->set_flashdata('reset_fail',1);
			echo json_encode('Email Tidak Valid');
		}
		die;
	}
}