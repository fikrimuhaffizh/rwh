<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Guest extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('md_kota');
        $this->load->model('md_pembayaran');
        $this->load->model('md_syarat_umroh');
        $this->load->model('md_syarat_status');
        $this->load->model('md_artikel');
        $this->load->model('md_media_detail');
        $this->load->model('md_galeri_album');
        $this->load->model('md_galeri_item');
        $this->load->model('md_paket');
        $this->load->model('md_kontak');
        $this->load->model('md_halaman');
        $this->load->model('md_slide');
        $this->load->model('md_hotel');
        $this->load->model('md_merchant');
        $this->load->model('md_paket_maskapai');
        $this->load->model('md_maskapai');
        $this->load->model('md_paket_waktu');
        $this->load->model('md_jenis_paket');
        $this->load->model('md_kelas');
        $this->load->model('md_paketwaktu_kelas');
        $this->load->model('md_jadwal_manasik');
        $this->load->model('md_kantor_cabang');
        $this->load->model('md_member');
        $this->load->model('md_paket_akomodasi');
        $this->load->model('md_pendaftaran');
        $this->load->model('md_pendaftaran_detail');
        $this->load->model('md_pengguna');
        $this->load->model('md_notifikasi');
        $this->load->model('md_konfirmasi');
        $this->load->model('md_testimoni');
        $this->load->model('md_keberangkatan');
        $this->load->model('md_bank');
        
        $this->load->helper('datetime');
        $this->load->helper('pagination');
        $this->load->helper('notif');
        $this->load->helper('email');
        $this->load->helper('encrypt');
        $this->load->helper('format');
        $this->load->helper('rwh');
        $this->load->helper('userLog');

        $this->load->library('email');
        $this->load->library('image_lib');  
        $this->load->library('googlefunction');
        
        date_default_timezone_set('Asia/Jakarta');

    }

//#------------------------------------------------------------------------------------------------#//  
    public function index($param='') {

        if (!isset($_REQUEST['code'])) {
            $data = array('loginUrl' => $this->googlefunction->getLoginUrl());
            $this->session->set_userdata('loginUrl',$data['loginUrl']);
        }

        $x = $this->md_paketwaktu_kelas->getPaketWaktuByPromo();
        for($i=0;$i<count($x);$i++){
            $y = $this->md_media_detail->getMediaDetailByMediaId($x[$i]->media_id);
            $x[$i]->media_link = $y[1]->media_link;
            $x[$i]->harga = rupiah_format($x[$i]->harga);
        }
        $page_data['paket_promo'] = $x;
        $page_data['atribut_form_umroh'] = getAtributFormPaket(1);
        $page_data['atribut_form_wisata'] = getAtributFormPaket(3);

        $x = $this->md_testimoni->getAllTestimoni();
        for($i=0;$i<count($x);$i++){
            $y = $this->md_media_detail->getMediaDetailByMediaId($x[$i]->media_id);
            $x[$i]->media_link = $y[2]->media_link;
        }
        $page_data['testimoni'] = $x;
        $page_data['slideshow'] = $this->md_slide->getAllSlide();
        $page_data['top3artikel'] = $this->md_artikel->get3topArtikel();
        $page_data['top4artikel'] = $this->md_artikel->get4topArtikel();
        $page_data['maskapai'] = $this->md_maskapai->getAllMaskapai();
        $page_data['page_name'] = 'beranda';
        $page_data['page_title'] = 'Riau Wisata Hati';

        $this->load->view('index', $page_data);   
    }

    public function daftar($param1='',$param2=''){
        $page_data['add_success'] = FALSE;
        if($param1 == 'add'){
            $eror='';
            $x['nama_lengkap'] = $this->input->post('nama_lengkap');
            $x['email'] = $this->input->post('email');
            $x['no_hp'] = $this->input->post('no_hp');
            $x['password'] = $this->input->post('password');

            //Cek panjang password
            if(strlen($this->input->post('password'))<8){
                $eror = $eror.' - Password Minimal 8 Karakter<br>';
            }

            //cek email ketersediaan email
            $cek = $this->md_member->getMemberByEmail($this->input->post('email'));
            if($cek){
                $eror = $eror.' - Email sudah digunakan';
            }

            if($eror!=''){
                $this->session->set_flashdata('err_daftar',$eror);
                $this->session->set_flashdata('err_data',$x);
                if($this->input->post('daftar')=='daftar'){
                    $this->session->set_flashdata('new-sign-in',1);
                    redirect(base_url().'daftar');
                }
                else {
                    redirect(base_url().'login');
                }
            } else {
                $data['nama_lengkap'] = $this->input->post('nama_lengkap');
                $data['email'] = $this->input->post('email');
                $data['no_hp'] = $this->input->post('no_hp');
                $data['password'] = hash('sha512',$this->input->post('password'));
                $data['tgl_regis'] = date('Y-m-d H:i:s');
                $data['ip_addr'] = $_SERVER['REMOTE_ADDR'];
                $data['registrasi'] = 'Web';
                $data['status'] = 1;      

         
                $member_id = $this->md_member->addMember($data);
                if ($member_id){
                    $link_verif = base_url().'verif_pendaftaran/'.encrypt($member_id); 
                    emailVerifPendaftaran($data['nama_lengkap'],$data['email'],$link_verif);
                    $this->session->set_flashdata('verif_pendaftaran',$data['email']);
                }
               
                redirect(base_url().'login','refresh');
                    
            }

        }
        else if($param1=='verif'){
            $member_id = decrypt($param2);
            $link_verif = base_url().'verif_pendaftaran/'.encrypt($member_id); 
            
            $x = $this->md_member->getMemberByMemberId($member_id);
            $result = emailVerifPendaftaran($x[0]->nama_lengkap,$x[0]->email,$link_verif);
            if($result=='success'){
                $this->session->set_flashdata('verif_pendaftaran',$x[0]->email);
                echo json_encode('update_success');
                
            }
        } 
        else {
            if($this->session->flashdata('new-sign-in')){
                $page_data['top4artikel'] = $this->md_artikel->get4topArtikel();
                $page_data['email'] = $this->session->userdata('member_email');
                $page_data['nama'] = $this->session->userdata('nama');
                $page_data['page_name'] = 'daftar';
                $page_data['page_title'] = 'Daftar Member';
                $this->load->view('index', $page_data); 
            } else {
                redirect($this->session->userdata('url'));
                
            }
        }  
    }

    public function artikel($param1='',$param2='') {
        $page_data['detail_artikel'] = FALSE;
        $page_data['prev_news'] = FALSE;
        $page_data['next_news'] = FALSE;

        //Meta Data
        $page_data['meta_desc'] = 'Riau Wisata Hati memberikan artikel menraik seputar islam, umroh, haji, dan informasi umum yang akan menambah wawasan pengunjung rwh.com';
        $page_data['meta_keyword'] = 'Umroh, Umroh Service, Riau, Umroh Riau, Umroh aman';
        $page_data['meta_img'] = 'http://'.$_SERVER['SERVER_NAME'].'/assets/img/brand/logo-black.png';
        $page_data['meta_type'] = 'article';
        $page_data['page_title'] = 'Artikel';

        if($param1=='detail'){
            $id = decrypt($param2);
            $temp = $this->md_artikel->getArtikelById($id);
            if(!$temp){
                $page_data['page_name'] = '404';
                $page_data['page_title'] = '404 Page Not Found ';
                $this->load->view('index', $page_data);
            }

            foreach($temp as $row){
                $x = $this->md_media_detail->getMediaDetailByMediaId($row->media_id);

                //Berita sebelum dan selanjtunya
                    $prev = $this->md_artikel->getPreviousArtikel($row->tgl_post);
                    if($prev) 
                        $page_data['prev_news'] = $prev;

                    $next = $this->md_artikel->getNextArtikel($row->tgl_post);
                    if($next)
                        $page_data['next_news'] = $next;

                $row->media_id = base_url().$x[1]->media_link;
                $row->tgl_post = date('l, d-F-Y',strtotime($row->tgl_post));

                //Meta Data
                $page_data['meta_desc'] = $row->meta_desc;
                $page_data['meta_keyword'] = $row->meta_keyword;
                $page_data['meta_img'] = base_url().$x[0]->media_link;
                $page_data['page_title'] = $row->judul;
            }
            $page_data['detail_artikel'] = $temp;
        } 
        else {
            //pagination settings
                $per_page = 3;
                $total_rows = count($this->md_artikel->getPublishedArtikel());
                $page = config_pagination($per_page,'artikel',$total_rows,2);

            $page_data['artikel'] = $this->md_artikel->pagination($per_page,$page,2);
            $page_data['pagination'] = $this->pagination->create_links();
            
            if($page==0)
                $page=1;
            else if($page>=2)
                $page_data['page_title'] = 'Artikel Halaman '.$page;

            $page_data['meta_desc'] = 'Index Artikel Riau Wisata Hati Halaman '.$page;
        }

        $page_data['top4artikel'] = $this->md_artikel->get4topArtikel();
        $page_data['page_name'] = 'artikel';
        
        $this->load->view('index', $page_data);   
    }

    public function kantor($param1="",$param2=""){
        if($param1=="getDetailKantor"){
            $daerah = str_replace('_',' ',$param2);
            $x = $this->md_kantor_cabang->getKantorCabangByKota($daerah);
            echo json_encode($x);
            die;
        }

        $page_data['top4artikel'] = $this->md_artikel->get4topArtikel();
        $page_data['kantor_cabang_provinsi'] = $this->md_kantor_cabang->getAllProvinsiKantorCabang();
        // echo_array($page_data['kantor_cabang_provinsi']);
        // die;
        $page_data['all_kota_kantor'] = $this->md_kantor_cabang->getAllKantorCabangDistinctByKota();
        // echo_array($page_data['all_kota_kantor']);die;
        $page_data['page_name'] = 'kantor';
        $page_data['page_title'] = 'Daftar Kantor Cabang';
        $this->load->view('index', $page_data);        
    }

    public function status(){
        $page_data['top4artikel'] = $this->md_artikel->get4topArtikel();
        $page_data['page_name'] = 'status';
        $page_data['page_title'] = 'Status Keberangkatan dan Pembayaran Jemaah';
        $this->load->view('index', $page_data);        
    }    

    public function paket($param1=""){
        //Default Value
        $btn_cari = $this->input->post('btn-cari');
        if($param1=="haji" || $param1=="umroh" || $param1=="wisata" || ($btn_cari && $btn_cari=='umroh') || ($btn_cari && $btn_cari=='wisata')){
            //Pagination Settings
            $per_page = 3;
            if($param1=="haji"){
                $x = $this->md_paket->getAllPaketHaji(2);  
                $berangkat = FALSE;
                $bulan     = FALSE;
                $tahun     = FALSE;     
                for($i=0;$i<count($x);$i++){
                    $y = $this->md_media_detail->getMediaDetailByMediaId($x[$i]->media_id);
                    $x[$i]->media_link = $y[1]->media_link;
                    $x[$i]->harga_terakhir = rupiah_format($x[$i]->harga_terakhir);
                }   
                $page_data['page_title'] = 'Paket Haji Plus';
            }
            else if($param1=="umroh" || ($btn_cari && $btn_cari=='umroh')){
                $berangkat = $this->input->post('keberangkatan_umroh');
                $bulan     = $this->input->post('bulan_umroh');
                $tahun     = $this->input->post('tahun_umroh'); 

                //Menggunakan Kota Search
               
                    $pass=0;
                    if($berangkat == "Semua Keberangkatan") 
                        $pass++;
                    else if(is_numeric($berangkat)){
                        $berangkat = decrypt($berangkat);
                        $pass++;
                    }

                    if($bulan == "Semua Bulan") 
                        $pass++;
                    else if(is_numeric($bulan))
                        $pass++;

                    if($tahun == "Semua Tahun")
                        $pass++;
                    else if(is_numeric($tahun))
                        $pass++;

                    //Filter result
                
                        $total_rows = $this->md_paket->getCountPaketBySearch($berangkat,$bulan,$tahun,"",1)[0]->total;
                        $page = config_pagination($per_page,$param1,$total_rows,2);
                        $x = $this->md_paket->getPaketBySearch($berangkat,$bulan,$tahun,"",1,$per_page,$page);

                
                    
                $page_data['atribut_form_umroh'] = getAtributFormPaket(1);           
                $page_data['page_title'] = 'Paket Umroh';            
            }
            else if($param1=="wisata" || ($btn_cari && $btn_cari=='wisata')){
                $berangkat = $this->input->post('keberangkatan_wisata');
                $bulan = $this->input->post('bulan_wisata');
                $tahun = $this->input->post('tahun_wisata'); 
                //Menggunakan Kota Search
                if($berangkat){
                    if($berangkat!="Semua Keberangkatan")
                        $berangkat = decrypt($berangkat);   

                    $x = $this->md_paket->getPaketBySearch($berangkat,$bulan,$tahun,"",3);   
                }  
                //Langsung Akses Halaman Wisata Islam 
                else {      
                    $x = $this->md_paket->getAllPaketByJenis(3);
                }
                $page_data['atribut_form_wisata'] = getAtributFormPaket(3);
                $page_data['page_title'] = 'Paket Wisata Islam';            
            } 


            if($param1!='haji'){
                for($i=0;$i<count($x);$i++){
                    $y = $this->md_paketwaktu_kelas->getPaketWaktuKelasByPaketWaktuId($x[$i]->paketwaktu_id);
                    //Karena diorder ASC, array 0 merupakan harga terendah
                    $x[$i]->harga  = $y[0]->harga;
                    $x[$i]->promo  = $y[0]->promo;
                    $x[$i]->diskon = $y[0]->diskon;
                }   

                if(!$berangkat) $berangkat = "Semua Keberangkatan";
                if(!$bulan) $bulan         = "Semua Bulan";
                if(!$tahun) $tahun         = "Semua Tahun";

                $page_data['dt_berangkat_awal'] = $berangkat;
                $page_data['dt_bulan']          = $bulan;
                $page_data['dt_tahun']          = $tahun;

                $page_data['list_bulan']           = all_bulan();
                $page_data['bulan_short']          = all_bulan_short();
                $page_data['maskapai_penerbangan'] = $this->md_maskapai->getAllMaskapai();
                $page_data['hotel']                = $this->md_hotel->getAllHotel();        
            }

             //pagination settings
                



            $page_data['paket'] = $x;
            $page_data['link'] = $this->pagination->create_links();


            // $page_data['paket']       = $x;
            $page_data['top4artikel'] = $this->md_artikel->get4topArtikel();
            $page_data['page_name']   = 'paket';
            $this->load->view('index', $page_data);     
        }
        else {
            $page_data['top4artikel'] = $this->md_artikel->get4topArtikel();
            $page_data['page_name'] = '404';
            $page_data['page_title'] = '404 Page Not Found ';
            $this->load->view('index', $page_data);
        }             
    }

    public function manasik(){

        $temp = $this->md_jadwal_manasik->getAllJadwalManasikAfterToday();
        $page_data['manasik'] = $temp;
        $page_data['top4artikel'] = $this->md_artikel->get4topArtikel();
        $page_data['page_name'] = 'manasik';
        $page_data['page_title'] = 'Daftar Jadwal Bimbingan Manasik';
        $this->load->view('index', $page_data);        
    }

    public function merchant(){

        $x = $this->md_merchant->getAllMerchant();
        for($i=0;$i<count($x);$i++){
            $y = $this->md_media_detail->getMediaDetailByMediaId($x[$i]->media_id);
            $x[$i]->img_link = base_url().$y[1]->media_link;
        }
        $page_data['merchant']    = $x;
        $page_data['top4artikel'] = $this->md_artikel->get4topArtikel();
        $page_data['page_name']   = 'merchant';
        $page_data['page_title']  = 'Kerja Sama Merchant dan Riau Wisata Hati';
        $this->load->view('index', $page_data);        
    }

    public function detail($param1='',$param2='',$param3='',$param4=''){
        $page_data['booking'] = FALSE;


        if($param1 == 'form_booking'){

            //Jika belum menjadi member, diarahkan ke halaman daftar dan simpan session paket yang di booking
            if(!$this->session->userdata('member_id')){ 
                $this->session->set_userdata('login_first','Silahkan login terlebih dahulu');
                if($param3){
                    //Booking Paket Reguler
                        $this->session->set_userdata('paket_id',$param2);
                        $this->session->set_userdata('pwk_id',$param3);
                        
                        //Booking paket yang sudah ada tanggal keberangkatan
                            if($param4){
                                $this->session->set_userdata('keberangkatan_id',$param4);
                    }
                }
                else
                    //Booking Paket Haji
                    $this->session->set_userdata('paket_id',$param2);

                redirect(base_url().'login');
            }

            $member_id = decrypt($this->session->userdata('member_id'));
            $paket_id = decrypt($param2);
            $x = $this->md_paket->getPaketByPaketId($paket_id);

            //Param3 hanya untuk Paket Reguler
            if($param3){
                $pwk_id = decrypt($param3);
                $c = $this->md_kantor_cabang->getKantorCabangById($x[0]->berangkat_awal);
                $dt['kota_berangkat_awal'] = $c[0]->provinsi;
                $dt['berangkat_awal'] = encrypt($x[0]->berangkat_awal);
                $dt['lama_hari'] = $x[0]->lama_hari.' Hari';
                $page_data['paketwaktu_kelas'] = $this->md_paketwaktu_kelas->getPaketWaktuKelasByIdV2($pwk_id);

                //Param4 hanya untuk paket reguler yang sudah ada tanggal keberangkatan
                    $page_data['keberangkatan_id'] = $param4 ? $param4 : FALSE;
                
            } 
            //Paket Haji
            else {
                $page_data['paketwaktu_kelas'] = FALSE;
                $dt['harga_terakhir']          = $x[0]->harga_terakhir;
                $dt['masa_tunggu']             = $x[0]->masa_tunggu;
                $dt['paket_id']                = encrypt($x[0]->paket_id);
            }

            $dt['nama_paket']          = $x[0]->nama_paket;
            $page_data['paket_detail'] = $dt;
            $page_data['booking']      = TRUE;

            $page_data['page_title'] = 'Booking '.$x[0]->nama_paket;
            $page_data['page_name'] = 'paket_detail';
            $page_data['meta_desc'] = 'Penjelasan syarat dan ketentuan sekaligus pengisian data jemaah dalam melakukan booking paket Riau Wisata Hati demi menjaga kenyamanan jemaah';
            $page_data['meta_keyword']  = 'RWH, Riau Wisata Hati, Umroh, Umroh Service, Riau, Umroh Riau, Umroh aman';
            $page_data['meta_type']  = 'webiste';
            $page_data['meta_img'] = 'http://'.$_SERVER['SERVER_NAME'].'/assets/frontend/img/brand/logo-black.png';
        }
        else if ($param1 == 'booking'){
            //Pendaftaran
            $data['member_id'] = decrypt($this->session->userdata('member_id'));
            $data['tgl_daftar'] = date('Y-m-d H:i:s');            
            $data['jenis_pendaftaran'] = 'Web';
            $data['status_aktif'] = 'Aktif';

            //Booking Paket Reguler
            if($this->input->post('pwk_id'))
                $data['kantorcabang_id'] = decrypt($this->input->post('kantorcabang_id'));
            
            //Booking Paket Haji
            else 
                $data['kantorcabang_id'] = NULL;

            
            $data['status'] = 1;
            
            $pendaftaran_id = $this->md_pendaftaran->addPendaftaran($data);
            $paket_id_forlink = $this->input->post('paket_id');
            if($pendaftaran_id){
                for($i=1;;$i++){
                    //Booking Paket Reguler
                    if($this->input->post('pwk_id')){
                        $jenis = 'Reguler';
                        $data2['pwk_id'] = decrypt($this->input->post('pwk_id'));
                        $pwk_id_forlink = $this->input->post('pwk_id');
                    } 
                    //Booking Paket Haji
                    else {
                        $jenis = 'Haji';
                        $data2['paket_id'] = decrypt($this->input->post('paket_id'));
                        $data2['tahun_est_berangkat'] = date('Y')+$this->input->post('masa_tunggu');
                        $pwk_id_forlink = '';
                    }            

                    $data2['pendaftaran_id'] = $pendaftaran_id;
                    $data2['nama_jemaah'] = $this->input->post('nama_jemaah_'.$i);
                    $data2['tgl_lahir'] = date('Y-m-d H:i:s',strtotime($this->input->post('tgl_lahir_'.$i)));
                    $data2['alamat'] = $this->input->post('alamat_'.$i);
                    $data2['jenis_kelamin'] = $this->input->post('jenis_kelamin_'.$i);
                    $data2['no_hp'] = $this->input->post('no_hp_'.$i);
                    $data2['email'] = $this->input->post('email_'.$i);
                    $data2['tgl_submit'] = date('Y-m-d H:i:s');
                    $data2['harus_bayar'] = $this->input->post('harga');
                    $data2['status_keberangkatan'] = 'Dalam Proses';
                    $data2['jenis_pendaftaran'] = $this->input->post('jenis_pendaftaran_'.$i);
                    $data2['status'] = 1;

                    //JIka keberangkatan sudah ada, langsung dimasukkan ke tabel pendaftar
                    if($this->input->post('keberangkatan_id')){
                        $data2['keberangkatan_id'] = decrypt($this->input->post('keberangkatan_id'));
                    }

                    if($data2['nama_jemaah']){
                       $pendaftarandetail_id = $this->md_pendaftaran_detail->addPendaftaranDetail($data2);
                       if($pendaftarandetail_id){
                        //Syarat Jemaah
                            if($jenis=='Reguler')
                                $y = $this->md_syarat_umroh->getAllSyaratUmrohDefault();
                            if($jenis=='Haji')
                                $y = $this->md_syarat_umroh->getAllSyaratHajiDefault();

                            foreach($y as $row){
                                $data3['syaratumroh_id'] = $row->syaratumroh_id;
                                $data3['pendaftarandetail_id'] = $pendaftarandetail_id;
                                $data3['status_syarat'] = 'Belum Lengkap';
                                $data3['status'] = 1;
                                $this->md_syarat_status->addSyaratStatus($data3);
                            }

                        //Notif to Admin
                            $nama_paket = $this->input->post('nama_paket');
                            $judul = $data2['nama_jemaah'].' melakukan Booking Paket';
                            $ket = 'Jemaah '.$data2['nama_jemaah'].' melakukan booking paket '.$nama_paket;
                            $link = 'administrator/pendaftaran/detail_jemaah/'.encrypt($pendaftarandetail_id);

                            $v = $this->md_pengguna->getPenggunaByLevel(array('Kantor Pusat','Keuangan'));
                            foreach($v as $row){
                                NotifToAdmin($judul,$ket,$link,$row->pengguna_id);
                            }
                       }
                    }
                    else break;
                }
            }
            
            $x['pendaftaran_id'] = encrypt($pendaftaran_id);
            $this->session->set_flashdata('booking_done',$x);
            
            redirect(base_url().'detail/form_booking/'.$paket_id_forlink.'/'.$pwk_id_forlink);
        } 
        else if($param1=='cekKodePromo'){
            $x = cekKodeAfiliasi($param2);
            echo json_encode($x);
            die;
        }
        //Detail Paket Haji
        else if($param1 == 'haji'){
            $paket_id = decrypt($param2);
            $x = $this->md_paket->getPaketByPaketId($paket_id);
            if($x){
                for($i=0;$i<count($x);$i++){
                    $a = $this->md_jenis_paket->getJenisPaketByJenisPaketId($x[$i]->jenispaket_id);
                    $b = $this->md_media_detail->getMediaDetailByMediaId($x[$i]->media_id);
                    $dt['paket_id'] = encrypt($x[0]->paket_id);
                    $dt['paket_pic'] = base_url().$b[1]->media_link;
                    $dt['nama_paket'] = $x[0]->nama_paket;
                    $dt['jenis_paket'] = $a[0]->jenis_paket;
                    $dt['harga_terakhir'] = rupiah_format($x[$i]->harga_terakhir);
                    $dt['masa_tunggu'] = $x[0]->masa_tunggu;
                    $dt['jenis_kuota'] = $x[0]->jenis_quota;
                    $dt['info_plus'] = $x[0]->info_plus;
                }
                $page_data['paket_detail'] = $dt;
                $page_data['page_title'] = 'Paket '.$dt['nama_paket'];
                $page_data['meta_desc'] = 'Menjelaskan detail dari paket '.$dt['nama_paket'].' dengan berbagai keunggulan dan fasilitas yang disediakan demi kenyamanan jemaah dalam memilih paket perjalanan bersama Riau Wisata Hati ';
                $page_data['meta_img'] = base_url().$b[1]->media_link;
                $page_data['meta_keyword']  = 'RWH, Riau Wisata Hati, Umroh, Umroh Service, Riau, Umroh Riau, Umroh aman';
                $page_data['meta_type']  = 'webiste';

            } else {
                $page_data['top4artikel'] = $this->md_artikel->get4topArtikel();
                $page_data['page_name'] = '404';
                $page_data['page_title'] = '404 Page Not Found ';
                $this->load->view('index', $page_data);
            }
        }
        //Detail Paket Reguler
        else {
            $paketwaktu_id = decrypt($param1);
            $y = $this->md_paket_waktu->getPaketWaktuByPaketWaktuIdIdV3($paketwaktu_id);
            if(!$y){
                $page_data['top4artikel'] = $this->md_artikel->get4topArtikel();
                $page_data['page_name']   = '404';
                $page_data['page_title']  = '404 Page Not Found ';
                $this->load->view('index', $page_data);
            } else {
                $paket_id = $y[0]->paket_id;
                $id       = $paket_id;
                $data     = array();
                $x        = $this->md_paket->getPaketByPaketId($id);
                for($i=0;$i<count($x);$i++){
                    $a = $this->md_jenis_paket->getJenisPaketByJenisPaketId($x[$i]->jenispaket_id);
                    $b = $this->md_media_detail->getMediaDetailByMediaId($x[$i]->media_id);
                    $c = $this->md_kota->getKotaById($x[$i]->berangkat_awal);

                    //Maskapai (d)
                        $temp = $this->md_paket_maskapai->getPaketMaskapaiByPaketId($id);
                        for($j=0;$j<count($temp);$j++){
                            $_temp  = $this->md_maskapai->getMaskapaiById($temp[$j]->maskapai_id);
                            $__temp = $this->md_media_detail->getMediaDetailByMediaId($_temp[0]->media_id);
                            $d[$j]['nama_maskapai'] = $_temp[0]->nama_maskapai;
                            $d[$j]['deskripsi']     = $_temp[0]->deskripsi;
                            $d[$j]['logo']          = base_url().$__temp[1]->media_link;
                        }
                        
                    //Paket Akomodasi (e)
                        $temp = $this->md_paket_akomodasi->getpaketAkomodasiByPaketId($id);
                        for($j=0;$j<count($temp);$j++){
                            $_temp = $this->md_hotel->getHotelById($temp[$j]->hotel);
                            $__temp = $this->md_media_detail->getMediaDetailByMediaId($_temp[0]->media_id);
                            $e[$j]['nama_hotel'] = $_temp[0]->nama_hotel;
                            $e[$j]['kota']       = $_temp[0]->kota;
                            $e[$j]['bintang']    = $_temp[0]->bintang;
                            $e[$j]['deskripsi']  = $_temp[0]->deskripsi;
                            $e[$j]['foto']       = base_url().$__temp[1]->media_link;
                            $e[$j]['foto_besar'] = base_url().$__temp[0]->media_link;
                        }

                    //Paket Waktu
                        $temp = $y;
                        $bulan = all_bulan();
                        for($j=0;$j<count($temp);$j++){
                            $__temp = $this->md_kelas->getKelasById($temp[$j]->kelas_id);
                            $f[$j]['pwk_id']         = encrypt($temp[$j]->pwk_id);
                            $f[$j]['kelas']          = $__temp[0]->kelas;
                            $f[$j]['jumlah_sekamar'] = $__temp[0]->jumlah_sekamar;
                            $f[$j]['harga']          = rupiah_format($temp[$j]->harga-$temp[$j]->diskon);
                            $f[$j]['harga_awal']     = rupiah_format($temp[$j]->harga_awal);
                            $f[$j]['diskon']         = $temp[$j]->diskon;
                            $f[$j]['promo']          = $temp[$j]->promo;
                            $f[$j]['berangkat']      = $bulan[$temp[$j]->bulan_berangkat].' '.$temp[$j]->tahun_berangkat;
                            $f[$j]['paketwaktu_id']  = $paketwaktu_id;
                        }

                    //Gabung Semua data paket
                        $dt['paket_id']          = encrypt($x[0]->paket_id);
                        $dt['paket_pic']         = base_url().$b[0]->media_link;
                        $dt['nama_paket']        = $x[0]->nama_paket;
                        $dt['jenis_paket']       = $a[0]->jenis_paket;
                        $dt['berangkat_awal']    = $c[0]->nama_kota;
                        $dt['berangkat_transit'] = $x[0]->berangkat_transit;
                        $dt['berangkat_landing'] = $x[0]->berangkat_landing;
                        $dt['info_plus']         = $x[0]->info_plus;
                        $dt['lama_hari']         = $x[0]->lama_hari.' Hari';
                        $dt['mulai_publish']     = date('d-F-Y H:i:s',strtotime($x[0]->mulai_publish));
                        $dt['akhir_publish']     = date('d-F-Y H:i:s',strtotime($x[0]->akhir_publish));
                        $dt['maskapai']          = $d;
                        $dt['paket_akomodasi']   = $e;
                        $dt['paket_waktu']       = $f;
                }
                $page_data['paket_detail'] = $dt;
                $page_data['page_title']   = 'Paket '.$dt['nama_paket'].' '.$f[0]['berangkat'].' '.$dt['berangkat_transit'];
                $page_data['meta_desc']    = 'Menjelaskan detail dari paket '.$dt['nama_paket'].' '.$f[0]['berangkat'].' '.$dt['berangkat_transit'].' dengan berbagai keunggulan dan fasilitas yang disediakan demi kenyamanan jemaah dalam memilih paket perjalanan bersama Riau Wisata Hati ';
                $page_data['meta_img']     = base_url().$b[1]->media_link;
                $page_data['meta_keyword'] = 'RWH, Riau Wisata Hati, Umroh, Umroh Service, Riau, Umroh Riau, Umroh aman';
                $page_data['meta_type']    = 'webiste';
            }

        } 

        $page_data['top4artikel'] = $this->md_artikel->get4topArtikel();
        $page_data['bulan']       = all_bulan();
        $page_data['page_name']   = 'paket_detail';
        $this->load->view('index', $page_data);  
    }

    public function galeri($param1="",$param2=""){
        $page_data['galeri_item'] = false;

        if($param1 == 'detail'){
            $id = decrypt($param2);

            //pagination settings
                $per_page = 9;
                $total_rows = count($this->md_galeri_item->getGaleriItemByGaleriAlbumId($id));
                $page = config_pagination($per_page,'guest/galeri/detail/'.$param2,$total_rows,5);

            if($page==0)
                $start=1;
            else
                $start = ($per_page*($page-1));

            
            $x = $this->md_galeri_item->pagination($id,$per_page,$start);
            $i=0;

            foreach($x as $row){
                $y = $this->md_media_detail->getMediaDetailByMediaId($row->media_id);
                $x[$i] = array(
                            'media_small' => $y[2]->media_link,
                            'media_medium' => $y[1]->media_link,
                            'media_large' => $y[0]->media_link
                        );
                $i++;
            }
            $y = $this->md_galeri_album->getGaleriAlbumById($id);
            $page_data['album_name'] = $y[0]->nama_album;
            $page_data['album_keterangan'] = $y[0]->keterangan;
            $page_data['galeri_item'] = $x;
            $page_data['link'] = $this->pagination->create_links();
        } 
        else {
            //pagination settings
                $per_page = 10;
                $total_rows = count($this->md_galeri_album->getAllGaleriAlbum());
                $page = config_pagination($per_page,'galeri',$total_rows,2);

                $x = $this->md_galeri_album->pagination($per_page,$page);
                foreach($x as $row){
                    $temp = $this->md_media_detail->getMediaDetailByMediaId($row->media_id);
                    $row->media_id = base_url().$temp[1]->media_link;
                }
            $page_data['album'] = $x;
            $page_data['link'] = $this->pagination->create_links();
        }
        $page_data['top4artikel'] = $this->md_artikel->get4topArtikel();
        $page_data['album'] = $x;
        $page_data['page_name'] = 'galeri';
        $page_data['page_title'] = 'Galeri Foto Kegiatan';
        $this->load->view('index', $page_data);        
    }

    public function kontak($param1=""){
        if($param1 == 'getWhatsappReady'){
            $x = $this->md_kontak->getWhatsappOnly();
            foreach($x as $row){
                $num = $row->hp;
                $row->hp = "62".substr_replace($num, '', 0, 1);
            }
            echo json_encode($x);
            die;
        }
        else if($param1 == 'send_email'){
            $nama = $this->input->post('name');
            $email = $this->input->post('email');
            $msg = $this->input->post('message');
            emailFromKontak($nama,$email,$msg);
            redirect(base_url().'guest/kontak');
        }

        $page_data['top4artikel'] = $this->md_artikel->get4topArtikel();
        $page_data['kontak'] = $this->md_kontak->getKontakUtama();
        $page_data['page_name'] = 'kontak';
        $page_data['page_title'] = 'Kontak ';
        $this->load->view('index', $page_data);        
    }

    public function keberangkatan($param1=""){
        $ready =0;
        $data = array();
        $list_bulan = all_bulan();
        $no_hp = $param1;
        $cek = $this->md_pendaftaran_detail->getPendaftaranDetailByNoHp($no_hp);
        if($cek){
            for($i=0;$i<count($cek);$i++){
            $nama_jemaah = $cek[$i]->nama_jemaah;
            $status_verif = $cek[$i]->status_verifikasi;
            if(!$status_verif){
                $dt2['nama_jemaah'] = $nama_jemaah;
                $dt2['status_verifikasi'] = 'Belum Verifikasi';
                array_push($data,$dt2);
            } 
            else if($status_verif=='Tidak Valid'){
                $dt2['nama_jemaah'] = $nama_jemaah;
                $dt2['status_verifikasi'] = 'Tidak Valid';
                array_push($data,$dt2);
            }
            else {
                $dt2['status_verifikasi'] = 'Valid';
                $harus_bayar = $cek[$i]->harus_bayar;
                $pwk_id = $cek[$i]->pwk_id;
                // Data Paket
                    $f = $this->md_paketwaktu_kelas->getPaketWaktuKelasByIdV2($pwk_id);
                    $berangkat = $list_bulan[$f[0]->bulan_berangkat].' '.$f[0]->tahun_berangkat;
                    $kelas = $f[0]->kelas.' (<i classs="fa fa-users"><i>&nbsp;'.$f[0]->jumlah_sekamar.' orang per kamar)';

                    $x = $this->md_paket->getPaketByPaketId($f[0]->paket_id);
                    $nama_paket = $x[0]->nama_paket;

                $pendaftarandetail_id = $cek[$i]->pendaftarandetail_id;
                //Cek Status Syarat Jemaah
                    $p = $this->md_syarat_status->getSyaratStatusByPendaftarandetailId($pendaftarandetail_id);
                    if($p){
                        for($q=0;$q<count($p);$q++){
                            $y = $this->md_syarat_umroh->getSyaratUmrohById($p[$q]->syaratumroh_id);
                            $p[$q]->persyaratan = $y[0]->persyaratan;
                        }
                        $status = count($this->md_syarat_status->getSyaratBelumLengkapByJemaah($pendaftarandetail_id));
                        if($status==0){
                            $status_syarat = '<button class="btn btn-xs btn-green"><i class="fa fa-check"></i>&nbsp;Syarat Lengkap</button>';
                            $ready++;
                        } else {
                            $status_syarat = '<button class="btn btn-xs btn-red"><i class="fa fa-clock-o"></i>&nbsp;'.$status.' Syarat belum lengkap</button>';
                        }
                        $dt_syarat = $p;
                    } else {
                        $status_syarat = '<button class="btn btn-xs btn-yellow"><i class="fa fa-clock-o"></i>&nbsp;Syarat belum ditentukan</button>';
                    }

                //Cek Status Pembayaran Jemaah

                    $y = $this->md_pembayaran->getTotalPembayaranByJemaah($pendaftarandetail_id);
                    $total_bayar = $y[0]->total_pembayaran;
                    if($harus_bayar == $total_bayar){
                        $sisa = '0';
                        $status_pembayaran = '<button class="btn btn-xs btn-green"><i class="fa fa-check"></i>&nbsp;Pembayaran Lunas</button>';
                        $ready++;
                    } else {
                        $sisa = $harus_bayar - $total_bayar;
                        $status_pembayaran = '<button class="btn btn-xs btn-red"><i class="fa fa-clock-o"></i>&nbsp;Pembayaran Belum Lunas</button>';
                    }
                    $v = $this->md_pembayaran->getPembayaranByJemaah($pendaftarandetail_id);
                    foreach($v as $row){
                        $row->jumlah_bayar = rupiah_format($row->jumlah_bayar);
                    }
                    $dt_bayar=$v;

                // Ready kah ?
                    if($ready==2){
                        $status_jemaah = '<button class="btn btn-xs btn-green"><i class="fa fa-check"></i>&nbsp;Ready</button>';
                    } else {
                        $status_jemaah = '<button class="btn btn-xs btn-yellow"><i class="fa fa-clock-o"></i>&nbsp;Dalam Proses</button>';
                    }

                //Gathered All data
                    $dt2['nama_jemaah'] = $nama_jemaah;
                    $dt2['nama_paket'] = $nama_paket;
                    $dt2['kelas'] = $kelas;
                    $dt2['berangkat'] = $berangkat;                
                    $dt2['status_syarat'] = $status_syarat;
                    $dt2['status_pembayaran'] = $status_pembayaran; 
                    $dt2['status_jemaah'] = $status_jemaah;
                    $dt2['sisa'] = rupiah_format($sisa);
                    $dt2['harus_bayar'] = rupiah_format($harus_bayar);
                    $dt2['total_bayar'] = rupiah_format($total_bayar);
                    $dt2['list_syarat'] = $dt_syarat;
                    $dt2['list_bayar'] = $dt_bayar;
                    array_push($data,$dt2);
               }
            }   
                echo json_encode($data);
            die;
        }
        else {
            $x = 'Tidak Valid';
        }
        echo json_encode($x);
        die;
    }

    public function halaman($param1=""){
        $page_data['top4artikel'] = $this->md_artikel->get4topArtikel();
        $x = $this->md_halaman->getHalamanByLink($param1);
        if($x){
            foreach($x as $row){
                $page_data['judul'] = $row->judul;
                $page_data['meta_desc'] = $row->meta_desc;
                $page_data['meta_keyword'] = $row->meta_keyword;
                $page_data['meta_type'] = 'webiste';
                $page_data['meta_img'] = FALSE;
            }
            $page_data['halaman'] = $x;
            $page_data['page_name'] = 'halaman';
            $page_data['page_title'] = $row->judul;

            $this->load->view('index', $page_data);    
        } else {
            $page_data['top4artikel'] = $this->md_artikel->get4topArtikel();
            $page_data['page_name'] = '404';
            $page_data['page_title'] = '404 Page Not Found ';
            $this->load->view('index', $page_data);  
        }
    }

    public function daftar_tafahum($param1=""){
        if($param1=="add"){
            $data['nama'] = $this->input->post('nama');
            $data['no_hp'] = $this->input->post('no_hp');
            $data['email'] = $this->input->post('email');
            $data['tgl_submit'] = date('Y-m-d H:i:s');
            $data['status'] = 1;
            $this->md_tafahum->addTafahum($data);
            //Notif to Admin
                $judul = $data['nama'].' request pendaftaran tafahum';
                $ket = $data['nama'].' melakukan request pendaftaran tafahum';
                $link = 'administrator/tafahum';

                $v = $this->md_pengguna->getPenggunaByLevel(array('Kantor Pusat','Keuangan'));
                foreach($v as $row){
                    NotifToAdmin($judul,$ket,$link,$row->pengguna_id);
                }
            $this->session->set_flashdata('info_success','Silahkan tunggu konfirmasi via telephone dari Kantor Pusat ');
            redirect(base_url().'daftar-tafahum');
        }
        $page_data['top4artikel'] = $this->md_artikel->get4topArtikel();

        $page_data['page_name'] = 'daftar_tafahum';
        $page_data['page_title'] = 'Daftar Tafahum';

        $this->load->view('index', $page_data);   
    }

    public function konfirmasi($param1=""){
        if($param1=="add"){
            $data['nama'] = $this->input->post('nama');
            $data['no_hp'] = $this->input->post('no_hp');
            $data['email'] = $this->input->post('email');
            $data['jumlah_bayar'] = str_replace('.',"",$this->input->post('jumlah_bayar'));
            $data['bank_tujuan'] = $this->input->post('bank_tujuan');
            $data['nama_pemilik_rek'] = $this->input->post('nama_pemilik_rek');
            $data['keterangan'] = $this->input->post('keterangan');
            $data['tgl_bayar'] = date('Y-m-d H:i:s',strtotime($this->input->post('tgl_bayar')));
            $data['tgl_submit'] = date('Y-m-d H:i:s');
            $data['status'] = 1;
            $this->md_konfirmasi->addKonfirmasi($data);
            
            //Notif to Admin
                $temp = $this->md_konfirmasi->getLatestkonfirmasi();
                $judul = 'Konfirmasi Pembayaran oleh '.$data['nama'];
                $ket = $data['nama'].' melakukan konfirmasi pembayaran yang telah dilakukan';
                $link = 'administrator/konfirmasi/detail/'.encrypt($temp[0]->konfirmasi_id);

                $v = $this->md_pengguna->getPenggunaByLevel(array('Kantor Pusat','Keuangan'));
                foreach($v as $row){
                    NotifToAdmin($judul,$ket,$link,$row->pengguna_id);
                }
            $this->session->set_flashdata('info_success','Konfirmasi Pembayaran telah diterima. Kantor Pusat akan segera melakukan validasi terkait konfirmasi yang diajukan');
            redirect(base_url().'konfirmasi');
        }

        $page_data['dt_bank_transfer']         = $this->md_bank->getBankByJenis('Transfer');
        $page_data['top4artikel'] = $this->md_artikel->get4topArtikel();
        $page_data['page_name'] = 'konfirmasi';
        $page_data['page_title'] = 'Konfirmasi Pembayaran ';
        $this->load->view('index', $page_data);        
    }
    
    public function error(){
        $page_data['top4artikel'] = $this->md_artikel->get4topArtikel();
        $page_data['page_name'] = '404';
        $page_data['page_title'] = '404 Page Not Found ';
        $this->load->view('index', $page_data);    
    }

}