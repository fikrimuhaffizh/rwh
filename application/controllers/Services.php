<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	require APPPATH . '/libraries/REST_Controller.php';

	class Services extends REST_Controller {
		/*
			201 = Request POST Successful
			401 = Unauthorized
			404 = Not Found
		*/
	    function __construct($config = 'rest') {
	        parent::__construct($config);
	        $this->load->model('md_media_detail');
	        $this->load->model('md_paket');
	        $this->load->model('md_paket_maskapai');
	        $this->load->model('md_maskapai');
	        $this->load->model('md_kantor_cabang');
	        $this->load->helper('encrypt');
	        $this->load->helper('rwh');
	        $this->load->helper('datetime');
	        $this->load->helper('format');
	        $this->load->helper('email');
	    }

	    public function getManualLogin_post() { 
	    	$this->load->model('md_member');
	    	$email = $this->post('email');
	    	$pass = hash('sha512', $this->post('password'));
	    	$data=array();
	    	if($email && $pass){
	    		$valid = TRUE;
	    		$data = $this->md_member->getMemberByEmailByPass($email,$pass);
	    		if(!$data){
	    			$valid = FALSE;
	    		}
		    	$response = ($data) ? 201 : 401;
	    	} 
	    	else {
	    		$response = 404;
	    	}
		    $this->response(array("response"=>$response,"result"=>$data));
	    }

	    public function getArtikel_post() { 
	    	$this->load->model('md_artikel');
	        $data = $this->md_artikel->getAllArtikel();

	    	//Get Media Link and Artikel Link
		    	if($data){
		    		foreach($data as $row){
		    			$x = $this->md_media_detail->getMediaDetailByMediaId($row->media_id);
		    			$row->media_link_medium = $x[1]->media_link;
		    			$row->media_link_small = $x[1]->media_link;
		    			$row->artikel_link = 'artikel/detail/'.encrypt($row->artikel_id).'/'.link_replace($row->judul);
		    			$row->tgl_post=date('d-F-Y',strtotime($row->tgl_post));
		    		}
		    	}

	        //Response Request
		    	$response = ($data) ? 201 : 404;
		        $this->response(array("response"=>$response,"result"=>$data));
	    }

	    public function getKontak_post() { 
	    	$this->load->model('md_kontak');
	        $data = $this->md_kontak->getAllKontak();


	        //Response Request
		    	$response = ($data) ? 201 : 404;
		        $this->response(array("response"=>$response,"result"=>$data));
	    }

	    public function getJadwalManasik_post() { 
	    	$this->load->model('md_jadwal_manasik');
	        $data = $this->md_jadwal_manasik->getAllJadwalManasikAfterToday();

	        //Response Request
		    	$response = ($data) ? 201 : 404;
		        $this->response(array("response"=>$response,"result"=>$data));
	    }

	    public function getGaleri_post() { 
	    	$this->load->model('md_galeri_item');
	    	$this->load->model('md_galeri_album');
	    	$id = $this->post('galerialbum_id');
	    	if($id){
	    		
	    		$data = $this->md_galeri_item->getGaleriItemByGaleriAlbumId($id);
	    	}
	    	else{
	    		
	        	$data = $this->md_galeri_album->getAllGaleriAlbum();
	        	foreach($data as $row){
	        		$y = $this->md_galeri_item->getGaleriItemByGaleriAlbumId($row->galerialbum_id);
		    		$row->jumlah_item = count($y);
	        	}
	    	}
	    	//Get Media Link dan Jumlah Item
		    	if($data){
		    		foreach($data as $row){
		    			$x = $this->md_media_detail->getMediaDetailByMediaId($row->media_id);
		    			$row->media_link_large = $x[0]->media_link;
		    			$row->media_link_medium = $x[1]->media_link;
		    			$row->media_link_small = $x[2]->media_link;

		    			
		    		}
		    	}

	        //Response Request
		    	$response = ($data) ? 201 : 404;
		        $this->response(array("response"=>$response,"result"=>$data));
	    }

	    public function getMerchant_post() { 
	    	$this->load->model('md_merchant');
	        $data = $this->md_merchant->getAllMerchant();

	    	//Get Media Link
		    	if($data){
		    		foreach($data as $row){
		    			$x = $this->md_media_detail->getMediaDetailByMediaId($row->media_id);
		    			$row->media_link_large = base_url().$x[1]->media_link;
		    			$row->media_link_medium = base_url().$x[1]->media_link;
		    			$row->media_link_small = base_url().$x[1]->media_link;
		    		}
		    	}

	        //Response Request
		    	$response = ($data) ? 201 : 404;
		        $this->response(array("response"=>$response,"result"=>$data));
	    }

	    public function getSlide_post() { 
	    	$this->load->model('md_slide');
	        $data = $this->md_slide->getAllSlide();

		    	if($data){
		    		foreach($data as $row){
		    			$row->url_slide = base_url().$row->url_slide;
		    			$row->url_thumbnail = base_url().$row->url_thumbnail;
		    		}
		    	}

	        //Response Request
		    	$response = ($data) ? 201 : 404;
		        $this->response(array("response"=>$response,"result"=>$data));
	    }

	    public function resetPassword_post(){
	    	$this->load->model('md_member');
	    	$email = $this->post('email');
	    	$password = generateRandomString(8);
			$data['password'] = hash('sha512', $password);
			$cek = $this->md_member->getMemberByEmail($email);
			if($cek){
				$this->md_member->updateMember($cek[0]->member_id,$data);
	            $result = emailResetPassword($cek[0]->nama_lengkap,$email,$password);
				$response = ($result) ? 201 : 404;
	            $data = "Email Valid";
			} else {
				$response = 404;
				$data = "Email Tidak Valid";
			}

			$this->response(array("response"=>$response,"result"=>$data));
		}

		public function getDataFormBooking_post(){
			$keberangkatan = $this->input->post('keberangkatan');
			$tahun = $this->input->post('tahun');
			$jenispaket_id = $this->input->post('jenispaket_id');

			if($keberangkatan && !$tahun){
				$data = $this->md_paket->getTahunBerangkatDistinctByJenisPaketIdByBerangkatAwal($jenispaket_id,$keberangkatan);
				$all=array("tahun_berangkat"=>"Semua Tahun");

			}
			else if($keberangkatan && $tahun){
				$bulan = all_bulan();
				$data = $this->md_paket->getBulanBerangkatDistinctByJenisPaketIdByBerangkatAwalByTahun($jenispaket_id,$keberangkatan,$tahun);
				foreach($data as $row){
					$row->bulan_text = $bulan[$row->bulan_berangkat];
				}
				$all=array("bulan_berangkat"=>"Semua Bulan","bulan_text"=>"Semua Bulan");
			}
			else{
				$data = $this->md_paket->getBerangakatawalDistinctByJenisPaketId($jenispaket_id);
				foreach($data as $row){
		            $y = $this->md_kantor_cabang->getKantorCabangById($row->berangkat_awal);
		            $row->provinsi = $y[0]->provinsi;
		            $row->kantorcabang_id = encrypt($y[0]->kantorcabang_id);
		        }
		        $all=array("berangkat_awal"=>"Semua Keberangkatan","kantorcabang_id"=>NULL,"provinsi"=>"Semua Keberangkatan");
			} 
			//Tambahan list "Semua Keberangkatan/Tahun/Bulan"
				array_push($data,$all);

	        //Response Request
		    	$response = ($data) ? 201 : 404;
		        $this->response(array("response"=>$response,"result"=>$data));
		}

		public function getListPaket_post(){
			$berangkat_awal = $this->input->post('berangkat_awal');
			$bulan = $this->input->post('bulan');
			$tahun = $this->input->post('tahun');
			$jenispaket_id = $this->input->post('jenispaket_id');	

			$all_bulan = all_bulan_short();
			$data = $this->md_paket->getPaketBySearch($berangkat_awal,$bulan,$tahun,"",$jenispaket_id);
			foreach($data as $row){
		        //Maskapai (d)
	                $temp = $this->md_paket_maskapai->getPaketMaskapaiByPaketId($row->paket_id);
	                for($j=0;$j<count($temp);$j++){
	                    $_temp = $this->md_maskapai->getMaskapaiById($temp[$j]->maskapai_id);
	                        $__temp = $this->md_media_detail->getMediaDetailByMediaId($_temp[0]->media_id);
	                        $d[$j]['logo'] = $__temp[2]->media_link;
	                }
	            $row->maskapai = $d;
				$row->paketwaktu_id = encrypt($row->paketwaktu_id);
				$row->bulan_text = $all_bulan[$row->bulan_berangkat];
		        $row->lama_hari = $row->lama_hari.' Hari Perjalanan';
			}
			
	        //Response Request
		    	$response = ($data) ? 201 : 404;
		        $this->response(array("response"=>$response,"result"=>$data));
		}

		public function getPaketDetail_post(){
			$this->load->model('md_jenis_paket');
			$this->load->model('md_media_detail');
			$this->load->model('md_kantor_cabang');
			$this->load->model('md_maskapai');
			$this->load->model('md_paket_akomodasi');
			$this->load->model('md_hotel');
			$this->load->model('md_kelas');
			$this->load->model('md_paket_waktu');
			$paketwaktu_id = decrypt($this->input->post('paketwaktu_id'));
			// echo $paketwaktu_id;

			//Just Copy Form Guest COntroller
            	$y = $this->md_paket_waktu->getPaketWaktuByPaketWaktuIdIdV3($paketwaktu_id);
                $paket_id = $y[0]->paket_id;
                $id = $paket_id;
                $data = array();
                $x = $this->md_paket->getPaketByPaketId($id);
                for($i=0;$i<count($x);$i++){
                    $a = $this->md_jenis_paket->getJenisPaketByJenisPaketId($x[$i]->jenispaket_id);
                    $b = $this->md_media_detail->getMediaDetailByMediaId($x[$i]->media_mobile_id);
                    if(!$b)
                    	$paket_pic = null;
                    else
                    	$paket_pic = $b[0]->media_link;

                    $c = $this->md_kantor_cabang->getKantorCabangById($x[$i]->berangkat_awal);

                    //Maskapai (d)
                        $temp = $this->md_paket_maskapai->getPaketMaskapaiByPaketId($id);
                        for($j=0;$j<count($temp);$j++){
                            $_temp = $this->md_maskapai->getMaskapaiById($temp[$j]->maskapai_id);
                            $d[$j]['nama_maskapai'] = $_temp[0]->nama_maskapai;
                            $d[$j]['deskripsi'] = $_temp[0]->deskripsi;
                            //Ambil Foto
                                $__temp = $this->md_media_detail->getMediaDetailByMediaId($_temp[0]->media_id);
                                $d[$j]['logo'] = $__temp[1]->media_link;
                        }
                        
                    //Paket Akomodasi (e)
                        $temp = $this->md_paket_akomodasi->getpaketAkomodasiByPaketId($id);
                        for($j=0;$j<count($temp);$j++){
                            $_temp = $this->md_hotel->getHotelById($temp[$j]->hotel);
                            $e[$j]['nama_hotel'] = $_temp[0]->nama_hotel;
                            $e[$j]['kota'] = $_temp[0]->kota;
                            $e[$j]['bintang'] = $_temp[0]->bintang;
                            $e[$j]['deskripsi'] = $_temp[0]->deskripsi;
                            //Ambil Foto
                                $__temp = $this->md_media_detail->getMediaDetailByMediaId($_temp[0]->media_id);
                                $e[$j]['foto'] = $__temp[1]->media_link;
                                $e[$j]['foto_besar'] = $__temp[0]->media_link;
                        }

                    //Paket Waktu
                        $temp = $y;
                        $bulan = all_bulan();
                        for($j=0;$j<count($temp);$j++){
                            $__temp = $this->md_kelas->getKelasById($temp[$j]->kelas_id);
                            $f[$j]['pwk_id'] = encrypt($temp[$j]->pwk_id);
                            $f[$j]['paketwaktu_id'] = encrypt($temp[$j]->paketwaktu_id);
                            $f[$j]['kelas'] = $__temp[0]->kelas;
                            $f[$j]['jumlah_sekamar'] = $__temp[0]->jumlah_sekamar.' Orang/kamar';
                            $f[$j]['harga'] = rupiah_format($temp[$j]->harga-$temp[$j]->diskon);
                            $f[$j]['harga_awal'] = rupiah_format($temp[$j]->harga_awal);
                            $f[$j]['diskon'] = rupiah_format($temp[$j]->diskon);
                            $f[$j]['promo'] = $temp[$j]->promo;
                            $f[$j]['berangkat'] = $bulan[$temp[$j]->bulan_berangkat].' '.$temp[$j]->tahun_berangkat;
                        }

                    //Gabung Semua data paket
                        $dt['paket_id'] = encrypt($x[0]->paket_id);
                        $dt['paket_pic'] = $paket_pic;
                        $dt['nama_paket'] = $x[0]->nama_paket;
                        $dt['jenis_paket'] = $a[0]->jenis_paket;
                        $dt['berangkat_awal'] = $c[0]->provinsi;
                        $dt['berangkat_transit'] = $x[0]->berangkat_transit;
                        $dt['berangkat_landing'] = $x[0]->berangkat_landing;
                        $dt['info_plus'] = $x[0]->info_plus;
                        $dt['lama_hari'] = $x[0]->lama_hari.' Hari';
                        $dt['mulai_publish'] = date('d-F-Y H:i:s',strtotime($x[0]->mulai_publish));
                        $dt['akhir_publish'] = date('d-F-Y H:i:s',strtotime($x[0]->akhir_publish));
                        $dt['maskapai'] = $d;
                        $dt['paket_akomodasi'] = $e;
                        $dt['paket_waktu'] = $f;
                }
                $data = $dt;

	        //Response Request
		    	$response = ($data) ? 201 : 404;
		        $this->response(array("response"=>$response,"result"=>$data));
		}

		public function bookingJemaah_post(){
			$this->load->model('md_pendaftaran_detail');
	    	$this->load->model('md_member');

	    	$jemaah = $this->post('nama_jemaah');
	    	$handphone  = $this->post('handphone');
	    	$email  = $this->post('email');
	    	$alamat  = $this->post('alamat');
	    	$jenis_kelamin  = $this->post('jenis_kelamin');
	    	$jenis_pendaftaran  = $this->post('jenis_pendaftaran');
	    	$tgl_lahir  = $this->post('tgl_lahir');
	    	$pwk_id  = $this->post('pwk_id');
	    	$paket_id = $this->post('paket_id');
	    	
	    	// $member_id  = decrypt($this->post('member_id'));
	    	$harga = remove_rupiah_format($this->post('harga'));

	    	for($i=0;$i<count($jemaah);$i++){
	    		$data['nama_jemaah'] = $jemaah[$i];
	    		$data['no_hp'] = $handphone[$i];
	    		$data['email'] = $email[$i];
	    		$data['alamat'] = $alamat[$i];
	    		$data['jenis_kelamin'] = $jenis_kelamin[$i];
	    		$data['jenis_pendaftaran'] = $jenis_pendaftaran[$i];
	    		$data['tgl_lahir'] = date('Y-m-d',strtotime($tgl_lahir[$i]));
	    		$data['status_keberangkatan'] = 'Dalam Proses';
	    		$data['tgl_submit'] = date('Y-m-d H:i:s');
                $data['harus_bayar'] = $harga;
                $data['status'] = 1;

                if($pwk_id){
                	$data['pwk_id'] = decrypt($pwk_id);
                }
                else if($paket_id){
                	$data['paket_id'] = decrypt($paket_id);
                	// $data['tahun_est_berangkat'] = date('Y')+$this->input->post('masa_tunggu');
                }

	    		$this->md_pendaftaran_detail->addPendaftaranDetail($data);
	    	}

	  //   	$password = generateRandomString(8);
			// $data['password'] = hash('sha512', $password);
			// $cek = $this->md_member->getMemberByEmail($email);
			// if($cek){
			// 	$this->md_member->updateMember($cek[0]->member_id,$data);
	  //           $result = emailResetPassword($cek[0]->nama_lengkap,$email,$password);
			// 	$response = ($result) ? 201 : 404;
	  //           $data = "Email Valid";
			// } else {
			// 	$response = 404;
			// 	$data = "Email Tidak Valid";
			// }

			$this->response(array("response"=>'201',"result"=>$harga));
		}		
	}
?>