<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Sitemap extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('md_artikel');
		$this->load->model('md_halaman');
		$this->load->model('md_paket');
		$this->load->helper('encrypt');
		$this->load->helper('rwh');
		$this->load->helper('userLog');

	}
	public function index()
	{

		$halaman[]=base_url().'halaman-sitemap.xml';
			$hal=$this->md_halaman->getHalamanHeader();
			$tgl[]=date('Y-m-d\Th:i:s',strtotime($hal[0]->tgl_post));


		$halaman[]=base_url().'artikel-sitemap.xml';
			$berita=$this->md_artikel->get3topArtikel();
			$tgl[]=date('Y-m-d\Th:i:s',strtotime($berita[0]->tgl_post));
	
		$link['data']['url']=$halaman;
		$link['data']['tgl']=$tgl;
		$link['data']['sitemap']=true;
		$this->load->view('page/sitemap',$link);
	}
	public function halaman(){
		//Halaman Reguler
			$halaman_reguler=$this->md_halaman->getHalamanReguler();
			foreach ($halaman_reguler as $x){
				$halaman[]=base_url().$x->link_halaman;
				$tgl[]=date('Y-m-d\Th:i:s',strtotime($x->tgl_post));
			}

		//Halaman Tambahan
			$halaman_tambahan=$this->md_halaman->getHalamanTambahan();
			if($halaman_tambahan){
				foreach ($halaman_tambahan as $y){
					$halaman[]=base_url().$y->link_halaman;
					$tgl[]=date('Y-m-d\Th:i:s',strtotime($y->tgl_post));
				}		
			}

		//Halaman Statik
			$tgl_create = "2017-11-10T14:15:42";
			$page = array('','haji','umroh','wisata','status','manasik',
						  'merchant','galeri','kantor','kontak','daftar','konfirmasi');
			for($i=0;$i<count($page);$i++){
				$halaman[]=base_url().$page[$i];	
				$tgl[]=$tgl_create;
			}
		//Halaman Paket Detail
			$x = $this->md_paket->getPaketBySearch("Semua Keberangkatan","Semua Bulan","Semua Tahun","","");
			// echo_array($x);
			if($x){
				foreach($x as $row){
					$tgl[]=date('Y-m-d\Th:i:s',strtotime($row->last_update));
					$halaman[]=base_url().'paket/detail/'.encrypt($row->paketwaktu_id).'/'.link_replace($row->nama_paket);
				}
			}


		$link['data']['url']=$halaman;
		$link['data']['tgl']=$tgl;
		$link['data']['sitemap']=false;
		$this->load->view('page/sitemap',$link);
	}
	public function artikel(){
		$berita=$this->md_artikel->getAllArtikel();
		foreach($berita as $x){
			$link[]=base_url().'detail/'.encrypt($x->artikel_id).'/'.link_replace($x->judul);
			$tgl[]=date('Y-m-d\Th:i:s',strtotime($x->tgl_perubahan));
		}
		$link['data']['url']=$link;
		$link['data']['tgl']=$tgl;
		$link['data']['sitemap']=false;
		$this->load->view('page/sitemap',$link);
	}	
	
}
