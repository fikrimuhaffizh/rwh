<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

function emailConfig(){
        $config = array();
        $config['charset'] = 'iso-8859-1';
        $config['useragent'] = 'Codeigniter';
        $config['protocol'] = "smtp";
        $config['mailtype'] = "html";
        $config['smtp_host'] = "ssl://smtp.googlemail.com";
        $config['smtp_port'] = "465";
        $config['smtp_timeout'] = "465";
        $config['smtp_user'] = "riauwisatahati18@gmail.com";
        $config['smtp_pass'] = "P@s5w0rd";
        $config['crlf'] = "\r\n";
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;
        return $config;
}

function emailFromKontak($nama,$email,$msg){
        $CI = get_instance();
        $CI->load->library('email');
        $CI->email->initialize(emailConfig());
        $CI->email->from('riauwisatahati18@gmail.com', 'Riau Wisata Hati');
        $CI->email->to('rwh@gmail.com');
        $CI->email->subject('Pertanyaan oleh '.$nama);
        $CI->email->message($msg);                        
        if($CI->email->send()) 
        $CI->session->set_flashdata("email_sent","success");   
        else {
        echo $CI->email->print_debugger();
        die;
        }
        redirect(base_url().'guest/kontak');

        
}
function emailResetPassword($nama,$email,$password){
        $CI = get_instance();
        $CI->load->library('email');
        $CI->email->initialize(emailConfig());
        $CI->email->from('riauwisatahati18@gmail.com', 'Riau Wisata Hati');
        $CI->email->to($email);
        $CI->email->subject('Reset Password Member RWH');
        $CI->email->message("
        Assalamualaikum ".$nama.",<br><br>

        Berikut password baru yang bisa anda gunakan untuk login Member RWH  :<br><br>
        Email : <b>".$email."</b><br>
        Password  : <b>".$password."</b><br>

        <br>Silahkan login menggunakan password diatas dan lakukan perubahan password di menu Edit Profile Member<br><Br>
        Terima Kasih<br><br>

        <i>Do not reply to this computer-generated email.<br> 
        Riau Wisata Hati - JL. Soekarno Hatta No. 555 E & F, Pekanbaru</i>                      
        
        ");                        
        if($CI->email->send()) 
                return 'success'; 
        else {
                echo $CI->email->print_debugger();
                return FALSE;
        }
}


function emailVerifPendaftaran($nama,$email,$link){
        $CI = get_instance();
        $CI->load->library('email');
        $CI->email->initialize(emailConfig());
        $CI->email->from('riauwisatahati18@gmail.com', 'Riau Wisata Hati');
        $CI->email->to($email);
        $CI->email->subject('Selamat Datang Di Riau Wisata Hati');
        $CI->email->message("
        Assalamualaikum ".$nama.",<br><br>

        Selamat bergabung menajdi Member RWH ! <br>

        Silahkan klik link verifikasi pendaftaran Member Riau Wisata Hati dibawah ini : <br><br>

        Link : <b>".$link."</b><br><br>

        Terima Kasih<br><br>

        <i>Do not reply to this computer-generated email.<br> 
        Riau Wisata Hati - JL. Soekarno Hatta No. 555 E & F, Pekanbaru</i>')                      
        
        ");                        
        if($CI->email->send()) 
                return 'success'; 
        else {
                echo $CI->email->print_debugger();
                return FALSE;
        }
}

function emailTolakSyarat($nama,$email,$link,$syarat){
        $CI = get_instance();
        $CI->load->library('email');
        $CI->email->initialize(emailConfig());
        $CI->email->from('riauwisatahati18@gmail.com', 'Riau Wisata Hati');
        $CI->email->to($email);
        $CI->email->subject('Verifikasi Syarat Jemaah');
        $CI->email->message("
        Assalamualaikum ".$nama.",<br><br>

        Mohon maaf, terkait syarat <b>".$syarat."</b> jemaah, Kantor Pusat <b>Menolak</b> syarat yang diupload.  <br>

        Silahkan klik link dibawah ini untuk mengupload ulang syarat : <br><br>

        Link : <b>".$link."</b><br><br>

        Terima Kasih<br><br>

        <i>Do not reply to this computer-generated email.<br> 
        Riau Wisata Hati - JL. Soekarno Hatta No. 555 E & F, Pekanbaru</i>')                      
        
        ");                        
        if($CI->email->send()) 
                return 'success'; 
        else {
                echo $CI->email->print_debugger();
                return FALSE;
        }
}
?>