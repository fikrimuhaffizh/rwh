<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
	
	function rupiah_format($angka){
         return "Rp " . number_format($angka,0,',','.');
    }

    function remove_number_format($angka){
        $angka = str_replace(".","", $angka);
        $angka = str_replace(",","", $angka);
        return $angka;
    }

    function remove_rupiah_format($angka){
    	$angka = str_replace("Rp ","", $angka);
        $angka = str_replace(".","", $angka);
        $angka = str_replace(",00","", $angka);
        return $angka;
    }

    function word_cut($text,$num_char){
        $char     = $text{$num_char - 1};
        while($char != ' ') {
            $char = $text{++$num_char}; // Cari spasi pada posisi 51, 52, 53, dst...
        }
        return substr($text, 0, $num_char) . '...';
    }
    
    function first_char($word){
        $x = strip_tags(str_replace('...','',word_cut($isi,1)));
        return $x[0];
    }

    function gathered_data($th=array()){
        for($i=0;$i<count($th);$i++){
            $row[]  = $th[$i]; 
        }
        return $row;
    }

    function number_format_decimal($angka){
        return number_format($angka,2,'.',',');
    }
?>