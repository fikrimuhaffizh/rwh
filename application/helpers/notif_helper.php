<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
	
function NotifToAdmin($judul,$ket,$link,$pengguna_id){
	$CI = get_instance();
    $data['jenis_notif'] = 'Web';
    $data['judul'] = $judul;
    $data['Keterangan'] = $ket;
    $data['link'] = $link;
    $data['tgl'] = date('Y-m-d H:i:s');
    $data['baca'] = 0;
    $data['pengguna_id'] = $pengguna_id;
    $data['status'] = 1;
    $CI->md_notifikasi->addNotifikasi($data);
}


?>