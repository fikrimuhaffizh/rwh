<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

// /*
//     ------------------ Pagination -------------------- 
//     ------------ Do not touch anything !!! -----------
// */
//     function get_datatables_query($model,$function,$column_order,$order,$column_search,$param=array())
//     {
//         $CI = get_instance(); 

//         $CI->$model->$function($param);

//             //Search
//                 $i = 0;
//                 foreach ($column_search as $item){
//                     if($_POST['search']['value']){
//                         if($i===0){
//                             $CI->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
//                             $CI->db->like($item, $_POST['search']['value']);
//                         }
//                         else{
//                             $CI->db->or_like($item, $_POST['search']['value']);
//                         }

//                         if(count($column_search) - 1 == $i) //last loop
//                             $CI->db->group_end(); //close bracket
//                     }
//                     $i++;
//                 }
//             //Order
//                 if(isset($_POST['order'])){
//                     $CI->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
//                 } 
//                 else if(isset($order)){
//                     $order = $order;
//                     $CI->db->order_by(key($order), $order[key($order)]);
//                 }
//     }

//     function get_datatables($model,$function,$column_order,$order,$column_search,$param=array())
//     {
//         $CI = get_instance(); 
//         get_datatables_query($model,$function,$column_order,$order,$column_search,$param);
//         if($_POST['length'] != -1){
//             $CI->db->limit($_POST['length'], $_POST['start']);
//         }
//         $query = $CI->db->get();
//         return $query->result();
//     }

//     function count_filtered($model,$function,$column_order,$order,$column_search,$param=array())
//     {
//         $CI = get_instance(); 
//         get_datatables_query($model,$function,$column_order,$order,$column_search,$param);
//         $query = $CI->db->get();
//         return $query->num_rows();
//     }

//     function count_all($model='',$function='',$param=array())
//     {
//         $CI = get_instance(); 
//         $temp = $CI->$model->$function($param);
//         $temp = $CI->db->get();
//         return count($temp->result());
//     }  

//     function gathered_data($th=array()){

//         for($i=0;$i<count($th);$i++){
//             $row[]  = $th[$i];
            
//         }
//         return $row;
//     }  

//     function data_record($draw,$model,$function,$column_order,$order,$column_search,$param=array(),$data){
//         return $output = array(
//             "draw"            => $draw,
//             "recordsTotal"    => count_all($model,$function,$param),
//             "recordsFiltered" => count_filtered($model,$function,$column_order,$order,$column_search,$param),
//             "data"            => $data);
//     }       
    

    function config_pagination($per_page,$base_url,$total_rows,$uri_segment){
        $CI = get_instance();
        $config['base_url'] = base_url().$base_url;
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $per_page;
        $config["uri_segment"] = $uri_segment;
        $config['use_page_numbers'] = TRUE;

        //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $CI->pagination->initialize($config);
        $page = $CI->uri->segment($uri_segment) ? $CI->uri->segment($uri_segment) : 0;
        return $page;
    }