<?php 

function get_menu_tree($parent_id) 
{
    $CI = get_instance();
    $menu = "";
    $query = " SELECT * FROM menu where status='1' and parent_id='" .$parent_id . "' ";
    $hasil = $CI->db->query($query)->result();  

    foreach($hasil as $row)
    {
       $menu .="<li class='nav-item'><a href='".$row->link."'>".$row->menu_name."</a>";           
       $menu .= "</li>";
    }
    
    return $menu;
} 
function echo_array($data){
    echo '<pre>';
    print_r($data);
    echo '</pre>';
    
}
function dtCompany(){
    $CI                = get_instance();
    $x                 = $CI->md_company->getCompany();
    $dt['nama_usaha']  = $x[0]->nama_usaha;
    $dt['nama_brand']  = $x[0]->nama_brand;
    $dt['alamat']      = $x[0]->alamat;
    $dt['kota']        = $x[0]->kota;
    $dt['provinsi']    = $x[0]->provinsi;
    $dt['kontak']      = $x[0]->kontak;
    $dt['keterangan']  = $x[0]->keterangan;
    $dt['logo_large']  = base_url().$x[0]->img_large;
    $dt['logo_medium'] = base_url().$x[0]->img_medium;
    $dt['logo_small']  = base_url().$x[0]->img_small;
    return $dt;
}

function AtributJemaah($row,$jenis){
    if($jenis == 'Visa'){
        if($row->total_visa>0){
            $style_visa =  'font-green-jungle';
            $title_visa = 'Visa Progresive (Sudah Bayar)';
        } else {
            $style_visa =  'font-dark';
            $title_visa = 'Visa Progresive (Belum Bayar)';
        }
        $progrv = $row->progressive=='Ya' ? '<a class="tooltips '.$style_visa.'" data-placement="top" title="'.$title_visa.'"><i class="fa fa-cc-visa"></i></a> ' : '';
        return $progrv;
        
    } 
    else if($jenis=='Mahram'){

        if($row->total_mahram>0){
            $style_mahram =  'font-dark';
            $title_mahram = 'Ada Mahram (Sudah Bayar)';
        } else {
            $style_mahram =  'font-dark';
            $title_mahram = 'Ada Mahram (Belum Bayar)';
        }
        $mahrm = $row->ada_mahram == 'Ya' ? '<a class="tooltips '.$style_mahram.'" data-placement="top" title="'.$title_mahram.'"><i class="fa fa-user-plus"></i></a> ' : '';
        return $mahrm;
    }
    else if($jenis=='Infant'){
        $x = $row->infant>0 ? '<a class="tooltips font-yellow-crusta" data-placement="top" title="Jemaah Infant"><i class="fa fa-child"></i></a> ' : '';
        return $x;
    }
    else if($jenis=='Tour Leader'){
        $x = $row->jenis_jemaah=='Tour Leader' ? '<a class="tooltips font-yellow" data-placement="top" title="Tour Leader"><i class="fa fa-trophy"></i></a> ' : '';
        return $x;
    }
    else if($jenis=='Sharing Bed'){
        $x = $row->sharing_bed>0 ? '<a class="tooltips" data-placement="top" title="Jemaah memilih Sharing Bed"><i class="fa fa-bed"></i></a> ' : '';
        return $x;
    }
    else if($jenis=='Kelas Pesawat'){
        if($row->kelas_pesawat == 'First Class')
            $x = '<a class="tooltips font-red" data-placement="top" title="Jemaah memilih First Class"><i class="fa fa-star"></i></a> ';
        else if ($row->kelas_pesawat == 'Business Class')
            $x = '<a class="tooltips font-blue" data-placement="top" title="Jemaah memilih Business Class"><i class="fa fa-star"></i></a> ';
        else
            $x = '';
        return $x;
    }
}

function roomTextColor($val){
    if($val == 'D' || $val == 'Double')
        $x = '<b class="font-green-jungle">Double</b>';
    else if($val == 'T' || $val == 'Triple')
        $x = '<b class="font-purple">Triple</b>';
    else if($val == 'Q' || $val == 'Quad')
        $x = '<b class="font-red">Quad</b>';
    return $x;   
}

function cetakHeader(){
    $CI = get_instance();
    $x ='   <div class="col-md-12">
                <span style="font-size: 30px"><b>'.dtCompany()['nama_usaha'].'</b>
                    <span class="pull-right"><img src="'.dtCompany()['logo_medium'].'"></span>
                </span>
                <h4 style="font-size: 13px">
                    '.$CI->session->userdata('alamat_kantor').', '.$CI->session->userdata('kota_kantor').'
                    <br>
                 Call Center :  '. $CI->session->userdata('telp_kantor').'
                </h4>
            </div>';
    return $x;
}




function formatKode($kode){
    $length_kode = strlen($kode);
    if($length_kode == 0){
        $c = '-';
    }
    else if($length_kode == 4){
        $a = substr($kode,0,2);
        $b = substr($kode,2,2);
        $c = $a.'-'.$b;
    } else if($length_kode == 5){
        $a = substr($kode,0,2);
        $b = substr($kode,2,3);
        $c = $a.'-'.$b;
    } else if($length_kode == 7){
        $a = substr($kode,0,2);
        $b1 = substr($kode,2,2);
        $b2 = substr($kode,4,3);
        $c = $a.'-'.$b1.'-'.$b2;
    } else $c = $kode;
    return $c;
}

function getMember($jemaah){
    $CI = get_instance();
    for($i=0;$i<count($jemaah);$i++){
        $y = $CI->md_member->getMemberByMemberId($jemaah[$i]->member_id);
        if($y)
            $jemaah[$i]->nama_member = $y[0]->nama_lengkap;
        else
            $jemaah[$i]->nama_member = '-';
        }
    return $jemaah;
}

function getAtributFormPaket($jenispaket_id){
    $CI = get_instance();
    //Pengambilan data Paket Reguler
    if($jenispaket_id != 2){
        //Tahun Berangkat
            $z = $CI->md_paket->getTahunBerangkatDistinctByJenisPaketId($jenispaket_id);

        //Berangkat Transit
            $w = $CI->md_paket->getTransitDistinctByJenisPaketId($jenispaket_id);

        //Berangkat Awal
            $x = $CI->md_paket->getBerangakatawalDistinctByJenisPaketId($jenispaket_id);
            for($i=0;$i<count($x);$i++){
                $y = $CI->md_kota->getKotaById($x[$i]->berangkat_awal);
                $x[$i]->provinsi = '('.$y[0]->kode_kota.') '.$y[0]->nama_kota;
                $x[$i]->kota_id = encrypt($y[0]->kota_id);
            }

        //Bulan
            $y = $CI->md_paket->getBulanBerangkatDistinctByJenisPaketId($jenispaket_id);
            $bulan = all_bulan();
            for($i=0;$i<count($y);$i++){
                $y[$i]->bulan = $bulan[$y[$i]->bulan_berangkat];
            }

        //Tahun
            $v = $CI->md_maskapai->getAllMaskapai();
            foreach($v as $row){
                $row->maskapai_id = encrypt($row->maskapai_id);
            }
            
        
        $data['maskapai']        = $v;
        $data['transit']         = $w;
        $data['berangkat_awal']  = $x;
        $data['bulan_berangkat'] = $y;
        $data['tahun_berangkat'] = $z;
        $data['jenis']           = 'Reguler';
    } 
    //Pengambilan data Paket haji
    else {
        $x = $CI->md_paket->getAllPaketHaji();
        foreach($x as $row){
            $row->harga_terakhir = rupiah_format($row->harga_terakhir);
            $row->paket_id = encrypt($row->paket_id);
        }
        $data['haji'] = $x;
        $data['jenis'] = 'Haji';
    }
    return $data;
}

function link_replace($string){
    $string = str_replace('|','',$string);
    $string = str_replace(array('[\', \']'), '', $string);
    $string = preg_replace('/\[.*\]/U', '', $string);
    $string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
    $string = htmlentities($string, ENT_COMPAT, 'utf-8');
    $string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );

    $string = preg_replace('/\s+/' , '-', $string);
    $string = preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $string);
    return strtolower(trim($string, '-'));
}

function generateKodePerwakilanDaerah($parent){
    $CI = get_instance();
    //Ambil kode afiliasi parent yang baru
        $w = $CI->md_kantor_cabang->getKantorCabangById($parent);
        $kode_prov = $w[0]->kode_afiliasi;

    //Cek perwakilan daerah terakhir yang ditambahkan dengan parent yang sama
        $x = $CI->md_kantor_cabang->getLatestPerwakilanDaerahByParent($parent);
        if($x){
            //Ambil sequence nya, tambahkan 1 lalu sesuaikan format.
                $last_seq = $x[0]->sequence;
                $new_seq = $last_seq+1;

                if(strlen($new_seq)==1)
                    $kode_perwakilan = '0'.$new_seq;
                else
                    $kode_perwakilan = $new_seq;
                
                $data['sequence'] = $new_seq;
                $data['kode_afiliasi'] = $kode_prov.$kode_perwakilan;
        } else {
            $data['sequence'] = 1;
            $data['kode_afiliasi'] = $kode_prov.'01';    
        }

    $data['parent'] = $parent;
    return $data;
}

function generateKodeKantorCabang(){
    $CI = get_instance();
    $x = $CI->md_kantor_cabang->getLatestKantorCabangOnly();
    if($x){
        $last_seq = $x[0]->sequence;
        $new_seq = $last_seq+1;

        if(strlen($new_seq)==1)
            $kode_prov = '0'.$new_seq;
        else
            $kode_prov = $new_seq;

        $data['sequence'] = $new_seq;
        $data['kode_afiliasi'] = $kode_prov;
    } else {
        $data['sequence'] = 1;
        $data['kode_afiliasi'] = '01';
    }
    $data['parent'] = null;
    return $data;
}

function getLatestSequence($jenis){
    $CI = get_instance();
    if($jenis == 'Kwitansi'){
        $x = $CI->md_pembayaran->getLatestPembayaran();
    } 
    else if($jenis == 'Registrasi Jemaah'){
        $x = $CI->md_pendaftaran_detail->getLatestPendaftaranDetail();
    }
    else {
        $x = $CI->md_transaksi->getLatestFakturByJenisFaktur($jenis);
    }

    $used_seq = $x ? $x[0]->seq : 0;

    if($used_seq > 0){
        $old_seq = $used_seq;
        $data['unformat_seq'] = $old_seq + 1;

        if(strlen($data['unformat_seq'])==1)
            $data['new_seq'] = '000'.$data['unformat_seq'];
        else if(strlen($data['unformat_seq'])==2)
            $data['new_seq'] = '00'.$data['unformat_seq'];
        else if(strlen($data['unformat_seq'])==3)
            $data['new_seq'] = '0'.$data['unformat_seq'];
        else if(strlen($data['unformat_seq'])==4)
            $data['new_seq'] = $data['unformat_seq'];
    } else {
        $data['unformat_seq'] = 1;
        $data['new_seq'] ='0001';
    }

    return $data;
}

function getListKantorCabang($login_type){
    $CI = get_instance();
    if($login_type == 'Administrator')
        return $CI->md_kantor_cabang->getAllKantorCabang();
    else 
        return $CI->md_kantor_cabang->getKantorCabangByKantorPengguna($CI->session->userdata('id_kantor'));
}

?>