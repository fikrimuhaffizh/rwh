<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

	function userLog($aksi='',$user='',$ket=''){

		//Get date and time
		$date=date("Y-m-d");
        $jam= date("H:i:s");
        $ip=$_SERVER['REMOTE_ADDR'];

		//put all into $log
		$log=array(
					'pengguna_id'=>$user,
					'tanggal'=>date('Y-m-d'),
				//	'jenis_aksi'=>NULL,
					'keterangan'=>$aksi,
                    'jam'=>$ket,
					'status'=>$jam,
					'ip'=>$ip
					
				  );
		return $log;
	}
	function adminLog($aksi='',$ket=''){
		//put all into $log
		$CI = get_instance();
		$log=array(
				'jenis_aksi'=>$aksi,
				'keterangan'=>$ket,
				'tgl'=>date('Y-m-d H:i:s'),
				'pengguna_id'=>$CI->session->userdata('pengguna_id'),
				'status'=>1,
				'ip_addr'=>$_SERVER['REMOTE_ADDR']
			  );
		return $log;
	}

	function getStatistik($ip='',$link='',$browser=''){
		$stat=array(
					'ip'=>$ip,
					'tgl'=>date("Y-m-d H:i:s"),
					'link_halaman'=>$link,
					'browser'=>$browser,
					'status' =>1
				  );
		return $stat;
	}




?>