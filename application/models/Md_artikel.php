<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Md_artikel extends CI_Model{

    function getArtikelById($id){
        $query="SELECT  med_dt.media_link,art.* FROM `artikel` AS art 
                  INNER JOIN `media_detail` AS med_dt ON med_dt.media_id = art.media_id
                WHERE med_dt.jenis_ukuran='medium' AND med_dt.status=1 AND art.status=1 AND art.artikel_id=?";

        $hasil=$this->db->query($query,array($id))->result();  
        return $hasil;
     
    }
    function getArtikelByIdV2($id){
        $this->db->select("*,CONVERT(VARCHAR(24),tgl_post,120) AS 'tgl_post2'");
        $this->db->from('artikel');
        $this->db->where('artikel_id',$id);
        $this->db->where('status',1);
        $hasil=$this->db->get();
        return $hasil->result();
    }
    function get3topArtikel(){
        $this->db->limit(3);
        $this->db->order_by("tgl_post ", "desc");
        $hasil=$this->db->get_where('artikel',array('post_status'=>'Publish','status'=>1))->result();
        return $hasil;  
    }
    function get4topArtikel(){
        $this->db->limit(4);
        $this->db->order_by('tgl_post','desc');
        $hasil=$this->db->get_where('artikel',array('status'=>1,'post_status'=>'Publish'))->result();
        return $hasil;
    }


    function getPublishedArtikel(){
        $this->db->order_by('tgl_post','desc');
        $hasil=$this->db->get_where('artikel',array('status'=>1,'post_status'=>'Publish'))->result();
        return $hasil;
    }   

    function getOtherArtikel($tgl_post){
        $this->db->limit(3);
        $this->db->order_by('tgl_post','DESC');
        $hasil=$this->db->get_Where('artikel',array('tgl_post <'=>$tgl_post,'status'=>1))->result();
        return $hasil;
    }

    function getNextArtikel($tgl_post){
        $this->db->limit(1);
        $this->db->order_by('tgl_post','asc');
        $hasil=$this->db->get_where('artikel',array('tgl_post >'=>$tgl_post,'status'=>1))->result();
        return $hasil;
    }

    function getPreviousArtikel($tgl_post){
        $this->db->limit(1);
        $this->db->order_by('tgl_post','desc');
        $hasil=$this->db->get_where('artikel',array('tgl_post <'=>$tgl_post,'status'=>1))->result();
        return $hasil;
  
    }    
// =============================================== //
    
    function getAllArtikel() {
        $this->db->order_by('artikel_id','desc');
        $hasil=$this->db->get_where('artikel',array('status'=>1))->result();
        return $hasil;
        // return $hasil;
        // $hasil = $this->db->query("SELECT * FROM artikel where status=1 order by tgl_post desc");
        // if ($hasil->num_rows() > 0) {
        //     foreach ($hasil->result() as $row) {
        //         $data[] = $row;
        //     }
        //     return $data;
        // }
    }
    
    function getBeritaAll() {
        $this->db->order_by('tgl_post','desc');
        $hasil=$this->db->get_where('artikel',array('jenisartikel_id'=>1,'status'=>1))->result();
        return $hasil;
        // $hasil = $this->db->query("SELECT * FROM artikel where status=1 and jenisartikel_id = 1 order by artikel_id asc");
        // if ($hasil->num_rows() > 0) {
        //     foreach ($hasil->result() as $row) {
        //         $data[] = $row;
        //     }
        //     return $data;
        // }
    }    
    function addArtikel($data){
         $this->db->insert('artikel', $data);
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    function updateArtikel($param, $data) {
        $this->db->where('artikel_id', $param);
        $this->db->update('artikel', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    function pagination($limit,$page)
     {
        $query="SELECT * FROM artikel WHERE post_Status = 'Publish' AND status =1 ORDER BY tgl_post desc LIMIT ?,? ";
        $hasil=$this->db->query($query,array((int)$page,(int)$limit))->result();  
        return $hasil;
    }    

}

