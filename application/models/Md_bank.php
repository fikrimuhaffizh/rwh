<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_bank extends CI_Model {

    function getBankById($id){
        $hasil = $this->db->get_where('bank',array('bank_id'=>$id,'status'=>1))->result();
        return $hasil;
    }

    function getBankByJenis($jenis){
        $hasil = $this->db->get_where('bank',array('jenis'=>$jenis,'status'=>1))->result();
        return $hasil;
    }

    function updateBank($param, $data) {
        $this->db->where('bank_id', $param);
        $this->db->update('bank', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function addBank($data){
        $this->db->insert('bank', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    function getAllBank(){
        /*
            Ordering pada table mengikuti urutan column yang diselect di query
        */
        return $this->datatables
        ->select('
                b.bank_id,
                b.nama_bank,
                b.nomor_rekening,
                b.jenis,
                (select COUNT(*) from pembayaran where bank_id = b.bank_id and status = 1) as dependency
            ')
        ->from('bank b')
        ->where('b.status = 1')
        ->generate();        
    }
}