<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_barang extends CI_Model {

	function getBarangByKantorCabangId($id){
        $this->db->join('barang_kantor bk','bk.barang_id = b.barang_id');
        $this->db->order_by('b.nama_barang','asc');
        return $this->db->get_where('barang b',array('bk.kantorcabang_id'=>$id,'b.status'=>1,'bk.status'=>1))->result();
	}

    function getBarangById($id){
        $hasil = $this->db->get_where('barang',array('barang_id'=>$id,'status'=>1))->result();
        return $hasil;
    }
    function getBarangByNamaBarang($nama){
        $hasil = $this->db->get_where('barang',array('nama_barang'=>$nama,'status'=>1))->result();
        return $hasil;
    }
    function getAllBarangV2(){
        $hasil = $this->db->get_where('barang',array('status'=>1))->result();
        return $hasil;
    }

    function getAllNamaBarangDistinct(){
        $login_type = $this->session->userdata('login_type');
        $x='';
        if($login_type!='Administrator' && $login_type!='Keuangan'){
            $x = 'and bk.kantorcabang_id ='.$this->session->userdata('id_kantor');
        }

        $query="SELECT DISTINCT b.nama_barang,b.barang_id 
                FROM barang b
                JOIN barang_kantor bk on bk.barang_id = b.barang_id
                WHERE b.status=1 and bk.status=1 ".$x." order by b.nama_barang asc;";
        return $this->db->query($query)->result();    
    }

    function updatebarang($param, $data) {
        $this->db->where('barang_id', $param);
        $this->db->update('barang', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function addBarang($data){
        $this->db->insert('barang', $data); 
         if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    function addBarang_transact($data){
        $this->db->insert('barang',$data);
    }

    function getAllBarangOnly(){
        /*
            Ordering pada table mengikuti urutan column yang diselect di query
        */
        return $this->datatables
        ->select('
                b.barang_id,
                b.nama_barang,
                b.satuan,
                b.harga_terakhir,
                b.kode_barang,
                (select count(*) from barang_kantor bk where bk.barang_id = b.barang_id and status=1) as total_kantor
            ')
        ->from('barang b')
        ->where('b.status = 1')
        ->generate();        
    }

    function getAllBarang(){
        /*
            Ordering pada table mengikuti urutan column yang diselect di query
        */
        return $this->datatables
        ->select('
                b.barang_id,
                b.nama_barang,
                kc.nama_cabang,
                bk.stok_akhir,
                b.satuan,
                b.harga_terakhir,
                (b.harga_terakhir*bk.stok_akhir) as total,
                bk.kantorcabang_id,
                bk.bkantor_id,
                b.kode_barang,
                (
                    (select count(*) from barang_transaksi bt where bk.bkantor_id = bk.bkantor_id and status = 1)
                ) as dependency,
            ')
        ->from('barang b')
        ->join('barang_kantor bk','bk.barang_id = b.barang_id')
        ->join('kantor_cabang kc','kc.kantorcabang_id = bk.kantorcabang_id')
        ->where('b.status = 1 and bk.status = 1 and kc.status = 1')
        ->generate();        
    }
}