<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_barang_kantor extends CI_Model {

    function getBarangKantorByBarangIdByKantorcabangId($id,$kantorcabang_id){
        $this->db->join('barang b','b.barang_id = bk.barang_id');
        $this->db->join('kantor_cabang kc','kc.kantorcabang_id = bk.kantorcabang_id');
        return $this->db->get_where('barang_kantor bk',array('bk.barang_id'=>$id,'bk.kantorcabang_id'=>$kantorcabang_id,'bk.status'=>1))->result();
    }

    function getBarangKantorById($id){
        $this->db->join('barang b','b.barang_id = bk.barang_id');
        $this->db->join('kantor_cabang kc','kc.kantorcabang_id = bk.kantorcabang_id');
        return $this->db->get_where('barang_kantor bk',array('bk.bkantor_id'=>$id,'bk.status'=>1))->result();
    }
    function getBarangKantorByBarangId($id){
        $this->db->select('b.*,kc.nama_cabang,(select count(*) from barang_transaksi where bkantor_id = bk.bkantor_id and status = 1) as dependency,bk.*');
        $this->db->join('barang b','b.barang_id = bk.barang_id');
        $this->db->join('kantor_cabang kc','kc.kantorcabang_id = bk.kantorcabang_id');
        return $this->db->get_where('barang_kantor bk',array('bk.barang_id'=>$id,'bk.status'=>1))->result();
    }

    function getBarangKantorByKantorcabangId($id){
        if($id != 'Semua Kantor')
            $this->db->where('bk.kantorcabang_id',$id);

        $this->db->join('barang b','b.barang_id = bk.barang_id');
        $this->db->join('kantor_cabang kc','kc.kantorcabang_id = bk.kantorcabang_id');
        $this->db->order_by('kc.nama_cabang','asc');
        $this->db->order_by('b.nama_barang','asc');
        return $this->db->get_where('barang_kantor bk',array('bk.status'=>1))->result();
    }

    function updateBarangKantorbyBarangIdByKantorcabangId($barang_id,$kantorcabang_id, $data) {
        $this->db->where('barang_id', $barang_id);
        $this->db->where('kantorcabang_id', $kantorcabang_id);
        $this->db->update('barang_kantor', $data);

    }

    function updateBarangKantorId($bkantor_id, $data) {
        $this->db->where('bkantor_id', $bkantor_id);
        $this->db->update('barang_kantor', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function addBarangKantor($data){
        $this->db->insert('barang_kantor', $data); 
         if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    function deleteBarangKantor($id){
        $this->db->where('bkantor_id',$id);
        $this->db->delete('barang_kantor');
    }

    function deleteBarangKantorByBarangId($id){
        $this->db->where('barang_id',$id);
        $this->db->delete('barang_kantor');
    }

}