<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_company extends CI_Model {

    function getCompany(){
        $query = 'SELECT 
                        cmp.*,
                        (SELECT media_link FROM media_detail WHERE media_id = cmp.media_id and status = 1 and jenis_ukuran="large") as img_large,
                        (SELECT media_link FROM media_detail WHERE media_id = cmp.media_id and status = 1 and jenis_ukuran="medium") as img_medium,
                        (SELECT media_link FROM media_detail WHERE media_id = cmp.media_id and status = 1 and jenis_ukuran="small") as img_small
                    FROM company cmp';
        return $this->db->query($query)->result();
    }

    function getCompanyById($id){

        return $this->db->get_where('company',array('company_id'=>$id,'status'=>1))->result();
    }

    function updatecompany($param, $data) {
        $this->db->where('company_id', $param);
        $this->db->update('company', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }



}