<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_galeri_album extends CI_Model {

    function getAllGaleriAlbum(){
        $hasil = $this->db->get_where('galeri_album',array('status'=>1))->result();
        return $hasil;
    }
    function getGaleriAlbumById($id){
        $hasil = $this->db->get_where('galeri_album',array('galerialbum_id'=>$id,'status'=>1))->result();
        return $hasil;
    }

    function getAllGaleriAlbumLink(){
        $query="SELECT mDetail.media_link,mDetail.jenis_ukuran,gAlbum.* FROM `galeri_album` AS gAlbum 
                    INNER JOIN `media_detail` AS mDetail ON mDetail.media_id = gAlbum.media_id
                    WHERE mDetail.status = 1 AND gAlbum.status = 1 ";

        $hasil=$this->db->query($query)->result();  
        return $hasil;
    }

    function updateGaleriAlbum($param, $data) {
        $this->db->where('galerialbum_id', $param);
        $this->db->update('galeri_album', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function addGaleriAlbum($data){
        $this->db->insert('galeri_album', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function pagination($limit,$page)
     {
        $query="SELECT * FROM galeri_album WHERE status =1 ORDER BY tgl_post desc LIMIT ?,? ";
        $hasil=$this->db->query($query,array((int)$page,(int)$limit))->result();  
        return $hasil;
    }  
}