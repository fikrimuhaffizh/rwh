<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_galeri_item extends CI_Model {

    function getAllGaleriItem(){
        $hasil = $this->db->get_where('galeri_item',array('status'=>1))->result();
        return $hasil;
    }

    function getGaleriItemById($id){
        $hasil = $this->db->get_where('galeri_item',array('galeriitem_id'=>$id,'status'=>1))->result();
        return $hasil;
    }

    function getGaleriItemByGaleriAlbumId($id){
        $hasil = $this->db->get_where('galeri_item',array('galerialbum_id'=>$id,'status'=>1))->result();
        return $hasil;
    }

    function getGaleriItemByGaleriAlbumIdWithLink($id){
        $query="SELECT gItem.*, mDetail.media_link,mDetail.jenis_ukuran FROM `galeri_item` AS gItem
                    INNER JOIN `media_detail` AS mDetail ON mDetail.media_id = gItem.media_id
                    WHERE gItem.status=1 AND mDetail.status=1 AND gItem.galerialbum_id=? AND mDetail.jenis_ukuran IN ('large','medium')";

        $hasil=$this->db->query($query,array($id))->result();  
        return $hasil;
    }

    function updateGaleriItem($param, $data) {
        $this->db->where('galeriitem_id', $param);
        $this->db->update('galeri_item', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function updateGaleriItemByGaleriAlbumId($galerialbum_id, $data) {
        $this->db->where('galerialbum_id', $galerialbum_id);
        $this->db->update('galeri_item', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function addGaleriItem($data){
        $this->db->insert('galeri_item', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function pagination($id,$limit,$page)
     {
        $query="SELECT * FROM galeri_item WHERE status =1 AND galerialbum_id = ? ORDER BY tgl_post desc LIMIT ?,? ";
        $hasil=$this->db->query($query,array($id,(int)$page,(int)$limit))->result();  
        return $hasil;
    }  

}