<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Md_halaman extends CI_Model{

    function getHalamanHeader(){

        $query=$this->db->order_by('tgl_post','desc')->get_where('halaman',array('status'=>1))->result();
        return $query;        
    }
// =========================== /
    function getAllHalaman() {
        $hasil=$this->db->get_where('halaman',array('status'=>1))->result();
        return $hasil;
    }

    function getHalamanReguler() {
        $hasil=$this->db->get_where('halaman',array('tipe'=>'reguler','status'=>1))->result();
        return $hasil;
    }
    function getHalamanTambahan() {
        $hasil=$this->db->get_where('halaman',array('tipe'=>'tambahan','status'=>1))->result();
        return $hasil;
    }
    
    function getHalamanById($id) {
        $hasil = $this->db->get_where('halaman', array('halaman_id' => $id,'status' => 1))->result();
        $data = $hasil;
        return $data;
    }
    
    function getHalamanByJudul($judul) {
        $data=$this->db->get_where('halaman',array('judul'=>$judul, 'status'=>1))->result();
        return $data;
    }

    function getHalamanByLink($link) {
        $data=$this->db->get_where('halaman',array('link_halaman'=>$link, 'status'=>1))->result();
        return $data;
    }

    function addHalaman($data){
         $this->db->insert('halaman', $data);
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    

    
    function updateHalaman($param, $data) {
        $this->db->where('halaman_id', $param);
        $this->db->update('halaman', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
 }         

