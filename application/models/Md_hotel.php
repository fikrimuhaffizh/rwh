<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_hotel extends CI_Model {

    function getAllHotel(){
        $this->db->select('ht.*,(select count(*) from paket_akomodasi where hotel = ht.hotel_id) as dependency');
        $this->db->order_by('ht.hotel_id','desc');
        $hasil = $this->db->get_where('hotel ht',array('ht.status'=>1))->result();
        return $hasil;
    }

    function getHotelById($id){
        $hasil = $this->db->get_where('hotel',array('hotel_id'=>$id,'status'=>1))->result();
        return $hasil;
    }

    function updateHotel($param, $data) {
        $this->db->where('hotel_id', $param);
        $this->db->update('hotel', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function addHotel($data){
        $this->db->insert('hotel', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}