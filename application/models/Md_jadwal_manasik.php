<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_jadwal_manasik extends CI_Model {

    function getAllJadwalManasik(){
        $this->db->order_by('jm.jadwalmanasik_id','desc');
        return $this->db->get_where('jadwal_manasik jm',array('jm.status'=>1))->result();
    }
    function getAllJadwalManasikAfterToday(){
        $this->db->order_by('tgl_manasik','asc');
        $hasil = $this->db->get_where('jadwal_manasik',array('tgl_manasik >='=>date('Y-m-d H:i:s'),'status'=>1))->result();
        return $hasil;
    }    
    function getJadwalManasikById($id){
        $hasil = $this->db->get_where('jadwal_manasik jm',array('jm.jadwalmanasik_id'=>$id,'jm.status'=>1))->result();
        return $hasil;
    }
    function getJadwalManasikByParent($id){
        $query="SELECT * FROM jadwal_manasik WHERE kantorcabang_id IN 
                    (SELECT kantorcabang_id FROM kantor_cabang WHERE parent = ? OR kantorcabang_id = ? AND status =1) AND status=1 ;";

        $hasil=$this->db->query($query,array($id,$id))->result();  
        return $hasil;
    }    
    function updateJadwalManasik($param, $data) {
        $this->db->where('jadwalmanasik_id', $param);
        $this->db->update('jadwal_manasik', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function addJadwalManasik($data){
        $this->db->insert('jadwal_manasik', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}