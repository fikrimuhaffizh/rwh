<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_jemaah extends CI_Model {

    function getAllJemaah(){
        $this->db->order_by('jemaah_id','desc');
        $hasil = $this->db->get_where('jemaah',array('status'=>1))->result();
        return $hasil;
    }
    function getLatestJemaah(){
        $this->db->limit(1);
        $this->db->order_by('jemaah_id','desc');
        $hasil = $this->db->get_where('jemaah',array('status'=>1))->result();
        return $hasil;
    }
    function getJemaahByJemaahId($id){
        $hasil = $this->db->get_where('jemaah',array('jemaah_id'=>$id,'status'=>1))->result();
        return $hasil;
    }
    function getJemaahByEmail($email){
        $hasil = $this->db->get_where('jemaah',array('email'=>$email,'status'=>1))->result();
        return $hasil;
    }

    function updateJemaah($id, $data) {
        $this->db->where('jemaah_id', $id);
        $this->db->update('jemaah', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function addJemaah($data){
        $this->db->insert('jemaah', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}