<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_surat_status extends CI_Model {

    function getLatestSuratStatusBySuratId($id){
        $this->db->limit(1);
        $this->db->order_by('suratstatus_id','desc');
        $hasil = $this->db->get_where('surat_status',array('surat_id'=>$id,'status'=>1))->result();
        return $hasil;
    }

	function getSuratStatusBySuratId($id){
        $hasil = $this->db->get_where('surat_status',array('surat_id'=>$id,'status'=>1))->result();
        return $hasil;
	}
    function addsuratStatus($data){
        $this->db->insert('surat_status', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}