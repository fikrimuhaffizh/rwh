<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_jenis_paket extends CI_Model {

    function getAllJenisPaket(){
        $hasil = $this->db->get_where('jenis_paket',array('status'=>1))->result();
        return $hasil;
    }

    function getLatestJenisPaketBySuratId($id){
        $this->db->limit(1);
        $this->db->order_by('jenispaket_id','desc');
        $hasil = $this->db->get_where('jenis_paket',array('jenispaket_id'=>$id,'status'=>1))->result();
        return $hasil;
    }

	function getJenisPaketByJenisPaketId($id){
        $hasil = $this->db->get_where('jenis_paket',array('jenispaket_id'=>$id,'status'=>1))->result();
        return $hasil;
	}
    function addsuratStatus($data){
        $this->db->insert('jenis_paket', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}