<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_kantor_cabang extends CI_Model {
    function countKantorCabang(){
        $this->db->select('COUNT(*) as total');
        return $this->db->get_where('kantor_cabang',array('status'=>1))->result();
    }
    function getAllKantorCabang(){
        $this->db->select('kc.*,(
                                (select COUNT(*) from barang_kantor where kantorcabang_id = kc.kantorcabang_id and status = 1) + 
                                (select COUNT(*) from jadwal_manasik where kantorcabang_id = kc.kantorcabang_id and status =1)
                            ) as dependency');
        $this->db->order_by('kc.kantorcabang_id','desc');
        $hasil = $this->db->get_where('kantor_cabang kc',array('kc.status'=>1))->result();
        return $hasil;
    }
    function getKantorCabangByKantorPengguna($id){
        $this->db->select('kc.*,(
                                (select COUNT(*) from barang_kantor where kantorcabang_id = kc.kantorcabang_id and status = 1) + 
                                (select COUNT(*) from jadwal_manasik where kantorcabang_id = kc.kantorcabang_id and status =1)
                            ) as dependency');
        $this->db->order_by('kc.kantorcabang_id','desc');
        $hasil = $this->db->get_where('kantor_cabang kc',array('kc.kantorcabang_id'=>$id,'kc.status'=>1))->result();
        return $hasil;
    }    
    function getKantorCabangByKodeAfiliasi($kode){
        $hasil = $this->db->get_where('kantor_cabang',array('kode_afiliasi'=>$kode,'status'=>1))->result();
        return $hasil;
    }
    function getAllKantorCabangDistinct(){
        $this->db->distinct('nama_daerah');
        $hasil = $this->db->get_where('kantor_cabang',array('status'=>1))->result();
        return $hasil;
    }
    function getAllKantorCabangDistinctByKota(){
        $this->db->select('DISTINCT(kota)');
        $hasil = $this->db->get_where('kantor_cabang',array('status'=>1))->result();
        return $hasil;
    }
    function getAllKantorCabangOnly(){
        $this->db->order_by('kantorcabang_id','desc');
        $hasil = $this->db->get_where('kantor_cabang',array('parent'=>NULL,'status'=>1))->result();
        return $hasil;
    }
    function getAllProvinsiKantorCabang(){
        $this->db->select('DISTINCT(provinsi)');
        $this->db->order_by('provinsi','desc');
        $hasil = $this->db->get_where('kantor_cabang',array('parent'=>NULL,'status'=>1))->result();
        return $hasil;
    }    
    function getAllKantorCabangWithSameKantorcabangParent($id){
        $query ="SELECT * FROM `kantor_cabang` WHERE (parent = ? OR kantorcabang_id = ?) AND status = 1";
        $hasil=$this->db->query($query,array($id,$id))->result();      
        return $hasil;
    } 
    function getLatestPerwakilanDaerahByParent($id){
        $this->db->limit(1);
        $this->db->order_by('sequence','desc');
        $hasil = $this->db->get_where('kantor_cabang',array('parent '=>$id,'status'=>1))->result();
        return $hasil;
    }
    function getPerwakilanDaerahByParent($id){
        $this->db->order_by('sequence','desc');
        $hasil = $this->db->get_where('kantor_cabang',array('parent '=>$id,'status'=>1))->result();
        return $hasil;
    }    
    function getKantorCabangByParent($id){
        $this->db->where('parent',$id);
        $this->db->or_where('kantorcabang_id',$id);
        $hasil = $this->db->get_where('kantor_cabang',array('status'=>1))->result();
        return $hasil;
    }
    function getLatestKantorCabangOnly(){
        $this->db->limit(1);
        $this->db->order_by('sequence','desc');
        $hasil = $this->db->get_where('kantor_cabang',array('parent'=>NULL,'status'=>1))->result();
        return $hasil;
    }
    function getLatestKantorCabang(){
        $this->db->limit(1);
        $this->db->order_by('kantorcabang_id','desc');        
        $hasil = $this->db->get_where('kantor_cabang',array('status'=>1))->result();
        return $hasil;
    }    
    function getAllPerwakilanDaerahOnly(){
        $hasil = $this->db->get_where('kantor_cabang',array('parent <>'=>NULL,'status'=>1))->result();
        return $hasil;
    }
    function getKantorCabangDefault(){
        $hasil = $this->db->get_where('kantor_cabang',array('default'=>'Ya','status'=>1))->result();
        return $hasil;
    }
    function getKantorCabangById($id){
        $hasil = $this->db->get_where('kantor_cabang',array('kantorcabang_id'=>$id,'status'=>1))->result();
        return $hasil;
    }

    function getKantorCabangByNamaDaerah($param){
        $hasil = $this->db->get_where('kantor_cabang',array('nama_daerah'=>$param,'status'=>1))->result();
        return $hasil;
    }
    function getKantorCabangByKota($param){
        $hasil = $this->db->get_where('kantor_cabang',array('kota'=>$param,'status'=>1))->result();
        return $hasil;
    }
    function getPerwakilanDaerahByKantorCabangId($id){
        $hasil = $this->db->get_where('kantor_cabang',array('parent'=>$id,'status'=>1))->result();
        return $hasil;
    }
    function updateKantorCabang($param, $data) {
        $this->db->where('kantorcabang_id', $param);
        $this->db->update('kantor_cabang', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function updateKantorCabangV2($param, $data) {
        $this->db->where('kantorcabang_id <>', $param);
        $this->db->update('kantor_cabang', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function addKantorCabang($data){
        $this->db->insert('kantor_cabang', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function getJmlKantorCabangOnly(){
        $hasil = $this->db->get_where('kantor_cabang',array('parent'=>NULL,'status'=>1));
        return $hasil->num_rows();
    }
    
    function getJmlPerwakilanDaerahOnly(){
        $hasil = $this->db->get_where('kantor_cabang',array('parent <>'=>NULL,'status'=>1));
        return $hasil->num_rows();
    }    

}