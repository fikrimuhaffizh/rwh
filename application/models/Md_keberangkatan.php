<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_keberangkatan extends CI_Model {

    
    function getAllKeberangkatan(){
        $hasil = $this->db->get_where('keberangkatan',array('status'=>1))->result();
        return $hasil;
    }
    function getKeberangkatanByPaketWaktuId($id){
        $this->db->order_by('tgl_keberangkatan','asc');
        $hasil = $this->db->get_where('keberangkatan kbrngktn',array('kbrngktn.paketwaktu_id'=>$id,'kbrngktn.status'=>1))->result();
        return $hasil;
    }
	function getKeberangkatanByKeberangkatanId($id){
        return $this->db->get_where('keberangkatan',array('keberangkatan_id'=>$id,'status'=>1))->result();
	}

    function getAvailableKeberangkatanJemaah(){
        $query="SELECT 
                    distinct
                    kt.nama_kota,
                    kbrgktn.tgl_keberangkatan,
                    pndftr_dt.keberangkatan_id,
                    pndftr_dt.kota_keberangkatan,
                    (select count(*) from pendaftaran_detail where kota_keberangkatan = pndftr_dt.kota_keberangkatan and keberangkatan_id = pndftr_dt.keberangkatan_id and status_verifikasi = 'Valid' and status_keberangkatan in ('Dalam Proses','Sudah Berangkat') and status =1) as total_jemaah,
                    pkt.nama_paket

                from pendaftaran_detail pndftr_dt 
                inner join kota kt on kt.kota_id = pndftr_dt.kota_keberangkatan
                inner join keberangkatan kbrgktn on kbrgktn.keberangkatan_id = pndftr_dt.keberangkatan_id
                inner join paketwaktu_kelas pwk on pwk.pwk_id = pndftr_dt.pwk_id
                inner join paket_waktu pw on pw.paketwaktu_id = pwk.paketwaktu_id
                inner join paket pkt on pkt.paket_id = pw.paket_id
                where 
                  pndftr_dt.status = 1 and
                  status_keberangkatan in ('Dalam Proses','Sudah Berangkat') and 
                  status_verifikasi = 'Valid'and
                  pndftr_dt.keberangkatan_id in (select distinct keberangkatan_id from pendaftaran_detail where status = 1)
                order by kbrgktn.tgl_keberangkatan desc;";
        $hasil=$this->db->query($query)->result();  
        return $hasil;
    } 

    function addKeberangkatan($data){
        $this->db->insert('keberangkatan', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function updateKeberangkatan($id, $data) {
        $this->db->where('keberangkatan_id', $id);
        $this->db->update('keberangkatan', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    function deleteKeberangkatanByPaketWaktuId($id){
        $this->db->where('paketwaktu_id',$id);
        $this->db->delete('keberangkatan');
    }

    function getLatestStatusSeat(){
        $query="SELECT 
                    kb.tgl_keberangkatan,
                    kb.kuota,
                    pw.bulan_berangkat,
                    pw.tahun_berangkat,
                    pk.berangkat_awal,
                    pk.nama_paket, 
                    kt.nama_kota, 
                    (select count(*) from pendaftaran_detail where status=1 and keberangkatan_id=kb.keberangkatan_id and infant<=0 and status_keberangkatan <> 'Cancel') as terisi,
                    kb.keberangkatan_id
                FROM keberangkatan  kb 
                    inner join paket_waktu pw on kb.paketwaktu_id=pw.paketwaktu_id 
                    inner join paket pk on pw.paket_id=pk.paket_id 
                    inner join kota kt on pk.berangkat_awal=kt.kota_id
                WHERE kb.status=1 and pw.status=1 and curdate() < kb.tgl_keberangkatan order by kt.nama_kota asc,kb.tgl_keberangkatan desc ";
        $hasil=$this->db->query($query)->result();  
        return $hasil;
    }    

    function getLatestStatusSeat_pagination(){
        return $this->datatables
        ->select('  
                    kb.keberangkatan_id,
                    pk.nama_paket, 
                    pw.tahun_berangkat,
                    kb.tgl_keberangkatan,
                    kb.kuota,
                    (select count(*) from pendaftaran_detail where status=1 and keberangkatan_id=kb.keberangkatan_id and infant<=0 and status_keberangkatan <> "Cancel") as terisi,
                    pw.bulan_berangkat,
                    pk.berangkat_awal,
                    kt.nama_kota,
                    pk.paket_id
        ')
        ->from('keberangkatan  kb ')
        ->join('paket_waktu pw','kb.paketwaktu_id=pw.paketwaktu_id ')
        ->join('paket pk','pk.paket_id = pw.paket_id ')
        ->join('kota kt','pk.berangkat_awal=kt.kota_id')
        ->where('kb.status=1 and pw.status=1 ')
        ->generate();   
    }

}