<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_keberangkatan_tour extends CI_Model {


    function getKeberangkatanTourByKeberangkatanId($id){
        $this->db->join('tour_leader tl','tl.tourleader_id = kt.tourleader_id','left');
        $hasil = $this->db->get_where('keberangkatan_tour kt',array('kt.keberangkatan_id'=>$id,'kt.status'=>1))->result();
        return $hasil;
    }

    function getKeberangkatanTourByTglKeberangkatan($tgl){
        $this->db->join('keberangkatan k','k.keberangkatan_id = kt.keberangkatan_id');
        $this->db->join('tour_leader tl','tl.tourleader_id = kt.tourleader_id','left');
        $hasil = $this->db->get_where('keberangkatan_tour kt',array('k.tgl_keberangkatan'=>$tgl,'kt.status'=>1))->result();
        return $hasil;
    }

    function updateKeberangkatanTourByKeberangkatanId($param, $data) {
        $this->db->where('keberangkatan_id', $param);
        $this->db->update('keberangkatan_tour', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function deleteKeberangkatanTourByKeberangkatanId($keberangkatan_id){
        $this->db->where_in('keberangkatan_id',$keberangkatan_id);
        $this->db->delete('keberangkatan_tour'); 
    }

    function addKeberangkatanTour($data){
        $this->db->insert('keberangkatan_tour', $data); 
         if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }
}