<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_kelas extends CI_Model {

	function getAllKelas(){
        $hasil = $this->db->get_where('kelas',array('status'=>1))->result();
        return $hasil;
	}

    function getKelasById($id){
        $hasil = $this->db->get_where('kelas',array('kelas_id'=>$id,'status'=>1))->result();
        return $hasil;
    }

    function getAllKelasV2(){
        $this->db->select('kelas');
        $hasil = $this->db->get_where('kelas',array('status'=>1))->result();
        return $hasil;
    }

    function updatekelas($param, $data) {
        $this->db->where('kelas_id', $param);
        $this->db->update('kelas', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function addKelas($data){
        $this->db->insert('kelas', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}