<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_konfirmasi extends CI_Model {

	function getAllKonfirmasi(){
        $this->db->order_by('konfirmasi_id','desc');
        $hasil = $this->db->get_where('konfirmasi_bayar',array('status'=>1))->result();
        return $hasil;
	}

    function getLatestkonfirmasi(){
        $this->db->limit(1);
        $this->db->order_by('konfirmasi_id','desc');
        $hasil = $this->db->get_where('konfirmasi_bayar',array('status'=>1))->result();
        return $hasil;
    }
    
    function getKonfirmasiById($id){
        $hasil = $this->db->get_where('konfirmasi_bayar',array('konfirmasi_id'=>$id,'status'=>1))->result();
        return $hasil;
    }

    function updateKonfirmasi($param, $data) {
        $this->db->where('konfirmasi_id', $param);
        $this->db->update('konfirmasi_bayar', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function addKonfirmasi($data){
        $this->db->insert('konfirmasi_bayar', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}