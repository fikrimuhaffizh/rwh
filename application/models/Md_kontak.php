<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_kontak extends CI_Model {

    function getAllKontak(){
        $hasil = $this->db->get_where('kontak',array('status'=>1))->result();
        return $hasil;
    }
    function getKontakById($id){
        $hasil = $this->db->get_where('kontak',array('kontak_id'=>$id,'status'=>1))->result();
        return $hasil;
    }
    function getKontakUtama(){
        $hasil = $this->db->get_where('kontak',array('kontak_id'=>1,'status'=>1))->result();
        return $hasil;
    }

    function getKontakForWhatsapp(){
        $hasil = $this->db->get_where('kontak',array('kontak_id<>'=>1,'status'=>1))->result();
        return $hasil;
    }
    function getWhatsappOnly(){
        $hasil = $this->db->get_where('kontak',array('whatsapp_chat<>'=>NULL,'status'=>1))->result();
        return $hasil;
    }    
    function updatekontak($param, $data) {
        $this->db->where('kontak_id', $param);
        $this->db->update('kontak', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function addKontak($data){
        $this->db->insert('kontak', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}