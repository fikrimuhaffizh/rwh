<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_kota extends CI_Model {

    function getAllKota(){
        /*
            Ordering pada table mengikuti urutan column yang diselect di query
        */
        return $this->datatables
        ->select('kt.kota_id,kt.nama_kota,kt.kode_kota,kt.status,( select COUNT(*) from paket where berangkat_awal = kt.kota_id and status = 1) as dependency')
        ->from('kota kt')
        ->where('kt.status = 1')
        ->generate();        
    }

    function getKotaById($id){
        $hasil = $this->db->get_where('kota',array('kota_id'=>$id,'status'=>1))->result();
        return $hasil;
    }

    function updatekota($param, $data) {
        $this->db->where('kota_id', $param);
        $this->db->update('kota', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function addKota($data){
        $this->db->insert('kota', $data); 
         if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }
}