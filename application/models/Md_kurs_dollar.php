<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_kurs_dollar extends CI_Model {

    function getLatestKursDollar(){
        $this->db->limit(1);
        $this->db->order_by('tgl_perubahan','desc');
        return $this->db->get_where('kurs_dollar',array('tgl_perubahan >= '=>date('Y-m-d'),'status'=>1))->result();
    }
    function addKursDollar($data){
        $this->db->insert('kurs_dollar', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}