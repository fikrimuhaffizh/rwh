<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Md_log extends CI_Model {

    function getLogAll() {

        $this->db->order_by('log_id','desc');
        $hasil=$this->db->get_where('log',array('status'=>1))->result();
        return $hasil;
    }
    function getLogByJenis($jenis){

        $this->db->where('keterangan',$jenis);
        $this->db->where('ip',1);
        $this->db->from('log');
        $hasil = $this->db->get()->result();
        return $hasil;
    }
    function getTop30Activity($login_type){
        if($login_type!='Administrator' and $login_type!='Keuangan' and $login_type!='Eksekutif'){
            $pengguna_id = $this->session->userdata('pengguna_id');
            $this->db->where('lg.pengguna_id',$pengguna_id);
        }
        $this->db->limit(30);
        $this->db->join('pengguna pg','pg.pengguna_id = lg.pengguna_id');
        $this->db->order_by('lg.log_id','desc');
        $this->db->where('lg.status',1);
        $this->db->from('log lg');
        return $this->db->get()->result();
    }
    function getLogLoginByMonthYear($month,$year){
        $query="  SELECT * FROM log WHERE jenis_aksi='Login' AND DATEPART(month, tgl) = ? AND DATEPART(year, tgl) = ?";
        $hasil=$this->db->query($query,array($month,$year))->result();  
        return $hasil;
    }

    function getLoginYearOnly(){
        $query="  SELECT DISTINCT(DATEPART(yyyy,tgl)) as tgl FROM log WHERE jenis_aksi='Login' AND status=?";
        $hasil=$this->db->query($query,array(1))->result();  
        return $hasil;
    }

    function getLogLoginBytgl($tgl){
        $this->db->order_by('tgl','desc');
        $this->db->where('tgl',$tgl);
        $this->db->where('jenis_aksi','Login');
        $this->db->where('status',1);
        $this->db->from('log');
        $hasil = $this->db->get()->result();
        return $hasil; 
    }

    function getLogJenis() {
        $hasil = $this->db->query("SELECT distinct jenis_aksi FROM log");
        if ($hasil->num_rows() > 0) {
            foreach ($hasil->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }


    
    function addLog($data){
         $this->db->insert('log', $data);
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    function addLog_transact($data){
        $this->db->insert('log', $data);
    }

    function getAllLogByLoginType($login_type){
        $query = '';
        if($login_type!='Administrator' and $login_type!='Keuangan' and $login_type!='Eksekutif'){
            $pengguna_id = $this->session->userdata('pengguna_id');
            $query = 'and lg.pengguna_id = '.$pengguna_id;
        }
        /*
            Ordering pada table mengikuti urutan column yang diselect di query
        */
        return $this->datatables
        ->select('  
            lg.log_id,
            pg.nama_lengkap,
            lg.jenis_aksi,
            lg.keterangan,
            lg.tgl
        ')
        ->from('log lg')
        ->join('pengguna pg','pg.pengguna_id = lg.pengguna_id','left')
        ->where('pg.status = 1 and lg.status=1 '.$query)
        ->generate();        
    }
}