<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_maskapai extends CI_Model {

    function getAllMaskapai(){
        $this->db->select('m.*,md.*,(select COUNT(*) from paket_maskapai where maskapai_id = m.maskapai_id and status = 1) as dependency');
        $this->db->join('media md','md.media_id = m.media_id');
        $this->db->order_by('m.maskapai_id','desc');
        $hasil = $this->db->get_where('maskapai m',array('m.status'=>1))->result();
        return $hasil;
    }

    function getMaskapaiById($id){
        $hasil = $this->db->get_where('maskapai',array('maskapai_id'=>$id,'status'=>1))->result();
        return $hasil;
    }

    function updateMaskapai($param, $data) {
        $this->db->where('maskapai_id', $param);
        $this->db->update('maskapai', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function addMaskapai($data){
        $this->db->insert('maskapai', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}