<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_media extends CI_Model {

    function getLatestMedia(){
        $this->db->limit(1);
        $this->db->order_by('media_id','desc');
        $hasil = $this->db->get_where('media',array('status'=>1))->result();
        return $hasil;
    }

    function getAllMediaByMedia(){
        $query ="SELECT * FROM media WHERE status=1 AND jenismedia_id IN (SELECT jenismedia_id from jenis_media WHERE jenis_media = 'Media') ORDER BY tgl_perubahan desc";
        $hasil=$this->db->query($query)->result();      
        return $hasil;
    }

	function getMediaByMediaId($id){
        $hasil = $this->db->get_where('media',array('media_id'=>$id,'status'=>1))->result();
        return $hasil;
	}
    function getMediaByJenisMedia($id){
        $this->db->order_by('media_id','desc');
        $hasil = $this->db->get_where('media',array('jenismedia_id'=>$id,'status'=>1))->result();
        return $hasil;
    }

    function updateMedia($param, $data) {
        $this->db->where('media_id', $param);
        $this->db->update('media', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function addMedia($data){
        $this->db->insert('media', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}