<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_media_detail extends CI_Model {

    function getMediaDetailByMediaId($id){
        $hasil = $this->db->get_where('media_detail',array('media_id'=>$id,'status'=>1))->result();
        return $hasil;
    }

    function getMediaDetailByMediaIdByUkuran($id,$ukuran){
        $hasil = $this->db->get_where('media_detail',array('media_id'=>$id,'jenis_ukuran'=>$ukuran,'status'=>1))->result();
        return $hasil;
    }

    function updateMediaDetail($param, $data) {
        $this->db->where('mediadetail_id', $param);
        $this->db->update('media_detail', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function updateMediaDetailByMediaId($id, $data) {
        $this->db->where('media_id', $id);
        $this->db->update('media_detail', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function addMediaDetail($data){
        $this->db->insert('media_detail', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}