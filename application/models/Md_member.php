<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_member extends CI_Model {

    function getAllMember(){
        $this->db->order_by('member_id','desc');
        $hasil = $this->db->get_where('member',array('status'=>1))->result();
        return $hasil;
    }
    function getLatestMember(){
        $this->db->limit(1);
        $this->db->order_by('member_id','desc');
        $hasil = $this->db->get_where('member',array('status'=>1))->result();
        return $hasil;
    }
    function getMemberByMemberId($id){
        $hasil = $this->db->get_where('member',array('member_id'=>$id,'status'=>1))->result();
        return $hasil;
    }
    function getMemberByEmail($email){
        $hasil = $this->db->get_where('member',array('email'=>$email,'status'=>1))->result();
        return $hasil;
    }
    function getMemberByEmailByPass($email,$pass){
        $hasil = $this->db->get_where('member',array('email'=>$email,'password'=>$pass,'status'=>1))->result();
        return $hasil;
    }    

    function updateMember($id, $data) {
        $this->db->where('member_id', $id);
        $this->db->update('member', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function addMember($data){
        $this->db->insert('member', $data); 
         if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    function addMember_transact($data){
        $this->db->insert('member', $data); 
    }

    function getJmlMember(){
        $hasil = $this->db->get_where('member',array('status'=>1));
         return $hasil->num_rows();
    }    

}