<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_merchant extends CI_Model {

    function getAllMerchant(){
        $hasil = $this->db->get_where('merchant',array('status'=>1))->result();
        return $hasil;
    }
    function get5TtopMerchant(){
        $this->db->limit(5);
        $hasil = $this->db->get_where('merchant',array('status'=>1))->result();
        return $hasil;
    }
    function getMerchantById($id){
        $hasil = $this->db->get_where('merchant',array('merchant_id'=>$id,'status'=>1))->result();
        return $hasil;
    }

    function updateMerchant($param, $data) {
        $this->db->where('merchant_id', $param);
        $this->db->update('merchant', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function addMerchant($data){
        $this->db->insert('merchant', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}