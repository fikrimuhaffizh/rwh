<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_notifikasi extends CI_Model {

    function getAllPushNotification(){
        $this->db->order_by('tgl','desc');
        $hasil = $this->db->get_where('notifikasi',array('tipe'=>'Push Notification','status'=>1))->result();
        return $hasil;
    }
    function getNotificationByPenggunaByBelumBaca($id){
        $hasil = $this->db->get_where('notifikasi',array('pengguna_id'=>$id,'baca'=>0,'status'=>1))->result();
        return $hasil;
    }
    
    function getNotifbyPengguna($id){

        return $this->datatables
        ->select('  
            ntf.notifikasi_id,
            ntf.judul,
            ntf.keterangan,
            ntf.tgl,
            ntf.baca,
            ntf.link
        ')
        ->from('notifikasi ntf')
        ->join('pengguna pg','pg.pengguna_id = ntf.pengguna_id','left')
        ->where('pg.status = 1 and ntf.status=1 and pg.pengguna_id = '.$id)
        ->generate();  

    }

    function getTop20NotifbyPengguna($id){
        $this->db->limit(20);
        $this->db->order_by('notifikasi_id','DESC');
        $hasil=$this->db->get_where('notifikasi',array('pengguna_id'=>$id,'status'=>1))->result();
        return $hasil;
    }    

    function addNotifikasi($data){
        $this->db->insert('notifikasi', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    function updateNotif($id,$data) {
        $this->db->where('notifikasi_id', $id);
        $this->db->update('notifikasi', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    function updateNotifReadAllByPengguna($id, $data) {
        $this->db->where('pengguna_id', $id);
        $this->db->update('notifikasi', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }   

    function updateNotifikasiByPendaftaranDetailId($id, $data) {
        $this->db->like('link', '/'.$id);
        $this->db->update('notifikasi', $data);
    }      

    function updateNotifikasiByPenggunaId($id, $data) {
        $this->db->like('pengguna_id', $id);
        $this->db->update('notifikasi', $data);
    }

}