<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_paket extends CI_Model {

    function getAllPaket(){
        $this->db->order_by('paket_id','desc');
        $hasil = $this->db->get_where('paket',array('status'=>1))->result();
        return $hasil;
    }
    function getAllPaketReguler(){
        $this->db->join('kota kt','kt.kota_id = p.berangkat_awal');
        $this->db->join('jenis_paket jp','jp.jenispaket_id = p.jenispaket_id');
        $this->db->order_by('p.paket_id','desc');
        $hasil = $this->db->get_where('paket p',array('p.jenispaket_id <>'=>2,'p.status'=>1))->result();
        return $hasil;
    }

    function getPaketWithPromo($paket_id){
        $query="SELECT * from paket pkt 
                    inner join paket_waktu pw on pw.paket_id = pkt.paket_id
                    inner join paketwaktu_kelas pwk on pwk.paketwaktu_id = pw.paketwaktu_id
                    where pwk.promo = 'Ya' and pkt.paket_id = ?;";
        return $this->db->query($query,array($paket_id))->result(); 
    }

    function getBerangakatawalDistinctByJenisPaketId($id){
        $query="SELECT DISTINCT berangkat_awal FROM paket WHERE status=1 AND jenispaket_id = ?";
        return $this->db->query($query,array($id))->result();  
    }   
    function getTransitDistinctByJenisPaketId($id){
        $query="SELECT DISTINCT berangkat_transit FROM `paket` where status = 1 AND jenispaket_id = ?";
        return $this->db->query($query,array($id))->result();  
    }
    function getBulanBerangkatDistinctByJenisPaketId($id){
        $query="SELECT DISTINCT pkt_wkt.bulan_berangkat FROM paket_waktu pkt_wkt 
                    INNER JOIN paket pkt ON pkt.paket_id = pkt_wkt.paket_id
                    WHERE pkt.jenispaket_id = ? AND pkt.status=1 AND pkt_wkt.status = 1 ORDER BY pkt_wkt.bulan_berangkat ASC";
        return $this->db->query($query,array($id))->result();  
    }  
    function getTahunBerangkatDistinctByJenisPaketId($id){
        $query="SELECT DISTINCT pkt_wkt.tahun_berangkat FROM paket_waktu pkt_wkt 
                    INNER JOIN paket pkt ON pkt.paket_id = pkt_wkt.paket_id
                    WHERE pkt.jenispaket_id = ? AND pkt.status=1 AND pkt_wkt.status = 1 ORDER BY pkt_wkt.tahun_berangkat ASC";
        return $this->db->query($query,array($id))->result();  
    } 

    function getTahunBerangkatDistinctByJenisPaketIdByBerangkatAwal($jenispaket_id,$berangkat_awal){
        $data = array();
        array_push($data,$jenispaket_id);
        $query="SELECT DISTINCT pkt_wkt.tahun_berangkat FROM paket_waktu pkt_wkt 
                    INNER JOIN paket pkt ON pkt.paket_id = pkt_wkt.paket_id
                    WHERE pkt.jenispaket_id = ? AND pkt.status=1 AND pkt_wkt.status = 1";

        if($berangkat_awal!="Semua Keberangkatan"){
            $query = $query." AND pkt.berangkat_awal=?";
            array_push($data,$berangkat_awal);
        }

        $query = $query." ORDER BY pkt_wkt.tahun_berangkat ASC";
        return $this->db->query($query,$data)->result();  
    }    
    function getBulanBerangkatDistinctByJenisPaketIdByBerangkatAwalByTahun($jenispaket_id,$berangkat_awal,$tahun){
        $data = array();
        array_push($data,$jenispaket_id);
        $query="SELECT DISTINCT pkt_wkt.bulan_berangkat FROM paket_waktu pkt_wkt 
                    INNER JOIN paket pkt ON pkt.paket_id = pkt_wkt.paket_id
                    WHERE pkt.jenispaket_id = ? AND pkt.status=1 AND pkt_wkt.status = 1";

        if($berangkat_awal!="Semua Keberangkatan"){
            $query = $query." AND pkt.berangkat_awal = ?";
            array_push($data,$berangkat_awal);
        }

        if($tahun!="Semua Tahun"){
            $query = $query." AND pkt_wkt.tahun_berangkat = ?";
            array_push($data,$tahun);
        }

        $query = $query." ORDER BY pkt_wkt.bulan_berangkat ASC";
        return $this->db->query($query,$data)->result();  
    }  
    function getAllPaketV2(){
        $query="SELECT 
                    pkt.paket_id,
                    pkt.nama_paket,
                    pktwkt.bulan_berangkat,
                    pktwkt.tahun_berangkat,
                    kot.nama_kota,
                    pktwkt.paketwaktu_id,
                    (
                        SELECT COUNT(*) FROM `pendaftaran_detail` AS _pndftr_dt 
                        INNER JOIN `paketwaktu_kelas` AS _pktwkt_kls ON _pndftr_dt.pwk_id = _pktwkt_kls.pwk_id 
                        WHERE _pktwkt_kls.paketwaktu_id=pktwkt.paketwaktu_id AND _pndftr_dt.status=1 and _pndftr_dt.status_verifikasi = 'Valid' and _pndftr_dt.status_keberangkatan in ('Dalam Proses','Sudah Berangkat')
                    ) as total_jemaah
                FROM `paket` AS pkt 
                    INNER JOIN `paket_waktu` AS pktwkt ON pkt.paket_id = pktwkt.paket_id
                    INNER JOIN `kota` AS kot ON pkt.berangkat_awal = kot.kota_id
                WHERE pkt.status=1 AND pktwkt.status = 1 ORDER BY  pktwkt.tahun_berangkat asc,pktwkt.bulan_berangkat asc";

        $hasil=$this->db->query($query)->result();  
        return $hasil;
    }

    function getAllPaketByJenis($jenis){
        $query="SELECT kt.nama_kota,pkt_wkt.paketwaktu_id,pkt_wkt.bulan_berangkat,pkt_wkt.tahun_berangkat,pkt.* FROM `paket_waktu` pkt_wkt
                    INNER JOIN `paket` pkt ON pkt.paket_id = pkt_wkt.paket_id
                    INNER JOIN `kota` kt ON pkt.berangkat_awal = kt.kota_id
                    WHERE pkt.jenispaket_id=? AND pkt.status=1 AND pkt_wkt.status=1 ORDER BY pkt_wkt.tahun_berangkat desc,pkt_wkt.bulan_berangkat desc";
        $hasil=$this->db->query($query,array($jenis))->result();  
        return $hasil;
    }

    function getAllPaketHaji(){
        $this->db->order_by('paket_id','desc');
        $hasil = $this->db->get_where('paket',array('jenispaket_id'=>2,'status'=>1))->result();
        return $hasil;
    }

    function getLatestPaket(){
        $this->db->limit(1);
        $this->db->order_by('paket_id','desc');
        $hasil = $this->db->get_where('paket',array('status'=>1))->result();
        return $hasil;
    }

    function getPaketDetailByJenisPaket($jenispaket_id){
        $query="SELECT  kntrcbg.nama_daerah,med_dt.media_link,pkt.paket_id,j_pkt.jenis_paket,pkt.nama_paket
                FROM `paket` AS pkt 
                  INNER JOIN `jenis_paket` AS j_pkt ON j_pkt.jenispaket_id = pkt.jenispaket_id
                  INNER JOIN `media_detail` AS med_dt ON med_dt.media_id = pkt.media_id
                  INNER JOIN `kantor_cabang` AS kntrcbg ON pkt.berangkat_awal = kntrcbg.kantorcabang_id
                WHERE pkt.jenispaket_id = ? AND med_dt.jenis_ukuran='medium' AND j_pkt.status=1 AND med_dt.status=1 AND pkt.status=1";

        $hasil=$this->db->query($query,array($jenispaket_id))->result();  
        return $hasil;

    }

    function getPaketBySearch($berangkat_awal="",$bulan_berangkat="",$tahun_berangkat="",$paket_id="",$jenispaket_id="",$page="",$limit=""){
        $data = array();
        $query="SELECT kt.nama_kota,pkt_wkt.paketwaktu_id,pkt_wkt.bulan_berangkat,pkt_wkt.tahun_berangkat,jns_pkt.jenis_paket,pkt.* 
                FROM `paket` pkt 
                    INNER JOIN kota kt ON kt.kota_id = pkt.berangkat_awal
                    INNER JOIN `jenis_paket` jns_pkt ON jns_pkt.jenispaket_id = pkt.jenispaket_id
                    INNER JOIN `paket_waktu` pkt_wkt ON pkt_wkt.paket_id = pkt.paket_id
                    WHERE kt.status=1 AND pkt.status=1 AND pkt_wkt.status=1 ";
        
        if($paket_id!=""){
            $query = $query." AND pkt.paket_id = ? ";
            array_push($data,$paket_id);
        }

        if($berangkat_awal && $berangkat_awal !="Semua Keberangkatan"){
            $query = $query."AND pkt.berangkat_awal = ? ";
            array_push($data,$berangkat_awal);
        } 

        if($bulan_berangkat && $bulan_berangkat !="Semua Bulan"){
            $query = $query."AND pkt_wkt.bulan_berangkat = ? ";
            array_push($data,$bulan_berangkat);
        }   
    
        if($tahun_berangkat && $tahun_berangkat !="Semua Tahun"){
            $query = $query."AND pkt_wkt.tahun_berangkat = ? ";
            array_push($data,$tahun_berangkat);
        }
        if($jenispaket_id){
            $query = $query."AND pkt.jenispaket_id = ?";
            array_push($data,$jenispaket_id);
        } 
        $query = $query . " ORDER BY pkt_wkt.tahun_berangkat desc,pkt_wkt.bulan_berangkat desc";

        if($limit && $page){
            $query = $query." LIMIT ?,?";
            array_push($data,(int)$limit);
            array_push($data,(int)$page);
        }

        $hasil=$this->db->query($query,$data)->result();  
        return $hasil;
    }

    function getCountPaketBySearch($berangkat_awal="",$bulan_berangkat="",$tahun_berangkat="",$paket_id="",$jenispaket_id=""){
        $data = array();
        $query="SELECT COUNT(*) as total
                FROM `paket` pkt 
                    INNER JOIN kota kt ON kt.kota_id = pkt.berangkat_awal
                    INNER JOIN `jenis_paket` jns_pkt ON jns_pkt.jenispaket_id = pkt.jenispaket_id
                    INNER JOIN `paket_waktu` pkt_wkt ON pkt_wkt.paket_id = pkt.paket_id
                    WHERE kt.status=1 AND pkt.status=1 AND pkt_wkt.status=1 ";
        
        if($paket_id!=""){
            $query = $query." AND pkt.paket_id = ? ";
            array_push($data,$paket_id);
        }

        if($berangkat_awal && $berangkat_awal !="Semua Keberangkatan"){
            $query = $query."AND pkt.berangkat_awal = ? ";
            array_push($data,$berangkat_awal);
        } 

        if($bulan_berangkat && $bulan_berangkat !="Semua Bulan"){
            $query = $query."AND pkt_wkt.bulan_berangkat = ? ";
            array_push($data,$bulan_berangkat);
        }   
    
        if($tahun_berangkat && $tahun_berangkat !="Semua Tahun"){
            $query = $query."AND pkt_wkt.tahun_berangkat = ? ";
            array_push($data,$tahun_berangkat);
        }
        if($jenispaket_id){
            $query = $query."AND pkt.jenispaket_id = ?";
            array_push($data,$jenispaket_id);
        } 
        $query = $query . ' ORDER BY pkt_wkt.tahun_berangkat desc,pkt_wkt.bulan_berangkat desc';

        $hasil=$this->db->query($query,$data)->result();  
        return $hasil;
    }

    function getPaketBySearchAdmin($berangkat_awal="",$bulan_berangkat="",$tahun_berangkat="",$paket_id="",$jenis="",$berangkat_transit="",$maskapai="",$bintang=""){
        $data = array();
        $query="SELECT  pktwkt_kls.pwk_id,
                        pktwkt_kls.harga,
                        pktwkt_kls.harga_awal,
                        pktwkt_kls.diskon,
                        pktwkt_kls.promo,
                        pktwkt_kls.harga_usd,
                        pktwkt_kls.kurs_dollar,
                        kls.kelas,
                        pkt_wkt.paketwaktu_id,
                        pkt_wkt.bulan_berangkat,
                        pkt_wkt.tahun_berangkat,
                        jns_pkt.jenis_paket,
                        pkt.nama_paket,
                        pkt.berangkat_awal,
                        pkt.paket_id,
                        kt.nama_kota,
                        kt.kode_kota
                FROM `paket` pkt 
                    INNER JOIN `jenis_paket` jns_pkt ON jns_pkt.jenispaket_id = pkt.jenispaket_id
                    INNER JOIN `paket_waktu` pkt_wkt ON pkt_wkt.paket_id = pkt.paket_id
                    INNER JOIN `paketwaktu_kelas` pktwkt_kls ON pktwkt_kls.paketwaktu_id = pkt_wkt.paketwaktu_id
                    INNER JOIN `kelas` kls ON kls.kelas_id = pktwkt_kls.kelas_id
                    INNER JOIN `kota` kt on kt.kota_id = pkt.berangkat_awal
                    WHERE pkt.status=1 AND pkt_wkt.status=1 AND pktwkt_kls.status=1 ";
        
        if($paket_id!=""){
            $query = $query." AND pkt.paket_id = ? ";
            array_push($data,$paket_id);
        }

        if($berangkat_awal && $berangkat_awal !="Semua Keberangkatan"){
            $query = $query."AND pkt.berangkat_awal = ? ";
            array_push($data,$berangkat_awal);
        } 

        if($berangkat_transit && $berangkat_transit !="Semua Transit"){
            $query = $query."AND pkt.berangkat_transit = ? ";
            array_push($data,$berangkat_transit);
        } 

        if($maskapai && $maskapai !="Semua Maskapai"){
            $query = $query."AND pkt.paket_id IN (select paket_id from paket_maskapai where maskapai_id = ?) ";
            array_push($data,$maskapai);
        }

        if($bintang && $bintang !="Semua Bintang"){
            $query = $query."AND pkt.paket_id IN (select paket_id from paket_akomodasi where hotel IN (select hotel_id from hotel where bintang = ?)) ";
            array_push($data,$bintang);
        }

        if($bulan_berangkat && $bulan_berangkat !="Semua Bulan"){
            $query = $query."AND pkt_wkt.bulan_berangkat = ? ";
            array_push($data,$bulan_berangkat);
        }   
    
        if($tahun_berangkat && $tahun_berangkat !="Semua Tahun"){
            $query = $query."AND pkt_wkt.tahun_berangkat = ? ";
            array_push($data,$tahun_berangkat);
        } 
        if($jenis!=""){
            $query = $query."AND pkt.jenispaket_id = ? ";
            array_push($data,$jenis);
        } 
        $query = $query . 'order by pkt_wkt.tahun_berangkat asc, 
                                    pkt_wkt.bulan_berangkat asc,
                                    pkt.berangkat_awal asc, 
                                    pkt.nama_paket asc ';
        $hasil=$this->db->query($query,$data)->result();  
        return $hasil;
    }    

    function getPaketBySearchOld($jenis,$berangkat_awal,$berangkat_tahun,$transit,$kelas,$grade_hotel,$lama_hari,$maskapai,$paket_id=""){
        $data = array($jenis);
        $query="SELECT pkt.paket_id,kbrgktn_kls.keberangkatankelas_id, pkt.nama_paket, kbrgktn.tgl_keberangkatan, kls.kelas, kbrgktn_kls.harga FROM `paket` AS pkt 
                    INNER JOIN `keberangkatan` AS kbrgktn ON kbrgktn.paket_id = pkt.paket_id
                    INNER JOIN `keberangkatan_kelas` AS kbrgktn_kls ON kbrgktn_kls.keberangkatan_id = kbrgktn.keberangkatan_id
                    INNER JOIN `kelas` AS kls ON kls.kelas_id = kbrgktn_kls.kelas_id
                    WHERE pkt.jenispaket_id = ? AND kbrgktn.status=1 AND kbrgktn_kls.status=1 AND kls.status=1 AND pkt.status=1 ";
        
        if($paket_id!=""){
            $query = $query." AND pkt.paket_id = ? ";
            array_push($data,$paket_id);
        }

        if($berangkat_awal && $berangkat_awal !="Semua Keberangkatan"){
            $query = $query."AND pkt.berangkat_awal = ? ";
            array_push($data,$berangkat_awal);
        } 

        if($berangkat_tahun && $berangkat_tahun !="Semua Tahun"){
            $query = $query."AND SUBSTRING(kbrgktn.tgl_keberangkatan, 1, 4) = ? ";
            array_push($data,$berangkat_tahun);
        }   
        
        if($transit && $transit !="Semua Transit"){
            $query = $query."AND pkt.berangkat_transit=? ";
            array_push($data,$transit);
        }

        if($grade_hotel && $grade_hotel !="Semua Grade Hotel"){
            $query = $query."AND pkt.paket_id IN (SELECT paket_id from `paket_akomodasi` AS pkt_akom WHERE pkt_akom.grade_hotel = ?) ";
            array_push($data,$grade_hotel);
        }

        if($kelas && $kelas !="Semua Kelas"){
            $query = $query."AND kls.kelas=? ";
            array_push($data,$kelas);
        }

        if($lama_hari && $lama_hari !="Semua Lama Hari"){
            $query = $query."AND pkt.lama_hari=? ";
            array_push($data,$lama_hari);
        }

        if($maskapai && $maskapai !="Semua Maskapai"){
            $query = $query."AND pkt.maskapai=? ";
            array_push($data,$maskapai);
        }


        $hasil=$this->db->query($query,$data)->result();  
        return $hasil;

    }

    function getAllTransit(){
        $query="SELECT DISTINCT berangkat_transit FROM `paket` where status = 1";
        return $this->db->query($query)->result();  
    }

    function getAllLanding(){
        $query="SELECT DISTINCT berangkat_landing FROM `paket` where status = 1";
        return $this->db->query($query)->result();  
    }

    function getAllKeberangkatan(){
        $query="SELECT DISTINCT berangkat_awal FROM `paket` where status = 1";
        return $this->db->query($query)->result();  
    }

    function getAllLamaHariByJenisPaket($id){
        $query="SELECT DISTINCT lama_hari FROM `paket` WHERE jenispaket_id = ? and status = 1 order by lama_hari asc";
        $hasil=$this->db->query($query,array($id))->result();  
        return $hasil;
    }

    function getLatestPaketByPaketId($id){
        $this->db->limit(1);
        $this->db->order_by('suratstatus_id','desc');
        $hasil = $this->db->get_where('paket',array('paket_id'=>$id,'status'=>1))->result();
        return $hasil;
    }

	function getPaketByPaketId($id){
        $hasil = $this->db->get_where('paket',array('paket_id'=>$id,'status'=>1))->result();
        return $hasil;
	}

    function getPaketByProvinsi($id){
        $hasil = $this->db->get_where('paket',array('berangkat_awal'=>$id,'status'=>1))->result();
        return $hasil;
    }
    function addPaket($data){
        $this->db->insert('paket', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function updatePaket($id, $data) {
        $this->db->where('paket_id', $id);
        $this->db->update('paket', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}