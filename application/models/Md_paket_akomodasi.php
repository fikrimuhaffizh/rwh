<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_paket_akomodasi extends CI_Model {

    function getAllGradeHotel(){
        $query="SELECT DISTINCT grade_hotel FROM `paket_akomodasi` order by grade_hotel asc";
        $hasil=$this->db->query($query)->result();  
        return $hasil;
    }
    function getAllHotel(){
        $query="SELECT DISTINCT hotel FROM `paket_akomodasi` order by hotel asc";
        $hasil=$this->db->query($query)->result();  
        return $hasil;
    }
    function getAllKota(){
        $query="SELECT DISTINCT kota FROM `paket_akomodasi` order by kota asc";
        $hasil=$this->db->query($query)->result();  
        return $hasil;
    }
	function getpaketAkomodasiByPaketAkomodasiId($id){
        $hasil = $this->db->get_where('paket_akomodasi',array('paketakomodasi_id'=>$id,'status'=>1))->result();
        return $hasil;
	}
    function getpaketAkomodasiByPaketId($id){
        $hasil = $this->db->get_where('paket_akomodasi',array('paket_id'=>$id,'status'=>1))->result();
        return $hasil;
    }
    function getAllPaketAkomodasi(){
        $hasil = $this->db->get_where('paket_akomodasi',array('status'=>1))->result();
        return $hasil;
    }
    function addPaketAkomodasi($data){
        $this->db->insert('paket_akomodasi', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    function updatePaketAkomodasiByPaketId($id, $data) {
        $this->db->where('paket_id', $id);
        $this->db->update('paket_akomodasi', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    function deletePaketAkomodasiByPaketId($id){
        $this->db->where('paket_id',$id);
        $this->db->delete('paket_akomodasi');
    }

}