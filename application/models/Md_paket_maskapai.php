<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_paket_maskapai extends CI_Model {

    // function getAllMaskapai(){
    //     $hasil = $this->db->get_where('maskapai',array('status'=>1))->result();
    //     return $hasil;
    // }

    function getPaketMaskapaiByPaketId($id){
        $hasil = $this->db->get_where('paket_maskapai',array('paket_id'=>$id,'status'=>1))->result();
        return $hasil;
    }

    function deletePaketMaskapai($param) {
        $this->db->where('paket_id', $param);
        $this->db->delete('paket_maskapai');
    }

    function addPaketMaskapai($data){
        $this->db->insert('paket_maskapai', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}