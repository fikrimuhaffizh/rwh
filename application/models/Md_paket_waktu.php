<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_paket_waktu extends CI_Model {

    function getLatestPaketWaktu(){
        $this->db->limit(1);
        $this->db->order_by('paketwaktu_id','desc');
        $hasil = $this->db->get_where('paket_waktu',array('status'=>1))->result();
        return $hasil;
    }
    function getPaketWaktuByPaketIdV3($id){
        $query = "SELECT * FROM paket_waktu AS pktwkt 
                    INNER JOIN paketwaktu_kelas AS pktwkt_kls ON pktwkt_kls.paketwaktu_id = pktwkt.paketwaktu_id
                    INNER JOIN kelas AS kls ON kls.kelas_id = pktwkt_kls.kelas_id
                    WHERE pktwkt.paket_id = ? AND pktwkt.status=1 AND pktwkt_kls.status=1 ORDER BY pktwkt_kls.harga asc LIMIT 1";
        $hasil=$this->db->query($query,array($id))->result();  
        return $hasil;
    }  
    function getPaketWaktuByPaketIdV2($id){
        $query = "SELECT * FROM paket_waktu AS pktwkt 
                    INNER JOIN paketwaktu_kelas AS pktwkt_kls ON pktwkt_kls.paketwaktu_id = pktwkt.paketwaktu_id
                    INNER JOIN kelas AS kls ON kls.kelas_id = pktwkt_kls.kelas_id
                    WHERE pktwkt.paket_id = ? AND pktwkt.status=1 AND pktwkt_kls.status=1 ORDER BY pktwkt.bulan_berangkat desc";
        $hasil=$this->db->query($query,array($id))->result();  
        return $hasil;
    }   
    function getPaketWaktuByPaketWaktuIdIdV3($id){
        $query = "SELECT * FROM paket_waktu AS pktwkt 
                    INNER JOIN paketwaktu_kelas AS pktwkt_kls ON pktwkt_kls.paketwaktu_id = pktwkt.paketwaktu_id
                    INNER JOIN kelas AS kls ON kls.kelas_id = pktwkt_kls.kelas_id
                    WHERE pktwkt.paketwaktu_id = ? AND pktwkt.status=1 AND pktwkt_kls.status=1 ORDER BY pktwkt.bulan_berangkat desc";
        $hasil=$this->db->query($query,array($id))->result();  
        return $hasil;
    }     
    function getPaketWaktuByPaketWaktuIdV2($id){
        $query = "SELECT * FROM paket_waktu AS pktwkt 
                    INNER JOIN paket AS pkt ON pktwkt.paket_id = pkt.paket_id
                    INNER JOIN kota AS kt ON pkt.berangkat_awal = kt.kota_id
                    WHERE pktwkt.paketwaktu_id = ? AND pktwkt.status=1 AND pkt.status=1 ORDER BY pktwkt.bulan_berangkat desc";
        $hasil=$this->db->query($query,array($id))->result();  
        return $hasil;
    }
     
    function getAllPaketWaktu(){
        $hasil = $this->db->get_where('paket_waktu',array('status'=>1))->result();
        return $hasil;
    }
    function getAllPaketWaktuBulan(){
        $query="  SELECT DISTINCT(bulan_berangkat) FROM paket_waktu WHERE status=1 order by bulan_berangkat asc";
        $hasil=$this->db->query($query)->result();  
        return $hasil;
    }
    function getAllPaketWaktuTahun(){
        $query="  SELECT DISTINCT(tahun_berangkat) FROM paket_waktu WHERE status=1 order by tahun_berangkat asc";
        $hasil=$this->db->query($query)->result();  
        return $hasil;
    }
    function getPaketWaktuByPaketId($id){
        $hasil = $this->db->get_where('paket_waktu',array('paket_id'=>$id,'status'=>1))->result();
        return $hasil;
    }
	function getPaketWaktuById($id){
        $hasil = $this->db->get_where('paket_waktu',array('paketwaktu_id'=>$id,'status'=>1))->result();
        return $hasil;
	}
    function getPaketWaktuByPaketIdByBulanByTahun($id,$bulan,$tahun){
        $hasil = $this->db->get_where('paket_waktu',array('paket_id'=>$id,'bulan_berangkat'=>$bulan,'tahun_berangkat'=>$tahun,'status'=>1))->result();
        return $hasil;
    }
    function addPaketWaktu($data){
        $this->db->insert('paket_waktu', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    function updatePaketWaktuByPaketId($id, $data) {
        $this->db->where('paket_id', $id);
        $this->db->update('paket_waktu', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    function updatePaketWaktuById($id, $data) {
        $this->db->where('paketwaktu_id', $id);
        $this->db->update('paket_waktu', $data);
    }    
    
    function updatePaketWaktuByPwkId($id, $data) {
        // $x = $this->db->get('paketwaktu_kelas',array('pwk_id'=>$id))->result();
        // foreach($x as $row){
        //     paket
        // }
        $this->db->where('pwk.pwk_id', $id);
        $this->db->join('paketwaktu_kelas pwk','pwk.paketwaktu_id = pw.paketwaktu_id');
        $this->db->update('paket_waktu pw', $data);
    }     

    function deletePaketWaktuByPaketId($id){
        $this->db->where('paket_id',$id);
        $this->db->delete('paket_waktu');
    }

     function getTop12PaketWaktuBulan(){
        $query="SELECT distinct tahun_berangkat,bulan_berangkat FROM paket_waktu where status=1 order by tahun_berangkat desc, bulan_berangkat desc limit 12";
        $hasil=$this->db->query($query)->result();  
        return $hasil;
    }
        
}