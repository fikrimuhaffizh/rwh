<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_paketwaktu_kelas extends CI_Model {


    function getAllPaketWaktuKelas(){
        $hasil = $this->db->get_where('paketwaktu_kelas',array('status'=>1))->result();
        return $hasil;
    }
    function getPaketWaktuKelasByPaketWaktuId($id){
        $this->db->order_by('harga','asc');
        $hasil = $this->db->get_where('paketwaktu_kelas',array('paketwaktu_id'=>$id,'status'=>1))->result();
        return $hasil;
    }
	function getPaketWaktuKelasById($id){
        $hasil = $this->db->get_where('paketwaktu_kelas',array('pwk_id'=>$id,'status'=>1))->result();
        return $hasil;
	}

    function getPaketWaktuKelasByIdV2($id){
        $this->db->select('*');
        $this->db->from('paketwaktu_kelas as pktwkt_kls');
        $this->db->join('paket_waktu as pktwkt','pktwkt_kls.paketwaktu_id = pktwkt.paketwaktu_id');
        $this->db->join('kelas as kls','kls.kelas_id = pktwkt_kls.kelas_id');
        $this->db->join('paket as pkt','pkt.paket_id = pktwkt.paket_id');
        $this->db->where('pktwkt_kls.pwk_id ='.$id);
        return $this->db->get()->result();
    }
    
    function getPaketWaktuByPromo(){
       $query="SELECT * FROM paketwaktu_kelas pktwkt_kls 
                INNER JOIN paket_waktu pktwkt ON pktwkt.paketwaktu_id = pktwkt_kls.paketwaktu_id
                INNER JOIN paket pkt ON pkt.paket_id = pktwkt.paket_id
                WHERE pktwkt_kls.promo = 'YA' AND pktwkt_kls.status=1";

        return $this->db->query($query)->result();
    }
    function getPaketWaktuKelasByIdV3($id){
        $this->db->select('*');
        $this->db->from('paketwaktu_kelas as pktwkt_kls');
        $this->db->join('paket_waktu as pktwkt','pktwkt_kls.paketwaktu_id = pktwkt.paketwaktu_id');
        $this->db->join('paket as pkt','pkt.paket_id = pktwkt.paket_id');
        $this->db->where('pktwkt_kls.pwk_id ='.$id);
        $this->db->limit(1);
        return $this->db->get()->result();
    }    
    function addPaketWaktuKelas($data){
        $this->db->insert('paketwaktu_kelas', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    function updatePaketWaktuKelasById($id, $data) {
        $this->db->where('pwk_id', $id);
        $this->db->update('paketwaktu_kelas', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    function updatePaketWaktuKelasByPaketWaktuId($id, $data) {
        $this->db->where('paketwaktu_id', $id);
        $this->db->update('paketwaktu_kelas', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    function deletePaketWaktuKelasByPaketWaktuId($id){
        $this->db->where('paketwaktu_id',$id);
        $this->db->delete('paketwaktu_kelas');
    }
}