<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_pembayaran extends CI_Model {

    function getTotalPembayaranByJemaah($id){
        $query="SELECT SUM(jumlah_bayar) AS total_pembayaran 
                FROM `pembayaran` 
                WHERE pendaftarandetail_id = ? AND status_verifikasi='Accept'  
                AND (status_pembayaran = 'Setoran Awal' OR status_pembayaran = 'Setoran Tambahan' OR status_pembayaran = 'Pelunasan')";
        $hasil=$this->db->query($query,array($id))->result();  
        return $hasil;
    }
    function getLatestPembayaran(){
        $this->db->limit(1);
        $this->db->order_by('pembayaran_id','desc');
        return $this->db->get_where('pembayaran',array('status'=>1))->result();
    }

    function getPembayaranByJemaah($id){
        $this->db->join('pengguna pg','pg.pengguna_id = pmbyr.pengguna_id');
        $this->db->order_by('pmbyr.tgl_update','desc');
        return $this->db->get_where('pembayaran pmbyr',array('pmbyr.pendaftarandetail_id'=>$id,'pmbyr.status'=>1))->result();
    }

    function getPembayaranByJemaahByJenis($id,$jenis){
 
        $this->db->order_by('tgl_update','asc');
        return $this->db->get_where('pembayaran',array('pendaftarandetail_id'=>$id,'jenis_transaksi'=>$jenis,'status'=>1))->result();
    }
    function getPembayaranById($id){
        $this->db->select('
                    pmbyrn.*,
                    b.*,
                    pndftr_dt.pendaftarandetail_id,
                    pndftr_dt.nama_jemaah,
                    pndftr_dt.no_registrasi,
                    kbrgktn.tgl_keberangkatan,
                    pktwkt_kls.*,
                    pktwkt.*,
                    pkt.nama_paket,
                    kt.*,
                    pndftr_dt.harus_bayar,
                    pg.*

            ');
        $this->db->join('pendaftaran_detail pndftr_dt','pndftr_dt.pendaftarandetail_id = pmbyrn.pendaftarandetail_id');
        $this->db->join('keberangkatan kbrgktn','kbrgktn.keberangkatan_id = pndftr_dt.keberangkatan_id','left');
        $this->db->join('paketwaktu_kelas pktwkt_kls','pktwkt_kls.pwk_id = pndftr_dt.pwk_id','left');
        $this->db->join('paket_waktu pktwkt','pktwkt.paketwaktu_id = pktwkt_kls.paketwaktu_id','left');
        $this->db->join('paket pkt','pkt.paket_id = pktwkt.paket_id','left');
        $this->db->join('kota kt','kt.kota_id = pkt.berangkat_awal','left');
        $this->db->join('bank b','b.bank_id = pmbyrn.bank_id','left');
        $this->db->join('pengguna pg','pg.pengguna_id = pmbyrn.pengguna_id');
        $this->db->order_by('pmbyrn.tgl_update','desc');
        return $this->db->get_where('pembayaran pmbyrn',array('pmbyrn.pembayaran_id'=>$id,'pmbyrn.status'=>1))->result();
    }    


    function getAllPembayaran($awal="",$akhir="",$paket="",$jenis_transaksi=""){
        $data = array();
        $query ="SELECT pg.nama_lengkap,kc.nama_cabang,pmbyr.status_verifikasi status_verifikasi_pembayaran, pmbyr.*, pndftr_dt.* FROM pembayaran pmbyr
                    INNER JOIN pendaftaran_detail pndftr_dt ON pndftr_dt.pendaftarandetail_id = pmbyr.pendaftarandetail_id
                    INNER JOIN paketwaktu_kelas pwk ON pwk.pwk_id = pndftr_dt.pwk_id
                    INNER JOIN paket_waktu pw ON pw.paketwaktu_id = pwk.paketwaktu_id
                    INNER JOIN paket pkt ON pkt.paket_id = pw.paket_id
                    INNER JOIN pengguna pg on pg.pengguna_id = pmbyr.pengguna_id
                    INNER JOIN kantor_cabang kc on kc.kantorcabang_id = pg.kantorcabang_id
                    WHERE pmbyr.status=1 AND pndftr_dt.status=1 AND pkt.status=1";

        if($awal && $akhir){
            $query = $query." AND pmbyr.tgl_bayar BETWEEN ? AND  DATE_ADD(?,INTERVAL 1 DAY) ";
            array_push($data,$awal);
            array_push($data,$akhir);
        }

        if($paket!='Semua Paket'){
            $query = $query." AND pkt.paket_id = ? ";
            array_push($data,$paket);
        }

        if($jenis_transaksi!='Semua Jenis'){
            $query = $query." AND pmbyr.jenis_transaksi = ? ";
            array_push($data,$jenis_transaksi);
        }

        $query = $query." ORDER BY pmbyr.no_kwitansi desc";

        $hasil=$this->db->query($query,$data)->result();  
        return $hasil;
    }

    function addPembayaran($data){
        $this->db->insert('pembayaran', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    function updatePembayaranByPendaftaranDetailId($id,$data){
        $this->db->where('pendaftarandetail_id', $id);
        $this->db->update('pembayaran', $data);
    }
    function updatePembayaran($id,$data){
        $this->db->where('pembayaran_id', $id);
        $this->db->update('pembayaran', $data);
    }

    function getListJemaahWithPembayaran($jenis){
        /*
            Ordering pada table mengikuti urutan column yang diselect di query
        */
            if($jenis=='umroh')
                $q = 'pndftr_dt.pwk_id is not null and status_keberangkatan = "Dalam Proses"';
            else if($jenis =='sudah_berangkat')
                $q = 'pndftr_dt.pwk_id is not null and status_keberangkatan = "Sudah Berangkat"';
            else if($jenis =='cancel')
                $q = 'pndftr_dt.pwk_id is not null and status_keberangkatan = "Cancel"';
            else if($jenis=='haji')
                $q = 'pndftr_dt.paket_id is not null';

        return $this->datatables
        ->select('  
            pndftr_dt.tgl_submit,
            pndftr_dt.nama_jemaah,
            kbrgktn.tgl_keberangkatan,
            pndftr_dt.harus_bayar,
            (
                select sum(jumlah_bayar) 
                from pembayaran 
                where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id 
                and jenis_transaksi = "Pembayaran" and status_verifikasi = "Accept" 
                and status=1 and (status_pembayaran = "Setoran Awal" OR status_pembayaran = "Setoran Tambahan" OR status_pembayaran = "Pelunasan")
            ) as total_pembayaran,
            (
                (pndftr_dt.harus_bayar-(pndftr_dt.infant + pndftr_dt.sharing_bed) + pndftr_dt.biaya_kelas_pesawat) - (
                                        select sum(jumlah_bayar) 
                                        from pembayaran where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id 
                                        and jenis_transaksi = "Pembayaran" and status_verifikasi = "Accept" 
                                        and status=1 and (status_pembayaran = "Setoran Awal" OR status_pembayaran = "Setoran Tambahan" OR status_pembayaran = "Pelunasan")
                                    ) + (
                                        select CASE WHEN COUNT(*) > 0 THEN sum(jumlah_bayar) ELSE "0" END
                                        from pembayaran where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id 
                                        and jenis_transaksi = "Pengembalian" and status_verifikasi = "Accept" 
                                        and status=1 and (jenis_pengembalian = "Berlebih")
                                    )
            ) as sisa_tanpa_tambahan_pengurang,
            (
                CASE 
                    WHEN jenis_jemaah = "Tour Leader" THEN "Tour Leader" 
                    WHEN 
                        (
                            select COUNT(*) 
                            from pembayaran 
                            where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id 
                            and jenis_transaksi = "Pembayaran" and status_verifikasi = "Accept" 
                            and status=1 and status_pembayaran = "Pelunasan"
                        ) > 0 THEN "Lunas"
                    WHEN 
                        (
                            select COUNT(*) 
                            from pembayaran 
                            where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id 
                            and jenis_transaksi = "Pembayaran" and status_verifikasi = "Accept" 
                            and status=1 and (status_pembayaran = "Setoran Awal" OR status_pembayaran = "Setoran Tambahan")
                        ) = 0 THEN "Belum Bayar" 
                    WHEN  
                        (
                            select COUNT(*) 
                            from pembayaran 
                            where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id 
                            and jenis_transaksi = "Pembayaran" and status_verifikasi = "Accept" 
                            and status=1 and status_pembayaran = "Pelunasan"
                        ) = 0 THEN "Belum Lunas" ELSE NULL
                END
            ) as status_bayar,            
            pndftr_dt.no_registrasi,
            pndftr_dt.harus_bayar_usd,
            pndftr_dt.kurs_dollar,
            pndftr_dt.pwk_id,
            (select sum(jumlah_bayar) from pembayaran where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and jenis_transaksi = "Pengembalian" and status_verifikasi = "Accept" and status=1) as total_pengembalian,
            pndftr_dt.pendaftarandetail_id,
            pndftr_dt.status_keberangkatan,
            pndftr_dt.ada_mahram,
            pndftr_dt.progressive,
            (select sum(jumlah_bayar) from pembayaran where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and jenis_transaksi = "Pembayaran" and status_verifikasi = "Accept" and status=1 and status_pembayaran="Visa") as total_visa,
            (select sum(jumlah_bayar) from pembayaran where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and jenis_transaksi = "Pembayaran" and status_verifikasi = "Accept" and status=1 and status_pembayaran="Mahram") as total_mahram,
            pndftr_dt.infant,
            pndftr_dt.sharing_bed,
            pndftr_dt.kelas_pesawat,
            pndftr_dt.biaya_kelas_pesawat,
            pndftr_dt.jenis_jemaah,
            (SELECT SUM(jumlah) FROM pendaftaran_detail_biaya where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and jenis="Tambahan" and status = 1) as total_biaya_tambahan,
            (SELECT SUM(jumlah) FROM pendaftaran_detail_biaya where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and jenis="Pengurang" and status = 1) as total_biaya_pengurang
        ')
        ->from('pendaftaran_detail pndftr_dt')
        ->join('keberangkatan kbrgktn','kbrgktn.keberangkatan_id = pndftr_dt.keberangkatan_id','left')
        ->join('paketwaktu_kelas pwk', 'pwk on pwk.pwk_id = pndftr_dt.pwk_id','left')
        ->where('pndftr_dt.status = 1 and pndftr_dt.status_verifikasi = "Valid" AND '.$q)
        ->generate();        
    }
}