<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_pendaftaran extends CI_Model {

    function getPendaftaranAll() {
        $query = "SELECT mbr.nama_lengkap,pdftr.*,pnylgr.nama_usaha FROM `pendaftaran` AS pdftr 
                    INNER JOIN `member` AS mbr ON mbr.member_id = pdftr.member_id
                    INNER JOIN `penyelenggara` AS pnylgr ON pnylgr.penyelenggara_id = pdftr.penyelenggara_id";
        $hasil=$this->db->query($query)->result();  
        return $hasil;
    }

    function getLatestPendaftaran(){
        $this->db->limit(1);
        $this->db->order_by('pendaftaran_id','desc');
        $hasil = $this->db->get_where('pendaftaran',array('status'=>1))->result();
        return $hasil;
    }
    function getPendaftaranById($id){
        $hasil = $this->db->get_where('pendaftaran',array('pendaftaran_id'=>$id,'status'=>1))->result();
        return $hasil;
    }
    function getPendaftaranByMemberId($id){
        $this->db->order_by('pendaftaran_id','desc');        
        $hasil = $this->db->get_where('pendaftaran',array('member_id'=>$id,'status'=>1))->result();
        return $hasil;
    }
    function getAllPendaftarTahun(){
        $query = "SELECT DISTINCT Year(tgl_daftar) as tahun_daftar FROM pendaftaran ORDER BY Year(tgl_daftar) DESC";
        $hasil=$this->db->query($query)->result();  
        return $hasil;
    }
    function addPendaftaran($data){
        $this->db->insert('pendaftaran', $data); 
         if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }
    function addPendaftaran_transact($data){
        $this->db->insert('pendaftaran', $data);
    }
    function updatePendaftaran($id,$data){
        $this->db->where('pendaftaran_id', $id);
        $this->db->update('pendaftaran', $data);
    }

}