<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_pendaftaran_detail extends CI_Model {

    function getLatestPendaftaranDetail(){
        $this->db->limit(1);
        $this->db->order_by('pendaftarandetail_id','desc');
        return $this->db->get_where('pendaftaran_detail',array('status'=>1))->result();
    }
    function countJemaahByStatus($status){
        $this->db->select('COUNT(*) as total');

        switch ($status) {
            case 'berangkat':
                $this->db->where('status_keberangkatan','Sudah Berangkat');
                break;
            case 'proses':
                $this->db->where('status_keberangkatan','Dalam Proses');
                break;
            case 'cancel':
                $this->db->where('status_keberangkatan','Cancel');
                break;

        }
            
        $this->db->where('status_verifikasi','Valid');
        // $this->db->where('pwk_id <>',NULL);
        $hasil = $this->db->get_where('pendaftaran_detail',array('status'=>1))->result();
        return $hasil;
    }
    function getPendaftaranDetailByNoHp($hp){
        $hasil = $this->db->get_where('pendaftaran_detail',array('no_hp'=>$hp,'status'=>1))->result();
        return $hasil;
    }
    function getPendaftaranDetailByPendaftaranId($id){
        $hasil = $this->db->get_where('pendaftaran_detail',array('pendaftaran_id'=>$id,'status'=>1))->result();
        return $hasil;
    }
    function getCountPendaftaranDetailByPwkId($id){
        $this->db->select('COUNT(*) as total');
        $hasil = $this->db->get_where('pendaftaran_detail',array('pwk_id'=>$id,'infant'=>null,'status'=>1))->result();
        return $hasil;
    }    
    function getPendaftaranDetailByKeberangkatanId($id){
        $this->db->join('pendaftaran pndftr','pndftr.pendaftaran_id = pndftr_dt.pendaftaran_id');
        $hasil = $this->db->get_where('pendaftaran_detail pndftr_dt',array('pndftr_dt.keberangkatan_id'=>$id,'pndftr_dt.status'=>1))->result();
        return $hasil;
    }

    function getPendaftaranDetailByKeberangkatanIdByKotaKeberangkatan($keberangkatan_id,$kota_keberangkatan){
        $this->db->join('pendaftaran pndftr','pndftr.pendaftaran_id = pndftr_dt.pendaftaran_id');
        $this->db->join('kota kt','kt.kota_id = pndftr_dt.kota_keberangkatan');
        $this->db->join('keberangkatan kbrngktn','kbrngktn.keberangkatan_id = pndftr_dt.keberangkatan_id');
        $hasil = $this->db->get_where('pendaftaran_detail pndftr_dt',array('pndftr_dt.keberangkatan_id'=>$keberangkatan_id,'pndftr_dt.kota_keberangkatan'=>$kota_keberangkatan,'pndftr_dt.status'=>1))->result();
        return $hasil;
    }

    function getPendaftaranDetailByKeberangkatanIdMember($keberangkatan,$kota_keberangkatan,$no_room=FALSE){
        if($no_room)
            $this->db->where('pndftr_dt.room',null);
        
        $this->db->join('paketwaktu_kelas pwk','pwk.pwk_id = pndftr_dt.pwk_id');
        $this->db->join('kelas kls','kls.kelas_id = pwk.kelas_id');
        $this->db->join('kota kt','kt.kota_id = pndftr_dt.kota_keberangkatan');
        $this->db->join('keberangkatan kbrgktn','kbrgktn.keberangkatan_id = pndftr_dt.keberangkatan_id');
        $this->db->join('pendaftaran pndftr','pndftr.pendaftaran_id = pndftr_dt.pendaftaran_id');
        $this->db->join('member mmbr','mmbr.member_id = pndftr.member_id','left');
        $this->db->join('paket_waktu pktwkt','pktwkt.paketwaktu_id = pwk.paketwaktu_id');
        $this->db->join('paket pkt','pkt.paket_id = pktwkt.paket_id');
        $this->db->where_in('pndftr_dt.status_keberangkatan',array('Dalam Proses','Sudah Berangkat'));
        $this->db->where('pndftr_dt.status_verifikasi','Valid');
        $this->db->order_by('pndftr_dt.pendaftaran_id','desc');
        $hasil = $this->db->get_where('pendaftaran_detail pndftr_dt',array('pndftr_dt.keberangkatan_id'=>$keberangkatan,'pndftr_dt.kota_keberangkatan'=>$kota_keberangkatan,'pndftr_dt.status'=>1))->result();
        return $hasil;
    }

    function getAllPendaftaranDetail(){
        $hasil = $this->db->get_where('pendaftaran_detail',array('status'=>1))->result();
        return $hasil;
    }
    function getPendaftaranDetailHajiByPaketId($id){
        $hasil = $this->db->get_where('pendaftaran_detail',array('pwk_id'=>null,'paket_id'=>$id,'status'=>1))->result();
        return $hasil;
    }
    function getPendaftaranDetailByMemberId($id){
        $query="SELECT mmbr.nama_lengkap, pndftr_dt.* FROM `pendaftaran_detail` AS pndftr_dt 
                    INNER JOIN `pendaftaran` AS pndftr ON pndftr.pendaftaran_id = pndftr_dt.pendaftaran_id
                    INNER JOIN `member` AS mmbr ON mmbr.member_id = pndftr.member_id
                    WHERE pndftr.member_id=? AND pndftr.status=1 AND pndftr_dt.status=1 order by pndftr.tgl_daftar desc";
        $hasil=$this->db->query($query,array($id))->result();  
        return $hasil;
    }
    function getTotalRoomJemaahByKeberangkatanIdByKotaBerangkat($id,$kota_keberangkatan){
        $query="SELECT 
                (select count(*) from pendaftaran_detail 
                    where room like 'Q%' and keberangkatan_id = ? and kota_keberangkatan = ? and status =1 and status_verifikasi = 'Valid' and status_keberangkatan in ('Dalam Proses','Sudah Berangkat')) as total_q,
                (select count(*) from pendaftaran_detail 
                    where room like 'T%' and keberangkatan_id = ? and kota_keberangkatan = ? and status =1 and status_verifikasi = 'Valid' and status_keberangkatan in ('Dalam Proses','Sudah Berangkat')) as total_t,
                (select count(*) from pendaftaran_detail 
                    where room like 'D%' and keberangkatan_id = ? and kota_keberangkatan = ? and status =1 and status_verifikasi = 'Valid' and status_keberangkatan in ('Dalam Proses','Sudah Berangkat')) as total_d;";
        $hasil=$this->db->query($query,array($id,$kota_keberangkatan,$id,$kota_keberangkatan,$id,$kota_keberangkatan))->result();  
        return $hasil;
    }
    function getPendaftaranDetailByPaketId($id="",$status_keberangkatan=""){
        $data = array();
        $query="SELECT *,
                    (SELECT SUM(jumlah) FROM pendaftaran_detail_biaya where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and jenis='Tambahan' and status = 1) as total_biaya_tambahan,
                    (SELECT SUM(jumlah) FROM pendaftaran_detail_biaya where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and jenis='Pengurang' and status = 1) as total_biaya_pengurang,
                    p.nama_lengkap nama_cs
                FROM pendaftaran_detail pndftr_dt 
                    INNER JOIN paketwaktu_kelas pwk_kls ON pwk_kls.pwk_id = pndftr_dt.pwk_id
                    INNER JOIN paket_waktu pktwkt ON pwk_kls.paketwaktu_id = pktwkt.paketwaktu_id
                    INNER JOIN kelas kls on kls.kelas_id = pwk_kls.kelas_id
                    INNER JOIN kota kt on kt.kota_id = pndftr_dt.kota_keberangkatan
                    INNER JOIN pengguna p on p.pengguna_id = pndftr_dt.pengguna_id
                    WHERE pwk_kls.status=1 AND pktwkt.status=1 AND";

         if($id!="Semua Paket"){
            $query = $query." pktwkt.paket_id = ? AND ";
            array_push($data,$id);
        } 

        if($status_keberangkatan){
            $query = $query.' pndftr_dt.status_keberangkatan = ? AND ';
            array_push($data,$status_keberangkatan);
        }


        $query = $query.' pndftr_dt.status=1 ORDER BY pndftr_dt.no_registrasi desc';

        $hasil=$this->db->query($query,$data)->result();  
        return $hasil;
    }  
    function getPendaftaranDetailByPaketIdByKeberangkatanId($paket_id="",$keberangkatan_id=""){
        $query="SELECT *
                FROM pendaftaran_detail pndftr_dt 
                    INNER JOIN paketwaktu_kelas pwk_kls ON pwk_kls.pwk_id = pndftr_dt.pwk_id
                    INNER JOIN paket_waktu pktwkt ON pwk_kls.paketwaktu_id = pktwkt.paketwaktu_id
                    WHERE pwk_kls.status=1 AND pktwkt.status=1 AND pktwkt.paket_id = ? and pndftr_dt.keberangkatan_id = ? and 
                    pndftr_dt.status=1 ORDER BY pndftr_dt.no_registrasi desc";

        $hasil=$this->db->query($query,array($paket_id,$keberangkatan_id))->result();  
        return $hasil;
    }     
    function getPendaftaranDetailByPaketIdV2($id){
        $query="SELECT * FROM pendaftaran_detail WHERE paket_id = ? AND status=1 ";
        $hasil=$this->db->query($query,array($id))->result();  
        return $hasil;
    }    
    function getAllPendaftaranDetailV2($awal="",$akhir="",$validasi=""){
        $data = array();
        $query ="SELECT pndftr_dt.jenis_pendaftaran AS tipe_pendaftaran, pndftr_dt.*,pndftr.*, kbrgktn.* FROM `pendaftaran_detail` AS pndftr_dt
                    INNER JOIN `pendaftaran` AS pndftr ON pndftr_dt.pendaftaran_id = pndftr.pendaftaran_id
                    LEFT JOIN `keberangkatan` AS kbrgktn ON pndftr_dt.keberangkatan_id = kbrgktn.keberangkatan_id
                    WHERE pndftr_dt.status=1 AND pndftr.status=1 ";

        if($awal && $akhir){
            $query = $query." AND pndftr.tgl_daftar BETWEEN ? AND  DATE_ADD(?,INTERVAL 1 DAY) ";
            array_push($data,$awal);
            array_push($data,$akhir);
        }

        if($validasi){
            $query = $query." AND pndftr_dt.status_verifikasi=? ";
            array_push($data,$validasi);
        }

        $query = $query." ORDER BY pndftr_dt.tgl_submit desc";

        $hasil=$this->db->query($query,$data)->result();  
        return $hasil;
    }
    function getAllPendaftaranWithKantorcabangParent($id){

        $query ="SELECT pndftr_dt.jenis_pendaftaran AS tipe_pendaftaran,pndftr_dt.*,pndftr.* FROM `pendaftaran_detail` AS pndftr_dt 
                    INNER JOIN `pendaftaran` AS pndftr ON pndftr_dt.pendaftaran_id = pndftr.pendaftaran_id
                    INNER JOIN `kantor_cabang` AS kntrcbg ON pndftr.kantorcabang_id = kntrcbg.kantorcabang_id
                    WHERE (kntrcbg.parent = ? OR kntrcbg.kantorcabang_id = ?) AND pndftr_dt.status=1 AND pndftr.status=1  ORDER BY pndftr_dt.tgl_submit desc";

        $hasil=$this->db->query($query,array($id,$id))->result();      
        return $hasil;
    }      
    function getAllPendaftaranByKantorcabangId($id){
        $query ="SELECT pndftr_dt.jenis_pendaftaran AS tipe_pendaftaran,pndftr_dt.*,pndftr.* FROM `pendaftaran_detail` AS pndftr_dt 
                    INNER JOIN `pendaftaran` AS pndftr ON pndftr_dt.pendaftaran_id = pndftr.pendaftaran_id
                    WHERE pndftr.kantorcabang_id=? AND pndftr_dt.status=1 AND pndftr.status=1 ORDER BY pndftr_dt.tgl_submit desc";
        $hasil=$this->db->query($query,array($id))->result();  
        return $hasil;
    }
    function getPendaftaranDetailByPendaftarandetailIdV1($id){
        $query ="SELECT pndftr_dt.jenis_pendaftaran AS tipe_pendaftar,pndftr_dt.*,pndftr.*,pkt.* FROM `pendaftaran_detail` AS pndftr_dt 
                    INNER JOIN `pendaftaran` AS pndftr ON pndftr_dt.pendaftaran_id = pndftr.pendaftaran_id
                    INNER JOIN paket as pkt on pkt.paket_id = pndftr_dt.paket_id
                    WHERE pndftr_dt.status=1 AND pndftr.status=1 AND pndftr_dt.pendaftarandetail_id = ?";
        $hasil=$this->db->query($query,array($id))->result();  
        return $hasil;
    }
    function getPendaftaranDetailByPendaftarandetailIdV2($id){
        //pndftr.paket_id is null berarti seluruh pendaftar reguler, karena paket haji pasti berisi paket_id nya
        $query ="SELECT 
                    pndftr_dt.jenis_pendaftaran AS tipe_pendaftar,
                    pndftr_dt.*,
                    pndftr.*,
                    kt.kota_id as id_kota_keberangkatan,
                    kt.nama_kota as kota_keberangkatan,
                    kt.kode_kota as kode_kota_keberangkatan,
                    ktp.kota_id as id_kota_paket,
                    ktp.nama_kota as kota_paket,
                    ktp.kode_kota as kode_kota_paket,
                    pwk.paketwaktu_id,
                    pwk.harga as harga_reguler,
                    pwk.harga_usd as harga_usd_reguler,
                    pwk.kurs_dollar as kurs_dollar_reguler,
                    p.nama_paket as nama_paket_reguler,
                    p_haji.nama_paket as nama_paket_haji,
                    p_haji.harga_terakhir,
                    p_haji.harga_terakhir_usd,
                    p_haji.harga_terakhir_usd_kurs,
                    p_haji.jenis_quota,
                    pw.bulan_berangkat,
                    pw.tahun_berangkat,

                    kl.kelas,
                    kbrgktn.tgl_keberangkatan,
                    (
                        case when pndftr_dt.pwk_id is not null 
                        then('Umroh')
                        else ('Haji')
                        END
                    ) as jenis_paket,
                    (SELECT SUM(jumlah) FROM pendaftaran_detail_biaya where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and jenis='Tambahan' and status = 1) as total_biaya_tambahan,
                    (SELECT SUM(jumlah) FROM pendaftaran_detail_biaya where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and jenis='Pengurang' and status = 1) as total_biaya_pengurang
                FROM `pendaftaran_detail` AS pndftr_dt 
                    LEFT join paket p_haji on p_haji.paket_id = pndftr_dt.paket_id
                    LEFT JOIN paketwaktu_kelas pwk ON pwk.pwk_id = pndftr_dt.pwk_id
                    LEFT JOIN paket_waktu pw on pw.paketwaktu_id = pwk.paketwaktu_id
                    LEFT join paket p on p.paket_id = pw.paket_id
                    LEFT join kota ktp on ktp.kota_id = p.berangkat_awal
                    LEFT join kelas kl on kl.kelas_id = pwk.kelas_id
                    LEFT join kota kt on kt.kota_id = pndftr_dt.kota_keberangkatan
                    LEFT JOIN pendaftaran pndftr ON pndftr_dt.pendaftaran_id = pndftr.pendaftaran_id
                    LEFT JOIN keberangkatan kbrgktn on kbrgktn.keberangkatan_id = pndftr_dt.keberangkatan_id
                    WHERE pndftr_dt.status=1 AND pndftr.status=1 AND pndftr_dt.pendaftarandetail_id = ?";
        $hasil=$this->db->query($query,array($id))->result();  
        return $hasil;
    }

    function getPendaftaranDetailByKantorcabangIdBykodePromo($id,$kode){
        $kode=$kode.'%';
        $query="SELECT pndftr.*,pndftr_dt.* FROM `pendaftaran` pndftr 
                    INNER JOIN `pendaftaran_detail` pndftr_dt ON pndftr_dt.pendaftaran_id = pndftr.pendaftaran_id
                    WHERE pndftr.status=1 AND pndftr_dt.status=1 AND (pndftr.kantorcabang_id = ? OR pndftr.kode_promo like ?)";

        $hasil=$this->db->query($query,array($id,$kode))->result();  
        return $hasil;
    }

    function getPendaftaranDetailByKantorcabangIdBykodePromoByFee($id,$kode,$fee){

        if($fee=='Sudah')
            $query= 'AND status_fee = ?';
        else
            $query= 'AND (status_fee = ? OR status_fee IS NULL) ';

        $kode=$kode.'%';
        $query="SELECT COUNT(pndftr_dt.pendaftarandetail_id) as total FROM `pendaftaran` pndftr 
                    INNER JOIN `pendaftaran_detail` pndftr_dt ON pndftr_dt.pendaftaran_id = pndftr.pendaftaran_id
                    WHERE pndftr.status=1 AND pndftr_dt.status=1 AND (pndftr.kantorcabang_id = ? OR pndftr.kode_promo like ?) ".$query;

        $hasil=$this->db->query($query,array($id,$kode,$fee))->result();  
        return $hasil;
    }

    function getPendaftaranDetailValidProsesPaketWaktuId($id){
        $query="SELECT 
                        pndftr.*,
                        kt.nama_kota,
                        pktwkt.tahun_berangkat,
                        pktwkt.bulan_berangkat,
                        pkt.nama_paket, 
                        pndftr_dt.*,
                        (select SUM(jumlah_bayar) from pembayaran where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and status_verifikasi='Accept' AND (status_pembayaran = 'Setoran Awal' OR status_pembayaran = 'Setoran Tambahan' OR status_pembayaran = 'Pelunasan')) as total_pembayaran,
                        (select sum(jumlah_bayar) from pembayaran where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and jenis_transaksi = 'Pembayaran' and status_verifikasi = 'Accept' and status=1 and status_pembayaran='Visa') as total_visa,
                        (select sum(jumlah_bayar) from pembayaran where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and jenis_transaksi = 'Pembayaran' and status_verifikasi = 'Accept' and status=1 and status_pembayaran='Mahram') as total_mahram
                FROM `pendaftaran_detail` AS pndftr_dt 
                    INNER JOIN `pendaftaran` AS pndftr ON pndftr.pendaftaran_id = pndftr_dt.pendaftaran_id
                    INNER JOIN `paketwaktu_kelas` AS pktwkt_kls ON pndftr_dt.pwk_id = pktwkt_kls.pwk_id 
                    INNER JOIN `paket_waktu` AS pktwkt ON pktwkt_kls.paketwaktu_id = pktwkt.paketwaktu_id
                    INNER JOIN `paket` AS pkt ON pktwkt.paket_id = pkt.paket_id
                    INNER JOIN `kota` AS kt ON pkt.berangkat_awal = kt.kota_id
                    WHERE pktwkt.paketwaktu_id=? AND pndftr.status=1 AND pndftr_dt.status=1 AND pkt.status=1 AND 
                    pktwkt.status=1 and pndftr_dt.status_verifikasi = 'Valid' and pndftr_dt.status_keberangkatan in ('Dalam Proses','Sudah Berangkat') ORDER BY pndftr.tgl_daftar desc";

        $hasil=$this->db->query($query,array($id))->result();  
        return $hasil;
    }

    function getPendaftaraDetailByTglKeberangkatan($tgl){
        $this->db->select('
                    pndftr_dt.*,
                    kbrgktn.*,
                    kls.*,
                    kt.*,
                    pkt.*,
                    pndftr.*,
                    p.nama_lengkap nama_cs,

                    (SELECT SUM(jumlah) FROM pendaftaran_detail_biaya where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and jenis="Tambahan" and status = 1) as total_biaya_tambahan,
                    (SELECT SUM(jumlah) FROM pendaftaran_detail_biaya where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and jenis="Pengurang" and status = 1) as total_biaya_pengurang,
                    (select SUM(jumlah_bayar) from pembayaran where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and status_verifikasi="Accept" AND (status_pembayaran = "Setoran Awal" OR status_pembayaran = "Setoran Tambahan" OR status_pembayaran = "Pelunasan")) as total_pembayaran,
                    (select sum(jumlah_bayar) from pembayaran where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and jenis_transaksi = "Pembayaran" and status_verifikasi = "Accept" and status=1 and status_pembayaran="Visa") as total_visa,
                    (select sum(jumlah_bayar) from pembayaran where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and jenis_transaksi = "Pembayaran" and status_verifikasi = "Accept" and status=1 and status_pembayaran="Mahram") as total_mahram
            ');
        $this->db->from('pendaftaran_detail pndftr_dt');
        $this->db->join('pendaftaran pndftr','pndftr.pendaftaran_id = pndftr_dt.pendaftaran_id');
        $this->db->join('paketwaktu_kelas pwk','pwk.pwk_id = pndftr_dt.pwk_id');
        $this->db->join('kelas kls','kls.kelas_id = pwk.kelas_id');
        $this->db->join('keberangkatan kbrgktn','kbrgktn.keberangkatan_id = pndftr_dt.keberangkatan_id');
        $this->db->join('paket_waktu pktwkt','pktwkt.paketwaktu_id = pwk.paketwaktu_id');
        $this->db->join('paket pkt','pkt.paket_id = pktwkt.paket_id');
        $this->db->join('kota kt','kt.kota_id = pkt.berangkat_awal');
        $this->db->join('pengguna p','p.pengguna_id = pndftr_dt.pengguna_id');
        $this->db->where('kbrgktn.tgl_keberangkatan',$tgl);
        $this->db->where('pndftr_dt.status',1);
        $this->db->where('kbrgktn.status',1);
        $this->db->order_by('pndftr_dt.no_registrasi','desc');
        return $this->db->get()->result();
    }

    function getPendaftaraDetailByKeberangkatanId($id){
        $this->db->select('
                    pndftr_dt.*,
                    kbrgktn.*,
                    kls.*,
                    kt.*,
                    pkt.*,
                    pndftr.*,
                    p.nama_lengkap nama_cs,

                    (SELECT SUM(jumlah) FROM pendaftaran_detail_biaya where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and jenis="Tambahan" and status = 1) as total_biaya_tambahan,
                    (SELECT SUM(jumlah) FROM pendaftaran_detail_biaya where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and jenis="Pengurang" and status = 1) as total_biaya_pengurang,
                    (select SUM(jumlah_bayar) from pembayaran where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and status_verifikasi="Accept" AND (status_pembayaran = "Setoran Awal" OR status_pembayaran = "Setoran Tambahan" OR status_pembayaran = "Pelunasan")) as total_pembayaran,
                    (select sum(jumlah_bayar) from pembayaran where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and jenis_transaksi = "Pembayaran" and status_verifikasi = "Accept" and status=1 and status_pembayaran="Visa") as total_visa,
                    (select sum(jumlah_bayar) from pembayaran where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and jenis_transaksi = "Pembayaran" and status_verifikasi = "Accept" and status=1 and status_pembayaran="Mahram") as total_mahram
            ');
        $this->db->from('pendaftaran_detail pndftr_dt');
        $this->db->join('pendaftaran pndftr','pndftr.pendaftaran_id = pndftr_dt.pendaftaran_id');
        $this->db->join('paketwaktu_kelas pwk','pwk.pwk_id = pndftr_dt.pwk_id');
        $this->db->join('kelas kls','kls.kelas_id = pwk.kelas_id');
        $this->db->join('keberangkatan kbrgktn','kbrgktn.keberangkatan_id = pndftr_dt.keberangkatan_id');
        $this->db->join('paket_waktu pktwkt','pktwkt.paketwaktu_id = pwk.paketwaktu_id');
        $this->db->join('paket pkt','pkt.paket_id = pktwkt.paket_id');
        $this->db->join('kota kt','kt.kota_id = pkt.berangkat_awal');
        $this->db->join('pengguna p','p.pengguna_id = pndftr_dt.pengguna_id');
        $this->db->where('kbrgktn.keberangkatan_id',$id);
        $this->db->where('pndftr_dt.status',1);
        $this->db->where('kbrgktn.status',1);
        $this->db->order_by('pndftr_dt.no_registrasi','desc');
        return $this->db->get()->result();
    }

    function addPendaftaranDetail($data){
        $this->db->insert('pendaftaran_detail', $data); 
         if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }
    function addPendaftaranDetail_transact($data){
        $this->db->insert('pendaftaran_detail', $data); 
    }
    function updatePendaftaranDetail($id,$data){
        $this->db->where('pendaftarandetail_id', $id);
        $this->db->update('pendaftaran_detail', $data);
    }
    function updatePendaftaranDetailByKeberangkatanId($id,$data){
        if(is_array($id))
            $this->db->where_in('keberangkatan_id', $id);
        else
            $this->db->where('keberangkatan_id', $id);

        $this->db->update('pendaftaran_detail', $data);
    }
    function updatePendaftaranDetailByPendaftaranId($id,$data){
        $this->db->where('pendaftaran_id', $id);
        $this->db->update('pendaftaran_detail', $data);
    }

    function getPendaftaranDetailByBulanByKota($tahun,$bulan,$kota_id){
        $query="SELECT * from pendaftaran_detail 
                    where status=1 and pwk_id in (
                        select pwk_id from paketwaktu_kelas where status=1 and paketwaktu_id in (
                            select paketwaktu_id from paket_waktu where status=1 and tahun_berangkat=? and bulan_berangkat=?
                        )
                    ) and kota_keberangkatan=? and status_verifikasi = 'Valid' and status_keberangkatan <> 'Cancel'";

        $hasil=$this->db->query($query,array($tahun,$bulan,$kota_id));  
        return $hasil->num_rows();
    }
    
    function getJmlPendaftaranDetail(){
        $hasil = $this->db->get_where('pendaftaran_detail',array('status'=>1));
        return $hasil->num_rows();
    }

    function getListJemaah($jenis){
        /*
            Ordering pada table mengikuti urutan column yang diselect di query
        */
            if($jenis=='umroh')
                $q = 'pndftr_dt.status_verifikasi ="Valid" and pndftr_dt.pwk_id is not null and status_keberangkatan = "Dalam Proses"';
            else if($jenis =='sudah_berangkat')
                $q = 'pndftr_dt.status_verifikasi ="Valid" and pndftr_dt.pwk_id is not null and status_keberangkatan = "Sudah Berangkat"';
            else if($jenis =='cancel')
                $q = 'pndftr_dt.status_verifikasi ="Valid" and pndftr_dt.pwk_id is not null and status_keberangkatan = "Cancel"';
            else if($jenis=='haji')
                $q = 'pndftr_dt.status_verifikasi ="Valid" and pndftr_dt.paket_id is not null';
            else if($jenis=='tidakValid')
                $q = 'pndftr_dt.status_verifikasi = "Tidak Valid"';

        return $this->datatables
        ->select('  
                pndftr_dt.tgl_submit,
                pndftr_dt.nama_jemaah,
                (
                    case when pndftr_dt.pwk_id is not null 
                    then(
                        select pkt.nama_paket from paketwaktu_kelas pwk
                        left join paket_waktu pw on pw.paketwaktu_id = pwk.paketwaktu_id
                        inner join paket pkt on pkt.paket_id = pw.paket_id 
                        where pwk.pwk_id = pndftr_dt.pwk_id
                    )
                    else (
                        select pkt.nama_paket from paket pkt where pkt.paket_id = pndftr_dt.paket_id
                    )
                    END
                ) as nama_paket,
                kt.nama_kota,
                kbrgktn.tgl_keberangkatan,
                pndftr.tgl_daftar,
                pndftr_dt.no_registrasi,
                pg.nama_lengkap as nama_cs,
                pndftr_dt.status_verifikasi,
                mbr.no_hp as no_hp_member,
                pndftr_dt.pendaftarandetail_id,
                (
                    case when pndftr_dt.pwk_id is not null 
                    then("Umroh")
                    else ("Haji")
                    END
                ) as jenis_paket,
                pndftr_dt.progressive,
                pndftr_dt.status_keberangkatan,
                pw.bulan_berangkat,
                pw.tahun_berangkat,
                pndftr_dt.ada_mahram,
                (select sum(jumlah_bayar) from pembayaran where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and jenis_transaksi = "Pembayaran" and status_verifikasi = "Accept" and status=1 and status_pembayaran="Visa") as total_visa,
                (select sum(jumlah_bayar) from pembayaran where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and jenis_transaksi = "Pembayaran" and status_verifikasi = "Accept" and status=1 and status_pembayaran="Mahram") as total_mahram,
                pndftr_dt.infant,
                pndftr_dt.sharing_bed,
                pndftr_dt.kelas_pesawat,
                pndftr_dt.biaya_kelas_pesawat,
                pndftr_dt.tahun_est_berangkat,
                pg.level,
                pndftr_dt.jenis_jemaah,
                pndftr_dt.no_hp,
                pndftr_dt.no_hp_2,

            ')
        ->from('pendaftaran_detail pndftr_dt')
        ->join('pendaftaran pndftr','pndftr.pendaftaran_id = pndftr_dt.pendaftaran_id')
        ->join('pengguna pg','pg.pengguna_id = pndftr.pengguna_id')
        ->join('paketwaktu_kelas pwk','pwk.pwk_id = pndftr_dt.pwk_id','left')
        ->join('paket_waktu pw','pw.paketwaktu_id = pwk.paketwaktu_id','left')
        ->join('member mbr', 'mbr.member_id = pndftr.member_id','left')
        ->join('keberangkatan kbrgktn', 'kbrgktn.keberangkatan_id = pndftr_dt.keberangkatan_id','left')
        ->join('kota kt','kt.kota_id = pndftr_dt.kota_keberangkatan','left')
        ->where('pndftr_dt.status = 1 AND '.$q)
        ->generate();        
    }

}