<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_pendaftaran_detail_biaya extends CI_Model {

    function getPendaftaranDetailBiayaByPendaftaranDetailIdByJenis($id,$jenis){
        return $this->db->get_where('pendaftaran_detail_biaya',array('pendaftarandetail_id'=>$id,'jenis'=>$jenis,'status'=>1))->result();
    }

    function getPendaftaranDetailBiayaById($id){ 
        return $this->db->get_where('pendaftaran_detail_biaya',array('pdbiaya_id'=>$id,'status'=>1))->result();
    }

    function removePendaftaranDetailBiayaByJemaah($id){
        $this->db->where('pendaftarandetail_id',$id);
        $this->db->delete('pendaftaran_detail_biaya');
    }

    function addPendaftaranDetailBiaya($data){
        $this->db->insert('pendaftaran_detail_biaya', $data); 
         if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }
    
    function addPendaftaranDetailBiaya_transact($data){
        $this->db->insert('pendaftaran_detail_biaya', $data); 
    }
    
    function updatePendaftaranDetailBiaya($id,$data){
        $this->db->where('pdbiaya_id',$id);
        $this->db->update('pendaftaran_detail_biaya', $data); 
    }     
}