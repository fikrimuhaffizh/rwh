<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Md_pengguna extends CI_Model {

    function getPenggunaAll() {
        $query=" SELECT png.*,kntrcbg.nama_cabang,kntrcbg.kantorcabang_id FROM pengguna AS png
                    INNER JOIN kantor_cabang AS kntrcbg ON kntrcbg.kantorcabang_id = png.kantorcabang_id
                    WHERE png.status=1 AND kntrcbg.status=1 ORDER BY png.pengguna_id DESC";
        $hasil=$this->db->query($query)->result();  
        return $hasil;
    }
    
    function getManagePengguna($id) {
        $this->db->order_by('nama','ASC');
        $hasil=$this->db->get_where('pengguna',array('pengguna_id !='=>$id,'status'=>1))->result();
        return $hasil;
    }
    
    function getPenggunaById($id) {
        $this->db->join('kantor_cabang kc','kc.kantorcabang_id = pg.kantorcabang_id');
        return $this->db->get_where('pengguna pg', array('pg.pengguna_id' => $id,'pg.status' => 1))->result();
    }
    function getPenggunaByKantocabangId($id) {
        $hasil = $this->db->get_where('pengguna', array('kantorcabang_id' => $id,'status' => 1))->result();
        $data = $hasil;
        return $data;
    }    
    function getPenggunaByLevel($level) {
        $this->db->from('pengguna');
        $this->db->where_in('level',$level);
        $this->db->where('status',1);
        $this->db->order_by('pengguna_id','desc');
        $hasil = $this->db->get()->result();
        return $hasil;
    }
    function getPenggunaByLevelKantorPusatKeuangan() {
        $query=" SELECT * FROM pengguna WHERE level in ('Kantor Pusat','Keuangan','Executive','Customer Service') And status=1 ORDER BY pengguna_id desc";
        $hasil=$this->db->query($query)->result();  
        return $hasil;
    }
    function getPenggunaByEmail($email) {
        $this->db->join('kantor_cabang kc','kc.kantorcabang_id = pg.kantorcabang_id');
        $hasil = $this->db->get_where('pengguna pg', array('pg.email' => $email,'pg.status' => 1))->result();
        return $hasil;
    }
    function getPenggunaByEmailByPass($email,$pass) {
        $hasil = $this->db->get_where('pengguna', array('email' => $email,'password' => $pass,'status' => 1))->result();
        return $hasil;
    }    
    function updatePengguna($id, $data) {
        $this->db->where('pengguna_id', $id);
        $this->db->update('pengguna', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }    
    function updatePenggunaByKantorcabangId($id, $data) {
        $this->db->where('kantorcabang_id', $id);
        $this->db->update('pengguna', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }     
    function addPengguna($data){
         $this->db->insert('pengguna', $data);
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}