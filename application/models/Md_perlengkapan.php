<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_perlengkapan extends CI_Model {

    function getPerlengkapanByPendaftarandetailId($id){
        $this->db->join('barang_transaksi bt','bt.btrans_id = prlngkpn.btrans_id');
        $this->db->join('barang_kantor bk','bk.bkantor_id = bt.bkantor_id');
        $this->db->join('barang brg','brg.barang_id = bk.barang_id');
        $this->db->join('faktur faktr','faktr.faktur_id = bt.faktur_id');
        $this->db->join('kantor_cabang kc','kc.kantorcabang_id = faktr.kantorcabang_id');

        return $this->db->get_where('perlengkapan prlngkpn',array('prlngkpn.pendaftarandetail_id'=>$id,'prlngkpn.status'=>1,'brg.status'=>1,'bt.status'=>1,'bk.status'=>1))->result();
    }
    function getPerlengkapanByFakturId($id){
        $this->db->join('perlengkapan prlngkpn','prlngkpn.btrans_id = bt.btrans_id');
        $this->db->join('faktur faktr','faktr.faktur_id = bt.faktur_id');
        $this->db->join('barang_kantor bk','bk.bkantor_id = bt.bkantor_id');
        $this->db->join('barang brg','brg.barang_id = bk.barang_id');
        return $this->db->get_where('barang_transaksi bt',array('bt.faktur_id'=>$id,'brg.status'=>1,'faktr.status'=>1))->result();
    }
    function getDetailJemaahByPerlengkapanByFakturId($id){
        $this->db->select('
                    pndftr_dt.nama_jemaah,
                    pndftr_dt.no_registrasi,
                    faktr.no_faktur,
                    brg.nama_barang,
                    brg.satuan,
                    bt.qty,
                    prlngkpn.tgl_penyerahan,
                    ktp.*,
                    pktwkt.*,
                    p.nama_paket as nama_paket_reguler,
                    p_haji.nama_paket as nama_paket_haji,
                    p_haji.harga_terakhir,
                    p_haji.harga_terakhir_usd,
                    p_haji.harga_terakhir_usd_kurs,
                    p_haji.jenis_quota,
                    pktwkt.bulan_berangkat,
                    pktwkt.tahun_berangkat,

                    kl.kelas,
                    kbrgktn.tgl_keberangkatan,
                    (
                        case when pndftr_dt.pwk_id is not null 
                        then("Umroh")
                        else ("Haji")
                        END
                    ) as jenis_paket,
                    faktr.keterangan,
            ');
        $this->db->join('perlengkapan prlngkpn','prlngkpn.btrans_id = bt.btrans_id');
        $this->db->join('faktur faktr','faktr.faktur_id = bt.faktur_id');
        $this->db->join('barang_kantor bk','bk.bkantor_id = bt.bkantor_id');
        $this->db->join('barang brg','brg.barang_id = bk.barang_id');
        $this->db->join('pendaftaran_detail pndftr_dt','pndftr_dt.pendaftarandetail_id = prlngkpn.pendaftarandetail_id');
        $this->db->join('keberangkatan kbrgktn','kbrgktn.keberangkatan_id = pndftr_dt.keberangkatan_id','left');
        $this->db->join('paketwaktu_kelas pktwkt_kls','pktwkt_kls.pwk_id = pndftr_dt.pwk_id','left');
        $this->db->join('paket_waktu pktwkt','pktwkt.paketwaktu_id = pktwkt_kls.paketwaktu_id','left');
        $this->db->join('kelas kl','kl.kelas_id = pktwkt_kls.kelas_id');
        //Paket Reguler
        $this->db->join('paket p','p.paket_id = pktwkt.paket_id','left');
        //Paket Haji
        $this->db->join('paket p_haji','p_haji.paket_id = pndftr_dt.paket_id','left');
        $this->db->join('kota ktp','ktp.kota_id = p.berangkat_awal','left');
        return $this->db->get_where('barang_transaksi bt',array('bt.faktur_id'=>$id,'brg.status'=>1,'faktr.status'=>1))->result();
    }
    function getTotalPerlengkapanByPendaftarandetailId($id){
        $query = "SELECT SUM(qty) as total_barang FROM perlengkapan where pendaftarandetail_id = ? and status = 1";
        return $this->db->query($query,array($id))->result();  
    }

    function addPerlengkapan($data){
        $this->db->insert('perlengkapan', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    function addPerlengkapan_transact($data){
        $this->db->insert('perlengkapan', $data); 
    }

    function getPerlengkapanById($id){
        return $this->db->get_where('perlengkapan pr',array('pr.perlengkapan_id'=>$id,'pr.status'=>1))->result();
    }    

    function updatePerlengkapan($id,$data){
        $this->db->where('perlengkapan_id',$id);
        $this->db->update('perlengkapan',$data);
    }

    function updatePerlengkapanByPendaftaranDetailIdByBtransId($id,$btrans_id,$data){
        $this->db->where('pendaftarandetail_id',$id);
        $this->db->where('btrans_id',$id);
        $this->db->update('perlengkapan',$data);
    }

    // function getListJemaahWithPersyaratan($jenis){
    //     /*
    //         Ordering pada table mengikuti urutan column yang diselect di query
    //     */
    //     if($jenis=='umroh')
    //         $q = 'pndftr_dt.pwk_id is not null';
    //     else if($jenis=='haji')
    //         $q = 'pndftr_dt.paket_id is not null';

    //     return $this->datatables
    //     ->select('  
    //         pndftr_dt.tgl_submit,
    //         pndftr_dt.nama_jemaah,
    //         kbrgktn.tgl_keberangkatan,
    //         pwk.harga as harus_bayar,
    //         pndftr_dt.pwk_id,
    //         (select count(*) from perlengkapan where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and status =1) as total_syarat,
    //         (select count(*) from perlengkapan where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and status_syarat="Belum Lengkap" and status =1) as belum_lengkap,
    //         pndftr_dt.pendaftarandetail_id')
    //     ->from('pendaftaran_detail pndftr_dt')
    //     ->join('keberangkatan kbrgktn','kbrgktn.keberangkatan_id = pndftr_dt.keberangkatan_id','left')
    //     ->join('paketwaktu_kelas pwk', 'pwk on pwk.pwk_id = pndftr_dt.pwk_id','left')
    //     ->where('pndftr_dt.status = 1 and pndftr_dt.status_verifikasi = "Valid"')
    //     ->generate();        
    // }


    function getListJemaahWithPerlengkapan($jenis){
        /*
            Ordering pada table mengikuti urutan column yang diselect di query
        */
            if($jenis=='umroh')
                $q = 'pndftr_dt.pwk_id is not null and status_keberangkatan = "Dalam Proses"';
            else if($jenis =='sudah_berangkat')
                $q = 'pndftr_dt.pwk_id is not null and status_keberangkatan = "Sudah Berangkat"';
            else if($jenis =='cancel')
                $q = 'pndftr_dt.pwk_id is not null and status_keberangkatan = "Cancel"';
            else if($jenis=='haji')
                $q = 'pndftr_dt.paket_id is not null';

        return $this->datatables
        ->select('  
            pndftr_dt.tgl_submit,
            pndftr_dt.nama_jemaah,
            kbrgktn.tgl_keberangkatan,
            pndftr_dt.no_registrasi,
            (
                select sum(bt.qty) from perlengkapan prlngkpn
                inner join barang_transaksi bt on bt.btrans_id = prlngkpn.btrans_id
                inner join barang_kantor bk on bk.bkantor_id = bt.bkantor_id
                where prlngkpn.pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and bt.status = 1 and prlngkpn.status = 1 and bk.status = 1
            ) as total_penyerahan,
            pndftr_dt.jenis_jemaah,
            pwk.harga as harus_bayar,
            pndftr_dt.pwk_id,
            pndftr_dt.pendaftarandetail_id,
            pndftr_dt.status_keberangkatan,
            pndftr_dt.ada_mahram,
            pndftr_dt.progressive,
            (select sum(jumlah_bayar) from pembayaran where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and jenis_transaksi = "Pembayaran" and status_verifikasi = "Accept" and status=1 and status_pembayaran="Visa") as total_visa,
            (select sum(jumlah_bayar) from pembayaran where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and jenis_transaksi = "Pembayaran" and status_verifikasi = "Accept" and status=1 and status_pembayaran="Mahram") as total_mahram,
            pndftr_dt.infant,
            pndftr_dt.sharing_bed,
            pndftr_dt.kelas_pesawat,
            pndftr_dt.biaya_kelas_pesawat,
            pndftr_dt.tahun_est_berangkat,
            (
                case when pndftr_dt.pwk_id is not null 
                then("Umroh")
                else ("Haji")
                END
            ) as jenis_paket
        ')
        ->from('pendaftaran_detail pndftr_dt')
        ->join('keberangkatan kbrgktn','kbrgktn.keberangkatan_id = pndftr_dt.keberangkatan_id','left')
        ->join('paketwaktu_kelas pwk', 'pwk on pwk.pwk_id = pndftr_dt.pwk_id','left')
        ->where('pndftr_dt.status = 1 and pndftr_dt.status_verifikasi = "Valid" AND '.$q)
        ->generate();        
    }    
}