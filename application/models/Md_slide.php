<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_slide extends CI_Model {

    function getLatestSlide(){
        $this->db->limit(1);
        $this->db->order_by('slide_id','desc');
        $hasil = $this->db->get_where('slide',array('status'=>1))->result();
        return $hasil;
    }

    function getAllSlide(){
        $hasil = $this->db->get_where('slide',array('status'=>1))->result();
        return $hasil;
    }

    function getSlideBySlideId($id){
        $hasil = $this->db->get_where('slide',array('slide_id'=>$id,'status'=>1))->result();
        return $hasil;
    }

    function updateSlide($param, $data) {
        $this->db->where('slide_id', $param);
        $this->db->update('slide', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function addSlide($data){
        $this->db->insert('slide', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}