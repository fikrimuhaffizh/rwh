<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_syarat_status extends CI_Model {

    function cekSyaratStatusPendaftar($syaratumroh_id,$pendaftarandetail_id){
        $hasil = $this->db->get_where('syarat_status',array('pendaftarandetail_id'=>$pendaftarandetail_id,'syaratumroh_id'=>$syaratumroh_id,'status'=>1))->result();
        return $hasil;
    }

    function getSyaratBelumLengkapByJemaah($id){
        $hasil = $this->db->get_where('syarat_status',array('pendaftarandetail_id'=>$id,'status_syarat'=>'Belum Lengkap','status'=>1))->result();
        return $hasil;
    }
    function getSyaratStatusBySyaratUmrohId($id){
        $hasil = $this->db->get_where('syarat_status',array('syaratumroh_id'=>$id,'status'=>1))->result();
        return $hasil;
    }
    function getSyaratStatusByPendaftarandetailId($id){
        $hasil = $this->db->get_where('syarat_status',array('pendaftarandetail_id'=>$id,'status'=>1))->result();
        return $hasil;
    }

	function getSyaratStatusByPendaftarandetailIdV2($id){
        $query="SELECT syrt_umrh.tipe,syrt_umrh.persyaratan, syrt_st.*,pd.*,pkt.nama_paket as nama_paket_reguler,pkt_haji.nama_paket as nama_paket_haji,kt.nama_kota,pktwkt.*,pktwkt_kls.*,kbrgktn.* FROM `syarat_status` AS syrt_st 
                    INNER JOIN `syarat_umroh` AS syrt_umrh ON syrt_umrh.syaratumroh_id = syrt_st.syaratumroh_id
                    INNER JOIN pendaftaran_detail pd ON pd.pendaftarandetail_id = syrt_st.pendaftarandetail_id
                    LEFT JOIN keberangkatan kbrgktn ON kbrgktn.keberangkatan_id = pd.keberangkatan_id
                    LEFT JOIN paketwaktu_kelas pktwkt_kls ON pktwkt_kls.pwk_id = pd.pwk_id
                    LEFT JOIN paket_waktu pktwkt ON pktwkt.paketwaktu_id = pktwkt_kls.paketwaktu_id
                    LEFT JOIN paket pkt ON pkt.paket_id = pktwkt.paket_id
                    LEFT JOIN paket pkt_haji ON pkt_haji.paket_id = pd.paket_id
                    LEFT JOIN kota kt ON kt.kota_id = pkt.berangkat_awal
                    WHERE syrt_st.pendaftarandetail_id = ? AND syrt_st.status=1 AND syrt_umrh.status = 1";

        $hasil=$this->db->query($query,array($id))->result();  
        return $hasil;
	}
    function addSyaratStatus($data){
        $this->db->insert('syarat_status', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    function addSyaratStatus_transact($data){
        $this->db->insert('syarat_status', $data); 
    }

    function updateAllSyaratStatusBySyaratStatusId($data,$id){
        $this->db->where('syaratstatus_id',$id);
        $this->db->update('syarat_status',$data);
    }
    function updateAllSyaratStatusByJemaah($data,$jemaah){
        $this->db->where('pendaftarandetail_id',$jemaah);
        $this->db->update('syarat_status',$data);
    }
    function updateSyaratStatusByJemaah($data,$id,$jemaah){
        $this->db->where('pendaftarandetail_id',$jemaah);
        $this->db->where('syaratumroh_id',$id);
        $this->db->update('syarat_status',$data);
    }

    function removeSyaratStatusBySyaratUmrohId($id){
        $this->db->where('syaratumroh_id',$id);
        $this->db->delete('syarat_status');
    }

    function removeSyaratStatusByJemaah($id,$jemaah){
        $this->db->where('pendaftarandetail_id',$jemaah);
        $this->db->where('syaratumroh_id',$id);
        $this->db->delete('syarat_status');
    }

    function getListJemaahWithPersyaratan($jenis){
        /*
            Ordering pada table mengikuti urutan column yang diselect di query
        */
            if($jenis=='umroh')
                $q = 'pndftr_dt.pwk_id is not null and status_keberangkatan = "Dalam Proses"';
            else if($jenis =='sudah_berangkat')
                $q = 'pndftr_dt.pwk_id is not null and status_keberangkatan = "Sudah Berangkat"';
            else if($jenis =='cancel')
                $q = 'pndftr_dt.pwk_id is not null and status_keberangkatan = "Cancel"';
            else if($jenis=='haji')
                $q = 'pndftr_dt.paket_id is not null';

        return $this->datatables
        ->select('  
            pndftr_dt.tgl_submit,
            pndftr_dt.nama_jemaah,
            kbrgktn.tgl_keberangkatan,
            pndftr_dt.no_registrasi,
            (
                case 
                    when (select COUNT(*) from syarat_status where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and status_syarat = "Belum Lengkap" and status = 1) = 0 
                then(
                    "Sudah Lengkap"
                )
                else (
                    "Belum Lengkap"
                )
                END
            ) as status_syarat,
            pwk.harga as harus_bayar,
            pndftr_dt.pwk_id,
            (select count(*) from syarat_status where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and status =1) as total_syarat,
            pndftr_dt.pendaftarandetail_id,
            pndftr_dt.status_keberangkatan,
            pndftr_dt.ada_mahram,
            pndftr_dt.progressive,
            (select sum(jumlah_bayar) from pembayaran where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and jenis_transaksi = "Pembayaran" and status_verifikasi = "Accept" and status=1 and status_pembayaran="Visa") as total_visa,
            (select sum(jumlah_bayar) from pembayaran where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and jenis_transaksi = "Pembayaran" and status_verifikasi = "Accept" and status=1 and status_pembayaran="Mahram") as total_mahram,
            (select COUNT(*) from syarat_status where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and status_syarat = "Belum Lengkap" and status = 1) as syarat_belum_lengkap,
            (select COUNT(*) from syarat_status where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and status_syarat = "Lengkap" and status = 1) as syarat_lengkap,
            (select COUNT(*) from syarat_status where pendaftarandetail_id = pndftr_dt.pendaftarandetail_id and status = 1) as total_syarat,
            pndftr_dt.infant,
            pndftr_dt.sharing_bed,
            pndftr_dt.kelas_pesawat,
            pndftr_dt.biaya_kelas_pesawat,
            pndftr_dt.tahun_est_berangkat,
            (
                case when pndftr_dt.pwk_id is not null 
                then("Umroh")
                else ("Haji")
                END
            ) as jenis_paket,
            pndftr_dt.jenis_jemaah
        ')
        ->from('pendaftaran_detail pndftr_dt')
        ->join('keberangkatan kbrgktn','kbrgktn.keberangkatan_id = pndftr_dt.keberangkatan_id','left')
        ->join('paketwaktu_kelas pwk', 'pwk on pwk.pwk_id = pndftr_dt.pwk_id','left')
        ->where('pndftr_dt.status = 1 and pndftr_dt.status_verifikasi = "Valid" AND '.$q)
        ->generate();        
    }

}