<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_syarat_umroh extends CI_Model {

    function getAllSyaratUmroh(){
        $this->db->order_by('syaratumroh_id','desc');
        $hasil = $this->db->get_where('syarat_umroh',array('jenis'=>'Umroh','status'=>1))->result();
        return $hasil;
    }
    function getAllSyaratHaji(){
        $this->db->order_by('syaratumroh_id','desc');
        $hasil = $this->db->get_where('syarat_umroh',array('jenis'=>'Haji','status'=>1))->result();
        return $hasil;
    }
    function getSyaratUmrohById($id){
        $hasil = $this->db->get_where('syarat_umroh',array('syaratumroh_id'=>$id,'status'=>1))->result();
        return $hasil;
    }
    function getAllSyaratUmrohDefault(){
        $this->db->order_by('syaratumroh_id','desc');
        $hasil = $this->db->get_where('syarat_umroh',array('aktif'=>1,'jenis'=>'Umroh','status'=>1))->result();
        return $hasil;
    }
    function getAllSyaratHajiDefault(){
        $this->db->order_by('syaratumroh_id','desc');
        $hasil = $this->db->get_where('syarat_umroh',array('aktif'=>1,'jenis'=>'Haji','status'=>1))->result();
        return $hasil;
    }

    function updateSyaratUmroh($param, $data) {
        $this->db->where('syaratumroh_id', $param);
        $this->db->update('syarat_umroh', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function addSyaratUmroh($data){
        $this->db->insert('syarat_umroh', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}