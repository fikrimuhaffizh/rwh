<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_testimoni extends CI_Model {

    function getAllTestimoni(){
        $this->db->order_by('tgl_post','desc');
        $hasil = $this->db->get_where('testimoni',array('status'=>1))->result();
        return $hasil;
    }
    function getTestimoniById($id){
        $hasil = $this->db->get_where('testimoni',array('testimoni_id'=>$id,'status'=>1))->result();
        return $hasil;
    }

    function updateTestimoni($param, $data) {
        $this->db->where('testimoni_id', $param);
        $this->db->update('testimoni', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function addTestimoni($data){
        $this->db->insert('testimoni', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}