<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_tour_leader extends CI_Model {

	// function getAllTourLeader(){
 //        $hasil = $this->db->get_where('tour_leader',array('status'=>1))->result();
 //        return $hasil;
	// }

    function getTourLeaderById($id){
        $hasil = $this->db->get_where('tour_leader',array('tourleader_id'=>$id,'status'=>1))->result();
        return $hasil;
    }

    function getAllTourLeaderV2(){
        $hasil = $this->db->get_where('tour_leader',array('status'=>1))->result();
        return $hasil;
    }

    function updateTourLeader($param, $data) {
        $this->db->where('tourleader_id', $param);
        $this->db->update('tour_leader', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function addTourLeader($data){
        $this->db->insert('tour_leader', $data); 
         if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    function getAllTourLeader(){
        /*
            Ordering pada table mengikuti urutan column yang diselect di query
        */
        return $this->datatables
        ->select('tourleader_id,nama,jenis_kelamin,no_hp,alamat')
        ->from('tour_leader')
        ->where('status = 1')
        ->generate();        
    }
}