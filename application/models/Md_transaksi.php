<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Md_transaksi extends CI_Model {

	// function getAllBarang(){
 //        $hasil = $this->db->get_where('barang',array('status'=>1))->result();
 //        return $hasil;
	// }

    function getBarangById($id){
        $hasil = $this->db->get_where('barang',array('barang_id'=>$id,'status'=>1))->result();
        return $hasil;
    }
    function getFakturById($id){
        $hasil = $this->db->get_where('faktur',array('faktur_id'=>$id,'status'=>1))->result();
        return $hasil;
    }
    function getFakturByIdByKantorcabangId($id,$kantorcabang_id){
        $hasil = $this->db->get_where('faktur',array('faktur_id'=>$id,'kantorcabang_id'=>$kantorcabang_id,'status'=>1))->result();
        return $hasil;
    }
    function getBarangTransaksiByBarangByKantorCabang($barang_id,$kantorcabang_id){
        $this->db->join('barang_kantor bk','bk.bkantor_id = bt.bkantor_id');
        $this->db->join('barang b','b.barang_id = bk.barang_id');
        $this->db->join('kantor_cabang kc','kc.kantorcabang_id=bk.kantorcabang_id');
        $hasil = $this->db->get_where('barang_transaksi bt',array('bk.kantorcabang_id'=>$kantorcabang_id,'bk.barang_id'=>$barang_id));
        return $hasil->result();
    }
    function getBarangTransaksiByKantorCabang($kantorcabang_id){
        $this->db->join('barang_kantor bk','bk.bkantor_id = bt.bkantor_id');
        $this->db->join('barang b','b.barang_id=bk.barang_id');
        $this->db->join('kantor_cabang kc','kc.kantorcabang_id=bk.kantorcabang_id');
        $hasil = $this->db->get_where('barang_transaksi bt',array('bk.kantorcabang_id'=>$kantorcabang_id));
        return $hasil->result();
    }
    function getBarangTransaksiByFakturId($id){
        $this->db->select('faktr.*,kc.nama_cabang,brg.barang_id,brg.nama_barang,brg.kode_barang,bt.*,bt.btrans_id');
        $this->db->join('faktur faktr','faktr.faktur_id = bt.faktur_id');
        $this->db->join('barang_kantor bk','bk.bkantor_id = bt.bkantor_id');
        $this->db->join('barang brg','brg.barang_id = bk.barang_id');
        $this->db->join('kantor_cabang kc','kc.kantorcabang_id = faktr.kantorcabang_id');
        return $this->db->get_where('barang_transaksi bt',array('bt.faktur_id'=>$id,'brg.status'=>1,'faktr.status'=>1))->result();
    }
    function getAllBarangV2(){
        $this->db->select('barang');
        $hasil = $this->db->get_where('barang',array('status'=>1))->result();
        return $hasil;
    }

    function getFakturByPendaftaranDetailId($id){
        $query = "SELECT * from faktur fk  where faktur_id in 
                    (select faktur_id from barang_transaksi where btrans_id in 
                        (select btrans_id from perlengkapan where pendaftarandetail_id = ? and status =1) and status = 1
                    ) 
                and status = 1
                order by faktur_id desc;";
        return $this->db->query($query,array($id))->result();
    }

    function getLatestFakturByJenisFaktur($jenis){
        $query = "SELECT * FROM faktur WHERE jenis_faktur = ? order by faktur_id desc limit 1";
        return $this->db->query($query,array($jenis))->result();
    }

    function getTop200RiwayatTransaksiBarang($bkantor_id){
        $query = "SELECT * FROM barang_transaksi bt 
                    INNER JOIN faktur fk on fk.faktur_id = bt.faktur_id 
                    INNER JOIN barang_kantor bk on bk.bkantor_id = bt.bkantor_id
                    INNER JOIN kantor_cabang kc on kc.kantorcabang_id = bk.kantorcabang_id
                    INNER JOIN barang b on b.barang_id = bk.barang_id
                    WHERE bk.bkantor_id = ? and b.status=1 and bk.status=1 and fk.status=1 and bt.status=1
                    order by fk.tgl_transaksi desc limit 200";
        return $this->db->query($query,array($bkantor_id))->result();
    }

    function getLaporanRiwayatTransaksiBarang($kantorcabang_id,$barang_id){
        $data = array();
        $query = "SELECT * FROM barang_transaksi bt 
                    INNER JOIN faktur fk on fk.faktur_id = bt.faktur_id 
                    INNER JOIN barang b on b.barang_id = bt.barang_id
                    INNER JOIN kantor_cabang kc on kc.kantorcabang_id = fk.kantorcabang_id
                    WHERE b.status = 1 and fk.status = 1 ";

        if($kantorcabang_id != 'Semua Kantor'){
            $query = $query."AND fk.kantorcabang_id = ? ";
            array_push($data,$kantorcabang_id);
        }

        if($barang_id != 'Semua Barang'){
            $query = $query."AND bt.barang_id = ? ";
            array_push($data,$barang_id);
        }        

        $query = $query."ORDER BY fk.tgl_transaksi desc";   
        return $this->db->query($query,$data)->result();
    }

    function updateFaktur($param, $data) {
        $this->db->where('faktur_id', $param);
        $this->db->update('faktur', $data);
    }

    function updateBarangTransaksiByFakturId($param, $data) {
        $this->db->where('faktur_id', $param);
        $this->db->update('barang_transaksi', $data);
    }

    function addFaktur($data){
        $this->db->insert('faktur', $data); 
         if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    function addFaktur_transact($data){
        $this->db->insert('faktur',$data);
    }

    function addBarang_Transaksi($data){
        $this->db->insert('barang_transaksi', $data); 
         if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    function addBarang_Transaksi_transact($data){
        $this->db->insert('barang_transaksi', $data); 
    }

    function getAllFaktur($kantorcabang_id){
        $x='';
        if($kantorcabang_id)
            $x = ' and kc.kantorcabang_id = '.$kantorcabang_id;
        /*
            Ordering pada table mengikuti urutan column yang diselect di query
        */
        return $this->datatables
        ->select('
                faktr.faktur_id,
                faktr.no_faktur,
                faktr.jenis_faktur,
                faktr.total_qty,
                faktr.tgl_transaksi,
                pngg.nama_lengkap,
                kc.nama_cabang')
        ->from('faktur faktr')
        ->join('kantor_cabang kc','kc.kantorcabang_id = faktr.kantorcabang_id')
        ->join('pengguna pngg','pngg.pengguna_id = faktr.pengguna_id')
        ->where('faktr.status = 1'.$x)
        ->generate();        
    }  

    function getAllBarangTransaksi($kantorcabang_id){
        $x='';
        if($kantorcabang_id)
            $x = ' and kc.kantorcabang_id = '.$kantorcabang_id;
        /*
            Ordering pada table mengikuti urutan column yang diselect di query
        */
        return $this->datatables
        ->select('
                faktr.faktur_id,
                brg.nama_barang,
                faktr.jenis_faktur,
                kc.nama_cabang,
                bt.qty,
                bt.stok_awal,
                bt.stok_akhir,
                faktr.tgl_transaksi,
                faktr.no_faktur,
                pngg.nama_lengkap
            ')
        ->from('barang_transaksi bt')
        ->join('barang_kantor bk','bk.bkantor_id = bt.bkantor_id')
        ->join('barang brg','brg.barang_id = bk.barang_id')
        ->join('faktur faktr','faktr.faktur_id = bt.faktur_id')
        ->join('kantor_cabang kc','kc.kantorcabang_id = faktr.kantorcabang_id')
        ->join('pengguna pngg','pngg.pengguna_id = faktr.pengguna_id')
        ->where('faktr.status = 1 AND brg.status=1 AND bt.status=1'.$x)
        ->generate();        
    }
}