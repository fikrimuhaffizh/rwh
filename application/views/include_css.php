<?php //Bootstrap Core CSS ?>
<link href="<?php echo base_url(); ?>assets/frontend/css/bootstrap.min.css" rel="stylesheet">

<?php //Layerslider ?>
<link href="<?php echo base_url(); ?>assets/frontend/layerslider/css/layerslider.css" rel="stylesheet">

<?php //Theme ?>
<link href="<?php echo base_url(); ?>assets/frontend/css/style.css" rel="stylesheet">

<?php //Color ?>
<link id="skin" href="<?php echo base_url(); ?>assets/frontend/skins/yellow.css" rel="stylesheet">

<?php //Floating Button ?>
<link href="<?php echo base_url(); ?>assets/frontend/css/kc.fab.css" rel="stylesheet">
