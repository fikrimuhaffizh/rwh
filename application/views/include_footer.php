<footer class="paddingtop-clear padding-top40">
    <div class="kc_fab_wrapper"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <div class="widget">
                    <h5>Tentang Riau Wisata Hati</h5>
                    <img src="<?php echo base_url(); ?>/assets/frontend/img/brand/logo-black.png" class="marginbot15" alt="Riau Wisata Hati Logo" />
                    <p>PT.Riau Wisata Hati, Travel umrah terbaik di Riau sebagai penyelenggara resmi Ibadah Umrah dari Kemenag RI dengan SK No. 167 Tahun 2016</p>
                    <a href="https://www.facebook.com/RiauWisataHati"><i class="fa fa-facebook fa-2x icon-square"></i></a>
                    <a href="javascript:;"><i class="fa fa-google-plus fa-2x icon-square"></i></a>
                    <a href="javascript:;"><i class="fa fa-instagram fa-2x icon-square"></i></a>
                    <a href="javascript:;"><i class="fa fa-youtube fa-2x icon-square"></i></a>
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <div class="widget">
                    <h5>Site Link</h5>
                    <ul class="list-icons link-list">
                        <li><i class="fa fa-angle-double-right"></i> <a href="<?php echo base_url() ?>">Beranda</a></li>
                        <li><i class="fa fa-angle-double-right"></i> <a href="<?php echo base_url().'profil' ?>">Profil</a></li>
                        <li><i class="fa fa-angle-double-right"></i> <a href="<?php echo base_url().'umroh' ?>">Haji dan Umroh</a></li>
                        <li><i class="fa fa-angle-double-right"></i> <a href="<?php echo base_url().'manasik' ?>">Manasik</a></li>
                        <li><i class="fa fa-angle-double-right"></i> <a href="<?php echo base_url().'merchant' ?>">Merchant</a></li>
                        <li><i class="fa fa-angle-double-right"></i> <a href="<?php echo base_url().'galeri' ?>">Galeri</a></li>
                        <li><i class="fa fa-angle-double-right"></i> <a href="<?php echo base_url().'kantor' ?>">Kantor Cabang</a></li>
                        <li><i class="fa fa-angle-double-right"></i> <a href="<?php echo base_url().'kontak' ?>">Kontak</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="widget">
                    <h5>Artikel Terbaru</h5>
                    <ul class="recent-post">
                        <?php 
                            if($top4artikel){
                                foreach($top4artikel as $row){
                                    $id = encrypt($row->artikel_id);
                                    $judul = strtolower(preg_replace('/\s+/', '-', $row->judul));
                                    $isi = word_cut($row->isi,120);
                        ?>
                                <li>
                                    <i class="fa fa-file-image-o post-data"></i>
                                    <a href="<?php echo base_url().'artikel/detail/'.$id.'/'.link_replace($judul) ?>"><?php echo $row->judul ?></a>
                                </li>
                        <?php   } 
                            }
                         ?>
                    </ul>
                </div>
            </div>

            <div class="col-md-3 col-sm-3">
                <div class="widget">
                    <h5>Kontak Kami</h5>
                    <ul class="top-link">
                        <li><a href="#" class="logdin"><i class="fa fa-phone"></i> 0852 888 55555</a></li>
                        <li><a href="#" class="logdin"><i class="fa fa-map-marker"></i> Jl. Hangtuah No 172 B - Pekanbaru</a></li>
                    </ul>
                    <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Friauwisatahati&tabs=timeline&width=340&height=70&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=false&appId" width="100%" height="70" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="subfooter">
        <p><?php echo date('Y') ?> &copy; Copyright <a href="<?php echo base_url() ?>">Riau Wisata Hati.</a> All rights Reserved.</p>
    </div>
</footer>