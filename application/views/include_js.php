    <script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/frontend/js/bootstrap.min.js"></script>
    
     <?php //Bootsnavs ?>  
    <script src="<?php echo base_url(); ?>assets/frontend/js/bootsnav.js"></script>
    
     <?php //Custom form ?>  
    <script src="<?php echo base_url(); ?>assets/frontend/js/form/jcf.js"></script>
    <script src="<?php echo base_url(); ?>assets/frontend/js/form/jcf.scrollable.js"></script>
    <script src="<?php echo base_url(); ?>assets/frontend/js/form/jcf.select.js"></script>
    
     <?php //Custom checkbox and radio ?>  
    <script src="<?php echo base_url(); ?>assets/frontend/js/checkator/fm.checkator.jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/frontend/js/checkator/setting.js"></script>
    
     <?php //LayerSlider script files  ?> 
    <script src="<?php echo base_url(); ?>assets/frontend/layerslider/js/greensock.js"></script>
    <script src="<?php echo base_url(); ?>assets/frontend/layerslider/js/layerslider.transitions.js"></script>
    <script src="<?php echo base_url(); ?>assets/frontend/layerslider/js/layerslider.kreaturamedia.jquery.js"></script>
    
     <?php //masonry ?>  
    <script src="<?php echo base_url(); ?>assets/frontend/js/masonry/masonry.min.js"></script>   
    <script src="<?php echo base_url(); ?>assets/frontend/js/masonry/masonry.filter.js"></script>
    <script src="<?php echo base_url(); ?>assets/frontend/js/masonry/setting.js"></script> 
    
     <?php //datetimepicker ?>  
    <script src="<?php echo base_url(); ?>assets/frontend/js/datetimepicker/jquery.datetimepicker.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/frontend/js/datetimepicker/setting.js"></script>
    
     <?php //owl carousel  ?>
    <script src="<?php echo base_url(); ?>assets/frontend/js/owlcarousel/owl.carousel.min.js"></script>  
    <script src="<?php echo base_url(); ?>assets/frontend/js/owlcarousel/setting.js"></script>
    
     <?php //price filter ?>  
    <script src="<?php echo base_url(); ?>assets/frontend/js/rangeSlider/ion.rangeSlider.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/frontend/js/rangeSlider/setting.js"></script>
    
     <?php //Parallax ?>  
    <script src="<?php echo base_url(); ?>assets/frontend/js/parallax/jquery.parallax-1.1.3.js"></script>
    <script src="<?php echo base_url(); ?>assets/frontend/js/parallax/setting.js"></script>
    

     <?php //Floating BUtton ?>  
    <script src="<?php echo base_url(); ?>assets/frontend/js/kc.fab.min.js"></script>

    <?php //custom input file ?>
    <script src="<?php echo base_url(); ?>assets/frontend/js/custominput/modernizr.js"></script>
    <script src="<?php echo base_url(); ?>assets/frontend/js/custominput/custom-file-input.js"></script>
    
     <?php //Custom ?>  
    <script src="<?php echo base_url(); ?>assets/frontend/js/custom.js"></script>
    
    <script type="text/javascript">
        jQuery("#layerslider").layerSlider({
            responsive: true,
            responsiveUnder: 1920,
            layersContainer: 1920,
            skin: 'fullwidth',
            navButtons:false,
            navStartStop:false,
            hoverPrevNext: false,
            skinsPath: '<?php echo base_url().'assets/frontend/layerslider/skins/' ?>'
        });

        $(document).ready(function(){
            var numb = [];
            $.ajax({
                method: 'POST',
                url: '<?php echo base_url() ?>kontak/getWhatsappReady/',
                dataType: 'json',
                success : function(resp){
                    console.log(resp);
                    if(resp.length>0){
                        numb.push({
                        "bgcolor":"#349b34",
                        "icon":"<i class='fa fa-whatsapp'>"
                        });

                        for(var i =0;i<resp.length;i++){
                            numb.push({
                                "url":"https://api.whatsapp.com/send?phone="+resp[i]['hp'],
                                "bgcolor":"#5bc43e ",
                                "color":"#fffff",
                                "icon":"<i class='fa fa-whatsapp'></i>",
                                "target":"_blank",
                                "title" : "Konsultan "+(i+1)
                            });
                        }
                        $('.kc_fab_wrapper').kc_fab(numb);
                    }
                }
            });
        })
    </script>