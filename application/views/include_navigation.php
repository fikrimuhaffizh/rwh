<script type="text/javascript">
    function search_func(){
        keyword = $('#keyword').val();
        window.open('https://www.google.co.id/#q=site:rwh.com+'+keyword,'_blank');
    }
</script>
<?php 

$hom = "";
$hjiumrh ="";
$mnsik = "";
$glri = "";
$kntk = "";
$stat = "";
$prog="";
$cbg="";
$prof="";
$konf="";
$mrchnt="";


if( $page_name == "paket" || $page_name=="status" || $page_name=="konfirmasi")
    $hjiumrh = 'active';

else if($page_title == "Profil")
    $prof = 'active';

else if($page_title == "Muthawif")
    $prof = 'active';


else if($page_name == "kantor")
    $cbg = 'active';

else if($page_name == "manasik")
    $mnsik = 'active';

else if($page_name == "merchant")
    $mrchnt = 'active';

else if($page_name == "galeri")
    $glri = 'active';

else if($page_name == "kontak")
    $kntk = 'active';

else if($page_name == "artikel")
    $hom = 'deactive';

else if($page_name == "beranda")
    $hom = 'active';

?>
   <style type="text/css">
       ul.top-share li a {
        color:white;
        border-color: white;
       }
       ul.top-link li a {
        color:white;
        border-color: white;
       }
       .top-wrapp{background:#F08519 }
       .btn-login{background: #f0a64d;color:white}
       .btn-logout{width:100%;}
   </style>
    <div class="top-wrapp">
        <div class="container">
            <div class="row">
                <div class="col-md-12 start">
                    <ul class="top-link pull-left">
                        
                        <li><a href="https://www.facebook.com/RiauWisataHati/"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="https://www.instagram.com/RiauWisataHati"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="javascript:;"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                    <!-- <ul class="top-share pull-right" >
                        <li><button class="btn btn-primary btn-xs" onclick="location.href = '<?php echo base_url().'konfirmasi' ?>';"><i class="fa fa-gear"></i>&nbsp; Konfirmasi Pembayaran</button></li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
       <nav class="navbar navbar-default navbar-sticky white bootsnav">
        <div class="top-search">
            <div class="container">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                    <input type="text" onclick="this.select()" onKeyDown="if(event.keyCode==13) search_func();" id="keyword" class="form-control" placeholder="Search">
                    <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
                </div>
            </div>
        </div>
        <div class="container">     
            
            <div class="attr-nav">
                    <ul>
                        <?php if($this->session->userdata('member_login')){ ?>   
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img class="img-circle" style="height:30px" src="<?php echo base_url().$this->session->userdata('picture') ?>" alt="Avatar">
                                </a>
                                <ul class="dropdown-menu cart-list fadeOutUp" style="display: none; opacity: 2;">
                                    <li>
                                        <a href="#" class="photo"><img src="<?php echo base_url().$this->session->userdata('picture') ?>" class="cart-thumb" alt="Avatar" /></a>
                                        <h6><a href="<?php echo base_url().'member' ?>">Dashboard</a></h6>
                                        <p><?php echo $this->session->userdata('nama') ?></p>
                                    </li>
                                    <li class="total">
                                        <a href="javascript:;" class="btn btn-xs btn-cart btn-logout" onClick="location.href='<?php echo base_url().'auth/logout/member' ?>'">Logout <i class="fa fa-sign-out"></i></a>
                                    </li>
                                </ul>
                            </li>
                        <?php } ?>
                        <li class="search"><a href="#"><i class="fa fa-search"></i></a></li>
                    </ul>
                </div>
        
  
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand padding-top10" href="<?php echo base_url() ?>"><img style="width:80px" src="<?php echo base_url(); ?>assets/frontend/img/brand/logo-black.png" class="log" alt="Riau Wisata Hati Logo"></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
                    <li class="<?php echo $hom ?>"><a href="<?php echo base_url();?>">Beranda</a></li>  
                    
                    <li class="dropdown <?php echo $prof ?>">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" >Profil</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url().'profil';?>">Perusahaan</a></li> 
                            <li><a href="<?php echo base_url().'muthawif';?>">Muthawif</a></li>
                        </ul>
                    </li>            
                    <li class="dropdown <?php echo $hjiumrh ?>">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" >Layanan</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url().'haji';?>">Haji</a></li>
                            <li><a href="<?php echo base_url().'umroh';?>">Umroh</a></li>
                            <li><a href="<?php echo base_url().'wisata';?>">Wisata Islam</a></li>
                            <li><a href="<?php echo base_url().'status';?>">Cek Keberangkatan</a></li>
                            <!-- <li><a href="<?php echo base_url().'konfirmasi';?>">Konfirmasi Pembayaran</a></li> -->
                        </ul>
                    </li>
                    <li class="<?php echo $mnsik ?>"><a href="<?php echo base_url().'manasik';?>">Manasik</a></li>
                    <li class="<?php echo $mrchnt ?>"><a href="<?php echo base_url().'merchant';?>">Merchant</a></li>
                    <li class="<?php echo $glri ?>"><a href="<?php echo base_url().'galeri';?>">Galeri</a></li>  
                    <li class="<?php echo $cbg ?>"><a href="<?php echo base_url().'kantor';?>">Kantor Cabang</a></li>
                    <li class="<?php echo $kntk ?>"><a href="<?php echo base_url().'kontak';?>">Kontak</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="top-wrapp hide">
        <div class="container">
            <div class="row">
                <div class="col-md-12 start">
                    <ul class="top-share pull-right" >
                    <?php 
                        if($this->session->userdata('member_login')){ ?>
                            <li><button class="btn btn-xs btn-primary" onclick="location.href='<?php echo base_url().'member' ?>'"> <i class="fa fa-user"></i>&nbsp; <span><?php echo $this->session->userdata('nama') ?></span></button></li>;
                            <li><button class="btn btn-primary btn-xs" onclick="location.href = '<?php echo base_url().'konfirmasi' ?>';"><i class="fa fa-gear"></i>&nbsp; Konfirmasi Pembayaran</button></li>
                    <?php    } else {
                    ?>
                        <li><button class="btn btn-primary btn-xs btn-login" onclick="location.href = '<?php echo base_url().'login' ?>';"><i class="fa fa-sign-in"></i> Login | Daftar </button></li>
                        <li><button class="btn btn-primary btn-xs" onclick="location.href = '<?php echo base_url().'konfirmasi' ?>';"><i class="fa fa-gear"></i>&nbsp; Konfirmasi Pembayaran</button></li>
                    <?php } ?>
                    </ul>
                    <ul class="top-link pull-left">
                        
                        <li><a href="https://www.facebook.com/RiauWisataHati"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="javascript:;"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="javascript:;"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

