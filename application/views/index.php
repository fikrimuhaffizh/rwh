<!DOCTYPE HTML>
<html class="no-js" lang="id">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="<?php echo base_url().'assets/frontend/img/brand/favicon.png' ?>">

    <?php
        if($page_name == 'beranda')  echo '<title> Riau Wisata Hati | Umroh mudah dan aman</title>';
        else echo '<title>'.$page_title.' | Riau Wisata Hati</title>';
        
        if($page_name!='halaman' && $page_name!='artikel' && $page_name!='paket_detail'){
            if($page_name == 'beranda'){
                $meta_desc = 'Riau Wisata Hati adalah salah satu brand di bawah bendera PT. Riau Wisata Hati, yang khusus melayani perencanaan perjalanan umroh';
            }
            else if($page_name == 'daftar'){
                $meta_desc ='Bergabunglah dengan Riau Wisata Hati dengan menjadi member Riau Wisata Hati';
            }
            else if($page_name == 'kantor'){
                $meta_desc ='Temukan informasi terkait Riau Wisata Hati yang tersebar di berbagai daerah';
            }
            else if($page_name == 'status'){
                $meta_desc ='Status Jemaah yang telah mendaftar di Riau Wisata Hati dapat dipantau secara berkala agar jemaah dapat mengetahui status terakhir keberangkatan mereka';
            }
            else if($page_name == 'paket'){

                if($page_title=='Paket Umroh'){
                    $meta_desc ='Temukan berbagai penawaran menarik dari paket umroh yang disediakan Riau Wisata Hati untuk memudahkan anda menuju baitullah';
                }
                else if($page_title=='Paket Haji Plus'){
                    $meta_desc ='Tunaikan kewajiban haji anda bersama Riau Wisata Hati';
                }
                else if($page_title=='Paket Wisata Islam'){
                    $meta_desc ='Kunjungi daerah yang memiliki sejarah peradaban islam demi meningkatkan keimanan bersama Riau Wisata Hati';
                }
            }
            else if($page_name == 'manasik'){
                $meta_desc ='Jadwal pelaksanaan manasik oleh Riau Wisata Hati yang dibimbing oleh Ustad terbaik';
            }
            else if($page_name == 'merchant'){
                $meta_desc ='Riau Wisata Hati memberikan diskon khusus berbelanja di merchant yang terdaftar kepada para member Riau Wisata Hati';
            }
            else if($page_name == 'galeri'){
                $meta_desc ='Galeri foto para jemaah yang melakukan berbagai kegiatan, perjalanan atau tour bersama Riau Wisata Hati';
            }
            else if($page_name == 'kontak'){
                $meta_desc ='Hubungi kami untuk informasi lebih lanjut terkait paket umroh, paket haji plus dan paket wisata islam Riau Wisata Hati';
            }
            else if($page_name == 'artikel'){
                $meta_desc = 'Riau Wisata Hati memberikan artikel menraik seputar islam, umroh, haji, dan informasi umum yang akan menambah wawasan pengunjung website riauwisatahati.com';
            }
            else if($page_name == 'konfirmasi'){
                $meta_desc = 'Konfirmasi Pembayaran akan memudahkan jemaah untuk melakukan tracking terhadap dana yang dibayarkan pada RWH sehingga tidak ada penipuan dalam transaksi dengan Riau Wisata Hati';
            }
            else if($page_name == '404'){
                $meta_desc = 'Halaman Riau Wisata Hati Tidak Ditemukan';
            }

            $meta_keyword = 'RWH, Riau Wisata Hati, Umroh, Umroh Service, Riau, Umroh Riau, Umroh aman';
            $meta_img = 'http://'.$_SERVER['SERVER_NAME'].'/assets/frontend/img/brand/logo-black.png';
            $meta_type = 'website';
        } 

        $meta_content = 'https://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
        $site_name = base_url();
        $site_img = base_url().'assets/frontend/img/brand/logo-black.png';
        $fb = 'https://www.facebook.com/RiauWisataHati/';
        $ig = 'https://www.instagram.com/RiauWisataHati';
        $gplus = '-';
        $twitter = '@riauwisatahati';
    ?>
    <meta name="author" content="Riau Wisata Hati">
    <meta name="description" content="<?php echo $meta_desc; ?>">
    <meta name="keywords" content="<?php echo $meta_keyword; ?>">
    <meta name="robots" content="index, follow">

    <link rel="publisher" href="<?php echo $gplus; ?>" />
    <link rel="canonical" href="<?php echo $site_name; ?>" />  
    <link rel="shortlink" href="<?php echo $site_name; ?>" />

    <meta property="og:site_name" content="<?php echo $site_name; ?>">
    <meta property="og:title" content="<?php echo $page_title ?>">
    <meta property="og:description" content="<?php echo $meta_desc; ?>">
    <meta property="og:type" content="<?php echo $meta_type; ?>">
    <meta property="og:image" content="<?php echo $meta_img; ?>">
    <meta property="og:url" content="<?php echo $meta_content?>">
    <meta property="fb:admins" content="76248932533" />

    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="<?php echo $twitter; ?>" />
    <meta name="twitter:creator" content="<?php echo $twitter; ?>">
    <meta name="twitter:title" content="<?php echo $page_title ?>" />
    <meta name="twitter:description" content="<?php echo $meta_desc; ?>" />
    <meta name="twitter:image" content="<?php echo $meta_img; ?>" />
        
    <script type='application/ld+json'>{"@context":"http://schema.org","@type":"WebSite","@id":"#website","url":"<?php echo $site_name ?>","name":"Riau Wisata Hati","alternateName":"Riau Wisata Hati"}}</script>
    <script type='application/ld+json'>{"@context":"http://schema.org","@type":"Organization","url":"<?php echo $site_name ?>","sameAs":["<?php echo $fb ?>","<?php echo $ig ?>","<?php echo $gplus ?>"],"@id":"#organization","name":"Riau Wisata Hati","logo":"<?php echo $site_img ?>"}</script>


    <?php include "include_css.php"; ?>
</head>

    <?php if($page_name=='login'){ ?>
        <?php include "page/".$page_name.".php"; ?>
        <?php include "include_js.php"; ?>
    <?php } else {?>
    <body>
        <div id="loading" class="loading-invisible">
            <div class="loading-center">
                <div class="loading-center-absolute">
                    <div class="object" id="object_four"></div>
                    <div class="object" id="object_three"></div>
                    <div class="object" id="object_two"></div>
                    <div class="object" id="object_one"></div>
                </div>
                <p>Riau Wisata Hati</p>
            </div>
        </div>
        
        <?php include "include_navigation.php"; ?>

        <div class="clearfix"></div>
        
        <?php include "page/".$page_name.".php"; ?>
        
        <div class="clearfix"></div>
        
        <?php include "include_footer.php"; ?>
        <?php include "include_js.php"; ?>
    </body>
    <?php } ?>
</html>
