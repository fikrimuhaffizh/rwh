
<div class="page-footer">
    <div class="page-footer-inner">Copyright &copy; Riau Wisata Hati <?php echo date('Y')?>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>