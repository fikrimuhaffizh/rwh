<?php $login_type = $this->session->userdata('login_type'); ?>
<script type="text/javascript">
    function setToggleSession(){
        url = '<?php echo base_url() ?>administrator/toggle';
        $.ajax({method: 'POST',url: url,dataType: 'json'});
    }
    function updateStatNotif(id,link){
        $.ajax({
            method: 'POST',
            data:'notifikasi_id='+id+'&link='+link,
            url: '<?php echo base_url() ?>administrator/notifikasi/baca',
            dataType: 'json',
            success : function(resp){
                if(resp == 'update_success'){
                    window.location = link;
                }
            }
        });
    }
    function updateKursHariIni(){
        $.ajax({
            method: 'POST',
            data: {kurs_dollar : $('input[name="kurs_dollar"]').val()},
            url: '<?php echo base_url() ?>administrator/kurs_dollar/update',
            dataType: 'json',
            success : function(resp){
                if(resp == 'update_success'){
                    location.reload();
                }
            }
        });
    }
</script>
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="<?php echo base_url() ?>rwhgate">
                <img src="<?php echo base_url() ?>assets/frontend/img/brand/logo-white.png" alt="logo" class="logo-default" style="margin-top:15px" /> </a>
            <div class="menu-toggler sidebar-toggler" onclick="setToggleSession()">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN PAGE TOP -->
        <div class="page-top">
            <!-- BEGIN HEADER SEARCH BOX -->
            <!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->
            <!-- END HEADER SEARCH BOX -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <li class="separator hide"> </li>
                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" style="padding:15px">
                            <?php 
                                $x = $this->md_kurs_dollar->getLatestKursDollar();
                                $kurs_hari_ini = $x ? number_format_decimal($x[0]->harga_rupiah) : '?'; 
                                $kurs_tgl = $x ? 'Last update : '.date('d-M-Y H:i',strtotime($x[0]->tgl_perubahan)) : 'Klik untuk update harga rupiah';
                            ?>
                            <span class="username username-hide-on-mobile">$1 = Rp <?php echo $kurs_hari_ini ?></span><br>
                            <span class="username username-hide-on-mobile font-white" style="font-size:10px"> <?php echo $kurs_tgl ?></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li style="padding:10px">
                                <small>Harga dollar saat ini :</small>
                                
                                    <input type="number" class="input-sm" style="width:63%" name="kurs_dollar" value="<?php echo $x ? $x[0]->harga_rupiah : ''; ?>">
                                    <button type="button" onclick="updateKursHariIni()" class="btn btn-success btn-sm pull-right">Simpan</button>
                               
                            </li>
                        </ul>
                    </li>
                    <li class="separator hide"> </li>
                    <li class="dropdown dropdown-extended dropdown-notification dropdown-dark" id="header_notification_bar">
                        <?php 
                            $cek = $this->md_notifikasi->getTop20NotifbyPengguna($this->session->userdata('pengguna_id'));
                            $belum_baca = $this->md_notifikasi->getNotificationByPenggunaByBelumBaca($this->session->userdata('pengguna_id'));
                        ?>
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <i class="icon-bell"></i>
                            <?php if(count($belum_baca)!=0){ ?>
                                <span class="badge badge-success"><?php echo count($belum_baca)?></span>
                            <?php } ?>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="external">
                                <h3>
                                    <span class="bold"><?php echo count($belum_baca) ?> pending</span> notifications</h3>
                                <a href="<?php echo base_url().'administrator/notifikasi' ?>">view all</a>
                            </li>
                            <li>
                                <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                <?php 
                                    foreach($cek as $row){ 
                                        $id = encrypt($row->notifikasi_id);
                                        $link = base_url().$row->link;
                                        //Style read/unread
                                            if($row->baca == 0)
                                                $style_read = 'label-success';
                                            else
                                                $style_read = '';
                                        //Style icon
                                            if($row->judul == 'Verifikasi Pembayaran')
                                                $style_icon = 'fa-dollar';
                                            else
                                                $style_icon = 'fa-bell';
                                ?>
                                    <li>
                                        <a onClick="updateStatNotif('<?php echo $id ?>','<?php echo $link ?>')">
                                            <span class="time"><?php time_check(strtotime($row->tgl)) ?></span>
                                            <span class="details">
                                                <span class="label label-sm label-icon <?php echo $style_read ?>">
                                                    <i class="fa <?php echo $style_icon ?>"></i>
                                                </span> <?php echo $row->judul ?></span>
                                        </a>
                                    </li>
                                <?php } ?>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="separator hide"> </li>
                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" style="padding:15px">
                            <span class="username username-hide-on-mobile"> 
                                <?php echo $this->session->userdata('nama'). ' - '.$this->session->userdata('kode_kota_pengguna') ?>
                                
                            </span><br>
                            <span class="username username-hide-on-mobile font-white" style="font-size: 10px"><?php echo $this->session->userdata('login_type') ?></span>
                            <img alt="" style="margin:-15px 0px 0px 25px" class="img-circle" src="<?php echo $this->session->userdata('picture') ?>" /> 
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="<?php echo base_url().'auth/logout' ?>"><i class="icon-logout"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END PAGE TOP -->
    </div>
    <!-- END HEADER INNER -->
</div>