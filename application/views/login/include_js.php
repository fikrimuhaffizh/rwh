    <!-- <script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script> -->
    <script src="<?php echo base_url()?>assets/backend/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>

    <!-- Editable table and datatable -->
    <script src="<?php echo base_url()?>assets/backend/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    <!-- Sweet alert -->
    <script src="<?php echo base_url()?>assets/backend/global/plugins/bootstrap-sweetalert/sweetalert.min.js" type="text/javascript"></script>
    <!-- Datetime Picker -->
    <script src="<?php echo base_url()?>assets/backend/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
    <!-- Bootstrap File Input -->
    <script src="<?php echo base_url()?>assets/backend/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
    <!-- Date Picker -->
    <script src="<?php echo base_url()?>assets/backend/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js" type="text/javascript"></script>
    <!-- Clipboard -->
    <script src="<?php echo base_url()?>assets/backend/pages/scripts/components-clipboard.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/global/plugins/clipboardjs/clipboard.min.js" type="text/javascript"></script>
    <!-- Form Repeater -->
    <script src="<?php echo base_url()?>assets/backend/pages/scripts/form-repeater.min.js" type="text/javascript"></script>
    <!-- Datatable column order -->
    <script src="<?php echo base_url()?>assets/backend/pages/scripts/table-datatables-colreorder.min.js" type="text/javascript"></script>
     <!-- Multi Select -->
    <script src="<?php echo base_url() ?>assets/backend/global/plugins/jquery-multi-select/js/jquery.multi-select.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/backend/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/backend/pages/scripts/components-multi-select.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/backend/pages/scripts/components-select2.min.js" type="text/javascript"></script>
    <!-- Highchart -->
    <script src="<?php echo base_url() ?>assets/backend/global/plugins/highcharts/js/highcharts.js" type="text/javascript"></script>

    <!-- tandaPemisahTitik,numbersonly,formatMoney -->
    <script src="<?php echo base_url()?>assets/backend/js/rwh.js" type="text/javascript"></script>

    
    <script src="<?php echo base_url()?>assets/backend/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/global/plugins/moment.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/global/plugins/morris/morris.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/pages/scripts/dashboard.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>           
    <script src="<?php echo base_url()?>assets/backend/global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/backend/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>

<script src="<?php echo base_url()?>assets/backend/pages/scripts/components-bootstrap-tagsinput.min.js" type="text/javascript"></script>
    <!-- END THEME LAYOUT SCRIPTS -->

    <script type="text/javascript">
    $(document).ready(function () {
        dateToday = new Date();
        $("#tgl_post").datepicker({ minDate: 0});
        $(".tgl_lahir").datepicker({format: 'dd/mm/yyyy'});
        $('#table-custom').DataTable({
            buttons:false
        });
        $('#table-custom2').DataTable({
            buttons:false
        });
    });
    </script>