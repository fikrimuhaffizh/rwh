<?php 
$dash        ="";
$setup       ="";
$bnk       ="";
$afil        ="";
$konten      ="";
$kls         ="";
$shbtsfwh    ="";
$jdmsk       ="";
$syrumrh     ="";
$pkt         ="";
$kntrcbg     ="";
$hlmn        ="";
$artkl       ="";
$slde        ="";
$glri        ="";
$mdia        ="";
$kntk        ="";
$mmbr        ="";
$pndftrn     ="";
$stjmaah     ="";
$pmbyrn      ="";
$pnggna      ="";
$mskpi       ="";
$kbrgktn     ="";
$htl         ="";
$jmah        ="";
$ntif        ="";
$loog        ="";
$mrchnt      ="";
$torld       ="";
$konf        ="";
$test        ="";
$lprn        ="";
$lprnbyr     ="";
$lprndftr    ="";
$lprnbrg     ="";
$lprnjmh     ="";
$room        ="";
$brng        ="";
$stck        ="";
$stck_transk ="";
$stck_hist   ="";
$prlngkpn    ="";
$kta         ="";
$comp        ="";
$lprntrn     ="";
$sms        = "";

if($page_name == 'dashboard'){
    $dash ="active";
}
if($page_name == 'pendaftaran'){
    $jmah = 'active'; 
    $pndftrn ="active";
}
else if($page_name == 'persyaratan'){
    $jmah = 'active'; 
    $stjmaah ="active";
}
else if($page_name == 'perlengkapan'){
    $jmah = 'active'; 
    $prlngkpn ="active";
}
else if($page_name == 'pembayaran'){
    $jmah = 'active'; 
    $pmbyrn ="active";
}
else if($page_name == 'pengguna'){
    $pnggna = 'active';
}
else if($page_name == 'member'){
    $mmbr = 'active';
}
else if($page_name == 'company'){
    $setup = 'active';
    $comp = 'active';
}
else if($page_name == 'kelas'){
    $setup = 'active';
    $kls = 'active';
}
else if($page_name == 'jadwal_manasik'){
    $setup = 'active';
    $jdmsk = 'active';
}
else if($page_name == 'syarat_umroh'){
    $setup = 'active';
    $syrumrh = 'active';
}
else if($page_name == 'paket'){
    $setup = 'active';
    $pkt = 'active';
}
else if($page_name == 'testimoni'){
    $setup = 'active';
    $test = 'active';
}
else if($page_name == 'kantor_cabang'){
    $setup = 'active';
    $kntrcbg = 'active';
}
else if($page_name == "halaman"){
    $konten = 'active';
    $hlmn = 'active';
}
else if($page_name == "artikel"){
    $konten = 'active';
    $artkl = 'active';
}
else if($page_name == "slideshow"){
    $konten = 'active';
    $slde = 'active';
}
else if($page_name == "galeri"){
    $konten = 'active';
    $glri = 'active';
}
else if($page_name == "media"){
    $mdia = 'active';
}
else if($page_name == "kontak"){
    $kntk = 'active';
}

else if($page_name == 'maskapai'){
    $setup = 'active';
    $mskpi ='active';
}
else if($page_name == 'hotel'){
    $setup = 'active';
    $htl ='active';
}
else if($page_name == 'merchant'){
    $setup = 'active';
    $mrchnt ='active';
}
else if($page_name == 'tour_leader'){
    $setup = 'active';
    $torld ='active';
}
else if($page_name == 'kota'){
    $setup = 'active';
    $kta ='active';
}
else if($page_name == 'barang'){
    $setup = 'active';
    $brng ='active';
}
else if($page_name == 'bank'){
    $setup = 'active';
    $bnk ='active';
}
else if($page_name == 'keberangkatan'){
    $jmah = 'active'; 
    $kbrgktn ="active";
}
else if($page_name == 'konfirmasi'){
    $jmah = 'active'; 
    $konf ="active";
}
else if($page_name == 'notifikasi'){
    $ntif ="active";
}
else if($page_name == 'log'){
    $loog="active";
}
else if($page_name == 'laporan_barang'){
    $lprn = 'active';
    $lprnbrg = 'active';
}
else if($page_name == 'laporan_transaksi'){
    $lprn = 'active';
    $lprntrn = 'active';
}
else if($page_name == 'laporan_jemaah'){
    $lprn = 'active';
    $lprnjmh = 'active';
}
else if($page_name == 'laporan_pembayaran'){
    $lprn = 'active';
    $lprnbyr = 'active';
}
else if($page_name == 'laporan_pendaftaran'){
    $lprn = 'active';
    $lprndftr = 'active';
}
else if($page_name == 'room'){
    $room ='active';
}
else if($page_name == 'sms'){
    $sms ='active';
}
else if($page_name == 'transaksi'){
    $stck = 'active'; 
    $stck_transk ="active";
}
else if($page_name == 'history_stock'){
    $stck = 'active'; 
    $stck_hist ="active";
}

/*//Hide menu yang tidak perlu ditampilkan. Kantor Pusat/Administrator muncul semua
if($this->session->userdata('login_type')=='Keuangan'){
    $pnggna = 'hide';
    $afil = 'hide';
    $konten = 'hide';
    $mdia = 'hide';
    $pdunibdh ="hide";
    $ppt ="hide";
    $kntk = 'hide';
    $fedbck = 'hide';
    $mmbr ="hide";
    $kbrgktn ="hide";
    $setup ="hide";
}
else if($this->session->userdata('login_type')=='Kantor Cabang'){
    $konten = 'hide';
    $mdia = 'hide';
    $pdunibdh ="hide";
    $ppt ="hide";
    $ppt ="hide";
    $pnggna = 'hide';

    //-->Tidak ada blast SMS

} else if($this->session->userdata('login_type')=='Perwakilan Daerah'){
    $pnggna = 'hide';
    $kntrcbg = 'hide';
    $konten = 'hide';
    $mdia = 'hide';
    $pdunibdh ="hide";
    $ppt ="hide";
    $kntk = 'hide';
    $fedbck = 'hide';
    $syrumrh = 'hide';
    $kls ="hide";
    $htl ="hide";
    $mskpi ="hide";
    $mmbr ="hide";
    $loog = "hide";
    $lprn = "hide";

} else if($this->session->userdata('login_type')=='Customer Service'){
    $pnggna = "hide";
    $kntk = "hide";
    $fedbck = "hide";
    $loog = "hide";
    $konten = "hide";
    $mdia = "hide";
    $kls = "hide";
    $htl = "hide";
    $mskpi="hide";
    $mrchnt="hide";
    $test="hide";
    $ppt="hide";
    $pdunibdh = "hide";

} else if($this->session->userdata('login_type')=='Executive'){
    $pnggna = "hide";
    $kntk = "hide";
    $fedbck = "hide";
    $loog = "hide";
    $konten = "hide";
    $mdia = "hide";
    $setup = "hide";
    $afil = 'hide';
    $mmbr = "hide";
    $jmah = "hide";
    $ntif = "hide";
}*/



$menu_pengguna                 = FALSE;
$menu_pengaturan               = FALSE;
    $menu_pengaturan_bank      = FALSE;
    $menu_pengaturan_barang    = FALSE;
    $menu_pengaturan_company   = FALSE;
    $menu_pengaturan_hotel     = FALSE;
    $menu_pengaturan_mansik    = FALSE;
    $menu_pengaturan_kantor    = FALSE;
    $menu_pengaturan_kelas     = FALSE;
    $menu_pengaturan_kota      = FALSE;
    $menu_pengaturan_maskapai  = FALSE;
    $menu_pengaturan_merchant  = FALSE;
    $menu_pengaturan_paket     = FALSE;
    $menu_pengaturan_syarat    = FALSE;
    $menu_pengaturan_testimoni = FALSE;
    $menu_pengaturan_tourlead  = FALSE;
$menu_konten                   = FALSE;
    $menu_konten_halaman       = FALSE;
    $menu_konten_artikel       = FALSE;
    $menu_konten_slideshow     = FALSE;
    $menu_konten_galeri        = FALSE;
$menu_media                    = FALSE;
$menu_jemaah                   = FALSE;
    $menu_jemaah_daftar        = FALSE;
    $menu_jemaah_perlengkapan  = FALSE;
    $menu_jemaah_syarat        = FALSE;
    $menu_jemaah_bayar         = FALSE;
    $menu_jemaah_keberangkatan = FALSE;
    $menu_jemaah_konfirmasi    = FALSE;
$menu_room                     = FALSE;
$menu_stock                    = FALSE;
$menu_sms                      = FALSE;
$menu_member                   = FALSE;
$menu_laporan                  = FALSE;
    $menu_laporan_barang       = FALSE;
    $menu_laporan_jemaah       = FALSE;
    $menu_laporan_pendaftaran  = FALSE;
    $menu_laporan_pembayaran   = FALSE;
    $menu_laporan_transaksi    = FALSE;
$menu_notifikasi               = FALSE;
$menu_log                      = FALSE;
$menu_kontak                   = FALSE;

$login_type = $this->session->userdata('login_type');
switch ($login_type) {
    case 'Administrator':
        $menu_pengguna                 = TRUE;
        $menu_pengaturan               = TRUE;
            $menu_pengaturan_bank      = TRUE;
            $menu_pengaturan_barang    = TRUE;
            $menu_pengaturan_company   = TRUE;
            $menu_pengaturan_hotel     = TRUE;
            $menu_pengaturan_mansik    = TRUE;
            $menu_pengaturan_kantor    = TRUE;
            $menu_pengaturan_kelas     = TRUE;
            $menu_pengaturan_kota      = TRUE;
            $menu_pengaturan_maskapai  = TRUE;
            $menu_pengaturan_merchant  = TRUE;
            $menu_pengaturan_paket     = TRUE;
            $menu_pengaturan_syarat    = TRUE;
            $menu_pengaturan_testimoni = TRUE;
            $menu_pengaturan_tourlead  = TRUE;
        $menu_konten                   = TRUE;
            $menu_konten_halaman       = TRUE;
            $menu_konten_artikel       = TRUE;
            $menu_konten_slideshow     = TRUE;
            $menu_konten_galeri        = TRUE;
        $menu_media                    = TRUE;
        $menu_jemaah                   = TRUE;
            $menu_jemaah_daftar        = TRUE;
            $menu_jemaah_perlengkapan  = TRUE;
            $menu_jemaah_syarat        = TRUE;
            $menu_jemaah_bayar         = TRUE;
            $menu_jemaah_keberangkatan = TRUE;
            $menu_jemaah_konfirmasi    = TRUE;
        $menu_room                     = TRUE;
        $menu_stock                    = TRUE;
        $menu_sms                      = TRUE;
        $menu_member                   = TRUE;
        $menu_laporan                  = TRUE;
            $menu_laporan_barang       = TRUE;
            $menu_laporan_jemaah       = TRUE;
            $menu_laporan_pendaftaran  = TRUE;
            $menu_laporan_pembayaran   = TRUE;
            $menu_laporan_transaksi    = TRUE;
        $menu_notifikasi               = TRUE;
        $menu_log                      = TRUE;
        $menu_kontak                   = TRUE;
        break;

    case 'Keuangan':
        $menu_jemaah                   = TRUE;
            $menu_jemaah_daftar        = TRUE;
            $menu_jemaah_perlengkapan  = TRUE;
            $menu_jemaah_syarat        = TRUE;
            $menu_jemaah_bayar         = TRUE;
            $menu_jemaah_keberangkatan = TRUE;
            $menu_jemaah_konfirmasi    = TRUE;
        $menu_room                     = TRUE;
        $menu_stock                    = TRUE;
        $menu_sms                      = TRUE;
        $menu_laporan                  = TRUE;
            $menu_laporan_jemaah       = TRUE;
            $menu_laporan_pendaftaran  = TRUE;
            $menu_laporan_barang       = TRUE;
            $menu_laporan_transaksi    = TRUE;
        $menu_notifikasi               = TRUE;
        $menu_log                      = TRUE;

        break;

    case 'Customer Service':
        $menu_jemaah                   = TRUE;
            $menu_jemaah_daftar        = TRUE;
            $menu_jemaah_syarat        = TRUE;
            $menu_jemaah_bayar         = TRUE;
            $menu_jemaah_konfirmasi    = TRUE;
        $menu_room                     = TRUE;
        $menu_sms                      = TRUE;
        $menu_laporan                  = TRUE;
            $menu_laporan_jemaah       = TRUE;
            $menu_laporan_pendaftaran  = TRUE;
        $menu_notifikasi               = TRUE;
        $menu_log                      = TRUE;
        break;

    case 'Logistik':
        $menu_stock                    = TRUE;
        $menu_jemaah                   = TRUE;
            $menu_jemaah_perlengkapan  = TRUE;
        $menu_laporan                  = TRUE;
            $menu_laporan_barang       = TRUE;
            $menu_laporan_transaksi    = TRUE;
        $menu_notifikasi               = TRUE;
        $menu_log                      = TRUE;
        break;

    case 'CS & Logistik':
        $menu_stock                    = TRUE;
        $menu_jemaah                   = TRUE;
            $menu_jemaah_daftar        = TRUE;
            $menu_jemaah_syarat        = TRUE;
            $menu_jemaah_bayar         = TRUE;
            $menu_jemaah_konfirmasi    = TRUE;
            $menu_jemaah_perlengkapan  = TRUE;
        $menu_room                     = TRUE;
        $menu_sms                      = TRUE;
        $menu_laporan                  = TRUE;
            $menu_laporan_jemaah       = TRUE;
            $menu_laporan_pendaftaran  = TRUE;
            $menu_laporan_barang       = TRUE;
            $menu_laporan_transaksi    = TRUE;
        $menu_notifikasi               = TRUE;
        $menu_log                      = TRUE;
        break;

    case 'Eksekutif':
        $menu_pengguna                 = TRUE;
        $menu_pengaturan               = TRUE;
            $menu_pengaturan_paket     = TRUE;
        $menu_jemaah                   = TRUE;
            $menu_jemaah_daftar        = TRUE;
            $menu_jemaah_keberangkatan = TRUE;
        $menu_stock                    = TRUE;
        $menu_sms                      = TRUE;
        $menu_laporan                  = TRUE;
            $menu_laporan_barang       = TRUE;
            $menu_laporan_jemaah       = TRUE;
            $menu_laporan_pendaftaran  = TRUE;
            $menu_laporan_pembayaran   = TRUE;
            $menu_laporan_transaksi    = TRUE;
        $menu_log                      = TRUE;

        break;
}

?>

<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
<!-- BEGIN SIDEBAR -->
<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
<div class="page-sidebar navbar-collapse collapse">
    <!-- BEGIN SIDEBAR MENU -->
    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->

    <?php $close_sidebar_menu = $this->session->userdata('menu_close_sidebar'); ?>
    <ul class="page-sidebar-menu  <?php echo $close_sidebar_menu ?>" data-keep-expanded="false" data-slide-speed="200">
        <li class="nav-item start <?php echo $dash ?>">
            <a href="<?php echo base_url().'administrator/dashboard' ?>" class="nav-link nav-toggle">
                <i class="icon-home"></i>
                <span class="title">Dashboard</span>
            </a>
        </li>
        <li class="heading <?php echo $setup ?>">
            <h3 class="uppercase">Features</h3>
        </li>
        <?php if($menu_pengguna){ ?>
            <li class="nav-item <?php echo $pnggna ?> ">
                <a href="<?php echo base_url().'administrator/pengguna' ?>" class="nav-link nav-toggle">
                    <i class="icon-user"></i>
                    <span class="title">Pengguna</span>
                </a>
            </li>
        <?php }  if($menu_pengaturan){ ?>
            <li class="nav-item <?php echo $setup ?> ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-settings"></i>
                    <span class="title">Pengaturan</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu ">
                    <?php if($menu_pengaturan_bank){ ?>
                        <li class="nav-item <?php echo $bnk ?>"><a href="<?php echo base_url().'administrator/bank' ?>" class="nav-link "><span class="title">Bank</span></a></li>    
                    <?php } if($menu_pengaturan_barang){ ?>
                        <li class="nav-item <?php echo $brng ?>"><a href="<?php echo base_url().'administrator/barang' ?>" class="nav-link "><span class="title">Barang</span></a></li>      
                    <?php } if ($menu_pengaturan_company) { ?>
                        <li class="nav-item <?php echo $comp ?>"><a href="<?php echo base_url().'administrator/company' ?>" class="nav-link "><span class="title">Company</span></a></li> 
                    <?php } if ($menu_pengaturan_hotel) { ?>           
                        <li class="nav-item <?php echo $htl ?>"><a href="<?php echo base_url().'administrator/hotel' ?>" class="nav-link "><span class="title">Hotel</span></a></li>                
                    <?php } if ($menu_pengaturan_mansik) { ?>
                        <li class="nav-item <?php echo $jdmsk ?>"><a href="<?php echo base_url().'administrator/jadwal_manasik' ?>" class="nav-link "><span class="title">Jadwal Manasik</span></a></li>
                    <?php } if ($menu_pengaturan_kantor) { ?>
                        <li class="nav-item <?php echo $kntrcbg ?>"><a href="<?php echo base_url().'administrator/kantor_cabang' ?>" class="nav-link "><span class="title">Kantor Cabang</span></a></li>
                    <?php } if ($menu_pengaturan_kelas) { ?>
                        <li class="nav-item <?php echo $kls ?>"><a href="<?php echo base_url().'administrator/kelas' ?>" class="nav-link "><span class="title">Kelas</span></a></li>
                    <?php } if ($menu_pengaturan_kota) { ?>
                        <li class="nav-item <?php echo $kta ?>"><a href="<?php echo base_url().'administrator/kota' ?>" class="nav-link "><span class="title">Kota Berangkat</span></a></li>
                    <?php } if ($menu_pengaturan_maskapai) { ?>
                        <li class="nav-item <?php echo $mskpi ?>"><a href="<?php echo base_url().'administrator/maskapai' ?>" class="nav-link "><span class="title">Maskapai</span></a></li>
                    <?php } if ($menu_pengaturan_merchant) { ?>
                        <li class="nav-item <?php echo $mrchnt ?>"><a href="<?php echo base_url().'administrator/merchant' ?>" class="nav-link "><span class="title">Merchant</span></a></li>                
                    <?php } if ($menu_pengaturan_paket) { ?>
                        <li class="nav-item <?php echo $pkt ?>"><a href="<?php echo base_url().'administrator/paket' ?>" class="nav-link "><span class="title">Paket</span></a></li>
                    <?php } if ($menu_pengaturan_syarat) { ?>
                        <li class="nav-item <?php echo $syrumrh ?>"><a href="<?php echo base_url().'administrator/syarat_umroh' ?>" class="nav-link "><span class="title">Syarat Umroh</span></a></li>
                    <?php } if ($menu_pengaturan_testimoni) { ?>
                        <li class="nav-item <?php echo $test ?>"><a href="<?php echo base_url().'administrator/testimoni' ?>" class="nav-link "><span class="title">Testimoni</span></a></li>
                    <?php } if ($menu_pengaturan_tourlead) { ?>
                        <li class="nav-item <?php echo $torld ?>"><a href="<?php echo base_url().'administrator/tour_leader' ?>" class="nav-link "><span class="title">Tour Leader</span></a></li>                 
                    <?php } ?>
                </ul>
            </li>    
        <?php }  if($menu_konten){ ?>   
            <li class="nav-item <?php echo $konten ?> ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-bulb"></i>
                    <span class="title">Konten Web</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <?php if ($menu_konten_halaman) { ?>
                        <li class="nav-item <?php echo $hlmn ?> "><a href="<?php echo base_url().'administrator/halaman' ?>" class="nav-link "><span class="title">Halaman</span></a></li>
                    <?php } if ($menu_konten_artikel) { ?>
                        <li class="nav-item <?php echo $artkl ?> "><a href="<?php echo base_url().'administrator/artikel' ?>" class="nav-link "><span class="title">Artikel</span></a></li>
                    <?php } if ($menu_konten_slideshow) { ?>
                        <li class="nav-item <?php echo $slde ?> "><a href="<?php echo base_url().'administrator/slideshow' ?>" class="nav-link "><span class="title">Slideshow</span></a></li>
                    <?php } if ($menu_konten_galeri) { ?>
                        <li class="nav-item <?php echo $glri ?> "><a href="<?php echo base_url().'administrator/galeri' ?>" class="nav-link "><span class="title">Galeri</span></a></li>
                    <?php } ?>
                </ul>
            </li>
        <?php }  if($menu_media){ ?>
            <li class="nav-item <?php echo $mdia ?> ">
                <a href="<?php echo base_url().'administrator/media' ?>" class="nav-link nav-toggle">
                    <i class="icon-diamond"></i>
                    <span class="title">Media</span>
                </a>
            </li>
            <li class="heading">
                <h3 class="uppercase">Data</h3>
            </li>
        <?php }  if($menu_jemaah){ ?>
            <li class="nav-item <?php echo $jmah ?> ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class=" icon-direction"></i>
                    <span class="title">Jemaah Management</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu ">
                    <?php if ($menu_jemaah_daftar){ ?>
                        <li class="nav-item <?php echo $pndftrn ?>"><a href="<?php echo base_url().'administrator/pendaftaran' ?>" class="nav-link nav-toggle"><span class="title">Jemaah</span></a></li>
                    <?php } if ($menu_jemaah_perlengkapan){ ?>
                        <li class="nav-item <?php echo $prlngkpn?>"><a href="<?php echo base_url().'administrator/perlengkapan' ?>" class="nav-link nav-toggle"><span class="title">Perlengkapan Jemaah</span></a></li>
                    <?php } if ($menu_jemaah_syarat){ ?>
                        <li class="nav-item <?php echo $stjmaah?>"><a href="<?php echo base_url().'administrator/persyaratan' ?>" class="nav-link nav-toggle"><span class="title">Persyaratan Jemaah</span></a></li>
                    <?php } if ($menu_jemaah_bayar){ ?>
                        <li class="nav-item <?php echo $pmbyrn ?>"><a href="<?php echo base_url().'administrator/pembayaran' ?>" class="nav-link nav-toggle"><span class="title">Pembayaran Jemaah</span></a></li>
                    <?php } if ($menu_jemaah_keberangkatan){ ?>
                        <li class="nav-item <?php echo $kbrgktn ?>"><a href="<?php echo base_url().'administrator/keberangkatan' ?>" class="nav-link nav-toggle"><span class="title">Keberangkatan Jemaah</span></a></li>
                    <?php } if ($menu_jemaah_konfirmasi){ ?>
                        <li class="nav-item <?php echo $konf ?>"><a href="<?php echo base_url().'administrator/konfirmasi' ?>" class="nav-link nav-toggle"><span class="title">Konfirmasi Pembayaran</span></a></li>                
                    <?php } ?>
                </ul>
            </li>
        <?php }  if($menu_room){ ?>
            <li class="nav-item <?php echo $room ?> ">
                <a href="<?php echo base_url().'administrator/room' ?>" class="nav-link nav-toggle">
                    <i class="icon-grid"></i>
                    <span class="title">Room Management</span>
                </a>
            </li>
        <?php }  if($menu_stock){ ?>
            <li class="nav-item <?php echo $stck_transk ?> ">
                <a href="<?php echo base_url().'administrator/transaksi' ?>" class="nav-link nav-toggle">
                    <i class="icon-briefcase"></i>
                    <span class="title">Stock Management</span>
                </a>
            </li>
        <?php }  if($menu_sms){ ?>
            <li class="nav-item <?php echo $stck_transk ?> ">
                <a href="<?php echo base_url().'administrator/sms' ?>" class="nav-link nav-toggle">
                    <i class="icon-envelope"></i>
                    <span class="title">SMS Blast</span>
                </a>
            </li>
        <?php }  if($menu_member){ ?>
            <li class="nav-item <?php echo $mmbr ?> ">
                <a href="<?php echo base_url().'administrator/member' ?>" class="nav-link nav-toggle">
                    <i class="icon-users"></i>
                    <span class="title">Pendaftar RWH</span>
                </a>
            </li>
        <?php }  if($menu_laporan){ ?>
            <li class="nav-item <?php echo $lprn ?> ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-printer"></i>
                    <span class="title">Laporan</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <?php if ($menu_laporan_jemaah){ ?>
                        <li class="nav-item <?php echo $lprnjmh ?> "><a href="<?php echo base_url().'administrator/laporan_jemaah' ?>" class="nav-link "><span class="title">Jemaah</span></a></li>
                    <?php } if ($menu_laporan_pendaftaran){ ?>
                        <li class="nav-item <?php echo $lprndftr ?> "><a href="<?php echo base_url().'administrator/laporan_pendaftaran' ?>" class="nav-link "><span class="title">Pendaftaran</span></a></li>
                    <?php } if ($menu_laporan_pembayaran){ ?>
                        <li class="nav-item <?php echo $lprnbyr ?> "><a href="<?php echo base_url().'administrator/laporan_pembayaran' ?>" class="nav-link "><span class="title">Pembayaran</span></a></li>
                    <?php } if ($menu_laporan_barang){ ?>
                        <li class="nav-item <?php echo $lprnbrg ?> "><a href="<?php echo base_url().'administrator/laporan_barang' ?>" class="nav-link "><span class="title">Stok Barang</span></a></li>
                    <?php } if ($menu_laporan_transaksi){ ?>
                        <li class="nav-item <?php echo $lprntrn ?> "><a href="<?php echo base_url().'administrator/laporan_transaksi' ?>" class="nav-link "><span class="title">Transaksi Barang</span></a></li>
                    <?php } ?>
                </ul>
            </li>
        <?php }  if($menu_notifikasi){ ?>
            <li class="nav-item <?php echo $ntif ?> ">
                <a href="<?php echo base_url().'administrator/notifikasi' ?>" class="nav-link nav-toggle">
                    <i class=" icon-bell"></i>
                    <span class="title">Notifikasi</span>
                </a>
            </li>
        <?php }  if($menu_log){ ?>
            <li class="nav-item <?php echo $loog ?> ">
                <a href="<?php echo base_url().'administrator/log' ?>" class="nav-link nav-toggle">
                    <i class="  icon-book-open"></i>
                    <span class="title">Log System</span>
                </a>
            </li>
        <?php }  if($menu_kontak){ ?>
            <li class="nav-item <?php echo $kntk ?> ">
                <a href="<?php echo base_url().'administrator/kontak' ?>" class="nav-link nav-toggle">
                    <i class="icon-call-end"></i>
                    <span class="title">Kontak</span>
                </a>
            </li>
        <?php } ?>
    </ul>
    <!-- END SIDEBAR MENU -->
</div>
<!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->