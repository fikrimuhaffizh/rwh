 
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title><?php echo $page_title.' | Riau Wisata Hati' ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Riau Wisata Hati Administrator page" name="description" />
        <meta content="" name="RWH" />
        <?php include "include_css.php" ?>
        <link rel="shortcut icon" href="<?php echo base_url().'assets/frontend/img/brand/favicon.png' ?>" /> 
    </head>
    <?php 
        if($page_name != 'login') {
            // page-sidebar-menu-closed page-sidebar-closed
            $close_sidebar = $this->session->userdata('body_close_sidebar');
    ?>
        <body class="page-header-fixed page-content-white <?php echo $close_sidebar ?>">
            <div class="page-wrapper">
                <?php include "include_header.php"; ?>
                <!-- BEGIN HEADER & CONTENT DIVIDER -->
                <div class="clearfix"> </div>
                <!-- END HEADER & CONTENT DIVIDER -->
                <!-- BEGIN CONTAINER -->
                <div class="page-container">
                <?php include "include_navigation.php" ?>
                <!-- BEGIN CONTENT -->
                <?php  include 'page/'. $page_name . '.php'; ?>
                <!-- END CONTENT -->
                </div>
                <!-- END CONTAINER -->
                <!-- BEGIN FOOTER -->
                <?php include "include_footer.php" ?>
                <?php include "include_js.php" ?>
                <!-- END FOOTER -->
            </div>   
        </body>
    <?php } else {?>
        <body class=" login">
            <?php  include 'page/'. $page_name . '.php'; ?>
            <?php include "include_js.php" ?>
        </body>
    <?php } ?>
</html>