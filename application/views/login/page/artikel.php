<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/backend/global/plugins/ckeditor-full/ckeditor.js"></script>
<script type="text/javascript">

    function cek_all(){
        empty=0;
        $('input', '#add-edit-form').each(function(){
            if($(this).val() == ""&& $(this).attr('id')) {
                // alert($(this).attr('id'));
                empty++;
            }
        })
        $('select', '#add-edit-form').each(function(){
            if($(this).val() == ""){
                 // alert($(this).attr('id'));
                empty++;
            }
        })
        var x = CKEDITOR.instances.isi.getData();
        if(!x){
             // alert('isi');
            empty++;
        }

        if(empty>0){
            alert("Data Artikel Belum Lengkap !");
            exit();
        }
    }
    function add_function(task){
        cek_all();
        CKEDITOR.instances.isi.updateElement();  
        var data = new FormData();    
        var file = document.getElementById('thumb').files[0];
        if (file) {
            var file_data = $('#thumb').prop('files')[0];  
        } else {
            alert('Silahkan pilih Thumbnail Artikel');
            return;
        }
            data.append('thumb', file_data); 
            data.append('judul',$('#judul').val());
            data.append('isi',$('#isi').val());
            data.append('post_status',$('#post_status').val());
            data.append('tgl_post',$('#tgl_post').val());
            data.append('meta_desc',$('#meta_desc').val());
            data.append('meta_key',$('#meta_key').val());

            swal({
                  title: "Memproses Data Artikel",
                  text: "Klik 'Create' untuk memulai.",
                  type: "info",
                  closeOnConfirm: false,
                  confirmButtonText: 'Create',
                  showLoaderOnConfirm: true,
                },
                function(){
                    $.ajax({
                        method: 'POST',
                        data:data,
                        url: '<?php echo base_url() ?>administrator/artikel/add',
                        dataType: 'text',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success : function(resp){
                            if(resp == '"add_success"'){
                                swal({
                                      title: "Tambah Artikel  ",
                                      text: "Penambahan berhasil",
                                      type: "success",
                                      confirmButtonClass: "btn-danger",
                                      confirmButtonText: "Ok",
                                    },
                                    function(){
                                        window.location.href='<?php echo base_url().'administrator/artikel/'?>';
                                    });
                            }
                            if(resp == '"add_fail"'){
                                swal({
                                      title: "Penambahan Gagal !",
                                      text: "File Thumbnail Tidak Sesuai atau Ukuran Melebihi 2 MB",
                                      type: "warning",
                                      confirmButtonClass: "btn-danger",
                                      confirmButtonText: "Ok",
                                    });
                            }
                        }
                    });
                });
    }

    function edit_function(task='',id=''){
        if(task == 'show'){
            window.location.href='<?php echo base_url().'administrator/artikel/show_edit_form/' ?>'+id;
        }

        if(task == 'update'){
            CKEDITOR.instances.isi.updateElement();  
            var data = new FormData();    
            

            var file = document.getElementById('thumb').files[0];
            if (file) {
                var file_data = $('#thumb').prop('files')[0];  
            }
                data.append('thumb', file_data); 
                data.append('judul',$('#judul').val());
                data.append('author',$('#author').val());
                data.append('isi',$('#isi').val());
                data.append('post_status',$('#post_status').val());
                data.append('tgl_post',$('#tgl_post').val());
                data.append('meta_desc',$('#meta_desc').val());
                data.append('meta_key',$('#meta_key').val());

            $.ajax({
                method: 'POST',
                data:data,
                url: '<?php echo base_url() ?>administrator/artikel/update/'+id,
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                success : function(resp){
                        swal({
                              title: "Update Artikel",
                              text: "Pembaharuan berhasil",
                              type: "success",
                              confirmButtonClass: "yellow-crusta",
                              confirmButtonText: "Ok",
                            },
                            function(){
                                window.location.href='<?php echo base_url().'administrator/artikel' ?>';
                            });
                }
            });
        }

        if(task == 'delete'){
            swal({
              title: "Hapus Data?",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "grey",
              confirmButtonText: "Yes, delete it!",
              closeOnConfirm: false
            },
            function(){
                $.ajax({
                    method: 'POST',
                    url: '<?php echo base_url() ?>administrator/artikel/delete/'+id,
                    dataType: 'json',
                    success : function(resp){
                        swal({
                              title: "Hapus Data",
                              text: "Penghapusan berhasil",
                              type: "success",
                              confirmButtonClass: "grey",
                              confirmButtonText: "Ok",
                            },
                            function(){
                                location.reload();
                            });
                    }
                });
            });
        }
    }

    $(document).ready(function(){

    });
</script>
<?php 
$judul = "";
$isi ="";
$tgl_post = date('Y-m-d');
$author ="";
$post_status = "Publish";
$meta_desc = "";
$meta_key = "";
 $img_link =base_url().'assets/frontend/img/noimagefound.jpg';
$btn_update = FALSE;
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Artikel
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo base_url().'administrator' ?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Artikel</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <?php if($show_form){ 
                    if($fill_form){
                        $id = encrypt($dt_artikel[0]->artikel_id);
                        $judul = $dt_artikel[0]->judul;
                        $isi = $dt_artikel[0]->isi;
                        $tgl_post = $dt_artikel[0]->tgl_post;
                        $author = $dt_artikel[0]->author;
                        $post_status = $dt_artikel[0]->post_status;
                        $meta_desc = $dt_artikel[0]->meta_desc;
                        $meta_key = $dt_artikel[0]->meta_keyword;
                        $temp = $this->md_media_detail->getMediaDetailByMediaId ($dt_artikel[0]->media_id);
                        if($temp){
                            $img_link = base_url().$temp[0]->media_link;
                        }
                        $btn_update = TRUE;
                    }
            ?>
                <div class="col-md-12">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <button onClick="window.history.back();" class="btn grey pull-left" style="margin-right: 10px"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;Kembali</button>
                            <?php if($fill_form) {?>
                                <div class="caption">
                                    <i class="icon-equalizer font-yellow-crusta"></i>
                                    <span class="caption-subject font-yellow-crusta bold uppercase">Edit  Artikel</span>
                                </div>
                            <?php } else { ?>
                                <div class="caption">
                                    <i class="icon-equalizer font-red"></i>
                                    <span class="caption-subject font-red bold uppercase">Tambah  Artikel</span>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="portlet-body form">
                            <div class="col-md-12">
                            </div>
                            <!-- BEGIN FORM-->
                            <?php echo form_open('administrator', array('id'=>'add-edit-form','class'=>'form-horizontal')); ?>
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Judul</label>
                                        <div class="col-md-4">
                                            <div class="input-icon right">
                                                <i class="fa fa-bookmark-o"></i>
                                                <input type="text" class="form-control" id="judul" placeholder="Enter text" value="<?php echo $judul ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Isi</label>
                                        <div class="col-md-10">
                                            <textarea class="ckeditor" name="isi" id="isi"><?php echo $isi ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Thumbnail</label>
                                        <div class="col-md-7">
                                            <div class="col-md-5">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <label class="label label-danger"><i class="fa fa-info-circle"></i> Optimal Image Resolution: 650x435 pixel</label>
                                                    <div class="fileinput-new thumbnail" >
                                                        <img src="<?php echo $img_link ?>" alt=""> </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; line-height: 10px;"></div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"> Select image </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="thumb" id="thumb"> </span>
                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Tanggal Posting</label>
                                        <div class="col-md-4">
                                            <div class="form-md-line-input form-md-floating-label input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
                                                <input type="text" class="form-control" name="tgl_post" id="tgl_post" value="<?php echo date('d-m-Y',strtotime($tgl_post)) ?>" readonly="" placeholder="dd-mm-yyyy">
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                            </div>                                                                        
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Post Status</label>
                                        <div class="col-md-4">
                                            <select class="form-control input-small" id="post_status" >
                                                <option value="Publish" <?php if($post_status == "Publish"){echo 'Selected';} ?> >Publish</option>
                                                <option value="Draft" <?php if($post_status == "Draft"){echo 'Selected';} ?> >Draft</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Meta Keyword</label>
                                        <div class="col-md-4">
                                            <div class="input-icon right">
                                                <i class="fa fa-key"></i>
                                                <input type="text" id="meta_key" class="form-control" value="<?php echo $meta_key ?>" placeholder="Enter text">
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Meta Description</label>
                                        <div class="col-md-4">
                                            <div class="input-icon right">
                                                <i class="fa fa-list"></i>
                                                <textarea id="meta_desc" rows="4" maxlength="150" class="form-control"><?php echo $meta_desc ?></textarea>
                                            </div>
                                            <span class="help-block"> <i>It's help your website SEO</i>. <i class="font-red">Max 150 Character !</i></span>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-2 col-md-4">
                                            <?php if($btn_update){ ?>
                                                <button type="button" onClick="edit_function('update',<?php echo $id ?>);" class="btn yellow-crusta">Update</button>
                                            <?php } else { ?>
                                                <button type="button" onClick="add_function();" class="btn green"><i class="fa fa-save"></i> Simpan Artikel</button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            <?php echo form_close();?>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <div class="col-md-12">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <button id="sample_editable_1_new" onClick="window.location.href='<?php echo base_url().'administrator/artikel/show_add_form' ?>'" class="btn btn-sm red">Tambah Artikel
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold uppercase"> Managed Artikel</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered  order-column" id="table-custom">
                                <thead>
                                    <tr>
                                        <th> NO </th>
                                        <th> Judul </th>
                                        <th> Post Status </th>
                                        <th> Tanggal Posting </th>
                                        <th> Aksi </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php if($artikel > 0){ 
                                        $count=0;
                                        foreach($artikel as $row){
                                            $id = encrypt($row->artikel_id);
                                            $judul = $row->judul;
                                            $post_status = $row->post_status;
                                            $tgl_post = date('d-F-Y',strtotime($row->tgl_post));
                                            if($post_status=='Publish')
                                                $post_status = '<button type="button" class="btn btn-xs btn-info"><i class="fa fa-bookmark"></i>&nbsp;Publish</button>';
                                            else 
                                                $post_status = '<button type="button" class="btn btn-xs green"><i class="fa fa-file-o"></i>&nbsp;Draft</button>';
                                        $count++;
                                ?>
                                    <tr>
                                        <td> <?php echo $count ?></td>
                                        <td> <?php echo $judul ?> </td>
                                        <td> <?php echo $post_status ?> </td>
                                        <td>  <i class="fa fa-calendar"></i> &nbsp;<?php echo $tgl_post ?> </td>
                                        <td>
                                            <div class="btn-group" >
                                                <button style="width:100%" class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                    <i class="fa fa-angle-down"></i>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="javascript:;" onClick="edit_function('show',<?php echo $id ?>,<?php echo $count; ?>);"><i class="fa fa-pencil"></i>&nbsp;Edit</a></li>
                                                    <li><a href="javascript:;" onClick="edit_function('delete',<?php echo $id ?>);"><i class="fa fa-trash"></i>&nbsp;Hapus</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php } 
                                } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            <?php } ?>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
