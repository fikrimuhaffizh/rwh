<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<?php include 'global_function.php';$login_type = $this->session->userdata('login_type'); ?>

<script type="text/javascript">
    function resetForm(){
        $('#btn-submit').removeAttr('disabled');
        $('#btn-submit').removeAttr('class');
        $('#add-form').trigger("reset");
    }
    function add_function(task){
        if(task == 'show'){
            resetForm();
            $('#btn-submit').attr('onClick','add_function("add")').addClass('btn red');
            $('#add_modal').modal();
        }
        if(task == 'add'){
            $('#btn-submit').attr('disabled',1);
            $.ajax({
                method: 'POST',
                data:$('#add-form').serialize(),
                url: '<?php echo base_url() ?>administrator/bank/add/',
                dataType: 'json',
                success : function(resp){
                    if(resp == "add_success"){
                        $('#add_modal').modal('hide');
                        swal({
                              title: "Berhasil !",
                              text : "Data berhasil ditambahkan",
                              type: "success",
                              timer: 800,
                              showConfirmButton : false
                            },
                            function(){
                                swal.close();
                                table.ajax.reload();
                        });
                    } else {
                        $('#btn-submit').removeAttr('disabled');
                        alert(resp['empty_val']);
                    }
                }
            });
        }
    }

    function edit_function(task='',id=''){
        if(task == 'edit'){
            resetForm();
            $('#add_modal').modal();
            $('#btn-submit').attr('onClick','edit_function("update",'+id+')').addClass('btn green');
            $('input[name=nama_bank]').val($('#nama_bank_'+id).val());
            $('input[name=nomor_rekening]').val($('#nomor_rekening_'+id).val());
            $('select[name=jenis] option[value='+$('#jenis_'+id).val()+']').prop("selected", true);
        }

        if(task == 'update'){
            $('#btn-submit').attr('disabled',1);
            $.ajax({
                method: 'POST',
                data:$('#add-form').serialize(),
                url: '<?php echo base_url() ?>administrator/bank/update/'+id,
                dataType: 'json',
                success : function(resp){
                    if(resp == "update_success"){
                        $('#btn-submit').removeAttr('disabled');
                        $('#add_modal').modal('hide');
                        swal({
                              title: "Berhasil !",
                              text : "Data berhasil diperbaharui",
                              type: "success",
                              timer: 800,
                              showConfirmButton : false
                            },
                            function(){
                                swal.close();
                                table.ajax.reload();
                        });
                    }
                }
            });
        }

        if(task == 'delete'){
            var nama_bank = $('#nama_bank_'+id).val();
            url = '<?php echo base_url() ?>administrator/bank/delete/'+id;
            swal({
                  title: "Hapus Data Bank?",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "red",
                  cancelButtonClass: "grey",
                  confirmButtonText: "Yes, Hapus",
                  closeOnConfirm: false,
                  showLoaderOnConfirm : true
                },
                function(){
                    $.ajax({
                      method: 'POST',
                      url: url,
                      data : {'nama_bank' : nama_bank},
                      dataType: 'json',
                      success : function(resp){
                          if(resp == 'update_success'){
                            swal({
                                  title: "Berhasil !",
                                  text : "Data berhasil dihapus",
                                  type: "success",
                                  timer: 800,
                                  showConfirmButton : false
                                },
                                function(){
                                    swal.close();
                                    table.ajax.reload();
                            });
                          }
                      }
                  });
                }); 
            
        }
    }

    $(document).ready(function(){

        table = $('#table_bank').DataTable({ 
            "processing": true, "serverSide": true,"order": [],
            "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
            "ajax": {"url": "<?php echo site_url('administrator/bank/pagination')?>","type": "POST"},
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                        {"targets": [0],"className": "dt-center"},
                        {"targets": [3],"className": "dt-center"}

                    ]
        }); 

    });
</script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Bank</h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="<?php echo base_url().'administrator' ?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Bank</span>
        </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">
        <div class="col-md-12">

                <div class="row">
                    <div class="col-md-12">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <button id="sample_editable_1_new" onClick="add_function('show')" class="btn btn-sm red">Tambah Bank
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject bold uppercase"> Managed Bank</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <?php if($this->session->flashdata('update_success')){?>
                                    <div class="alert bg-green-jungle font-white col-md-6"><i class="fa fa-check"></i> <?php echo $this->session->flashdata('update_success') ?></div>
                                <?php } 
                                    if($this->session->flashdata('add_success')){ 
                                        echo '<div class="alert bg-green-jungle font-white"><i class="fa fa-check"></i> Penambahan bank berhasil</div>';
                                    } ?>
                                <table class="table table-striped table-bordered  table-condensed" id="table_bank">
                                    <thead>
                                        <tr>
                                            <th> No </th>
                                            <th> Nama Bank</th>
                                            <th> Rekening </th>
                                            <th> Jenis </th>
                                            <th> Aksi </th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<!-- add Modal-->
<div class="modal fade" id="add_modal"  role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bank"></i>
                    <span class="caption-subject sbold uppercase">Data Bank</span>
                </div>
            </div>
            <?php echo form_open('administrator', array( 'id' => 'add-form')); ?>
                <div class="portlet-body form">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="form_control_1">Nama Bank</label>
                                <input type="text" class="form-control input-sm" name="nama_bank" id="nama_bank" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="form_control_1">Nomor Rekening</label>
                                <input type="text" class="form-control input-sm" name="nomor_rekening" onkeypress="return event.charCode >= 45 && event.charCode <= 57" id="nomor_rekening" required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="form_control_1">Jenis</label>
                                <select class="form-control input-sm" name="jenis" id="jenis" required>
                                    <option value="Transfer">Transfer</option>
                                    <option value="EDC">EDC</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn grey">Cancel &nbsp;<i class="fa fa-times"></i></button>            
                    <button id="btn-submit" type="button" >Simpan &nbsp;<i class="fa fa-save"></i></button>
                </div>
                <?php echo form_close();?>
        </div>

    </div>
</div>