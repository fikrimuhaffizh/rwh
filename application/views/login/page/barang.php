<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<?php include 'global_function.php';$login_type = $this->session->userdata('login_type'); ?>

<script type="text/javascript">
    var listbarang = [];
    function resetForm(){
        $('#btn-submit').removeAttr('disabled');
        $('#btn-submit').removeAttr('class');
        $('#btn-reset').removeAttr('disabled');
        $('#add-form').trigger("reset");

    }
    function add_function(task){
        if(task == 'show'){
            resetForm();
            $('#edit_notice').hide();
            $('#btn-submit').attr('onClick','').addClass('btn red');
            $('#add_modal').modal();
            $('.add_form').show();
            $('.edit_form').hide();
        }
        if(task == 'add'){
            $('#btn-submit').attr('disabled',1);
            $.ajax({
                method: 'POST',
                data:$('#add-form').serialize(),
                url: '<?php echo base_url() ?>administrator/barang/add/',
                dataType: 'json',
                success : function(resp){
                    if(resp == "add_success"){
                        $('#add_modal').modal('hide');
                        swal({
                              title: "Berhasil !",
                              text : "Data berhasil ditambahkan",
                              type: "success",
                              timer: 800,
                              showConfirmButton : false
                            },
                            function(){
                                swal.close();
                                table.ajax.reload();
                        });
                    }
                }
            });
        }
    }

    function edit_function(task='',id=''){
        if(task == 'edit'){
            resetForm();
            $('#edit_notice').show();
            $('#add_modal').modal();
            $('.add_form').hide();
            $('.edit_form').show();
            $('input[name=nama_barang]').val($('#nama_barang_'+id).val());
            $('input[name=kode_barang]').val($('#kode_barang_'+id).val());
            $('input[name=harga_terakhir]').val($('#harga_terakhir_'+id).val());
            $('select[name=satuan] option[value='+$('#satuan_'+id).val()+']').prop("selected", true);
            $('input[name=barang_id]').val($('#barang_id_'+id).val());
            $('select[name=edit_kantor_cabang_modal] option[value='+$('#kantorcabang_id_'+id).val()+']').prop("selected", true);
            $('#btn-submit').attr('onClick','edit_function("update",'+id+')').addClass('btn green');
        }
        if(task == 'update'){
            $('#btn-submit').attr('disabled',1);
            $.ajax({
                method: 'POST',
                data:$('#add-form').serialize(),
                url: '<?php echo base_url() ?>administrator/barang/update/'+id,
                dataType: 'json',
                success : function(resp){
                    if(resp == "update_success"){
                        $('#btn-submit').removeAttr('disabled');
                        $('#add_modal').modal('hide');
                        swal({
                              title: "Berhasil !",
                              text : "Data berhasil diperbaharui",
                              type: "success",
                              timer: 800,
                              showConfirmButton : false
                            },
                            function(){
                                swal.close();
                                table.ajax.reload();
                        });
                    }
                }
            });
        }
        if(task == 'reset'){
            var serialized = $('#reset-form').serialize();
             if(serialized.indexOf('=&') > -1 || serialized.substr(serialized.length - 1) == '='){
               return alert('Form reset tidak boleh kosong !');
            }
            $('#reset_modal').modal('hide');
            swal({
                  title: "Reset Stok Barang?",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "red",
                  cancelButtonClass: "grey",
                  confirmButtonText: "Yes, Reset !",
                  closeOnConfirm: false,
                  showLoaderOnConfirm : true
                },
                function(){
                    $('#btn-reset').attr('disabled',1);
                    $.ajax({
                        method: 'POST',
                        data: serialized,
                        url: '<?php echo base_url() ?>administrator/barang/reset_stock/',
                        dataType: 'json',
                        success : function(resp){
                            if(resp == "update_success"){
                                $('#btn-submit').removeAttr('disabled');
                                
                                swal({
                                      title: "Berhasil !",
                                      text : "Data berhasil diperbaharui",
                                      type: "success",
                                      timer: 800,
                                      showConfirmButton : false
                                    },
                                    function(){
                                        location.reload();
                                });
                            }
                        }
                    });
                });  
        }
        if(task == 'add_to_kantor'){
            $('#result_lokasi').hide();
            $('#result_lokasi').empty();
            dtCabang = $('#add-lokasi-form').serialize();
            barang_id = id;
            $.ajax({
                method: 'POST',
                data:dtCabang,
                url: '<?php echo base_url() ?>administrator/barang/updateLokasiBarang/'+id,
                dataType : 'json',
                beforeSend : function(){$('#listKantorDiv').hide();$('.loading').show();},
                success : function(resp){
                    $('.loading').hide();
                    $('#listKantorDiv').show();
                    $('#result_lokasi').show();

                    
                    if(resp[0].length>0){
                        for(var i=0;i<resp[0].length;i++){
                            var success = '<div class="label label-success"><i class="fa fa-check"></i> '+resp[0][i]+'</div><div class="clearfix"></div><br>';
                            $('#result_lokasi').append(success);
                        }
                    }

                    if(resp[0].length>0){
                        for(var i=0;i<resp[1].length;i++){
                            var error = '<div class="label label-danger"><i class="fa fa-times"></i> '+resp[1][i]+'</div><div class="clearfix"></div><br>';
                            $('#result_lokasi').append(error);
                        }
                    }
                    table.ajax.reload();
                }   
            });   
        }
        if(task == 'delete'){
            url = '<?php echo base_url() ?>administrator/barang/delete/'+id;
            swal({
                  title: "Hapus Data Barang?",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "red",
                  cancelButtonClass: "grey",
                  confirmButtonText: "Yes, delete it!",
                  closeOnConfirm: false,
                  showLoaderOnConfirm : true
                },
                function(){
                    $.ajax({
                      method: 'POST',
                      url: url,
                      dataType: 'json',
                      success : function(resp){
                          if(resp == 'update_success'){
                            swal.close();
                            table.ajax.reload();
                          }
                      }
                  });
                }); 
        }
        if(task == 'delete_barangkantor'){
            url = '<?php echo base_url() ?>administrator/barang/delete_barangkantor/'+id;
            swal({
                  title: "Hapus Barang dari Kantor "+$('#nama_cabang_'+id).val()+"?",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "red",
                  cancelButtonClass: "grey",
                  confirmButtonText: "Yes, delete it!",
                  closeOnConfirm: false,
                  showLoaderOnConfirm : true
                },
                function(){
                    $.ajax({
                      method: 'POST',
                      url: url,
                      dataType: 'json',
                      success : function(resp){
                          if(resp == 'update_success'){
                            location.reload();
                          }
                      }
                  });
                }); 
        }
        if(task == 'show_reset'){
            resetForm();
            $('input[name="bkantor_id"]').val(id);
            $('#reset_modal').modal();
        }
        if(task == 'show_add_to_kantor'){
            $('#add_to_kantor_modal').modal();
            $('#result_lokasi').hide();
            $('#checkAllLokasi').prop('checked', false); 
            $('.kantor_cabang_modal').val(null).trigger('change');
            $('.kantor_cabang_modal').empty();
            $('#btn-simpanLokasi').attr("onClick","edit_function('add_to_kantor',"+id+")").addClass('btn red');

            var selectedVal = [];
            $.ajax({
                method: 'POST',
                data:{'barang_id' : id},
                url: '<?php echo base_url() ?>administrator/barang/getListKantorCabangByBarang',
                dataType : 'json',
                beforeSend : function(){$('#listKantorDiv').hide();$('#result_lokasi').hide();$('#result_lokasi').empty();$('.loading').show();},
                success : function(resp){
                    $('.loading').hide();
                    $('#listKantorDiv').show();
                    if(resp.length>0){
                        for(var i=0;i<resp.length;i++){
                            var newOption = new Option(resp[i]['nama_cabang'], resp[i]['kantorcabang_id'], false, false);
                            $('.kantor_cabang_modal').append(newOption).trigger('change');
                            if(resp[i]['selected']){
                                selectedVal[i] = resp[i]['kantorcabang_id'];
                            }
                        } 
                            $('.kantor_cabang_modal').val(selectedVal);
                    } else {
                        alert('Barang tidak ada dikantor manapun');
                    }
                }
            });
        }
    }
    function selectAllLokasi(val){
        if($("#checkAllLokasi").is(':checked') ){
            $('#multi2').find('option').prop('selected', 'selected').end().select2();
        } else {
            $('#multi2').find('option').prop('selected', false).end().select2();
        }

    }

    function toggleKantorCabang(check=""){
        if(check){
            $('.kantor_cabang_modal').attr('disabled',1);
        }
        else{
            $('.kantor_cabang_modal').removeAttr('disabled');
        }
    }

    $(document).ready(function(){
        table = $('#table_barang_new').DataTable({ 
            "processing": true, "serverSide": true,"order": [],
            "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
            "ajax": {"url": "<?php echo site_url('administrator/barang/pagination2')?>","type": "POST"},
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                        {"targets": [0],"className": "dt-center"},
                        {"targets": [2],"className": "dt-center"},
                        {"targets": [3],"className": "dt-center"},
                        {"targets": [4],"className": "dt-center"}
                    ]
        });
    });

</script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Inventaris Barang
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="<?php echo base_url().'administrator' ?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Barang</span>
        </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">
        <div class="col-md-12">
            <?php if($riwayat){ ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a onClick="window.history.go(-1)" class="btn btn-sm yellow-crusta"><i class="fa fa-arrow-circle-o-left"></i> Kembali</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <i class="fa fa-list font-dark"></i>
                                    <span class="caption-subject bold uppercase"> TOP 200 Riwayat Transaksi <?php echo '<span class="font-red">"'.$dt[0]->nama_barang.'"</span> di kantor <span class="font-red">"'.$dt[0]->nama_cabang.'"<?span>' ?></span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered  order-column" id="table-custom">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center"> No </th>
                                            <th> Barang </th>
                                            <th> Tanggal Transaksi </th>
                                            <th> No Faktur</th>
                                            <th style="text-align: center"> Jenis Transaksi </th>
                                            <th style="text-align: center"> Qty </th>
                                            <th style="text-align: center"> Stok Awal </th>
                                            <th style="text-align: center"> Stok Akhir </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            // echo_array($riwayat);
                                            $count = 0;
                                            foreach($dt as $row){
                                                $jenis_faktur = $row->jenis_trans == 'Masuk' ? '<span class="font-green-jungle"><i class="fa fa-download"></i> Barang Masuk</span>' : '<span class="font-red"><i class="fa fa-upload"></i> Barang Keluar</span>';
                                        ?>
                                            <tr>    
                                                <td style="text-align: center"><?php echo ++$count?></td>
                                                <td><?php echo $row->nama_barang ?></td>
                                                <td><i class="fa fa-calendar"></i> <?php echo date('d-M-Y H:i',strtotime($row->tgl_transaksi))?></td>
                                                <td><?php echo $row->no_faktur?></td>
                                                <td style="text-align: center"><?php echo $jenis_faktur?></td>
                                                <td style="text-align: center"><?php echo $row->qty ?></td>
                                                <td style="text-align: center"><?php echo $row->stok_awal ?></td>
                                                <td style="text-align: center"><?php echo $row->stok_akhir ?></td>
                                            </tr>
                                        <?php        
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            
            <?php } else if($list_kantor){?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="<?php echo base_url().'administrator/barang' ?>"class="btn btn-sm yellow-crusta"><i class="fa fa-arrow-circle-o-left"></i> Kembali</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <i class="fa fa-list font-dark"></i>
                                    <span class="caption-subject bold uppercase"> Lokasi Barang <?php echo '<span class="font-red">"'.$brg[0]->nama_barang.'"</span>' ?></span>

                                </div>
                            </div>
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered  order-column" id="table-custom">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center"> No </th>
                                            <th> Nama Barang</th>
                                            <th> Kepemilikan</th>
                                            <th style="text-align: center"> Stok Akhir </th>
                                            <th style="text-align: center"> Aksi </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $count = 0;
                                            foreach($dt as $row){
                                                // echo_array($row);
                                                $id = encrypt($row->bkantor_id);
                                               $btn_delete  = '<li><a href="javascript:;" onClick="edit_function(\'delete_barangkantor\','. $id.')"><i class="fa fa-trash"></i>&nbsp;Hapus</a></li>';
                                               $btn_history = '';
                                                $btn_reset = '<li><a onClick="edit_function(\'show_reset\','.$id.')"><i class="fa fa-refresh"></i>&nbsp;Reset Stock</a></li>';
                                               if($row->dependency > 0){
                                                    $btn_delete = '';
                                                    $btn_history = '<li><a href="'.base_url().'/administrator/barang/riwayat/'.$id.'"><i class="fa fa-list"></i>&nbsp;Riwayat</a></li>';
                                                }
                                        ?>
                                            <tr>    
                                                <td style="text-align: center"><?php echo ++$count?></td>
                                                <td><?php echo $row->nama_barang ?></td>
                                                <td><?php echo $row->nama_cabang ?></td>
                                                <td style="text-align: center"><?php echo $row->stok_akhir?></td>
                                                <td><?php echo '<div class="btn-group">
                                                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            '.$btn_history.$btn_delete.$btn_reset.'
                                                        </ul>
                                                        <input type="hidden" id="nama_cabang_'.$id.'" value="'.$row->nama_cabang.'">
                                                        <input type="hidden" id="barang_id_'.$id.'" value="'.encrypt($row->barang_id).'">
                                                    </div>' ?>
                                                </td>
                                            </tr>
                                        <?php        
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            
            <?php } else {?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <button id="sample_editable_1_new" onClick="add_function('show')" class="btn btn-sm red">Tambah Barang
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject bold uppercase"> Managed Barang</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <?php 
                                if($this->session->flashdata('error')){
                                    echo '<div class="alert bg-red font-white col-md-6"><i class="fa fa-times"></i> '. $this->session->flashdata('error').'</div>';
                                } 
                                if($this->session->flashdata('success')){ 
                                    echo '<div class="alert bg-green-jungle font-white col-md-6"><i class="fa fa-check"></i> '. $this->session->flashdata('success').'</div>';
                                }?>

                                <table class="table table-striped table-bordered  table-condensed" id="table_barang_new">
                                    <thead>
                                        <tr>
                                            <th> No </th>
                                            <th> Nama Barang</th>
                                            <th> Satuan </th>
                                            <th> Harga u/ satuan </th>
                                            <th> Lokasi </th>
                                            <th> Aksi </th>
                                        </tr>
                                    </thead>
                                </table>
                                
                            </div>
                        </div>
                    </div>
                </div>
            
            <?php } ?>
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<!-- lokasi Modal-->
<div class="modal fade" id="add_to_kantor_modal"  role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-social-dropbox"></i>
                    <span class="caption-subject sbold uppercase">Lokasi Barang</span>
                </div>
            </div>
            <?php echo form_open('administrator', array( 'id' => 'add-lokasi-form')); ?>
                <div class="portlet-body form">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="loading pull-left" style="display: none">
                                <img src="<?php echo base_url().'assets/frontend/img/AjaxLoader.gif' ?>" alt="">
                                <span>Mohon Tunggu...</span>
                            </div>
                            <div class="form-group"  id="listKantorDiv" required>
                                <label for="form_control_1">Silahkan Tambah atau Hapus lokasi barang dari kantor cabang berikut :  </label>
                                <div class="">
                                    <div class="input-group select2-bootstrap-append ">
                                        <select class="form-control select2 kantor_cabang_modal" id="multi2" name="kantor_cabang[]" multiple required>
                                               
                                        </select>
                                    </div>
                                    <div class="mt-checkbox-list">
                                        <label class="mt-checkbox mt-checkbox-outline"> Set untuk semua lokasi kantor cabang
                                            <input type="checkbox" id="checkAllLokasi" onclick="selectAllLokasi()">
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                        <div class="col-md-12 display-none" id="result_lokasi">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn grey">Cancel &nbsp;<i class="fa fa-times"></i></button>            
                    <button id="btn-simpanLokasi" class="btn red" type="button" >Simpan &nbsp;<i class="fa fa-save"></i></button>
                </div>
                <?php echo form_close();?>
        </div>

    </div>
</div>

<!-- add Modal-->
<div class="modal fade" id="add_modal"  role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-social-dropbox"></i>
                    <span class="caption-subject sbold uppercase">Data Barang</span>
                </div>
            </div>
            <?php echo form_open('administrator/barang/add/', array( 'id' => 'add-form')); ?>
                <div class="portlet-body form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_control_1">Nama Barang</label>
                                <input type="text" class="form-control input-sm" name="nama_barang" id="nama_barang" required>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="form_control_1">Kode</label>
                                <input type="text" class="form-control input-sm" name="kode_barang" id="kode_barang">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_control_1">Satuan</label>
                                <select class="form-control input-sm" name="satuan" id="satuan" required>
                                    <option></option>
                                    <option value="Buah">Buah</option>
                                    <option value="Lembar">Lembar</option>
                                    <option value="Unit">Unit</option>
                                    <option value="Pasang">Pasang</option>
                                    <option value="Keping">Keping</option>
                                    <option value="Dus">Dus</option>
                                    <option value="Karung">Karung</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_control_1">Harga u/ satuan</label>
                                <input type="number" class="form-control input-sm" name="harga_terakhir" id="harga_terakhir">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn grey">Cancel &nbsp;<i class="fa fa-times"></i></button>            
                    <button id="btn-submit" type="submit" >Simpan &nbsp;<i class="fa fa-save"></i></button>
                    <input type="hidden" name="barang_id">
                </div>
                <?php echo form_close();?>
        </div>

    </div>
</div>

<!-- add Modal-->
<div class="modal fade" id="reset_modal"  role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" fa fa-refresh font-red"></i>
                    <span class="caption-subject font-red sbold uppercase">Reset Stok Barang</span>
                </div>
            </div>
            <?php echo form_open('administrator/barang/reset_stock', array( 'id' => 'reset-form')); ?>
                <div class="portlet-body form">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_control_1">Reset stok ke jumlah : </label>
                                    <input type="number" class="form-control pull-right" style="width:100px" name="stok" id="stok" required>
                                    <input type="hidden" name="bkantor_id">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="form_control_1">Keterangan : </label>
                                    <textarea class="form-control" name="keterangan" required></textarea>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-sm grey">Cancel &nbsp;<i class="fa fa-times"></i></button>            
                    <button type="button" id="btn-reset" onClick="edit_function('reset')" class="btn btn-sm red" >Reset &nbsp;<i class="fa fa-save"></i></button>
                </div>
            <?php echo form_close();?>
        </div>

    </div>
</div