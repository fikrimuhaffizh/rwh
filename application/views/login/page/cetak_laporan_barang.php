
<link href="<?php echo base_url()?>assets/backend/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<?php
if($output=='Excel'){
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=stok_barang_".time().".xls");
    header("Cache-Control: no-cache, must-revalidate");
    header("Pragma: no-cache");
}
?>
<style>
table {border-collapse: collapse;padding: 8px;}
th, td {padding: 8px;border: 1px solid #ddd;}
th {background-color: #bababa;}
th {font-family: "Times New Roman", Times, serif;}
</style>
<html>
    <body>
        <h2>List Stok Barang  <?php echo $nama_cabang ?></h2>
        <table border="1">
            <th>No.</th>
            <th>Nama Barang</th>
            <th>Kepemilikan</th>
            <th>Stok Akhir</th>
            <th>Satuan</th>
            <?php 
                $count=0;

                foreach($dt as $row){
             ?>
                <tr>
                    <td><?php echo ++$count ?>.</td>
                    <td><?php echo $row->nama_barang; ?></td>
                    <td><?php echo $row->nama_cabang; ?></td>
                    <td><?php echo $row->stok_akhir; ?></td>
                    <td><?php echo $row->satuan?></td>
                </tr>
            <?php } ?>
        </table>
    </body>
</html>