<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<link href="<?php echo base_url()?>assets/backend/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    var a=0;
</script>
<?php
if($output=='Excel'){
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=list_jemaah_".time().".xls");
    header("Cache-Control: no-cache, must-revalidate");
    header("Pragma: no-cache");
}
?>
<style>
table {border-collapse: collapse;padding: 8px;}
th, td {padding: 8px;border: 1px solid #ddd;}
th {background-color: #bababa;}
th {font-family: "Times New Roman", Times, serif;}
</style>
<html>
    <body>
        <h1>List Jemaah "<?php echo $nama_paket ?>"</h1>
        <?php 
            if($tour_leader){
                $tl ='';
                echo '<h3>Tour Leader : ';
                foreach($tour_leader as $row) {
                    $tl .= '<b>'.$row->nama.'</b>, '; 
                }
                echo substr_replace($tl, '', -2);
                echo '</h3>';
            }
        ?>
        <table border="1">
            <tr>
                <th rowspan="2">No.</th>
                <th rowspan="2">No Registrasi</th>
                <th rowspan="2">Nama Jemaah</th>
                <th rowspan="2">Sex</th>
                <th rowspan="2">Handphone</th>
                <th rowspan="2">Email</th>
                <th rowspan="2">Nama Paket</th>
                <th rowspan="2">Harga Paket</th>
                <th rowspan="2">Kelas</th>
                <?php if($total_header_b>0){ ?>
                    <th colspan="<?php echo $total_header_b?>">Pembayaran</th>
                <?php } ?>

                <th rowspan="2">Total Pembayaran<br>(Awal, Tambahan, Pelunasan)</th>
                
                <?php if($total_header_k>0){ ?>
                    <th colspan="<?php echo $total_header_k?>">Pengembalian</th>
                <?php } ?>

                <th rowspan="2">Total<br>Pengembalian</th>
                <th rowspan="2">Biaya Tambahan</th>
                <th rowspan="2">Biaya Pengurang</th>
                <th rowspan="2">Sisa</th>
                <th rowspan="2">Status<br>Pembayaran</th>
                <th rowspan="2">Tanggal<br>Keberangkatan</th>
                <th rowspan="2">Berangkat<br>Dari</th>
                <th rowspan="2">Status<br>Keberangkatan</th>
                <th rowspan="2">Room</th>
                <th rowspan="2">Petugas</th>
            <tr>
            <?php 
                if($total_header_b>0){
                    for($i=0;$i<$total_header_b;$i++){
                        echo '<th>'.($i+1).'</th>';
                    }
                }
            ?>
            
            <?php 
                if($total_header_k>0){
                    for($i=0;$i<$total_header_k;$i++){
                        echo '<th>'.($i+1).'</th>';
                    }
                }
            ?>    
            </tr>
        </tr>
            <?php 
                $count=0;
                foreach($dt as $row){
                    $sisa = '-';
                    $keberangkatan    = "-";
                    $total_pembayaran = 0;
                    $total_pengembalian = 0;
                    /* BIAYA */
                        //Reguler
                            if($row->pwk_id){
                                $x = $this->md_paketwaktu_kelas->getPaketWaktuKelasByIdV3($row->pwk_id);
                                $y = $this->md_pembayaran->getPembayaranByJemaahByJenis($row->pendaftarandetail_id,'Pembayaran');
                                $z = $this->md_pembayaran->getPembayaranByJemaahByJenis($row->pendaftarandetail_id,'Pengembalian');
                            } 
                        //Haji
                            else {
                                $x = $this->md_paket->getPaketByPaketId($row->paket_id);
                            }

                    $a = $this->md_keberangkatan->getKeberangkatanByKeberangkatanId($row->keberangkatan_id);
                    $keberangkatan = $a ? date('d-M-Y',strtotime($a[0]->tgl_keberangkatan)) :'-';
             ?>
                <tr>
                    <td valign="top" align="center"><?php echo ++$count ?>.</td>
                    <td valign="top" align="center" ><?php echo $row->no_registrasi?></td>
                    <td valign="top"><?php echo $row->jenis_jemaah=='Tour Leader' ? $row->nama_jemaah.'<br><small>(Tour Leader)</small>' : $row->nama_jemaah ?></td>
                    <td valign="top" align="center"><?php echo $row->jenis_kelamin == 'Pria' ? 'M' : 'F'; ?></td>
                    <td valign="top"><?php echo $row->no_hp_2 ? $row->no_hp.' / '.$row->no_hp_2 : $row->no_hp; ?></td>
                    <td valign="top"><?php echo $row->email ? $row->email : '-' ; ?></td>
                    <td valign="top"><?php echo $x[0]->nama_paket; ?></td>
                    <td valign="top"><?php echo number_format($row->harus_bayar); ?></td>
                    <td valign="top" align="center"><?php echo $row->kelas?></td>

                    <?php   
                        $lunas = '0';
                        for($i=0;$i<$total_header_b;$i++){
                            if(!empty($y[$i])){
                                
                                $jumlah = number_format($y[$i]->jumlah_bayar);
                                $jumlah = $jumlah.' <br><small class="pull-right">('.$y[$i]->status_pembayaran.')</small>';
                                
                                if(
                                    $y[$i]->status_pembayaran=='Setoran Awal' ||
                                    $y[$i]->status_pembayaran=='Setoran Tambahan' ||
                                    $y[$i]->status_pembayaran=='Pelunasan'){
                                    $total_pembayaran += $y[$i]->jumlah_bayar;
                                }

                                if($y[$i]->status_pembayaran=='Pelunasan')
                                    $lunas='1';


                            } else {
                                $jumlah = '-';
                                
                            }
                            echo '<td valign="top">'.$jumlah.'</td>';
                        }   
                        //Status Pembayaran
                        $infant                 = $row->infant ? $row->infant : 0;
                        $sharing_bed            = $row->sharing_bed ? $row->sharing_bed : 0;
                        $biaya_kelas_pesawat    = $row->biaya_kelas_pesawat ? $row->biaya_kelas_pesawat : 0;
                        $harga_setelah_potongan = $row->harus_bayar-($infant+$sharing_bed)+$biaya_kelas_pesawat;
                        $harga_setelah_potongan = ($harga_setelah_potongan+$row->total_biaya_tambahan) - $row->total_biaya_pengurang;
                        $sisa = $harga_setelah_potongan-($total_pembayaran - $total_pengembalian);


                        $status = $lunas=='1' ? 'Lunas' : 'Belum Lunas</label>';
                        if(!$y)
                            $status = 'Belum Bayar</label>';
                        if($row->jenis_jemaah=='Tour Leader'){
                            $status = '-';
                            $sisa = 0;
                        }
                        
                    ?>
                    
                    <td valign="top"><?php echo $total_pembayaran == 0 ? '-' : number_format($total_pembayaran); ?></td>

                    <?php   
                        for($i=0;$i<$total_header_k;$i++){
                            if(!empty($z[$i])){
                                $jumlah = number_format($z[$i]->jumlah_bayar);
                                $jumlah = $jumlah.' <br><small class="pull-right">('.$z[$i]->jenis_pengembalian.')</small>';
                                $total_pengembalian += $z[$i]->jumlah_bayar;
                            } else {
                                $jumlah = '-';
                            }
                            echo '<td valign="top">'.$jumlah.'</td>';
                        }         
                    ?>
                    <td valign="top"><?php echo $total_pengembalian == 0 ? '-' : number_format($total_pengembalian); ?></td>
                    <td valign="top"><?php echo $row->total_biaya_tambahan == 0 ? '-' : number_format($row->total_biaya_tambahan)?></td>  
                    <td valign="top"><?php echo $row->total_biaya_pengurang == 0 ? '-' : number_format($row->total_biaya_pengurang)?></td>  
                    <td valign="top"><?php echo $sisa == 0 ? '-' : number_format($sisa)?></td>   
                    <td valign="top" align="center"><?php echo $status?></td>                
                    <td valign="top" align="center"><?php echo $keberangkatan?></td>
                    <td valign="top" align="center"><?php echo $row->nama_kota?></td>
                    <td valign="top" align="center"><?php echo $row->status_keberangkatan?></td>
                    <td valign="top" align="center"><?php echo $row->room ? $row->room : '-'?></td>
                    <td valign="top" align="center"><?php echo $row->nama_cs.'<br><small>('.date('d-M-Y H:i',strtotime($row->tgl_submit)).')</small>'?></td>
                </tr>
            <?php } ?>
        </table>
    </body>
</html>
