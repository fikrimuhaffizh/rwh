
<link href="<?php echo base_url()?>assets/backend/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<?php
if($output=='Excel'){
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=list_pembayaran_".time().".xls");
    header("Cache-Control: no-cache, must-revalidate");
    header("Pragma: no-cache");
}
?>
<style>
table {border-collapse: collapse;padding: 8px;}
th, td {padding: 8px;border: 1px solid #ddd;}
th {background-color: #bababa;}
th {font-family: "Times New Roman", Times, serif;}
</style>
<html>
    <body>
        <h2>List Pembayaran Jemaah : <?php echo date('d-F-Y',strtotime($tgl_awal)).' hingga '.date('d-F-Y',strtotime($tgl_akhir)) ?></h2>
        <table border="1">
            <th>No.</th>
            <th>No Kwitansi</th>
            <th>Nama Jemaah</th>
            <th>Sex</th>
            <th>Alamat</th>
            <th>Handphone</th>
            <th>Email</th>
            <th>Nama Paket</th>
            <th>Transaksi</th>
            <th>Jenis</th>
            <th>Tanggal</th>
            <th>Metode</th>
            <th>Jumlah</th>
            <th>Petugas</th>
            <th>Kantor</th>
           
            <?php 
                $count=0;
                foreach($dt as $row){
                    $total_pembayaran = "-";
                    $status = "-";
                    //Reguler
                        if($row->pwk_id){
                            $x = $this->md_paketwaktu_kelas->getPaketWaktuKelasByIdV3($row->pwk_id);
                        } 
                    //Haji
                        else {
                            $x = $this->md_paket->getPaketByPaketId($row->paket_id);
                        }
                    $status = !$row->status_verifikasi_pembayaran ? "Menunggu" : $row->status_verifikasi_pembayaran;

                    //Jenis
                        $jenis = $row->jenis_transaksi == 'Pembayaran' ? $row->status_pembayaran : $row->jenis_pengembalian;

                    //CS

             ?>
                <tr>
                    <td><?php echo ++$count ?>.</td>
                    <td><?php echo $row->no_kwitansi?></td>
                    <td><?php echo $row->nama_jemaah; ?></td>
                    <td><?php echo $row->jenis_kelamin == 'Pria' ? 'M' : 'F'; ?></td>
                    <td><?php echo $row->alamat; ?></td>
                    <td><?php echo $row->no_hp; ?></td>
                    <td><?php echo $row->email ? $row->email : '-'; ?></td>                    
                    <td><?php echo $x[0]->nama_paket; ?></td>
                    <td><?php echo $row->jenis_transaksi; ?></td>
                    <td><?php echo $jenis ?></td>
                    <td><?php echo date('d-M-Y H:i',strtotime($row->tgl_bayar)); ?></td>
                    <td><?php echo $row->metode_transaksi; ?></td>
                    <td><?php echo number_format($row->jumlah_bayar); ?></td>
                    <td><?php echo $row->nama_lengkap?></td>                  
                    <td><?php echo $row->nama_cabang?></td>    
                </tr>
            <?php } ?>
        </table>
    </body>
</html>
