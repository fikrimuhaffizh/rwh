
<link href="<?php echo base_url()?>assets/backend/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<?php
if($output=='Excel'){
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=list_pendaftar_".time().".xls");
    header("Cache-Control: no-cache, must-revalidate");
    header("Pragma: no-cache");
}
?>
<style>
table {border-collapse: collapse;padding: 8px;}
th, td {padding: 8px;border: 1px solid #ddd;}
th {background-color: #bababa;}
th {font-family: "Times New Roman", Times, serif;}
</style>
<html>
    <body>
        <h2>List Pendaftaran Jemaah : <?php echo date('d-F-Y',strtotime($tgl_awal)).' hingga '.date('d-F-Y',strtotime($tgl_akhir)); ?></h2>
        <table border="1">
            <th>No.</th>
            <th>Tanggal Daftar</th>
            <th>Nama Jemaah</th>
            <th>Jenis Kelamin</th>
            <th>Alamat</th>
            <th>Handphone</th>
            <th>Email</th>
            <th>Nama Paket</th>
            <th>Jenis Pendaftar</th>
            <th>Harus Bayar</th>
            <th>Status Pendaftaran</th>
            <?php 
                $count=0;
                foreach($dt as $row){
                    //Reguler
                    if($row->pwk_id){
                        $x = $this->md_paketwaktu_kelas->getPaketWaktuKelasByIdV3($row->pwk_id);
                        $harus_bayar = rupiah_format($row->harus_bayar);
                    } 
                    //Haji
                    else {
                        $x = $this->md_paket->getPaketByPaketId($row->paket_id);
                        $harus_bayar = "-";
                    }
                    $y = $this->md_jenis_paket->getJenisPaketByJenisPaketId($x[0]->jenispaket_id);
                    
             ?>
                <tr>
                    <td><?php echo ++$count ?>.</td>
                    <td><?php echo date('d-M-Y H:i',strtotime($row->tgl_daftar)); ?></td>
                    <td><?php echo $row->nama_jemaah; ?></td>
                    <td><?php echo $row->jenis_kelamin; ?></td>
                    <td><?php echo $row->alamat; ?></td>
                    <td><?php echo $row->no_hp; ?></td>
                    <td><?php echo $row->email ? $row->email : '-' ; ?></td>
                    <td><?php echo $x[0]->nama_paket; ?></td>
                    <td><?php echo $y[0]->jenis_paket; ?></td>
                    <td><?php echo $harus_bayar; ?></td>
                    <td><?php echo $row->status_verifikasi ? $row->status_verifikasi : "Menunggu Validasi"?></td>
                </tr>
            <?php } ?>
        </table>
    </body>
</html>