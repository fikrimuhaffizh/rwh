<link href="<?php echo base_url()?>assets/backend/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>assets/backend/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>assets/backend/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>assets/backend/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url()?>assets/backend/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?php echo base_url()?>assets/backend/global/css/plugins.min.css" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url()?>assets/backend/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>assets/backend/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="<?php echo base_url()?>assets/backend/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>


<script type="text/javascript">
    window.print();
    $(document).ready(function(){
       $('.main_page').clone().appendTo('.duplicate_page');
    }); 
</script>

<style>
    @font-face {
        font-family: book_antiqua;
        src: url('<?php echo base_url(); ?>assets/backend/font/book_antiqua.ttf');
    }
    body, h1, h2, h3, h4, h5, small, span { font-family: book_antiqua, Arial, sans-serif; }
    @page { size: auto;  margin: 0mm;}
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {padding:3px;font-size: 11px}
    li {font-size: 12px}
    .font11{font-size: 11px}
    .font18{font-size: 18px}
    .font28{font-size: 28px}
</style>

<div class="main_page">
    <div class="col-md-12" style="margin-top:25px">
        
        <?php echo cetakHeader(); ?>

        <div class="col-md-12">
            </div>
            <?php 
                foreach($pembayaran as $row)
                $jenis_transaksi = $row->jenis_transaksi;
                if($jenis_transaksi == 'Pembayaran'){
                    $kata1 = 'Telah diterima pembayaran oleh pihak RWH dengan detail sebagai berikut :';
                    $kata2 = 'Diserahkan Oleh';
                    $kata3 = 'Diterima Oleh';
                } else if($jenis_transaksi=='Pengembalian'){
                    $kata1 = 'Telah dilakukan pengembalian dana dari pihak RWH dengan detail sebagai berikut :';
                    $kata2 = 'Diterima Oleh';
                    $kata3 = 'Diserhakan Oleh';
                }
                $bulan = all_bulan();
            ?>
            <div class="col-md-12 col-sm-12" >
                <div style="border-top:1px solid #efefef"></div>
                <br>
                <div style="text-align: center;margin-top:-10px;margin-bottom:-20px"><b class="font28">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>KWITANSI</u></b> <span class="pull-right">No : <b class="font18"><?php echo $row->no_kwitansi?></b></span></div>
                <br>
                <div>
                <h5><?php echo $kata1 ?></h5>
                <div>
                    <div class="col-md-12">
                        <table class="table">
                            <tr>
                                <td width="150">No. Registrasi</td>
                                <td>:</td>
                                <td><b><?php echo $row->no_registrasi ?></b></td>
                            </tr>
                            <tr>
                                <td>Nama Jemaah</td>
                                <td>:</td>
                                <td><?php echo $row->nama_jemaah ?></td>
                            </tr>
                            <tr>
                                <td>Paket</td>
                                <td>:</td>
                                <td><?php echo $row->nama_paket?></td>
                            </tr>
                            <tr>
                                <td>Keberangkatan</td>
                                <td>:</td>
                                <td><?php echo $row->nama_kota.', '.$bulan[$row->bulan_berangkat].' '.$row->tahun_berangkat?></td>
                            </tr>    
        
                            <tr>
                                <td>Tanggal Keberangkatan</td>
                                <td>:</td>
                                <td><?php echo $row->tgl_keberangkatan ? date('d-F-Y',strtotime($row->tgl_keberangkatan)) : '-' ?></td>
                            </tr>   
<!--                             <?php if($jenis_transaksi=='Pembayaran'){ ?>
                                <tr>
                                    <td>Status Pembayaran</td>
                                    <td>:</td>
                                    <td><?php echo $row->status_pembayaran?></td>
                                </tr>                                    
                            <?php } else if($jenis_transaksi =='Pengembalian'){?>  
                                <tr>
                                    <td>Jenis Pengembalian</td>
                                    <td>:</td>
                                    <td><?php echo $row->jenis_pengembalian?></td>
                                </tr>                                      
                            <?php } ?> -->
                            <tr>
                                <td>Jumlah <?php echo $jenis_transaksi ?></td>
                                <td>:</td>
                                <td>Rp. <?php echo number_format($row->jumlah_bayar).' <b>( '.($row->status_pembayaran ? $row->status_pembayaran : $row->jenis_pengembalian).' )</b>'?></td>
                            </tr>  
                            <tr>
                                <td>Catata <?php echo $jenis_transaksi ?></td>
                                <td>:</td>
                                <td><?php echo $row->catatan ? $row->catatan : '-'?></td>
                            </tr> 
                        </table>
                    </div>
                    <h5>diharapkan bukti <?php echo strtolower($jenis_transaksi) ?> ini dapat digunakan dengan sebagaimana mestinya.</h5>
                    <br>
                    <div class="col-md-12">
                        <div class=" pull-right">
                            <span><?php echo $this->session->userdata('kota_pengguna') ?>, <?php echo date('d-F-Y')?></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <div class="pull-left center" style="text-align: center">
                            <br>
                            <b><?php echo $kata2 ?></b>
                            <br><br><br><br>
                            <span>(...........................................................................)</span>
                            <br><span>Nama Lengkap dan Tanda Tangan</span>
                        </div>
                        <div class=" pull-right" style="text-align: center">
                            <br>
                            <b><?php echo $kata3 ?></b>
                            <br><br><br><br>
                            <span>_____________________________________</span>
                            <br>
                            <span><?php echo $this->session->userdata('nama') ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div><br>
<div class="duplicate_page"></div>

<?php die; ?>