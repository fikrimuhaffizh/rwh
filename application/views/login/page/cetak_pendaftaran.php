<!-- BEGIN GLOBAL MANDATORY STYLES -->

<link href="<?php echo base_url()?>assets/backend/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>assets/backend/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>assets/backend/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>assets/backend/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?php echo base_url()?>assets/backend/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?php echo base_url()?>assets/backend/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="<?php echo base_url()?>assets/backend/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>assets/backend/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="<?php echo base_url()?>assets/backend/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />



<script type="text/javascript">
    window.print();
</script>

<style>
    @font-face {
        font-family: book_antiqua;
        src: url('<?php echo base_url(); ?>assets/backend/font/book_antiqua.ttf');
    }
    body, h1, h2, h3, h4, h5, small, span { font-family: book_antiqua, Arial, sans-serif; }
    @page { size: auto;  margin: 0mm;}
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {padding:3px;font-size: 11px}
    li {font-size: 12px}
    .font11{font-size: 11px}
    .font10{font-size: 10px}
    .warna-merah{color:red;}
}
</style>

<div class="col-md-12" style="margin-top:25px">
    
    <?php echo cetakHeader(); ?>
    
    <div class="col-md-12">
        </div>
        <?php 
            // echo_array($dt_jemaah);
            foreach($dt_jemaah as $row)
      
            $hp                   = $row->no_hp;
            $tempat_lahir         = $row->tempat_lahir ? $row->tempat_lahir : '-';
            $harus_bayar          = rupiah_format($row->harus_bayar);
            $statusverif          = $row->status_verifikasi == 'Valid' ? '<b>Valid</b>' : '<b>Menunggu</b>';
            $status_keberangkatan = $row->status_keberangkatan == 'Dalam Proses' ? '<b class="font-red9">Dalam Proses</b>' : $row->status_keberangkatan;


            //Booking Paket Haji
            if($row->jenis_paket == 'Haji'){
                $nama_paket             = $row->nama_paket_haji;
                $keberangkatan          = "Estimasi Tahun".$row->tahun_est_berangkat;
                $jenis_quota            = $row->jenis_quota;
                $harga                  = $row->harga_terakhir_usd ? '$'.$row->harga_terakhir_usd : rupiah_format($row->harga_terakhir);
                $tgl_berangkat          = '-';
                $kamar                  = '-';
                $harga_setelah_potongan = FALSE;
                $show_harusBayar = FALSE;
            } 
            //Booking Paket Reguler
            else {

                $nama_paket             = $row->nama_paket_reguler;
                $keberangkatan          = $list_bulan[$row->bulan_berangkat].' '.$row->tahun_berangkat;
                $berangkat_awal         = $row->kota_keberangkatan;
                $harga                  = $row->harus_bayar_usd ? '$'.$row->harus_bayar_usd : rupiah_format($row->harus_bayar);
                $tgl_berangkat          = $row->tgl_keberangkatan ? '<i class="fa fa-plane"></i>&nbsp;'.date('d-M-Y',strtotime($row->tgl_keberangkatan)) : 'Belum dijadwalkan';
                $kamar                  = $row->kelas;
                $infant                 = $row->infant ? $row->infant : 0;
                $sharing_bed            = $row->sharing_bed ? $row->sharing_bed : 0;
                $biaya_kelas_pesawat    = $row->biaya_kelas_pesawat ? $row->biaya_kelas_pesawat : 0;
                $biaya_tambahan         = $biaya_kelas_pesawat;
                $biaya_pengurang        = $infant+$sharing_bed;

            }


        ?>
        <div class="col-md-12 col-sm-12" >
            <div style="border-top:1px solid #efefef"></div>
            <div class="row" >
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <h5>Data Jemaah : </h5>
                    <table class="table" style="margin-bottom: 0px">
                        <tr>
                            <td width="130">Nama Jemah</td>
                            <td>:</td>
                            <td><?php echo $row->nama_jemaah ?></td>
                        </tr>
                       
                        <tr>
                            <td>Tempat & Tanggal Lahir</td>
                            <td>:</td>
                            <td><?php echo $row->tempat_lahir.', '.indo_date($row->tgl_lahir) ?></td>
                        </tr>
                        <tr>
                            <td>Jenis Kelamin</td>
                            <td>:</td>
                            <td><?php echo $row->jenis_kelamin ?></td>
                        </tr>
                        <tr>
                            <td>No Identitas</td>
                            <td>:</td>
                            <td><?php echo $row->no_ktp ? $row->no_ktp : '-'; ?></td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td>:</td>
                            <td><?php echo $row->alamat ? $row->alamat : '-' ?></td>
                        </tr>
                        <tr>
                            <td>Handphone</td>
                            <td>:</td>
                            <td><?php echo $hp ?></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>:</td>
                            <td><?php echo $row->email ? $row->email : '-' ?></td>
                        </tr>
                        <tr>
                            <td>Pendaftar</td>
                            <td>:</td>
                            <td><?php echo $row->nama_member ?></td>
                        </tr>
                        <tr>
                            <td>Infant</td>
                            <td>:</td>
                            <td><?php echo $row->infant ? 'Ya': 'Tidak' ?></td>
                        </tr>
                        <tr>
                            <td>Sharing Bed</td>
                            <td>:</td>
                            <td><?php echo $row->sharing_bed ?  'Ya': 'Tidak' ?></td>
                        </tr>
                        <tr>
                            <td>Kelas Pesawat</td>
                            <td>:</td>
                            <td><?php echo $row->kelas_pesawat ?></td>
                        </tr>
                    </table>        
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <h5>&nbsp;<span class="pull-right">No. <b><?php echo $row->no_registrasi ?></b></span></h5>
                    <table class="table" style="margin-bottom: 0px">
                        <tr>
                            <td width="130">Tanggal Daftar</td>
                            <td>:</td>
                            <td><?php echo indo_date($row->tgl_submit) ?></td>
                        </tr>
                        <tr>
                            <td>Nama Paket</td>
                            <td>:</td>
                            <td><?php echo $nama_paket ?></td>
                        </tr>
                        <tr>
                            <td>Harga Paket</td>
                            <td>:</td>
                            <td><b><?php echo $harga; ?></b></td>
                        </tr>
                        <tr>
                            <td>Kelas</td>
                            <td>:</td>
                            <td><b><?php echo $kamar; ?></b></td>
                        </tr>
                        <tr>
                            
                            <?php if($row->jenis_paket == 'Umroh'){ ?>
                                <td>Berangkat Dari</td>
                                <td>:</td>
                                <td><?php echo $berangkat_awal ?></td>
                            <?php } else { ?>
                                <td>Jenis Quota</td>
                                <td>:</td>
                                <td><?php echo $jenis_quota ?></td>
                            <?php } ?>

                        </tr>
                        <tr>
                            <td> Keberangkatan</td>
                            <td>:</td>
                            <td><?php echo $keberangkatan ?></td>
                        </tr>
                        <tr>
                            <td> Tanggal Berangkat</td>
                            <td>:</td>
                            <td><?php echo $tgl_berangkat ?></td>
                        </tr>
                        <tr>
                            <td> Status Keberangkatan</td>
                            <td>:</td>
                            <td><?php echo $status_keberangkatan ?></td>
                        </tr>
                        <tr>
                            <td>Visa Progressive</td>
                            <td>:</td>
                            <td><?php echo $row->progressive ?></td>
                        </tr>
                        <tr>
                            <td>Biaya Tambahan</td>
                            <td>:</td>
                            <td><b><?php echo $biaya_tambahan > 0 ? rupiah_format($biaya_tambahan) : '-'; ?></b></td>
                        </tr>


                        <tr>
                            <td>Biaya Pengurang</td>
                            <td>:</td>
                            <td><b><?php echo $biaya_pengurang > 0 ? rupiah_format($biaya_pengurang) : '-'; ?></b></td>
                        </tr>


                    </table>
                </div>
   
                    <div class="col-md-12" style="padding-bottom:5px">
                        <table class="table" style="margin-bottom: 5px">
                            <tr>
                                <td width="130">Catatan Jemaah</td>
                                <td width="10">:</td>
                                <td><i><?php echo $row->catatan ? $row->catatan : '-' ?></i></td>
                            </tr>
                        </table>
                    </div>
          
            </div>
        </div>
        <div class="col-md-12 col-sm-12">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="font11">
                    <div class="row">
                        <h5><b>Biaya Umroh Sudah Termasuk :</b></h5>
                        <ul>
                            <li>Tiket Penerbangan Kelas Ekonomi (PP)</li>
                            <li>Hotel Dekat Dengan Masjidi Haram</li>
                            <li>Makan 3x Sehari (Khas Indonesia)</li>
                            <li>Muthawwif / Guide yang Berpengalaman</li>
                            <li>Manasik 2x (Indonesia & Madinah)</li>
                            <li>Full Ziarah Madinah & Mekkah</li>
                            <li>Bus Full AC</li>
                            <li>Perlengkapan Umroh : Travel Bag, Tas Pasport, Tas Sendal, Sajadah, Ihram/Mukena, Bantal, Selimut, Sabuk, Buku Panduan Do'a dan Manasik Umrah, Bahan Seragam Batik & Air Zam-zam 5 Liter <b>(Jaket Hanya Untuk Pogram VIP)</b></li>
                        </ul>
                    </div>
                </div>
                <div class="font11">
                    <div class="row">
                        <h5><b>Biaya Umroh Belum Termasuk :</b></h5>
                        <ul>
                            <li>Pembuatan Passport atau Penambahan Nama</li>
                            <li>Suntik Meningitis dan Influenza</li>
                            <li>Surat Mahram Rp. 300.000 (bagi wanita dibawah umur 45 tahun yang berangkat tanpa muhrim)</li>
                            <li>Telp/Laundry/Keperluan Pribadi</li>
                            <li>Kursi Roda</li>
                        </ul>
                    </div>
                </div> 
                <div class="font11"></div>
                    <div class=" row">
                        <br><i>
                        <b>Catatan :</b>
                        <ul>
                            <li>Pembayaran dianggap sah setelah ada Validasi dari Bank/Kwitansi dari Kantor Pusat</li>
                            <li>Pembayaran dengan Cek/Giro dianggap sah apabila sudah dicairkan</li>
                        </ul>
                    </i>
                    </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="font11" style="border:1px solid #000000;padding:-5px">
                    <div class="col-md-12">
                        <h5><b>Ketentuan Pembatalan :</b></h5>
                        <ul style="padding-left:20px">
                            <li>Pembatalan > 45 Hari Sebelum Keberangkatan, Setoran Awal Hangus</li>
                            <li>Pembatalan > 30 Hari Sebelum Keberangkatan, Dipotong 30% dari Paket</li>
                            <li>Pembatalan > 20 Hari Sebelum Keberangkatan, Dipotong 75% dari Paket</li>
                            <li>Pembatalan > 10 Hari Sebelum Keberangkatan, Dipotong 100% dari Paket</li>
                            <li><b>Pembatalan > 14 Hari Sebelum Keberangkatan Akibat Sakit Keras, Meninggal Dunia atau Kecelakaan Uang Kembali 100%</b></li>
                        </ul>
                    </div>
                </div><br>
                <div class="font11 " style="border:1px solid #000000">
                    <div class="col-md-12">
                        <h5><b>Catatan Penting :</b></h5>
                        <ul style="padding-left:20px">
                            <li>Jamaah Umrah bersedia melengkapi dokumen yang diperlukan untuk pengurusan VISA paling lambat 21 hari sebelum keberangkatan. (Perusahaan tidak bertanggung jawab oleh keterlambatan penyerahan sokumen oleh Jamaah)</li>
                            <li>Jamaah Umrah Wajib Melunasi Biaya Umrah selambat-lambatnya 30 hari sebelum keberangkatan</li>
                            <li>Kepastian Keberangkatan Umrah Tergantung Kelulusan VISA dari Kedutaan Besar Saudi Arabia di Jakarta</li>
                            <li>Jamaah Umrah sudah setuju dengan segala ketentuan yang telah ditetapkan oleh PT.RIAU WISATA HATI</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-sm-12" >
            <div class="col-md-12 font11">
                <div class="col-md-6 col-xs-6">
                    <br><br>
                    <div style="text-align: center;margin-top:-10px">
                        <b>Pemohon Sesuai KTP</b>
                        <br><br><br><br>
                        <span>_____________________________________</span>
                        <br>
                        <span><?php echo $row->nama_jemaah?></span>
                    </div>
                </div>
                <div class="col-md-6 col-xs-6">
                    <div style="text-align: center;margin-top:-10px">
                        <br><br>
                        <b>Penerima Pendaftaran</b>
                        <br><br><br><br>
                        <span>_____________________________________</span>
                        <br>
                        <span><?php echo $this->session->userdata('nama') ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php die; ?>