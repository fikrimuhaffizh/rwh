<link href="<?php echo base_url()?>assets/backend/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>assets/backend/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>assets/backend/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>assets/backend/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url()?>assets/backend/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?php echo base_url()?>assets/backend/global/css/plugins.min.css" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url()?>assets/backend/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>assets/backend/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="<?php echo base_url()?>assets/backend/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>


<script type="text/javascript">
    window.print();
    $(document).ready(function(){
        var total_perlengkapan = '<?php echo count($perlengkapan); ?>';
        if(total_perlengkapan<6){
            $('.main_page').clone().appendTo('.duplicate_page');
        }
    }); 
</script>

<style>
    @font-face {
        font-family: book_antiqua;
        src: url('<?php echo base_url(); ?>assets/backend/font/book_antiqua.ttf');
    }
    body, h1, h2, h3, h4, h5, small, span { font-family: book_antiqua, Arial, sans-serif; }
    @page { size: auto;  margin: 0mm;}
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {padding:3px;font-size: 11px}
    li {font-size: 12px}
    .font11{font-size: 11px}
    .font18{font-size: 18px}
    .font28{font-size: 22px}
</style>

<div class="main_page">
    <div class="col-md-12" style="margin-top:20px">
        
        <?php echo cetakHeader(); ?>
    
        <div class="col-md-12">
            </div>
            <?php 
                // echo_array($perlengkapan);
                foreach($perlengkapan as $row)
                $jenis_transaksi = 'Penyerahan Perlengkapan';
                $kata1 = 'Telah dilakukan penyerahan perlengkapan jemaah dari pihak RWH dengan detail sebagai berikut :';
                $kata2 = 'Diterima Oleh';
                $kata3 = 'Diserhakan Oleh';
                $bulan = all_bulan();

                $keberangkatan = $row->jenis_paket == 'Haji' ? 'Estimasi Tahun '.$row->tahun_est_berangkat : $row->nama_kota.', '.$bulan[$row->bulan_berangkat].' '.$row->tahun_berangkat;
            ?>
            <div class="col-md-12 col-sm-12" >
                <div style="border-top:1px solid #efefef"></div>
                <br>
                <div style="text-align: center;margin-top:-10px;margin-bottom:-15px"><b class="font28">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>SERAH TERIMA</u></b> <span class="pull-right">No : <b class="font18"><?php echo $row->no_faktur?></b></span></div>
                <br>
                <div>
                <h5><?php echo $kata1 ?></h5>
                
                
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <table class="table"  style="margin-bottom:0px">
                            <tr>
                                <td width="140">No. Registrasi</td>
                                <td width="10">:</td>
                                <td><b><?php echo $row->no_registrasi ?></b></td>
                            </tr>
                            <tr>
                                <td>Nama Jemaah</td>
                                <td>:</td>
                                <td><?php echo $row->nama_jemaah ?></td>
                            </tr>
                            <tr>
                                <td>Paket</td>
                                <td>:</td>
                                <td><?php echo $row->nama_paket_reguler ? $row->nama_paket_reguler : $row->nama_paket_haji ?></td>
                            </tr>
                            <tr>
                                <td>Keberangkatan</td>
                                <td>:</td>
                                <td><?php echo $keberangkatan?></td>
                            </tr>   
                            <tr>
                                <td>Tanggal Keberangkatan</td>
                                <td>:</td>
                                <td><?php echo $row->tgl_keberangkatan ? date('d-F-Y',strtotime($row->tgl_keberangkatan)) : '-' ?></td>
                            </tr> 
                            <tr>
                                <td>Catatan</td>
                                <td>:</td>
                                <td><?php echo $row->keterangan ? $row->keterangan : '-' ?></td>
                            </tr> 
                        </table>  
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <table class="table" style="margin-bottom:0px">
                            <tr>
                                <td width="180"><b>List Perlengkapan</b></td>
                                <td width="10">:</td>
                                <td>
                                </td>
                            </tr>
                            
                            <?php foreach($perlengkapan as $row2){?>
                                    <tr>
                                        <td><i><?php echo $row2->nama_barang?></i></td>
                                        <td><i><?php echo $row2->qty.' '.$row2->satuan?></i></td>
                                        <td><i><i><?php echo date('d-M-Y',strtotime($row2->tgl_penyerahan))?></i></td>
                                    </tr>   

                                
                            <?php } ?> 
                            
                        </table>  
                    </div>
                    <div class="clearfix"></div>
                    <h5>diharapkan bukti <?php echo strtolower($jenis_transaksi) ?> ini dapat digunakan dengan sebagaimana mestinya.</h5>
        
                    <div class="col-md-12">
                        <div class=" pull-right">
                            <span><?php echo $this->session->userdata('kota_pengguna') ?>, <?php echo date('d').'-'.$bulan[date('n')].'-'.date('Y')?></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-md-12">
                        <div class="pull-left center" style="text-align: center">
                            <br>
                            <b><?php echo $kata2 ?></b>
                            <br><br><br>
                            <span>(...........................................................................)</span>
                            <br><span>Nama Lengkap dan Tanda Tangan</span>
                        </div>
                        <div class=" pull-right" style="text-align: center">
                            <br>
                            <b><?php echo $kata3 ?></b>
                            <br><br><br>
                            <span>_____________________________________</span>
                            <br>
                            <span><?php echo $this->session->userdata('nama') ?></span>
                        </div>
                    </div>
                
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div><br>
<div class="duplicate_page"></div>

<?php die; ?>