<div id="chart_jamaah" style="width: 100%; height: 350px; "></div>
	<script type="text/javascript">
		jQuery(document).ready(function() {
		    $("#chart_jamaah").highcharts( {
		    chart: {
		        type: 'column'
		    },
		    title: {
		        text: 'Jumlah Jemaah Pada Setiap Kota Keberangkatan'
		    },
		    subtitle: {
		        text: 'Source: riauwisatahati.com'
		    },
		    xAxis: {
		        categories: [
				
				<?php
				$listbulan=all_bulan();
				$i=0;
				foreach($bulan as $dtbulan){
					$i++;
					if($i==count($bulan)){
						echo "'".substr($listbulan[$dtbulan->bulan_berangkat],0,3)." ".$dtbulan->tahun_berangkat."' ";
						//echo $dtbulan->bulan_berangkat;
					}else{
						echo "'".substr($listbulan[$dtbulan->bulan_berangkat],0,3)." ".$dtbulan->tahun_berangkat."', ";
						//echo $dtbulan->bulan_berangkat." , ";
					}
				}
				?>],
		        crosshair: true
		    },
		    yAxis: {
		        min: 0,
		        title: {
		            text: 'Jemaah (@)'
		        }
		    },
		    tooltip: {
		        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
		        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		            '<td style="padding:0"><b>{point.y:.0f} jamaah</b></td></tr>',
		        footerFormat: '</table>',
		        shared: true,
		        useHTML: true
		    },
		    plotOptions: {
		        column: {
		            pointPadding: 0.2,
		            borderWidth: 0
		        }
		    },
		    series: [
					<?php
						$j=0;
						foreach($kota['data'] as $kt){
							$j++;
							echo "{";
							echo "name: '".$kt->nama_kota."', ";
							echo "data: [";
								$k=0;
								foreach($bulan as $dtbulan){
									$k++;
									$dtjamaah=$this->md_pendaftaran_detail->getPendaftaranDetailByBulanByKota($dtbulan->tahun_berangkat,$dtbulan->bulan_berangkat,$kt->kota_id);				
									if($k==count($bulan)){
										echo $dtjamaah;
									}else{
										echo $dtjamaah.",";
										
									}
								}
							echo "]";
							if($j==count($kota)){	
								echo " } ";
							}else{
								echo " }, ";
							}			
						}
					?>
				]
			})
		});
	</script>