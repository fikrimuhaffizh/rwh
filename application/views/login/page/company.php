<?php 
     $img_link =base_url().'assets/frontend/img/noimagefound.jpg';
?>
<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<script type="text/javascript">

    function edit_function(task=''){
        if(task == 'edit'){

            $('.data_input').removeClass('hide');
            $('.data_text').addClass('hide');

            $('.btn_save').removeClass('hide');
            $('.btn_cancel').removeClass('hide');

            $('.btn_edit').addClass('hide');
            $('.btn_hapus').addClass('hide');
        }

        else if(task == 'cancel'){
            $('.data_input').addClass('hide');
            $('.data_text').removeClass('hide');

            $('.btn_save').addClass('hide');
            $('.btn_cancel').addClass('hide');

            $('.btn_edit').removeClass('hide');
            $('.btn_hapus').removeClass('hide');
        }
    }
</script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Data Perusahaan
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="<?php echo base_url().'administrator' ?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Company</span>
        </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">
        <div class="col-md-12">
            <div class="row">
            <!-- List Barang -->
                <div class="col-md-6">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold uppercase"> Data Perusahaan</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <label>Silahkan perbaharui jika diperlukan. Data ini akan menjadi data acuan dalam menampilkan data perusahaan di Cetak Jemaah, Perlengkapan, Pembayaran maupun Pengembalian.</label>
                            <?php 
                                    if($this->session->flashdata('update_success')){ 
                                        echo '<div class="alert bg-green-jungle font-white"><i class="fa fa-check"></i> Update data perusahaan berhasil</div>';
                                    }   
            
                                    foreach($company as $row){
                                    echo form_open_multipart('administrator/company/update/'.encrypt($row->company_id));
                            ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="pull-right">
                                                <a onClick="edit_function('edit');" class="btn btn-sm green btn_edit"><i class="fa fa-pencil"></i> Edit Data Perusahaan</a>
                                                <button type="submit" class="btn btn-sm green-jungle btn_save hide"><i class="fa fa-save"></i> Simpan</button>
                                                <a onClick="edit_function('cancel');" class="btn btn-sm yellow-crusta btn_cancel hide"><i class="fa fa-times"></i> Cancel</a>
                                            </div>
                                            <table class="table">
                                                <tr>
                                                    <td>Logo Usaha</td>
                                                    <td class="data_text"><img id="company_thumb" src="<?php echo $row->img_medium ? base_url().$row->img_large : $img_link ?>" alt=""> </td>
                                                    <td class="data_input hide">
                                                        <label class="label label-danger" style="font-size: 13px"><i class="fa fa-info-circle"></i>&nbsp;Optimal Image Resolution : 320 x 183</label>
                                                        <hr>
                                                        <div class="clearfix"></div>
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" >
                                                                <img id="company_thumb" src="<?php echo $row->img_medium ? base_url().$row->img_large : $img_link ?>" alt=""> 
                                                            </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style=" line-height: 10px;"></div>
                                                            <div>
                                                                <span class="btn default btn-file btn-sm">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="perusahaan_logo" id="perusahaan_logo"> </span>
                                                                <a href="javascript:;" class="btn btn-sm red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="120">Nama Usaha</td>
                                                    <td class="data_text"><h5><?php echo $row->nama_usaha ?></h5></td>
                                                    <td class="data_input hide"><input type="text" class="form-control" name="nama_usaha" value="<?php echo $row->nama_usaha ?>" required></td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Brand</td>
                                                    <td class="data_text"><h5><?php echo $row->nama_brand ?></h5></td>
                                                    <td class="data_input hide"><input type="text" class="form-control" name="nama_brand" value="<?php echo $row->nama_brand ?>" required></td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>Kontak</td>
                                                    <td class="data_text"><h5><?php echo $row->kontak ?></h5></td>
                                                    <td class="data_input hide"><input type="text" class="form-control" name="kontak" value="<?php echo $row->kontak ?>" required></td>
                                                </tr>
                                                <tr>
                                                    <td>Alamat</td>
                                                    <td class="data_text"><h5><?php echo $row->alamat ?></h5></td>
                                                    <td class="data_input hide"><input type="text" class="form-control" name="alamat" value="<?php echo $row->alamat ?>" required></td>
                                                </tr>
                                                <tr>
                                                    <td>Kota</td>
                                                    <td class="data_text"><h5><?php echo $row->kota ?></h5></td>
                                                    <td class="data_input hide"><input type="text" class="form-control" name="kota" value="<?php echo $row->kota ?>" required></td>
                                                </tr>
                                                <tr>
                                                    <td>Provinsi</td>
                                                    <td class="data_text"><h5><?php echo $row->provinsi ?></h5></td>
                                                    <td class="data_input hide"><input type="text" class="form-control" name="provinsi" value="<?php echo $row->provinsi ?>" required></td>
                                                </tr>
                                                <tr>
                                                    <td>Keterangan</td>
                                                    <td class="data_text"><h5><?php echo $row->keterangan ?></h5></td>
                                                    <td class="data_input hide"><input type="text" class="form-control" name="keterangan" value="<?php echo $row->keterangan ?>" required></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                            <?php 
                                    echo form_close(); 
                                    }
                            ?>
                        <div class="clearfix"></div>
                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                </div>
            </div>

    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>