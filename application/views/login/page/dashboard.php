<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready( function () {
        $('#table-custom2').DataTable({
            buttons:false
        });
        table = $('#table_keberangkatan').DataTable({ 
            "processing": true, "serverSide": true,"order": [],
            "lengthMenu": [[10, 25, 50, -1], [15, 25, 50, "All"]],
            "ajax": {"url": "<?php echo site_url('administrator/dashboard/paginationTglKeberangkatan/')?>","type": "POST"},
            "order": [[ 6, "asc" ],[ 0, "desc" ]],
            "columnDefs": [
                        {"targets": [0],"className": "dt-center"},
                        {"targets": [4],"className": "dt-center"},
                        {"targets": [5],"className": "dt-center"},
                        {"targets": [6],"orderable": false }
                    ],
            'rowCallback': function(row, data, dataIndex){
               // Get row ID
               var rowsPerKeberangkatanId_selected = data[0];

                if (data[5] == '<a class="font-red"><b>0</b></a>') {
                    $(row).css('backgroundColor', '#eef1f5');
             
                }
            }
        }); 
    });
</script>
<style type="text/css">
    table.dataTable td.sorting_1, table.dataTable td.sorting_2, table.dataTable td.sorting_3, table.dataTable th.sorting_1, table.dataTable th.sorting_2, table.dataTable th.sorting_3{background: #ffffff00!important;}
</style>
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>Dashboard
                    <small>statistik jemaah dan keberangkatan</small>
                </h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="#">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Dashboard</span>
            </li>
        </ul>
        <?php if($show_jemaah){ ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group  btn-group-devided">
                                        <a href="<?php echo base_url().'administrator/dashboard/' ?>" class="btn btn-sm yellow-crusta"><i class="fa fa-arrow-circle-o-left">&nbsp;</i>Kembali</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <h4>
                                    <i class="fa fa-list"></i> Total <b><?php echo count($show_jemaah); ?></b> Jemaah <b><?php echo $show_jemaah[0]->nama_paket ?></b>
                                    Berangkat <b class="font-red"><?php echo date('d-F-Y',strtotime($show_jemaah[0]->tgl_keberangkatan)) ?></b>
                                </h4>
                            </div>
                            
                            <div class="portlet-body">
                                <table class="table table-bordered" id="table-custom">
                                     <thead>
                                        <tr>
                                            <th style="text-align: center"> No. </th>
                                            <th> Nama Jemaah</th>
                                            <th> Member </th>
                                            <th style="text-align: center"> Syarat</th>
                                            <th style="text-align: center"> Petugas </th>
                                            <th style="text-align: center">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if($show_jemaah > 0){ 
                                        $count=0;
                                        foreach($show_jemaah as $row){
                                            $status_jemaah="";
                                            $id            = encrypt($row->pendaftarandetail_id);
                                            $nama_jemaah   = $row->nama_jemaah;

                                            //Cek Status Syarat Jemaah
                                                $cek = $this->md_syarat_status->getSyaratStatusByPendaftarandetailId($row->pendaftarandetail_id);
                                                if($cek){
                                                    $status = count($this->md_syarat_status->getSyaratBelumLengkapByJemaah(decrypt($id)));
                                                    if($status==0){
                                                        $status_syarat = '<button class="btn btn-xs green"><i class="fa fa-check"></i></button>Syarat Lengkap';
                                                    } else {
                                                        $status_syarat = '<button class="btn btn-xs red"><i class="fa fa-times"></i></button>'.$status.' Syarat belum lengkap';
                                                    }
                                                } else {
                                                    $status_syarat = '<button class="btn btn-xs red"><i class="fa fa-exclamation"></i></button> Syarat belum ditentukan';
                                                }
                        
                                            //No registrasi
                                                $no_regis = '<small style="font-size:11px"><a href="'.base_url().'/administrator/pendaftaran/detail_jemaah/'.$id.'">'.$row->no_registrasi.'</a><span class="pull-right" id="label_jemaah_'.$id.'">';
                                                $bc     = AtributJemaah($row,'Business Class');
                                                $sb     = AtributJemaah($row,'Sharing Bed');
                                                $inft   = AtributJemaah($row,'Infant');
                                                $progrv = AtributJemaah($row,'Visa');
                                                $mahrm  = AtributJemaah($row,'Mahram');
                                                $tourled = AtributJemaah($row,'Tour Leader');
                                                $atribut = ''.$no_regis.$bc.$sb.$inft.$mahrm.$progrv.$tourled;


                                            $status_keberangkatan = $row->status_keberangkatan;
                                            $row_style = $status_keberangkatan == 'Sudah Berangkat' ? 'background-color:#F0FFF0' : '';
                                    ?>
                                        <tr id="row_<?php echo $id ?>" style="<?php echo $row_style ?>">
                                            <td style="text-align: center"><?php echo ++$count; ?>.</td>
                                            <td><?php echo $nama_jemaah.'<br>'.$atribut?></td>
                                            <td><?php echo $row->nama_member ?></td>
                                            <td><span><?php echo $status_syarat ?></span></td>
                                            <td style="text-align: center"><?php echo $row->nama_cs.'<br><small style="font-size:11px">'.date('d-M-Y H:i',strtotime($row->tgl_submit)).'</span></small>' ?></td>
                                            <td style="text-align: center"><a href="<?php echo base_url().'/administrator/pendaftaran/detail_jemaah/'.$id?>" class="btn btn-xs green">Detail <i class="fa fa-search"></i></a></td>
                                            
                                        </tr>
                                    <?php } 
                                    } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
        
        <?php } else { ?>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
                        <div class="visual">
                            <i class="fa fa-users"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup" data-value="<?php echo $jml_jamaah_proses?>">0</span>
                            </div>
                            <div class="desc"> On Process </div>
                        </div>
                    </a>
                </div>
                
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a class="dashboard-stat dashboard-stat-v2 green-jungle" href="#">
                        <div class="visual">
                            <i class="icon-plane"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup" data-value="<?php echo $jml_jamaah_berangkat?>"></span> </div>
                            <div class="desc"> Sudah Berangkat </div>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                        <div class="visual">
                            <i class="fa fa-times"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup" data-value="<?php echo $jml_jamaah_cancel?>">0</span></div>
                            <div class="desc"> Cancel </div>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a class="dashboard-stat dashboard-stat-v2 purple" href="#">
                        <div class="visual">
                            <i class="fa fa-building"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup" data-value="<?php echo $jml_kantor_cabang?>"></span> </div>
                            <div class="desc"> Kantor Cabang </div>
                        </div>
                    </a>
                </div>

                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject bold uppercase font-dark">Jemaah Per Tanggal Keberangkatan</span>
                                <span class="caption-helper">Statistik Jemaah</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="tabbable tabbable-custom">
                                    <div class="portlet-body">
                                        
                                        <label class="label bg-blue"><i class="fa fa-info-circle"></i>&nbsp;Data keberangkatan paket akan muncul jika sudah di set tanggal keberangkatan</label>
                                        <div class="clearfix"></div><br>
                                        <table class="table table-condensed table-responsive" id="table_keberangkatan">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Paket</th>
                                                    <th style="text-align: center">Bulan Keberangkatan</th>
                                                    <th style="text-align: center">Tanggal Keberangkatan</th>
                                                    <th style="text-align: center">Kuota</th>
                                                    <th style="text-align: center">Seat Terisi</th>                                                    
                                                    <th style="text-align: center">Sisa Seat</th>
                                                    <th style="text-align: center">Detail</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject bold uppercase font-green">Jemaah Per Bulan Keberangkatan</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <?php include "chart_jamaah.php";?> 
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject bold uppercase font-green">TOP 30 Recent Activities</span>
                                <span class="caption-helper">30 aktifitas terbaru Pengguna system. Klik <a href="<?php echo base_url().'administrator/log' ?>">Di sini</a> untuk melihat semua log</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-condensed order-column table-responsive" id="table-custom2">
                                <thead>
                                    <tr>
                                        <th> No</th>
                                        <th> Pengguna</th>
                                        <th> Keterangan </th>
                                        <th> Tanggal </th>
                                    </tr>
                                </thead>
                               <?php 
                                if($log){
                                    $count=1;
                                    foreach($log as $row){

                                ?>  
                                    <tr>
                                        <td style="text-align: center"><?php echo $count++; ?></td>
                                        <td><?php echo $row->nama_lengkap ?></td>
                                        <td><?php echo $row->keterangan ?></td>
                                        <td><small><i class="fa fa-clock-o"></i>&nbsp; <?php echo date('d-M-Y H:i',strtotime($row->tgl)) ?> | <i>(<?php echo time_passed(strtotime($row->tgl)) ?>)</small></i></td>
                                    </tr>
                                <?php } 
                                }?>
                            </table>
                        </div>
                    </div>
                </div>                    
            </div>
        
        <?php } ?>
    </div>
</div>
