<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<?php 
    $img_link = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA?text=no+image';
    $DOM = [    'id',
                'media_id',
                'nama_album',
                'author',
                'tgl_post',
                'keterangan',
                'alt_teks'
            ];
?>
<script type="text/javascript">
    var dom = [ 'id',
                'media_id',
                'nama_album',
                'author',
                'tgl_post',
                'keterangan',
                'alt_teks'
            ];
    function cek_all(){
        a= $('#nama_album').val();
        c= $('#tgl_post').val();
        d= $('#keterangan').val();
        e= $('#alt_teks').val();
        if (document.getElementById("album").files.length == 0) {
            alert("Please Select Image");
            document.getElementById("album").click();
            exit();
        }
        if(a==''||c==''||d==''||e==''){
            alert("Data belum lengkap");
            exit();
        }
    }
    function empty_form(id_form){
      $('input', '#'+id_form).each(function(){$(this).val("");})
      $('textarea', '#'+id_form).each(function(){$(this).val("");})
      $('select', '#'+id_form).each(function(){$(this).val("");})
    }
    function add_function(task){
        if(task == 'show'){
            empty_form('add-form');
            $('.btn-simpan').show();
            $('.btn-update').hide();            
            $('.img-preview').attr('src','<?php echo $img_link ?>');
            $('#add_modal').modal();
        }
        else  if(task == 'add'){
            cek_all();
            $('#add_modal').modal('hide');
            var file = document.getElementById('album').files[0];
            if (file) {
                var file_data = $('#album').prop('files')[0];  
                var data = new FormData();               
                data.append('album', file_data); 
                data.append('nama_album',$('#nama_album').val());
                data.append('keterangan',$('#keterangan').val());
                data.append('tgl_post',$('#tgl_post').val());
                data.append('alt_teks',$('#alt_teks').val());
            }
            
            swal({
                  title: "Meng-upload File",
                  text: "Klik 'Upload' untuk memulai.",
                  type: "info",
                  closeOnConfirm: false,
                  confirmButtonText: 'Upload',
                  showLoaderOnConfirm: true,
                },
                function(){
                    $.ajax({
                        method: 'POST',
                        data:data,
                        url: '<?php echo base_url() ?>administrator/galeri/add_album',
                        dataType: 'text',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success : function(resp){
                            if(resp == '"add_success"'){
                                swal({
                                      title: "Tambah Album  ",
                                      text: "Penambahan berhasil",
                                      type: "success",
                                      confirmButtonClass: "btn-danger",
                                      confirmButtonText: "Ok",
                                    },
                                    function(){
                                        location.reload();
                                    });
                            }
                            if(resp == '"add_fail"'){
                                swal({
                                      title: "Penambahan Gagal !",
                                      text: "File Tidak Sesuai atau Ukuran Melebihi 2 MB",
                                      type: "warning",
                                      confirmButtonClass: "btn-danger",
                                      confirmButtonText: "Ok",
                                    });
                            }
                        }
                    });
                });
        }
        else  if(task == 'add_photo'){
            galerialbum_id = '<?php echo $galerialbum_id ?>';

            var file = document.getElementById('photo').files[0];
            if (file) {
                var file_data = $('#photo').prop('files')[0];  
                var data = new FormData();               
                data.append('photo', file_data); 
                data.append('galerialbum_id', galerialbum_id); 
            }
            
            swal({
                  title: "Meng-upload File",
                  text: "Klik 'Upload' untuk memulai.",
                  type: "info",
                  closeOnConfirm: false,
                  confirmButtonText: 'Upload',
                  showLoaderOnConfirm: true,
                },
                function(){
                    $.ajax({
                        method: 'POST',
                        data:data,
                        url: '<?php echo base_url() ?>administrator/galeri/add_photo',
                        dataType: 'text',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success : function(resp){
                            if(resp == '"add_success"'){
                                location.reload();
                            }
                            if(resp == '"add_fail"'){
                                swal({
                                      title: "Penambahan Gagal !",
                                      text: "File Tidak Sesuai atau Ukuran Melebihi 2 MB",
                                      type: "warning",
                                      confirmButtonClass: "btn-danger",
                                      confirmButtonText: "Ok",
                                    });
                            }
                        }
                    });
                });
            
        }
    }

    function edit_function(task='',id='',media_id=''){
        if(task == 'edit'){
            empty_form('add-form');
            $('.btn-simpan').hide();
            $('.btn-update').show();
            $('#nama_album').val($('#'+dom[2]+'_edit_'+id).val());
            $('#tgl_post').val($('#'+dom[4]+'_edit_'+id).val());
            $('#keterangan').val($('#'+dom[5]+'_edit_'+id).val());
            $('#alt_teks').val($('#'+dom[6]+'_edit_'+id).val());
            $('.img-preview').attr('src',$('#url_thumbnail_edit_'+id).val());
            $('#galerialbum_id').val($('#'+dom[0]+'_edit_'+id).val());
            $('#media_id').val($('#'+dom[1]+'_edit_'+id).val());
            $('#add_modal').modal();
        }
        if(task == 'update'){
            // data = $('#add-form').serialize();
            $('#add_modal').modal('hide');
            data = new FormData($('#add-form')[0]);
            swal({
                  title: "memperbaharui Album",
                  text: "Klik 'Update' untuk memulai.",
                  type: "info",
                  closeOnConfirm: false,
                  confirmButtonText: 'Upload',
                  showLoaderOnConfirm: true,
                },
                function(){
                $.ajax({
                    method: 'POST',
                    data:data,
                    url: '<?php echo base_url() ?>administrator/galeri/update_album/',
                    cache:false,
                    contentType: false,
                    processData: false,
                    success : function(resp){
                        if(resp == '"update_success"')
                            swal({
                                  title: "Update Album",
                                  text: "Pembaharuan berhasil",
                                  type: "success",
                                  confirmButtonClass: "green-sharp",
                                  confirmButtonText: "Ok",
                                },
                                function(){
                                    location.reload();
                                });
                            
                    }
                });
                });
            
        }
        if(task == 'delete'){
            swal({
              title: "Hapus Data?",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "grey",
              confirmButtonText: "Yes, delete it!",
              closeOnConfirm: false
            },
            function(){
                $.ajax({
                    method: 'POST',
                    url: '<?php echo base_url() ?>administrator/galeri/delete_album/'+id,
                    dataType: 'json',
                    success : function(resp){
                        if(resp == 'update_success')
                            $('#add_modal').modal('hide');
                            swal({
                                  title: "Hapus Data",
                                  text: "Penghapusan berhasil",
                                  type: "success",
                                  confirmButtonClass: "grey",
                                  confirmButtonText: "Ok",
                                },
                                function(){
                                    location.reload();
                                });
                            
                    }
                });
            });
        }
        if(task == 'delete_item'){
            swal({
              title: "Hapus Photo?",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "grey",
              confirmButtonText: "Yes, delete it!",
              closeOnConfirm: false
            },
            function(){
                $.ajax({
                    method: 'POST',
                    data:'media_id='+media_id,
                    url: '<?php echo base_url() ?>administrator/galeri/delete_item/'+id,
                    dataType: 'json',
                    success : function(resp){
                        if(resp == 'update_success')
                            location.reload();
                    }
                });
            });
        }
    }

</script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Galeri Album
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="<?php echo base_url().'administrator' ?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Galeri Album</span>
        </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE BASE CONTENT -->
     
    <div class="row">
        <div class="col-md-12">
            
            <?php if($lihat){ ?>
                <div class="portlet  bordered">     
                    <div style="margin:10px">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>
                            <div>
                                <span class="btn red btn-file">
                                    <span class="fileinput-new"> Select image </span>
                                    <span class="fileinput-exists"> Change </span>
                                    <input type="file" name="photo" id="photo"> </span>

                                <a href="javascript:;" class="btn grey fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                <a href="javascript:;" onClick="add_function('add_photo')" class="btn yellow-crusta fileinput-exists" data-dismiss="fileinput"> Upload </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-dark"></i>
                            <span class="caption-subject font-dark bold uppercase"><?php echo 'Album '.$nama_album ?></span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="mt-element-card mt-element-overlay">

                            
                            <div class="row">
                                <?php 
                                    
                                    $count=1;$i=0;
                                    foreach($item as $row){
                                        $id=encrypt($row->galeriitem_id);
                                        $media_id = $row->media_id;
                                        $link_medium = $this->md_media_detail->getMediaDetailByMediaIdByUkuran($media_id,'medium');
                                        $link_large = $this->md_media_detail->getMediaDetailByMediaIdByUkuran($media_id,'large');
                                        $count++;
                                        if($i == 0) 
                                            echo '<div class="col-md-12">';
                                        $i++;
                                ?>

                                    <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12" >
                                        <div class="mt-card-item" style="margin-bottom: 0px">
                                            <div class="mt-card-avatar mt-overlay-1">
                                                <img src="<?php echo base_url().$link_medium[0]->media_link ?>" style="background: #efefef;padding:10px"/>
                                                <div class="mt-overlay">
                                                    <ul class="mt-info">
                                                        <li>
                                                            <a class="btn default btn-outline" target="_blank" href="<?php echo base_url().$link_large[0]->media_link;?>">
                                                                <i class="icon-magnifier"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a class="btn default btn-outline" target="_blank" onClick="edit_function('delete_item',<?php echo $id?>,<?php echo encrypt($media_id)?>);">
                                                                <i class="fa fa-trash"></i>
                                                            </a>
                                                            
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php if($i == 6) {echo '</div>';$i=0;} } ?>
                            </div>
                        </div>
                    </div>
                </div>
            
            <?php } else { ?>
                <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <button type="button" onClick="add_function('show')" class="btn btn-sm red">Tambah Album &nbsp;<i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-dark"></i>
                            <span class="caption-subject font-dark bold uppercase">Album</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="mt-element-card mt-element-overlay">
                            <div class="row">
                                <?php 
                                    
                                    $count=1;
                                    foreach($album as $row){
                                        $id=encrypt($row->galerialbum_id);
                                        $media_id = $row->media_id;
                                        $nama_album  = $row->nama_album;
                                        $keterangan = $row->keterangan;
                                        $link = $this->md_media_detail->getMediaDetailByMediaIdByUkuran($media_id,'medium');
                                        $md = $this->md_media->getMediaByMediaId($media_id);
                                        $count++;
                                ?>
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="mt-card-item" style="margin-bottom: 0px">
                                            <div class="mt-card-avatar mt-overlay-1">
                                                <img src="<?php echo base_url().$link[0]->media_link ?>" />
                                                <div class="mt-overlay">
                                                    <ul class="mt-info">
                                                        <li>
                                                            <a class="btn default btn-outline" target="_blank" href="<?php echo base_url().$link[0]->media_link;?>">
                                                                <i class="icon-magnifier"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a class="btn default btn-outline" href="<?php echo base_url().'administrator/galeri/lihat_album/'.$id?>">
                                                                Lihat Album
                                                            </a>
                                                        </li>
                                                        
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="mt-card-content">
                                                <h3 class="mt-card-name"><?php echo $nama_album; ?></h3>
                                                <div class="mt-card-social">
                                                    <ul>
                                                        <li>
                                                            <button type="button" onClick="edit_function('edit',<?php echo $count ?>);" class="btn btn-xs yellow-crusta">Edit Album &nbsp;<i class="fa fa-pencil"></i></button>
                                                        </li>
                                                        <li>
                                                            <button type="button" onClick="edit_function('delete',<?php echo $id ?>);" class="btn btn-xs grey">Hapus Album &nbsp;<i class="fa fa-trash"></i></button>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="hide">
                                            <!-- Mencetak Input hidden untuk data detail -->
                                            <input type="text" id="id_edit_<?php echo $count ?>" value="<?php echo $id ?>">
                                            <input type="text" id="media_id_edit_<?php echo $count ?>" value="<?php echo encrypt($media_id) ?>">
                                            <input type="text" id="nama_album_edit_<?php echo $count ?>" value="<?php echo $nama_album ?>">
                                            <input type="text" id="author_edit_<?php echo $count ?>" value="Administrator">
                                            <input type="text" id="tgl_post_edit_<?php echo $count ?>" value="<?php echo $row->tgl_post ?>">
                                            <input type="text" id="keterangan_edit_<?php echo $count ?>" value="<?php echo $keterangan ?>">
                                            <input type="text" id="alt_teks_edit_<?php echo $count ?>" value="<?php echo $row->alt_teks ?>">
                                            <input type="text" id="url_thumbnail_edit_<?php echo $count ?>" value="<?php echo base_url().$link[0]->media_link ?>">
                                        </div>
                                        
                                    </div>
                                            
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<!-- add Modal-->
<div class="modal fade" id="add_modal"  role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-plus font-red"></i>
                    <span class="caption-subject font-red sbold uppercase">Tambah Album</span>
                </div>
            </div>
            <div class="portlet-body form">
                <?php echo form_open('administrator', array( 'id' => 'add-form')); ?>
                    <div class="row">
                        <div class="col-md-5    ">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail">
                                    <img class="img-preview" src="<?php echo $img_link ?>" alt=""> 
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; line-height: 10px;"></div>
                                <div>
                                    <span class="btn default btn-file">
                                        <span class="fileinput-new"> Select image </span>
                                        <span class="fileinput-exists"> Change </span>
                                        <input type="file" name="album" id="album" required> </span>
                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <span class="label label-danger">Note!</span> 
                            <h5><i>Gambar ini digunakan sebagai Thumbnail Album. Optimal Image Width 1024px. Album akan tampil sesuai dengan tanggal posting</i></h5>
                            <span class="label label-danger">Note!</span> 
                            <h5><i>Alt Teks termasuk meningkatkan nilai SEO suatu website. Alt Teks berisi suatu hal terkait gambar yang di-upload </i></h5>
                        </div>
                    </div>             
                    <br>   
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_control_1">Nama Album</label>
                                <input type="text" class="form-control" name="nama_album" id="nama_album">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="form_control_1">Tanggal Post</label>
                            <div class="form-group input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
                                    <input type="text" class="form-control" name="tgl_post" id="tgl_post" readonly="" placeholder="Tanggal Posting">
                                    <span class="input-group-btn">
                                        <button class="btn default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_control_1">Keterangan</label>
                                <textarea type="text" rows="2" class="form-control" name="keterangan" id="keterangan"></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_control_1">Alt Teks</label>
                                <textarea type="text" rows="2" class="form-control" name="alt_teks" id="alt_teks"></textarea>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="galerialbum_id" id="galerialbum_id">
                    <input type="hidden" name="media_id" id="media_id">
                <?php echo form_close();?>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-circle grey">Cancel &nbsp;<i class="fa fa-times"></i></button>            
                <button type="button" onClick="add_function('add')" class="btn-simpan btn btn-circle red">Simpan &nbsp;<i class="fa fa-save"></i></button>
                <button type="button" onClick="edit_function('update')" class="btn-update btn btn-circle yellow-crusta">Simpan Perubahan &nbsp;<i class="fa fa-save"></i></button>
            </div>
        </div>

    </div>
</div>