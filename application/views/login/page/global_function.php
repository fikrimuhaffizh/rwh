<script type="text/javascript">
    function AjaxCRUD(task="",data="",url=""){
        if(task == 'update' || task == 'add'){
            $.ajax({
                method: 'POST',
                data:data,
                url: url,
                cache:false,
                contentType: false,
                processData: false,
                success : function(resp){
                    if(resp == '"update_success"' || resp == '"add_success"'){
                        swal({
                              title: "Berhasil !",
                              text : "Aksi berhasil dilakukan",
                              type: "success",
                              timer: 800,
                              showConfirmButton : false
                            },
                            function(){
                                location.reload();
                        });
                    }
                    else if(resp == '"add_fail"'){
                        swal({
                              title: "Gagal !",
                              text: "File Tidak Sesuai atau Ukuran Melebihi 2 MB",
                              type: "warning",
                              confirmButtonClass: "btn-danger",
                              confirmButtonText: "Ok",
                            });
                    }
                }
            });
        } 
        else if(task == 'edit'){
          var data;
          $.ajax({
                method: 'POST',
                url: url,
                dataType: 'json',
                success : function(resp){
                    edit_function("process_respon","",resp);
                }
            });

        } 
        else if(task == 'detail'){
          var data;
          $.ajax({
                method: 'POST',
                url: url,
                dataType: 'json',
                success : function(resp){
                    detail_function("process_respon","",resp);
                }
            });

        } 
        else if(task == 'delete'){
          swal({
              title: "Hapus Data?",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "grey",
              confirmButtonText: "Yes, delete it!",
              closeOnConfirm: false
            },
            function(){
                $.ajax({
                  method: 'POST',
                  url: url,
                  dataType: 'json',
                  success : function(resp){
                      if(resp == 'update_success')
                        swal({
                              title: "Hapus Data",
                              text: "Penghapusan data berhasil",
                              type: "success",
                              timer: 10,
                            showConfirmButton : false
                          },
                          function(){
                              location.reload();
                        }); 
                  }
              });
            }); 
        }
    }

    function empty_form(id_form){
      
      $('input', '#'+id_form).each(function(){$(this).val("");})
      $('textarea', '#'+id_form).each(function(){$(this).val("");})
      $('select', '#'+id_form).each(function(){$(this).val("");})
    }
</script>