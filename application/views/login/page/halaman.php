<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/backend/global/plugins/ckeditor-full/ckeditor.js"></script>
<script type="text/javascript">
    function cek_all(){
        a = $('#judul').val();
        b = $('#isi').val();
        c = $('#meta_desc').val();
        d = $('#meta_key').val();
        e = $('#link').val();

        if(a=='' ||b=='' ||c=='' ||d==''||e==''){
            alert("Data Halaman Belum Lengkap !");
            return;
        }
    }
    function add_function(task){
        cek_all();
        var data = new FormData();   
        data.append('judul',$('#judul').val());
        data.append('isi',$('#isi').val());
        data.append('meta_desc',$('#meta_desc').val());
        data.append('meta_key',$('#meta_key').val());
        data.append('link',$('#link').val());
        $.ajax({
            method: 'POST',
            data:data,
            url: '<?php echo base_url() ?>administrator/halaman/add',
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            success : function(resp){
                if(resp == 'add_success')
                    swal({
                          title: "Tambah Halaman",
                          text: "Penambahan berhasil",
                          type: "success",
                          confirmButtonClass: "btn-danger",
                          confirmButtonText: "Ok",
                        },
                        function(){
                            window.location.href='<?php echo base_url().'administrator/halaman/'?>';
                        });
                    
            }
        });
    }
    function edit_function(task='',id=''){
        if(task == 'show'){
            window.location.href='<?php echo base_url().'administrator/halaman/show_edit_form/' ?>'+id;
        }
        if(task == 'update'){
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }

            var data = new FormData();   
            data.append('judul',$('#judul').val());
            data.append('isi',$('#isi').val());
            data.append('meta_desc',$('#meta_desc').val());
            data.append('meta_key',$('#meta_key').val());
            data.append('link',$('#link').val());
            $.ajax({
                method: 'POST',
                data:data,
                url: '<?php echo base_url() ?>administrator/halaman/update/'+id,
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                success : function(resp){
                    if(resp == 'update_success')
                        $('#add_modal').modal('hide');
                        swal({
                              title: "Update Halaman",
                              text: "Pembaharuan berhasil",
                              type: "success",
                              confirmButtonClass: "green-sharp",
                              confirmButtonText: "Ok",
                            },
                            function(){
                                location.reload();
                            });
                        
                }
            });
        }
        
        if(task == 'delete'){
            swal({
              title: "Hapus Data?",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "grey",
              confirmButtonText: "Yes, delete it!",
              closeOnConfirm: false
            },
            function(){
                data = $('#edit-form'+id).serialize();
                $.ajax({
                    method: 'POST',
                    data:data,
                    url: '<?php echo base_url() ?>administrator/halaman/delete/'+id,
                    dataType: 'json',
                    success : function(resp){
                        if(resp == 'update_success')
                            $('#add_modal').modal('hide');
                            swal({
                                  title: "Hapus Data",
                                  text: "Penghapusan berhasil",
                                  type: "success",
                                  confirmButtonClass: "grey",
                                  confirmButtonText: "Ok",
                                },
                                function(){
                                    location.reload();
                                });
                            
                    }
                });
            }); 
        }
    }
    CKEDITOR.on('instanceCreated', function(e) {
           e.editor.on('contentDom', function() {
               e.editor.document.on('keyup', function(event) {
                   //update ckeditor fields
                   for (instance in CKEDITOR.instances) {
                       CKEDITOR.instances[instance].updateElement();
                   }
               }
           );
       });
   });     
</script>
<?php 
    $id = "";
    $judul = "";
    $link = "";
    $isi = "";
    $meta_desc = "";
    $meta_key = "";
    $btn_update = FALSE;
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Halaman
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="<?php echo base_url().'administrator' ?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Halaman</span>
        </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE BASE CONTENT -->

    <div class="row">
        <?php if($show_form){ 
                if($fill_form){
                    $id = encrypt($dt_halaman[0]->halaman_id);
                    $judul = $dt_halaman[0]->judul;
                    $isi = $dt_halaman[0]->isi;
                    $tgl_post = $dt_halaman[0]->tgl_post;
                    $author = $dt_halaman[0]->author;
                    $meta_desc = $dt_halaman[0]->meta_desc;
                    $meta_key = $dt_halaman[0]->meta_keyword;
                    $link = $dt_halaman[0]->link_halaman;
                    $btn_update = TRUE;
                }
        ?>
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <button onClick="window.history.back();" class="btn grey pull-left" style="margin-right: 10px"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;Kembali</button>
                        <?php if($fill_form) {?>
                            <div class="caption">
                                <i class="icon-equalizer font-yellow-crusta"></i>
                                <span class="caption-subject font-yellow-crusta bold uppercase">Edit  Halaman</span>
                            </div>
                        <?php } else { ?>
                            <div class="caption">
                                <i class="icon-equalizer font-red"></i>
                                <span class="caption-subject font-red bold uppercase">Tambah  Halaman</span>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <?php echo form_open('administrator', array('id'=>'add-form','class'=>'form-horizontal')); ?>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Judul</label>
                                    <div class="col-md-4">
                                        <div class="input-icon right">
                                            <i class="fa fa-bookmark-o"></i>
                                            <input type="text" class="form-control" name="judul" id="judul" placeholder="Enter text" value="<?php echo $judul ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Link Halaman</label>
                                    <div class="col-md-6">
                                        <div class="input-icon right">
                                            <span style="float:left;"><?php echo base_url()?><input type="text" style="float:right" class="form-control input-small" name="link" id="link" placeholder="Enter text" value="<?php echo $link ?>"></span>
                                        </div>
                                    <span class="help-block"><br><br><i class="font-red">Cukup masukkan <b>1 KATA</b> yang mewakili halaman tersebut</i></span>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Isi</label>
                                    <div class="col-md-10">
                                        <textarea class="ckeditor" name="isi" id="isi"><?php echo $isi ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Meta Keyword</label>
                                    <div class="col-md-4">
                                        <div class="input-icon right">
                                            <i class="fa fa-key"></i>
                                            <input type="text" id="meta_key" class="form-control" value="<?php echo $meta_key ?>" placeholder="Enter text">
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Meta Description</label>
                                    <div class="col-md-4">
                                        <div class="input-icon right">
                                            <i class="fa fa-list"></i>
                                            <textarea id="meta_desc" rows="4" maxlength="150" class="form-control"><?php echo $meta_desc ?></textarea>
                                        </div>
                                        <span class="help-block"> <i>It's help your website SEO</i>. <i class="font-red">Max 150 Character !</i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-2 col-md-4">
                                        <?php if($btn_update){ ?>
                                            <button type="button" onClick="edit_function('update',<?php echo $id ?>);" class="btn yellow-crusta">Update</button>
                                        <?php } else { ?>
                                            <button type="button" onClick="add_function();" class="btn red">Simpan</button>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close();?>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        <?php } else { ?>
            <div class="col-md-12">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <button id="sample_editable_1_new" onClick="window.location.href='<?php echo base_url().'administrator/halaman/show_add_form' ?>'" class="btn btn-sm red">Tambah Halaman
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Managed Halaman</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered  table-checkable order-column" id="table-custom">
                            <thead>
                                <tr>
                                    <th> No. </th>
                                    <th> Judul </th>
                                    <th> Tipe </th>
                                    <th> Link Halaman </th>
                                    <th> Last Update </th>
                                    <th> Aksi </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if($halaman > 0){ 
                                    $count=0;
                                    foreach($halaman as $row){
                                        $id=encrypt($row->halaman_id);
                                        $judul = $row->judul;
                                        $isi = $row->isi;
                                        $tipe = $row->tipe;
                                        $author = $row->author;
                                        $tgl_post = date('d-F-Y H:i:s',strtotime($row->tgl_post));
                                        $meta_desc = $row->meta_desc;
                                        $meta_keyword = $row->meta_keyword;
                                        $link = base_url().$row->link_halaman;
                                    $count++;
                            ?>
                                <tr>
                                    <td> <?php echo $count ?></td>
                                    <td> <?php echo $judul ?> </td>
                                    <td> <?php echo $tipe ?> </td>
                                    <td> <a href="<?php echo $link?>" target="_blank"><?php echo $link ?></a></td>
                                    <td> <i class="fa fa-calendar"></i> &nbsp;<?php echo $tgl_post ?> </td>
                                    <td>
                                        <div class="btn-group" >
                                            <button style="width:100%" class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="javascript:;" onClick="edit_function('show',<?php echo $id ?>,<?php echo $count; ?>);"><i class="fa fa-pencil"></i>&nbsp;Edit</a></li>
                                                <?php if ($tipe == 'Tambahan') { ?>
                                                <li><a href="javascript:;" onClick="edit_function('delete',<?php echo $id ?>);"><i class="fa fa-trash"></i>&nbsp;Hapus</a></li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php } 
                            } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        <?php } ?>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
