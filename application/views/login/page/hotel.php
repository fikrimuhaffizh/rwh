<?php 
     $img_link =base_url().'assets/frontend/img/noimagefound.jpg';
?>
<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<link href="<?php echo base_url()?>assets/backend/css/star-rating.min.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url()?>assets/backend/js/star-rating.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/backend/global/plugins/ckeditor/ckeditor.js"></script>
<?php include 'global_function.php';$login_type = $this->session->userdata('login_type'); ?>
<!-- Rating -->

<script type="text/javascript">


    function add_function(task){
        if(task == 'show'){
            $('.caption-hotel-text').remove();
            $('.caption-hotel').after('<span class="caption-hotel-text"><i class=" icon-plus font-red"></i><span class="caption-subject font-red sbold uppercase"> Tambah hotel</span></span>');
            
            //Reset dahulu formnya
                empty_form('add-form');
                $('#bintang_modal').rating('clear');
                CKEDITOR.instances.deskripsi.setData();
                $('#hotel_thumb').attr('src','<?php echo $img_link ?>');

            $('#edit_button').hide();    
            $('#add_button').show();                  
            $('#add_modal').modal();
        }

        else if(task == 'add'){
            if(check_empty()>0){
                return alert('Form Belum Lengkap');
            }

            $('#add_modal').modal('hide');    
            //Unutk CKEditor harus melalui update Elemenet agar terbaca ketika men-submit form melalui ajax
            CKEDITOR.instances.deskripsi.updateElement();        
            data = new FormData($('#add-form')[0]);
            url = '<?php echo base_url() ?>administrator/hotel/add';
            AjaxCRUD('add',data,url);
        }
    }
    function edit_function(task='',id='',resp=""){

        if(task == 'edit'){
            url = '<?php echo base_url() ?>administrator/hotel/edit/'+id;
            AjaxCRUD('edit',"",url);
        }

        else if(task == 'update'){
            if(check_empty('update')>0){
                return alert('Form Belum Lengkap');
            }
            $('#add_modal').modal('hide');

            CKEDITOR.instances.deskripsi.updateElement();    
            data = new FormData($('#add-form')[0]);
            url = '<?php echo base_url() ?>administrator/hotel/update';
            AjaxCRUD('update',data,url);
        }

        else if(task == 'delete'){
            url = '<?php echo base_url() ?>administrator/hotel/delete/'+id;
            AjaxCRUD('delete',"",url);         
        }

        else if(task == 'process_respon'){
            $('.caption-hotel-text').remove();
            $('.caption-hotel').after('<span class="caption-hotel-text"><i class=" icon-pencil font-yellow-crusta"></i><span class="caption-subject font-yellow-crusta sbold uppercase"> Update hotel</span></span>');
            $('#hotel_id').val(resp.dt[0]['hotel_id']);
            $('#nama_hotel').val(resp.dt[0]['nama_hotel']);
            $('#kota').val(resp.dt[0]['kota']);
            CKEDITOR.instances.deskripsi.setData(resp.dt[0]['deskripsi']);

            $('#bintang_modal').val(resp.dt[0]['bintang']);
            $('#bintang_modal').rating('destroy');
            $('#bintang_modal').rating({
                showCaption : false,
                showClear : false,
                size : 'sm',
                step : 1
            });

            if(resp.dt[0]['media_id']){
                $('#hotel_thumb').attr('src',resp.dt[0]['media_id']);
            }

            $('#edit_button').show();    
            $('#add_button').hide();                     
            $('#add_modal').modal();
        }
    }

    function detail_function(task="",id="",resp=""){
        if(task=='process_respon'){
            $('#detail_nama_hotel').text(resp.dt[0]['nama_hotel']);
            $('#detail_kota').text(resp.dt[0]['kota']);
            $('#detail_deskripsi').html(resp.dt[0]['deskripsi']);

            $('#detail_bintang').val(resp.dt[0]['bintang']);
            $('#detail_bintang').rating('destroy');
            $('#detail_bintang').rating({
                showCaption : false,
                showClear : false,
                size : 'sm',
                step : 1
            });

            var avatar_link = resp.dt[0]['media_id'];
            $('#detail_avatar').attr('src',avatar_link);

            $('#detail_modal').modal();
        }

        else if(task=="detail") {
            url = '<?php echo base_url() ?>administrator/hotel/edit/'+id;
            AjaxCRUD('detail',"",url);
        }
    }

    function check_empty(task=""){
        var empty = 0;
            $('input', '#add-form').each(function(){
                if($(this).val() == "" && $(this).attr('id') && $(this).attr('id')!="hotel_id"){
                    empty++;
                    //Khusus ketika update, bagian hotel_pic tidak perlu dicek
                    if(task=='update' && $(this).attr('id')=='hotel_pic')
                        empty--;
                }
            })
            var x = CKEDITOR.instances.deskripsi.getData();
            if(!x) empty++;

            return empty;
    } 

    $(document).ready(function(){
        CKEDITOR.replace( 'deskripsi' );
    });
</script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Hotel
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="<?php echo base_url().'administrator' ?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Hotel</span>
        </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">
    <!-- List hotel -->
        <div class="col-md-12">
            <div class="table-toolbar">
                <div class="row">
                    <div class="col-md-6">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <button type="button" onClick="add_function('show')" class="btn btn-sm red">Tambah Hotel &nbsp;<i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Managed Hotel</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-checkable" id="table-custom">
                        <thead>
                            <tr>
                                <th> NO</th>
                                <th> Nama Hotel </th>
                                <th> Kota</th>
                                <th> Grade</th>
                                <th> Aksi </th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if($hotel > 0){ 
                                $count=0;
                                foreach($hotel as $row){
                                    $count++;
                                    $id=encrypt($row->hotel_id);
                                    $nama_hotel = $row->nama_hotel;
                                    $kota = $row->kota;
                                    $deskripsi = $row->deskripsi;
                                    $temp = $this->md_media_detail->getMediaDetailByMediaId($row->media_id);
                                    if($temp){
                                        $logo = base_url().$temp[1]->media_link;
                                    }                                 
                                    
                        ?>
                            <tr>
                                <td><?php echo $count; ?></td> 
                                <td><?php echo $nama_hotel ?></td>
                                <td><?php echo $kota ?></td>
                                <td><input type="number" style="display:none" id="bintang_table" class="rating" data-show-clear="false" data-show-caption="false" data-size="xs" data-readOnly="true" value="<?php echo $row->bintang ?>"/></td>
                                <td>
                                    <div class="btn-group" >
                                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="javascript:;" onClick="detail_function('detail',<?php echo $id ?>);" class="btn_detail<?php echo $id ?>"><i class="fa fa-search"></i>&nbsp;Detail</a></li>                                            
                                            <li><a href="javascript:;" onClick="edit_function('edit',<?php echo $id ?>);" class="btn_edit<?php echo $id ?>"><i class="fa fa-pencil"></i>&nbsp;Edit</a></li>
                                            <?php if(!$row->dependency){?>
                                                <li><a href="javascript:;" onClick="edit_function('delete',<?php echo $id ?>);" class="btn_hapus<?php echo $id ?>"><i class="fa fa-trash"></i>&nbsp;Hapus</a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        <?php } 
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<!-- add Modal-->
<div class="modal fade" id="add_modal" role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-hotel"></span>
                </div>
            </div>
            <div class="portlet-body form">
                <?php echo form_open('administrator', array( 'id' => 'add-form')); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <label class="label label-danger" style="font-size: 13px"><i class="fa fa-info-circle"></i> Optimal Image Resolution : 650 x 435</label>
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" >
                                        <img id="hotel_thumb" src="<?php echo $img_link ?>" alt=""> 
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style=" line-height: 10px;"></div>
                                    <div>
                                        <span class="btn default btn-file btn-xs">
                                            <span class="fileinput-new"> Select image </span>
                                            <span class="fileinput-exists"> Change </span>
                                            <input type="file" name="hotel_pic" id="hotel_pic"> </span>
                                        <a href="javascript:;" class="btn btn-xs red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                    </div>
                                </div>
                            </div>  
                            <div class="col-md-6">                  
                                <div class="row">
                                    <div class="form-group ">
                                        <label for="form_control_1">Nama Hotel</label>
                                        <input type="text" class="form-control" name="nama_hotel" id="nama_hotel">
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="form-group ">
                                        <label for="form_control_1">Kota</label>
                                        <input type="text" class="form-control" name="kota" id="kota">
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="form-group">
                                        <label for="form_control_1">Bintang</label>
                                        <input type="number" name="bintang" id="bintang_modal" data-show-clear="false" data-show-caption="false" data-size="sm" class="rating" data-step=1  required />
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <hr>
                            <div class="col-md-12">
                                
                                <div class="row">
                                    <div class="form-group ">
                                        <label class="backdrop-form" style="background: rgb(247, 247, 247);padding:10px;margin:0px;width:100%"><i class="fa fa-warning"></i>&nbsp;Silahkan tulis fitur, bonus, diskon atau info lain terkait hotel ini</label>
                                        <textarea class="form-control" class="ckeditor" name="deskripsi" id="deskripsi" rows="4"></textarea>
                                    </div>
                                </div> 
                            </div>                      
                        </div>
                    </div>
                    <input type="hidden" name="hotel_id" id="hotel_id">
                <?php echo form_close();?>
            </div>
            <br>
            <div class="modal-footer">
                <button type="button" id="add_button" onClick="add_function('add')" class="btn btn-circle red">Simpan &nbsp;<i class="fa fa-save"></i></button>
                <button type="button" id="edit_button" onClick="edit_function('update')" class="btn btn-circle yellow-crusta">Update &nbsp;<i class="fa fa-save"></i></button>
            </div>
        </div>

    </div>
</div>

<!-- detail Modal-->
<div class="modal fade" id="detail_modal" role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog"  style="width:800px">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-search"></i>
                    <span class="header-modal caption-subject sbold uppercase">Detail Hotel</span>
                </div>
            </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-4 col-sm-3">
                            <div class="fileinput-new thumbnail">
                                <img id="detail_avatar" src="<?php echo base_url().'assets/frontend/img/no-avatar.png' ?>" alt=""> 
                            </div> 
                        </div>
                        <div class="col-md-8">
                            <h3><span id="detail_nama_hotel"></span></h3>
                            <table class="table" style="margin-top:15px">
                                <tr>
                                    <td><i class="icon-screen-smartphone"></i>&nbsp; <span>Kota</td>
                                    <td>:</td>
                                    <td><span id="detail_kota"></span></td>
                                </tr>
                                <tr>
                                    <td><i class="icon-star"></i>&nbsp; <span>Grade</td>
                                    <td>:</td>
                                    <td><input type="number" id="detail_bintang" data-show-clear="false" data-show-caption="false" data-size="sm" class="rating" data-step=1  required /></td>
                                </tr>
                                <tr>
                                    <td><i class="icon-map"></i>&nbsp; <span>Deskripsi</td>
                                    <td>:</td>
                                    <td><span id="detail_deskripsi"></span></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-circle grey">Close &nbsp;<i class="fa fa-times"></i></button>            
            </div>
        </div>

    </div>
</div>

