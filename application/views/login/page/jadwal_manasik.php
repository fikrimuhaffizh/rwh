<?php $login_type = $this->session->userdata('login_type'); ?>
<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/backend/global/plugins/ckeditor/ckeditor.js"></script>
<?php include 'global_function.php'; ?>

<script type="text/javascript">
    function add_function(task){
        if(task == 'show'){
            //Empty Form
                empty_form('add-form');
                CKEDITOR.instances.keterangan.setData("");

            $('.manasik_form').hide(); 
            $('#edit_button').hide();    
            $('#add_button').show(); 
            $('#add_modal').modal();
        }
        else if(task == 'add'){
            if(check_empty()>0){
                return alert('Form Belum Lengkap');
            }

            $('#add_modal').modal('hide'); 
            //Unutk CKEditor harus melalui update Elemenet agar terbaca ketika men-submit form melalui ajx
            CKEDITOR.instances.keterangan.updateElement();  
            data = new FormData($('#add-form')[0]);
            url = '<?php echo base_url() ?>administrator/jadwal_manasik/add';
            AjaxCRUD('add',data,url);
        }
    }

    function edit_function(task='',id='',resp=''){
        if(task == 'edit'){
            url = '<?php echo base_url() ?>administrator/jadwal_manasik/edit/'+id;
            AjaxCRUD('edit',"",url); 

        }
        else if(task == 'process_respon'){
            
            $('.manasik_form').show();
            $('#jadwalmanasik_id').val(resp[0]['jadwalmanasik_id']);
            $('#tempat').val(resp[0]['tempat']);
            $('#pembicara').val(resp[0]['pembicara']);
            $('#alamat_tempat').val(resp[0]['alamat_tempat']);
            $('#tgl_manasik').val(resp[0]['tgl_manasik']);
            CKEDITOR.instances.keterangan.setData(resp[0]['keterangan']);

            $('#edit_button').show();    
            $('#add_button').hide();  
            $('#add_modal').modal();
        }
        else if(task == 'update'){
            if(check_empty()>0){
                return alert('Form Belum Lengkap');
            }
            $('#add_modal').modal('hide');
            CKEDITOR.instances.keterangan.updateElement();  
            data = new FormData($('#add-form')[0]);
            url = '<?php echo base_url() ?>administrator/jadwal_manasik/update';
            AjaxCRUD('update',data,url);
        }

        else if(task == 'delete'){
            url = '<?php echo base_url() ?>administrator/jadwal_manasik/delete/'+id;
            AjaxCRUD('delete',"",url);   
        }
    }

    function detail_function(task="",id="",resp=""){
        if(task=='process_respon'){
            $('#detail_tempat').text(resp[0]['tempat']);
            $('#detail_pembicara').text(resp[0]['pembicara']);
            $('#detail_alamat_tempat').text(resp[0]['alamat_tempat']);
            $('#detail_tgl_manasik').text(resp[0]['tgl_manasik']);
            $('#detail_keterangan').html(resp[0]['keterangan']);

            $('#detail_modal').modal();
        }

        else if(task=="detail") {
            url = '<?php echo base_url() ?>administrator/jadwal_manasik/edit/'+id;
            AjaxCRUD('detail',"",url);
        }
    }

    function check_empty(){
        var empty = 0;
            $('input', '#add-form').each(function(){
                if($(this).val() == "" && $(this).attr('id') 
                    && $(this).attr('id')!="jadwalmanasik_id"){
                    empty++;
                }
            })
            $('select', '#add-form').each(function(){
                if($(this).val() == "" && $(this).attr('id')){
                    empty++;
                }
            })
            var x = CKEDITOR.instances.keterangan.getData();
            if(!x) empty++;

            return empty;
    }  

    $(function() {
        $('.manasik_form').hide(); 
        $('#kantor_cabang').change(function(){
            $('.manasik_form').show(); 
        });
        CKEDITOR.replace( 'keterangan' );
    });
</script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Jadwal Manasik
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="<?php echo base_url().'administrator' ?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Jadwal Manasik</span>
        </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE BASE CONTENT -->

    <div class="row">
        <div class="col-md-12">
            <div class="table-toolbar">
            <?php if($login_type != 'Perwakilan Daerah'){?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="btn-group">
                            <button id="sample_editable_1_new" onClick="add_function('show')" class="btn btn-sm red">Tambah Jadwal Manasik
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Managed Jadwal Manasik</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-checkable" id="table-custom">
                        <thead>
                            <tr>
                                <th> NO</th>
                                <th> Kantor Cabang</th>
                                <th> Pembicara</th>
                                <th> Tanggal Manasik </th>
                                <th> Aksi </th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if($jadwal_manasik > 0){ 
                                $count=0;
                                foreach($jadwal_manasik as $row){
                                    $count++;
                                    $id=encrypt($row->jadwalmanasik_id);
                                    $tgl_manasik = date('d-F-Y H:i',strtotime($row->tgl_manasik));
                                    $pembicara = $row->pembicara;
                        ?>
                                <tr>
                                    <td align="center"><?php echo $count; ?></td>
                                    <td><?php echo $row->tempat ?></td>
                                    <td><?php echo $pembicara ?></td>
                                    <td><i class="fa fa-calendar"></i>&nbsp; <?php echo $tgl_manasik ?></td>
                                    <td>
                                        <div class="btn-group" >
                                            <button style="width:100%" class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="javascript:;" onClick="detail_function('detail',<?php echo $id ?>);" class="btn_detail<?php echo $id ?>"><i class="fa fa-search"></i>&nbsp;Detail</a></li>
                                                <li><a href="javascript:;" onClick="edit_function('edit',<?php echo $id ?>);" class="btn_edit<?php echo $id ?>"><i class="fa fa-pencil"></i>&nbsp;Edit</a></li>
                                                <li><a href="javascript:;" onClick="edit_function('delete',<?php echo $id ?>);" class="btn_hapus<?php echo $id ?>"><i class="fa fa-trash"></i>&nbsp;Hapus</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                        <?php } 
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<!-- add Modal-->
<div class="modal fade" id="add_modal" role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-plus font-red"></i>
                    <span class="caption-subject font-red sbold uppercase">Tambah Jadwal Manasik</span>
                </div>
            </div>
            <div class="portlet-body form">
                <?php echo form_open('administrator', array( 'id' => 'add-form')); ?>
                    <div class="col-md-12">
                        <div class="row ">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_control_1">Lokasi</label>
                                    <input type="text" class="form-control" name="tempat" id="tempat">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label input-group date form_datetime bs-datetime" data-date="<?php echo date('Y-m-d H:i:s'); ?>" data-date-format='Y-m-d H:i:s' data-date-start-date="+0d">
                                    <input class="form-control" size="16" type="text" name="tgl_manasik" id="tgl_manasik" value="" readonly  placeholder="Tanggal Manasik">
                                    <span class="input-group-addon">
                                        <button class="btn default date-set" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_control_1">Pembicara</label>
                                    <textarea type="text" rows="2" class="form-control" name="pembicara" id="pembicara"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_control_1">Alamat</label>
                                    <textarea type="text" rows="2" class="form-control" name="alamat_tempat" id="alamat_tempat"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="backdrop-form" style="background: rgb(247, 247, 247);padding:10px;margin:0px;width:100%"><i class="fa fa-warning"></i>&nbsp;Silahkan tulis fitur, bonus, diskon atau info lain terkait jadwal manasik ini</label>
                                    <textarea type="text" class="ckeditor" name="keterangan" id="keterangan"></textarea>
                                </div>
                            </div>
                        </div>                    
                    </div>
                    <input type="hidden" name="jadwalmanasik_id" id="jadwalmanasik_id">
                <?php echo form_close();?>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-circle grey">Cancel &nbsp;<i class="fa fa-times"></i></button>            
                <button type="button" id="add_button" onClick="add_function('add')" class="btn btn-circle red">Simpan &nbsp;<i class="fa fa-save"></i></button>
                <button type="button" id="edit_button" onClick="edit_function('update')" class="btn btn-circle yellow-crusta">Update &nbsp;<i class="fa fa-save"></i></button>
            </div>
        </div>
    </div>
</div>

<!-- detail Modal-->
<div class="modal fade" id="detail_modal" role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="portlet light bordered">
            <div class="portlet-body">
                <div class="caption">
                    <i class="fa fa-search"></i>
                    <span class="header-modal caption-subject sbold uppercase">Detail Jadwal Manasik</span>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h3><span id="detail_nama_lengkap"></span></h3>
                        <table class="table" style="margin-top:15px">                       
                            <tr>
                                <td><i class="icon-tag"></i>&nbsp; <span>Pembicara</td>
                                <td>:</td>
                                <td><span id="detail_pembicara"></span></td>
                            </tr>
                            <tr>
                                <td><i class="icon-calendar"></i>&nbsp; <span>Tanggal Manasik</td>
                                <td>:</td>
                                <td><span id="detail_tgl_manasik"></span></td>
                            </tr>
                            <tr>
                                <td><i class="fa fa-location-arrow"></i>&nbsp; <span>Lokasi</td>
                                <td>:</td>
                                <td><span id="detail_tempat"></span></td>
                            </tr>
                            <tr>
                                <td><i class="icon-map"></i>&nbsp; <span>Alamat</td>
                                <td>:</td>
                                <td><span id="detail_alamat_tempat"></span></td>
                            </tr>
                            <tr>
                                <td><i class="icon-pin"></i>&nbsp; <span>Keterangan</td>
                                <td>:</td>
                                <td><span id="detail_keterangan"></span></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-circle grey">Close &nbsp;<i class="fa fa-times"></i></button>            
            </div>
        </div>

    </div>
</div>



