<?php 
    $login_type = $this->session->userdata('login_type');
?>
<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<?php include 'global_function.php'; ?>
<script type="text/javascript">
    function add_function(task){
        if(task == 'show'){
            //Reset Form
            empty_form('add-form');
            $('#kantorcabang_thumb').attr('src','<?php echo base_url().'assets/frontend/img/no-avatar.png' ?>');
            $('#edit_button').hide();    
            $('#add_button').show();                  
            $('#add_modal').modal();
        }
        else if(task == 'add'){
            if(check_empty()>0){
                return alert('Form Belum Lengkap');
            }

            pass = $('#password').val();
            conf_pass = $('#conf_password').val();
            if(pass && pass!=conf_pass)
                return alert('Konfirmasi Password Salah !');

            $('#add_modal').modal('hide');            
            data = new FormData($('#add-form')[0]);
            url = '<?php echo base_url() ?>administrator/kantor_cabang/add';
            AjaxCRUD('add',data,url);
        }
    }

    function edit_function(task='',id='',resp=''){
        if(task == 'edit'){
            url = '<?php echo base_url() ?>administrator/kantor_cabang/edit/'+id;
            AjaxCRUD('edit',"",url);
        } 
        else if(task == 'process_respon'){

            $('#nama_cabang').addClass('edited').val(resp[0]['nama_cabang']);
            $('#nama_daerah').addClass('edited').val(resp[0]['nama_daerah']);
            $('#provinsi').addClass('edited').val(resp[0]['provinsi']);
            $('#kota').addClass('edited').val(resp[0]['kota']);
            $('#kode_kota').addClass('edited').val(resp[0]['kode_kota']);
            $('#alamat').addClass('edited').val(resp[0]['alamat']);
            $('#telp').addClass('edited').val(resp[0]['telp']);

            if(resp[0]['media_id']){
                $('#kantorcabang_thumb').attr('src',resp[0]['media_id']);
            } else {
                $('#kantorcabang_thumb').attr('src','<?php echo base_url().'assets/frontend/img/no-avatar.png' ?>');
            }

            $('#kantorcabang_id').val(resp[0]['kantorcabang_id']);

            $('#edit_button').show();    
            $('#add_button').hide();                     
            $('#add_modal').modal();
        }
        else if(task == 'update'){
            if(check_empty()>0){
                alert('Form Belum Lengkap');
                return;
            }
            $('#add_modal').modal('hide');
            data = new FormData($('#add-form'+id)[0]);
            url = '<?php echo base_url() ?>administrator/kantor_cabang/update/';
            AjaxCRUD('update',data,url);
        }
        else if(task == 'delete'){
            url = '<?php echo base_url() ?>administrator/kantor_cabang/delete/'+id;
            AjaxCRUD('delete',"",url);   
        }
    }

    function detail_function(task="",id="",resp=""){
        if(task=='process_respon'){
  
            
            if(resp.length == 1){

                $('#detail_nama_cabang').text(resp[0]['nama_cabang']);
                $('#detail_jenis').text(resp[0]['jenis']);
                $('#detail_cabang').text(resp[0]['cabang']);
                $('#detail_provinsi').text(resp[0]['provinsi']);
                $('#detail_nama_daerah').text(resp[0]['nama_daerah']);
                $('#detail_alamat').text(resp[0]['alamat']);
                $('#detail_kota').text(resp[0]['kota']+' - '+ resp[0]['kode_kota']);
                $('#detail_telp').text(resp[0]['telp']);
                $('#detail_tgl_input').text(resp[0]['tgl_input']);

                if(resp[0]['media_id']){
                    var avatar_link = resp[0]['media_id'];
                    $('#detail_avatar').attr('src',avatar_link);
                } else {
                    $('#detail_avatar').attr('src','<?php echo base_url().'assets/frontend/img/no-avatar.png' ?>');
                }

                $('.form-modal').removeClass('form-md-floating-label');
                $('#detail_modal').modal();
            }
        }
        else if(task=="detail") {
            url = '<?php echo base_url() ?>administrator/kantor_cabang/edit/'+id;
            AjaxCRUD('detail',"",url);
        }
    }

    function check_empty(){
        var empty = 0;
            $('input', '#add-form').each(function(){
                if($(this).val() == "" && $(this).attr('id') 
                    && $(this).attr('id')!="kantorcabang_id"
                    && $(this).attr('id')!="kantorcabang_pic"){
                    empty++;
                }
            })
            $('textarea', '#add-form').each(function(){
                if($(this).val() == "" && $(this).attr('id')){
                    empty++;
                }
            })
            return empty;
    } 

</script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Kantor Cabang
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="<?php echo base_url().'administrator' ?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Kantor Cabang</span>
        </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">
        <div class="col-md-12">
            <div class="table-toolbar">
                <div class="row">
                    <div class="col-md-12">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <button type="button" onClick="add_function('show')" class="btn btn-sm red">Tambah Kantor Cabang &nbsp;<i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Managed Kantor Cabang</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered  table-checkable order-column" id="table-custom">
                        <thead>
                            <tr>
                                <th> NO </th>
                                <th> Nama Kantor</th>
                                <th> Kota</th>
                                <th> Daerah</th>
                                <th> Telephone</th>
                                <th> Aksi </th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if($kantor_cabang > 0){ 
                                $count=0;
                                foreach($kantor_cabang as $row){
                                    $count++;
                                    $id=encrypt($row->kantorcabang_id);
                                    $hp = $row->hp;
                                    $telp = $row->telp;
                                    $alamat = $row->alamat;
                                    $email = $row->email;
                                    if(!$telp) 
                                        $telp = "-";

                                    if(!$hp) 
                                        $hp = "-";

                                    if(!$alamat) 
                                        $alamat = "-";

                                    if($row->parent) 
                                        $jenis = "Perwakilan Daerah";
                                    else
                                        $jenis = "Kantor Cabang";     
                        ?>
                            <tr>
                                <td><?php echo $count; ?></td> 
                                <td><?php echo $row->nama_cabang; ?></td>
                                <td><?php echo $row->kode_kota.' - '.$row->kota ?></td>
                                <td><?php echo $row->nama_daerah?></td>
                                <td><?php echo $row->telp ?></td>
                                <td>
                                    <div class="btn-group" >
                                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="javascript:;" onClick="detail_function('detail',<?php echo $id ?>);" ><i class="fa fa-search"></i>&nbsp;Detail</a></li>                           
                                            <li><a href="javascript:;" onClick="edit_function('edit',<?php echo $id ?>);"><i class="fa fa-pencil"></i>&nbsp;Edit</a></li>
                                            <?php if(!$row->dependency){ ?>
                                                <li><a href="javascript:;" onClick="edit_function('delete',<?php echo $id ?>);"><i class="fa fa-trash"></i>&nbsp;Hapus</a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        <?php } 
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<!-- add Modal-->
<div class="modal fade" id="add_modal" role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-plus font-red"></i>
                    <span class="caption-subject font-red sbold uppercase">Tambah kantor_cabang</span>
                </div>
            </div>
            <div class="portlet-body form">
                <?php echo form_open('administrator', array( 'id' => 'add-form')); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <label class="label label-danger" style="font-size: 13px"><i class="fa fa-info-circle"></i>&nbsp;Optimal Image Resolution : 320 x 183</label>
                                    <small><span class="font-red">*</span> Foto hanya Optional</small>
                                    <div class="fileinput-new thumbnail" >
                                        <img id="kantorcabang_thumb" src="<?php echo base_url().'assets/frontend/img/no-avatar.png' ?>" alt=""> 
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style=" line-height: 10px;"></div>
                                    <div>
                                        <span class="btn default btn-file btn-sm">
                                            <span class="fileinput-new"> Select image </span>
                                            <span class="fileinput-exists"> Change </span>
                                            <input type="file" name="kantorcabang_pic" id="kantorcabang_pic"> </span>
                                        <a href="javascript:;" class="btn btn-sm red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                    </div>
                                </div>
                            </div>            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_control_1">Nama Kantor</label>
                                    <input type="text" class="form-control input-sm" name="nama_cabang" id="nama_cabang" placeholder="ex. Kantor Arengka">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_control_1">Provinsi</label>
                                    <input type="text" class="form-control input-sm" name="provinsi" id="provinsi" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="form_control_1">Kota</label>
                                    <input type="text" class="form-control input-sm" name="kota" id="kota" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="form_control_1">Kode Kota</label>
                                    <input type="text" class="form-control input-sm" name="kode_kota" id="kode_kota" maxlength="3"  placeholder="3 Digit" required>
                                </div>
                            </div>                                                      
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_control_1">Nama Daerah</label>
                                    <input type="text" class="form-control input-sm" name="nama_daerah" id="nama_daerah">
                                </div>
                            </div>  
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_control_1">Telephone</label>
                                    <input type="text" class="form-control input-sm" name="telp" id="telp">
                                </div>
                            </div>
                            <div class="col-md-6 div_jenis">
                                <div class="form-group ">
                                    <label for="form_control_1">Jenis</label>
                                    <select class="form-control input-sm" name="jenis" id="jenis">
                                        <option value="Kantor Cabang" selected>Kantor Cabang</option>
                                    </select>

                                </div>
                            </div>
                            <div class="col-md-12" id="div_alamat">
                                <div class="form-group">
                                    <label for="form_control_1">Alamat</label>
                                    <textarea type="text" rows="1" class="form-control input-sm" name="alamat" id="alamat"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="kantorcabang_id" id="kantorcabang_id">
                <?php echo form_close();?>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn grey">Cancel &nbsp;<i class="fa fa-times"></i></button>            
                <button type="button" id="add_button" onClick="add_function('add')" class="btn red">Simpan &nbsp;<i class="fa fa-save"></i></button>
                <button type="button" id="edit_button" onClick="edit_function('update')" class="btn green">Update &nbsp;<i class="fa fa-save"></i></button>
            </div>
        </div>

    </div>
</div>

<!-- detail Kantor Cabang / Perwakilan Daerah Modal-->
<div class="modal fade" id="detail_modal" role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog"  style="width:800px">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-search"></i>
                    <span class="header-modal caption-subject sbold uppercase">Detail Kantor Cabang</span>
                </div>
            </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-4 col-sm-3">
                            <div class="fileinput-new thumbnail">
                                <img id="detail_avatar" src="<?php echo base_url().'assets/frontend/img/no-avatar.png' ?>" alt=""> 
                            </div> 
                        </div>
                        <div class="col-md-8">
                            <h3 style="margin-top:-0px"><span id="detail_nama_cabang"></span></h3>
                            <table class="table" style="margin-top:15px">
                                <tr>
                                    <td width="190"><i class="fa fa-reorder "></i>&nbsp; <span>Jenis</td>
                                    <td>:</td>
                                    <td><span id="detail_jenis"></span></td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-map-marker"></i>&nbsp; <span>Nama Daerah</td>
                                    <td>:</td>
                                    <td><span id="detail_nama_daerah"></span></td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-map-marker"></i>&nbsp; <span>Provinsi</td>
                                    <td>:</td>
                                    <td><span id="detail_provinsi"></span></td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-map-marker"></i>&nbsp; <span>Kota</td>
                                    <td>:</td>
                                    <td><span id="detail_kota"></span></td>
                                </tr>
                                <tr>
                                    <td><i class="icon-home"></i>&nbsp; <span>Alamat</td>
                                    <td>:</td>
                                    <td><span id="detail_alamat"></span></td>
                                </tr>
                                <tr>
                                    <td><i class="icon-call-end"></i>&nbsp; <span>Telephone</td>
                                    <td>:</td>
                                    <td><span id="detail_telp"></span></td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-calendar "></i>&nbsp; <span>Tanggal Bergabung</td>
                                    <td>:</td>
                                    <td><span id="detail_tgl_input"></span></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn grey">Close &nbsp;<i class="fa fa-times"></i></button>            
            </div>
        </div>

    </div>
</div>