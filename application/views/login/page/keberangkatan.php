<?php $login_type = $this->session->userdata('login_type') ?>
<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function tgl_berangkat_function(task="",id="",bln_start="",thn_start=""){
        if(task == 'show'){
            //Custom
                $('#add-form').trigger("reset");
                $('#paketwaktu_id').val(id);
                $('.title-header').text('Set Tanggal Berangkat');
                $('.form-modal').addClass('form-md-floating-label');

                $('.kuota-div').addClass('hide');
                $('.tgl-div').removeClass('hide');
                $('.set-text').text("Silahkan pilih tanggal keberangkatan. :");

                $('.btn_update_kuota').hide();
                $('.btn_update_tourlead').hide();
                $('.btn_update_tgl').show();

            
            var startDate = new Date(thn_start, (bln_start-1), 1);
            var endDate = new Date(thn_start, (bln_start-1), 31);

            $("#set_tgl_berangkat").datepicker({
                multidate: true,
                clearBtn :true,
                format: 'dd/mm/yyyy'
                // startDate: startDate,
                // endDate: endDate
            });

            $('#add_edit_modal').modal();
        } 
        else if(task == 'show_kuota'){
            $('.list_tgl_kuota').remove();
            $.ajax({
                method: 'POST',
                data:data,
                url: '<?php echo base_url() ?>administrator/keberangkatan/getTglBerangkat/'+id,
                dataType: 'json',
                beforeSend: function() {$('.loading').show();},
                success : function(resp){
                    $('.loading').hide();
                    if(resp.data.length > 0){
                        for(var i=0;i<resp.data.length;i++){
                            kuota = resp.kuota[i]>0 ? resp.kuota[i] : ''; 
                            var x = '<tr class="list_tgl_kuota" style="text-align:center"><td><h5 class="font-red"><i class="fa fa-calendar"></i>&nbsp;'+resp.data[i]+'</h5></td>';
                                x+='<td> : </td><td><div class="form-group">';
                                x+='    <div class="col-md-4">';
                                x+='        <input type="hidden" class="form-control"  name="keberangkatan_id[]" value="'+resp.id[i]+'">';
                                x+='        <input type="text" class="form-control" style="width:50px" name="kuota[]" id="kuota[]" value="'+resp.kuota[i]+'" onkeypress="return event.charCode >= 45 && event.charCode <= 57">';
                                x+='    </div> Orang';
                                x+='</div></td></tr>';
                            $('.list_tgl_brgkt').before(x);
                        }
                    } else {
                        var x = '<div class="alert alert-danger">Tanggal belum di set !</div>';
                        $('.list_tgl_brgkt').before(x);

                    }
                }
            });

            //Custom
                $('#kuota').val("");
                $('#paketwaktu_id').val(id);
                $('.title-header').text('Set Kuota');
                $('.form-modal').addClass('form-md-floating-label');

                $('.kuota-div').removeClass('hide');
                $('.tgl-div').addClass('hide');
                $('.set-text').text("Set Kuota per Tanggal Keberangkatan :");

                $('.btn_update_kuota').show();
                $('.btn_update_tourlead').hide();
                $('.btn_update_tgl').hide();           

            $('#add_edit_modal').modal();
        }
        else if(task == 'show_TourLead'){
            $('.list_tgl_kuota').remove();
            $.ajax({
                method: 'POST',
                data:data,
                url: '<?php echo base_url() ?>administrator/keberangkatan/getTglBerangkat/'+id,
                dataType: 'json',
                beforeSend: function() {$('.loading').show();},
                success : function(resp){
                    $('.loading').hide();
                    if(resp.data.length > 0){
                        console.log(resp.data);
                        for(var i=0;i<resp.data.length;i++){
                            keberangkatan_id = resp.id[i];

                            var x ='     <tr class="list_tgl_kuota" style="text-align:center">';
                                x+='          <td><h5 class="font-red"><i class="fa fa-calendar"></i>&nbsp;'+resp.data[i]+'</h5></td>';
                                x+='          <td> : </td>';
                                x+='          <td>';
                                x+='              <div class="form-group">';
                                x+='                  <div class="col-md-4">';
                                x+='                       <input type="hidden" class="form-control"  name="keberangkatan_id[]" value="'+resp.id[i]+'">';
                                x+='                       <div class="input-group select2-bootstrap-append">'
                                x+='                       <select class="form-control select2 input-medium" id="multi_'+resp.id[i]+'" name="'+resp.id[i]+'_tour_leader[]" multiple>';
                                                                x+='<option value="" disabled>Pilih Tour Leader...</option>';
                                                                <?php if($list_tour_leader){ 
                                                                        foreach($list_tour_leader['data'] as $row){
                                                                ?>
                                                                        selected = '';
                                                                        for(var j =0;j<resp.tourleader[keberangkatan_id][0].length;j++){
                                                                            if('<?php echo $row->tourleader_id ?>' == resp.tourleader[keberangkatan_id][0][j]['tourleader_id'])
                                                                                selected = 'selected';
                                                                        }
                                                                        x+='<option value="<?php echo encrypt($row->tourleader_id).'_'.$row->nama ?>" '+selected+'><?php echo $row->nama ?></option>';
                                                                <?php    }
                                                                    }
                                                                ?>
                                x+='                       <span class="input-group-btn"><button class="btn btn-default" type="button" data-select2-open="multi-append"><span class="glyphicon glyphicon-search"></span></button></span></select>';
                                x+='                       </div>'
                                x+='                   </div>';
                                x+='               </div>';
                                x+='          </td>';
                                x+='     </tr>';
                            $('.list_tgl_brgkt').before(x);
                            $('#multi_'+resp.id[i]).select2();
                        }
                    } else {
                        var x = '<div class="alert alert-danger">Tanggal belum di set !</div>';
                        $('.list_tgl_brgkt').before(x);

                    }
                }
            });

            //Custom
                $('#paketwaktu_id').val(id);
                $('.title-header').text('Set Tour Leader');
                $('.set-text').text("Set Tour Leader Tanggal Keberangkatan :");
                $('.form-modal').addClass('form-md-floating-label');

                $('.kuota-div').removeClass('hide');
                $('.tgl-div').addClass('hide');

                $('.btn_update_kuota').hide();
                $('.btn_update_tourlead').show();
                $('.btn_update_tgl').hide();          

            $('#add_edit_modal').modal();
        }        
        else if(task == 'add'){
            id = $('#paketwaktu_id').val();
            $('#add_edit_modal').modal('hide');
            var data = $('#add-form').serialize();
            $.ajax({
                method: 'POST',
                data:data,
                url: '<?php echo base_url() ?>administrator/keberangkatan/add/'+id,
                dataType: 'json',
                success : function(resp){
                    if(resp == 'add_success'){
                        swal({
                              title: "Set Tanggal dan Kuota  ",
                              text: "Tanggal dan Kuota Keberangkatan Disimpan",
                              type: "success",
                              showConfirmButton : false,
                              timer : 800
                            },
                            function(){
                                location.reload();
                            });
                    }
                }
            });
        }
        else if(task == 'add_kuota'){
            $('#add_edit_modal').modal('hide');
            var data = $('#add-form').serialize();
            $.ajax({
                method: 'POST',
                data:data,
                url: '<?php echo base_url() ?>administrator/keberangkatan/add_kuota/',
                dataType: 'json',
                success : function(resp){
                    if(resp == 'update_success'){
                        swal({
                              title: "Set Kuota",
                              text: "Kuota Keberangkatan Disimpan",
                              type: "success",
                              showConfirmButton : false,
                              timer : 800
                            },
                            function(){
                                location.reload();
                            });
                    }
                }
            });
        }
        else if(task == 'add_tourLead'){
            $('#add_edit_modal').modal('hide');
            var data = $('#add-form').serialize();
            $.ajax({
                method: 'POST',
                data:data,
                url: '<?php echo base_url() ?>administrator/keberangkatan/add_tourLead/',
                dataType: 'json',
                success : function(resp){
                    if(resp == 'update_success'){
                        swal({
                              title: "Set Tour Leader",
                              text: "Tour Leader Keberangkatan Disimpan",
                              type: "success",
                              showConfirmButton : false,
                              timer : 800
                            },
                            function(){
                                location.reload();
                            });
                    }
                }
            });
        }        
        else if(task == 'SetStatusKeberangkatanPerTanggal'){
            $('#setStatusKeberangkatanModal').modal('hide');
            var data = $('#setStatusKeberangatan-form').serialize();
            $.ajax({
                method: 'POST',
                data:data,
                url: '<?php echo base_url() ?>administrator/keberangkatan/setStatusKeberangkatanPerTanggal/',
                dataType: 'json',
                success : function(resp){
                    if(resp == 'update_success'){
                        swal({
                              title: "Set Status Keberangkatan  ",
                              text: "Status Keberangkatan Disimpan",
                              type: "success",
                              timer:800,
                              showConfirmButton : false,
                            },
                            function(){
                                location.reload();
                            });
                    }
                }
            });
        }
       else if(task == 'SetKeberangkatanPerTanggal'){
            $('#setKeberangkatanModal').modal('hide');
            var data = $('#setKeberangkatan-form').serialize();
            $.ajax({
                method: 'POST',
                data:data,
                url: '<?php echo base_url() ?>administrator/keberangkatan/SetKeberangkatanPerTanggal/',
                dataType: 'json',
                success : function(resp){
                    if(resp == 'update_success'){
                        swal({
                              title: "Set Status Keberangkatan  ",
                              text: "Status Keberangkatan Disimpan",
                              type: "success",
                              timer:800,
                              showConfirmButton : false,
                            },
                            function(){
                                location.reload();
                            });
                    }
                }
            });
        }
    }
    function show_function(task="",id=""){
        if(task=='detailJemaah'){
            $.ajax({
                method: 'POST',
                url: '<?php echo base_url() ?>administrator/keberangkatan/getDetailJemaah/'+id,
                dataType: 'json',
                success : function(resp){
                    if(resp){
                        var pembayaran = resp['pembayaran'];

                        console.log(pembayaran);
                        for(var i=0;i<pembayaran.length;i++){
                            console.log(pembayaran[i]['jumlah_bayar']);
                        }

                        var syarat = resp['syarat'];
                        for(var i=0;i<syarat.length;i++){
                            console.log(syarat[i]['persyaratan']+ ' : '+syarat[i]['status_syarat']);
                        }
                    } 
                }
            });
        } 
    }
    function setTglBerangkat(id=""){
        $.ajax({
                method: 'POST',
                url: '<?php echo base_url() ?>administrator/keberangkatan/setTglBerangkat/'+id,
                data: 'keberangkatan_id='+$('#tgl_berangkat_'+id).val(),
                dataType: 'json',
                success : function(resp){
                    if(resp == 'update_success'){
                        swal({
                              title: "Set Tanggal Berhasil",
                              type: "success",
                              timer: 1000,
                              showConfirmButton : false
                            });
                    }
                }
            });
    }
    function setStatusKeberangkatan(id=""){
        var a = $('#status_keberangkatan_'+id).val();
        $.ajax({
                method: 'POST',
                url: '<?php echo base_url() ?>administrator/keberangkatan/setStatusKeberangkatan/'+id,
                data: {
                    status_keberangkatan : a
                },
                dataType: 'json',
                success : function(resp){
                    if(resp == 'update_success'){
                    swal({
                          title: "Status Keberangkatan diperbaharui",
                          type: "success",
                          timer: 1000,
                          showConfirmButton : false
                        });

                    if(a=='Sudah Berangkat')
                        $('#row_'+id).attr('style','background-color: #F0FFF0');
                    if(a=='Dalam Proses')
                        $('#row_'+id).attr('style','background-color: #FFFFFF');
                    }
                }
            });
    }
    $(function() {
        // $('#table_custom').DataTable();
        $('#list_jemaah').hide(); 
        $('#multi_tgl_berangkat').change(function(){
            if($('#multi_tgl_berangkat').val()!="") {
                $('#list_jemaah').show(); 
            } else {
                $('#list_jemaah').hide(); 
            } 
        });
    });
</script>

<style type="text/css">
    .tr-center{text-align: center}
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Keberangkatan Jemaah
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>

        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo base_url().'administrator' ?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Keberangkatan Jemaah</span>
            </li>
        </ul>
            <?php if($show_jemaah) {?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group  btn-group-devided">
                                        <a href="<?php echo base_url().'administrator/keberangkatan/' ?>" class="btn btn-sm yellow-crusta"><i class="fa fa-arrow-circle-o-left">&nbsp;</i>Kembali</a>
                                        <?php if(count($jemaah)>0){ ?>
                                        <a href="javascript:;" data-toggle="modal" data-target="#setKeberangkatanModal" class="btn btn-sm red"><i class="fa fa-plus"></i>&nbsp;Set Tanggal Keberangkatan Multi Jemaah</a>
                                        <a href="javascript:;" data-toggle="modal" data-target="#setStatusKeberangkatanModal" class="btn btn-sm green-jungle"><i class="fa fa-plane"></i>&nbsp;Set Status Keberangkatan Multi Jemaah</a>
                                    <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <h4>
                                    <i class="fa fa-list"></i> Total <b><?php echo count($jemaah); ?></b> Jemaah <b><?php echo $keberangkatan[0]->nama_paket ?></b>. 
                                    Berangkat dari <?php echo '<b>'.$keberangkatan[0]->nama_kota.'</b> pada bulan <b>'.$bulan[$keberangkatan[0]->bulan_berangkat].' - '.$keberangkatan[0]->tahun_berangkat.'</b>' ?>
                                </h4>
                            </div>
                            
                            <div class="portlet-body">
                                <table class="table">
                                     <thead>
                                        <tr>
                                            <th style="text-align: center"> No. </th>
                                            <th> Nama Jemaah</th>
                                            <th> Member </th>
                                            <th style="text-align: center"> Syarat</th>
                                            <th style="text-align: center"> Pembayaran</th>
                                            <th style="text-align: center"> Tanggal Keberangkatan </th>
                                            <th style="text-align: center">Status Keberangkatan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if($jemaah > 0){ 
                                        $count=0;
                                        foreach($jemaah as $row){
                                            $status_jemaah="";
                                            $id            = encrypt($row->pendaftarandetail_id);
                                            $nama_jemaah   = $row->nama_jemaah;
                                            $keberangkatan =  $bulan[$row->bulan_berangkat].' '.$row->tahun_berangkat;
                                            $nama_paket    = $row->nama_paket;

                                            //Cek Status Syarat Jemaah
                                                $cek = $this->md_syarat_status->getSyaratStatusByPendaftarandetailId($row->pendaftarandetail_id);
                                                if($cek){
                                                    $status = count($this->md_syarat_status->getSyaratBelumLengkapByJemaah(decrypt($id)));
                                                    if($status==0){
                                                        $status_syarat = '<button class="btn btn-xs green"><i class="fa fa-check"></i></button>Syarat Lengkap';
                                                    } else {
                                                        $status_syarat = '<button class="btn btn-xs red"><i class="fa fa-times"></i></button>'.$status.' Syarat belum lengkap';
                                                    }
                                                } else {
                                                    $status_syarat = '<button class="btn btn-xs red"><i class="fa fa-exclamation"></i></button> Syarat belum ditentukan';
                                                }

                                            //Cek Status Pembayaran Jemaah
                                       
                                                $total_bayar = $row->total_pembayaran;
                                                //Cek infant, business class, dan sharing bed
                                                    $infant                 = $row->infant ? $row->infant : 0;
                                                    $sharing_bed            = $row->sharing_bed ? $row->sharing_bed : 0;
                                                    $business_class         = $row->business_class ? $row->business_class : 0;
                                                    $harga_setelah_potongan = $row->harus_bayar-($infant+$sharing_bed)+$business_class;

                                                if($harga_setelah_potongan == $total_bayar)
                                                    $status_pembayaran = '<b class="font-green-jungle">Lunas</b>';
                                                else if($harga_setelah_potongan < $total_bayar)
                                                    $status_pembayaran = '<b class="font-red-mint ">Melebihi Harga Paket !</b>';
                                                else 
                                                    $status_pembayaran = '<b class="font-grey">Belum Lunas</b>';

                                                $harga_bayar = '
                                                                <small>Harus Bayar : '.number_format($harga_setelah_potongan).'</small><br>
                                                                <small>Sudah Bayar : '.number_format($total_bayar).'</small><br>
                                                            ';
                        
                                            //No registrasi
                                                $no_regis = '<small style="font-size:11px">'.$row->no_registrasi.'<span class="pull-right" id="label_jemaah_'.$id.'">';
                                                $bc     = AtributJemaah($row,'Business Class');
                                                $sb     = AtributJemaah($row,'Sharing Bed');
                                                $inft   = AtributJemaah($row,'Infant');
                                                $progrv = AtributJemaah($row,'Visa');
                                                $mahrm  = AtributJemaah($row,'Mahram');


                                            $status_keberangkatan = $row->status_keberangkatan;
                                            $row_style = $status_keberangkatan == 'Sudah Berangkat' ? 'background-color:#F0FFF0' : '';
                                    ?>
                                        <tr id="row_<?php echo $id ?>" style="<?php echo $row_style ?>">
                                            <td style="text-align: center"><?php echo ++$count; ?>.</td>
                                            <td><?php echo $nama_jemaah.'<br>'.$no_regis.$bc.$sb.$inft.$mahrm.$progrv?></td>
                                            <td><?php echo $row->nama_member ?></td>
                                            <td style="width:220px"><span><?php echo $status_syarat ?></span></td>
                                            <td style="width:220px;text-align: center"><?php echo $harga_bayar ?><a href="<?php echo base_url().'administrator/pembayaran/detail_jemaah/'.$id.'/pembayaran' ?>"><span><?php echo $status_pembayaran ?></span></a></td>
                                            <td style="width:200px;text-align: center">
                                                <?php if($tgl_berangkat){ ?>
                                                    <select class=" input-sm input-small " onchange="setTglBerangkat(<?php echo $id ?>)" id="tgl_berangkat_<?php echo $id ?>">
                                                        <option value="" readonly="">Pilih Tanggal Keberangkatan ...</option>
                                                        <?php foreach($tgl_berangkat as $tgl){
                                                            $select = encrypt($row->keberangkatan_id) == $tgl->keberangkatan_id ? 'selected' : '';
                                                            echo '<option value="'.$tgl->keberangkatan_id.'"'.$select.'>'.$tgl->tgl_keberangkatan.'</option>';
                                                        } ?>
                                                    </select> 
                                                <?php } else { ?>
                                                    <span class="label label-danger">Belum dijadwalkan</span>
                                                <?php } ?>
                                            </td>
                                            <td style="width:200px;text-align: center">
                                                <select class=" input-sm input-small" onchange="setStatusKeberangkatan(<?php echo $id ?>)" id="status_keberangkatan_<?php echo $id ?>">
                                                    <option value="Dalam Proses" <?php if($row->status_keberangkatan=='Dalam Proses'){echo 'selected';}?>>Dalam Proses</option>
                                                    <option value="Sudah Berangkat" <?php if($row->status_keberangkatan=='Sudah Berangkat'){echo 'selected';}?>>Sudah Berangkat</option>
                                                </select>
                                            </td>
                                            
                                        </tr>
                                    <?php } 
                                    } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
            
            <?php } else { ?>
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject bold uppercase"> Managed Keberangkatan Jemaah</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table-custom">
                                    <thead>
                                        <tr>
                                            <th> No. </th>
                                            <th> Nama Paket </th>
                                            <th> Jemaah </th>
                                            <th> Keberangkatan </th>
                                            <th> Tgl Berangkat </th>
                                            <th> Kuota</th>
                                            <th width="100"> Tour Leader </th>
                                            <th> Aksi </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if($list_paket > 0){ 
                                            $count=0;
                                            foreach($list_paket as $row){
                                                $status_tgl = "";
                                                $kuota="";
                                                $tlt = "";
                                                $id = encrypt($row->paketwaktu_id);
                                                $keberangkatan =  $bulan[$row->bulan_berangkat].' '.$row->tahun_berangkat;
                                                $kota = $row->nama_kota;
                                                $count++;

                                                $tgl = $this->md_keberangkatan->getKeberangkatanByPaketWaktuId($row->paketwaktu_id);
                                                if(!$tgl){
                                                    $status_tgl = '<span class="font-red" style="font-size:12px">Belum di jadwalkan</span>';
                                                    $kuota      = '<span class="font-red" style="font-size:12px">-</span>';
                                                    $tlt        = '<span class="font-red" style="font-size:12px">-</span>';
                                                } else {
                                                    foreach($tgl as $row2){
                                                        $status_tgl = $status_tgl.'<b  style="font-size: 12px;">'.date('d-M-Y',strtotime($row2->tgl_keberangkatan)).'</b><br><br>';
                                                        
                                                        //List Kuota
                                                        $kuota =$kuota.'<b  style="font-size: 12px;">'.date('d-M-Y',strtotime($row2->tgl_keberangkatan)).'</b>'.': <br>';
                                                        if($row2->kuota>0){
                                                            $kuota = $kuota.'<small class="bg-default">'.$row2->kuota.' Orang</small><br>';
                                                        } else {$kuota = $kuota.'-<br>';}


                                                        //Ambil list tourleader
                                                            $z = $this->md_keberangkatan_tour->getKeberangkatanTourByKeberangkatanId($row2->keberangkatan_id);
                                                            $tlt =$tlt.'<b  style="font-size: 12px;">'.date('d-M-Y',strtotime($row2->tgl_keberangkatan)).'</b>'.': <br>';
                                                            if($z){
                                                                $temp = '';
                                                                foreach($z as $row3){
                                                                    $temp = $temp.'<small class="bg-default ">'.$row3->nama.'</small>,';
                                                                }
                                                                $temp = substr_replace($temp, "",-1);
                                                                $tlt = $tlt.$temp.'<br>';
                                                            } else {
                                                                $tlt =  $tlt.'-<br>';
                                                            }
                                                          
                                                    }
                                                }

                                    ?>
                                        <tr>
                                            <td><?php echo $count; ?>. </td>
                                            <td><?php echo $row->nama_paket?></td>
                                            <td style="text-align:center;"><?php echo $row->total_jemaah == 0 ? '-' : $row->total_jemaah.' Orang' ?></td>
                                            <td><span><i class="fa fa-plane"></i>&nbsp;<?php echo $kota.' - '.$keberangkatan ?></span></td>
                                            <td>
                                                <button type="button" onClick="tgl_berangkat_function('show',<?php echo $id?>,'<?php echo $row->bulan_berangkat ?>','<?php echo $row->tahun_berangkat ?>')" class="btn btn-xs red btn_detail<?php echo $id ?>">Set Tanggal&nbsp;<i class="fa fa-pencil"></i></button>
                                                <br>
                                                <?php echo $status_tgl; ?>
                                            </td>
                                            <td>
                                                <button type="button" onClick="tgl_berangkat_function('show_kuota',<?php echo $id?>)" class="btn btn-xs grey btn_detail<?php echo $id ?>">Set Kuota &nbsp;<i class="fa fa-pencil"></i></button>
                                                <br>
                                                <?php echo $kuota; ?>
                                            </td>
                                            <td>
                                                <button type="button" onClick="tgl_berangkat_function('show_TourLead',<?php echo $id?>)" class="btn btn-xs blue btn_detail<?php echo $id ?>">Set Tour Leader &nbsp;<i class="fa fa-pencil"></i></button>
                                                <br>
                                                <?php echo $tlt;?>
                                            </td>
                                            <td>
                                                <div class="actions">
                                                    <div class="btn-group">
                                                        <button type="button" onClick="window.location='<?php echo base_url().'administrator/keberangkatan/show_jemaah/'.$id ?>'" class="btn btn-xs green btn_detail<?php echo $id ?>">Cek Jemaah &nbsp;<i class="fa fa-search"></i></button>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php } 
                                    } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
            
            <?php } ?>

        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<!-- add Tanggal Keberangkatan Modal-->
<div class="modal fade" id="add_edit_modal"  role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-users"></i>
                    <span class="title-header header-modal caption-subject sbold uppercase"></span>
                </div>
            </div>
            <div class="portlet-body form">
                <?php echo form_open('administrator', array( 'id' => 'add-form')); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="set-text"></h4>
                            <div class="loading pull-left" style="display: none">
                                <img src="<?php echo base_url().'assets/frontend/img/AjaxLoader.gif' ?>" alt="">
                                <span>Mohon Tunggu...</span>
                            </div>
                            <div class="tgl-div col-md-12 hide">
                                <h5><b class="font-red">Perhatian !</b><br>Data Kuota dan Tanggal Keberangkatan setiap Jemaah terkait Paket Umroh yang dipilih akan terhapus jika <span class="font-red">sebelumnya</span> sudah pernah melakukan set tanggal keberangkatan per jemaah.</h5>
                                <hr>
                                <div class="form-modal form-group form-md-line-input input-group date" data-date-format="dd-mm-yyyy ">
                                    <input type="text" class="form-control" name="tgl_berangkat" id="set_tgl_berangkat" readonly="" placeholder="dd-mm-yyyy">
                                    <span class="input-group-addon">
                                        <button class="btn btn-xs default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                    <label for="form_control_1" style="color: #e73d4a">Tanggal bisa dipilih lebih dari satu</label>
                                </div>
                            </div>
                            <div class=" kuota-div col-md-12 hide">
                                <table class="table" >
                                    <tr class="list_tgl_brgkt"></tr>
                                </table>
                            </div>
                        </div>
                    </div>
                <?php echo form_close();?>
            </div>
            <div class="modal-footer">
                <input type="hidden" id="paketwaktu_id">
                <button type="button" data-dismiss="modal" class="btn btn-sm btn-circle grey">Cancel &nbsp;<i class="fa fa-times"></i></button>   
                <button type="button" onClick="tgl_berangkat_function('add_tourLead')" class=" btn_update_tourlead btn btn-sm  btn-circle blue">Simpan Tour Leader&nbsp;<i class="fa fa-save"></i></button>           
                <button type="button" onClick="tgl_berangkat_function('add_kuota')" class="btn_update_kuota btn btn-sm btn-circle grey">Simpan Kuota&nbsp;<i class="fa fa-save"></i></button>           
                <button type="button" onClick="tgl_berangkat_function('add')" class="btn_update_tgl btn btn-sm  btn-circle red">Simpan Tanggal&nbsp;<i class="fa fa-save"></i></button>
            </div>
        </div>
    </div>
</div>

<!-- set Tanggal Keberangkatan Modal-->
<div class="modal fade" id="setKeberangkatanModal"  role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-calendar"></i>
                    <span class="header-modal caption-subject sbold uppercase">Set Tanggal Keberangkatan Jemaah2</span>
                </div>
            </div>
            <?php if($tgl_berangkat){ ?>
            <div class="portlet-body form">
                <?php echo form_open('administrator', array( 'id' => 'setKeberangkatan-form')); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="set-text">Pilih Tanggal : </h4>
                            <div class="col-md-12">
                                    <select class="form-control multi_tgl_berangkat" id="multi_tgl_berangkat" name="multi_tgl_berangkat">
                                        <option value="" selected>... Pilih Tanggal Keberangkatan ...</option>
                                        <?php foreach($tgl_berangkat as $tgl){
                                            echo '<option value="'.$tgl->keberangkatan_id.'">'.$tgl->tgl_keberangkatan.'</option>';
                                        } ?>
                                    </select>
                            </div>
                            <div class="clearfix"></div>
                            <br>
                            <div class="col-md-12" id="list_jemaah">
                                <h4 class="set-text">Pilih Jemaah : </h4>
                                <div class="form-group">
                                    <select multiple="multiple" class="multi-select" id="my_multi_select1" name="multi_jemaah[]">

                                        <?php 
                                            foreach($jemaah as $jmh){
                                                echo '<option value="'.encrypt($jmh->pendaftarandetail_id).'">'.$jmh->nama_jemaah.'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                               <h5 class="font-red"><b>*</b> Klik jemaah kiri untuk memilih jemaah<br><b>*</b> Klik jemaah kanan untuk membatalkan jemaah yang sudah dipilih</h5></b>
                            </div>
                            <input type="hidden" id="paketwaktu_id">
                        </div>
                    </div>
                <?php echo form_close();?>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn grey">Cancel &nbsp;<i class="fa fa-times"></i></button>            
                <button type="button" onclick="tgl_berangkat_function('SetKeberangkatanPerTanggal')" class="btn red">Simpan &nbsp;<i class="fa fa-save"></i></button>           
            </div>
            <?php } else { ?>
                <div class="alert alert-danger">Tanggal Keberangkatan Paket belum dijadwalkan</div>
            <?php } ?>
        </div>
    </div>
</div>

<!-- set Status Keberangkatan Modal-->
<div class="modal fade" id="setStatusKeberangkatanModal"  role="basic" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="portlet light bordered" style="width:500px">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-plane"></i>
                    <span class="header-modal caption-subject sbold uppercase">Set Status Keberangkatan Jemaah</span>
                </div>
            </div>
            <?php if($tgl_berangkat){?>
                <div class="portlet-body form">
                    <?php echo form_open('administrator/keberangkatan/SetStatusKeberangkatanPerTanggal', array( 'id' => 'setStatusKeberangatan-form')); ?>
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="set-text">Pilih Tanggal Keberangkatan Jemaah : </h4>
                                <div class="form-group">
                                    <select multiple="multiple" class="multi-select" id="my_multi_select2" name="multi_tgl[]" required>
                                        <?php 

                                            foreach($tgl_berangkat as $tglb){
                                                echo '<option value="'.$tglb->keberangkatan_id.'_'.$tglb->tgl_keberangkatan.'">'.$tglb->tgl_keberangkatan.'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h4 class="set-text">Pilih Status yang akan ditetapkan : </h4>
                                    <div class="form-group">
                                        <select class="form-control input-medium input-md" name="status_keberangkatan" required>
                                            <option value="Dalam Proses">Dalam Proses</option>
                                            <option value="Sudah Berangkat">Sudah Berangkat</option>
                                        </select>
                                    </div>
                                <input type="hidden" id="paketwaktu_id" value="<?php echo $_paketwaktu_id ?>">
                            </div>
                        </div>
                <div class="clearfix"></div><br>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn grey">Cancel &nbsp;<i class="fa fa-times"></i></button>            
                    <button type="button" onClick="tgl_berangkat_function('SetStatusKeberangkatanPerTanggal')" class="btn_update_kuota btn red">Simpan &nbsp;<i class="fa fa-save"></i></button>           
                </div>
            <?php echo form_close();?>
            <?php } else { ?>
                <div class="alert alert-danger">Tanggal Keberangkatan Paket belum dijadwalkan</div>
            <?php } ?>
        </div>
    </div>
</div>
