<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<?php include 'global_function.php';$login_type = $this->session->userdata('login_type'); ?>

<script type="text/javascript">
    function add_function(task){
        if(task == 'show'){
            $('#add_modal').modal();
        }
        if(task == 'add'){
            $('#add_modal').modal('hide');
            data = new FormData($('#add-form')[0]);
            url = '<?php echo base_url() ?>administrator/kelas/add';
            AjaxCRUD('add',data,url);
        }
    }

    function edit_function(task='',id=''){
        if(task == 'edit'){
            $('.data_input'+id).removeClass('hide');
            $('.data_text'+id).addClass('hide');

            $('.btn_save'+id).removeClass('hide');
            $('.btn_cancel'+id).removeClass('hide');

            $('.btn_edit'+id).addClass('hide');
            $('.btn_hapus'+id).addClass('hide');
        }

        if(task == 'cancel'){
            $('.data_input'+id).addClass('hide');
            $('.data_text'+id).removeClass('hide');

            $('.btn_save'+id).addClass('hide');
            $('.btn_cancel'+id).addClass('hide');

            $('.btn_edit'+id).removeClass('hide');
            $('.btn_hapus'+id).removeClass('hide');
        }

        if(task == 'update'){
            data = new FormData($('#edit-form'+id)[0]);
            url = '<?php echo base_url() ?>administrator/kelas/update/'+id;
            AjaxCRUD('update',data,url);
        }

        if(task == 'delete'){
            url = '<?php echo base_url() ?>administrator/kelas/delete/'+id;
            AjaxCRUD('delete',"",url);
        }
    }

</script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Kelas Keberangkatan
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="<?php echo base_url().'administrator' ?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Kelas</span>
        </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">
        <div class="col-md-12">
            <div class="row">
            
            <!-- Edit Form -->
                <div class="update_form col-md-6 hide">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-pencil font-yellow"></i>
                                <span class="caption-subject font-yellow sbold uppercase">Edit Kelas</span>
                            </div>
                            <div class="actions">
                                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <form role="form">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group form-md-line-input form-md-floating-label">
                                    <input type="text" class="form-control" name="kelas" id="kelas">
                                    <label for="form_control_1">Nama Kelas</label>
                                </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            <!-- List Kelas -->
                <div class="col-md-12">
                <?php if($login_type == 'Kantor Pusat'){ ?>
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <button id="sample_editable_1_new" onClick="add_function('show')" class="btn btn-sm red">Tambah Kelas
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold uppercase"> Managed Kelas</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                        
                        <table class="table table-striped table-bordered  table-checkable order-column" id="table-custom">
                            <thead>
                                <tr>
                                    <th style="text-align:center;"> NO</th>
                                    <th style="text-align:center"> Kelas </th>
                                    <th style="text-align:center"> Jumlah Sekamar</th>
                                    <th> Aksi </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                $count=1;
                                foreach($kelas as $kls){
                                    $id=encrypt($kls->kelas_id);
                                    $kelas = $kls->kelas;
                                    $jumlah_sekamar = $kls->jumlah_sekamar;
                            ?>
                                
                                <tr >
                                    <?php echo form_open('administrator', array( 'id' => 'edit-form'.$id)); ?>
                                        <td style="text-align:center;"><?php echo $count++?></td>
                                        <td style="text-align:center;">
                                            <div style="margin:0px;padding:0px" class="data_input<?php echo $id ?> hide form-group form-md-line-input">
                                                <input type="text" class="form-control" name="kelas" id="kelas" value="<?php echo $kelas ?>" style="text-align: center">
                                                <label for="kelas"></label>
                                            </div>
                                            <span class="data_text<?php echo $id ?>"><?php echo $kelas ?></span>
                                        </td>
                                        <td style="text-align:center;">
                                            <div style="margin:0px;padding:0px" class="data_input<?php echo $id ?> hide form-group form-md-line-input">
                                                <input type="text" class="form-control" name="jumlah_sekamar" id="jumlah_sekamar" value="<?php echo $jumlah_sekamar ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="text-align: center">
                                                <label for="jumlah_sekamar"></label>
                                            </div>
                                            <span class="data_text<?php echo $id ?>"><i class="fa fa-users"></i>&nbsp; <?php echo $jumlah_sekamar ?>&nbsp; Orang</span>
                                        </td>
                                    <?php echo form_close();?>
                                    <td>
                                            <div class="btn-group" >
                                                <button style="width:100%" class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                    <i class="fa fa-angle-down"></i>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="javascript:;" onClick="edit_function('update',<?php echo $id ?>);" class="btn_save<?php echo $id ?> hide"><i class="fa fa-save"></i>&nbsp;Save</a></li>
                                                    <li><a href="javascript:;" onClick="edit_function('cancel',<?php echo $id ?>);" class="btn_cancel<?php echo $id ?> hide"><i class="fa fa-times"></i>&nbsp;Cancel</a></li>
                                                    <li><a href="javascript:;" onClick="edit_function('edit',<?php echo $id ?>);" class="btn_edit<?php echo $id ?>"><i class="fa fa-pencil"></i>&nbsp;Edit</a></li>
                                                    <li><a href="javascript:;" onClick="edit_function('delete',<?php echo $id ?>);" class="btn_hapus<?php echo $id ?>"><i class="fa fa-trash"></i>&nbsp;Hapus</a></li>
                                                </ul>
                                            </div>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                        
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<!-- add Modal-->
<div class="modal fade" id="add_modal"  role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-plus font-red"></i>
                    <span class="caption-subject font-red sbold uppercase">Tambah Kelas</span>
                </div>
            </div>
            <div class="portlet-body form">
                <?php echo form_open('administrator', array( 'id' => 'add-form')); ?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_control_1">Nama Kelas</label>
                                <input type="text" class="form-control" name="kelas" id="kelas">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_control_1">Jumlah Sekamar</label>
                                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" name="jumlah_sekamar">
                            </div>
                        </div>
                    </div>
                <?php echo form_close();?>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-circle grey">Cancel &nbsp;<i class="fa fa-times"></i></button>            
                <button type="button" onClick="add_function('add')" class="btn btn-circle red">Simpan &nbsp;<i class="fa fa-save"></i></button>
            </div>
        </div>

    </div>
</div>