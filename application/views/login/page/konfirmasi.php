<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<?php include 'global_function.php';$login_type = $this->session->userdata('login_type'); ?>

<script type="text/javascript">
    function add_function(task){
        if(task == 'show'){
            $('#add_modal').modal();
        }
        if(task == 'add'){
            $('#add_modal').modal('hide');
            data = new FormData($('#add-form')[0]);
            url = '<?php echo base_url() ?>administrator/konfirmasi/add';
            AjaxCRUD('add',data,url);
        }
    }

    function edit_function(task='',id=''){
        if(task == 'edit'){
            $('.data_input'+id).removeClass('hide');
            $('.data_text'+id).addClass('hide');

            $('.btn_save'+id).removeClass('hide');
            $('.btn_cancel'+id).removeClass('hide');

            $('.btn_edit'+id).addClass('hide');
            $('.btn_hapus'+id).addClass('hide');
        }

        if(task == 'cancel'){
            $('.data_input'+id).addClass('hide');
            $('.data_text'+id).removeClass('hide');

            $('.btn_save'+id).addClass('hide');
            $('.btn_cancel'+id).addClass('hide');

            $('.btn_edit'+id).removeClass('hide');
            $('.btn_hapus'+id).removeClass('hide');
        }

        if(task == 'update'){
            data = new FormData($('#edit-form'+id)[0]);
            url = '<?php echo base_url() ?>administrator/konfirmasi/update/'+id;
            AjaxCRUD('update',data,url);
        }

        if(task == 'delete'){
            url = '<?php echo base_url() ?>administrator/konfirmasi/delete/'+id;
            AjaxCRUD('delete',"",url);
        }
    }
    function gantiStatusValidasi(id){
        val = $('#status_validasi'+id).val();
        url = "<?php echo base_url().'administrator/konfirmasi/setStatusValidasi'?>";
        $.ajax({
            method: 'POST',
            url: url,
            data : 'status_validasi='+val+'&konfirmasi_id='+id,
            dataType: 'json',
            success : function(resp){
                if(resp == "update_success"){
                    swal({
                      title: "Berhasil !",
                      text : "Aksi berhasil dilakukan",
                      type: "success",
                      timer: 800,
                      showConfirmButton : false
                    });
                    $('#badge_'+id).remove();
                    
                    if(val==id+'_Tidak Valid')
                        $('#label_konfirmasi_'+id).append('<span class="font-red" id="tidak_valid_badge_'+id+'" style="font-size:12px"><i class="fa fa-times"></i> Tidak Valid</span>');
                    else if(val==id+'_Valid')
                        $('#tidak_valid_badge_'+id).remove();
                } 
            }
        });
    }
</script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Konfirmasi Pembayaran
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="<?php echo base_url().'administrator' ?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Konfirmasi Pembayaran</span>
        </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">        
            <?php if($show_detail){?>
                <div class="col-md-8">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-red-sunglo">
                                <i class="icon-settings font-red-sunglo"></i>
                                <span class="caption-subject bold uppercase">Konfirmasi Pembayaran</span>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <?php echo form_open('administrator/konfirmasi/setStatusValidasi', array( 'id' => 'verif-form')); ?>
                                    <?php if($dt_konfirmasi[0]->status_validasi=='Valid'){ ?>
                                            <label class="alert alert-success col-md-12"><i class="fa fa-check"></i>&nbsp; Pembayaran Valid dan Sudah Diterima</label>
                                    <?php } else if($dt_konfirmasi[0]->status_validasi=='Tidak Valid'){ ?>
                                            <label class="alert alert-danger col-md-12"><i class="fa fa-times"></i>&nbsp; Pembayaran Tidak Valid</label>
                                    <?php } ?>
                                    <h3>Jemaah berikut melakukan konfirmasi pembayaran yang telah dilakukan :</h3>
                                    <?php //echo_array($dt_konfirmasi) ?>
                                    <table class="table">
                                        <tr>
                                            <td>Nama</td><td>:</td>
                                            <td><?php echo $dt_konfirmasi[0]->nama ?></td>
                                        </tr>
                                        <tr>
                                            <td>Email</td><td>:</td>
                                            <td><?php echo $dt_konfirmasi[0]->email ?></td>
                                        </tr>
                                        <tr>
                                            <td>Telephone</td><td>:</td>
                                            <td><?php echo $dt_konfirmasi[0]->no_hp ?></td>
                                        </tr>
                                        <tr>
                                            <td>Jumlah Bayar</td><td>:</td>
                                            <td><?php echo rupiah_format($dt_konfirmasi[0]->jumlah_bayar) ?></td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Bayar</td><td>:</td>
                                            <td><?php echo date('d-M-Y',strtotime($dt_konfirmasi[0]->tgl_bayar)) ?></td>
                                        </tr>
                                        <tr>
                                            <td>Bank Tujuan</td><td>:</td>
                                            <td><?php echo $dt_konfirmasi[0]->bank_tujuan ?></td>
                                        </tr>
                                        <tr>
                                            <td>Nama Pengirim / Pemilik Rekening</td><td>:</td>
                                            <td><?php echo $dt_konfirmasi[0]->nama_pemilik_rek ?></td>
                                        </tr>
                                            <td>Tanggal Konfirmasi</td><td>:</td>
                                            <td><?php echo date('d-M-Y',strtotime($dt_konfirmasi[0]->tgl_submit)) ?></td>
                                        </tr>
                                        <tr>
                                            <td>Keterangan</td><td>:</td>
                                            <td><?php echo $dt_konfirmasi[0]->keterangan ?></td>
                                        </tr>
                                        <tr>
                                            <td>Status Validasi</td><td>:</td>
                                            <td><?php if(!$dt_konfirmasi[0]->status_validasi){echo 'Menunggu...';}else{echo $dt_konfirmasi[0]->status_validasi;} ?></td>
                                        </tr>
                                        </table>
                                        <label class="alert alert-info"><i class="fa fa-info-circle"></i>&nbsp;Silahkan Kantor Pusat menghubungi jemaah terkait pembayaran Valid atau Tidak Valid</label><br>
                                        <input type="hidden" name="konfirmasi_id" value="<?php echo encrypt($dt_konfirmasi[0]->konfirmasi_id) ?>">
                                        <?php if(!$dt_konfirmasi[0]->status_validasi){ ?>
                                        <button id="btn_verif" name="btn_verif" value="Valid" class="btn green-jungle"><i class="fa fa-check"></i>&nbsp;Pembayaran Valid dan Sudah Diterima</button>
                                        <button id="btn_verif" name="btn_verif" value="Tidak Valid" class="btn red"><i class="fa fa-times"></i>&nbsp;Pembayaran Tidak Valid</button>
                                        <?php } ?>
                            <?php echo form_close();?>
                        </div>
                    </div>
                </div>

            <?php } else { ?>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Managed Konfirmasi Pembayaran</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-checkable" id="table-custom">
                                        <thead>
                                            <tr>
                                                <th style="text-align:center;"> NO</th>
                                                <th style="text-align:center"> Nama </th>
                                                <th style="text-align:center"> Handphone</th>
                                                <th style="text-align:center"> Tanggal Pengajuan</th>
                                                <th style="text-align:center"> Status</th>
                                                <th> Aksi </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                            $count=1;
                                            foreach($konfirmasi as $row){
                                                $id=encrypt($row->konfirmasi_id);
                                                $label='';
                                                $verif = $row->status_validasi;
                                                if(!$verif)
                                                    $label = '<span class="badge badge-danger" id="badge_'.$id.'"> new </span>';
                                                else if($verif == 'Tidak Valid'){
                                                    $label = '<span class="font-red" id="tidak_valid_badge_'.$id.'" style="font-size:12px"><i class="fa fa-times"></i> Tidak Valid</span>';
                                                }
                                        ?>
                                            
                                            <tr >
                                                <?php echo form_open('administrator', array( 'id' => 'edit-form'.$id)); ?>
                                                    <td style="text-align:center"><?php echo $count++?></td>
                                                    <td><?php echo $row->nama.'  <span id="label_konfirmasi_'.$id.'">'.$label.'</span>'?></td>
                                                    <td><?php echo $row->no_hp ?></td>
                                                    <td><?php echo date('d-F-Y h:i',strtotime($row->tgl_submit))?></td>
                                                    <td>
                                                        <select onChange="gantiStatusValidasi(<?php echo $id?>)" name="status_validasi<?php echo $id?>" id="status_validasi<?php echo $id?>">
                                                            <option>Menunggu...</option>
                                                            <option value="<?php echo $id?>_Valid" <?php if($row->status_validasi=='Valid'){echo 'selected';} ?>>Valid</option>
                                                            <option value="<?php echo $id?>_Tidak Valid" <?php if($row->status_validasi=='Tidak Valid'){echo 'selected';} ?>>Tidak Valid</option>
                                                        </select>
                                                    </td>
                                                <?php echo form_close();?>
                                                <td>
                                                    <div class="btn-group" >
                                                        <button style="width:100%" class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="javascript:;" onClick="window.location='<?php echo base_url().'administrator/konfirmasi/detail/'.$id ?>'"><i class="fa fa-search"></i>&nbsp;Detail</a></li>
                                                            <li><a href="javascript:;" onClick="edit_function('delete',<?php echo $id ?>);" class="btn_hapus<?php echo $id ?>"><i class="fa fa-trash"></i>&nbsp;Hapus</a></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<!-- add Modal-->
<div class="modal fade" id="add_modal"  role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-plus font-red"></i>
                    <span class="caption-subject font-red sbold uppercase">Tambah Konfirmasi</span>
                </div>
            </div>
            <div class="portlet-body form">
                <?php echo form_open('administrator', array( 'id' => 'add-form')); ?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_control_1">Nama Konfirmasi</label>
                                <input type="text" class="form-control" name="konfirmasi" id="konfirmasi">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_control_1">Jumlah Sekamar</label>
                                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" name="jumlah_sekamar">
                            </div>
                        </div>
                    </div>
                <?php echo form_close();?>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-circle grey">Cancel &nbsp;<i class="fa fa-times"></i></button>            
                <button type="button" onClick="add_function('add')" class="btn btn-circle red">Simpan &nbsp;<i class="fa fa-save"></i></button>
            </div>
        </div>

    </div>
</div>