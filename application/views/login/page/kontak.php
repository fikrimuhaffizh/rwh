<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function add_function(task){
        if(task == 'show'){
            $('#add_modal').modal();
        }
        if(task == 'add'){
            data = $('#add-form').serialize();
            $.ajax({
                method: 'POST',
                data:data,
                url: '<?php echo base_url() ?>administrator/kontak/add',
                dataType: 'json',
                success : function(resp){
                    if(resp == 'add_success')
                        $('#add_modal').modal('hide');
                        swal({
                              title: "Tambah Kontak",
                              text: "Penambahan berhasil",
                              type: "success",
                              confirmButtonClass: "btn-danger",
                              confirmButtonText: "Ok",
                            },
                            function(){
                                location.reload();
                            });
                        
                }
            });
        }
    }

    function edit_function(task='',id=''){
        if(task == 'edit'){
            $('.data_input'+id).removeClass('hide');
            $('.data_text'+id).addClass('hide');

            $('.btn_save'+id).removeClass('hide');
            $('.btn_cancel'+id).removeClass('hide');

            $('.btn_edit'+id).addClass('hide');
            $('.btn_hapus'+id).addClass('hide');
        }
        if(task == 'cancel'){
            $('.data_input'+id).addClass('hide');
            $('.data_text'+id).removeClass('hide');

            $('.btn_save'+id).addClass('hide');
            $('.btn_cancel'+id).addClass('hide');

            $('.btn_edit'+id).removeClass('hide');
            $('.btn_hapus'+id).removeClass('hide');
        }
        if(task == 'update'){
            data = $('#edit-form'+id).serialize();
            $.ajax({
                method: 'POST',
                data:data,
                url: '<?php echo base_url() ?>administrator/kontak/update/'+id,
                dataType: 'json',
                success : function(resp){
                    if(resp == 'update_success')
                        $('#add_modal').modal('hide');
                        swal({
                              title: "Update Kontak",
                              text: "Pembaharuan berhasil",
                              type: "success",
                              confirmButtonClass: "green-sharp",
                              confirmButtonText: "Ok",
                            },
                            function(){
                                location.reload();
                        });
                        
                }
            });
        }
        if(task == 'delete'){
            swal({
              title: "Hapus Data?",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "grey",
              confirmButtonText: "Yes, delete it!",
              closeOnConfirm: false
            },
            function(){
                data = $('#edit-form'+id).serialize();
                $.ajax({
                    method: 'POST',
                    data:data,
                    url: '<?php echo base_url() ?>administrator/kontak/delete/'+id,
                    dataType: 'json',
                    success : function(resp){
                        if(resp == 'update_success')
                            $('#add_modal').modal('hide');
                            swal({
                                  title: "Hapus Data",
                                  text: "Penghapusan berhasil",
                                  type: "success",
                                  confirmButtonClass: "grey",
                                  confirmButtonText: "Ok",
                                },
                                function(){
                                    location.reload();
                                });
                            
                    }
                });
            });

            
        }
    }

    function wa_ready(id=''){
        if($('#wa_ready_'+id).is(':checked')) {
            flag = 'Ready';
        } else {
            flag = null;
        }

        $.ajax({
            method: 'POST',
            url: '<?php echo base_url() ?>administrator/kontak/wa_ready/'+id,
            data : 'wa_ready='+flag,
            dataType: 'json',
            success : function(resp){
                if(resp == 'update_success')
                    swal({
                              title: "Update Kontak",
                              text: "Pembaharuan berhasil",
                              type: "success",
                              confirmButtonClass: "green-sharp",
                              confirmButtonText: "Ok",
                            },
                            function(){
                                location.reload();
                        });
                    
            }
        });

        
    }

</script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Kontak
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="<?php echo base_url().'administrator' ?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Kontak</span>
        </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">
    <!-- List kontak -->
        <div class="col-md-12">
            <div class="portlet  bordered">     
                <div class="btn-group">
                    <button id="sample_editable_1_new" onClick="add_function('show')" class="btn btn-sm green-jungle">Tambah Kontak Whatsapp&nbsp; <i class="fa fa-whatsapp"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <?php 
                $count=1;
                foreach($kontak_utama as $row){
                    $id=encrypt($row->kontak_id);
                    $telp = $row->telp;
                    $faks = $row->faks;
                    $hp = $row->hp;
                    $email = $row->email;
                    $alamat = $row->alamat;
                    $status = '';
                    $style_checkbox = 'has-error';
                    if($row->whatsapp_chat == 'Ready'){
                        $status = 'checked';
                        $style_checkbox = 'has-success';
                    }
            ?>

            <div class="portlet light bordered">
                <div class="portlet-title" style="border-bottom:0px;margin-bottom: 0px">
                    <div class="caption">
                        <i class="icon-call-end font-red-mint"></i>
                        <span class="caption-subject font-red-mint sbold">&nbsp; Kontak Utama RWH</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <button type="button" style="margin:2px 2px 2px 2px" onClick="edit_function('update',<?php echo $id ?>);" class="btn btn-sm green-sharp btn_save<?php echo $id ?> hide">Save &nbsp;<i class="fa fa-save"></i></button>
                            <button type="button" style="margin:2px 2px 2px 2px" onClick="edit_function('cancel',<?php echo $id ?>);" class="btn btn-sm grey btn_cancel<?php echo $id ?> hide">Cancel &nbsp;<i class="fa fa-times"></i></button>
                            <button type="button" style="margin:2px 2px 2px 2px" onClick="edit_function('edit',<?php echo $id ?>);" class="btn btn-sm yellow-crusta btn_edit<?php echo $id ?>">Edit &nbsp;<i class="fa fa-pencil"></i></button>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                <?php echo form_open('administrator', array( 'id' => 'edit-form'.$id)); ?>
                    <table class="table ">
                        <tr>
                            <td style="width:110px"><i class="fa fa-phone"></i>&nbsp;Telephone</td>
                            <td>
                                <div style="margin:0px;padding:0px" class="data_input<?php echo $id ?> hide form-group form-md-line-input">
                                    <input type="text" class="form-control" name="telp" id="telp" value="<?php echo $telp ?>">
                                    <label for="telp"></label>
                                </div>
                                <span class="data_text<?php echo $id ?>"><?php echo $telp ?></span>
                            </td>
                        </tr>
                    <!--    <tr>
                            <td><i class="fa fa-fax"></i>&nbsp;Fax</td>
                            <td>
                                <div style="margin:0px;padding:0px" class="data_input<?php echo $id ?> hide form-group form-md-line-input">
                                    <input type="text" class="form-control" name="faks" id="faks" value="<?php echo $faks ?>">
                                    <label for="faks"></label>
                                </div>
                                <span class="data_text<?php echo $id ?>"><?php echo $faks ?></span>
                            </td>
                        </tr> -->
                        <tr>
                            <td><i class="fa fa-mobile-phone"></i>&nbsp;Handphone</td>
                            <td>
                                <div style="margin:0px;padding:0px" class="data_input<?php echo $id ?> hide form-group form-md-line-input">
                                    <input type="text" class="form-control" name="hp" id="hp" value="<?php echo $hp ?>">
                                    <label for="hp"></label>
                                </div>
                                <span class="data_text<?php echo $id ?>"><?php echo $hp ?></span>
                                <div class="md-checkbox <?php echo $style_checkbox ?> pull-right">
                                    <input type="checkbox" onclick="wa_ready(<?php echo $id ?>)" id="wa_ready_<?php echo $id ?>" class="md-check" <?php echo $status; ?>>
                                    <label for="wa_ready_<?php echo $id ?>">
                                        <span class="inc"></span>
                                        <span class="check"></span>
                                        <span class="box"></span> <i class="fa fa-whatsapp"></i> Whatsapp Ready </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-envelope"></i>&nbsp;Email</td>
                            <td>
                                <div style="margin:0px;padding:0px" class="data_input<?php echo $id ?> hide form-group form-md-line-input">
                                    <textarea class="form-control" name="email" id="email" rows="1"><?php echo $email ?></textarea>
                                    <label for="email"></label>
                                </div>
                                <span class="data_text<?php echo $id ?>"><?php echo $email ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-home"></i>&nbsp;Alamat</td>
                            <td>
                                <div style="margin:0px;padding:0px" class="data_input<?php echo $id ?> hide form-group form-md-line-input">
                                    <textarea type="text" class="form-control" name="alamat" id="alamat" ><?php echo $alamat ?></textarea>
                                    <label for="alamat"></label>
                                </div>
                                <span class="data_text<?php echo $id ?>"><?php echo $alamat ?></span>
                            </td>
                        </tr>
                    </table>
                <?php echo form_close();?>
                </div>
            </div>
            <?php } ?>
        </div>
        <div class="col-md-6">
            <?php 
                $count=0;
                foreach($kontak_wa as $row){
                    $id=encrypt($row->kontak_id);
                    $status = '';
                    $style_checkbox = 'has-error';
                    if($row->whatsapp_chat == 'Ready'){
                        $status = 'checked';
                        $style_checkbox = 'has-success';
                    }
                    $count++;
            ?>
                <div class="portlet light">
                 
                    <?php echo form_open('administrator', array( 'id' => 'edit-form'.$id)); ?>
                        <table class="table" style="margin-bottom: 0px">
                            <tr>
                                <td><i class="fa fa-whatsapp"></i>&nbsp;WA <?php echo $count ?></td>
                                <td>
                                    <div style="margin:0px;padding:0px" class="data_input<?php echo $id ?> hide form-group form-md-line-input">
                                        <input type="text" class="form-control" name="hp" id="hp" value="<?php echo $row->hp ?>">
                                        <label for="hp"></label>
                                    </div>
                                    <span class="data_text<?php echo $id ?>"><?php echo $row->hp ?></span>
                                    
                                </td>
                                <td>
                                    <div class="md-checkbox <?php echo $style_checkbox ?> pull-right">
                                        <input type="checkbox" onclick="wa_ready(<?php echo $id ?>)" id="wa_ready_<?php echo $id ?>" class="md-check" <?php echo $status; ?>>
                                        <label for="wa_ready_<?php echo $id ?>">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> <i class="fa fa-whatsapp"></i> Ready </label>
                                    </div>
                                </td>
                                <td>
                                    <button type="button" style="margin:2px 2px 2px 2px" onClick="edit_function('update',<?php echo $id ?>);" class="btn btn-xs green-sharp btn_save<?php echo $id ?> hide">Save &nbsp;<i class="fa fa-save"></i></button>
                                    <button type="button" style="margin:2px 2px 2px 2px" onClick="edit_function('cancel',<?php echo $id ?>);" class="btn btn-xs grey btn_cancel<?php echo $id ?> hide">Cancel &nbsp;<i class="fa fa-times"></i></button>
                                    <button type="button" style="margin:2px 2px 2px 2px" onClick="edit_function('edit',<?php echo $id ?>);" class="btn btn-xs yellow-crusta btn_edit<?php echo $id ?>">Edit &nbsp;<i class="fa fa-pencil"></i></button>
                                    <button type="button" style="margin:2px 2px 2px 2px" onClick="edit_function('delete',<?php echo $id ?>);" class="btn btn-xs grey btn_hapus<?php echo $id ?>">Hapus &nbsp;<i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                        </table>
                    <?php echo form_close();?>
                   
                </div>
            <?php } ?>
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<!-- add Modal-->
<div class="modal fade" id="add_modal"  role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" fa fa-whatsapp font-green-jungle"></i>
                    <span class="caption-subject font-green-jungle sbold uppercase">Tambah Kontak Whatsapp</span>
                </div>
            </div>
            <div class="portlet-body form">
                <?php echo form_open('administrator', array( 'id' => 'add-form')); ?>
<!--                     <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" name="telp" id="telp">
                                <label for="form_control_1">Telephone</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" name="faks" name="faks">
                                <label for="form_control_1">Fax</label>
                            </div>

                        </div>
                    </div> -->
                    <div class="row">
                        <div class="col-md-12">
                            
                                <table class="table">
                                    <tr>
                                        <td>Handphone ready for Whatsapp</td><td>:</td>
                                        <td><input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" name="hp" id="hp"></td>
                                    </tr>
                                </table>
                        </div>
                    </div>
<!--                         <div class="col-md-6">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <textarea type="text" rows="1" class="form-control" name="email" id="email"></textarea>
                                <label for="form_control_1">Email</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">                        
                        <div class="col-md-12">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <textarea type="text" rows="2" class="form-control" name="alamat" id="alamat"></textarea>
                                <label for="form_control_1">Alamat</label>
                            </div>
                        </div>
                    </div> -->
                <?php echo form_close();?>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-circle grey">Cancel &nbsp;<i class="fa fa-times"></i></button>            
                <button type="button" onClick="add_function('add')" class="btn btn-circle green-jungle">Simpan &nbsp;<i class="fa fa-save"></i></button>
            </div>
        </div>

    </div>
</div>