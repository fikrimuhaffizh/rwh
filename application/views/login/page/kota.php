<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<?php include 'global_function.php';$login_type = $this->session->userdata('login_type'); ?>

<script type="text/javascript">
    function add_function(task){
        if(task == 'show'){
            $('#add-form').trigger("reset");
            $('#btn-submit').attr('onClick','add_function("add")').addClass('btn btn-circle red');
            $('#add_modal').modal();
        }
        if(task == 'add'){
            $('#add_modal').modal('hide');
            data = new FormData($('#add-form')[0]);
            $.ajax({
                method: 'POST',
                data:data,
                url: '<?php echo base_url() ?>administrator/kota/add',
                cache:false,
                contentType: false,
                processData: false,
                success : function(resp){
                    if(resp == '"add_success"'){
                        swal({
                              title: "Berhasil !",
                              text : "Kota berhasil ditambahkan",
                              type: "success",
                              timer: 800,
                              showConfirmButton : false
                            },
                            function(){
                                swal.close();
                                table.ajax.reload();
                        });
                    }
                }
            });
        }
    }

    function edit_function(task='',id=''){
        if(task == 'edit'){
            $('#add_modal').modal();
            $('input[name=nama_kota]').val($('#nama_kota_'+id).val());
            $('input[name=kode_kota]').val($('#kode_kota_'+id).val());
            $('#btn-submit').attr('onClick','edit_function("update",'+id+')').addClass('btn btn-circle green');
        }

        if(task == 'update'){
            $.ajax({
                method: 'POST',
                data:$('#add-form').serialize(),
                url: '<?php echo base_url() ?>administrator/kota/update/'+id,
                dataType: 'json',
                success : function(resp){
                    if(resp == "update_success"){
                        $('#add_modal').modal('hide');
                        swal({
                              title: "Berhasil !",
                              text : "Kota berhasil diperbaharui",
                              type: "success",
                              timer: 800,
                              showConfirmButton : false
                            },
                            function(){
                                swal.close();
                                table.ajax.reload();
                        });
                    }
                }
            });
        }

        if(task == 'delete'){
            url = '<?php echo base_url() ?>administrator/kota/delete/'+id;
            swal({
                  title: "Hapus Data Kota?",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "red",
                  cancelButtonClass: "grey",
                  confirmButtonText: "Yes, delete it!",
                  closeOnConfirm: false,
                  showLoaderOnConfirm : true
                },
                function(){
                    $.ajax({
                      method: 'POST',
                      url: url,
                      dataType: 'json',
                      success : function(resp){
                          if(resp == 'update_success'){
                            swal.close();
                            table.ajax.reload();
                          }
                      }
                  });
                }); 
            
        }
    }
    $(document).ready(function(){

        table = $('#table_kota').DataTable({ 
            "processing": true, "serverSide": true,"order": [],
            "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
            "ajax": {"url": "<?php echo site_url('administrator/kota/pagination')?>","type": "POST"},
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                        {"targets": [0],"className": "dt-center"},
                        {"targets": [2],"className": "dt-center"}

                    ]
        }); 

    });
</script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Kota Berangkat
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="<?php echo base_url().'administrator' ?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Kota Berangkat</span>
        </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">
        <div class="col-md-12">
            <div class="row">
            
            <!-- Edit Form -->
                <div class="update_form col-md-6 hide">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-pencil font-yellow"></i>
                                <span class="caption-subject font-yellow sbold uppercase">Edit Kota</span>
                            </div>
                            <div class="actions">
                                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <form role="form">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group form-md-line-input form-md-floating-label">
                                    <input type="text" class="form-control" name="kota" id="kota">
                                    <label for="form_control_1">Nama Kota</label>
                                </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            <!-- List Kota -->
                <div class="col-md-12">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <button id="sample_editable_1_new" onClick="add_function('show')" class="btn btn-sm red">Tambah Kota
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold uppercase"> Managed Kota</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered  table-condensed" id="table_kota">
                                <thead>
                                    <tr>
                                        <th> No </th>
                                        <th> Nama Kota</th>
                                        <th> Kode Kota</th>
                                        <th> Aksi </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<!-- add Modal-->
<div class="modal fade" id="add_modal"  role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-social-dropbox font-red"></i>
                    <span class="caption-subject font-red sbold uppercase">Data Kota</span>
                </div>
            </div>
            <div class="portlet-body form">
                <?php echo form_open('administrator', array( 'id' => 'add-form')); ?>
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group">
                                <label for="form_control_1">Nama Kota</label>
                                <input type="text" class="form-control input-sm" name="nama_kota" id="nama_kota">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="form_control_1">Kode Kota</label>
                                <input type="text" class="form-control input-sm" name="kode_kota" maxlength="3" id="kode_kota" placeholder="3 Kata">
                            </div>
                        </div>
                    </div>
                <?php echo form_close();?>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-circle grey">Cancel &nbsp;<i class="fa fa-times"></i></button>            
                <button id="btn-submit" type="button" >Simpan &nbsp;<i class="fa fa-save"></i></button>
            </div>
        </div>

    </div>
</div>