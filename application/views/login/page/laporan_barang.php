<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>Laporan Stok Barang
                </h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo base_url().'administrator' ?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Laporan Stok Barang</span>
            </li>
        </ul>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-docs"></i>
                            <span class="caption-subject sbold uppercase">Laporan Stok Barang</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                    <?php echo form_open(base_url().'administrator/laporan_barang/cetak', array('id'=>'add-edit-form','class'=>'form-horizontal form-row-seperated','target'=>'_blank')); ?>
                    <div class="form-body">
                        <div class="form-group div_paket">                         
                            <label class="control-label col-md-2">Kantor Cabang</label>
                            <div class="col-md-5">
                                <select class="form-control" name="kantorcabang_id" required="">
                                    <?php 
                                        if($this->session->userdata('login_type')=='Administrator'){echo '<option value="Semua Kantor" selected>Semua Kantor</option>';}
                                        foreach($kantor as $row){
                                    ?>
                                        <option value="<?php echo encrypt($row->kantorcabang_id) ?>"><?php echo $row->nama_cabang; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>                             
                        <div class="form-group">
                            <label class="control-label col-md-2">Output Laporan</label>
                            <div class="col-md-5">
                                <select class="form-control" name="output" required="">
                                    <option value="Web" selected>Web</option>
                                    <option value="Excel">Excel</option>
                                </select>
                            </div>
                        </div>
                    <div class="form-actions">
                        <div class="row">
                            <hr>
                            <div class="col-md-offset-2 col-md-6">
                                <button type="submit" class="btn green">
                                    <i class="fa fa-print"></i> Cetak Laporan</button>
                            </div>
                        </div>
                    </div>
                <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
