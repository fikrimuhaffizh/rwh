<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function toggleJenis(jenis){
        if(jenis=='paket'){
            $('.div_keberangkatan').hide('fast');
            $('.div_paket').show('slow');
        } else if(jenis=='keberangkatan'){
            $('.div_paket').hide('slow');
            $('.div_keberangkatan').show('fast');
        }
    }
</script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>Laporan Jemaah
                </h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo base_url().'administrator' ?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Laporan Jemaah</span>
            </li>
        </ul>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-docs"></i>
                            <span class="caption-subject sbold uppercase">Laporan Jemaah</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                    <?php echo form_open(base_url().'administrator/laporan_jemaah/cetak', array('id'=>'add-edit-form','class'=>'form-horizontal form-row-seperated','target'=>'_blank')); ?>
                    <div class="form-body">

                        <div class="form-group">                         
                            <label class="control-label col-md-2">Laporan</label>
                            <div class="col-md-5">
                                <select class="form-control" name="jenis" onChange="toggleJenis(this.value)">
                                    <option value="paket">Per Paket</option>
                                    <option value="keberangkatan">Per Tanggal Keberangkatan</option>
                                </select>
                                
                            </div>
                        </div> 
                        <div class="form-group div_paket">                         
                            <label class="control-label col-md-2">Paket</label>
                            <div class="col-md-5">
                                <select class="form-control" name="paket_id" required="">
                                    <option value="Semua Paket" selected>Semua Paket</option>
                                    <?php 
                                        for($i=0;$i<count($paket);$i++){
                                    ?>
                                        <option value="<?php echo encrypt($paket[$i]->paket_id) ?>"><?php echo $paket[$i]->nama_paket; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>     
                        <div class="form-group div_keberangkatan" style="display: none">                         
                            <label class="control-label col-md-2">Tanggal Kebrangkatan</label>
                            <div class="col-md-5">
                                <div class="form-md-line-input form-md-floating-label input-group date date-picker" data-date-format="dd-mm-yyyy">
                                    <input type="text" class="form-control" name="tgl_keberangkatan" id="tgl_keberangkatan" readonly="" placeholder="dd-mm-yyyy">
                                    <span class="input-group-btn">
                                        <button class="btn default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>   
                        <div class="form-group div_paket">
                            <label class="control-label col-md-2">Status Keberangkatan</label>
                            <div class="col-md-5">
                                <select class="form-control" name="status_keberangkatan">
                                    <option value="" selected>Semua Status</option>
                                    <option value="Dalam Proses" >Dalam Proses</option>
                                    <option value="Sudah Berangkat">Sudah Berangkat</option>
                                    <option value="Cancel">Cancel</option>
                                </select>
                            </div>
                        </div>                      
                        <div class="form-group">
                            <label class="control-label col-md-2">Output Laporan</label>
                            <div class="col-md-5">
                                <select class="form-control" name="output" required="">
                                    <option value="Web" selected>Web</option>
                                    <option value="Excel">Excel</option>
                                </select>
                            </div>
                        </div>
                    <div class="form-actions">
                        <div class="row">
                            <hr>
                            <div class="col-md-offset-2 col-md-6">
                                <button type="submit" class="btn green">
                                    <i class="fa fa-print"></i> Cetak Laporan</button>
                            </div>
                        </div>
                    </div>
                <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
