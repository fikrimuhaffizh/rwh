<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>Laporan Pembayaran / Pengembalian
                </h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo base_url().'administrator' ?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Laporan Transaksi</span>
            </li>
        </ul>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-docs"></i>
                            <span class="caption-subject sbold uppercase">Laporan Pembayaran / Pengembalian Jemaah</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <?php echo form_open(base_url().'administrator/laporan_pembayaran/cetak', array('id'=>'add-edit-form','class'=>'form-horizontal form-row-seperated','target'=>'_blank')); ?>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Jenis Transaksi</label>
                                    <div class="col-md-5">
                                        <select class="form-control" name="jenis_transaksi" required="">
                                            <option value="Semua Jenis" selected>Semua Jenis</option>
                                            <option value="Pembayaran">Pembayaran</option>
                                            <option value="Pengembalian">Pengembalian</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group div_paket">                         
                                    <label class="control-label col-md-2">Paket</label>
                                    <div class="col-md-5">
                                        <select class="form-control" name="paket_id" required="">
                                            <option value="Semua Paket" selected>Semua Paket</option>
                                            <?php 
                                                for($i=0;$i<count($paket);$i++){
                                            ?>
                                                <option value="<?php echo encrypt($paket[$i]->paket_id) ?>"><?php echo $paket[$i]->nama_paket; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">Tanggal</label>
                                    <div class="col-md-5">
                                        <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                            <input type="text" class="form-control" name="tgl_awal" placeholder="Awal" autocomplete="off" required>
                                            <span class="input-group-addon"> - </span>
                                            <input type="text" class="form-control" name="tgl_akhir" placeholder="Akhir" autocomplete="off" required> 
                                        </div>   
                                    </div>
                                </div>                        
                                <div class="form-group">
                                    <label class="control-label col-md-2">Output Laporan</label>
                                    <div class="col-md-5">
                                        <select class="form-control" name="output" required="">
                                            <option value="Web" selected>Web</option>
                                            <option value="Excel">Excel</option>
                                        </select>
                                       
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <hr>
                                        <div class="col-md-offset-2 col-md-6">
                                            <button type="submit" class="btn green">
                                                <i class="fa fa-print"></i> Cetak Laporan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
