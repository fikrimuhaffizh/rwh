<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<script type="text/javascript"></script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>Laporan Pendaftaran
                </h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo base_url().'administrator' ?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Laporan Pendaftaran</span>
            </li>
        </ul>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-docs"></i>
                            <span class="caption-subject sbold uppercase">Laporan Pendaftaran Jemaah</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                    <?php echo form_open(base_url().'administrator/laporan_pendaftaran/cetak', array('id'=>'add-edit-form','class'=>'form-horizontal form-row-seperated','target'=>'_blank')); ?>
                    <hr>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-2"> Tanggal Pendaftaran</label>
                            <div class="col-md-3">
                                    <div class="input-group  date-picker input-daterange"  data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                        <input type="text" class="form-control" name="tgl_awal" placeholder="Awal" required>
                                        <span class="input-group-addon"> - </span>
                                        <input type="text" class="form-control" name="tgl_akhir" placeholder="Akhir" required> 
                                    </div>                                
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label class="control-label col-md-2">Output Laporan</label>
                            <div class="col-md-3">
                                <select class="form-control" name="output" required="">
                                    <option value="Web" selected>Web</option>
                                    <option value="Excel">Excel</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <hr>
                                <div class="col-md-offset-2 col-md-6">
                                    <button type="submit" class="btn green">
                                        <i class="fa fa-print"></i> Cetak Laporan Pendaftaran</button>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
