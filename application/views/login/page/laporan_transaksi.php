<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function getListBarangByKantorCabang(id){
        $('.list_barang').hide();
        $('#list_barang').empty();
        $.ajax({
                method: 'POST',
                data:{'kantorcabang_id' : id},
                url: '<?php echo base_url() ?>administrator/barang/getListBarangByKantorCabang',
                dataType : 'JSON',
                beforeSend: function(){$('.loading_ajax').show();},
                complete: function(){$('.loading_ajax').hide();},
                success : function(resp){
                    if(resp.length>0){
                        $('.list_barang').show();
                        for(var i=0;i<resp.length;i++){
                            nama = resp[i]['nama_barang'];
                            id = resp[i]['barang_id'];
                            $('#list_barang').append('<option value="'+id+'">'+nama+'</option>');
                        }
                        $('#list_barang').append('<option value="Semua Barang">Semua Barang</option>');                    
                    } else {
                        alert('Kantor Cabang tidak memiliki list barang !');
                    }
                }
            });
    }

    function toggleJenis(jenis){
        if(jenis=='paket'){
            $('.div_keberangkatan').hide('fast');
            $('.div_paket').show('slow');
        } else if(jenis=='keberangkatan'){
            $('.div_paket').hide('slow');
            $('.div_keberangkatan').show('fast');
        }
    }
</script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>Laporan Transaksi Barang
                </h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo base_url().'administrator' ?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Laporan Transaksi Barang</span>
            </li>
        </ul>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-docs"></i>
                            <span class="caption-subject sbold uppercase">Laporan Transaksi Barang</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                    <?php echo form_open(base_url().'administrator/laporan_transaksi/cetak', array('id'=>'add-edit-form','class'=>'form-horizontal form-row-seperated','target'=>'_blank')); ?>
                    <div class="form-body">
                        <div class="form-group div_paket">                         
                            <label class="control-label col-md-2">Kantor Cabang</label>
                            <div class="col-md-5">
                                <select class="form-control" name="kantorcabang_id" required="" onchange="getListBarangByKantorCabang(this.value)">
                                    <option value="" selected>Pilih Kantor...</option>
                                    <?php 
                                        if($this->session->userdata('login_type')=='Administrator'){echo '<option value="Semua Kantor">Semua Kantor</option>';}
                                        foreach($kantor as $row){
                                    ?>
                                        <option value="<?php echo encrypt($row->kantorcabang_id) ?>"><?php echo $row->nama_cabang; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>                            
                        <div class="form-group">                         
                            <label class="control-label col-md-2">Barang</label>
                            <div class="col-md-5 list_barang">
                                <select class="form-control" id="list_barang" name="barang_id" required="">
                                    <option value="Semua Barang">Semua Barang</option>
                                </select>
                            </div>
                            <div class="col-md-5 loading_ajax display-none">
                                <img src="<?php echo base_url().'assets/frontend/img/AjaxLoader.gif' ?>" alt="">
                                <span>Mohon Tunggu...</span>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="control-label col-md-2">Output Laporan</label>
                            <div class="col-md-5">
                                <select class="form-control" name="output" required="">
                                    <option value="Web" selected>Web</option>
                                    <option value="Excel">Excel</option>
                                </select>
                            </div>
                        </div>
                    <div class="form-actions">
                        <div class="row">
                            <hr>
                            <div class="col-md-offset-2 col-md-6">
                                <button type="submit" class="btn green">
                                    <i class="fa fa-print"></i> Cetak Laporan</button>
                            </div>
                        </div>
                    </div>
                <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
