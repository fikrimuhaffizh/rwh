

<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>

<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>

<link href="<?php echo base_url() ?>assets/backend/pages/css/login-5.min.css" rel="stylesheet" type="text/css" />

<script src="<?php echo base_url() ?>assets/backend/pages/scripts/login-5.min.js" type="text/javascript"></script>

<script src="<?php echo base_url() ?>assets/backend/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>

        <script src="<?php echo base_url() ?>assets/backend/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>

        <div class="user-login-5">

            <div class="row bs-reset">

                <div class="col-md-6  col-xs-12 bs-reset mt-login-5-bsfix">

                    <div class="login-bg" style="background-image:url(<?php echo base_url() ?>assets/frontend/img/brand/bg1.jpg)">

                    <img class="login-logo" src="<?php echo base_url() ?>assets/frontend/img/brand/logo-black.png" /> 

                </div>

                </div>

                <div class="col-md-6 col-xs-12 login-container bs-reset mt-login-5-bsfix bg-white">

                    <div class="login-content">

                        <h1>PT. Riau Wisata Hati</h1>

                        <p><i> Menjadi Penyelenggara haji dan umrah terbaik di Indonesia yang memfokuskan kepada kepuasan setiap Jemaah dalam beribadah dengan motto "Bukan Jalan Jalan Biasa" </i></p>

                        <?php echo form_open('auth/oauth',array('class'=>'login-form')); ?>

                            <?php if($this->session->flashdata('err_login')){?>

                                <div class="alert alert-danger"><span>Username atau Password salah !</span></div>

                            <?php } ?>

                            <div class="row">

                                <div class="col-xs-6">

                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Username" name="username" required/> </div>

                                <div class="col-xs-6">

                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="password" autocomplete="off" placeholder="Password" name="password" required/> </div>

                            </div>

                            <div class="row">

                                <div class="col-sm-4">

                                    

                                </div>

                                <div class="col-sm-8 text-right">

                                    

                                    <button class="btn green" type="submit">Sign In</button>

                                </div>

                            </div>

                        <?php echo form_close(); ?>

                    </div>

                    <div class="login-footer">

                        <div class="row bs-reset">

                            <div class="col-xs-12 bs-reset">

                                <div class="login-copyright text-right">

                                    <p>Copyright &copy; PT.Riau Wisata Hati <?php echo date('Y') ?></p>

                                </div>

                                <div class="clearfix"></div>

                                <hr>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

