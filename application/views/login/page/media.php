<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function cek_all(){
        a= $('#author').val();
        if (document.getElementById("media").files.length == 0) {
            alert("Please Select Image");
            document.getElementById("media").click();
            exit();
        }
    }
        function add_function(task){
            if(task == 'show'){
                $('#add_modal').modal();
            }
            if(task == 'add'){
                cek_all();
                $('#add_modal').modal('hide');
                var file = document.getElementById('media').files[0];
                if (file) {
                    var file_data = $('#media').prop('files')[0];  
                    var data = new FormData();               
                    data.append('media', file_data); 
                    data.append('judul',$('#judul').val());
                }
                swal({
                      title: "Meng-upload File",
                      text: "Klik 'Upload' untuk memulai.",
                      type: "info",
                      closeOnConfirm: false,
                      confirmButtonText: 'Upload',
                      showLoaderOnConfirm: true,
                    },
                    function(){
                        $.ajax({
                            method: 'POST',
                            data:data,
                            url: '<?php echo base_url() ?>administrator/media/add',
                            dataType: 'text',
                            cache: false,
                            contentType: false,
                            processData: false,
                            success : function(resp){
                                if(resp == '"add_success"'){
                                    swal({
                                          title: "Tambah Media  ",
                                          text: "Penambahan berhasil",
                                          type: "success",
                                          confirmButtonClass: "btn-danger",
                                          confirmButtonText: "Ok",
                                        },
                                        function(){
                                            location.reload();
                                        });
                                }
                                if(resp == '"add_fail"'){
                                    swal({
                                          title: "Penambahan Gagal !",
                                          text: "File Tidak Sesuai atau Ukuran Melebihi 2 MB",
                                          type: "warning",
                                          confirmButtonClass: "btn-danger",
                                          confirmButtonText: "Ok",
                                        });
                                }
                            }
                        });
                    });
                
            }
        }

        function edit_function(task='',id=''){
            if(task == 'edit'){
            $('.data_input'+id).removeClass('hide');
            $('.data_text'+id).addClass('hide');

            $('.btn_save'+id).removeClass('hide');
            $('.btn_cancel'+id).removeClass('hide');

            $('.btn_edit'+id).addClass('hide');
            $('.btn_hapus'+id).addClass('hide');

        }

        if(task == 'cancel'){
            $('.data_input'+id).addClass('hide');
            $('.data_text'+id).removeClass('hide');

            $('.btn_save'+id).addClass('hide');
            $('.btn_cancel'+id).addClass('hide');
            $('.btn_tambah_gambar').removeClass('hide');

            $('.btn_edit'+id).removeClass('hide');
            $('.btn_hapus'+id).removeClass('hide');
        }

        if(task == 'update'){
            data = $('#edit-form'+id).serialize();
            $.ajax({
                method: 'POST',
                data:data,
                url: '<?php echo base_url() ?>administrator/kontak/update/'+id,
                dataType: 'json',
                success : function(resp){
                    if(resp == 'update_success')
                        $('#add_modal').modal('hide');
                        swal({
                              title: "Update Kontak",
                              text: "Pembaharuan berhasil",
                              type: "success",
                              confirmButtonClass: "green-sharp",
                              confirmButtonText: "Ok",
                            },
                            function(){
                                location.reload();
                            });
                        
                }
            });
        }
        if(task == 'delete'){
            swal({
              title: "Hapus Data?",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "grey",
              confirmButtonText: "Yes, delete it!",
              closeOnConfirm: false
            },
            function(){
                data = $('#edit-form'+id).serialize();
                $.ajax({
                    method: 'POST',
                    url: '<?php echo base_url() ?>administrator/media/delete/'+id,
                    dataType: 'json',
                    success : function(resp){
                        if(resp == 'update_success')
                            $('#add_modal').modal('hide');
                            swal({
                                  title: "Hapus Data",
                                  text: "Penghapusan berhasil",
                                  type: "success",
                                  confirmButtonClass: "grey",
                                  confirmButtonText: "Ok",
                                },
                                function(){
                                    location.reload();
                                });
                            
                    }
                });
            });

            
        }
    }

</script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Media
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="<?php echo base_url().'administrator' ?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Media</span>
        </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">
    <!-- List Media	 -->
        <div class="col-md-12">
            <div class="table-toolbar">
                <div class="row">
                    <div class="col-md-6">
                        <div class="btn-group">
                            <button type="button" onClick="add_function('show')" class="btn_tambah_gambar btn btn-sm red">Tambah File  &nbsp;<i class="fa fa-file"></i>&nbsp; / Gambar &nbsp;<i class="fa fa-image"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Managed Media</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered  table-checkable order-column" id="table-custom">
                                    <thead >
                                        <th style="text-align: center"> No. </th>
                                        <th style="text-align: center"> Judul </th>
                                        <th style="text-align: center"> File/Gambar </th>
                                        <th style="text-align: center"> Link </th>
                                        <th style="text-align: center"> Last Update </th>
                                        <th style="text-align: center"> Actions </th>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $count=0;
                                            foreach($media as $row){
                                                $btn_hapus = TRUE;
                                                $id=encrypt($row->media_id);
                                                $judul  = $row->judul;
                                                $deskripsi = $row->deskripsi;
                                                $alt_teks = $row->alt_teks;
                                                $tipe = $row->tipe;
                                                $tgl_upload = date('d-M-Y H:i:s',strtotime($row->tgl_upload));
                                                $tgl_perubahan = date('d-M-Y H:i:s',strtotime($row->tgl_perubahan));
                                                $author = $row->author;
                                                
                                                $temp = explode ('_',$judul);
                                                if($temp[0]=='Paket' || $temp[0]=='Thumbnail Artikel')
                                                    $btn_hapus=FALSE;

                                                if($tipe=='file'){
                                                    $link = $this->md_media_detail->getMediaDetailByMediaId(decrypt($id));
                                                    $show = '<div style="margin-top:10px" <i class="fa fa-file fa-3x"></i></div>';
                                                }
                                                if($tipe=='gambar'){
                                                    
                                                    $link_small = $this->md_media_detail->getMediaDetailByMediaIdByUkuran(decrypt($id),'small');
                                                    $link_medium = $this->md_media_detail->getMediaDetailByMediaIdByUkuran(decrypt($id),'medium');
                                                    $link_large = $this->md_media_detail->getMediaDetailByMediaIdByUkuran(decrypt($id),'large');
                                                    $show = '<img src="'.base_url().$link_small[0]->media_link.'">';
                                                }

                                            
                                            $count++;
                                        ?>
                                        <tr>
                                            <td style="text-align: center"> <?php echo $count; ?> </td>
                                            <td style="text-align: center"> <?php echo $judul;?> </td>
                                            <td style="text-align: center"> <?php echo $show;?> </td>
                                            <td>               
                                                <?php if($tipe=='gambar') {?>                                           
                                                <a href="javascript:;" style="margin:5px" class="btn btn-xs grey mt-clipboard" 
                                                    data-clipboard-action="copy" 
                                                    data-clipboard-text="<?php echo base_url().$link_small[0]->media_link ; ?>">
                                                    <i class="icon-note"></i> Copy Small Link
                                                </a>
                                                <a href="javascript:;" style="margin:5px" class="btn btn-xs green mt-clipboard" 
                                                    data-clipboard-action="copy" 
                                                    data-clipboard-text="<?php echo base_url().$link_medium[0]->media_link ; ?>">
                                                    <i class="icon-note"></i> Copy Medium Link
                                                </a>
                                                <a href="javascript:;" style="margin:5px" class="btn btn-xs red mt-clipboard" 
                                                    data-clipboard-action="copy" 
                                                    data-clipboard-text="<?php echo base_url().$link_large[0]->media_link ; ?>">
                                                    <i class="icon-note"></i> Copy Large Link
                                                </a>
                                                <?php } ?>
                                                <?php if($tipe=='file') {?>                                           
                                                <a href="javascript:;" class="btn btn-xs green mt-clipboard" 
                                                    data-clipboard-action="copy" 
                                                    data-clipboard-text="<?php echo base_url().$link[0]->media_link ; ?>">
                                                    <i class="icon-note"></i> Copy File Link
                                                </a>
                                                <?php } ?>
                                            </td>
                                            <td style="text-align: center"> <i class="fa fa-calendar"></i>&nbsp;<?php echo $tgl_perubahan; ?> </td>
                                            <td style="text-align: center">
                                                <div class="actions">
                                                    <div class="btn-group">
<!--                                                             <button type="button" style="margin:2px 2px 2px 2px" onClick="edit_function('update',<?php echo $id ?>);" class="btn btn-sm btn-circle green-sharp btn_save<?php echo $id ?> hide">Save &nbsp;<i class="fa fa-save"></i></button>
                                            <button type="button" style="margin:2px 2px 2px 2px" onClick="edit_function('cancel',<?php echo $id ?>);" class="btn btn-sm btn-circle grey btn_cancel<?php echo $id ?> hide">Cancel &nbsp;<i class="fa fa-times"></i></button>
                                            <button type="button" style="margin:2px 2px 2px 2px" onClick="edit_function('edit',<?php echo $id ?>);" class="btn btn-sm btn-circle yellow-crusta btn_edit<?php echo $id ?>">Edit &nbsp;<i class="fa fa-pencil"></i></button> -->                                                 
                                            <?php if($btn_hapus ){ ?>
                                                <button type="button" onClick="edit_function('delete',<?php echo $id ?>);" class="btn btn-sm grey btn_hapus<?php echo $id ?>">Hapus &nbsp;<i class="fa fa-trash"></i></button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<!-- add Modal-->
<div class="modal fade" id="add_modal"  role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-plus font-red"></i>
                    <span class="caption-subject font-red sbold uppercase">Tambah File / Gambar</span>
                </div>
            </div>
            <div class="portlet-body form">
                <?php echo form_open('administrator', array( 'id' => 'add-form')); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <span class="label label-danger">Note!</span> 
                            <h5><i>Jika Judul tidak diisi, nama file/gambar asli akan menjadi judul</i></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" name="judul" id="judul">
                                <label for="form_control_1">Judul (optional) </label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-5">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>
                                    <div>
                                        <span class="btn default btn-file">
                                            <span class="fileinput-new"> Select Image or File </span>
                                            <span class="fileinput-exists"> Change </span>
                                            <input type="file" name="media" id="media" required> </span>
                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <span class="label label-danger">Note!</span> 
                                <h5><i>Gambar atau File ini dapat digunakan dengan meng-copy link yang telah di generate ke dalam halaman yang ingin disisipkan gambar atau file tersebut</i></h5>
                            </div>
                        </div>
                    </div>
                    <br>
                <?php echo form_close();?>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-circle grey">Cancel &nbsp;<i class="fa fa-times"></i></button>            
                <button type="button" onClick="add_function('add')" class="btn btn-circle red">Simpan &nbsp;<i class="fa fa-save"></i></button>
            </div>
        </div>

    </div>
</div>
















