<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<?php include 'global_function.php'; $login_type = $this->session->userdata('login_type');?>
<script type="text/javascript">
    var dom = [
                'member_id',
                'nama_lengkap',
                'email',
                'no_hp',
                'alamat',
                'kota',
                'password',
                'registrasi',
                'status_aktif',
                'tgl_lahir',
                'tgl_regis',
                'total_jemaah'
            ];
    function add_function(task){
        if(task == 'show'){
            reset();
            $('.header-modal').text('Tambah Member');
            $('.form-modal').addClass('form-md-floating-label');
            $('.btn_add').removeClass('hide');
            $('.btn_update').addClass('hide');
            $('#add_edit_modal').modal();
        }
        if(task == 'add'){
            if(check_empty()>0)
                return alert('Form Belum Lengkap');
            
            $('#add_edit_modal').modal('hide');
            data = new FormData($('#add-form')[0]);
            url = '<?php echo base_url() ?>administrator/member/add';
            AjaxCRUD('add',data,url);
        }
    }

    function edit_function(task='',id='',count=''){

        if(task == 'show'){
            reset();
            $('.header-modal').text('Edit Member');
            var avatar_link = '<?php echo base_url() ?>'+$('#avatar_detail_'+count).val();
            $('#avatar').attr('src',avatar_link);
            $('#member_id').val(id);
            //Memasukkan data yang hidden ke inputnya masing masing pada modal add_edit_modal
            for(var i=0;i<dom.length;i++){
                $('#'+dom[i]).val($('#'+dom[i]+'_detail_'+count).val());
            }


            $('.btn_update').removeClass('hide');
            $('.btn_add').addClass('hide');

            $('.form-modal').removeClass('form-md-floating-label');
            $('#add_edit_modal').modal();
        }

        if(task == 'delete'){
            url = '<?php echo base_url() ?>administrator/member/delete/'+id;
            AjaxCRUD('delete',"",url);         
        }

        if(task == 'update'){
            //cek_all();
                $('#add_edit_modal').modal('hide');

                var data = new FormData();   
                var file = document.getElementById('avatar_select').files[0];
                if (file) {
                    data.append('avatar', $('#avatar_select').prop('files')[0]); 
                }
                data.append('nama_lengkap',$('#nama_lengkap').val());
                data.append('email',$('#email').val());
                data.append('no_hp',$('#no_hp').val());
                data.append('tgl_lahir',$('#tgl_lahir').val());
                data.append('alamat',$('#alamat').val());
                data.append('kota',$('#kota').val());
                data.append('member_id',$('#member_id').val());
                
                swal({
                      title: "Memperbaharui Member",
                      text: "Klik 'Update' untuk memulai.",
                      type: "info",
                      closeOnConfirm: false,
                      confirmButtonText: 'Update',
                      showLoaderOnConfirm: true,
                    },
                    function(){
                        $.ajax({
                            method: 'POST',
                            data:data,
                            url: '<?php echo base_url() ?>administrator/member/update/'+id,
                            dataType: 'text',
                            cache: false,
                            contentType: false,
                            processData: false,
                            success : function(resp){
                                if(resp == '"update_success"'){
                                    swal({
                                          title: "Pembaharuan Member  ",
                                          text: "Pembaharuan berhasil",
                                          type: "success",
                                          confirmButtonClass: "green",
                                          confirmButtonText: "Ok",
                                        },
                                        function(){
                                            location.reload();
                                        });
                                }
                                if(resp == '"add_fail"'){
                                    swal({
                                          title: "Penambahan Gagal !",
                                          text: "File Tidak Sesuai atau Ukuran Melebihi 2 MB",
                                          type: "warning",
                                          confirmButtonClass: "btn-danger",
                                          confirmButtonText: "Ok",
                                        });
                                }
                            }
                        });
                    });
        }

    }

    function detail_function(task="",count){
        if(task=='getListJemaah'){
            url = '<?php echo base_url()?>administrator/member/getListJemaah/'+$('#detail_member_id').val();
            $.ajax({
                method: 'POST',
                url: url,
                dataType: 'json',
                success : function(resp){
                    $('.row_list_jemaah').remove();
                    for(var i=0;i<resp.length;i++){
                        var x = '<tr class="row_list_jemaah"><td>'+(i+1)+'.</td><td>'+resp[i]['nama_jemaah']+'</td></tr>';
                        $(".start_row_list_jemaah").before(x);
                    }
                }
            });
        } else {
            reset();
            $('.row_list_jemaah').remove();
            $('#collapse_').removeClass('in');
            var avatar_link = '<?php echo base_url() ?>'+$('#avatar_detail_'+count).val();
            $('#detail_avatar').attr('src',avatar_link);


            for(var i=0;i<dom.length;i++){
                if(dom[i]=='member_id'){
                    $('#detail_'+dom[i]).val($('#'+dom[i]+'_detail_'+count).val());
                } else {
                    $('#detail_'+dom[i]).text($('#'+dom[i]+'_detail_'+count).val());
                }
            }
            
            $('.btn_list_jemaah').removeClass('hide');
            if($('#detail_total_jemaah').text()=='0 Jemaah')
                $('.btn_list_jemaah').addClass('hide');
            
                

            $('.form-modal').removeClass('form-md-floating-label');
            $('#detail_modal').modal();
        }
    }

    function reset(){
        //reset
        for(var i=0;i<dom.length;i++){
            $('#'+dom[i]).val("");
        }
    }

    function check_empty(){
        var empty = 0;
            $('input', '#add-form').each(function(){
                if($(this).val() == "" && $(this).attr('id')
                     && $(this).attr('id')!="avatar_select"){
                    empty++;
                }
            })
            $('textarea', '#add-form').each(function(){
                if($(this).val() == "" && $(this).attr('id')){
                    empty++;
                }
            })
            return empty;
    }  
</script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Member
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo base_url().'administrator' ?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Member</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <?php if($login_type == 'Kantor Pusat'){ ?>
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <button id="sample_editable_1_new" onClick="add_function('show')" class="btn btn-sm red">Tambah Member
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> Managed Member</span>
                        </div>
                    </div>
                    <?php if($this->session->flashdata('reset_sahabat_success')){ ?>
                        <div class="col-md-6">
                            <label class="alert alert-success"><i class="fa fa-check"></i> <?php echo $this->session->flashdata('reset_sahabat_success') ?></label>
                        </div>
                    <?php } ?>
                    <div class="portlet-body">
                        
                        <table class="table table-striped table-bordered  table-checkable order-column" id="table-custom">
                            <thead>
                                <tr>
                                    <th> NO </th>
                                    <th> Nama </th>
                                    <th> Email </th>
                                    <th> Total Jemaah </th>
                                    <th> Tanggal Registrasi </th>
                                    <th> Aksi </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if($member > 0){ 
                                    $count=0;
                                    foreach($member as $row){
                                        $id = encrypt($row->member_id);
                                        $nama_lengkap = $row->nama_lengkap;
                                        $email = $row->email ? $row->email :'-' ;
                                        $no_hp = $row->no_hp;
                                        $alamat = $row->alamat;
                                        $kota = $row->kota;
                                        $password = $row->password;
                                        $registrasi = $row->registrasi;
                                        $status_aktif = $row->status_aktif;
                                        $total_jemaah = $row->total_jemaah;
                                        $tgl_regis = $row->tgl_regis;
                                        $tgl_lahir = $row->tgl_lahir;
                                        $avatar = $row->avatar;
                                        $count++;

                                        
                                        $btn_upgrade = '';
                                        $btn_reset_sahabat = 'hide';
                                        
                            ?>
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td>
                                        <?php echo $nama_lengkap; ?>
                                    </td>
                                    <td><a href="<?php echo $row->email; ?>"><i class="fa fa-envelope"></i>&nbsp;<?php echo $email; ?> </td>
                                    <td><i class="fa fa-users"></i>&nbsp;<?php echo $total_jemaah?> Jemaah</td>
                                    <td class="center"><i class="fa fa-calendar"></i>&nbsp;<?php echo $tgl_regis ?> </td>
                                    <td>
                                        <div class="btn-group" >
                                            <button style="width:100%" class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="javascript:;" onClick="detail_function(<?php echo $id ?>,<?php echo $count; ?>);" class="btn_detail<?php echo $id ?>"><i class="fa fa-search"></i>&nbsp;Detail</a></li>
                                              
                                                    <li><a href="javascript:;" onClick="edit_function('show',<?php echo $id ?>,<?php echo $count; ?>);"><i class="fa fa-pencil"></i>&nbsp;Edit</a></li>
                                                    <?php if($total_jemaah==0){ ?>
                                                        <li><a href="javascript:;" onClick="edit_function('delete',<?php echo $id ?>);" class="btn_hapus<?php echo $id ?>"><i class="fa fa-trash"></i>&nbsp;Hapus</a></li>
                                                    <?php } ?>
                                            
                                            </ul>
                                        </div>
                                        <div class="hide">
                                            <input type="text" id="member_id_detail_<?php echo $count; ?>" value="<?php echo $id ?>">
                                            <input type="text" id="nama_lengkap_detail_<?php echo $count; ?>" value="<?php echo $nama_lengkap ?>">
                                            <input type="text" id="email_detail_<?php echo $count; ?>" value="<?php echo $email ?>">
                                            <input type="text" id="no_hp_detail_<?php echo $count; ?>" value="<?php echo $no_hp ?>">
                                            <input type="text" id="alamat_detail_<?php echo $count; ?>" value="<?php echo $alamat ?>">
                                            <input type="text" id="kota_detail_<?php echo $count; ?>" value="<?php echo $kota ?>">
                                            <input type="text" id="registrasi_detail" value="<?php echo $registrasi ?>">
                                            <input type="text" id="status_aktif_detail_<?php echo $count; ?>" value="<?php echo $status_aktif ?>">
                                            <input type="text" id="tgl_lahir_detail_<?php echo $count; ?>" value="<?php echo $tgl_lahir ?>">
                                            <input type="text" id="tgl_regis_detail_<?php echo $count; ?>" value="<?php echo $tgl_regis ?>">
                                            <input type="text" id="avatar_detail_<?php echo $count; ?>" value="<?php echo $avatar ?>">
                                            <input type="text" id="total_jemaah_detail_<?php echo $count; ?>" value="<?php echo $total_jemaah ?> Jemaah">
                                            
                                         </div>
                                    </td>
                                    
                                </tr>
                            <?php } 
                            } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>

        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<!-- add edit Modal-->
<div class="modal fade" id="add_edit_modal"  role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-users font-red"></i>
                    <span class="header-modal caption-subject font-red sbold uppercase">Tambah Member</span>
                </div>
            </div>
            <div class="portlet-body form" >
                <?php echo form_open('administrator', array( 'id' => 'add-form')); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <label class="label label-danger" style="font-size: 13px"><i class="fa fa-info-circle"></i>&nbsp;Optimal Image Resolution : 320 x 183</label>
                                        <label><span class="font-red" style="font-size:13px;">*</span> Foto hanya Optional</label>
                                        <div class="fileinput-new thumbnail">
                                            <img id="avatar" src="<?php echo base_url().'assets/frontend/img/no-avatar.png' ?>" alt=""> 
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> Select image </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="avatar" id="avatar_select"> </span>
                                            <a href="javascript:;" class="btn btn-sm red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                        </div>
                                    </div>
                                </div>
                            <div class="col-md-6">
                                <div class="form-modal form-group">
                                    <label for="form_control_1">Nama</label>
                                    <input type="text" class="form-control" name="nama_lengkap" id="nama_lengkap">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="form_control_1">Tanggal Lahir</label>
                                <div class="form-modal form-group input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="-300y">
                                    <input type="text" class="form-control" name="tgl_lahir" id="tgl_lahir" readonly="" placeholder="dd-mm-yyyy">
                                    <span class="input-group-btn">
                                        <button class="btn default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-modal form-group">
                                    <label for="form_control_1">Handphone</label>
                                    <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" name="no_hp" id="no_hp">
                                </div>
                        </div> 
                            <div class="col-md-6">
                                <div class="form-modal form-group">
                                    <label for="form_control_1">Email</label>
                                    <input type="text"  class="form-control" name="email" id="email">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-modal form-group">
                                    <label for="form_control_1">Alamat</label>
                                    <textarea type="text" rows="1" class="form-control" name="alamat" id="alamat"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-modal form-group">
                                    <label for="form_control_1">Kota</label>
                                    <input type="text" class="form-control" name="kota" id="kota">
                                </div>
                            </div>
                        </div>
                    </div>
                <?php echo form_close();?>
            </div>
            <div class="modal-footer">
                <input type="hidden" id="member_id">
                <button type="button" data-dismiss="modal" class="btn btn-circle grey">Cancel &nbsp;<i class="fa fa-times"></i></button>            
                <button type="button" onClick="add_function('add')" class="btn_add btn btn-circle red hide">Simpan &nbsp;<i class="fa fa-save"></i></button>
                <button type="button" onClick="edit_function('update')" class="btn_update btn btn-circle green hide">Simpan Perubahan &nbsp;<i class="fa fa-save"></i></button>
            </div>
        </div>

    </div>
</div>

<!-- detail Modal-->
<div class="modal fade" id="detail_modal" role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog"  style="width:800px">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-search"></i>
                    <span class="header-modal caption-subject sbold uppercase">Detail Member</span>
                </div>
            </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-4 col-sm-3">
                            <div class="fileinput-new thumbnail">
                                <img id="detail_avatar" src="<?php echo base_url().'asssets/frontend/img/no-avatar.png' ?>" alt=""> 
                            </div> 
                        </div>
                        <div class="col-md-8">
                            <h3><span id="detail_nama_lengkap"></span></h3>
                            <table class="table" style="margin-top:15px">
                                <tr>
                                    <td><i class="icon-calendar"></i>&nbsp; <span>Tanggal Lahir</td>
                                    <td>:</td>
                                    <td><span id="detail_tgl_lahir"></span></td>
                                </tr>
                                <tr>
                                    <td><i class="icon-screen-smartphone"></i>&nbsp; <span>Handphone</td>
                                    <td>:</td>
                                    <td><span id="detail_no_hp"></span></td>
                                </tr>
                                <tr>
                                    <td><i class="icon-envelope-letter"></i>&nbsp; <span>Email</td>
                                    <td>:</td>
                                    <td><span id="detail_email"></span></td>
                                </tr>
                                <tr>
                                    <td><i class="icon-map"></i>&nbsp; <span>Alamat</td>
                                    <td>:</td>
                                    <td><span id="detail_alamat"></span></td>
                                </tr>
                                <tr>
                                    <td><i class="icon-home"></i>&nbsp; <span>Kota</td>
                                    <td>:</td>
                                    <td><span id="detail_kota"></span></td>
                                </tr>
                                <tr>
                                    <td><i class="icon-calendar"></i>&nbsp; <span>Registrasi</td>
                                    <td>:</td>
                                    <td><span id="detail_tgl_regis"></span></td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-users"></i>&nbsp; <span>Total Jemaah</td>
                                    <td>:</td>
                                    <td>
                                        <span id="detail_total_jemaah"></span> | 
                                        <a class="btn_list_jemaah" onClick="detail_function('getListJemaah')" data-toggle="collapse" href="#collapse_">Lihat Detail</a>
                                        <div id="collapse_" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <table class="table">
                                                    <tr class="start_row_list_jemaah hide"></tr>
                                                </table>                                                                                            
                                            </div>
                                        </div>
                                        <input type="hidden" id="detail_member_id">
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-circle grey">Close &nbsp;<i class="fa fa-times"></i></button>            
            </div>
        </div>

    </div>
</div>


