<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        table = $('#table_log').DataTable({ 
            "processing": true, "serverSide": true,"order": [],
            "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
            "ajax": {"url": "<?php echo site_url('administrator/notifikasi/pagination')?>","type": "POST"},
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                        {"targets": [0],"className": "dt-center"}
                    ]
        });       
    });
</script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Notifikasi
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo base_url().'administrator' ?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Notifikasi</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase"> List Notifikasi</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-stripfed table-bordered  table-condensed" id="table_log">
                            <thead>
                                <tr>
                                    <th> NO </th>
                                    <th> Judul </th>
                                    <th> Keterangan </th>
                                    <th> Tanggal </th>
                                    <th> Status </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>

        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->


