<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<link href="<?php echo base_url()?>assets/backend/css/star-rating.min.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url()?>assets/backend/js/star-rating.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/backend/global/plugins/ckeditor/ckeditor.js"></script>
<style type="text/css">

</style>

<?php 
$login_type = $this->session->userdata('login_type');
$img_link = base_url().'assets/frontend/img/noimagefound.jpg';

?>
<script type="text/javascript">

    var num=1;
    var akom_num = 1;
    var pktwkt = 2;
    var pktwkt_kls = 1;



    //CRUD Function
        function add_function(task){
            if(task == 'add'){
                if(cek_all()>0){
                     alert("Pastikan Gambar dan Form data paket sudah terisi");
                     return;
                }
                CKEDITOR.instances.info_plus.updateElement();
                data = new FormData($('#add-form')[0]);
                $('#add_edit_modal').modal('hide');
                
                swal({
                      title: "Menyimpan Data Paket",
                      text: "Klik 'Tambah' untuk memulai.",
                      type: "info",
                      closeOnConfirm: false,
                      confirmButtonText: 'Tambah',
                      showLoaderOnConfirm: true,
                    },
                    function(){
                        $.ajax({
                            method: 'POST',
                            data:data,
                            url: '<?php echo base_url() ?>administrator/paket/add',
                            dataType: 'text',
                            cache: false,
                            contentType: false,
                            processData: false,
                            success : function(resp){
                                if(resp == '"add_success"'){
                                    swal({
                                          title: "Tambah Paket  ",
                                          text: "Penambahan berhasil",
                                          type: "success",
                                          confirmButtonClass: "btn-danger",
                                          confirmButtonText: "Ok",
                                        },
                                        function(){
                                            window.location.href="paket";
                                    });
                                }
                                if(resp == '"add_fail"'){
                                    swal({
                                          title: "Penambahan Gagal !",
                                          text: "File Tidak Sesuai atau Ukuran Melebihi 2 MB",
                                          type: "warning",
                                          confirmButtonClass: "btn-danger",
                                          confirmButtonText: "Ok",
                                    });
                                }
                            }
                        });
                    });
            }
        }

        function edit_function(task='',id='',count=''){

            if(task == 'delete'){
                swal({
                  title: "Hapus Paket?",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "grey",
                  confirmButtonText: "Yes, delete it!",
                  closeOnConfirm: false
                },
                function(){
                    $.ajax({
                        method: 'POST',
                        url: '<?php echo base_url() ?>administrator/paket/delete/'+id,
                        dataType: 'json',
                        success : function(resp){
                            if(resp == 'update_success')
                                swal({
                                      title: "Hapus Paket",
                                      text: "Penghapusan berhasil",
                                      type: "success",
                                      confirmButtonClass: "grey",
                                      confirmButtonText: "Ok",
                                    },
                                    function(){
                                        location.reload();
                                    });
                                
                        }
                    });
                });
            }
        }

        function detail_function(task='',id=''){
            $('#detail_avatar').attr('src','<?php echo $img_link ?>');
            $('#detail_avatar_mobile').attr('src','<?php echo $img_link ?>');
            $('#detail_avatar_href').attr('href','');
            $('#detail_avatar_mobile_href').attr('href','');

            $('.reguler').hide('slow');
            $('.haji').hide('slow');
            url = '<?php echo base_url() ?>administrator/paket/detail/'+id;
            $.ajax({
                method: 'POST',
                url: url,
                dataType: 'json',
                success : function(resp){
                    console.log(resp);
                    $('#detail_avatar').attr('src',resp['paket_pic']);
                    $('#detail_avatar_mobile').attr('src',resp['paket_mobile_pic']);

                    $('#detail_avatar_href').attr('href',resp['paket_pic']);
                    $('#detail_avatar_mobile_href').attr('href',resp['paket_mobile_pic']);

                    if(resp['jenis_paket']=='Haji'){
                        $('.haji').show('slow');
                        $('#detail_nama_paket_haji').text(resp['nama_paket']);
                        $('#detail_jenis_paket_haji').text(resp['jenis_paket']);
                        $('#detail_masa_tunggu').text(resp['masa_tunggu']);
                        $('#detail_harga_terakhir').text(resp['harga_terakhir']);
                        $('#detail_jenis_kuota').text(resp['jenis_quota']);
                        $('#detail_info_plus_haji').html(resp['info_plus']);
                    } 
                    else {
                        $('.reguler').show('slow');
                        $('#detail_nama_paket').text(resp['nama_paket']);
                        $('#detail_jenis_paket').text(resp['jenis_paket']);
                        $('#detail_berangkat_awal').text(resp['berangkat_awal']);
                        $('#detail_berangkat_landing').text(resp['berangkat_landing']);
                        $('#detail_berangkat_transit').text(resp['berangkat_transit']);
                        $('#detail_info_plus').html(resp['info_plus']);
                        $('#detail_lama_hari').text(resp['lama_hari']);
                        $('#detail_mulai_publish').text(resp['mulai_publish']);
                        $('#detail_akhir_publish').text(resp['akhir_publish']);

                        //Maskapai
                        $('.detail_maskapai_avatar').remove();
                        for(var i=0;i<resp['maskapai'].length;i++){
                            var o = '<div class="col-md-12 detail_maskapai_avatar" style="padding:0px">';
                                o+= '   <a data-toggle="collapse" href="#collapse_maskapai_'+i+'"><i class="fa fa-arrow-circle-o-right"></i>&nbsp;'+resp['maskapai'][i]['nama_maskapai']+'</a>';
                                o+= '   <div id="collapse_maskapai_'+i+'" class="panel-collapse collapse">';
                                o+= '       <div class="panel-body">';
                                o+= '           <img class="detail_maskapai_avatar" src="'+resp['maskapai'][i]['logo']+'"><br><br>'; 
                                o+= '           <span id="deskripsi">'+resp['maskapai'][i]['deskripsi']+'</span>';                         
                                o+= '       </div>';
                                o+= '   </div>';
                                o+= '</div>';

                            $('#detail_maskapai').after(o);                 
                            // alert(resp['maskapai'][i]['nama_maskapai']);
                        }

                        //Hotel
                        $('.detail_hotel_list').remove();
                        for(var i=0;i<resp['paket_akomodasi'].length;i++){
                            var o = '<div class="col-md-12 detail_hotel_list" style="padding:0px">';
                                o+= '   <a data-toggle="collapse" href="#collapse_'+i+'"><i class="fa fa-arrow-circle-o-right"></i>&nbsp;'+resp['paket_akomodasi'][i]['nama_hotel']+'</a>';
                                o+= '   <div id="collapse_'+i+'" class="panel-collapse collapse">';
                                o+= '       <div class="panel-body">';
                                o+= '           <input type="number" id="detail_hotel_bintang_'+i+'" class="rating" data-show-clear="false" data-show-caption="false" data-size="xs" data-readOnly="true" value="'+resp['paket_akomodasi'][i]['bintang']+'"/>'
                                o+= '           <img style="width:100%" class="detail_hotel_avatar" src="'+resp['paket_akomodasi'][i]['foto']+'"><br><br>'; 
                                o+= '           <span id="deskripsi">'+resp['paket_akomodasi'][i]['deskripsi']+'</span>';                         
                                o+= '       </div>';
                                o+= '   </div>';
                                o+= '</div>';

                            $('#detail_hotel').after(o);
                            $('#detail_hotel_bintang_'+i+'').rating();
                        }

                        bln_temp='';
                        $('.list_harga').remove();
                        for(var i=0;i<resp['paket_waktu'].length;i++){
                            hrg               = resp['paket_waktu'][i]['harga'];
                            hrg_usd           = resp['paket_waktu'][i]['harga_usd'] ? '$ '+resp['paket_waktu'][i]['harga_usd']+'<br><small style="font-size:10px"><i>(Rate '+resp['paket_waktu'][i]['kurs_dollar']+')</i></small>' : null;
                            $kurs             = resp['paket_waktu'][i]['kurs_dollar'] ? resp['paket_waktu'][i]['kurs_dollar'] : null;
                            kls               = resp['paket_waktu'][i]['kelas'];
                            kmr               = resp['paket_waktu'][i]['jumlah_sekamar'];
                            bln_berangkat     = resp['paket_waktu'][i]['berangkat'];
                            tgl_keberangkatan = resp['paket_waktu'][i]['tgl_berangkat'];
                            var o ='';

                                o+='<tr class="list_harga">';
                                if(bln_berangkat != bln_temp){
                                    o+='<td rowspan="1"><a><i class="fa fa-calendar"></i>&nbsp; '+bln_berangkat+' </a><br><small>Tanggal keberangkatan : <br>'+tgl_keberangkatan+'</small></td>';
                                }
                                else {
                                    o+='<td>&nbsp;</td>';
                                }
                                o+= '   <td>'+kls+'</td>'; 
                                if(hrg_usd)
                                    o+= '   <td>'+hrg_usd+'</td>';
                                o+= '   <td>'+hrg+'</td>'; 
                                o+= '</tr>';

                                bln_temp = bln_berangkat;
                            $('#detail_harga_bulan').before(o);
                            // alert(resp['maskapai'][i]['nama_maskapai']);
                        }
                    }
                }
            });

            $('#detail_modal').modal();
        }

        function cek_all(task='',$form=''){
            var empty = 0;            
            $('input', '#add-form').each(function(){
                var id   = $(this).attr('id');
                var val  = $(this).val();
                var name = $(this).attr('name');

                if(id && id != 'paket_id' && id != 'add_paket_kurs' && id != 'paket_mobile_pic') {
                    if(val == "") {
                        empty++;
                    }
                }

                    
            });
            $('select', '#add-form').each(function(){
                var id  = $(this).attr('id');
                var val = $(this).val();

                if(val == ""){
                    empty++;
                }

                //Jika Harga dalam USD, cek add_paket_kurs
                if(id == 'harga_dalam' && val == 'USD'){
                    if($('#add_paket_kurs').val()==""){
                        alert("Jika Haraga dalam USD, pastikan Kurs Dollar sudah terisi !");
                        empty++;
                    } else {
                        empty--;
                    }
                }
            });
            var x = CKEDITOR.instances.info_plus.getData();
            if(!x)
                empty++;

            return empty;
        }

    //Duplicate and Delete Akomodasi
        function duplicate_akomodasi(){
            akom_num++;
            var clonedDiv  = '     <div class="col-md-12" id="akom_list_'+akom_num+'">';
                clonedDiv += '           <h4 >Akomodasi '+akom_num+'</h4>';
                clonedDiv += '           <div class="form-group col-md-6" id="hotel_'+akom_num+'_div">';
                clonedDiv += '                <select id="hotel_'+akom_num+'" onChange="getDetailHotel('+akom_num+')" class="form-control select2" name="hotel_'+akom_num+'">';
                clonedDiv += '                    <option value="">Pilih Hotel</option>';                
                clonedDiv += '                     <?php $i=0; foreach($hotel as $hot){ $i++;?>';
                clonedDiv += '                        <option value="<?php echo encrypt($hot->hotel_id) ?>"><?php echo $hot->nama_hotel?></option>';
                clonedDiv += '                     <?php } ?>';
                clonedDiv += '                </select>';
                clonedDiv += '           </div>';
                clonedDiv += '   </div>'

            $('#akom_list_'+(akom_num-1)).after(clonedDiv);
            $('#hotel_'+akom_num).select2({placeholder: "Select a state"}).trigger('change');
            $('#hotel_rating_'+akom_num).rating();

        }

        function hapus_akomodasi(){
            if(akom_num!=1){
                $('#akom_list_'+akom_num).remove();
                akom_num--;
            }
        }

    //Duplicate and Delete Keberangkatan
        function duplicate_keberangkatan(){

            var clonedDiv  = '                    <div class="row" id="paketwaktu_list_'+pktwkt+'" style="background: rgb(247, 247, 247);margin:10px;padding:3px">';
                clonedDiv += '                        <div class="col-md-6 no-margin-no-padding">';
                clonedDiv += '                                <div class="col-md-12" style="margin-top:10px;margin-bottom: 10px">Keberangkatan '+pktwkt+'</div>';
                clonedDiv += '                                      <div class="col-md-6">';
                clonedDiv += '                                          <div class="form-group">';
                clonedDiv += '                                               <select class="form-control input-sm" name="bulan_berangkat_'+pktwkt+'" id="bulan_berangkat_'+pktwkt+'">';
                clonedDiv += '                                                  <option value="">Bulan...</option>';
                clonedDiv += '                                                  <?php $i=0; foreach($bulan as $bln){ ?>';
                clonedDiv += '                                                      <option value="<?php echo $i++; ?>"><?php echo $bln?></option>';
                clonedDiv += '                                                  <?php } ?> ';
                clonedDiv += '                                              </select>';
                clonedDiv += '                                          </div>';
                clonedDiv += '                                      </div>';
                clonedDiv += '                                      <div class="col-md-6">';
                clonedDiv += '                                          <div class="form-group">';
                clonedDiv += '                                              <select class="form-control input-sm" name="tahun_berangkat_'+pktwkt+'" id="tahun_berangkat_'+pktwkt+'">';
                clonedDiv += '                                                  <option value="">Tahun...</option>';
                clonedDiv += '                                                  <?php foreach($tahun as $thn){?>';
                clonedDiv += '                                                       <option value="<?php echo $thn ?>"><?php echo $thn?></option>';
                clonedDiv += '                                                 <?php } ?> ';
                clonedDiv += '                                              </select>';
                clonedDiv += '                                          </div>';
                clonedDiv += '                                      </div>';
                clonedDiv += '                                  </div>';


                clonedDiv += '                        <div class="col-md-6 no-margin-no-padding">';
                clonedDiv += '                            <div class="col-md-12" style="margin-top:8px;margin-bottom:10px">';
                clonedDiv += '                                <button type="button" class="btn btn-xs red" onClick="duplicate_kelas('+pktwkt+');"><i class="fa fa-plus"></i>&nbsp;Tambah Kelas</button>';
                clonedDiv += '                                <button type="button" class="btn btn-xs grey" onClick="hapus_kelas('+pktwkt+');"><i class="fa fa-trash"></i>&nbsp;Hapus</button>';
                clonedDiv += '                            </div>';
                clonedDiv += '                            <div id="kelas_form_'+pktwkt+'_1">';
                clonedDiv += '                            <div class="col-md-6">';
                clonedDiv += '                                <div class="form-group">';
                clonedDiv += '                                    <select class="form-control input-sm" name="kelas_'+pktwkt+'_1" id="kelas_'+pktwkt+'_1">';
                clonedDiv += '                                        <option value="">Kelas...</option>';
                clonedDiv += '                                        <?php foreach($kelas as $kls){?>';
                clonedDiv += '                                            <option value="<?php echo $kls->kelas_id ?>"><?php echo $kls->kelas?></option>';
                clonedDiv += '                                        <?php } ?> ';
                clonedDiv += '                                    </select>';
                clonedDiv += '                                </div>';
                clonedDiv += '                            </div>';
                clonedDiv += '                            <div class="col-md-6">';
                clonedDiv += '                                <div class="form-group">';
                clonedDiv += '                                    <input type="text" placeholder="Harga" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" onkeypress="return event.charCode >= 45 && event.charCode <= 57" class="form-control input-sm" name="harga_'+pktwkt+'_1" id="harga_'+pktwkt+'_1">';
                clonedDiv += '                                </div>';
                clonedDiv += '                            </div>';
                clonedDiv += '                            </div>';
                clonedDiv += '                        </div>';
                clonedDiv += '                        <span class="hide">Keberangkatan ke <input id="pktwkt_num_'+pktwkt+'" type="hidden" value="'+pktwkt+'">';
                clonedDiv += '                        Kelas ke <input id="kelas_num_based_pktwkt_'+pktwkt+'" type="hidden" value="1"></span>';            
                clonedDiv += '                    </div>';

            $('#paketwaktu_list_'+(pktwkt-1)+'').before(clonedDiv);
            pktwkt++;
            $( ".tgl_keberangkatan" ).each(function() {
                    $( this ).datepicker();
            });
        }

        function hapus_keberangkatan(){
            if(pktwkt != 1){
                pktwkt--;
                $('#paketwaktu_list_'+pktwkt).remove();
                
            } 
        }

    //Duplicate and Delete Kelas
        function duplicate_kelas(pktwkt_num){
            kls = $('#kelas_num_based_pktwkt_'+pktwkt_num).val();
            kls++;
            // alert('Tambah untuk Keberangkatan '+pktwkt_num+' - Kelas '+kls);
            var clonedDiv = '                            <div id="kelas_form_'+pktwkt_num+'_'+kls+'">';
                clonedDiv += '                            <div class="col-md-6">';
                clonedDiv += '                                <div class="form-group">';
                clonedDiv += '                                    <select class="form-control input-sm" name="kelas_'+pktwkt_num+'_'+kls+'" id="kelas_'+pktwkt_num+'_'+kls+'">';
                clonedDiv += '                                        <option value="">Kelas...</option>';
                clonedDiv += '                                        <?php foreach($kelas as $kls){?>';
                clonedDiv += '                                            <option value="<?php echo $kls->kelas_id ?>"><?php echo $kls->kelas?></option>';
                clonedDiv += '                                        <?php } ?> ';
                clonedDiv += '                                    </select>';
                clonedDiv += '                                </div>';
                clonedDiv += '                            </div>';
                clonedDiv += '                            <div class="col-md-6">';
                clonedDiv += '                                <div class="form-group">';
                clonedDiv += '                                    <input type="text" placeholder="Harga" class="form-control input-sm" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" onkeypress="return event.charCode >= 45 && event.charCode <= 57" name="harga_'+pktwkt_num+'_'+kls+'" id="harga_'+pktwkt_num+'_'+kls+'">';
                clonedDiv += '                                </div>';
                clonedDiv += '                            </div>';
                clonedDiv += '                            </div>';
            $('#kelas_form_'+pktwkt_num+'_'+(kls-1)).before(clonedDiv);
            $('#kelas_num_based_pktwkt_'+pktwkt_num).val(kls);
        }

        function hapus_kelas(pktwkt_num){
            kls = $('#kelas_num_based_pktwkt_'+pktwkt_num).val();
            kls--;
            if(kls!='0'){
                kls++;
                // alert(pktwkt_num+'_'+kls);
                $('#kelas_form_'+pktwkt_num+'_'+kls).remove();
                kls--;
                $('#kelas_num_based_pktwkt_'+pktwkt_num).val(kls);
            }
        }

    //Auto Complete Function 
        function substringMatcher(strs){
            return function findMatches(q, cb) {
                var matches, substringRegex;
                matches = [];
                substrRegex = new RegExp(q, 'i');
                $.each(strs, function(i, str) {
                  if (substrRegex.test(str)) {
                    matches.push(str);
                  }
                });
                cb(matches);
              };
        }

        function setAutocomplete(){
            $('#transit #berangkat_transit').typeahead('val','');
            $('#landing #berangkat_landing').typeahead('val','');
            //Pengambilan data untuk form autocomplete
                $.ajax({
                    method: 'POST',
                    url: '<?php echo base_url() ?>administrator/paket/getAutoCompleteData',
                    dataType: 'json',
                    success : function(resp){

                        $('#transit #berangkat_transit').typeahead({
                          hint: false,highlight: true,minLength: 1},{source: substringMatcher(resp[2]['transit'])});

                        $('#landing #berangkat_landing').typeahead({
                          hint: false,highlight: true,minLength: 1},{source: substringMatcher(resp[1]['landing'])});
                    }
                });
        }

    function getDetailHotel(pos){
        $('#hotel_keterangan_'+pos).remove();
        val = $('#hotel_'+pos).val();
        $.ajax({
                method: 'POST',
                url: '<?php echo base_url() ?>administrator/paket/getDetailHotel/'+val,
                dataType: 'json',
                success : function(resp){
                    if(resp){
                        var x =' <div class="col-md-12" id="hotel_keterangan_'+pos+'"><table class="table"><tr ">';
                            x +='     <td style="width:50px"><input type="number" id="hotel_rating_'+pos+'" class="rating" data-show-clear="false" data-show-caption="false" data-size="xs" data-readOnly="true" value="'+resp[0]['bintang']+'"/></td>';
                            x +='     <td><span>'+resp[0]['deskripsi']+'</span></td>';
                            x +=' </tr></table></div>';
                        $('#hotel_'+pos+'_div').after(x);
                        $('#hotel_rating_'+pos).rating();
                    }
                }
            });
    }

    function setPromoPaket(id){
        val = "Tidak";
        if ($('#paket_promo_'+id).is(":checked")){
            val='Ya';
        }
        pwk_id = $('#pwk_id_'+id).val();
        $.ajax({
                method: 'POST',
                url: '<?php echo base_url() ?>administrator/paket/setPromoPaket/',
                data : 'pwk_id='+pwk_id+'&val='+val,
                dataType: 'json',
                success : function(resp){
                    if(resp){
                        swal({
                              title: "Berhasil",
                              text: "Aksi berhasil dilakukan",
                              type: "success",
                              timer: 800,
                            showConfirmButton : false
                          })
                    }
                }
            });
    }

    function simpanDataPaket(){
        var empty = 0;            
        $('input', '#datapaket-form').each(function(){
            if($(this).val() == "" && $(this).attr('id')!='paket_id' && $(this).attr('id')) {
                empty++;
                if( $(this).attr('id')=='paket_pic' || 
                    $(this).attr('id')=='paket_mobile_pic' ||
                    ($('#harga_dalam').val()=='IDR' && $(this).attr('id')=='add_paket_kurs')
                ){
                    empty--;
                }
            }
        })
        $('select', '#datapaket-form').each(function(){
            if($(this).val() == ""){
                empty++;
            }
        })
        if(empty>0)
            return alert("Form Data Paket Belum Lengkap");

        CKEDITOR.instances.info_plus.updateElement();
        var x = CKEDITOR.instances.info_plus.getData();
            if(!x)
                return alert("Form Itinerary Belum Lengkap");

        swal({
              title: "Memperbaharui Data Paket",
              text: "Klik 'update' untuk memulai.",
              type: "info",
              closeOnConfirm: false,
              confirmButtonText: 'Update',
              showLoaderOnConfirm: true,
            },
            function(){
                $.ajax({
                    method: 'POST',
                    data: new FormData($('#datapaket-form')[0]),
                    url: '<?php echo base_url() ?>administrator/paket/simpanDataPaket',
                    dataType: 'text',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success : function(resp){
                        if(resp == '"update_success"'){
                            swal({
                              title: "Update Paket  ",
                              text: "Pembaharuan berhasil",
                              type: "success",
                              showConfirmButton : false,
                              timer:800
                            });
                        }
                        if(resp == '"add_fail"'){
                            swal({
                                title: "Penambahan Gagal !",
                                text: "File Tidak Sesuai atau Ukuran Melebihi 2 MB",
                                type: "warning",
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Ok",
                            });
                        }
                    }
                });
            });
    }

    function simpanKeberangkatan(task,pos){
        paket_id = $('#paket_id_'+pos).val();
        pwk_id = $('#pwk_id_'+pos).val();
        paketwaktu_id = $('#paketwaktu_id_'+pos).val();
        if(task=='update' || task == 'add'){
            bulan      = $('#bulan_'+pos).val();
            tahun      = $('#tahun_'+pos).val();
            kelas      = $('#kelas_'+pos).val();
            harga_idr  = $('#harga_idr_'+pos).val();
            harga_usd  = $('#harga_usd_'+pos).val();
            kurs_dollar= $('#kurs_dollar_'+pos).val();
            diskon     = $('#diskon_'+pos).val();
            bulan_text = $('#bulan_'+pos+' option:selected').text();
            kelas_text = $('#kelas_'+pos+' option:selected').text();

            if(bulan==""||tahun==""||kelas==""||harga_idr=="")
                return alert('Form Keberangkatan Belum Lengkap');

            data = 'task='+task+'&paket_id='+paket_id+'&pwk_id='+pwk_id+'&paketwaktu_id='+paketwaktu_id+'&bulan='+bulan+'&tahun='+tahun+'&kelas_id='+kelas+'&harga_idr='+harga_idr+'&harga_usd='+harga_usd+'&kurs_dollar='+kurs_dollar+'&diskon='+diskon;
        } else if(task == 'delete'){
            data = 'task='+task+'&pwk_id='+pwk_id+'&paketwaktu_id='+paketwaktu_id;
        }
        $.ajax({
                method: 'POST',
                url: '<?php echo base_url() ?>administrator/paket/simpanKeberangkatan/',
                data : data,
                dataType: 'json',
                success : function(resp){
                    if(resp){
                        if(task=='add')
                            location.reload();
                        else {
                            swal({
                                title: "Berhasil",
                                text: "Aksi berhasil dilakukan",
                                type: "success",
                                timer: 800,
                                showConfirmButton : false})

                            if(task=='update'){
                                $('#bulan_text_'+pos).text(bulan_text);
                                $('#tahun_text_'+pos).text(tahun);
                                $('#kelas_text_'+pos).text(kelas_text);
                                $('#harga_idr_text_'+pos).text(harga_idr);
                                $('#harga_usd_text_'+pos).text(harga_usd);
                                $('#kurs_dollar_'+pos).text(kurs_dollar);
                                $('#diskon_text_'+pos).text(diskon);
                                toggleKeberangkatanForm(pos);
                            } else if(task == 'delete'){
                                $('#list_keberangkatan_'+pos).hide('slow');
                            }
                        }
                    }
                }
            });
    }

    function hitungRupiah(pos){
        var usd  = $('#harga_usd_'+pos).val();
        var kurs = $('#kurs_dollar_'+pos).val();
        var x    = usd*kurs;
        $('#harga_idr_'+pos).val(x.formatMoney(0, ',', '.'));
    }

    function toggleKeberangkatanForm(pos){
        $('#bulan_'+pos).toggle();
        $('#tahun_'+pos).toggle();
        $('#kelas_'+pos).toggle();
        $('#harga_idr_'+pos).toggle();
        $('#harga_usd_'+pos).toggle();
        $('#kurs_dollar_'+pos).toggle();
        $('#diskon_'+pos).toggle();
        // $('.keberangkatan_form').toggle();
        $('.keberangkatan_form_text_'+pos).toggle();
        $('#keberangkatan_form_btnEdit_'+pos).toggle();
        $('#keberangkatan_form_btnSave_'+pos).toggle();
        $('#keberangkatan_form_btnCancel_'+pos).toggle();
        $('#keberangkatan_form_btnHapus_'+pos).toggle();
    }

    function toggleAddKeberangkatanForm(){
        $('#list_keberangkatan_0').toggle('slow');
        $('.AddKeberangkatanForm_btnCancel').toggle('slow');
    }

    function toggleKursInput(){
        $('.kurs_input').toggle('slow');
        $('#add_paket_kurs').val('');
    }

    $(document).ready(function(){

        setAutocomplete();
        $('.akhir_publish').datetimepicker({
            multidate: false
        });

        // CKEDITOR.replace( 'info_plus' );

        $('.keberangkatan_form').hide();
        $('.diskon_form').hide();
        $('.keberangkatan_form_btnSave').hide();
        $('.keberangkatan_form_btnCancel').hide();
        $('.diskon_form_btnSave').hide();
        $('.diskon_form_btnCancel').hide();        
        $('.AddKeberangkatanForm_btnCancel').hide();
        $('#list_keberangkatan_0').hide();
    });

</script>
<style type="text/css">
    .backdrop-form{background: rgb(247, 247, 247);margin:10px;padding:10px 10px 0px 10px}
    .no-margin-no-padding {margin:0px;padding:0px}
    .noPadding {padding:0px;}
    .noMargin {margin:0px;}
</style>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Paket
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo base_url().'administrator' ?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Paket</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <?php if($form_paket){ ?>
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a href="<?php echo base_url().'administrator/paket/' ?>" class="btn btn-sm yellow-crusta">Kembali <i class="fa fa-arrow-circle-o-left"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-users font-red"></i>
                                <span class="header-modal caption-subject font-red sbold uppercase"><?php echo $caption ?></span>
                            </div>
                        </div>
                        <div class="portlet-body form col-md-12" >
                            <?php echo form_open('administrator', array( 'id' => 'add-form')); ?>
                                <div class="col-md-6 col-sm-12">
                                    <h2>Data Paket</h2>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <label class="label label-danger" style="font-size:12px"><i class="fa fa-info-circle"></i>&nbsp;Optimal Image Resolution : 650 x 900</label>
                                                    <div class="fileinput-new thumbnail" >
                                                        <img id="paket_pic_detail" src="<?php echo $img_link ?>" alt=""> 
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style=" line-height: 10px;"></div>
                                                    <div>
                                                        <span class="btn default btn-file btn-sm">
                                                            <span class="fileinput-new"> Select image for Web </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="paket_pic" id="paket_pic" required> </span>
                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <label class="label label-danger" style="font-size:12px"><i class="fa fa-info-circle"></i>&nbsp;Optimal Image Resolution : 650 x 370</label>
                                                    <div class="fileinput-new thumbnail" >
                                                        <img id="paket_pic_detail" src="<?php echo $img_link ?>" alt=""> 
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style=" line-height: 10px;"></div>
                                                    <div>
                                                        <span class="btn default btn-file btn-sm">
                                                            <span class="fileinput-new"> Select image for Mobile</span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="paket_mobile_pic" id="paket_mobile_pic" required> </span>
                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12">
                                            <hr>
                                                <div class="col-md-12">
                                                    <div class=" form-group">
                                                        <label for="form_control_1">Nama Paket</label>
                                                        <input type="text" class="form-control input-sm" name="nama_paket" id="nama_paket" placeholder="ex. Umroh Promo" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12" >
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="form_control_1">Jenis Paket</label>
                                                        <select class="form-control input-sm" name="jenispaket_id" id="jenispaket_id" required>
                                                        <option value="" selected>...</option>
                                                        <?php foreach($jenis_paket as $jp){ 
                                                            if($jp->jenis_paket!='Haji'){
                                                        ?>
                                                            <option value="<?php echo encrypt($jp->jenispaket_id) ?>"><?php echo $jp->jenis_paket ?></option>
                                                        <?php }
                                                        } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class=" form-group">
                                                        <label for="form_control_1">Jumlah Hari</label>
                                                        <input type="text" placeholder="ex. 12" onkeypress='return event.charCode >= 45 && event.charCode <= 57' class="form-control input-sm" name="lama_hari" id="lama_hari" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-md-6">
                                                    <div class=" form-group form-md-line-input input-group date form_datetime bs-datetime " data-date-format="dd-mm-yyyy " data-date-start-date="-300y">
                                                        <input type="text" class="form-control" name="mulai_publish" id="mulai_publish" readonly="" placeholder="dd-mm-yyyy">
                                                        <span class="input-group-addon">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </span>
                                                        <label for="form_control_1" style="color: #e73d4a">Mulai Publish Paket</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class=" form-group form-md-line-input input-group date bs-datetime form_datetime" data-date-format="dd-mm-yyyy" data-date-start-date="-300y">
                                                        <input type="text" class="form-control" name="akhir_publish" id="akhir_publish" readonly="" placeholder="dd-mm-yyyy">
                                                        <span class="input-group-addon">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </span>
                                                        <label for="form_control_1" style="color: #e73d4a">Batas Akhir Publish Paket</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      
                                    <div class="col-md-12 col-sm-12">
                                        <h2>Penerbangan & Hotel</h2>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12" >
                                                        <div class="row" style="background: rgb(247, 247, 247);margin:10px;padding:10px">
                                                            <div class="col-md-12">
                                                                <div id="penerbangan" class="">
                                                                    <div class="form-group">
                                                                        <label for="single" class="control-label">Maskapai <span style="font-size:12px">(Bisa lebih dari satu)</span></label>
                                                                        <div class="input-group select2-bootstrap-append">
                                                                            <select class="form-control select2" id="multi2" name="maskapai[]" multiple>
                                                                                    <option value=""></option>
                                                                                <?php $i=0; foreach($maskapai_penerbangan as $mask){ $i++;?>
                                                                                    <option value="<?php echo encrypt($mask->maskapai_id) ?>"><?php echo $mask->nama_maskapai.' ('.$mask->kode_maskapai.')'?></option>
                                                                                <?php } ?> 
                                                                            </select>
                                                                            <span class="input-group-btn">
                                                                                <button class="btn btn-default" type="button" data-select2-open="multi-append">
                                                                                    <span class="glyphicon glyphicon-search"></span>
                                                                                </button>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div id="keberangkatan" class="">
                                                                    <div class="form-group">
                                                                        <label for="single" class="control-label">Kota Berangkat Awal</label>
                                                                        <select id="single" class="form-control select2" name="berangkat_awal">
                                                                            <option value=""></option>
                                                                            <?php $i=0; foreach($kota_keberangkatan['data'] as $row){ $i++;?>
                                                                                <option value="<?php echo encrypt($row->kota_id) ?>"><?php echo '('.$row->kode_kota.') '.$row->nama_kota?></option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>   
                                                            <div class="col-md-4">
                                                                <div id="transit" class=" form-group">
                                                                    <label for="form_control_1">Transit</label>
                                                                    <input type="text" class="form-control" name="berangkat_transit" id="berangkat_transit" placeholder="ex. Padang">
                                                                </div>
                                                            </div> 
                                                            <div class="col-md-4">
                                                                <div id="landing" class=" form-group">
                                                                    <label for="form_control_1">Landing</label>
                                                                    <input type="text" class="form-control" name="berangkat_landing" id="berangkat_landing" placeholder="ex. Jeddah">
                                                                    
                                                                </div>
                                                            </div> 
                                                            <div class="col-md-12">
                                                                <div id="penerbangan" class=" form-group ">
                                                                    <label for="single" class="control-label">Hotel <span style="font-size:12px">(Bisa lebih dari satu)</span></label>
                                                                    <div class="input-group select2-bootstrap-append">
                                                                        <select class="form-control select2" id="hotel" name="hotel[]" multiple>
                                                                            <?php $i=0; foreach($hotel as $htl){ $i++;?>
                                                                                <option value="<?php echo encrypt($htl->hotel_id) ?>"><?php echo $htl->nama_hotel?></option>
                                                                            <?php } ?> 
                                                                        </select>
                                                                        <span class="input-group-btn">
                                                                            <button class="btn btn-default" type="button" data-select2-open="multi-append">
                                                                                <span class="glyphicon glyphicon-search"></span>
                                                                            </button>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div> 
                                    </div>                                 
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <h2>Itinerary</h2>
                                    <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <label class="backdrop-form" style="margin:0px;width:100%"><i class="fa fa-warning"></i>&nbsp;Silahkan tulis fitur, bonus, diskon atau info lain terkait paket ini</label>
                                                        <textarea type="text" class="ckeditor" rows="2" class="form-control" name="info_plus" id="info_plus" placeholder="Itinerary"></textarea>
                                                    </div>
                                                </div>
                                            </div>  
                                        </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <h2>Keberangkatan </h2>
                                    <div class="row" >
                                        <div class="col-md-12 col-sm-12">
                                            <button type="button" class="btn btn-xs red" onClick="duplicate_keberangkatan();"><i class="fa fa-plus"></i>&nbsp;Tambah Keberangkatan</button>
                                            <button type="button" class="btn btn-xs grey" onClick="hapus_keberangkatan();"><i class="fa fa-trash"></i>&nbsp;Hapus</button>
                                            <div class="pull-right">
                                                <span>Harga dalam : </span>
                                                <select id="harga_dalam" onchange="toggleKursInput(this.value)">
                                                    <option value="IDR">Rupiah (IDR)</option>
                                                    <option value="USD">Dollar (USD)</option>
                                                </select>
                                                <span class="kurs_input display-none"> | Kurs dollar : <input type="text" style="width:80px" id="add_paket_kurs" name="add_paket_kurs" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" onkeypress='return event.charCode >= 45 && event.charCode <= 57'></span>
                                            </div>
                                            <hr>
                                            <div class="clearfix"></div>
                                            <div class="kurs_input display-none pull-right">
                                                <span class="label bg-grey font-dark"><i class="fa fa-info-circle"></i> Isi kolom <b>HARGA</b> dengan nilai USD. Nilai <b>RUPIAH</b> didapat dari <b>USD</b> x <b>KURS DOLLAR</b></span>
                                            </div>

                                        </div>
                                        <div class="col-md-12" >
                                            <div class="row" id="paketwaktu_form no-margin-no-padding">
                                                <div class="row" id="paketwaktu_list_1" style="background: rgb(247, 247, 247);margin:10px;padding:3px">
                                                    <div class="col-md-6 no-margin-no-padding">
                                                        <div class="col-md-12" style="margin-top:10px;margin-bottom: 10px">
                                                            <span>Keberangkatan 1</span>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <select class="form-control input-sm" name="bulan_berangkat_1" id="bulan_berangkat_1">
                                                                    <option value="">Bulan...</option>
                                                                    <?php $i=0; foreach($bulan as $bln){ ;?>
                                                                        <option value="<?php echo $i++ ?>"><?php echo $bln?></option>
                                                                    <?php } ?> 
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <select class="form-control input-sm" name="tahun_berangkat_1" id="tahun_berangkat_1">
                                                                    <option value="">Tahun...</option>
                                                                    <?php foreach($tahun as $thn){?>
                                                                        <option value="<?php echo $thn ?>"><?php echo $thn?></option>
                                                                    <?php } ?> 
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                    
                                                    <div class="col-md-6 no-margin-no-padding">
                                                        <div class="col-md-12" style="margin-top:8px;margin-bottom: 10px">
                                                            <button type="button" class="btn btn-xs red" onClick="duplicate_kelas(1);"><i class="fa fa-plus"></i>&nbsp;Tambah Kelas</button>
                                                            <button type="button" class="btn btn-xs grey" onClick="hapus_kelas(1);"><i class="fa fa-trash"></i>&nbsp;Hapus</button>
                                                        </div>
                                                        <div id="kelas_form_1_1">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <select class="form-control input-sm" name="kelas_1_1" id="kelas_1_1">
                                                                        <option value="">Kelas...</option>
                                                                        <?php foreach($kelas as $kls){?>
                                                                            <option value="<?php echo $kls->kelas_id ?>"><?php echo $kls->kelas?></option>
                                                                        <?php } ?> 
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <input type="text" placeholder="Harga" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" onkeypress='return event.charCode >= 45 && event.charCode <= 57' class="form-control input-sm" name="harga_1_1" id="harga_1_1">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input id="kelas_num_based_pktwkt_1" type="hidden" value="1">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php echo form_close();?>                             
                        </div>
                        <div class="clearfix"></div>
                        <hr>
                        <div class="modal-footer" style="border-top:0px">
                            <input type="text" class="hide" id="paket_id">
                            <button type="button" onClick="add_function('add')" class="btn-add btn btn-circle red">Simpan Paket &nbsp;<i class="fa fa-save"></i></button>
                        </div>
                    </div>
                
                <?php } else if($form_haji){ ?>
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a href="<?php echo base_url().'administrator/paket/' ?>" class="btn btn-sm yellow-crusta">Kembali <i class="fa fa-arrow-circle-o-left"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-users font-blue"></i>
                                <span class="header-modal caption-subject font-blue sbold uppercase"><?php echo $caption ?></span>
                            </div>
                        </div>
                        <div class="portlet-body form col-md-12" >
                            <?php echo form_open('administrator', array( 'id' => 'add-form')); ?>
                                <div class="col-md-6 col-sm-12">
                                    <h2>Data Paket Haji</h2>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <label class="label label-info" style="font-size:12px"><i class="fa fa-info-circle"></i>&nbsp;Optimal Image Resolution : 650 x 900</label>
                                                <div class="fileinput-new thumbnail" >
                                                    <img id="paket_pic_detail" src="<?php echo $img_link ?>" alt=""> 
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style=" line-height: 10px;"></div>
                                                <div>
                                                    <span class="btn default btn-file btn-sm">
                                                        <span class="fileinput-new"> Select image for Web </span>
                                                        <span class="fileinput-exists"> Change </span>
                                                        <input type="file" name="paket_pic" id="paket_pic" required> </span>
                                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <label class="label label-info" style="font-size:12px"><i class="fa fa-info-circle"></i>&nbsp;Optimal Image Resolution : 650 x 370</label>
                                                <div class="fileinput-new thumbnail" >
                                                    <img id="paket_pic_detail" src="<?php echo $img_link ?>" alt=""> 
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style=" line-height: 10px;"></div>
                                                <div>
                                                    <span class="btn default btn-file btn-sm">
                                                        <span class="fileinput-new"> Select image for Mobile</span>
                                                        <span class="fileinput-exists"> Change </span>
                                                        <input type="file" name="paket_mobile_pic" id="paket_mobile_pic" required> </span>
                                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="col-md-12 col-sm-12">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <hr>
                                                    <div class="col-md-6">
                                                        <div class=" form-group">
                                                            <label for="form_control_1">Nama Paket</label>
                                                            <input type="text" class="form-control" name="nama_paket_haji" id="nama_paket" placeholder="ex. Haji Berkah" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="form_control_1">Jenis Quota</label>
                                                            <select class="form-control" name="jenis_kuota" id="jenis_kuota" required>
                                                                <option value="" selected disabled>Pilih Jenis Kuota</option>
                                                                <option value="Haji Reguler">Haji Reguler</option>
                                                                <option value="Haji Plus">Haji Plus</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class=" form-group">
                                                            <label for="form_control_1">Masa Tunggu (Tahun)</label>
                                                            <input type="text" placeholder="ex. 12" onkeypress='return event.charCode >= 45 && event.charCode <= 57' class="form-control" name="masa_tunggu" id="lama_hari" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12" >
                                                    <div class="kurs_input display-none pull-right">
                                                        <span class="label bg-grey font-dark"><i class="fa fa-info-circle"></i> Isi kolom <b>HARGA</b> dengan nilai USD. Nilai <b>RUPIAH</b> didapat dari <b>USD</b> x <b>KURS DOLLAR</b></span>
                                                        <div class="clearfix"></div>
                                                        <hr>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="pull-left">
                                                            <span>Harga dalam : </span>
                                                            <select id="harga_dalam"  onchange="toggleKursInput(this.value)">
                                                                <option value="IDR">Rupiah (IDR)</option>
                                                                <option value="USD">Dollar (USD)</option>
                                                            </select>
                                                            <span class="kurs_input display-none"> | Kurs dollar : <input type="text" style="width:80px" id="add_paket_kurs" name="add_paket_kurs" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" onkeypress='return event.charCode >= 45 && event.charCode <= 57'></span>
                                                        </div>
                                                        <div class="pull-right">
                                                            <input type="text" onkeydown="return numbersonly(this, event);" placeholder="Harga" onkeyup="javascript:tandaPemisahTitik(this);" onkeypress='return event.charCode >= 45 && event.charCode <= 57' class="form-control input-large pull-left" name="harga_terakhir" id="harga_terakhir">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>                                 
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <h2>Itinerary </h2>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                    <label class="backdrop-form" style="font-size:12px;margin:0px;line-height:0px;width:100%"><i class="fa fa-warning"></i>&nbsp;Silahkan tulis fitur, bonus, diskon atau info lain terkait paket ini</label>
                                                    <textarea type="text" class="ckeditor" rows="2" class="form-control" name="info_plus" id="info_plus" placeholder="Itinerary"></textarea>
                                               
                                            </div>
                                        </div>  
                                    </div>                                    
                                </div>
                            <?php echo form_close();?>                             
                        </div>
                        <div class="clearfix"></div>
                        <hr>
                        <div class="modal-footer" style="border-top:0px">
                            <input type="text" class="hide" id="paket_id">
                            <button type="button" onClick="add_function('add')" class="btn-add btn btn-circle blue">Simpan Paket Haji &nbsp;<i class="fa fa-save"></i></button>
                        </div>
                    </div>                
               
                <?php } else if($edit_paket){ ?> 
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a href="<?php echo base_url().'administrator/paket/' ?>" class="btn btn-sm yellow-crusta">Kembali <i class="fa fa-arrow-circle-o-left"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet light ">
                        <?php $dt = $edit_paket;//echo_array($dt) ?>
                        <div class="portlet-body" >
                            <div class="col-md-12">
                                <div class="clearfix"></div>
                                <hr>
                                <h2>Data Paket, Hotel, Penerbangan & Itinerary</h2><br>
                                <?php echo form_open('administrator', array( 'id' => 'datapaket-form')); ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-5 noPadding">
                                                <div class="col-md-12">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <label class="label label-danger" style="font-size:12px"><i class="fa fa-info-circle"></i>&nbsp;Optimal Image Resolution : 650 x 900</label><br>
                                                        <div class="fileinput-new thumbnail" >
                                                            <img id="paket_pic_detail"  style="width:100%" src="<?php echo $dt['paket_pic'] ?>" alt=""> 
                                                        </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style=" line-height: 10px;"></div>
                                                        <div>
                                                            <span class="btn default btn-file btn-sm">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="paket_pic" id="paket_pic" required> </span>
                                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                <div class="clearfix"></div>
                                                <hr>
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <label class="label label-danger" style="font-size:12px"><i class="fa fa-info-circle"></i>&nbsp;Optimal Image Resolution : 650 x 370</label><br>
                                                        <div class="fileinput-new thumbnail" >
                                                            <img id="paket_pic_detail" style="width:100%" src="<?php echo $dt['paket_mobile_pic'] ?>" alt=""> 
                                                        </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style=" line-height: 10px;"></div>
                                                        <div>
                                                            <span class="btn default btn-file btn-sm">
                                                                <span class="fileinput-new"> Select image for Mobile</span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="paket_mobile_pic" id="paket_mobile_pic" required> </span>
                                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-7 noPadding">
                                                <div class="col-md-12 noPadding" >
                                                    <div class="col-md-6">
                                                        <div class=" form-group">
                                                            <label for="form_control_1">Nama Paket</label>
                                                            <input type="text" class="form-control input-sm" name="nama_paket" id="nama_paket" value="<?php echo $dt['nama_paket'] ?>" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="form_control_1">Jenis Paket</label>
                                                            <select class="form-control input-sm" name="jenispaket_id" id="jenispaket_id" required>
                                                            <option value=""></option>
                                                            <?php foreach($jenis_paket as $jp){ ?>
                                                                <option value="<?php echo encrypt($jp->jenispaket_id) ?>" <?php if($dt['jenispaket_id'] == encrypt($jp->jenispaket_id)){echo "selected";} ?>><?php echo $jp->jenis_paket ?></option>
                                                            <?php } ?>

                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class=" form-group">
                                                            <label for="form_control_1">Jumlah Hari</label>
                                                            <input type="text" onkeypress='return event.charCode >= 45 && event.charCode <= 57' class="form-control input-sm" name="lama_hari" id="lama_hari" value="<?php echo $dt['lama_hari'] ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 noPadding" >
                                                    <div class="col-md-6">
                                                        <div class=" form-group form-md-line-input input-group date form_datetime bs-datetime " data-date-format="dd-mm-yyyy " data-date-start-date="-300y"  style="margin-bottom: 20px;">
                                                            <input type="text" class="form-control input-sm" name="mulai_publish" id="mulai_publish" value="<?php echo date('Y-m-d H:i:s',strtotime($dt['mulai_publish'])) ?>" readonly="" placeholder="dd-mm-yyyy">
                                                            <span class="input-group-addon">
                                                                <button class="btn btn-sm default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                            <label for="form_control_1" style="color: #e73d4a">Mulai Publish Paket</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class=" form-group form-md-line-input input-group date bs-datetime form_datetime" data-date-format="dd-mm-yyyy" data-date-start-date="-300y"  style="margin-bottom: 20px;">
                                                            <input type="text" class="form-control input-sm" name="akhir_publish" id="akhir_publish" value="<?php echo date('Y-m-d H:i:s',strtotime($dt['akhir_publish'])) ?>" readonly="" placeholder="dd-mm-yyyy">
                                                            <span class="input-group-addon">
                                                                <button class="btn btn-sm default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                            <label for="form_control_1" style="color: #e73d4a">Batas Akhir Publish Paket</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div id="keberangkatan" class=" form-group">
                                                            <div class="form-group">
                                                                <label for="single" class="control-label">Berangkat Awal</label>
                                                                <select class="form-control select2" name="berangkat_awal" id="berangkat_awal" required>
                                                                <?php foreach($kota_keberangkatan['data'] as $kk){ ?>
                                                                    <option value="<?php echo encrypt($kk->kota_id) ?>" <?php if(encrypt($kk->kota_id) == $dt['kota_id']){echo 'selected';} ?>><?php echo '('.$kk->kode_kota.') '.$kk->nama_kota ?></option>
                                                                <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                    <div class="col-md-4">
                                                        <div id="transit" class=" form-group">
                                                            <label for="form_control_1">Transit</label>
                                                            <input type="text" class="form-control" name="berangkat_transit" value="<?php echo $dt['berangkat_transit'] ?>" id="berangkat_transit" placeholder="ex. Padang">
                                                        </div>
                                                    </div> 
                                                    <div class="col-md-4">
                                                        <div id="landing" class=" form-group">
                                                            <label for="form_control_1">Landing</label>
                                                            <input type="text" class="form-control" name="berangkat_landing" value="<?php echo $dt['berangkat_landing'] ?>" id="berangkat_landing" placeholder="ex. Jeddah">
                                                        </div>
                                                    </div> 
                                                    <div class="col-md-12">
                                                        <div id="penerbangan" class=" form-group ">
                                                            <label for="single" class="control-label">Maskapai</label>
                                                            <div class="input-group select2-bootstrap-append">
                                                                <select class="form-control select2" id="maskapai" name="maskapai[]" multiple>
                                                                    <?php $i=0; foreach($maskapai_penerbangan as $mask){ $i++;?>
                                                                        <option value="<?php echo encrypt($mask->maskapai_id) ?>" 
                                                                            <?php foreach($dt['maskapai'] as $row){
                                                                                if($row['maskapai_id'] == encrypt($mask->maskapai_id)){echo "Selected";}
                                                                                }?>
                                                                        >
                                                                            <?php echo $mask->nama_maskapai?>
                                                                        </option>
                                                                    <?php } ?> 
                                                                </select>
                                                                <span class="input-group-btn">
                                                                    <button class="btn btn-default" type="button" data-select2-open="multi-append">
                                                                        <span class="glyphicon glyphicon-search"></span>
                                                                    </button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div id="penerbangan" class=" form-group ">
                                                            <label for="single" class="control-label">Hotel</label>
                                                            <div class="input-group select2-bootstrap-append">
                                                                <select class="form-control select2" id="hotel" name="hotel[]" multiple>
                                                                    <?php $i=0; foreach($hotel as $htl){ $i++;?>
                                                                        <option value="<?php echo encrypt($htl->hotel_id) ?>" <?php foreach($dt['paket_akomodasi'] as $row){if($row['hotel_id'] == encrypt($htl->hotel_id)){echo "Selected";}}?>><?php echo $htl->nama_hotel?></option>
                                                                    <?php } ?> 
                                                                </select>
                                                                <span class="input-group-btn">
                                                                    <button class="btn btn-default" type="button" data-select2-open="multi-append">
                                                                        <span class="glyphicon glyphicon-search"></span>
                                                                    </button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group form-md-line-input has-error">
                                                            <textarea type="text" class="ckeditor" rows="2" class="form-control" name="info_plus" id="info_plus" placeholder="Itinerary"><?php echo $dt['info_plus'] ?></textarea>
                                                        </div>
                                                        <button type="button" onClick="simpanDataPaket()" class="btn-add btn-sm btn red pull-right">Simpan Data Paket &nbsp;<i class="fa fa-save"></i></button>
                                                        <input type="hidden" name="paket_id" id="paket_id" value="<?php echo $dt['paket_id'] ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php echo form_close();?>
                                <hr>                   
                                <h2>Data Keberangkatan, Harga, Promo & Diskon</h2>
                                <div class="row row_keberangkatan">
                                    <div class="col-md-12">
                                        <button type="button" class="btnAdd btn btn-xs red" onClick="toggleAddKeberangkatanForm();"><i class="fa fa-plus"></i>&nbsp;Tambah Keberangkatan</button>
                                        <button type="button" class="AddKeberangkatanForm_btnCancel btn btn-xs grey" onClick="toggleAddKeberangkatanForm();"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                    </div>
                                    <div class="row" id="paketwaktu_form" style="margin:0px;padding:0px">
                                        <div class="col-md-12">
                                            <div class="col-md-8 noPadding">
                                                <table class="table">
                                                    <!-- Untuk Add Keberangkatan -->
                                                    <tr id="list_keberangkatan_0">
                                                        <td>
                                                            <select name="bulan_berangkat_0" id="bulan_0">
                                                                <option value="" selected>Bulan Berangkat...</option>
                                                                <?php $i=0; foreach($bulan as $bln){ ?>
                                                                    <option value="<?php echo $i; ?>"><?php echo $bln?></option>
                                                                <?php $i++;} ?> 
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select name="tahun_berangkat_0" id="tahun_0">
                                                                <option value="" selected>Tahun Berangkat...</option>
                                                                <?php foreach($tahun as $thn){?>
                                                                    <option value="<?php echo $thn ?>"><?php echo $thn?></option>
                                                                <?php } ?> 
                                                            </select>                                                                
                                                        </td>
                                                        <td>
                                                            <select name="kelas_0" id="kelas_0">
                                                                <option value="" selected>Kelas...</option>
                                                                <?php foreach($kelas as $kls){?>
                                                                    <option value="<?php echo encrypt($kls->kelas_id) ?>"><?php echo $kls->kelas?></option>
                                                                <?php } ?> 
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="text" placeholder="Harga Keberangkatan..." onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" onkeypress='return event.charCode >= 45 && event.charCode <= 57' name="harga_0" id="harga_0">
                                                        </td>
                                                        <td>
                                                            <button class="btn btn-xs red dropdown-toggle" onClick="simpanKeberangkatan('add',0)"" type="button"> Simpan <i class="fa fa-save"></i></button>
                                                            <input type="hidden" name="paket_id_0" id="paket_id_0" value="<?php echo $dt['paket_id'] ?>">
                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    
                                                </table>
                                            </div>
                                            <table class="table">
                                                <th></th>
                                                <th> Jemaah </th>
                                                <th> Bulan </th>
                                                <th> Tahun </th>
                                                <th> Kelas </th>
                                                <th> IDR (Rp) </th>
                                                <th> USD ($) </th>
                                                <th> Kurs </th>
                                                <th> Diskon </th>
                                                <th> Aksi </th>
                                                <th> Promo </th>

                                                <?php 
                                                    $count=0;
                                                    foreach ($dt['paket_waktu'] as $row){
                                                        $total_jemaah = $row['total_jemaah'];
                                                        $count++;
                                                ?>
                                                    <tr id="list_keberangkatan_<?php echo $count ?>">
                                                        <td><i class="fa fa-plane"></i></td>
                                                        <td>
                                                            <?php echo '<a><i class="fa fa-user"></i> '.$total_jemaah.' Jemaah</a>' ?>
                                                        </td>
                                                        <td>
                                                            <select class="keberangkatan_form" name="bulan_berangkat_<?php echo $count ?>" id="bulan_<?php echo $count ?>">
                                                                <?php $i=0; foreach($bulan as $bln){ ?>
                                                                    <option value="<?php echo $i; ?>" <?php if($row['bulan_berangkat']==$i){echo 'selected';} ?>><?php echo $bln?></option>
                                                                <?php $i++;} ?> 
                                                            </select>
                                                            <span class="keberangkatan_form_text_<?php echo $count ?>" id="bulan_text_<?php echo $count ?>"><?php echo $row['bulan_berangkat_format'] ?></span>
                                                                
                                                        </td>
                                                        <td>
                                                            <select class="keberangkatan_form" name="tahun_berangkat_<?php echo $count ?>" id="tahun_<?php echo $count ?>">
                                                                <?php foreach($tahun as $thn){?>
                                                                    <option value="<?php echo $thn ?>" <?php if($row['tahun_berangkat']==$thn){echo 'selected';} ?>><?php echo $thn?></option>
                                                                <?php } ?> 
                                                            </select>
                                                            <span class="keberangkatan_form_text_<?php echo $count ?>" id="tahun_text_<?php echo $count ?>"><?php echo $row['tahun_berangkat'] ?></span>
                                                        
                                                        </td>
                                                        <td>
                                                            <select class="keberangkatan_form" name="kelas_<?php echo $count ?>" id="kelas_<?php echo $count ?>">
                                                                <?php foreach($kelas as $kls){?>
                                                                    <option value="<?php echo encrypt($kls->kelas_id) ?>" <?php if($row['kelas_id']==encrypt($kls->kelas_id)){echo 'selected';} ?>><?php echo $kls->kelas?></option>
                                                                <?php } ?> 
                                                            </select>
                                                            <span class="keberangkatan_form_text_<?php echo $count ?>" id="kelas_text_<?php echo $count ?>"><?php echo $row['kelas'] ?></span>
                                                        </td>
                                                        
                                                        <td>
                                                            <input class="keberangkatan_form" style="width:120px" type="text"  onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" onkeypress='return event.charCode >= 45 && event.charCode <= 57' value="<?php echo $row['harga'] ?>" name="harga_idr_<?php echo $count ?>" id="harga_idr_<?php echo $count ?>">
                                                            <span class="keberangkatan_form_text_<?php echo $count ?> pull-right" id="harga_idr_text_<?php echo $count ?>"><?php echo $row['harga']?></span>
                                                        </td>
                                                        <td>
                                                            <input class="keberangkatan_form" style="width:120px" type="text"  onkeyup="hitungRupiah('<?php echo $count ?>')" value="<?php echo $row['harga_usd'] ?>" name="harga_usd_<?php echo $count ?>" id="harga_usd_<?php echo $count ?>">
                                                            <span class="keberangkatan_form_text_<?php echo $count ?> pull-right" id="harga_usd_text_<?php echo $count ?>"><?php echo $row['harga_usd'] ? $row['harga_usd'] : '-'?></span>
                                                        </td>
                                                        <td>
                                                            <input class="keberangkatan_form" style="width:120px" type="text"  onkeyup="hitungRupiah('<?php echo $count ?>')" value="<?php echo $row['kurs_dollar'] ?>" name="kurs_dollar_<?php echo $count ?>" id="kurs_dollar_<?php echo $count ?>">
                                                            <span class="keberangkatan_form_text_<?php echo $count ?> pull-right"><?php echo $row['kurs_dollar'] ?></span>
                                                        </td>
                                                        <td>
                                                            <input class="keberangkatan_form" style="width:120px" type="text" value="<?php echo $row['diskon']?>" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" name="diskon_<?php echo $count ?>" id="diskon_<?php echo $count ?>" placeholder="Tulis Diskon Harga ...">
                                                            <span class="keberangkatan_form_text_<?php echo $count ?> pull-right" id="diskon_text_<?php echo $count ?>"><?php echo $row['diskon'] ? $row['diskon'] : '-' ?></span>
                                                        </td>
                                                        <td>
                                                            <div class="btn-group" >
                                                                <button style="width:100%" class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                                    <i class="fa fa-angle-down"></i>
                                                                </button>
                                                                <ul class="dropdown-menu" role="menu">
                                                                    <li class="keberangkatan_form_btnEdit" id="keberangkatan_form_btnEdit_<?php echo $count ?>"><a href="javascript:;" onClick="toggleKeberangkatanForm(<?php echo $count ?>)"><i class="fa fa-pencil"></i>&nbsp;Edit</a></li>
                                                                    <li class="keberangkatan_form_btnSave" id="keberangkatan_form_btnSave_<?php echo $count ?>"><a href="javascript:;" onClick="simpanKeberangkatan('update',<?php echo $count ?>)"><i class="fa fa-save"></i>&nbsp;Save</a></li>
                                                                    <li class="keberangkatan_form_btnCancel" id="keberangkatan_form_btnCancel_<?php echo $count ?>"><a href="javascript:;" onClick="toggleKeberangkatanForm(<?php echo $count ?>)"><i class="fa fa-times"></i>&nbsp;Cancel</a></li>
                                                                    <?php if($total_jemaah==0){ ?>
                                                                        <li class="keberangkatan_form_btnHapus" id="keberangkatan_form_btnHapus_<?php echo $count ?>"><a href="javascript:;" onClick="simpanKeberangkatan('delete',<?php echo $count ?>)"><i class="fa fa-trash"></i>&nbsp;Hapus</a></li>
                                                                    <?php } ?>
                                                                </ul>
                                                            </div>
                                                            <input type="hidden" name="pwk_id" id="pwk_id_<?php echo $count ?>" value="<?php echo $row['pwk_id'] ?>">
                                                            <input type="hidden" name="paketwaktu_id" id="paketwaktu_id_<?php echo $count ?>" value="<?php echo $row['paketwaktu_id'] ?>">
                                                        </td>
                                                        <td>
                                                            <label><input type="checkbox" name="paket_promo_<?php echo $count ?>" id="paket_promo_<?php echo $count ?>" onChange="setPromoPaket(<?php echo $count ?>)" <?php if($row['promo']=='Ya'){echo "checked";}else{echo "";} ?>> Promo </label>
                                                        </td>
                                                    </tr>
                                                <?php    }
                                                ?>
                                            </table>
                                            <label><span class="font-red">*</span> Cheklist pada 'Promo' untuk menampilkan paket di beranda</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                
                <?php } else if($edit_paket_haji){ ?> 
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a href="<?php echo base_url().'administrator/paket/' ?>" class="btn btn-sm yellow-crusta">Kembali <i class="fa fa-arrow-circle-o-left"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-users font-red"></i>
                                <span class="header-modal caption-subject font-red sbold uppercase">Edit Paket</span>
                            </div>
                        </div>
                        <?php 
                            $dt = $edit_paket_haji; 
                            if($dt['harga_terakhir_usd_kurs']){
                                $harga_terakhir = $dt['harga_terakhir_usd'];
                                $style_input_kurs = '';
                            } else {
                                $harga_terakhir = $dt['harga_terakhir'];
                                $style_input_kurs = 'display-none';
                            }
                            
                         
                            echo form_open_multipart('administrator', array( 'id' => 'datapaket-form')); ?>

                                <div class="col-md-6 col-sm-12">
                                    <h2>Data Paket Haji</h2>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <label class="label label-info" style="font-size:12px"><i class="fa fa-info-circle"></i>&nbsp;Optimal Image Resolution : 650 x 900</label>
                                                <div class="fileinput-new thumbnail" >
                                                    <img id="paket_pic_detail" src="<?php echo $dt['paket_pic'] ?>" alt=""> 
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style=" line-height: 10px;"></div>
                                                <div>
                                                    <span class="btn default btn-file btn-sm">
                                                        <span class="fileinput-new"> Select image for Web </span>
                                                        <span class="fileinput-exists"> Change </span>
                                                        <input type="file" name="paket_pic" id="paket_pic" required> </span>
                                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <label class="label label-info" style="font-size:12px"><i class="fa fa-info-circle"></i>&nbsp;Optimal Image Resolution : 650 x 370</label>
                                                <div class="fileinput-new thumbnail" >
                                                    <img id="paket_pic_detail" src="<?php echo $dt['paket_mobile_pic'] ?>" alt=""> 
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style=" line-height: 10px;"></div>
                                                <div>
                                                    <span class="btn default btn-file btn-sm">
                                                        <span class="fileinput-new"> Select image for Mobile</span>
                                                        <span class="fileinput-exists"> Change </span>
                                                        <input type="file" name="paket_mobile_pic" id="paket_mobile_pic" required> </span>
                                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="col-md-12 col-sm-12">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <hr>
                                                    <div class="col-md-6">
                                                        <div class=" form-group">
                                                            <label for="form_control_1">Nama Paket</label>
                                                            <input type="text" class="form-control" name="nama_paket" id="nama_paket" placeholder="ex. Haji Berkah" value="<?php echo $dt['nama_paket'] ?>" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="form_control_1">Jenis Quota</label>
                                                            <select class="form-control" name="jenis_quota" id="jenis_quota" required>
                                                                <option value="" selected disabled>Pilih Jenis Kuota</option>
                                                                <option value="Haji Reguler" <?php if($dt['jenis_quota'] == "Haji Reguler"){echo "selected";} ?> >Haji Reguler</option>
                                                                <option value="Haji Plus" <?php if($dt['jenis_quota'] == "Haji Plus"){echo "selected";} ?>>Haji Plus</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class=" form-group">
                                                            <label for="form_control_1">Masa Tunggu (Tahun)</label>
                                                            <input type="text" placeholder="ex. 12" value="<?php echo $dt['masa_tunggu'] ?>" onkeypress='return event.charCode >= 45 && event.charCode <= 57' class="form-control" name="masa_tunggu" id="lama_hari" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12" >
                                                    <div class="kurs_input <?php echo $style_input_kurs ?> pull-right">
                                                        <span class="label bg-grey font-dark"><i class="fa fa-info-circle"></i> Isi kolom <b>HARGA</b> dengan nilai USD. Nilai <b>RUPIAH</b> didapat dari <b>USD</b> x <b>KURS DOLLAR</b></span>
                                                        <div class="clearfix"></div>
                                                        <hr>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="pull-left">
                                                            <span>Harga dalam : </span>
                                                            <select id="harga_dalam"  onchange="toggleKursInput(this.value)">
                                                                <option value="IDR" <?php if(!$dt['harga_terakhir_usd_kurs']){echo 'selected';}?>>Rupiah (IDR)</option>
                                                                <option value="USD" <?php if($dt['harga_terakhir_usd_kurs']){echo 'selected';}?>>Dollar (USD)</option>
                                                            </select>
                                                            <span class="kurs_input <?php echo $style_input_kurs ?>"> | Kurs dollar : <input type="text" style="width:80px" value="<?php echo $dt['harga_terakhir_usd_kurs'] ?>" id="add_paket_kurs" name="add_paket_kurs" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" onkeypress='return event.charCode >= 45 && event.charCode <= 57'></span>
                                                        </div>
                                                        <div class="pull-right">
                                                            <input type="text" onkeydown="return numbersonly(this, event);" value="<?php echo $harga_terakhir ?>" placeholder="Harga" onkeyup="javascript:tandaPemisahTitik(this);" onkeypress='return event.charCode >= 45 && event.charCode <= 57' class="form-control input-medium pull-left" name="harga_terakhir" id="harga_terakhir">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>                                 
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <h2>Itinerary </h2>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                    <label class="backdrop-form" style="font-size:12px;margin:0px;line-height:0px;width:100%"><i class="fa fa-warning"></i>&nbsp;Silahkan tulis fitur, bonus, diskon atau info lain terkait paket ini</label>
                                                    <textarea type="text" class="ckeditor" rows="2" class="form-control" name="info_plus" id="info_plus" placeholder="Itinerary"><?php echo $dt['info_plus'] ?></textarea>
                                               
                                            </div>
                                        </div>  
                                    </div>                                    
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                                <div class="modal-footer" style="border-top:0px;padding:0px">
                                    <button type="button" onClick="simpanDataPaket()" class="btn-add btn-sm btn blue">Simpan &nbsp;<i class="fa fa-save"></i></button>
                                    <input type="hidden" name="paket_id" id="paket_id" value="<?php echo $dt['paket_id'] ?>">
                                    <input type="hidden" name="jenispaket_id" id="jenispaket_id" value="<?php echo encrypt(2); ?>">
                                </div>
                            <?php echo form_close(); ?>                                 
                        <div class="clearfix"></div>
                    </div>
                
                <?php } else { ?>
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a href="<?php echo base_url().'administrator/paket/form_add_paket' ?>" class="btn btn-sm red">Tambah Paket Reguler <i class="fa fa-plus"></i></a>
                                </div>
                                <div class="btn-group">
                                    <a href="<?php echo base_url().'administrator/paket/form_add_haji' ?>" class="btn btn-sm blue">Tambah Paket Haji <i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold uppercase"> Managed Paket</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#tab_1_1" data-toggle="tab"> Umroh / Wisata Islam </a>
                                        </li>
                                        <li>
                                            <a href="#tab_1_2" data-toggle="tab"> Haji </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in" id="tab_1_1">
                                            <table class="table table-striped table-bordered table-checkable" id="table-custom">
                                                <thead>
                                                    <tr>
                                                        <th> No</th>
                                                        <th> Nama Paket </th>
                                                        <th> Jenis Paket </th>
                                                        <th> Keberangkatan </th>
                                                        <th> Status Paket</th>
                                                        <th> Aksi </th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php if($paket > 0){ 
                                                        
                                                        $count=0;//echo_array($paket_haji);
                                                        foreach($paket as $row){
                                                            $id = encrypt($row->paket_id);
                                                            $jenispaket_id = $row->jenispaket_id;
                                                            
                                                            $now = date('Y-m-d H:i:s');
                                                            $datetime1 = new DateTime($row->mulai_publish);
                                                            $datetime2 = new DateTime($now);
                                                            $datetime3 = new DateTime($row->akhir_publish);

                                                            $diff_publish = $datetime1->diff($datetime2);
                                                            $diff_akhir = $datetime2->diff($datetime3);

                                                             if($row->mulai_publish > $now){
                                                                $status_paket = '<i class="fa fa-hand-paper-o" style="color:green"></i>&nbsp; Publish '.$diff_publish->days.' Hari kedepan';
                                                            } else if($row->mulai_publish < $now){
                                                                if($row->akhir_publish < $now)
                                                                    $status_paket = '<i class="fa fa-times" style="color:red"></i>&nbsp; Sudah Berakhir';
                                                                 else 
                                                                    $status_paket = '<i class="fa fa-hand-peace-o"></i>&nbsp; Berakhir '.$diff_akhir->days.' Hari lagi';
                                                            } 

                                                            //Keberangkatan dikosongkan untuk paket haji
                                                            if($row->jenispaket_id==2){
                                                                $keberangkatan = '-';
                                                                $status_paket ='';
                                                            }
                                                            else{
                                                                $keberangkatan = '<i class="fa fa-map-marker"></i> ('.$row->kode_kota.') '.$row->nama_kota;
                                                            }
                                                            //Cek Ada promo atau enggak
                                                            $cek_promo = $this->md_paket->getPaketWithPromo($row->paket_id);
                                                            $label='';
                                                            if($cek_promo){
                                                                $label = '<button class="btn btn-xs red pull-right">Promo !</button>';
                                                            }

                                                            $count++;
                                                ?>
                                                    <tr>
                                                        <td><?php echo $count; ?></td>
                                                        <td><?php echo $row->nama_paket.$label; ?></td>
                                                        <td><?php echo $row->jenis_paket; ?></td>
                                                        <td><?php echo $keberangkatan?></td>
                                                        <td><?php echo $status_paket.' ('.$row->pendaftar.' jemaah)' ?></td>
                                                        <td>
                                                            <div class="btn-group" >
                                                                <button style="width:100%" class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                                    <i class="fa fa-angle-down"></i>
                                                                </button>
                                                                <ul class="dropdown-menu" role="menu">
                                                                    <li><a href="javascript:;" onClick="detail_function('detail',<?php echo $id ?>);" class="btn_detail<?php echo $id ?>"><i class="fa fa-search"></i>&nbsp;Detail</a></li>
                                                                  
                                                                        <!-- <li><a href="javascript:;" onClick="edit_function('duplicate',<?php echo $id ?>,<?php echo $count; ?>);" class="btn_detail<?php echo $id ?>"><i class="fa fa-copy"></i>&nbsp;Duplicate</a></li> -->
                                                                        <li><a href="<?php echo base_url().'administrator/paket/edit/'.$id ?>" class="btn_edit<?php echo $id ?>"><i class="fa fa-pencil"></i>&nbsp;Edit</a></li>
                                                                        <?php if($row->pendaftar==0){?>
                                                                            <li ><a href="javascript:;" onClick="edit_function('delete',<?php echo $id ?>);" class="btn_hapus<?php echo $id ?>"><i class="fa fa-trash"></i>&nbsp;Hapus</a></li>
                                                                        <?php } ?>
                                                                </ul>
                                                            </div>

                                                        </td>
                                                    </tr>
                                                <?php } 
                                                } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane fade" id="tab_1_2">
                                            <table class="table table-striped table-bordered table-checkable" id="table-custom2">
                                                <thead>
                                                    <tr>
                                                        <th> NO</th>
                                                        <th> Nama Paket </th>
                                                        <th> Jenis Paket </th>
                                                        <th> Masa Tunggu </th>
                                                        <th> Harga Terakhir </th>
                                                        <th> Status Paket</th>
                                                        <th> Aksi </th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php if($paket_haji > 0){ 
                                                        
                                                        $count=0;//echo_array($paket_haji);
                                                        foreach($paket_haji as $row){
                                                            $id = encrypt($row->paket_id);
                                                            $harga_usd = '';
                                                            if($row->harga_terakhir_usd_kurs){
                                                                $harga_usd = '<br><small><b>$ '.number_format($row->harga_terakhir_usd).'</b> <i>(kurs '.number_format($row->harga_terakhir_usd_kurs).')</i></small>';
                                                            }
                                                ?>
                                                    <tr>
                                                        <td><?php echo ++$count; ?></td>
                                                        <td><?php echo $row->nama_paket; ?></td>
                                                        <td><?php echo $row->jenis_quota; ?></td>
                                                        <td><i class="fa fa-refresh"></i> <?php echo $row->masa_tunggu?> Tahun</td>
                                                        <td><?php echo rupiah_format($row->harga_terakhir).$harga_usd?></td>
                                                        <td><?php echo ' ('.$row->pendaftar.' jemaah)' ?></td>
                                                        <td>
                                                            <div class="btn-group" >
                                                                <button style="width:100%" class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                                    <i class="fa fa-angle-down"></i>
                                                                </button>
                                                                <ul class="dropdown-menu" role="menu">
                                                                    <li><a href="javascript:;" onClick="detail_function('detail',<?php echo $id ?>);" class="btn_detail<?php echo $id ?>"><i class="fa fa-search"></i>&nbsp;Detail</a></li>
                                                              
                                                                        <!-- <li><a href="javascript:;" onClick="edit_function('duplicate',<?php echo $id ?>,<?php echo $count; ?>);" class="btn_detail<?php echo $id ?>"><i class="fa fa-copy"></i>&nbsp;Duplicate</a></li> -->
                                                                        <li><a href="<?php echo base_url().'administrator/paket/edit/'.$id ?>" class="btn_edit<?php echo $id ?>"><i class="fa fa-pencil"></i>&nbsp;Edit</a></li>
                                                                        <?php if($row->pendaftar==0){?>
                                                                        <li><a href="javascript:;" onClick="edit_function('delete',<?php echo $id ?>);" class="btn_hapus<?php echo $id ?>"><i class="fa fa-trash"></i>&nbsp;Hapus</a></li>
                                                                        <?php } ?>
                                                                   
                                                                </ul>
                                                            </div>

                                                        </td>
                                                    </tr>
                                                <?php } 
                                                } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                    </div>
                
                <?php } ?>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->


<!-- detail Modal-->
<div class="modal fade" id="detail_modal" role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog"  style="width:750px">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-search"></i>
                    <span class="header-modal caption-subject sbold uppercase">Detail Paket</span>
                </div>
            </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-4 col-sm-3">
                            <h4>Web Picture</h4>
                            <div class="fileinput-new thumbnail">
                                <a hreg="" id="detail_avatar_href" target="_blank"><img id="detail_avatar" src="<?php echo base_url().'assets/frontend/img/no-avatar.png' ?>" alt=""> </a>
                            </div> 
                            <h4>Mobile Picture</h4>
                            <div class="fileinput-new thumbnail">
                                <a hreg="" id="detail_avatar_mobile_href" target="_blank"><img id="detail_avatar_mobile" src="<?php echo base_url().'assets/frontend/img/no-avatar.png' ?>" alt=""></a> 
                            </div>
                        </div>
                        <div class="col-md-8 reguler">
                            <h3><i class="fa fa-tasks"></i>&nbsp;<span id="detail_nama_paket"></span></h3>
                            <div class="col-md-12">
                                <table class="table">
                                    <tr>
                                        <td style="width:150px"><i class="icon-briefcase"></i>&nbsp; <span>Jenis Paket</td>
                                        <td>:</td>
                                        <td><span id="detail_jenis_paket"></span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="icon-trophy"></i>&nbsp; <span>Lama Perjalanan</td>
                                        <td>:</td>
                                        <td><span id="detail_lama_hari"></span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="icon-plane"></i>&nbsp; <span>Maskapai</td>
                                        <td>:</td>
                                        <td><div class="col-md-12>"><span id="detail_maskapai"></span></div></td>
                                    </tr>
                                    <tr>
                                        <td><i class="icon-screen-smartphone"></i>&nbsp; <span>Berangkat awal</td>
                                        <td>:</td>
                                        <td><span id="detail_berangkat_awal"></span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="icon-screen-smartphone"></i>&nbsp; <span>Transit</td>
                                        <td>:</td>
                                        <td><span id="detail_berangkat_transit"></span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="icon-screen-smartphone"></i>&nbsp; <span>Landing</td>
                                        <td>:</td>
                                        <td><span id="detail_berangkat_landing"></span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="icon-home"></i>&nbsp; <span>Hotel</td>
                                        <td>:</td>
                                        <td><span id="detail_hotel"></span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="icon-calendar"></i>&nbsp; <span>Mulai Publish</td>
                                        <td>:</td>
                                        <td><span id="detail_mulai_publish"></span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="icon-calendar"></i>&nbsp; <span>Akhir Publish</td>
                                        <td>:</td>
                                        <td><span id="detail_akhir_publish"></span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="icon-map"></i>&nbsp; <span>Itinerary</td>
                                        <td>:</td>
                                        <td><span id="detail_info_plus"></span></td>
                                    </tr>
                                </table>
                            </div>
                            <h3><i class="fa fa-plane"></i>&nbsp;List Harga dan Bulan Keberangkatan</h3>
                            <div class="col-md-12">
                                <table class="table">
                                    <tr id="detail_harga_bulan"></tr>
                                </table>
                            </div> 
                        </div>
                        <div class="col-md-8 haji">
                            <h3><i class="fa fa-tasks"></i>&nbsp;<span id="detail_nama_paket_haji"></span></h3>
                            <div class="col-md-12">
                                <table class="table">
                                    <tr>
                                        <td style="width:180px"><h4><i class="icon-briefcase"></i>&nbsp; Jenis Paket</td>
                                        <td><h4>:</h4></td>
                                        <td><h4 id="detail_jenis_paket_haji"></h4></td>
                                    </tr>
                                    <tr>
                                        <td><h4><i class="icon-trophy"></i>&nbsp; Jenis Kuota</td>
                                        <td><h4>:</h4></td>
                                        <td><h4 id="detail_jenis_kuota"></h4></td>
                                    </tr>
                                    <tr>
                                        <td><h4><i class="fa fa-refresh"></i>&nbsp; Masa Tunggu</td>
                                        <td><h4>:</h4></td>
                                        <td><h4 id="detail_masa_tunggu"></h4></td>
                                    </tr> 
                                    <tr>
                                        <td><h4><i class="fa fa-dollar"></i>&nbsp; Harga Terakhir</td>
                                        <td><h4>:</h4></td>
                                        <td><h4 id="detail_harga_terakhir"></h4></td>
                                    </tr>                                   
                                    <tr>
                                        <td><h4><i class="icon-map"></i>&nbsp; Itinerary</td>
                                        <td><h4>:</h4></td>
                                        <td><h4 id="detail_info_plus_haji"></h4></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-circle grey">Close &nbsp;<i class="fa fa-times"></i></button>            
            </div>
        </div>

    </div>
</div>

