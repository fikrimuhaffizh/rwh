<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<script type="text/javascript">
    var harus_bayar=0;
    var harus_bayar_rupiah ='';

    function toggle_form(form){
        $('.form-transfer').empty();
        if(form=='Transfer Pembayaran'){
            var x =' <h4>&nbsp;</h4> <table class="table">';
                x+='    <tr>';
                x+='        <td>Nama Rekening Pengirim</td>';
                x+='        <td>:</td>';
                x+='        <td><input type="text" class="form-control input-sm" name="nama_pengirim" id="nama_pengirim" required autocomplete="off"></td>';
                x+='    </tr>';
                x+='    <tr>';
                x+='        <td>Bank Pengirim</td>';
                x+='        <td>:</td>';
                x+='        <td><input type="text" class="form-control input-sm" name="bank_pengirim" id="bank_pengirim" required autocomplete="off"></td>';
                x+='    </tr>';
                x+='    <tr>';
                x+='        <td>Bank Penerima (RWH)</td>';
                x+='        <td>:</td>';
                x+='        <td>';
                x+='            <select name="bank_penerima" class="form-control input-sm" id="bank_penerima" required>';
                x+='                <option value="">Pilih Bank...</option>';
                                <?php if($dt_bank_transfer){ 
                                    foreach($dt_bank_transfer as $dbt){
                                ?>
                                    x+='<option value="<?php echo encrypt($dbt->bank_id)?>"><?php echo $dbt->nama_bank.' ('.$dbt->nomor_rekening.')' ?></option>';
                                <?php } }?>
                x+='            </select>';
                x+='        </td>';
                x+='    </tr>';
                x+='    <tr>';
                x+='        <td>Tanggal Transfer Ke Rekening RWH</td>';
                x+='        <td>:</td>';
                x+='        <td><input class="form-control input-sm date-picker " type="text" name="tgl_bayar_bank" autocomplete="off" placeholder="dd/mm/yyyy" required></td>';
                x+='    </tr>';
                x+='</table>';
            $('.form-transfer').append(x);
            $('[name="tgl_bayar_bank"]').datepicker({format: 'dd/mm/yyyy'});
        } else if(form=='Transfer Pengembalian'){
            var x =' <h4>&nbsp;</h4> <table class="table">';
                x+='    <tr>';
                x+='        <td>No Rekening Penerima</td>';
                x+='        <td>:</td>';
                x+='        <td><input type="text" class="form-control input-sm" name="kembali_no_rek_penerima" required autocomplete="off"></td>';
                x+='    </tr>';
                x+='    <tr>';
                x+='        <td>Nama Rekening Penerima</td>';
                x+='        <td>:</td>';
                x+='        <td><input type="text" class="form-control input-sm" name="kembali_rek_penerima" required autocomplete="off"></td>';
                x+='    </tr>';
                x+='    <tr>';
                x+='        <td>Bank Penerima</td>';
                x+='        <td>:</td>';
                x+='        <td><input type="text" class="form-control input-sm" name="kembali_bank_penerima" required autocomplete="off"></td>';
                x+='    </tr>';
                x+='        <td>Tanggal Transfer Ke Rekening Penerima</td>';
                x+='        <td>:</td>';
                x+='        <td><input class="form-control input-sm date-picker " type="text" name="kembali_tgl_transfer" autocomplete="off" placeholder="dd/mm/yyyy" required></td>';
                x+='    </tr>';
                x+='</table>';
                $('.form-transfer').append(x);
                $('[name="kembali_tgl_transfer"]').datepicker({format: 'dd/mm/yyyy'});
        } else if(form=='EDC Pembayaran' || form=='EDC Pengembalian'){
            var x =' <h4>&nbsp;</h4> <table class="table">';
                x+='    <tr>';
                x+='        <td>Bank EDC</td>';
                x+='        <td>:</td>';
                x+='        <td>';
                x+='            <select name="bank_penerima" class="form-control input-sm" id="bank_penerima" required>';
                x+='                <option value="">Pilih Bank...</option>';
                                <?php if($dt_bank_edc){ 
                                    foreach($dt_bank_edc as $dbt){
                                ?>
                                    x+='<option value="<?php echo encrypt($dbt->bank_id)?>"><?php echo $dbt->nama_bank.' ('.$dbt->nomor_rekening.')' ?></option>';
                                <?php } }?>
                x+='            </select>';
                x+='        </td>';
                x+='    <tr>';
                x+='        <td>Biaya Tambahan</td>';
                x+='        <td>:</td>';
                x+='        <td><input type="text" class="form-control input-sm" name="biaya_tambahan" required autocomplete="off"></td>';
                x+='    </tr>';
                x+='</table>';
                $('.form-transfer').append(x);
        }
    }

    function toggle_cancel_form(jenis){
        if(jenis=='Berlebih'){
            $('#row_ketentuan_cancel').remove();
        } else {
            harus_bayar        = '<?php echo $dt_jemaah ? $dt_jemaah[0]->harus_bayar : 0?>';
            harus_bayar_rupiah = '<?php echo $dt_jemaah ? rupiah_format($dt_jemaah[0]->harus_bayar) : '-'?>';
            var x ='<tr id="row_ketentuan_cancel">';
                x+='    <td>Ketentuan Cancel/Pembantalan</td>';
                x+='    <td>:</td>';
                x+='    <td>';
                x+='        <select name="jenis_cancel" class="form-control input-sm" onChange="hitungPotongan(this.value)" required>';
                x+='            <option value="">Pilih ketentuan...</option>';
                x+='            <option value="setoran_awal_hangus">45 Hari sebelum keberangkatan</option>';
                x+='            <option value="30">30 Hari sebelum keberangkatan</option>';
                x+='            <option value="75">20 Hari sebelum keberangkatan</option>';
                x+='            <option value="100">10 Hari sebelum keberangkatan</option>';
                x+='            <option value="0">14 Hari sebelum keberangkatan (Meninggal Dunia)</option>';
                x+='        </select>';
                x+='    </td>';
                x+='</tr>';
            $('#row_jenis_pengembalian').after(x);
        }
    }

    function hitungPotongan(persen){
        $('#detail_potongan').remove();
        
        var x = '<div id="detail_potongan" style="font-style:italic">';
            x+= '<small>Harga Paket : '+harus_bayar_rupiah+'</small><br>';

            if(persen == 'setoran_awal_hangus'){
                    setoran_awal = $('#setoran_awal').val() * 1;
                    $('input[name=jumlah_pengembalian]').val(setoran_awal.formatMoney(0, ',', '.'));
                    x+= '<small>Ketentuan    : Setoran Awal Hangus (Rp '+setoran_awal.formatMoney(0, ',', '.');
                    if(setoran_awal==0)
                        x+=' / Jemaah belum melakukan pembayaran Setoran Awal';
                    x+=')</small>';
                    x+= '</div>';
            } else {
                temp = harus_bayar-(harus_bayar * (persen/100));
                $('input[name=jumlah_pengembalian]').val(temp.formatMoney(0, ',', '.'));
                x+= '<small>Ketentuan    : Dipotong '+persen+'% dari harga paket</small>';
            }

        x+= '</div>';
        $('input[name=jumlah_pengembalian]').after(x);
    }

    function edit_function(task='',id='',jenis=''){
        if(task == 'show_pembayaran'){
            $('.hstr_bayar').remove();
            $('.total_pembayaran_list').remove();
            $('.form_bayar').addClass('hide');
            $('#pendaftarandetail_id_bayar').val(id);
            $('.pembayaran_list').remove();
            $('.belum_bayar').remove();
            $('#pembayaran_modal').modal();
            $.ajax({
                    method: 'POST',
                    url: '<?php echo base_url() ?>administrator/pembayaran/get_list_pembayaran/'+id,
                    dataType: 'json',
                    beforeSend : function(){$('#loading').show();},
                    success : function(resp){
                        $('#loading').hide();
                        var pos = 0;
                        var total_bayar = 0;
                        if(resp.length>0){
                            for(var i=0;i<resp.length;i++){
                                var status = "";
                                var jenis_transaksi = resp[i]['jenis_transaksi'];
                                var status_verif = resp[i]['status_verifikasi'];
                                if(status_verif == 'Accept')
                                    status_verif = '<span class="font-green-jungle"><i class="fa fa-check"></i>&nbsp; Accept</span>';
                                else if(status_verif == 'Decline')
                                    status_verif = '<span class="font-red"><i class="fa fa-times"></i>&nbsp; Decline</span>';
                                else
                                    status_verif = '<span><i class="fa fa-clock"></i>&nbsp; Dalam Proses</span>';

                                $('.belum_bayar').remove();

                                var status_pembayaran = jenis_transaksi == 'Pembayaran' ? resp[i]['status_pembayaran'] : resp[i]['jenis_pengembalian'];
                                var x ='<div class="pembayaran_list">';
                                    x+='    <div class="col-md-12 panel-body">';
                                    x+='        <h4>Detail '+jenis_transaksi+' : </h4>';
                                    x+='        <table class="table">';
                                    x+='        <tr>';
                                    x+='            <td width="200">No. Kwitansi</td>';
                                    x+='            <td width="50">:</td>';
                                    x+='            <td>'+resp[i]['no_kwitansi']+'</td>';
                                    x+='        </tr>';
                                    x+='        <tr>';
                                    x+='            <td>Jenis</td>';
                                    x+='            <td>:</td>';
                                    x+='            <td>'+status_pembayaran+'</td>';
                                    x+='        </tr>';
                                    x+='        <tr>';
                                    x+='            <td>Jumlah</td>';
                                    x+='            <td>:</td>';
                                    x+='            <td>'+resp[i]['jumlah_bayar']+'</td>';
                                    x+='        </tr>';
                                    x+='        <tr>';
                                    x+='            <td>Metode Transaksi</td>';
                                    x+='            <td>:</td>';
                                    x+='            <td>'+resp[i]['metode_transaksi']+'</td>';
                                    x+='        <tr>';
                                    x+='            <td>Diproses Oleh</td>';
                                    x+='            <td>:</td>';
                                    x+='            <td>'+resp[i]['nama_lengkap']+' <small>('+resp[i]['tgl_update']+')</small></td>';
                                    x+='        </tr>';                              
                                    x+='        </tr>';  
                                    x+='            <td>Catatan</td>';
                                    x+='            <td>:</td>';
                                    x+='            <td>'+resp[i]['catatan']+'</td>';
                                    x+='        </tr>';      
                                    x+='        </table>';

                                    if(resp[i]['jenis_transaksi']=='Pembayaran' && resp[i]['metode_transaksi']=='Transfer'){
                                        x+='        <h4>Detail Transfer : </h4>';
                                        x+='        <table class="table">';
                                        x+='        <tr>';
                                        x+='            <td width="200">Nama Rekening Pengirim</td>';
                                        x+='            <td width="50">:</td>';
                                        x+='            <td>'+resp[i]['nama_rek_pengirim']+'</td>';
                                        x+='        </tr>';
                                        x+='        <tr>';
                                        x+='            <td>Bank Pengirim</td>';
                                        x+='            <td>:</td>';
                                        x+='            <td>'+resp[i]['bank_pengirim']+'</td>';
                                        x+='        </tr>';
                                        x+='        <tr>';
                                        x+='            <td>Bank Penerima</td>';
                                        x+='            <td>:</td>';
                                        x+='            <td>'+resp[i]['nama_bank']+' ('+resp[i]['nomor_rekening']+')</td>';
                                        x+='        </tr>';    
                                        x+='        <tr>';
                                        x+='            <td>Transfer Ke Rekening RWH</td>';
                                        x+='            <td>:</td>';
                                        x+='            <td>'+resp[i]['tgl_bayar_bank']+'</td>';
                                        x+='        </tr>'; 
                                        x+='        </table>'; 
                                        
                                    } else if(resp[i]['jenis_transaksi']=='Pengembalian' && resp[i]['metode_transaksi']=='Transfer'){
                                        x+='        <h4>Detail Transfer : </h4>';
                                        x+='        <table class="table">';
                                        x+='        <tr>';
                                        x+='            <td width="200">Nama Rekening Penerima</td>';
                                        x+='            <td width="50">:</td>';
                                        x+='            <td>'+resp[i]['kembali_rek_penerima']+'</td>';
                                        x+='        </tr>';
                                        x+='        <tr>';
                                        x+='            <td>Rekening Pengirim</td>';
                                        x+='            <td>:</td>';
                                        x+='            <td>'+resp[i]['kembali_bank_penerima']+'</td>';
                                        x+='        </tr>';   
                                        x+='        <tr>';
                                        x+='            <td>Transfer Ke Rekening Penerima</td>';
                                        x+='            <td>:</td>';
                                        x+='            <td>'+resp[i]['kembali_tgl_transfer']+'</td>';
                                        x+='        </tr>';  
                                        x+='        </table>';
                                        
                                    } else if(resp[i]['jenis_transaksi']=='Pembayaran' && resp[i]['metode_transaksi']=='EDC'){
                                        x+='        <h4>Detail EDC : </h4>';
                                        x+='        <table class="table">';
                                        x+='        <tr>';
                                        x+='            <td width="200">Bank </td>';
                                        x+='            <td width="50">:</td>';
                                        x+='            <td>'+resp[i]['nama_bank']+'</td>';
                                        x+='        </tr>';    
                                        x+='        <tr>';
                                        x+='            <td>Biaya Tambahan</td>';
                                        x+='            <td>:</td>';
                                        x+='            <td>'+resp[i]['biaya_tambahan']+'</td>';
                                        x+='        </tr>'; 
                                        x+='        </table>'; 
                                        
                                    }

                                    x+='    </div>'; 
                                    x+='    <div class="col-md-12 hide">' ;
                                    x+='        <h4>Verifikasi Pembayaran : </h4>';
                                    x+='        <table class="table">';
                                    x+='        <tr>';
                                    x+='            <td>Status Verifikasi</td>';
                                    x+='            <td>:</td>';
                                    x+='            <td>'+resp[i]['status_verifikasi_style']+'</td>';
                                    x+='        </tr>';   

                                        if(resp[i]['status_verifikasi_style']=='Dalam Proses') {                                     
                                            x+='        <tr>';
                                            x+='            <td>&nbsp;</td><td>&nbsp;</td>';
                                            x+='            <td><a href="javascript:;" onClick="edit_function("edit",'+id+')><i class="fa fa-times"></i>&nbsp;Cancel Pembayaran</a></td>';
                                            x+='        </tr>';                       
                                        } else {
                                            x+='        <tr>';
                                            x+='            <td>Verifikasi oleh </td>';
                                            x+='            <td>:</td>';
                                            x+='            <td>'+resp[i]['kantor_pusat_nama']+'</td>';
                                            x+='        </tr>'; 
                                            x+='        <tr>';
                                            x+='            <td>Tanggal Verifikasi</td>';
                                            x+='            <td>:</td>';
                                            x+='            <td>'+resp[i]['kantor_pusat_tgl_verif']+'</td>';
                                            x+='        </tr>';                                                                                 
                                        } 

                                    x+='         </table>';
                                    x+='    </div>';
                                   
                                    
                                    $('#list_detail_pembayaran').after(x);
                                    pos++;
                            }
                        }
                    }
                });
        } 

        else if(task == 'update'){
        }
        else if(task == 'edit'){
            alert(id);
        }
        else if(task == 'delete'){
            url = '<?php echo base_url() ?>administrator/pembayaran/delete/'+id+'/'+jenis;
            swal({
              title: "Hapus Data "+jenis+" Jemaah ?",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "btn-danger",
              confirmButtonText: "Ya, Hapus",
              closeOnConfirm: false,
              showLoaderOnConfirm: true
            },
            function(){
                $.ajax({
                  method: 'POST',
                  url: url,
                  dataType: 'json',
                  success : function(resp){
                      if(resp == 'update_success'){
                          location.reload()
                      }
                  }
              });
            });
        } 
        else if(task == 'update_harga_paket'){
            pendaftarandetail_id = '<?php echo $pendaftarandetail_id ?>';
            swal({
              title: "Perbaharui harga paket jemaah berdasarkan kurs '<?php echo $kurs_hari_ini ?>' ?",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "btn-danger",
              confirmButtonText: "Update",
              closeOnConfirm: false,
              showLoaderOnConfirm: true
            },
            function(){
                $.ajax({
                  method: 'POST',
                  url: '<?php echo base_url().'administrator/pembayaran/update_harga_paket' ?>',
                  data : {'pendaftarandetail_id' : pendaftarandetail_id,'kurs_hari_ini':'<?php echo $kurs_hari_ini ?>'},
                  dataType: 'json',
                  success : function(resp){
                      if(resp == 'update_success'){
                          location.reload()
                      }
                  }
              });
            });
        }
    }

    function cek_kurs(val,usd){
        $('.btn-submit').show();
        $('input[name=jumlah_bayar]').val('');
        $('#pelunasan_warning').empty();
        if(val == 'Pelunasan' && usd){
            kurs_hari_ini = '<?php echo $kurs_hari_ini ?>';
            if(kurs_hari_ini=='?'){
                $('.btn-submit').hide();
                $('#pelunasan_warning').append('<small class="font-red"><i class="fa fa-times"></i> Silahkan update kurs dollar hari ini untuk menyelesaikan Pelunasan !</small>');
            } else {
                $('#pelunasan_warning').append('<small><i>Rate rupiah hari ini : <?php echo $kurs_hari_ini ?></i>.<br> Silahkan klik <a onClick="edit_function(\'update_harga_paket\')">Update Harga Paket Jemaah</a> untuk menyesuaikan "Sisa Bayar" jemaah ketika pelunasan</small>');
            }
        } else if(val == 'Mahram'){
            var jumlah = 300000;
            $('input[name=jumlah_bayar]').val(jumlah.formatMoney(0, ',', '.'));
        }
    }

    $(document).ready(function() {
        table = $('#table_umroh').DataTable({ 
            "processing": true, "serverSide": true,"order": [],
            "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
            "ajax": {"url": "<?php echo site_url('administrator/pembayaran/pagination/umroh')?>","type": "POST"},
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                        {"targets": [0],"className": "dt-center"},
                        {"targets": [2],"className": "dt-center"},
                        {"targets": [6],"orderable": false,"className": "dt-center"},
                        {"targets": [7],"orderable": false},
                    ],
            "initComplete" : function (resp) {
                this.api().columns([6]).every( function () {
                    var column = this;
                    var select = $('<select class="from-control input-sm input-small"><option value="">Semua Status</option><option value="Lunas">Lunas</option><option value="Belum Lunas">Belum Lunas</option><option value="Belum Bayar">Belum Bayar</option><option value="Tour Leader">Tour Leader</option></select>')
                                    .appendTo( $('#filter_status_bayar_umroh').empty())
                                    .on( 'change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                        column.search( $(this).val(), true, false ) .draw();
                                    });
                    
                });
            } 
        });  

        table3 = $('#table_sudah_berangkat').DataTable({ 
            "processing": true, "serverSide": true,"order": [],
            "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
            "ajax": {"url": "<?php echo site_url('administrator/pembayaran/pagination/sudah_berangkat')?>","type": "POST"},
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                        {"targets": [0],"className": "dt-center"},
                        {"targets": [2],"className": "dt-center"},
                        {"targets": [3],"className": "dt-center"},
                    ],
            "initComplete" : function (resp) {
                this.api().columns([6]).every( function () {
                    var column = this;
                    var select = $('<select class="from-control input-sm input-small"><option value="">Semua Status</option><option value="Lunas">Lunas</option><option value="Belum Lunas">Belum Lunas</option><option value="Belum Bayar">Belum Bayar</option><option value="Tour Leader">Tour Leader</option></select>')
                                    .appendTo( $('#filter_status_bayar_sudah_berangkat').empty())
                                    .on( 'change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                        column.search( $(this).val(), true, false ) .draw();
                                    });
                    
                });
            },
            "createdRow" : function(row,data,index){$(row).css("background-color","#F0FFF0");}
        });

        table4 = $('#table_cancel').DataTable({ 
            "processing": true, "serverSide": true,"order": [],
            "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
            "ajax": {"url": "<?php echo site_url('administrator/pembayaran/pagination/cancel')?>","type": "POST"},
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                        {"targets": [0],"className": "dt-center"},
                        {"targets": [2],"className": "dt-center"},
                        {"targets": [3],"className": "dt-center"},
                    ],
            "initComplete" : function (resp) {
                this.api().columns([6]).every( function () {
                    var column = this;
                    var select = $('<select class="from-control input-sm input-small"><option value="">Semua Status</option><option value="Lunas">Lunas</option><option value="Belum Lunas">Belum Lunas</option><option value="Melebihi Harga">Melebihi Harga</option><option value="Belum Bayar">Belum Bayar</option></select>')
                                    .appendTo( $('#filter_status_bayar_cancel').empty())
                                    .on( 'change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                        column.search( $(this).val(), true, false ) .draw();
                                    });
                    
                });
            },
            "createdRow" : function(row,data,index){$(row).css("background-color","#FFF1F1");}
        });  
        table = $('.data-table').DataTable(); 
    });  
</script>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Pembayaran Jemaah
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>

        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo base_url().'administrator' ?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Pembayaran Jemaah</span>
            </li>
        </ul>
            <div class="row">
                <?php if($show_detail_pembayaran){ ?>
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <i class="fa fa-list font-dark"></i>
                                    <span class="caption-subject bold uppercase"> List Pembayaran Jemaah <span class="font-red">"<?php echo $dt_jemaah[0]->nama_jemaah ?>"</span></span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <?php if($this->session->flashdata('add_success')){ echo '<label class="alert alert-success"><i class="fa fa-check"></i> '.$this->session->flashdata('sucess').' jemaah disimpan</label>';}?>

                                <?php echo form_open('administrator/pembayaran/add/pembayaran', array( 'id' => 'add-pembayaran-form')); ?>
                                    <div class="portlet-body form" >
                                        <a href="<?php echo base_url().'administrator/pembayaran' ?>" class="btn btn-sm yellow-crusta"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;Kembali</a>
                                        <a data-toggle="collapse" href="#collapse" class="btn btn-sm green-jungle"><i class="fa fa-plus"></i>&nbsp;Tambah Pembayaran</a>
                                        <div class="pull-right">
                                            <a href="<?php echo base_url().'administrator/pembayaran/detail_jemaah/'.$pendaftarandetail_id.'/pengembalian'?>"  class="btn btn-sm red-flamingo">Detail Pengembalian&nbsp;<i class="fa fa-arrow-circle-o-right"></i></a>
                                            <a href="<?php echo base_url().'administrator/pendaftaran/detail_jemaah/'.$pendaftarandetail_id?>" class="btn btn-sm grey">Detail Jemaah &nbsp;<i class="fa fa-user"></i></a>
                                        </div>
                                        <div id="collapse" class="panel-collapse collapse">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-5">
                                                        <h4>Silahkan isi form pembayaran berikut :</h4>
                                                        <table class="table col-md-offset-1">
                                                            <tr>
                                                                <td style="width:160px">Metode Transaksi</td>
                                                                <td>:</td>
                                                                <td>
                                                                    <select class="form-control input-sm" id="metode_transaksi" name="metode_transaksi" onchange="toggle_form(this.value+' Pembayaran')">
                                                                        <option value="Tunai">Tunai</option>
                                                                        <option value="Transfer">Transfer</option>
                                                                        <option value="EDC">EDC</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Tanggal Pembayaran</td>
                                                                <td>:</td>
                                                                <td><input class="form-control input-sm date-picker " type="text" value="<?php echo date('m/d/Y')?>" name="tgl_bayar" autocomplete="off" placeholder="Tanggal Penyerahan" required></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Status Pembayaran</td>
                                                                <td>:</td>
                                                                <td>
                                                                    <select name="status_pembayaran" onChange="cek_kurs(this.value,'<?php echo $dt_jemaah[0]->harus_bayar_usd ?>')" class="form-control input-sm" id="status_pembayaran" required>
                                                                        <option value="">Pilih Status ...</option>
                                                                        <option value="Setoran Awal">Paket | Setoran Awal</option>
                                                                        <option value="Setoran Tambahan">Paket | Setoran Tambahan</option>
                                                                        <option value="Pelunasan">Paket | Pelunasan</option>
                                                                        <option value="Visa">Visa Progressive</option>
                                                                        <option value="Mahram">Mahram</option>
                                                                        <option value="Lainnya">Lainnya</option>
                                                                    </select>
                                                                    <div id="pelunasan_warning"></div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Jumlah Pembayaran</td>
                                                                <td>:</td>
                                                                <td><input type="text" class="form-control input-sm" name="jumlah_bayar" placeholder="Rp. -" id="jumlah_bayar" autocomplete="off" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" onkeypress='return event.charCode >= 45 && event.charCode <= 57' required></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Catatan</td>
                                                                <td>:</td>
                                                                <td><textarea class="form-control input-sm" name="catatan" maxlength="500" placeholder="Maksimal 500 Karakter" id="jumlah_bayar" autocomplete="off"></textarea></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-5 form-transfer" style="padding-left:50px">
                                                    </div>
                                                    <div class="col-md-12 col-md-offset-3">
                                                        <button type="button" onclick="edit_function('hide_form_bayar')" class="btn-submit_cancel btn btn-sm grey">Cancel &nbsp;<i class="fa fa-times"></i></button>            
                                                        <button type="submit" class="btn-submit display-none btn-sm btn green-jungle">Submit &nbsp;<i class="fa fa-save"></i></button>
                                                        <input type="hidden" name ="pendaftarandetail_id" id="pendaftarandetail_id" value="<?php echo $pendaftarandetail_id?>">
                                                        <input type="hidden" name="jenis_transaksi" value="Pembayaran">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <br>
                                    </div>
                                <?php echo form_close();?>  
                                <?php 
                                $infant                 = $dt_jemaah[0]->infant ? $dt_jemaah[0]->infant : 0;
                                $sharing_bed            = $dt_jemaah[0]->sharing_bed ? $dt_jemaah[0]->sharing_bed : 0;
                                $biaya_kelas_pesawat    = $dt_jemaah[0]->biaya_kelas_pesawat ? $dt_jemaah[0]->biaya_kelas_pesawat : 0;
                                $harga_setelah_potongan = $dt_jemaah[0]->harus_bayar-($infant+$sharing_bed)+$biaya_kelas_pesawat;
                                $harga_setelah_potongan = ($harga_setelah_potongan + $dt_jemaah[0]->total_biaya_tambahan) - $dt_jemaah[0]->total_biaya_pengurang;
                                // echo_array($dt_jemaah);
                                ?>

                                    <div class="col-md-6" style="padding-left:0px" >
                                        <label>Jumlah Harus Bayar : <b>Rp. <?php echo number_format_decimal($harga_setelah_potongan) ?></b> <a  data-toggle="collapse" href="#collapse2">(Lihat Selengkapnya)</a></label>
                                        <div id="collapse2" class="panel-collapse collapse">
                                            <table class="table table-condensed ">

                                                <tr>
                                                    <td><small>Harga Paket : </small></td>
                                                    <?php if($dt_jemaah[0]->harus_bayar_usd){ ?>
                                                        <td><small class="pull-right"><b><?php echo '<i style="font-size:11px">(Rate '.number_format($dt_jemaah[0]->kurs_dollar).')</i>&nbsp;&nbsp;$'.number_format_decimal($dt_jemaah[0]->harus_bayar_usd).' | '. number_format_decimal($dt_jemaah[0]->harus_bayar) ?></small></td>
                                                    <?php } else { ?>
                                                        <td><small class="pull-right"><b><?php echo number_format_decimal($dt_jemaah[0]->harus_bayar) ?></small></td>
                                                    <?php } ?>

                                                </tr>
                                                <?php if($infant || $sharing_bed || $biaya_kelas_pesawat){ 
                                                    if($infant){ ?>
                                                        <tr>
                                                            <td><small>Jemaah Infant :</small></td>
                                                            <td><small class="pull-right">(-) <?php echo number_format_decimal($infant) ?></small></td>
                                                        </tr>
                                                    <?php }
                                                    if($sharing_bed){ ?>
                                                        <tr>
                                                            <td><small>Jemaah Sharing Bed :</small></td>
                                                            <td><small class="pull-right">(-) <?php echo number_format_decimal($sharing_bed) ?></small></td>
                                                        </tr>
                                                    <?php }
                                                    if($biaya_kelas_pesawat){ ?>
                                                        <tr>
                                                            <td><small>Biaya <?php echo $dt_jemaah[0]->kelas_pesawat ?> :</small></td>
                                                            <td><small class="pull-right">(+) <?php echo number_format_decimal($biaya_kelas_pesawat) ?></small></td>
                                                        </tr>
                                                    <?php } 
                                                }

                                                $biaya_tambahan = $this->md_pendaftaran_detail_biaya->getPendaftaranDetailBiayaByPendaftaranDetailIdByJenis($dt_jemaah[0]->pendaftarandetail_id,'Tambahan');
                                                if($biaya_tambahan){
                                                  foreach($biaya_tambahan as $bt){
                                                    $text = '';
                                                    if($bt->jumlah_usd>0)
                                                      $text = '<i><small>(Rate '.number_format($bt->kurs_dollar).')</small></i>&nbsp;&nbsp;$'.number_format_decimal($bt->jumlah_usd).' | ';

                                                    echo '  <tr>
                                                                <td><small>'.$bt->biaya.' :</small></td>
                                                                <td><small class="pull-right">'.$text.'(+) '.number_format_decimal($bt->jumlah).'</small></td>
                                                            </tr>';
                                                  }
                                                }

                                                $biaya_pengurang = $this->md_pendaftaran_detail_biaya->getPendaftaranDetailBiayaByPendaftaranDetailIdByJenis($dt_jemaah[0]->pendaftarandetail_id,'Pengurang');
                                                if($biaya_pengurang){
                                                  foreach($biaya_pengurang as $bp){
                                                    $text = '';
                                                    if($bp->jumlah_usd>0)
                                                      $text = '<i><small>(Rate '.number_format($bt->kurs_dollar).')</small></i>&nbsp;&nbsp;$'.number_format_decimal($bt->jumlah_usd).' | ';

                                                    echo '  <tr>
                                                                <td><small>'.$bp->biaya.' :</small></td>
                                                                <td><small class="pull-right">'.$text.'(-) '.number_format_decimal($bp->jumlah).'</small></td>
                                                            </tr>';
                                                  }
                                                } 

                                            ?>
       
                                                <tr>
                                                    <td><small>Total Biaya Tambahan<br> <i>(Diluar Sharing Bed & Kelas Pesawat)</i></td>
                                                    <td><small><span class="pull-right"><b><?php echo number_format_decimal($dt_jemaah[0]->total_biaya_tambahan) ?></span></td>
                                                </tr>
                                                <tr>
                                                    <td><small>Total Biaya Pengurang<br> <i>(Diluar Infant)</i></td>
                                                    <td><small><span class="pull-right"><b><?php echo number_format_decimal($dt_jemaah[0]->total_biaya_pengurang) ?></span></td>
                                                </tr>
                                            
                                                    <tr>
                                                        <td><small>Total :</small></td>
                                                        <td><small class="pull-right"><b>Rp. <?php echo number_format_decimal($harga_setelah_potongan) ?></b></small></td>
                                                    </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                          
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr class="bg-green-jungle font-white">
                                                <th style="text-align: center;">No</th>
                                                <th style="text-align: center;">No Kwitansi</th>
                                                <th style="text-align: center;">Jenis Pembayaran</th>
                                                <th style="text-align: center;">Tanggal Input Pembayaran</th>
                                                <th>Jumlah</th>
                                                <th style="text-align: center;">Petugas</th>
                                                <th>Status</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $no=0;
                                                $sisa = 0;
                                                $total = 0;
                                                foreach($dt_pembayaran as $row){
                                                    $id = $row->pembayaran_id;
                                                    if($row->status_pembayaran == 'Setoran Awal' || $row->status_pembayaran == 'Setoran Tambahan' || $row->status_pembayaran == 'Pelunasan'){

                                                        $total += (float) $row->jumlah_bayar;
                                                    }
                                            ?>
                                                <tr>
                                                    <td style="text-align: center;"><?php echo ++$no; ?></td>
                                                    <td style="text-align: center;"><?php echo $row->no_kwitansi ?></td>
                                                    <td style="text-align: center;"><?php echo $row->status_pembayaran ?></td>
                                                    <td style="text-align: center;"><i class="fa fa-calendar"></i> <?php echo date('d-M-Y H:i',strtotime($row->tgl_update)) ?></td>
                                                    <td>Rp <span class="pull-right"><?php echo number_format_decimal($row->jumlah_bayar) ?></span></td>
                                                    <td style="text-align: center;"><?php echo $row->nama_lengkap ?></td>
                                                    <td><?php echo $row->status_verifikasi_style ?></td>
                                                    <td><div class="btn-group" >
                                                            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                                <i class="fa fa-angle-down"></i>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu">
                                                                <li><a href="javascript:;" onClick="edit_function('show_pembayaran',<?php echo $row->pembayaran_id ?>)" class="btn_detail<?php echo $id ?>"><i class="fa fa-search"></i>&nbsp;Detail</a></li>                                            
                                                                <li><a href="<?php echo base_url().'administrator/pembayaran/cetak/'.$row->pembayaran_id ?>" target="_blank" class="btn_edit<?php echo $id ?>"><i class="fa fa-print"></i>&nbsp;Cetak</a></li>
                                                                <li><a href="javascript:;" onClick="edit_function('delete',<?php echo $id ?>,'Pembayaran');" class="btn_hapus<?php echo $id ?>"><i class="fa fa-trash"></i>&nbsp;Batalkan</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table> 
                                    <div class="col-md-6 pull-right" style="padding-right: 0px">
                                        <table class="table table-condensed">
                                            <tr>
                                                <td width="300">Total <small>(Setoran Awal, Tambahan dan Pelunasan)</small></td>
                                                <td><span class="pull-right"><?php echo number_format_decimal($total) ?></span></td>
                                            </tr>
                                            <tr>
                                                <td>Harus Bayar</td>
                                                <td><span class="pull-right"><?php echo number_format_decimal($harga_setelah_potongan) ?></span></td>
                                            </tr>
                                            <tr>
                                                <td>Sisa</td>
                                                <td><span class="pull-right"><b><?php echo number_format_decimal($harga_setelah_potongan-$total) ?></b></span></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                
                <?php } else if($show_detail_pengembalian){ ?>
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <i class="fa fa-list font-dark"></i>
                                    <span class="caption-subject bold uppercase"> List Pengembalian Jemaah <span class="font-red">"<?php echo $dt_jemaah[0]->nama_jemaah ?>"</span></span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <?php 
                                    if($this->session->flashdata('jemaah_cancel')){ echo '<label class="alert alert-danger"><i class="fa fa-check"></i> Pembatalan berhasil. Keberangkatan Jemaah dibatalkan !</label>';}
                                    if($this->session->flashdata('add_success')){ echo '<label class="alert alert-success"><i class="fa fa-check"></i> '.$this->session->flashdata('sucess').' jemaah disimpan</label>';}
                                ?>

                                <?php echo form_open('administrator/pembayaran/add/pembayaran', array( 'id' => 'add-pembayaran-form')); ?>
                                    <div class="portlet-body form" >
                                        <a href="<?php echo base_url().'administrator/pembayaran' ?>" class="btn btn-sm yellow-crusta"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;Kembali</a>
                                        <a data-toggle="collapse" href="#collapse" class="btn btn-sm red-flamingo"><i class="fa fa-plus"></i>&nbsp;Tambah Pengembalian</a>
                                        <div class="pull-right">
                                            <a href="<?php echo base_url().'administrator/pembayaran/detail_jemaah/'.$pendaftarandetail_id.'/pembayaran'?>"  class="btn btn-sm green-jungle">Detail Pembayaran&nbsp;<i class="fa fa-arrow-circle-o-right"></i></a>
                                            <a href="<?php echo base_url().'administrator/pendaftaran/detail_jemaah/'.$pendaftarandetail_id?>" class="btn btn-sm grey">Detail Jemaah &nbsp;<i class="fa fa-user"></i></a>
                                        </div>
                                        <div id="collapse" class="panel-collapse collapse">                                      
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-6">
                                                        <h4>Silahkan isi form pengembalian berikut :</h4>
                                                        <table class=" col-md-offset-1 table">
                                                            <tr>
                                                                <td width="220">Metode Transaksi</td>
                                                                <td width="30">:</td>
                                                                <td>
                                                                    <select class="form-control input-sm" id="metode_transaksi" name="metode_transaksi" onchange="toggle_form(this.value+' Pengembalian')">
                                                                        <option value="Tunai">Tunai</option>
                                                                        <option value="Transfer">Transfer</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Tanggal Pengembalian</td>
                                                                <td>:</td>
                                                                <td><input class="form-control input-sm date-picker " type="text" value="<?php echo date('m/d/Y')?>" name="tgl_pengembalian" autocomplete="off" placeholder="Tanggal Penyerahan" required></td>
                                                            </tr>
                                                            <tr id="row_jenis_pengembalian">
                                                                <td>Jenis Pengembalian</td>
                                                                <td>:</td>
                                                                <td>
                                                                    <?php if($dt_jemaah[0]->status_keberangkatan=='Sudah Berangkat' || $dt_jemaah[0]->status_keberangkatan=='Cancel'){$hide_cancel=TRUE;}else{$hide_cancel=FALSE;} ?>    
                                                                    <select name="jenis_pengembalian" class="form-control input-sm" onchange="toggle_cancel_form(this.value)" required>
                                                                        <option value="Berlebih">Berlebih</option>
                                                                        <option value="Cancel" <?php echo $hide_cancel ? 'disabled' : '' ?>>Cancel/Pembantalan</option>
                                                                    </select>
                                                                    <input type="hidden" id="setoran_awal" value="<?php echo $total_setoran_awal ?>">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Jumlah Pengembalian </td>
                                                                <td>:</td>
                                                                <td><input type="text" class="form-control input-sm" name="jumlah_pengembalian" autocomplete="off" placeholder="Rp. -" id="jumlah_bayar" onkeypress='return event.charCode >= 45 && event.charCode <= 57' required></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Catatan</td>
                                                                <td>:</td>
                                                                <td><textarea class="form-control input-sm" name="catatan" maxlength="500" placeholder="Maksimal 500 Karakter" id="jumlah_bayar" autocomplete="off" required></textarea></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-5 form-transfer" style="padding-left:50px">

                                                    </div>
                                                    <div class="col-md-12 col-md-offset-3">
                                                        <input type="hidden" name ="pendaftarandetail_id" id="pendaftarandetail_id" value="<?php echo $pendaftarandetail_id?>">
                                                        <input type="hidden" name="jenis_transaksi" value="Pengembalian">
                                                        <button type="button" onclick="edit_function('hide_form_bayar')" class="btn btn-sm grey">Cancel &nbsp;<i class="fa fa-times"></i></button>            
                                                        <button type="submit" class="btn_update btn-sm btn green-jungle">Submit &nbsp;<i class="fa fa-save"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="clearfix"></div>
                                    </div>
                                <?php echo form_close();?>                          
                                <table class="table">
                                    <thead>
                                        <tr class="bg-red-flamingo font-white">
                                            <th style="text-align: center;">No</th>
                                            <th style="text-align: center;">No Kwitansi</th>
                                            <th style="text-align: center;">Jenis Pengembalian</th>
                                            <th style="text-align: center;">Tanggal Input Pengembalian</th>
                                            <th>Jumlah</th>
                                            <th>Petugas</th>
                                            <th>Status</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $no=0;
                                            foreach($dt_pengembalian as $row){
                                                $id = $row->pembayaran_id;
                                        ?>
                                            <tr>
                                                <td style="text-align: center;"><?php echo ++$no; ?></td>
                                                <td style="text-align: center;"><?php echo $row->no_kwitansi ?></td>
                                                <td style="text-align: center;"><?php echo $row->jenis_pengembalian ?></td>
                                                <td style="text-align: center;"><i class="fa fa-calendar"></i> <?php echo date('d-M-Y H:i',strtotime($row->tgl_update)) ?></td>
                                                <td>Rp <span class="pull-right"><?php echo number_format($row->jumlah_bayar) ?></span></td>
                                                <td><?php echo $row->nama_lengkap ?></td>
                                                <td><?php echo $row->status_verifikasi_style ?></td>
                                                <td><div class="btn-group" >
                                                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="javascript:;" onClick="edit_function('show_pembayaran',<?php echo $row->pembayaran_id ?>)" class="btn_detail<?php echo $id ?>"><i class="fa fa-search"></i>&nbsp;Detail</a></li>                                            
                                                            <li><a href="<?php echo base_url().'administrator/pembayaran/cetak/'.$row->pembayaran_id ?>" target="_blank" class="btn_edit<?php echo $id ?>"><i class="fa fa-print"></i>&nbsp;Cetak</a></li>
                                                            <li><a href="javascript:;" onClick="edit_function('delete',<?php echo $id ?>,'Pengembalian');" class="btn_hapus<?php echo $id ?>"><i class="fa fa-trash"></i>&nbsp;Batalkan</a></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                    <?php } ?>
                                    </tbody>
                                </table> 
                            </div>
                        </div>
                    </div>
                
                <?php } else if($show_verif){?>
                    <div class="col-md-8">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-red-sunglo">
                                    <i class="icon-settings font-red-sunglo"></i>
                                    <span class="caption-subject bold uppercase">Verifikasi Pembayaran</span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <?php echo form_open('administrator/pembayaran/update_verif', array( 'id' => 'verif-form')); ?>
                                        <?php if($dt_byr[0]->status_verifikasi=='Acceptabangjahat'){ ?>
                                                <label class="alert alert-success col-md-12"><i class="fa fa-check"></i>&nbsp; Pembayaran diterima</label>
                                        <?php } else if($dt_byr[0]->status_verifikasi=='Decline'){ ?>
                                                <label class="alert alert-danger col-md-12"><i class="fa fa-times"></i>&nbsp; Pembayaran ditolak</label>
                                        <?php } ?>
                                        <h3>Telah dilakukannya pembayaran pada :</h3>
                                        <table class="table">
                                            <tr>
                                                <td>Kantor Cabang</td><td>:</td>
                                                <td><?php echo $dt_byr[0]->nama_kantor ?></td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td><td>:</td>
                                                <td><?php echo $dt_byr[0]->alamat ?></td>
                                            </tr>
                                            <tr>
                                                <td>Telephone</td><td>:</td>
                                                <td><?php echo $dt_byr[0]->telp ?></td>
                                            </tr>
                                            <tr>
                                                <td>Handphone</td><td>:</td>
                                                <td><?php echo $dt_byr[0]->hp ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Memproses Pembayaran</td><td>:</td>
                                                <td><?php echo $dt_byr[0]->tgl_bayar ?></td>
                                            </tr>
                                        </table>

                                        <h3> dengan rincian pembayaran sebagai berikut :</h3>
                                            <table class="table">
                                                <tr>
                                                    <td>Nama Jemaah</td><td>:</td>
                                                    <td><?php echo $dt_byr[0]->nama_jemaah ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Handphone</td><td>:</td>
                                                    <td><?php echo $dt_byr[0]->no_hp ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Paket</td><td>:</td>
                                                    <td><?php echo $dt_byr[0]->nama_paket ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Harus Bayar (Harga Paket)</td><td>:</td>
                                                    <td><?php echo $dt_byr[0]->harus_bayar ?></td>
                                                </tr>
                                                    <td>Tanggal Pembayaran Ke Rekening RWH</td><td>:</td>
                                                    <td><?php echo $dt_byr[0]->tgl_bayar_bank ?></td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>Rekening Pengirim</td><td>:</td>
                                                    <td><?php echo $dt_byr[0]->bank_pengirim.', a/n. '.$dt_byr[0]->nama_rek_pengirim ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Jenis Pembayaran</td><td>:</td>
                                                    <td><?php echo $dt_byr[0]->status_pembayaran ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Jumlah Bayar</td><td>:</td>
                                                    <td><?php echo rupiah_format($dt_byr[0]->jumlah_bayar) ?></td>
                                                </tr>
                                            </table>
                                    <div class="form-actions">
                                        <input type="hidden" name="pembayaran_id" id="pembayaran_id" value="<?php echo $dt_byr[0]->pembayaran_id ?>">
                                        <?php if($dt_byr[0]->status_verifikasi){ ?>
                                            <h3><i class="fa fa-info-circle"></i>&nbsp;Verifikasi telah dilakukan oleh :</h3>
                                            <div class="col-md-12">
                                                <table class="table">
                                                    <tr>
                                                        <td>Kantor Pusat</td><td>:</td>
                                                        <td><?php echo $dt_byr[0]->kantor_pusat_nama?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kontak</td><td>:</td>
                                                        <td><?php echo $dt_byr[0]->no_hp?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kantor Pusat</td><td>:</td>
                                                        <td><?php echo $dt_byr[0]->kantor_pusat_nama?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tanggal Verifikasi</td><td>:</td>
                                                        <td><?php echo $dt_byr[0]->tgl_input?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Status</td><td>:</td>
                                                        <td><?php 
                                                                if($dt_byr[0]->status_verifikasi=='Accept'){
                                                                    echo '<span class="font-green-jungle"><i class="fa fa-check"></i>&nbsp;'.$dt_byr[0]->status_verifikasi.'</span>';
                                                                } else {
                                                                    echo '<span class="font-red"><i class="fa fa-times"></i>&nbsp;'.$dt_byr[0]->status_verifikasi.'</span>';
                                                                }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        <?php } else { ?>
                                            <label class="alert alert-info"><i class="fa fa-info-circle"></i>&nbsp;Pastikan memvalidasi data diatas sebelum menerima/menolak pembayaran</label><br>
                                            <button id="btn_verif" name="btn_verif" value="terima" class="btn green-jungle"><i class="fa fa-check"></i>&nbsp;Terima Pembayaran</button>
                                            <button id="btn_verif" name="btn_verif" value="tolak" class="btn red"><i class="fa fa-times"></i>&nbsp;Tolak Pembayaran</button>
                                        <?php } ?>
                                    </div>
                                <?php echo form_close();?>
                            </div>
                        </div>
                    </div>

                <?php } else { ?>
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <?php if($this->session->flashdata('verif')=='terima'){ ?>
                                <label class="alert alert-success col-md-12"><i class="fa fa-check"></i>&nbsp; Pembayaran diterima</label>
                        <?php } else if($this->session->flashdata('verif')=='tolak'){ ?>
                                <label class="alert alert-danger col-md-12"><i class="fa fa-check"></i>&nbsp; Pembayaran ditolak</label>
                        <?php } ?>
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject bold uppercase"> Managed Pembayaran Jemaah</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_1_1" data-toggle="tab"> <i class="icon-reload"></i> On Process </a>
                                    </li>
                                    <li>
                                        <a href="#tab_1_2" data-toggle="tab"> <i class="fa fa-paper-plane-o"></i> Sudah Berangkat </a>
                                    </li>
                                    <li>
                                        <a href="#tab_1_3" data-toggle="tab"> <i class="icon-ban"></i> Cancel </a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane fade <?php echo !$this->session->userdata('jemaah_cancel')? 'active in' : '' ?>" id="tab_1_1">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-inline pull-left">
                                                        <div class="form-group"><span>Filter By : </span></div>
                                                        <div class="form-group"><div id="filter_status_bayar_umroh"></div></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-stripfed table-bordered  table-condensed table-hover" id="table_umroh">
                                            <thead>
                                                <tr>
                                                    <th>NO</th>
                                                    <th>Nama Jemaah</th>
                                                    <th>Tgl Berangkat</th>
                                                    <th>Harus Bayar</th>
                                                    <th>Sudah Bayar</th>
                                                    <th>Sisa</th>
                                                    <th>Status</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="tab_1_2">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-inline pull-left">
                                                        <div class="form-group"><span>Filter By : </span></div>
                                                        <div class="form-group"><div id="filter_status_bayar_sudah_berangkat"></div></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered  table-condensed" id="table_sudah_berangkat">
                                            <thead>
                                                <tr>
                                                    <th>NO</th>
                                                    <th>Nama Jemaah</th>
                                                    <th>Tgl Berangkat</th>
                                                    <th>Harus Bayar</th>
                                                    <th>Sudah Bayar</th>
                                                    <th>Sisa</th>
                                                    <th>Status</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>     
                                    <div class="tab-pane fade <?php echo $this->session->userdata('jemaah_cancel')? 'active in' : '' ?>" id="tab_1_3">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-inline pull-left">
                                                        <div class="form-group"><span>Filter By : </span></div>
                                                        <div class="form-group"><div id="filter_status_bayar_cancel"></div></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered  table-condensed" id="table_cancel">
                                            <thead>
                                                <tr>
                                                    <th>NO</th>
                                                    <th>Nama Jemaah</th>
                                                    <th>Tgl Berangkat</th>
                                                    <th>Harus Bayar</th>
                                                    <th>Sudah Bayar</th>
                                                    <th>Sisa</th>
                                                    <th>Status</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                
                <?php } ?>
            </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<div class="modal fade" id="pembayaran_modal"  role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="portlet light bordered">
            <div class="portlet-body form" >
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="col-md-12 display-none" id="loading">
                            <span>Mengambil data ...</span>
                            <img src="<?php echo base_url().'assets\frontend\img\AjaxLoader.gif'?>">
                        </h4>
                        <div class="panel-group accordion scrollable" id="accordion2">
                            <span class="hide" id="list_detail_pembayaran"></span>
                             <div class="pull-right">
                                <button type="button" data-dismiss="modal" class="btn grey">Close</i></button>            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
