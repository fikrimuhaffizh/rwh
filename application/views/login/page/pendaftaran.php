<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<link href="<?php echo base_url()?>assets/backend/css/star-rating.min.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url()?>assets/backend/js/star-rating.min.js" type="text/javascript"></script>
<?php 
    include 'global_function.php'; 
    $img_link = base_url().'assets/frontend/img/noimagefound.jpg';
?>
<script type="text/javascript">
    var form_count       = 1;
    var count_jemaah     = 1;  
    var multiple_jemaah  = 1;
    var form_tambahan    = 0;
    var form_pengurang   = 0;
    var form_count       = 1;   
    var total_all_jemaah = 0;

    function updateAllTable(){
        table.ajax.reload();
        table2.ajax.reload();
        table3.ajax.reload();
        table4.ajax.reload();
        table5.ajax.reload();
    }

    //CRUD Jemaah
        function edit_function(task='',id='',resp=''){
            if(task == 'edit'){
                url = '<?php echo base_url() ?>administrator/pendaftaran/edit/'+id;
                AjaxCRUD('edit',"",url); 
            }
            else if(task == 'process_respon'){
                $('#nama_lengkap').val(resp[0]['nama_jemaah']);
                $('#alamat').val(resp[0]['alamat']);
                $('#email').val(resp[0]['email']);
                $('#no_hp').val(resp[0]['no_hp']);
                $('#no_hp_2').val(resp[0]['no_hp_2']);
                $('#tgl_lahir').val(resp[0]['tgl_lahir']);
                $('#tempat_lahir').val(resp[0]['tempat_lahir']);
                $('#pendaftaran_id').val(resp[0]['pendaftaran_id']);
                $('#pendaftarandetail_id').val(resp[0]['pendaftarandetail_id']);
                $('#no_ktp').val(resp[0]['no_ktp']);
                $('#catatan').val(resp[0]['catatan']);
                $('#riwayat_penyakit').val(resp[0]['riwayat_penyakit']);
                $('select[name=jenis_jemaah] option[value="'+resp[0]['jenis_jemaah']+'"]').prop("selected", true);
                $('select[name=jenis_kelamin] option[value='+resp[0]['jenis_kelamin']+']').prop("selected", true);
                $('select[name=mahram] option[value='+resp[0]['ada_mahram']+']').prop("selected", true);
                $('select[name=progressive] option[value='+resp[0]['progressive']+']').prop("selected", true);
                $('select[name=kota_keberangkatan] option[value='+resp[0]['id_kota_keberangkatan']+']').prop("selected", true);
                $('select[name=kelas_pesawat] option[value="'+resp[0]['kelas_pesawat']+'"]').prop("selected", true);

                $('select[name=infant] option[value=Tidak]').prop("selected", true);
                if(resp[0]['infant']>0)
                  $('select[name=infant] option[value=Ya]').prop("selected", true);
            
                  
                show_biaya_tambahan('Tidak','modal','sharing_bed',0);
                $('select[name=sharing_bed] option[value=Tidak]').prop("selected", true);
                if(resp[0]['sharing_bed']>0){
                  $('select[name=sharing_bed] option[value=Ya]').prop("selected", true);
                  show_biaya_tambahan('Ya','modal','sharing_bed',resp[0]['sharing_bed']);
                }

                show_biaya_tambahan(resp[0]['kelas_pesawat'],'modal','kelas_pesawat',0);
                if(resp[0]['biaya_kelas_pesawat']){
                  show_biaya_tambahan(resp[0]['kelas_pesawat'],'modal','kelas_pesawat',resp[0]['biaya_kelas_pesawat']);
                }

                //jika ikut salah satu member
                    if(resp[0]['member_id']){

                        $('#choose_member').removeAttr('disabled');
                        $('#choose_member option[value="'+resp[0]['member_id']+'"]').attr("selected",1).trigger('change');
                        $('.checkbox_member')[0].checked = true;
                    } else {

                        $('.checkbox_member')[1].checked = true;
                    }



                $('.form_paket').hide();
                $('.list_paket').remove();
                $('.pilihan_paket').remove();
                $("#list_biaya_tambahan_0").empty();
                $("#list_biaya_pengurang_0").empty();


                //Booking Paket Reguler
                  if(resp[0]['jenis'] == 'Reguler'){

                      harga_usd = resp[0]['harga_usd'] ? ' ('+resp[0]['harga_usd']+')' : '';
                      var x  = '<div class="pilihan_paket">';
                          x += '  <h4 class="marginbot-clear">'+resp[0]['nama_paket']+'</h4> &nbsp;<i class="fa fa-plane"></i>&nbsp;'+resp[0]['berangkat_awal']+' | <i class="fa fa-calendar"></i>&nbsp; '+resp[0]['bulan_berangkat']+' '+resp[0]['tahun_berangkat']+' | '+resp[0]['kelas']+', '+resp[0]['harga']+harga_usd+'<hr>';
                          x += '   <span class="hide"><input type="radio" checked class="pwk_id" name="pwk_id[]" id="pwk_id[]" value="'+resp[0]['pwk_id']+'"> '+resp[0]['nama_paket']+' | &nbsp;<i class="fa fa-plane"></i>&nbsp;'+resp[0]['berangkat_awal']+' | &nbsp;<i class="fa fa-calendar"></i>&nbsp;'+resp[0]['bulan_berangkat']+' '+resp[0]['tahun_berangkat']+' <br> '+resp[0]['kelas']+', '+resp[0]['harga']+'</span>';
                          x += '</div>'

                      $('#pilihan_paket').append(x);                
                      $('#harus_bayar').val(resp[0]['harga']);
                  }

                //Booking Paket haji
                  else{

                      var x  = '<div class="pilihan_paket">';
                          x += '  <h4 class="marginbot-clear">'+resp[0]['nama_paket']+'</h4> &nbsp;<i class="fa fa-refresh"></i>&nbsp; Estimasi berangkat tahun '+resp[0]['tahun_est_berangkat']+' | <i class="icon-trophy"></i>&nbsp; '+resp[0]['jenis_quota']+' | &nbsp; '+resp[0]['harga']+'<hr>';
                          x += '   <span class="hide"><input type="radio" checked class="paket_id" name="paket_id[]" id="paket_id[]" value="'+resp[0]['paket_id']+'"> &nbsp;<i class="fa fa-refresh"></i>&nbsp; Estimasi berangkat tahun '+resp[0]['tahun_est_berangkat']+' | <i class="icon-trophy"></i>&nbsp; '+resp[0]['jenis_quota']+' | <i class="fa fa-dollar"></i>&nbsp; '+resp[0]['harga']+'</span>';
                          x += '</div>'

                      $('#pilihan_paket').append(x);                

                  }

                //Biaya Tambahan dan Pengurang
                  var list_biaya_tambahan = resp[0]['list_biaya_tambahan'];
                  for(var i=0;i<list_biaya_tambahan.length;i++){
                    biaya      = list_biaya_tambahan[i]['biaya'];
                    jumlah_rp  = list_biaya_tambahan[i]['jumlah'];
                    jumlah_usd = list_biaya_tambahan[i]['jumlah_usd'];
                    kurs_dollar= list_biaya_tambahan[i]['kurs_dollar'];

                    if(jumlah_usd>0){
                      jumlah     = jumlah_usd;
                      use_dollar = 'checked';
                      detail_info = '<a class="tooltips" data-placement="top" title="'+jumlah_rp+' ('+jumlah_usd+' * '+kurs_dollar+')"><i class="fa fa-info-circle"></i></a>';

                    }else{
                      jumlah = jumlah_rp;
                      use_dollar = '';
                      detail_info= '';
                    }

                    form_tambahan++;
                    var x='   <div class="form_tambahan_0_'+form_tambahan+' margin-bottom-5">';
                        x+='    <div class="col-md-7 noPaddingLeft"><input type="text" class="form-control input-sm" id="nama_biaya_tambahan_0_'+form_tambahan+'" name="nama_biaya_tambahan_0[]" placeholder="Biaya Tambahan" value="'+biaya+'" required></div>';
                        x+='    <div class="col-md-5 noPaddingLeft noPaddingRight"><input type="text" id="biaya_tambahan_0_'+form_tambahan+'" onkeypress="return event.charCode >= 48 && event.charCode <= 57" class="form-control input-sm" value="'+jumlah+'" placeholder="Jumlah" autocomplete="off" name="biaya_tambahan_0[]" required></div>';
                        x+='    <label class="mt-checkbox mt-checkbox-outline"><input type="checkbox" onClick="cekKurs(\'0_'+form_tambahan+'\',\'biaya_tambahan\')" id="biaya_tambahan_kurs_dollar_0_'+form_tambahan+'" name="biaya_tambahan_kurs_dollar_0[]" '+use_dollar+'><small> Harga dalam dollar ($) '+detail_info+'</small><span></span></label>';
                        x+='    <input type="hidden" id="biaya_tambahan_0_'+form_tambahan+'" value="'+kurs_dollar+'" name="kurs_dollar_biaya_tambahan_0[]">';                        
                        x+='    <a class="label label-danger pull-right" onClick="removeFormBiaya(\'0_'+form_tambahan+'\',\'Tambahan\')"><i class="fa fa-times"></i></a><div class="clearfix"></div>';
                        x+='  </div>';
                    $("#list_biaya_tambahan_0").prepend(x);
                  }

                  var list_biaya_pengurang = resp[0]['list_biaya_pengurang'];
                  for(var i=0;i<list_biaya_pengurang.length;i++){
                    biaya       = list_biaya_pengurang[i]['biaya'];
                    jumlah_rp   = list_biaya_pengurang[i]['jumlah'];
                    jumlah_usd  = list_biaya_pengurang[i]['jumlah_usd'];
                    kurs_dollar = list_biaya_pengurang[i]['kurs_dollar'];

                    if(jumlah_usd>0){
                      jumlah      = jumlah_usd;
                      use_dollar  = 'checked';
                      detail_info = '<a class="tooltips" data-placement="top" title="'+jumlah_rp+' ('+jumlah_usd+' * '+kurs_dollar+')"><i class="fa fa-info-circle"></i></a>';

                    }else{
                      jumlah = jumlah_rp;
                      use_dollar = '';
                      detail_info= '';
                    }

                    form_pengurang++;
                    var x='   <div class="form_pengurang_0_'+form_pengurang+' margin-bottom-5">';
                        x+='    <div class="col-md-7 noPaddingLeft"><input type="text" class="form-control input-sm" id="nama_biaya_pengurang_0_'+form_pengurang+'" name="nama_biaya_pengurang_0[]" placeholder="Pengurang Biaya" value="'+biaya+'" required></div>';
                        x+='    <div class="col-md-5 noPaddingLeft noPaddingRight"><input type="text" id="biaya_pengurang_0_'+form_pengurang+'" onkeypress="return event.charCode >= 48 && event.charCode <= 57" class="form-control input-sm" placeholder="Jumlah" value="'+jumlah+'" autocomplete="off" name="biaya_pengurang_0[]" required></div>';
                        x+='    <label class="mt-checkbox mt-checkbox-outline"><input type="checkbox" onClick="cekKurs(\'0_'+form_pengurang+'\',\'pengurang\')" id="pengurang_kurs_dollar_0_'+form_pengurang+'" name="pengurang_kurs_dollar_0[]" '+use_dollar+'><small> Harga dalam dollar ($) '+detail_info+'</small><span></span></label>';                        
                        x+='    <input type="hidden" id="biaya_tambahan_0_'+form_pengurang+'" value="'+kurs_dollar+'" name="kurs_dollar_pengurang_0[]">';                                                
                        x+='    <a  class="label label-danger pull-right" onClick="removeFormBiaya(\'0_'+form_pengurang+'\',\'Pengurang\')"><i class="fa fa-times"></i></a><div class="clearfix"></div>';
                        x+='  </div>';
                    $("#list_biaya_pengurang_0").prepend(x); 
                  }

                $(".tgl_lahir").datepicker({format: 'dd/mm/yyyy'});
                $('.header-modal').text('Edit Pendaftaran');
                $('.btn_add').hide();
                $('.btn_update').removeClass('hide');
                $('.btn-show_edit_paket').show();
                $('.btn-cancel_edit_paket').hide();
                $('#add_edit_modal').modal();
            }
            else if(task == 'delete'){
                url = '<?php echo base_url() ?>administrator/pendaftaran/delete/'+id;
                swal({
                  title: "Hapus Data?",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "grey",
                  confirmButtonText: "Yes, delete it!",
                  closeOnConfirm: false,
                  showLoaderOnConfirm: true
                },
                function(){
                    $.ajax({
                      method: 'POST',
                      url: url,
                      dataType: 'json',
                      success : function(resp){
                          if(resp == 'update_success'){
                              swal.close();
                              updateAllTable();
                          }
                      }
                  });
                });
            }
            else if(task == 'cancel'){

                url = '<?php echo base_url() ?>administrator/pendaftaran/cancel/'+id;
                swal({

                  title: "Batalkan Jemaah ?",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Ya, Batalkan Jemaah",
                  closeOnConfirm: false,
                  showLoaderOnConfirm: true

                },
                function(){

                    $.ajax({

                      method: 'POST',
                      url: url,
                      dataType: 'json',
                      success : function(resp){

                          if(resp == 'update_success'){

                              swal.close();
                              updateAllTable();
                          }

                      }

                  });
                });
            }      
            else if(task == 'process_again'){

                url = '<?php echo base_url() ?>administrator/pendaftaran/process_again/'+id;
                swal({

                  title: "Proses Kembali Jemaah ?",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Ya, Proses Jemaah",
                  closeOnConfirm: false,
                  showLoaderOnConfirm: true

                },
                function(){

                    $.ajax({

                      method: 'POST',
                      url: url,
                      dataType: 'json',
                      success : function(resp){

                          if(resp == 'update_success'){

                              swal.close();
                              updateAllTable();
                          }

                      }

                  });
                });
            }       
            else if(task == 'update'){
              //Agar melewati validasi di fungsi validateform()
                count_jemaah++;
              //Cek Data Jemaah. List form yang tidak mengapa jika kosong 
                var x = validateAddForm();
                if(x!='ok'){
                    return false;
                }

                $('#add_edit_modal').modal('hide');
                url = '<?php echo base_url() ?>administrator/pendaftaran/update';
                $.ajax({
                    method: 'POST',
                    url: url,
                    data : $('#add-form-jemaah').serialize(),
                    dataType: 'json',
                    success : function(resp){
                        if(resp == "update_success"){
                            swal({
                              title: "Berhasil !",
                              text : "Data jemaah berhasil diperbaharui",
                              type: "success",
                              timer: 800,
                              showConfirmButton : false
                            });
                            updateAllTable();
                        } 
                    }
                });                
            }
        }

        function detail_function(task='',id=''){

            $('#detail_avatar').attr('src','<?php echo $img_link ?>');
            $('#detail_avatar_mobile').attr('src','<?php echo $img_link ?>');
            $('#detail_avatar_href').attr('href','');
            $('#detail_avatar_mobile_href').attr('href','');


            $('.reguler').hide('slow');
            $('.haji').hide('slow');
            url = '<?php echo base_url() ?>administrator/paket/detail/'+id;
            $.ajax({

                method: 'POST',
                url: url,
                dataType: 'json',
                success : function(resp){

                    $('#detail_avatar').attr('src',resp['paket_pic']);
                    $('#detail_avatar_mobile').attr('src',resp['paket_mobile_pic']);


                    $('#detail_avatar_href').attr('href',resp['paket_pic']);
                    $('#detail_avatar_mobile_href').attr('href',resp['paket_mobile_pic']);


                    $('.reguler').show('slow');
                    $('#detail_nama_paket').text(resp['nama_paket']);
                    $('#detail_jenis_paket').text(resp['jenis_paket']);
                    $('#detail_berangkat_awal').text(resp['berangkat_awal']);
                    $('#detail_berangkat_landing').text(resp['berangkat_landing']);
                    $('#detail_berangkat_transit').text(resp['berangkat_transit']);
                    $('#detail_info_plus').html(resp['info_plus']);
                    $('#detail_lama_hari').text(resp['lama_hari']);
                    $('#detail_mulai_publish').text(resp['mulai_publish']);
                    $('#detail_akhir_publish').text(resp['akhir_publish']);


                    //Maskapai

                    $('.detail_maskapai_avatar').remove();
                    for(var i=0;i<resp['maskapai'].length;i++){

                        var o = '<div class="col-md-12 detail_maskapai_avatar" style="padding:0px">';
                            o+= '   <a data-toggle="collapse" href="#collapse_maskapai_'+i+'"><i class="fa fa-arrow-circle-o-right"></i>&nbsp;'+resp['maskapai'][i]['nama_maskapai']+'</a>';
                            o+= '   <div id="collapse_maskapai_'+i+'" class="panel-collapse collapse">';
                            o+= '       <div class="panel-body">';
                            o+= '           <img class="detail_maskapai_avatar" src="'+resp['maskapai'][i]['logo']+'"><br><br>'; 

                            o+= '           <span id="deskripsi">'+resp['maskapai'][i]['deskripsi']+'</span>';                         

                            o+= '       </div>';
                            o+= '   </div>';
                            o+= '</div>';


                        $('#detail_maskapai').after(o);                 
                    }



                    //Hotel

                    $('.detail_hotel_list').remove();
                    for(var i=0;i<resp['paket_akomodasi'].length;i++){

                        var o = '<div class="col-md-12 detail_hotel_list" style="padding:0px">';
                            o+= '   <a data-toggle="collapse" href="#collapse_'+i+'"><i class="fa fa-arrow-circle-o-right"></i>&nbsp;'+resp['paket_akomodasi'][i]['nama_hotel']+'</a>';
                            o+= '   <div id="collapse_'+i+'" class="panel-collapse collapse">';
                            o+= '       <div class="panel-body">';
                            o+= '           <input type="number" id="detail_hotel_bintang_'+i+'" class="rating" data-show-clear="false" data-show-caption="false" data-size="xs" data-readOnly="true" value="'+resp['paket_akomodasi'][i]['bintang']+'"/>'

                            o+= '           <img style="width:100%" class="detail_hotel_avatar" src="'+resp['paket_akomodasi'][i]['foto']+'"><br><br>'; 

                            o+= '           <span id="deskripsi">'+resp['paket_akomodasi'][i]['deskripsi']+'</span>';                         

                            o+= '       </div>';
                            o+= '   </div>';
                            o+= '</div>';


                        $('#detail_hotel').after(o);
                        $('#detail_hotel_bintang_'+i+'').rating();
                    }

                    

                }

            });


            $('#detail_modal').modal();
        }  

        function show_edit_paket(){

            $('.form_paket').show();
            $('.btn-show_edit_paket').hide();
            $('.btn-cancel_edit_paket').show();
        }

        function cancel_edit_paket(){

            $('.list_paket').remove();
            $('.form_paket').hide();
            $('.btn-show_edit_paket').show();
            $('.btn-cancel_edit_paket').hide();
        }

        function gantiStatusVerif(id){

            val = $('#status_verifikasi_'+id).val();
            url = "<?php echo base_url().'administrator/pendaftaran/setVerifikasi/'?>";
            $.ajax({

                method: 'POST',
                url: url,
                data : 'status_verifikasi='+val,
                dataType: 'json',
                success : function(resp){

                    if(resp == "update_success"){

                        swal({

                          title: "Berhasil !",
                          text : "Aksi berhasil dilakukan",
                          type: "success",
                          timer: 800,
                          showConfirmButton : false

                        });
                        updateAllTable();
                    } 

                }

            });
        }

    //Pilihan Paket
        function filterFormPaket(){
          $('.btn_cari').hide();
            jenispaket_id = $('#jenis_paket').val();
            url = "<?php echo base_url().'administrator/pendaftaran/filterFormPaket/'?>"+jenispaket_id;
            $.ajax({

                method: 'POST',
                url: url,
                dataType: 'json',
                beforeSend: function() {$('.loading').show();$('.list_paket').remove();},
                success : function(resp){

                    $('.loading').hide();
                    $('.btn_cari').show();
                    jenis = resp['jenis'];
                    if(jenis=='Reguler'){

                        $('.paket-umroh-wisata-form').show('fast');
                        berangkat = resp['berangkat_awal'];
                        bulan = resp['bulan_berangkat'];
                        tahun = resp['tahun_berangkat'];
                        transit = resp['transit'];
                        maskapai = resp['maskapai'];
                        

                        $('#provinsi').empty();
                        $('#bulan').empty();
                        $('#tahun').empty();
                        $('#berangkat_transit').empty();
                        $('#maskapai').empty();


                        if(berangkat.length>0){

                            $('#provinsi').removeAttr('disabled');
                            $('#provinsi').append('<option value="Semua Keberangkatan">Semua Keberangkatan</option>');
                            for(var i=0;i<berangkat.length;i++){

                                $('#provinsi').append('<option value="'+berangkat[i]['kota_id']+'">'+berangkat[i]['provinsi']+'</option>');
                            }

                        }

                        if(bulan.length>0){

                            $('#bulan').removeAttr('disabled');
                            $('#bulan').append('<option value="Semua Bulan">Semua Bulan</option>');
                            for(var i=0;i<bulan.length;i++){

                                $('#bulan').append('<option value="'+bulan[i]['bulan_berangkat']+'">'+bulan[i]['bulan']+'</option>');
                            }

                        }

                        if(tahun.length>0){

                            $('#tahun').removeAttr('disabled');
                            $('#tahun').append('<option value="Semua Tahun">Semua Tahun</option>');
                            for(var i=0;i<tahun.length;i++){

                                $('#tahun').append('<option value="'+tahun[i]['tahun_berangkat']+'">'+tahun[i]['tahun_berangkat']+'</option>');
                            }

                        }

                        if(transit.length>0){

                            $('#berangkat_transit').removeAttr('disabled');
                            $('#berangkat_transit').append('<option value="Semua Transit">Semua Transit</option>');
                            for(var i=0;i<transit.length;i++){

                                $('#berangkat_transit').append('<option value="'+transit[i]['berangkat_transit']+'">'+transit[i]['berangkat_transit']+'</option>');
                            }

                        }

                        if(maskapai.length>0){
                            $('#maskapai').removeAttr('disabled');
                            $('#maskapai').append('<option value="Semua Maskapai">Semua Maskapai</option>');
                            for(var i=0;i<maskapai.length;i++){
                                $('#maskapai').append('<option value="'+maskapai[i]['maskapai_id']+'">'+maskapai[i]['nama_maskapai']+'</option>');
                            }
                        }
                    }

                    else if(jenis=='Haji'){

                        $('.paket-umroh-wisata-form').hide('fast');
                        haji = resp['haji'];
                        for(var i=0;i<haji.length;i++){

                            var x  = '  <div class="mt-radio-list list_paket haji_radio_btn" style="padding-bottom:0px">';
                                x += '    <label class="mt-radio mt-radio-outline " style="margin-bottom:0px">';
                                x += '          <input type="radio" class="paket_id" onChange="clear_reguler()"  name="paket_id[]" id="paket_id[]" value="'+haji[i]['paket_id']+'">&nbsp;<i class="icon-trophy"></i>&nbsp; '+haji[i]['nama_paket']+' ('+haji[i]['jenis_quota']+') <br> &nbsp;<i class="fa fa-refresh"></i>&nbsp; Masa TUnggu '+haji[i]['masa_tunggu']+' Tahun | <i class="fa fa-dollar"></i>&nbsp; '+haji[i]['harga_terakhir'] ;
                                x += '      <span></span>';
                                x += '    </label>';
                                x += '  </div>';
                            $('#hasil_pencarian').before(x);
                        }
                    }

                }

            });
        }

        function cek_paket(){

            jenispaket        = $('#jenis_paket').val();
            provinsi          = $('#provinsi').val();
            bulan             = $('#bulan').val();
            tahun             = $('#tahun').val();
            maskapai          = $('#maskapai').val();
            bintang           = $('#bintang').val();
            berangkat_transit = $('#berangkat_transit').val();


            if(!provinsi || !bulan || !tahun)

                return alert('Form pemilihan paket belum lengkap !');


            $.ajax({

                method: 'POST',
                data : {

                        provinsi : provinsi,
                        bulan : bulan,
                        tahun : tahun,
                        jenis_paket : jenispaket,
                        maskapai : maskapai,
                        bintang : bintang,
                        berangkat_transit : berangkat_transit 

                    },
                url: "<?php echo base_url().'administrator/pendaftaran/cekPaket/'?>",
                dataType: 'json',
                beforeSend: function() {

                    $('.loading').show();
                    $('.list_paket').remove();
                },
                success : function(resp){

                    $('.loading').hide();
                    if(resp.length>0){

                        temp = resp[0]['paketwaktu_id'];
                        show_tgl_keberangkatan = true;
                        var no =1;
                        for(var i=0;i<resp.length;i++){

                            

                            var x  = '  <div class="mt-radio-list list_paket" style="padding-bottom:0px">';
                            var y ='';
                                //Jika paket sudah diset tanggal keberangkatannya

                                    keberangkatan = resp[i]['keberangkatan'];
                                    if(keberangkatan){

                                        y+='<div style="margin-bottom:10px;margin-top:5px"><small>Tanggal Keberangkatan sudah tersedia : ';
                                        y+='    <select id="tgl_keberangkatan_'+resp[i]['paketwaktu_id']+'" class="tgl_keberangkatan tgl_keberangkatan_'+resp[i]['paketwaktu_id']+'" name="tgl_keberangkatan_'+resp[i]['paketwaktu_id']+'" onChange="clear_reguler('+resp[i]['paketwaktu_id']+')">';
                                        y+='        <option value=""></option>';


                                                    for(var j=0;j<keberangkatan.length;j++){

                                                        y+='<option value="'+keberangkatan[j]['keberangkatan_id']+'">'+keberangkatan[j]['tgl_keberangkatan']+'</option>';
                                                    }



                                        y+='    </select>';
                                        y+='</div></small></div>';
                                    }



                                list_header = '<div class="clearfix"><hr>'+no +'. <a style="text-decoration:none" onClick="detail_function(\'detail\','+resp[i]['paket_id']+');">'+resp[i]['nama_paket']+' | &nbsp;<i class="fa fa-plane"></i>&nbsp;'+resp[i]['berangkat_awal']+' | &nbsp;<i class="fa fa-calendar"></i>&nbsp;'+resp[i]['bulan_berangkat']+' '+resp[i]['tahun_berangkat']+'</a>'+y;
                                if(i>0){
                                    if(temp!=resp[i]['paketwaktu_id']){
                                        x += list_header;
                                        no++;
                                        show_tgl_keberangkatan = true;
                                    }
                                } else {
                                    x += list_header;
                                    no++;
                                }

                                harga     = resp[i]['harga'];
                                harga_usd = resp[i]['harga_usd'] ? ' ('+resp[i]['harga_usd']+')' : '';
                                jemaah    = (resp[i]['kelas']=='Quad')?'4':(resp[i]['kelas'] == 'Triple')?'3':'2';  
                                promo     = resp[i]['promo'] ? '&nbsp;&nbsp;<a class="btn btn-xs red">Promo !</a>' : '';

                                x += '    <label class="mt-radio mt-radio-outline" style="margin-bottom:0px">';
                                x += '          <input type="radio" class="pwk_id paketwaktu_id_'+resp[i]['paketwaktu_id']+'" onChange="clear_haji('+resp[i]['paketwaktu_id']+')" name="pwk_id[]" id="pwk_id[]" value="'+resp[i]['paketwaktu_id']+'_'+resp[i]['pwk_id']+'">'+resp[i]['kelas']+', '+harga+harga_usd+promo ;
                                x += '      <span></span>';
                                x += '    </label>';
                                x += '  </div>';
                            $('#hasil_pencarian').before(x);
                            temp = resp[i]['paketwaktu_id'];
                            show_tgl_keberangkatan = false;
                        }

                    }

                    else {

                        var x = '<hr><label class="alert alert-danger list_paket col-md-12">Oopss... Paket Tidak Ditemukan &nbsp;<i class="fa fa-times"></i></label>';
                        $('#hasil_pencarian').append(x);
                    }

                }

            });
        }         

    //Clear saat pilih paket
        function clear_reguler(paketwaktu_id=""){
            $('.pwk_id').removeAttr('checked');
            clear_other_tgl_berangkat(paketwaktu_id);
        }

        function clear_haji(paketwaktu_id=""){
            $('.paket_id').removeAttr('checked');
            clear_other_tgl_berangkat(paketwaktu_id);
        }

        function clear_other_tgl_berangkat(paketwaktu_id){

            <?php //clear all select option except current selected ?>
            $("select.tgl_keberangkatan").not($('.tgl_keberangkatan_'+paketwaktu_id)).each(function(){

                $(this).find('option').removeAttr("selected");
            }) 



            /*$(".duplicate_form_data_jemaah").empty();  



            remove_form();   */

        }  

    //Form List Jemaah

        function add_form(){

          var x='';
          for(var i=0;i<multiple_jemaah;i++){

              diff = count_jemaah++;
                  x+='<div class="col-md-12 div_form_data_jemaah_'+form_count+'">';
                  x+='    <h4 class="div_form_data_jemaah_'+form_count+'""><b>Data Jemaah '+form_count+'</b></h4>';
                  x+='    <div class="row form_data_jemaah" style="border-bottom:1px solid #333;padding-left:0px;padding-right:0px">';
                  x+='        <div class="col-md-3">';
                  x+='            <div class="form-group" >';
                  x+='                <small>Jenis Jemaah</small>';
                  x+='                <select class="form-control input-sm" name="jenis_jemaah[]"><option selected>Jemaah Biasa</option><option>Tour Leader</option></select>';
                  x+='            </div>';
                  x+='        </div>';
                  x+='        <div class="col-md-5">';
                  x+='            <div class="form-modal form-group ">';
                  x+='                <small for="form_control_1" class="bg-red-mint font-white">&nbsp;<span>*</span>Nama Jemaah '+diff+'&nbsp;</small>';
                  x+='                <input type="text" class="form-control input-sm " name="nama_lengkap[]" id="nama_lengkap" required>';
                  x+='            </div>';
                  x+='        </div>';


                  x+='        <div class="col-md-2">';
                  x+='            <div class="form-group">';
                  x+='                <small>No Identitas</small>';
                  x+='                <input class="form-control input-sm" type="text" name="no_ktp[]" id="no_ktp">';
                  x+='            </div>';
                  x+='        </div>';

                  x+='        <div class="col-md-2">';
                  x+='            <div class="form-group" >';
                  x+='                <small><span class="font-red">*</span>Gender</small>';
                  x+='                <select class="form-control input-sm" name="jenis_kelamin[]"><option></option><option>Pria</option><option>Wanita</option></select>';
                  x+='            </div>';
                  x+='        </div>';                        

                  x+='        <div class="col-md-3">';
                  x+='            <div class="form-group">';
                  x+='                <small><span class="font-red">*</span>Tempat Lahir</small>';
                  x+='                <input class="form-control input-sm" type="text" name="tempat_lahir[]" id="tempat_lahir" required>';
                  x+='            </div>';
                  x+='        </div>';

                  x+='        <div class="col-md-3">';
                  x+='            <div class="form-group">';
                  x+='                <small><span class="font-red">*</span>Tgl Lahir</small>';
                  x+='                <input class="form-control input-sm date-picker" type="text" name="tgl_lahir[]" id="tgl_lahir[]" placeholder="dd/mm/yyyy" autocomplete="off" required>';
                  x+='            </div>';
                  x+='        </div>';

                  x+='        <div class="col-md-6">';
                  x+='            <div class="form-modal form-group ">';
                  x+='                <small><span class="font-red">*</span>Alamat</small>';
                  x+='                <textarea type="text" rows="1" class="form-control" name="alamat[]" required></textarea>';
                  x+='            </div>';
                  x+='        </div>';  

                  x+='        <div class="col-md-3">';
                  x+='            <div class="form-modal form-group ">';
                  x+='                <small><span class="font-red">*</span>No HP 1</small>';
                  x+='                <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" class="form-control input-sm" name="no_hp[]" id="no_hp" required>';
                  x+='            </div>';
                  x+='        </div>';                                  

                  x+='        <div class="col-md-3">';
                  x+='            <div class="form-modal form-group ">';
                  x+='                <small>No HP 2</small>';
                  x+='                <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" class="form-control input-sm" name="no_hp_2[]" id="no_hp_2">';
                  x+='            </div>';
                  x+='        </div>';

                  x+='        <div class="col-md-3">';
                  x+='            <div class="form-modal form-group ">';
                  x+='                <small>Email</small>';
                  x+='                <input type="text"  class="form-control input-sm" name="email[]" id="email">';
                  x+='            </div>';
                  x+='        </div>';

                  x+='        <div class="col-md-3">';
                  x+='            <div class="form-group" >';
                  x+='                <small>Biaya Mahram ?</small>';
                  x+='                <select class="form-control input-sm" name="mahram[]"><option>Ya</option><option selected>Tidak</option></select>';
                  x+='            </div>';
                  x+='        </div>';                        


                  x+='        <div class="col-md-3">';
                  x+='            <div class="form-group" >';
                  x+='                <small>Progressive ?</small>';
                  x+='                <select class="form-control input-sm" name="progresive[]"><option>Ya</option><option selected>Tidak</option></select>';
                  x+='            </div>';
                  x+='        </div>';

                  x+='        <div class="col-md-3">';
                  x+='            <div class="form-group" >';
                  x+='                <small>Infant ?</small>';
                  x+='                <select class="form-control input-sm" name="infant[]"><option>Ya</option><option selected>Tidak</option></select>';
                  x+='            </div>';
                  x+='        </div>';

                  x+='        <div class="col-md-3">';
                  x+='            <div class="form-group" >';
                  x+='                <small>Sharing Bed</small>';
                  x+='                <select class="form-control input-sm" onChange="show_biaya_tambahan(this.value,'+form_count+',\'sharing_bed\')"><option>Ya</option><option selected>Tidak</option></select>';
                  x+='            </div>';
                  x+='            <div class="form-group" id="biaya_sharing_bed_'+form_count+'"></div>';
                  x+='        </div>';

                  x+='        <div class="col-md-3">';
                  x+='            <div class="form-group" >';
                  x+='                <small>Kelas Pesawat</small>';
                  x+='                <select class="form-control input-sm" name="kelas_pesawat[]" onChange="show_biaya_tambahan(this.value,'+form_count+',\'kelas_pesawat\')"><option selected>Economy Class</option><option>Business Class</option><option>First Class</option></select>';
                  x+='            </div>';
                  x+='            <div class="form-group" id="biaya_kelas_pesawat_'+form_count+'"></div>';                  
                  x+='        </div>';
                  
                  x+='        <div class="col-md-5">';
                  x+='            <div class="form-modal form-group ">';
                  x+='                <small>Riwayat Penyakit</small>';
                  x+='                <textarea type="text" rows="2" class="form-control" name="riwayat_penyakit[]" id="riwayat_penyakit" ></textarea>';
                  x+='            </div>';
                  x+='        </div>';

                  x+='        <div class="col-md-7">';
                  x+='            <div class="form-modal form-group ">';
                  x+='                <small>Catatan</small>';
                  x+='                <textarea type="text" rows="2" class="form-control" name="catatan[]" id="catatan" ></textarea>';
                  x+='            </div>';
                  x+='        </div>';



                  x+='        <div class="col-md-12 noPadding">';
                  x+='           <div class="col-md-6">';
                  x+='              <button type="button" class="btn btn-xs green-jungle pull-left" onClick="addFormBiaya('+form_count+',\'Tambahan\')"><i class="fa fa-plus"></i> Biaya Tambahan</button><div class="clearfix"></div><hr>';
                  x+='              <div id="list_biaya_tambahan_'+form_count+'"></div>';
                  x+='           </div>';
                  x+='           <div class="col-md-6">';
                  x+='              <button type="button" class="btn btn-xs red-flamingo pull-right" onClick="addFormBiaya('+form_count+',\'Pengurang\')"><i class="fa fa-plus"></i>  Biaya Pengurang</button><div class="clearfix"></div><hr>';
                  x+='              <div id="list_biaya_pengurang_'+form_count+'"></div>';
                  x+='           </div>';
                  x+='        </div>';
                  x+='    </div><div class="clearfix"></div><hr>';
                  x+='</div> ';
              form_count++;


          }

          $('#counter_jemaah').text((form_count-1));
          $(".duplicate_form_data_jemaah").prepend(x);
          $('.btn-hapus').show();
          $('.date-picker').datepicker({format: 'dd/mm/yyyy'});
        }

        function addFormBiaya(count,jenis){
          if(jenis=='Tambahan'){
            form_tambahan++;
            var x='   <div class="form_tambahan_'+count+'_'+form_tambahan+' margin-bottom-5">';
                x+='    <div class="col-md-7 noPaddingLeft"><input type="text" class="form-control input-sm" id="nama_biaya_tambahan_'+count+'_'+form_tambahan+'" name="nama_biaya_tambahan_'+count+'[]" placeholder="Biaya Tambahan" required></div>';
                x+='    <div class="col-md-5 noPaddingLeft noPaddingRight"><input type="text" id="biaya_tambahan_'+count+'_'+form_tambahan+'" onkeypress="return event.charCode >= 48 && event.charCode <= 57" class="form-control input-sm" placeholder="Jumlah" autocomplete="off" name="biaya_tambahan_'+count+'[]" required></div>';
                x+='    <label class="mt-checkbox mt-checkbox-outline"><input type="checkbox" onClick="cekKurs(\''+count+'_'+form_tambahan+'\',\'biaya_tambahan\')" id="biaya_tambahan_kurs_dollar_'+count+'_'+form_tambahan+'" name="biaya_tambahan_kurs_dollar_'+count+'[]"><small> Harga dalam dollar ($)</small><span></span></label>';
                x+='    <a class="label label-danger pull-right" onClick="removeFormBiaya(\''+count+'_'+form_tambahan+'\',\'Tambahan\')"><i class="fa fa-times"></i></a><div class="clearfix"></div>';
                x+='  </div>';
            $("#list_biaya_tambahan_"+count).prepend(x);
          } 
          else if(jenis=='Pengurang'){
            form_pengurang++;
            var x='   <div class="form_pengurang_'+count+'_'+form_pengurang+' margin-bottom-5">';
                x+='    <div class="col-md-7 noPaddingLeft"><input type="text" class="form-control input-sm" id="nama_biaya_pengurang_'+count+'_'+form_tambahan+'" name="nama_biaya_pengurang_'+count+'[]" placeholder="Pengurang Biaya" required></div>';
                x+='    <div class="col-md-5 noPaddingLeft noPaddingRight"><input type="text" id="biaya_pengurang_'+count+'_'+form_tambahan+'" onkeypress="return event.charCode >= 48 && event.charCode <= 57" class="form-control input-sm" placeholder="Jumlah" autocomplete="off" name="biaya_pengurang_'+count+'[]" required></div>';
                x+='    <label class="mt-checkbox mt-checkbox-outline"><input type="checkbox" onClick="cekKurs(\''+count+'_'+form_tambahan+'\',\'pengurang\')" id="pengurang_kurs_dollar_'+count+'_'+form_tambahan+'" name="pengurang_kurs_dollar_'+count+'[]"><small> Harga dalam dollar ($)</small><span></span></label>';                
                x+='    <a class="label label-danger pull-right" onClick="removeFormBiaya(\''+count+'_'+form_pengurang+'\',\'Pengurang\')"><i class="fa fa-times"></i></a><div class="clearfix"></div>';
                x+='  </div>';
            $("#list_biaya_pengurang_"+count).prepend(x);          
          }
        }

        function show_biaya_tambahan(stat,count,jenis,jumlah=""){
          if(jenis=='kelas_pesawat'){
            $('#biaya_kelas_pesawat_'+count).empty();
            if(stat=='Business Class' || stat=='First Class'){ 
              var x='<small><span class="font-red">*</span>Biaya Kelas Pesawat (Rp)</small>';
              if(count=='modal'){
                  x+='<input class="form-control input-sm input-small biaya_kelas_pesawat" type="text" name="biaya_kelas_pesawat" autocomplete="off" value="'+jumlah+'" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" onkeypress="return event.charCode >= 45 && event.charCode <= 57" required>';
              } else {
                  x+='<input class="form-control input-sm biaya_kelas_pesawat" type="text" name="biaya_kelas_pesawat[]" autocomplete="off" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" onkeypress="return event.charCode >= 45 && event.charCode <= 57" required>';
              }
              $('#biaya_kelas_pesawat_'+count).append(x); 
            }
          } else { 
            $('#biaya_sharing_bed_'+count).empty();
            if(stat=='Ya'){ 
              var x='<small><span class="font-red">*</span>Biaya Sharing Bed (Rp)</small>';
              if(count=='modal'){
                  x+='<input class="form-control input-sm input-small biaya_sharing_bed" type="text" name="biaya_sharing_bed" value="'+jumlah+'" autocomplete="off" onkeypress="return event.charCode >= 45 && event.charCode <= 57" required>';
              } else {
                  x+='<input class="form-control input-sm biaya_sharing_bed" type="text" name="biaya_sharing_bed[]" autocomplete="off" onkeypress="return event.charCode >= 45 && event.charCode <= 57" required>';
              }
              $('#biaya_sharing_bed_'+count).append(x); 
            }
          }
        }

        function removeFormBiaya(count,jenis){
        
          if(jenis=='Tambahan'){
            $(".form_tambahan_"+count).remove();
          } 
          else if(jenis=='Pengurang'){
            $(".form_pengurang_"+count).remove();   
          }
        }

        function remove_form(){
          for(var i=0;i<multiple_jemaah;i++){
              if(form_count>1){
                  form_count--;
                  count_jemaah--;
                  $(".div_form_data_jemaah_"+form_count).remove();
              }
          }

          $('#counter_jemaah').text((form_count-1));
          if(form_count==1)
              $('.btn-hapus').hide();
        }

        function cekKurs(count,jenis){
          if(!$('input[name=kurs_dollar]').val()){
            $('#'+jenis+'_kurs_dollar_'+count).prop('checked', false);
            return alert('Silahkan update kurs dollar hari ini terlebih dahulu !');
          } else {
            $('#kurs_dollar_hari_ini').val($('input[name=kurs_dollar]').val());
            $('#kurs_dollar_hari_ini_modal').val($('input[name=kurs_dollar]').val());
          }
        }

    //Toggle Form Marketing/Member

        function show_member_list(task=""){

            if(task=='ya_lama' || task=='ya'){
                $('#choose_member').removeAttr('disabled');
                $('#table_new_member').hide();
                $('.choose_member').show();
            } 

            else if (task=='ya_baru') {
                $('#choose_member').attr('disabled', 'disabled');
                $('#table_new_member').show();
                $('.choose_member').hide();
            }

            else if (task=='tidak')  {
                $('#choose_member').attr('disabled', 'disabled');
                $('#table_new_member').hide();
                $('.choose_member').hide();
            }
            empty_member_form();
        } 


        function empty_member_form(){
            $('#nama_member').val("");
            $('#tgl_lahir_member').val("");
            $('#alamat_member').val("");
            $('#email_member').val("");
        }    


    function validateAddForm(){
        //Cek Data Jemaah
            empty = 0;
            $('input', '#add-form-jemaah').each(function(){

                if($(this).val()=="" && $(this).attr('id')

                    //List yang tidak perlu di cek
                        && $(this).attr('id')!="checkbox_member[]"
                        && $(this).attr('id')!="pendaftarandetail_id"
                        && $(this).attr('id')!="pendaftaran_id"
                        && $(this).attr('id')!="no_ktp"
                        && $(this).attr('id')!="no_hp_2"
                        && $(this).attr('id')!="email"
                        && $(this).attr('id')!="kurs_dollar_hari_ini"
                        && $(this).attr('id')!="kurs_dollar_hari_ini_modal"

                    //Data Member baru akan dicek dibawah ...
                        && $(this).attr('id')!="nama_member"
                        && $(this).attr('id')!="tgl_lahir_member"
                        && $(this).attr('id')!="alamat_member"
                        && $(this).attr('id')!="email_member"
                        && $(this).attr('id')!="no_hp_member"
                ){
                  empty++;
              }
            });

            if(empty>0) return alert('Data Penting Jemaah tidak boleh kosong');
        //Cek Form Paket
            //Booking Paket Reguler
              pwk_id = 0;
               $('.pwk_id:checked').each(function() {
                  pwk_id = $(this).val();
               });
             //Booking Paket Haji
               paket_id =0;
               $('.paket_id:checked').each(function() {
                  paket_id = $(this).val();
               });
               if(!pwk_id && !paket_id) return alert('Silahkan pilih paket terlebih dahulu');

        //Cek Form Member jika daftar dengan member baru
            if($(".checkbox_member:checked").val()=='ya_baru'){
                if(!$('#nama_member').val() && !$('#tgl_lahir_member').val() && !$('#alamat_member').val() && !$('#email_member').val() && !$('#no_hp_member').val()){
                    return alert('Data Member baru tidak lengkap');
                }
            }

        //Cek Jemaah
            if(count_jemaah==1)
              return alert('Jemaah tidak boleh kosong !');

        //Semua aman ?
            return 'ok';
    }

    $(document).ready(function(){
        $("#add-form-jemaah").submit(function() {
          var x = validateAddForm();
          if (x=='ok') {
            return true;
          }
          else {
            if(x=='1' || x=='3' || x=='4')
                alert('Form Data Jemaah, Member dan Paket Jemaah belum lengkap');
            if(x=='2')
                alert('Kode Promo/Afiliasi tidak valid !');
            return false;
          }
        });

        table = $('#table_umroh').DataTable({ 
            "processing": true, "serverSide": true,"order": [],
            "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
            "ajax": {"url": "<?php echo site_url('administrator/pendaftaran/pagination/umroh')?>","type": "POST"},
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                        {"targets": [0],"className": "dt-center"},
                        {"targets": [4],"className": "dt-center"},
                        {"targets": [5],"className": "dt-center"},
                        {"targets": [6],"orderable": false }
                    ]
        }); 

       table2 = $('#table_haji').DataTable({ 
            "processing": true, "serverSide": true,"order": [],
            "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
            "ajax": {"url": "<?php echo site_url('administrator/pendaftaran/pagination/haji')?>","type": "POST"},
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                        {"targets": [0],"className": "dt-center",
                         "targets": [4],"className": "dt-center",
                         "targets": [5],"className": "dt-center"}
                    ]
        });

        table3 = $('#table_sudah_berangkat').DataTable({ 
            "processing": true, "serverSide": true,"order": [],
            "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
            "ajax": {"url": "<?php echo site_url('administrator/pendaftaran/pagination/sudah_berangkat')?>","type": "POST"},
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                        {"targets": [0],"className": "dt-center"},
                        {"targets": [4],"className": "dt-center"},
                        {"targets": [5],"className": "dt-center"},
                        {"targets": [6],"orderable": false }
                    ],
            "createdRow" : function(row,data,index){$(row).css("background-color","#F0FFF0");}
        });

        table4 = $('#table_cancel').DataTable({ 

            "processing": true, "serverSide": true,"order": [],
            "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
            "ajax": {"url": "<?php echo site_url('administrator/pendaftaran/pagination/cancel')?>","type": "POST"},
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                        {"targets": [0],"className": "dt-center"},
                        {"targets": [4],"className": "dt-center"},
                        {"targets": [5],"className": "dt-center"},
                        {"targets": [6],"orderable": false }
                    ],
            "createdRow" : function(row,data,index){$(row).css("background-color","#FFF1F1");}
        });

        table5 = $('#table_tidakValid').DataTable({ 

            "processing": true, "serverSide": true,"order": [],
            "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
            "ajax": {"url": "<?php echo site_url('administrator/pendaftaran/pagination/tidakValid')?>","type": "POST"},
            "order": [[ 0, "desc" ]],
            "columnDefs": [

                        {"targets": [0],"className": "dt-center"},
                        {"targets": [4],"className": "dt-center"},
                        {"targets": [5],"className": "dt-center"},
                        {"targets": [6],"orderable": false }

                    ],
            "createdRow" : function(row,data,index){$(row).css("background-color","#f1f1f2");}
        });
    });


</script>
<style>
    @page { size: auto;  margin: 0mm; }

    .form_data_jemaah{background: rgb(247, 247, 247);margin:10px 0px 0px 0px;padding:10px}
    .noPadding {padding:0px;}
    .noPaddingLeft {padding-left:0px;}
    .noPaddingRight {padding-right:0px;}
    .noMargin {margin:0px;}
    .noMargingLeft {margin-left:0px;}
    .noMarginRight {margin-right:0px;}    
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Jemaah

                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo base_url().'administrator' ?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Jemaah</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        

        <?php if($show_form){ ?>
            <div class="row">
                <?php if($this->session->flashdata('redirect_room')){ ?>
                    <div class="col-md-12 col-sm-12">
                        <div class="portlet light bordered">
                            <hr>
                            <div class="portlet-body">
                                <div class="col-md-12" style="text-align: center">
                                    <div class="row margin-bottom-40 about-header">
                                        <div class="col-md-12">
                                            <h1>Penambahan Jemaah Berhasil !</h1>
                                            <h3> Jemaah sudah ditetapkan tanggal keberangkatan. Insgin lanjutkan ke penentuan Room Jemaah ? </h3>
                                            <a href="<?php echo base_url().'administrator/pendaftaran/daftar' ?>" class="btn yellow-crusta"><i class="fa fa-arrow-circle-o-left"></i> Kembali</a>
                                            <a href="<?php echo base_url().'administrator/room/show_detail/'.$this->session->flashdata('redirect_room') ?>" class="btn btn-danger">Room Management <i class="fa fa-arrow-circle-o-right"></i> </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                            </div>
                        </div>
                    </div>
                
                <?php } else {?>
                    <div class="col-md-12 col-sm-12">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <a href="<?php echo base_url().'administrator/pendaftaran' ?>" class="btn btn-sm yellow-crusta pull-left"><i class="fa fa-arrow-circle-o-left"></i> Kembali</a>
                                <div class="clearfix"></div>
                                <div class="caption">
                                    <i class=" icon-users font-red"></i>
                                    <span class="header-modal caption-subject font-red sbold uppercase">Pendaftaran Jemaah</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <?php echo form_open('administrator/pendaftaran/add', array( 'id' => 'add-form-jemaah')); ?>
                                    <?php if($this->session->flashdata('add_success')){ ?>
                                        <div class="col-md-12">
                                            <div class="alert bg-green-jungle font-white"><i class="fa fa-check"></i>&nbsp;Jemaah berhasil didaftarkan</div>
                                        </div>
                                    <?php } ?>
                                    <div class="col-md-5">
                                        <h3 style="margin-top:0px">Pilihan Paket</h3>
                                        <div class="row form_data_jemaah">
                                            <button onClick="show_edit_paket()" type="button" class="btn btn-sm green-haze pull-right btn-show_edit_paket" style="display: none"><i class="fa fa-pencil"></i>&nbsp;Ganti Paket Jemaah</button>
                                            <button onClick="cancel_edit_paket()" type="button" class="btn btn-sm yellow-crusta pull-right btn-cancel_edit_paket" style="display: none"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                            

                                            <div class="col-md-6 form_paket">
                                                <div class="form-group">
                                                    <label for="single" class="control-label"><span class="font-red">*</span>Jenis Paket</label>
                                                    <div class="input-group select2-bootstrap-append">
                                                        <select class="form-control select2" id="jenis_paket" name="jenis_paket" onChange="filterFormPaket()" required>
                                                                <option value="" selected>Pilih Jenis Paket</option>
                                                                <option value="<?php echo encrypt("1") ?>">Umroh</option>
                                                                <option value="<?php echo encrypt("2") ?>">Haji</option>
                                                                <option value="<?php echo encrypt("3") ?>">Wisata Islam</option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="form-group paket-umroh-wisata-form display-none">
                                                    <label for="single" class="control-label">Berangkat Awal</label>
                                                    <div class="input-group select2-bootstrap-append">
                                                        <select class="form-control select2" id="provinsi" name="provinsi" disabled>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 form_paket paket-umroh-wisata-form display-none">
                                                <div class="form-group">
                                                    <label for="single" class="control-label">Bulan</label>
                                                    <div class="input-group select2-bootstrap-append">
                                                        <select class="form-control select2" id="bulan" name="bulan" disabled>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 form_paket paket-umroh-wisata-form display-none">
                                                <div class="form-group">
                                                    <label for="single" class="control-label">Tahun</label>
                                                    <div class="input-group select2-bootstrap-append">
                                                        <select class="form-control select2" id="tahun" name="tahun" disabled>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-md-12 paket-umroh-wisata-form display-none" >
                                                <a data-toggle="collapse" class="pull-right" href="#collapse_advanced_search">Show Advanced Search <i class="fa fa-arrow-circle-o-right"></i></a>
                                                <hr>
                                                <div id="collapse_advanced_search" class="panel-collapse collapse">
                                                    <div class="panel-body noPadding">
                                                        <div class="col-md-6 noPaddingLeft form_paket">
                                                            <div class="form-group">
                                                                <label for="single" class="control-label">Transit</label>
                                                                <div class="input-group select2-bootstrap-append">
                                                                    <select class="form-control select2" id="berangkat_transit" name="berangkat_transit" disabled>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 noPaddingLeft noPaddingRight form_paket ">
                                                            <div class="form-group">
                                                                <label for="single" class="control-label">Maskapai</label>
                                                                <div class="input-group select2-bootstrap-append">
                                                                    <select class="form-control select2" id="maskapai" name="maskapai" disabled>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 noPaddingLeft form_paket ">
                                                            <div class="form-group">
                                                                <label for="single" class="control-label">Bintang Hotel</label>
                                                                <div class="input-group select2-bootstrap-append">
                                                                    <select class="form-control select2" id="bintang" name="bintang">
                                                                        <option>Semua Bintang</option>
                                                                        <option value="1">Bintang 1</option>
                                                                        <option value="2">Bintang 2</option>
                                                                        <option value="3">Bintang 3</option>
                                                                        <option value="4">Bintang 4</option>
                                                                        <option value="5">Bintang 5</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>                      

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="loading pull-right display-none">
                                                <br>
                                                <img src="<?php echo base_url().'assets/frontend/img/AjaxLoader.gif' ?>" alt="">
                                                <span>Mohon Tunggu...</span>
                                            </div>
                                            <div class="col-md-12  form_paket paket-umroh-wisata-form display-none margin-bottom-10">
                                                <button onClick="cek_paket()" class="btn green-haze pull-left" type="button">Cari Paket &nbsp;<i class="fa fa-search"></i></button>
                                            </div>
                                            <div class="col-md-12">
                                              <div style="overflow-y: scroll;max-height: 680px;">
                                                <div  id="hasil_pencarian"></div>
                                              </div>
                                            </div>  
                                        </div> 
                                    </div> 

                                    <div class="col-md-7">
                                        <h3 style="margin-top:0px">Pendaftar RWH</h3>
                                        <div class="row form_data_jemaah" >
                                            <div class="col-md-12">
                                                <div class="form-group" style="margin-bottom: 0px">
                                                    <label><span class="font-red">*</span>Apakah Jemaah baru mendaftar melalui salah seorang Pendaftar RWH ?</label>
                                                    <div class="mt-radio-list">
                                                        <label class="mt-radio">
                                                            <input type="radio" onClick="show_member_list('ya_lama')" class="checkbox_member" name="checkbox_member[]" id="checkbox_member[]" value="ya_lama" required > Ya, Jemaah mendaftar melalui pendaftar yang tersedia

                                                            <div class=" select2-bootstrap-append display-none choose_member input-large">
                                                                <select class="form-control select2" id="choose_member" name="member_id" disabled>
                                                                        <option value="" selected></option>
                                                                    <?php $i=0; foreach($member as $mbr){ $i++;?>
                                                                        <option value="<?php echo encrypt($mbr->member_id) ?>"><?php echo $mbr->nama_lengkap .' <small>('.$mbr->no_hp.')</small>'?></option>
                                                                    <?php } ?> 

                                                                </select>
                                                                

                                                            </div>
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-radio">
                                                            <input type="radio" onClick="show_member_list('ya_baru')" class="checkbox_member" name="checkbox_member[]" id="checkbox_member[]" value="ya_baru" required> Ya, Jemaah akan didaftarkan atas pendaftar yang baru

                                                            <span></span>
                                                            <div>
                                                                <table id="table_new_member" class="table display-none">
                                                                    <tr>
                                                                        <td><i class="fa fa-user"></i> Nama Member</td>
                                                                        <td><input type="text" class="form-control input-medium input-sm" id="nama_member" name="nama_member"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><i class="fa fa-calendar"></i> Tanggal Lahir</td>
                                                                        <td><input type="text" class="form-control input-medium input-sm date-picker" id="tgl_lahir_member" name="tgl_lahir_member"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><i class="fa fa-home"></i> Alamat</td>
                                                                        <td><input type="text" class="form-control input-medium input-sm" id="alamat_member" name="alamat_member"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><i class="fa fa-envelope"></i> Email</td>
                                                                        <td><input type="text" class="form-control input-medium input-sm" id="email_member" name="email_member"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><i class="fa fa-phone"></i> Handphone</td>
                                                                        <td><input type="text" class="form-control input-medium input-sm" id="no_hp_member" name="no_hp_member"></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </label>
                                                        <label class="mt-radio">
                                                            <input type="radio" onClick="show_member_list('tidak')" class="checkbox_member" name="checkbox_member[]" id="checkbox_member[]" value="tidak" required> Tidak, Jemaah mendaftar secara mandiri

                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <h3>Jemaah RWH</h3>
                                          <div class="pull-left">
                                              <button type="button" class="btn btn-xs red" onclick="add_form()"><i class="fa fa-plus"></i> Tambah Jemaah</button>
                                              <button type="button" class="btn btn-xs display-none btn-hapus" onclick="remove_form()">Hapus Jemaah</button>
                                          </div>
                                        <div class="pull-right">
                                            <i>Total Jemaah : <span id="counter_jemaah">0 Jemaah</span></i>
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr>
                                          <div class="duplicate_form_data_jemaah"  style="overflow-y: scroll;max-height: 650px;"></div> 
                                    </div>
                                                                        
                                    <input type="hidden" name="pendaftaran_id" id="pendaftaran_id">
                                    <input type="hidden" name="pendaftarandetail_id" id="pendaftarandetail_id">
                                    <input type="hidden" name="kurs_dollar_hari_ini" id="kurs_dollar_hari_ini">
      
                                    <hr>
                                    <div class="col-md-12">
                                       
                                        <hr>
                                    </div> 

                                    <div class="col-md-12">
                                        <div class="pull-right">
                                            <a href="<?php echo base_url().'administrator/pendaftaran' ?>" class="btn grey">Cancel &nbsp;<i class="fa fa-times"></i></a>            

                                            <button type="submit" class="btn_add btn red">Simpan &nbsp;<i class="fa fa-save"></i></button>
                                        </div>
                                    </div>
                                <?php echo form_close();?>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                
                <?php } ?>
            </div>

        <?php } else if($show_detail){ ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a href="javascript:history.go(-1)" class="btn btn-sm yellow-crusta">Kembali <i class="fa fa-arrow-circle-o-left"></i></a>
                                </div>
                                <div class="btn-group">
                                    <a href="<?php echo base_url().'administrator/pendaftaran/detail_jemaah/'.encrypt($dt_jemaah[0]->pendaftarandetail_id).'/cetak' ?>" class="btn btn-sm green">Cetak <i class="fa fa-print"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    </div>
                    <?php 
                        foreach($dt_jemaah as $row)

                        $ya      = '<span class="font-green-jungle"><i class="fa fa-check"></i> Ya</span>';
                        $sudah   = '<span class="font-green-jungle"><i class="fa fa-check"></i> Sudah</span>';
                        $lengkap = '<span class="font-green-jungle"><i class="fa fa-check"></i> Lengkap</span>';
                        $tidak   = '<span class="font-red"><i class="fa fa-times"></i> Tidak</span>';
                        $belum   = '<span class="font-red"><i class="fa fa-times"></i> Belum</span>';

                        $infant                 = $row->infant ? $row->infant : 0;
                        $sharing_bed            = $row->sharing_bed ? $row->sharing_bed : 0;
                        $biaya_kelas_pesawat    = $row->biaya_kelas_pesawat ? $row->biaya_kelas_pesawat : 0;

                        //Booking Paket Haji
                        if($row->jenis_paket=='Haji'){
                            $nama_paket             = $row->nama_paket_haji;
                            $keberangkatan          =  "Estimasi Tahun ".$row->tahun_est_berangkat;
                            $jenis_quota            = $row->jenis_quota;
                            $tgl_berangkat          = '-';
                            $kamar                  = '-';
                            $kelas                  = '-';
                            $room                   = '-';
                            $status_keberangkatan   = '-';
                            $harus_bayar            = $row->harga_terakhir_usd ? '<i><small>(Rate '.number_format($row->harga_terakhir_usd_kurs).')</small></i> $'.number_format_decimal($row->harga_terakhir_usd).' | '.number_format_decimal($row->harga_terakhir) : number_format_decimal($row->harga_terakhir);
                            $harga_setelah_potongan = $harus_bayar ;

                        } 

                        //Booking Paket Reguler
                        else {
                            $tgl_berangkat          = $row->tgl_keberangkatan ? '<span class="font-green-jungle"><i class="fa fa-plane"></i>&nbsp;'.date('d-M-Y',strtotime($row->tgl_keberangkatan)).'</span>' : '<span class="font-red">Belum dijadwalkan </span>';
                            $nama_paket             = $row->nama_paket_reguler;
                            $kelas                  = roomTextColor($row->kelas);
                            $room                   = $row->room ? $row->room : $belum;
                            $keberangkatan          = $list_bulan[$row->bulan_berangkat].' '.$row->tahun_berangkat;
                            $harus_bayar            = $row->harus_bayar_usd ? '<i><small>(Rate '.number_format($row->kurs_dollar).')</small></i> $'.number_format_decimal($row->harus_bayar_usd).' | (+) '.number_format_decimal($row->harus_bayar) : number_format_decimal($row->harus_bayar);
                            $harga_setelah_potongan = $row->harus_bayar-($infant+$sharing_bed)+$biaya_kelas_pesawat;
                            $harga_setelah_potongan = ($harga_setelah_potongan + $row->total_biaya_tambahan) - $row->total_biaya_pengurang;

                            if($row->status_keberangkatan=='Dalam Proses')
                              $status_keberangkatan =  '<span class="font-red"><i class="fa fa-refresh"></i> Dalam Proses</span>';
                            else if($row->status_keberangkatan == 'Sudah Berangkat')
                              $status_keberangkatan = '<span class="font-green-jungle"><i class="fa fa-check"></i> Sudah Berangkat</span>';
                            else
                              $status_keberangkatan = '<span class="font-yellow-crusta"><i class="fa fa-times"></i> Cancel</span>';
                        }

                        $statusverif = $row->status_verifikasi == 'Valid' ? '<span class="font-green-jungle"><i class="fa fa-check"></i> Valid</span>' : '<span class="font-red"><i class="fa fa-refresh"></i> Menunggu</span>';



                    ?>
                    <div class="col-md-12 col-sm-12" style="padding-left: 0px">
                        <div class="col-md-6">
                            <div class="portlet light bordered">
                                <h2>Data Jemaah <?php if($row->jenis_jemaah=='Tour Leader'){ ?><b class="pull-right font-yellow" style="font-size: 22px"><i class="fa fa-trophy"></i> Tour Leader</b><?php } ?></h2>
                                <div class="row" >
                                    <div class="col-md-12">
                                        <table class="table">
                                            <tr>
                                                <td width="180">No. Registrasi</td>
                                                <td><?php echo $row->no_registrasi ?></td>
                                            </tr>
                                            <tr>
                                                <td width="180">Nama Jemaah</td>
                                                <td><?php echo $row->nama_jemaah ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tempat Tanggal Lahir</td>
                                                <td><?php echo $row->tempat_lahir.', '.date('d-F-Y',strtotime($row->tgl_lahir)) ?></td>
                                            </tr>
                                            <tr>
                                                <td>No Identitas</td>
                                                <td><?php echo $row->no_ktp ? $row->no_ktp : '-'; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Handphone</td>
                                                <td><?php echo $row->no_hp_2 ? $row->no_hp.' / '.$row->no_hp_2 : $row->no_hp; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td>
                                                <td><?php echo $row->alamat ? $row->alamat : '-' ?></td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td><?php echo $row->email ? $row->email : '-'  ?></td>
                                            </tr>
                                            <tr>
                                                <td>Riwayat Penyakit</td>
                                                <td><?php echo $row->riwayat_penyakit ? $row->riwayat_penyakit : '-'; ?></td>
                                            </tr>
                                        </table>
                                        <div class="col-md-6 noPaddingLeft">
                                          <table class="table">
                                              <tr>
                                                  <td width="180">Progressive</td>
                                                  <td><?php echo $row->progressive=='Ya' ? $ya: $tidak ?></td>
                                              </tr>
                                              <tr>
                                                  <td>Biaya Mahram</td>
                                                  <td><?php echo $row->ada_mahram=='Ya' ? $ya: $tidak ?></td>
                                              </tr>
                                              <tr>
                                                  <td>Infant</td>
                                                  <td><?php echo $row->infant ? $ya: $tidak ?></td>
                                              </tr>
                                          </table>
                                      </div>
                                      <div class="col-md-6 noPaddingRight">
                                          <table class="table">
                                              <tr>
                                                  <td width="180">Sharing Bed</td>
                                                  <td><?php echo $row->sharing_bed ?  $ya: $tidak ?></td>
                                              </tr>
                                              <tr>
                                                  <td>Kelas Pesawat</td>
                                                  <td><?php echo $row->kelas_pesawat ?></td>
                                              </tr>
                                              <tr>
                                                  <td>Status Verifikasi</td>
                                                  <td><?php echo $statusverif ?></td>
                                              </tr>
                                          </table>
                                      </div>
                                      <table class="table">
                                        <tr>
                                            <td width="180">Catatan</td>
                                            <td><?php echo $row->catatan ? $row->catatan :'-' ?></td>
                                        </tr>
                                      </table>
                                    </div>
                                </div>
                                <h2>Persyaratan Jemaah</h2>
                                <div class="row" >
                                    <div class="col-md-12">
                                        <table class="table">
                                            <th>No</th>
                                            <th>Syarat</th>
                                            <th>Status</th>
                                            <th>Tanggal Penyerahan</th>
                                            <?php 

                                                $count=0;;
                                                foreach($dt_syarat as $row2){
                                            ?>
                                            <tr>
                                                <td><?php echo ++$count ?>.</td>
                                                <td><?php echo $row2->persyaratan ?></td>
                                                <td><?php echo $row2->status_syarat == 'Lengkap' ? $lengkap : $belum;?></td>
                                                <td><?php echo $row2->tgl_penyerahan ? date('d-F-Y',strtotime($row2->tgl_penyerahan)) : '-'?></td>
                                            </tr>
                                               

                                            <?php } ?>
                                        </table>
                                    </div>
                                </div>
                                <h2>Perlengkapan Jemaah</h2>
                                <div class="row" >
                                    <div class="col-md-12">
                                        <?php if($dt_perlengkapan){ ?>
                                            <table class="table">
                                                <th>No</th>
                                                <th>Nama Barang</th>
                                                <th>Qty</th>
                                                <th>Tanggal Penyerahan</th>
                                                <th>Lokasi Penyerahan</th>
                                                <?php 

                                                    $count=0;
                                                    foreach($dt_perlengkapan as $row3){

                                                ?>
                                                <tr>
                                                    <td><?php echo ++$count?>.</td>
                                                    <td><?php echo $row3->nama_barang ?></td>
                                                    <td><?php echo $row3->qty?></td>
                                                    <td><?php echo date('d-F-Y',strtotime($row3->tgl_penyerahan))?></td>
                                                    <td><?php echo $row3->nama_daerah?></td>
                                                </tr>
                                                   

                                                <?php } ?>
                                            </table>
                                        <?php } else { ?>
                                            <div class="alert alert-danger"><i class="fa fa-times"></i> Belum ada penyerahan perlengkapan</div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portlet light bordered">
                                <h2>Pendaftaran Paket</h2>
                                <div class="row" >
                                    <div class="col-md-12">
                                        <table class="table">
                                            <tr>
                                                <td width="180">Tanggal Daftar</td>
                                                <td><?php echo date('d-F-Y H:i',strtotime($row->tgl_daftar)); ?></td>
                                            </tr>
                                            <tr>
                                                <td>Atas Member</td>
                                                <td><?php echo $row->nama_member ?></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Paket</td>
                                                <td><?php echo $nama_paket ?></td>
                                            </tr>
                                            <tr>
                                                <td>Kelas</td>
                                                <td><?php echo $kelas; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Harga Paket</td>
                                                <td><?php echo $harus_bayar; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Harus Bayar</td>
                                                <td>
                                                    <?php 

                                                    if($row->jenis_paket == 'Haji'){
                                                      echo $harus_bayar.' <small><a  data-toggle="collapse" href="#collapse2">(Lihat Selengkapnya)</a></small>';
                                                      echo '<div id="collapse2" class="panel-collapse collapse">';
                                                    }
                                                    else {
                                                      echo 'Rp. '. number_format_decimal($harga_setelah_potongan).' <small><a  data-toggle="collapse" href="#collapse2">(Lihat Selengkapnya)</a></small>';
                                                      echo '<div id="collapse2" class="panel-collapse collapse">';
                                                      echo '<small>Harga Awal Paket  </small><small class="pull-right">'.$harus_bayar.'</small><br>';
                                                      
                                                      if($infant || $sharing_bed || $biaya_kelas_pesawat){
                                                        
                                                        
                                                        echo $infant ? '<small>Jemaah Infant</small><small class="pull-right">(-) '.number_format_decimal($infant).'</small><br>':'';
                                                        echo $sharing_bed ? '<small>Jemaah Sharing Bed  </small><small class="pull-right">(-) '.number_format_decimal($sharing_bed).'</small><br>':'';
                                                        echo $biaya_kelas_pesawat ? '<small>Biaya '.$row->kelas_pesawat.'  </small><small class="pull-right">(+) '.number_format_decimal($biaya_kelas_pesawat).'</small><br>':'';
                                                      }

                                                      
                                                    }
                                                        $biaya_tambahan = $this->md_pendaftaran_detail_biaya->getPendaftaranDetailBiayaByPendaftaranDetailIdByJenis($row->pendaftarandetail_id,'Tambahan');
                                                        if($biaya_tambahan){
                                                          foreach($biaya_tambahan as $bt){
                                                            $text = '';
                                                            if($bt->jumlah_usd>0)
                                                              $text = '<i><small>(Rate '.number_format($bt->kurs_dollar).')</small></i> $'.number_format_decimal($bt->jumlah_usd).' | ';

                                                            echo '<small>'.$bt->biaya.'</small><small class="pull-right">'.$text.'(+) '.number_format_decimal($bt->jumlah).'</small><br>';
                                                          }
                                                        }

                                                        $biaya_pengurang = $this->md_pendaftaran_detail_biaya->getPendaftaranDetailBiayaByPendaftaranDetailIdByJenis($row->pendaftarandetail_id,'Pengurang');
                                                        if($biaya_pengurang){
                                                          foreach($biaya_pengurang as $bp){
                                                            $text = '';
                                                            if($bp->jumlah_usd>0)
                                                              $text = '<i><small>(Rate '.number_format($bt->kurs_dollar).')</small></i> $'.number_format_decimal($bt->jumlah_usd).' | ';
                                                            echo '<small>'.$bp->biaya.'</small><small class="pull-right">'.$text.'(-) '.number_format_decimal($bp->jumlah).'</small><br>';
                                                          }
                                                        }
                                                        //
                                                        if($infant || $sharing_bed || $biaya_kelas_pesawat || $biaya_tambahan || $biaya_pengurang ){
                                                              echo '<b><small>Total </small><small class="pull-right">Rp. '.number_format_decimal($harga_setelah_potongan).'</small></b>';
                                                        }

                                                    echo '</div>';

                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <?php if($row->jenis_paket == 'Umroh'){ ?>
                                                    <td for="form_control_1">Berangkat Dari</td>
                                                    <td><?php echo $row->kota_keberangkatan ?></td>
                                                <?php } else { ?>
                                                    <td for="form_control_1">Jenis Quota</td>
                                                    <td><?php echo $jenis_quota ?></td>
                                                <?php } ?>  

                                            </tr>
                                            <tr>
                                                <td>Keberangkatan</td>
                                                <td><?php echo $keberangkatan ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Keberangkatan</td>
                                                <td><?php echo $tgl_berangkat ?></td>
                                            </tr>
                                            <tr>
                                                <td>Status Keberangkatan</td>
                                                <td><?php echo $status_keberangkatan ?></td>
                                            </tr>
                                            <tr>
                                                <td>Penempatan Room</td>
                                                <td><?php echo $room ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <h2>Pembayaran Jemaah</h2>
                                <div class="row" style="margin:10px;">
                                    <?php 

                                      if($dt_pembayaran){ ?>
                                          <table class="table">
                                              <th>Tanggal Transaksi</th>
                                              <th>Jenis</th>
                                              <th>Status</th>
                                              <th>Nominal</th>
                                               <?php 



                                                      $count=0;
                                                      $total_bayar = 0;
                                                      $total_pengembalian = 0;
                                                      foreach($dt_pembayaran as $row){

                                                          if($row->jenis_transaksi=='Pembayaran'){
                                                              $total_bayar += $row->jumlah_bayar;
                                                              $style = '';
                                                              $status_p = $row->status_pembayaran;
                                                          } else if($row->jenis_transaksi=='Pengembalian') {
                                                              $total_pengembalian += $row->jumlah_bayar;
                                                              $style = '#FFF1F1';
                                                              $status_p = $row->jenis_pengembalian;
                                                          }

                                                          if($row->status_verifikasi=='Accept')

                                                              $status_bayar ='<span class="font-green-jungle"><i class="fa fa-check"></i></span>';
                                                          else if($row->status_verifikasi=='Decline')

                                                              $status_bayar ='<span class="font-red"><i class="fa fa-times"></i></span>';
                                                          else

                                                              $status_bayar ='Dalam Proses';
                                              ?>
                                                      <tr style="background-color: <?php echo $style ?>">
                                                          <td><?php echo date('d-F-Y',strtotime($row->tgl_bayar))?></td>
                                                          <td><?php echo $row->jenis_transaksi?></td>
                                                          <td><?php echo $status_p?></td>
                                                          <td>Rp. <span class="pull-right"><?php echo number_format_decimal($row->jumlah_bayar) ?></span></td>
                                                      </tr>
                                              <?php   } ?> 

                                          </table>
                                          <hr>
                                          <div class="col-md-6 noPadding pull-right">
                                              <h5>Total Pembayaran : <b class="pull-right"><?php echo number_format_decimal($total_bayar); ?></b></h5>
                                              <h5>Harus Bayar : <b class="pull-right"><?php echo number_format_decimal($harga_setelah_potongan) ?></b></h5>
                                              <h5>Sisa Bayar <b class="pull-right"><?php echo number_format_decimal($harga_setelah_potongan - $total_bayar)?></b></h5>
                                              <h5>Total Pengembalian : <b class="pull-right"><?php echo $total_pengembalian ? number_format_decimal($total_pengembalian) : '-'; ?></b></h5>
                                          </div>
                                    <?php   

                                        } else if($row->jenis_jemaah=='Tour Leader'){echo '<div class="alert alert-info"><i class="fa fa-exclamation"></i>&nbsp; Pembayaran tidak tersedia untuk Tour Leader</div>';} else { ?>
                                            <div class="alert alert-danger"><i class="fa fa-times"></i>&nbsp; Jemaah Belum Melakukan Pembayaran</div>
                                    <?php } ?> 

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php } else {?>
            <div class="row">
                    <div class="col-md-12">
                        <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <!-- <a onClick="add_function('show')" class="btn btn-sm red">Pendaftaran Jemaah <i class="fa fa-plus"></i></a> -->
                                    <a href="<?php echo base_url().'administrator/pendaftaran/daftar' ?>" class="btn btn-sm red">Pendaftaran Jemaah <i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject bold uppercase"> Managed Pendaftaran Jemaah</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_1_1" data-toggle="tab"> <i class="icon-reload"></i> On Process </a>
                                    </li>
                                    <li>
                                        <a href="#tab_1_2" data-toggle="tab"> <i class="fa fa-paper-plane-o"></i> Sudah Berangkat </a>
                                    </li>
                                    <li>
                                        <a href="#tab_1_3" data-toggle="tab"> <i class="icon-ban"></i> Cancel </a>
                                    </li>
                                    <li>
                                        <a href="#tab_1_4" data-toggle="tab"> <i class="fa fa-times"></i> Tidak Valid </a>
                                    </li>
                                    <li>
                                        <a href="#tab_1_5" data-toggle="tab"> <i class="icon-star"></i> Jemaah Haji </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab_1_1">
                                        <table class="table table-bordered table-hover" id="table_umroh">
                                            <thead>
                                                <tr>
                                                    <th> NO </th>
                                                    <th> Nama Jemaah</th>
                                                    <th> Paket </th>
                                                    <th> Kota Berangkat </th>
                                                    <th> Tgl Berangkat </th>
                                                    <th> Petugas </th>
                                                    <th> Verifikasi </th>
                                                    <th> Aksi </th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="tab_1_2">
                                        <table class="table table-bordered" id="table_sudah_berangkat">
                                            <thead>
                                                <tr>
                                                    <th> NO </th>
                                                    <th> Nama Jemaah</th>
                                                    <th> Paket </th>
                                                    <th> Kota Berangkat </th>
                                                    <th> Tgl Berangkat </th>
                                                    <th> Petugas </th>
                                                    <th> Verifikasi </th>
                                                    <th> Aksi </th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>     

                                    <div class="tab-pane fade" id="tab_1_3">
                                        <table class="table table-bordered" id="table_cancel">
                                            <thead>
                                                <tr>
                                                    <th> NO </th>
                                                    <th> Nama Jemaah</th>
                                                    <th> Paket </th>
                                                    <th> Kota Berangkat </th>
                                                    <th> Tgl Berangkat </th>
                                                    <th> Petugas </th>
                                                    <th> Verifikasi </th>
                                                    <th> Aksi </th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>    

                                    <div class="tab-pane fade" id="tab_1_4">
                                        <table class="table table-bordered" id="table_tidakValid">
                                            <thead>
                                                <tr>
                                                    <th> NO </th>
                                                    <th> Nama Jemaah</th>
                                                    <th> Paket </th>
                                                    <th> Kota Berangkat </th>
                                                    <th> Tgl Berangkat </th>
                                                    <th> Petugas </th>
                                                    <th> Verifikasi </th>
                                                    <th> Aksi </th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>                                                                                                       

                                    <div class="tab-pane fade" id="tab_1_5">
                                        <table class="table table-bordered" id="table_haji">
                                            <thead>
                                                <tr>
                                                    <th> NO </th>
                                                    <th> Nama Jemaah</th>
                                                    <th> Paket </th>
                                                    <th> Kota Berangkat </th>
                                                    <th> Tahun Est. Berangkat </th>
                                                    <th> Petugas </th>
                                                    <th> Verifikasi </th>
                                                    <th> Aksi </th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>   

                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
            </div>
       
        <?php } ?>


    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->


<div class="modal fade" id="add_edit_modal" role="basic" aria-hidden="true">
    <div class="modal-dialog"  style="width:780px;">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-users font-red"></i>
                    <span class="header-modal caption-subject font-red sbold uppercase">Tambah Pendaftaran</span>
                </div>
            </div>
            <div class="portlet-body">
              <div class="loading pull-left" style="display: none">
                  <img src="<?php echo base_url().'assets/frontend/img/AjaxLoader.gif' ?>" alt="">
                  <span>Mohon Tunggu...</span>
              </div>
                <?php echo form_open('administrator', array( 'id' => 'add-form-jemaah')); ?>
                        <h3 style="margin: 0px">Data Jemaah</h3>
                        <div class="row" style="background: rgb(247, 247, 247);margin:10px;padding:5px">
                          <div class="col-md-3">
                                <div class="form-group" >
                                    <small>Jenis Jemaah</small>
                                    <select class="form-control input-sm input-sm" id="jenis_jemaah" name="jenis_jemaah"><option value="Jemaah Biasa">Jemaah Biasa</option><option value="Tour Leader">Tour Leader</option></select>
                                </div>
                            </div>   
                            <div class="col-md-4">
                                <div class="form-modal form-group ">
                                    <small><span class="font-red">*</span>Nama</small>
                                    <input type="text" class="form-control input-sm" name="nama_lengkap" id="nama_lengkap">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <small>Nomor Identitas</small>
                                    <input class="form-control input-sm" type="text" name="no_ktp" id="no_ktp">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group" >
                                    <small><span class="font-red">*</span>Jenis Kelamin</small>
                                    <select class="form-control input-sm input-sm" id="jenis_kelamin" name="jenis_kelamin"><option value="Pria">Pria</option><option value="Wanita">Wanita</option></select>
                                </div>
                            </div>                    
                            <div class="col-md-3">
                                <div class="form-group">
                                    <small><span class="font-red">*</span>Tempat Lahir</small>
                                    <input class="form-control input-sm" type="text" name="tempat_lahir" id="tempat_lahir">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <small><span class="font-red">*</span>Tanggal Lahir</small>
                                    <input class="form-control tgl_lahir input-sm" type="text" name="tgl_lahir" id="tgl_lahir" autocomplete="off">
                                </div>
                            </div>   
                            <div class="col-md-3">
                                <div class="form-modal form-group ">
                                    <small><span class="font-red">*</span>Handphone</small>
                                    <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control input-sm" name="no_hp" id="no_hp">
                                </div>
                            </div>    
                            <div class="col-md-3">
                                <div class="form-modal form-group ">
                                    <small>Handphone 2</small>
                                    <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control input-sm" name="no_hp_2" id="no_hp_2">
                                </div>
                            </div> 
                            <div class="col-md-6">
                                <div class="form-modal form-group ">
                                    <small><span class="font-red">*</span>Alamat</small>
                                    <input class="form-control input-sm" type="text" name="alamat" id="alamat">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-modal form-group ">
                                    <small>Email</small>
                                    <input type="text"  class="form-control input-sm" name="email" id="email" >
                                </div>
                            </div>
                            <div class="col-md-3">
                               <div class="form-group" >
                                    <small>Biaya Mahram ?</small>
                                    <select class="form-control input-sm" name="mahram"><option value="Ya">Ya</option><option value="Tidak">Tidak</option></select>
                                </div>
                            </div>      
                            <div class="col-md-3">
                               <div class="form-group" >
                                    <small>Progressiv</small>
                                    <select class="form-control input-sm" name="progressive"><option value="Ya">Ya</option><option value="Tidak">Tidak</option></select>
                                </div>
                            </div>  
                            <div class="col-md-3">
                               <div class="form-group" >
                                    <small>Infant</small>
                                    <select class="form-control input-sm" name="infant"><option value="Ya">Ya</option><option value="Tidak">Tidak</option></select>
                                </div>
                            </div>
                            <div class="col-md-3">
                               <div class="form-group" >
                                    <small>Sharing Bed</small>
                                    <select class="form-control input-sm" onChange="show_biaya_tambahan(this.value,'modal','sharing_bed')" name="sharing_bed"><option value="Ya">Ya</option><option value="Tidak">Tidak</option></select>
                                </div>
                            </div>
                            <div class="col-md-3">
                               <div class="form-group" >
                                    <small>Kelas Pesawat</small>
                                    <select class="form-control input-sm" onChange="show_biaya_tambahan(this.value,'modal','kelas_pesawat')" name="kelas_pesawat"><option value="Economy Class">Economy Class</option><option value="Business Class">Business Class</option><option value="First Class">First Class</option></select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-modal form-group ">
                                    <small>Riwayat Penyakit</small>
                                    <textarea type="text" rows="1" class="form-control input-sm" name="riwayat_penyakit" id="riwayat_penyakit" ></textarea>
                                </div>
                            </div>
                            <div class="col-md-3">
                              <div class="form-group" id="biaya_sharing_bed_modal">
                                  
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="form-group" id="biaya_kelas_pesawat_modal">
                                  
                              </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-modal form-group ">
                                    <small>Catatan</small>
                                    <textarea type="text" rows="2" class="form-control input-sm" name="catatan" id="catatan" ></textarea>
                                </div>
                            </div>
                            <div class="col-md-12 noPadding">
                               <div class="col-md-6" style="border-right: 1px #d9d9d9 solid">
                                  <button type="button" class="btn btn-xs green-jungle pull-left" onClick="addFormBiaya(0,'Tambahan')"><i class="fa fa-plus"></i> Biaya Tambahan</button><div class="clearfix"></div>
                                  <div id="list_biaya_tambahan_0" style="padding-top: 10px"></div>
                               </div>
                               <div class="col-md-6">
                                  <button type="button" class="btn btn-xs red-flamingo pull-right" onClick="addFormBiaya(0,'Pengurang')"><i class="fa fa-plus"></i>  Biaya Pengurang</button><div class="clearfix"></div>                                  <div id="list_biaya_pengurang_0" style="padding-top: 10px"></div>
                               </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <i style="font-size: 12px"><span class="font-red">*</span> Data wajib diisi</i>
                            <hr>
                        </div> 

                        <h3>Pendaftar RWH</h3>
                        <div class="row" style="background: rgb(247, 247, 247);margin:10px;padding:10px">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><span class="font-red">*</span>Apakah Jemaah baru mendaftar melalui salah seorang Pendaftar RWH ?</small>
                                    <div class="mt-radio-inline">
                                        <label class="mt-radio">
                                            <input type="radio" onClick="show_member_list('ya')" class="checkbox_member" name="checkbox_member[]" id="checkbox_member[]" value="ya" > Ya, Jemaah mendaftar melalui :

                                            <div class="input-group select2-bootstrap-append">
                                                <select class="form-control select2" id="choose_member" name="member_id" disabled>
                                                        <option value="" selected></option>
                                                    <?php $i=0; foreach($member as $mbr){ $i++;?>
                                                        <option value="<?php echo encrypt($mbr->member_id) ?>"><?php echo $mbr->nama_lengkap.' ('.$mbr->no_hp.')'?></option>
                                                    <?php } ?> 

                                                </select>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button" data-select2-open="multi-append">
                                                        <span class="glyphicon glyphicon-search"></span>
                                                    </button>
                                                </span>
                                            </div>
                                            <span></span>
                                        </label>
                                        <label class="mt-radio">
                                            <input type="radio" onClick="show_member_list('tidak')" class="checkbox_member" name="checkbox_member[]" id="checkbox_member[]" value="tidak"> Tidak, Jemaah mendaftar secara mandiri
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h3>Pilihan Paket</h3>
                        <div class="row " style="background: rgb(247, 247, 247);margin:10px;padding:10px">
                            <div id="pilihan_paket"></div>
                            <div class="clearfix"></div>
                            <div class="pull-left">
                                <span>Berangkat dari : </span> 

                                <select id="kota_keberangkatan" name="kota_keberangkatan">
                                  <?php 

                                    foreach($kota['data'] as $kt){

                                      echo '<option value="'.encrypt($kt->kota_id).'">'.$kt->nama_kota.'</option>';
                                    }

                                  ?>
                                </select>
                            </div>
                            <button onClick="show_edit_paket()" type="button" class="btn btn-xs green-haze pull-right btn-show_edit_paket"><i class="fa fa-pencil"></i>&nbsp;Ganti Paket Jemaah</button>
                            <button onClick="cancel_edit_paket()" type="button" class="btn btn-xs yellow-crusta pull-right btn-cancel_edit_paket"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                            <div class="clearfix"></div>
                            <hr>
                            <div class="col-md-6 form_paket">
                                <div class="form-group">
                                    <label for="single" class="control-label"><span class="font-red">*</span>Jenis Paket</small>
                                    <div class="input-group select2-bootstrap-append input-medium">
                                        <select class="form-control select2" id="jenis_paket" name="jenis_paket" onChange="filterFormPaket()">
                                                <option value="" selected></option>
                                                <option value="<?php echo encrypt("1") ?>">Umroh</option>
                                                <option value="<?php echo encrypt("2") ?>">Haji</option>
                                                <option value="<?php echo encrypt("3") ?>">Wisata Islam</option>
                                        </select>
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button" data-select2-open="multi-append">
                                                <span class="glyphicon glyphicon-search"></span>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group paket-umroh-wisata-form">
                                    <label for="single" class="control-label">Berangkat Awal</small>
                                    <div class="input-group select2-bootstrap-append">
                                        <select class="form-control select2" id="provinsi" name="provinsi" disabled>
                                        </select>
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button" data-select2-open="multi-append">
                                                <span class="glyphicon glyphicon-search"></span>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 form_paket paket-umroh-wisata-form">
                                <div class="form-group">
                                    <label for="single" class="control-label">Bulan</small>
                                    <div class="input-group select2-bootstrap-append">
                                        <select class="form-control select2" id="bulan" name="bulan" disabled>
                                        </select>
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button" data-select2-open="multi-append">
                                                <span class="glyphicon glyphicon-search"></span>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 form_paket paket-umroh-wisata-form">
                                <div class="form-group">
                                    <label for="single" class="control-label">Tahun</small>
                                    <div class="input-group select2-bootstrap-append">
                                        <select class="form-control select2" id="tahun" name="tahun" disabled>
                                        </select>
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button" data-select2-open="multi-append">
                                                <span class="glyphicon glyphicon-search"></span>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 form_paket paket-umroh-wisata-form" >
                                <a data-toggle="collapse" href="#collapse_advanced_search">Show Advanced Search <i class="fa fa-arrow-circle-o-right"></i></a>
                                <hr>
                                <div id="collapse_advanced_search" class="panel-collapse collapse">
                                    <div class="panel-body noPadding">
                                        <div class="col-md-6 noPaddingLeft form_paket">
                                            <div class="form-group">
                                                <label for="single" class="control-label">Transit</small>
                                                <div class="input-group select2-bootstrap-append">
                                                    <select class="form-control select2" id="berangkat_transit" name="berangkat_transit" disabled>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 noPaddingLeft noPaddingRight form_paket ">
                                            <div class="form-group">
                                                <label for="single" class="control-label">Maskapai</small>
                                                <div class="input-group select2-bootstrap-append">
                                                    <select class="form-control select2" id="maskapai" name="maskapai" disabled>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 noPaddingLeft form_paket ">
                                            <div class="form-group">
                                                <label for="single" class="control-label">Bintang Hotel</small>
                                                <div class="input-group select2-bootstrap-append">
                                                    <select class="form-control select2" id="bintang" name="bintang">
                                                        <option>Semua Bintang</option>
                                                        <option value="1">Bintang 1</option>
                                                        <option value="2">Bintang 2</option>
                                                        <option value="3">Bintang 3</option>
                                                        <option value="4">Bintang 4</option>
                                                        <option value="5">Bintang 5</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>                      

                                    </div>
                                </div>
                            </div>
                                                       

                            <div class="loading pull-left" style="display: none">
                                <img src="<?php echo base_url().'assets/frontend/img/AjaxLoader.gif' ?>" alt="">
                                <span>Mohon Tunggu...</span>
                            </div>
                            <div class="col-md-12 form_paket paket-umroh-wisata-form">
                                <a onClick="cek_paket()" class="btn btn_cari green-haze pull-right">Cari Paket &nbsp;<i class="fa fa-search"></i></a>
                            </div>
                            <div class="clearfix"></div>
                            

                            <div class="col-md-12">
                                <span id="hasil_pencarian"></span>
                            </div>  
                        </div>                                          

                        <input type="hidden" name="pendaftaran_id" id="pendaftaran_id">
                        <input type="hidden" name="pendaftarandetail_id" id="pendaftarandetail_id">
                        <input type="hidden" name="harus_bayar"id="harus_bayar">
                        <input type="hidden" name="kurs_dollar_hari_ini_modal" id="kurs_dollar_hari_ini_modal">
                <?php echo form_close();?>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn grey">Cancel &nbsp;<i class="fa fa-times"></i></button>            

                <button type="button" onClick="add_function('add')" class="btn_add btn red hide">Simpan &nbsp;<i class="fa fa-save"></i></button>
                <button type="button" onClick="edit_function('update')" class="btn_update btn yellow-crusta hide">Simpan Perubahan &nbsp;<i class="fa fa-save"></i></button>
            </div>
        </div>


    </div>
</div>




<!-- detail Modal-->
<div class="modal fade" id="detail_modal" role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog"  style="width:750px">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-search"></i>
                    <span class="header-modal caption-subject sbold uppercase">Detail Paket</span>
                </div>
            </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-4 col-sm-3">
                            <h4>Web Picture</h4>
                            <div class="fileinput-new thumbnail">
                                <a hreg="" id="detail_avatar_href" target="_blank"><img id="detail_avatar" src="<?php echo base_url().'assets/frontend/img/no-avatar.png' ?>" alt=""> </a>
                            </div> 

                            <h4>Mobile Picture</h4>
                            <div class="fileinput-new thumbnail">
                                <a hreg="" id="detail_avatar_mobile_href" target="_blank"><img id="detail_avatar_mobile" src="<?php echo base_url().'assets/frontend/img/no-avatar.png' ?>" alt=""></a> 

                            </div>
                        </div>
                        <div class="col-md-8 reguler">
                            <h3><i class="fa fa-tasks"></i>&nbsp;<span id="detail_nama_paket"></span></h3>
                            <div class="col-md-12">
                                <table class="table">
                                    <tr>
                                        <td style="width:150px"><i class="icon-briefcase"></i>&nbsp; <span>Jenis Paket</td>
                                        <td>:</td>
                                        <td><span id="detail_jenis_paket"></span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="icon-trophy"></i>&nbsp; <span>Lama Perjalanan</td>
                                        <td>:</td>
                                        <td><span id="detail_lama_hari"></span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="icon-plane"></i>&nbsp; <span>Maskapai</td>
                                        <td>:</td>
                                        <td><div class="col-md-12>"><span id="detail_maskapai"></span></div></td>
                                    </tr>
                                    <tr>
                                        <td><i class="icon-screen-smartphone"></i>&nbsp; <span>Berangkat awal</td>
                                        <td>:</td>
                                        <td><span id="detail_berangkat_awal"></span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="icon-screen-smartphone"></i>&nbsp; <span>Transit</td>
                                        <td>:</td>
                                        <td><span id="detail_berangkat_transit"></span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="icon-screen-smartphone"></i>&nbsp; <span>Landing</td>
                                        <td>:</td>
                                        <td><span id="detail_berangkat_landing"></span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="icon-home"></i>&nbsp; <span>Hotel</td>
                                        <td>:</td>
                                        <td><span id="detail_hotel"></span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="icon-calendar"></i>&nbsp; <span>Mulai Publish</td>
                                        <td>:</td>
                                        <td><span id="detail_mulai_publish"></span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="icon-calendar"></i>&nbsp; <span>Akhir Publish</td>
                                        <td>:</td>
                                        <td><span id="detail_akhir_publish"></span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="icon-map"></i>&nbsp; <span>Info Tambahan</td>
                                        <td>:</td>
                                        <td><span id="detail_info_plus"></span></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-circle grey">Close &nbsp;<i class="fa fa-times"></i></button>            

            </div>
        </div>


    </div>
</div>