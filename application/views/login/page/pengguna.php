<?php $login_type = $this->session->userdata('login_type'); ?>
<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<?php include 'global_function.php'; ?>
<script type="text/javascript">
    function add_function(task){
        if(task == 'show'){
            $('#add_modal').modal();
        }
        if(task == 'add'){
            if(check_empty()>0){
                return alert('Form Belum Lengkap');
            }
            pass = $('#password').val();
            conf_pass = $('#conf_password').val();
            if(pass && pass!=conf_pass)
                return alert('Konfirmasi Password Salah !');

            $('#add_modal').modal('hide');
            data = new FormData($('#add-form')[0]);
            url = '<?php echo base_url() ?>administrator/pengguna/add';
            AjaxCRUD('add',data,url);
        }
    }

    function edit_function(task='',id=''){
        if(task == 'edit'){

            $('.data_input'+id).removeClass('hide');
            $('.data_text'+id).addClass('hide');

            $('.btn_save'+id).removeClass('hide');
            $('.btn_cancel'+id).removeClass('hide');

            $('.btn_edit'+id).addClass('hide');
            $('.btn_hapus'+id).addClass('hide');
        }

        else if(task == 'cancel'){
            $('.data_input'+id).addClass('hide');
            $('.data_text'+id).removeClass('hide');

            $('.btn_save'+id).addClass('hide');
            $('.btn_cancel'+id).addClass('hide');

            $('.btn_edit'+id).removeClass('hide');
            $('.btn_hapus'+id).removeClass('hide');
        }

        else if(task == 'update'){

            if(
                $('#kota_'+id).val()== ""||
                $('#no_hp_'+id).val()== "" ||
                $('#email_'+id).val()== "" ||
                $('#nama_lengkap_'+id).val()== ""
            ) return alert('Form pengguna belum lengkap');

            data = new FormData($('#edit-form'+id)[0]);
            url = '<?php echo base_url() ?>administrator/pengguna/update/'+id;
            AjaxCRUD('update',data,url);
        }

        else if(task == 'delete'){
            url = '<?php echo base_url() ?>administrator/pengguna/delete/'+id;
            AjaxCRUD('delete',"",url);
        }

        else if(task == 'edit_akun'){
            $('#pengguna_id').val(id);
            $('#edit_akun_modal').modal();
        }

        else if(task == 'update_akun'){
            pass = $('#update_password').val();
            conf_pass = $('#update_conf_password').val();
            if(pass && pass!=conf_pass)
                return alert('Konfirmasi Password Salah !');

            id = $('#pengguna_id').val();
            $('#edit_akun_modal').modal('hide');
            data = new FormData($('#edit_akun-form')[0]);
            url = '<?php echo base_url() ?>administrator/pengguna/reset_akun/'+id;
            AjaxCRUD('update',data,url);

        }        
    }
    function change_kantor(id){
        $('#_perwakilan_'+id).hide(); 
        $('#_cabang_'+id).hide();

        $('#perwakilan_'+id).val("").trigger('change'); 
        $('#cabang_'+id).val("").trigger('change');

        if($('#level_'+id).val() == 'Perwakilan Daerah') {
            $('#_perwakilan_'+id).show('slow'); 
            $('#_cabang_'+id).hide();
        } 
        else if($('#level_'+id).val() == 'Kantor Pusat' || $('#level_'+id).val() == 'Kantor Cabang') {
            $('#_perwakilan_'+id).hide(); 
            $('#_cabang_'+id).show('slow');
        } 
    }
    function check_empty(){
        var empty = 0;
            $('input', '#add-form').each(function(){
                if($(this).val() == "" && $(this).attr('id')){
                    empty++;
                }
            })
            $('select', '#add-form').each(function(){
                if($(this).val() == "" && $(this).attr('id')
                    && $(this).attr('id')!="cabang"
                    && $(this).attr('id')!="perwakilan"){
                    empty++;
                }
            })
            return empty;
    }   
    $(function() {
        $('#level').change(function(){
            if($('#level').val() == 'Perwakilan Daerah') {
                $('#div_perwakilan').show('slow'); 
                $('#div_cabang').hide();
            } 
            else if($('#level').val() == 'Kantor Pusat' || $('#level').val() == 'Kantor Cabang') {
                $('#div_perwakilan').hide(); 
                $('#div_cabang').show('slow');
            } 
        });
        $('#div_perwakilan').hide(); 
        $('#div_cabang').hide();
    });      
</script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Pengguna
                <small>Sistem</small>
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="<?php echo base_url().'administrator' ?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Pengguna</span>
        </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">
        <div class="col-md-12">
            <div class="table-toolbar">
                <div class="row">
                    <div class="col-md-6">
                        <div class="btn-group">
                            <button id="sample_editable_1_new" onClick="add_function('show')" class="btn btn-sm red">Tambah Pengguna
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Managed Pengguna</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered " id="table-custom">
                        <thead>
                            <tr>
                                <th> No. </th>
                                <th> Nama </th>
                                <th> Handphone </th>
                                <th> Email </th>
                                <th> Kantor Cabang </th>
                                <th> Level </th>
                                <th> Aksi </th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if($pengguna > 0){ 
                                $count=0;
                                foreach($pengguna as $row){
                                    $id=encrypt($row->pengguna_id);
                                    $nama = $row->nama_lengkap;
                                    $hp = $row->no_hp;
                                    $level = $row->level;
                                    $email = $row->email;
                                    $count++;
                        ?>
                            <tr>
                        <?php echo form_open('administrator', array( 'id' => 'edit-form'.$id)); ?>
                                <td align="center">
                                    <?php echo $count; ?>.
                                </td>
                                <td>
                                    <div style="margin:0px;padding:0px" class="data_input<?php echo $id ?> hide form-group">
                                        <label for="nama">Nama</label>
                                        <input type="text" class="form-control" name="nama_lengkap" id="nama_lengkap_<?php echo $id ?>" value="<?php echo $nama ?>">
                                    </div>
                                    <span class="data_text<?php echo $id ?>"><?php echo $nama ?></span>
                                </td>
                                <td>
                                    <div style="margin:0px;padding:0px" class="data_input<?php echo $id ?> hide form-group">
                                        <label for="hp">Handphone</label>
                                        <input type="text" class="form-control" name="no_hp" id="no_hp_<?php echo $id ?>" value="<?php echo $hp ?>">
                                    </div>
                                    <span class="data_text<?php echo $id ?>"><i class="fa fa-phone"></i>&nbsp;<?php echo $hp ?></span><br>
                                </td>
                                <td>
                                    
                                    <div style="margin:0px;padding:0px" class="data_input<?php echo $id ?> hide form-group">
                                        <label for="email">Email</label>
                                        <input type="text" class="form-control" name="email" id="email_<?php echo $id ?>" value="<?php echo $email ?>">
                                    </div>
                                    <span class="data_text<?php echo $id ?>"><a><i class="fa fa-envelope"></i>&nbsp;<?php echo $email ?></a></span>
                                </td>
                                <td>
                                    
                                    <div style="margin:0px;padding:0px" class="data_input<?php echo $id ?> hide form-group">
                                        <label for="email">Kantor Cabang</label>
                                        <select class="form-control" name="kantorcabang_id" id="kantorcabang_id_<?php echo $id ?>">
                                            <?php foreach($kantor_cabang as $kc){
                                                    $selected = $kc->kantorcabang_id == $row->kantorcabang_id ? 'selected' :'';
                                                    echo '<option value="'.encrypt($kc->kantorcabang_id).'" '.$selected.'>'.$kc->nama_cabang.'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <span class="data_text<?php echo $id ?>">&nbsp;<?php echo $row->nama_cabang ?></span>
                                </td>
                                <td>
                                    <div style="margin:0px;padding:0px" onChange="change_kantor(<?php echo $id ?>)" class="data_input<?php echo $id ?> hide form-group">
                                        <label for="email">Level</label>
                                        <select class="form-control" name="level" id="level_<?php echo $id ?>">
                                            <option value="Administrator" <?php if($level=='Administrator'){echo 'selected';} ?> >Administrator</option>
                                            <option value="Keuangan" <?php if($level=='Keuangan'){echo 'selected';} ?> >Keuangan</option>
                                            <option value="Customer Service" <?php if($level=='Customer Service'){echo 'selected';} ?>>Customer Service</option>
                                            <option value="Logistik" <?php if($level=='Logistik'){echo 'selected';} ?> >Logistik</option>
                                            <option value="CS & Logistik" <?php if($level=='CS & Logistik'){echo 'selected';} ?> >CS & Logistik</option>
                                            <option value="Eksekutif" <?php if($level=='Eksekutif'){echo 'selected';} ?> >Eksekutif</option>
                                        </select>
                                    </div>
                                    <span class="data_text<?php echo $id ?>"><?php echo $level ?></span>
                                    
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="javascript:;" onClick="edit_function('update',<?php echo $id ?>);" class="btn_save<?php echo $id ?> hide"><i class="fa fa-save"></i>&nbsp;Save</a></li>
                                            <li><a href="javascript:;" onClick="edit_function('cancel',<?php echo $id ?>);" class="btn_cancel<?php echo $id ?> hide"><i class="fa fa-times"></i>&nbsp;Cancel</a></li>
                                            <li><a href="javascript:;" onClick="edit_function('edit',<?php echo $id ?>);" class="btn_edit<?php echo $id ?>"><i class="fa fa-pencil"></i>&nbsp;Edit</a></li>
                                            <li><a href="javascript:;" onClick="edit_function('delete',<?php echo $id ?>);" class="btn_hapus<?php echo $id ?>"><i class="fa fa-trash"></i>&nbsp;Hapus</a></li>
                                            <li><a href="javascript:;" onClick="edit_function('edit_akun',<?php echo $id ?>);" class="btn_edit<?php echo $id ?>"><i class="fa fa-pencil"></i>&nbsp;Ganti Password</a></li>
                                        </ul>
                                    </div>
                                    <div class="hide">
                                        <input type="text" id="id_detail_<?php echo $count; ?>" value="<?php echo $id ?>">
                                     </div>
                                </td>
                                
                        <?php echo form_close();?>
                            </tr>
                        <?php } 
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<!-- add Modal-->
<div class="modal fade" id="add_modal" role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-plus font-red"></i>
                    <span class="caption-subject font-red sbold uppercase">Tambah pengguna</span>
                </div>
            </div>
            <div class="portlet-body form">
                <?php echo form_open('administrator', array( 'id' => 'add-form')); ?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_control_1">Nama</label>
                                <input type="text" class="form-control input-sm" name="nama_lengkap" id="nama_lengkap">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_control_1">Email</label>
                                <input type="text" class="form-control input-sm" name="email" id="email" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_control_1">Handphone</label>
                                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control input-sm" name="no_hp" id="no_hp" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" required>
                                <label for="form_control_1">Level</label>
                                <select class="form-control input-sm" name="level" id="level">
                                    <option value="" selected>Pilih Level...</option>
                                    <option value="Administrator">Administrator</option>
                                    <option value="Keuangan">Keuangan</option>
                                    <option value="Customer Service">Customer Service</option>
                                    <option value="Logistik">Logistik</option>
                                    <option value="CS & Logistik">CS & Logistik</option>
                                    <option value="Eksekutif">Eksekutif</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" required>
                                <label for="form_control_1">Kantor Cabang </label>
                                <select class="form-control input-sm" name="kantorcabang_id" id="kantorcabang_id">
                                        <option value="" selected>Pilih Kantor Cabang...</option>
                                        <?php
                                            foreach($kantor_cabang as $row){
                                                echo '<option value="'.encrypt($row->kantorcabang_id).'">'.$row->nama_cabang.'</option>';
                                            }
                                        ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <hr>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_control_1">Password</label>
                                <input type="text" class="form-control input-sm" name="password" id="password" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_control_1">Confirm Password</label>
                                <input type="password" class="form-control input-sm" name="conf_password" id="conf_password" required>
                            </div>
                        </div>
                    </div>
                <?php echo form_close();?>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-circle grey">Cancel &nbsp;<i class="fa fa-times"></i></button>            
                <button type="button" onClick="add_function('add')" class="btn btn-circle red">Simpan &nbsp;<i class="fa fa-save"></i></button>
            </div>
        </div>

    </div>
</div>

<!-- edit akun Modal-->
<div class="modal fade" id="edit_akun_modal" role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-plus font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">Reset Password</span>
                </div>
            </div>
            <div class="portlet-body form">
                <?php echo form_open('administrator', array( 'id' => 'edit_akun-form')); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_control_1" style="font-size:12px">New Password</label>
                                        <input type="text" class="form-control" autocomplete="off" name="update_password" id="update_password" >
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_control_1" style="font-size:12px">Confirm Password</label>
                                        <input type="password" class="form-control" autocomplete="off" name="update_conf_password" id="update_conf_password">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="pengguna_id" id="pengguna_id">
                <?php echo form_close();?>
            </div>
            <div class="modal-footer">
                <button type="button" id="edit_button" onClick="edit_function('update_akun')" class="btn btn-circle green">Reset &nbsp;<i class="fa fa-save"></i></button>
            </div>
        </div>

    </div>
</div>
