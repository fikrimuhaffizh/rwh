<?php $login_type=$this->session->userdata('login_type') ?>
<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<script type="text/javascript">
    var count = 0;
    var total_qty =0;
    var listbarang = [];
    function detail(id=""){
        $('#list_detail').empty();
        $('#detail_modal').modal();
        $.ajax({
                method: 'POST',
                url: '<?php echo base_url() ?>administrator/perlengkapan/get_list_perlengkapan/'+id,
                dataType: 'json',
                beforeSend : function(){$('#loading_modal').show();},
                success : function(resp){
                    $('#loading_modal').hide();
                    if(resp.length>0){
                        catatan = resp[0]['keterangan'] ? resp[0]['keterangan'] :'-';
                        var x ='<div class="detail_list">';
                            x+='    <div class="col-md-12 panel-body">';
                            x+='        <h4>List barang yang diserahkan : </h4>';
                            x+='        <table class="table">';
                            x+='        <th>No</th>';
                            x+='        <th>Barang</th>';
                            x+='        <th>Qty</th>';
                            x+='        <th>Tgl Penyerahan</th>';
                            var pos=0;
                            for(var i=0;i<resp.length;i++){
                                pos++;
                                x+='        <tr>';
                                x+='            <td>'+pos+'. </td>';
                                x+='            <td>'+resp[i]['nama_barang']+'</td>';
                                x+='            <td>'+resp[i]['qty']+' '+resp[i]['satuan']+'</td>';
                                x+='            <td>'+resp[i]['tgl_penyerahan']+'</td>';
                                x+='        </tr>';
                                
                            }
                            x+='        </table>';
                            x+='        <table class="table">';
                            x+='        <tr>';
                            x+='            <td width="80"><h5>Catatan : </h5></td>';
                            x+='            <td><h5>'+catatan+'</h5>';
                            x+='        </table>';
                            x+='    </div>'; 
                            x+='</div>';
                        $('#list_detail').append(x);
                    }
                }
            });
    }
    function getListBarangByKantorCabang(id){
        $('#list_barang').empty();
        $('#btn-tambah-brg').hide();
        $.ajax({
                method: 'POST',
                data:{'kantorcabang_id' : id},
                url: '<?php echo base_url() ?>administrator/barang/getListBarangByKantorCabang',
                dataType : 'JSON',
                beforeSend : function(){$('#loading').show();},
                success : function(resp){
                    $('#loading').hide();
                    if(resp.length>0){
                        $('#btn-tambah-brg').show('hide');
                        listbarang = resp;                        
                    } else {
                        alert('Kantor Cabang tidak memiliki list barang !');
                    }
                }
            });
    }
    function pembatalan(id,kantorcabang_id,pendaftarandetail_id){
        swal({
          title: "Pembatalan Penyerahan Perlengkapan",
          text: "Batalkan penyerahan barang jemaah ? ",
          type: "warning",
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Ok",
          showCancelButton: true,
        },
        function(){
            $.ajax({
                method: 'POST',
                data:{'kantorcabang_id' : kantorcabang_id, 'faktur_id' : id,'pendaftarandetail_id' : pendaftarandetail_id},
                url: '<?php echo base_url() ?>administrator/perlengkapan/pembatalan',
                dataType: 'json',
                success : function(resp){
                    if(resp=='update_success'){
                        location.reload();
                    } 
                }
            });
        });
    }
    function tambah_barang(aksi,urutan){
        if(listbarang.length==0){
            return alert('Silahkan pilih kantor terlebih dahulu...')
        }

        count++;
        var x = '';
            x+='    <tr id="barang_'+count+'">';
            x+='        <td>Barang : </td>';
            x+='        <td>';
            x+='            <select class="form-control input-sm" name="bkantor_id[]" required>';
            x+='                <option value="">Pilih Barang...</option>';
                                if(listbarang.length>0){
                                    for(var i =0;i<listbarang.length;i++){
                                        var stok = listbarang[i]['stok_akhir'];
                                        var disabled ='';
                                        if(stok==0)
                                            disabled = 'disabled';

                                        x+='<option '+disabled+' value='+listbarang[i]['bkantor_id']+'>'+listbarang[i]['nama_barang']+' (stok '+listbarang[i]['stok_akhir']+')</option>';
                                    }
                                }
            x+='            </select>';
            x+='        </td>';
            x+='        <td>Tgl Penyerahan : </td>';            
            x+='        <td><input class="form-control input-sm date-picker date-picker_'+count+'" type="text" name="tgl_penyerahan[]" autocomplete="off" placeholder="Tanggal Penyerahan" required>';
            x+='        </td>';            
            x+='        <td>Qty : </td>';
            x+='        <td>';
            x+='            <input type="number" class="form-control input-sm pull-left" style="width:70px" value="1" name="qty[]" required >';
            x+='            <a class="btn red btn-sm pull-right" onClick="hapus_barang('+count+')"><i class="fa fa-trash"></i></a>';
            x+='        </td>';
            x+='    </tr>';
            $('#list_barang').append(x);
            $('.date-picker_'+count).datepicker('update', new Date());
            $('.btn-simpan').show();
    }
    function hapus_barang(urutan){
        $('#barang_'+urutan).hide('puff',function(){$(this).remove()});
    }
    $(document).ready(function(){
        table = $('#table_umroh').DataTable({ 
            "processing": true, "serverSide": true,"order": [],
            "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
            "ajax": {"url": "<?php echo site_url('administrator/perlengkapan/pagination/umroh')?>","type": "POST"},
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                        {"targets": [0],"className": "dt-center"},
                        {"targets": [2],"className": "dt-center"},
                        {"targets": [3],"className": "dt-center"},
                    ]
        }); 

       table2 = $('#table_haji').DataTable({ 
            "processing": true, "serverSide": true,"order": [],
            "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
            "ajax": {"url": "<?php echo site_url('administrator/perlengkapan/pagination/haji')?>","type": "POST"},
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                        {"targets": [0],"className": "dt-center"}
                    ]
        });

        table3 = $('#table_sudah_berangkat').DataTable({ 
            "processing": true, "serverSide": true,"order": [],
            "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
            "ajax": {"url": "<?php echo site_url('administrator/perlengkapan/pagination/sudah_berangkat')?>","type": "POST"},
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                        {"targets": [0],"className": "dt-center"},
                        {"targets": [2],"className": "dt-center"},
                        {"targets": [3],"className": "dt-center"},
                    ],
            "createdRow" : function(row,data,index){$(row).css("background-color","#F0FFF0");}
        });

        table4 = $('#table_cancel').DataTable({ 
            "processing": true, "serverSide": true,"order": [],
            "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
            "ajax": {"url": "<?php echo site_url('administrator/perlengkapan/pagination/cancel')?>","type": "POST"},
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                        {"targets": [0],"className": "dt-center"},
                        {"targets": [2],"className": "dt-center"},
                        {"targets": [3],"className": "dt-center"},
                    ],
            "createdRow" : function(row,data,index){$(row).css("background-color","#FFF1F1");}
        });


        $("#add-form").submit(function() {
            $('[name="qty[]"]').each(function(){
                total_qty += Number($(this).val());
                $('[name="total_qty"]').val(total_qty);
            })
        });       
    });
</script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Perlengkapan Jemaah
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>

        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo base_url().'administrator' ?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Perlengkapan Jemaah</span>
            </li>
        </ul>
            <div class="row">               
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        
                            <?php if($show_detail){ ?>
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="fa fa-list font-dark"></i>
                                        <span class="caption-subject bold uppercase"> List Penyerahan Perlengkapan Jemaah <span class="font-red">"<?php echo $dt_jemaah[0]->nama_jemaah ?>"</span></span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <a href="<?php echo base_url().'administrator/perlengkapan'?>" class="btn btn-sm yellow-crusta margin-left-5"><i class="fa fa-arrow-circle-o-left"></i> Kembali</a>
                                    <a class="btn btn-sm green-jungle list-toggle-container collapsed" data-toggle="collapse" data-parent="#accordion1" onclick=" " href="#task-1" aria-expanded="false">
                                        <div class="list-toggle done">
                                            <div><i class="fa fa-hand-paper-o"></i> Serahkan Perlengkapan</div>
                                        </div>
                                    </a>
                                    <hr>
                                    
                                    <?php if($this->session->flashdata('err_add')){ ?>
                                        <div class="alert alert-danger"><i class="fa fa-times"></i> Gagal menyerahkan perlengkapan ! <br> <?php foreach($this->session->flashdata('err_add') as $err){echo '- '.$err.'<br>';} ?></div>
                                    <?php } ?>
                                    <?php if($this->session->flashdata('add_success')){ ?>
                                        <div class="alert bg-green-jungle font-white"><i class="fa fa-check"></i> Data penyerahan berhasil disimpan</div>
                                    <?php } ?>

                                    <div class="task-list panel-collapse collapse" id="task-1" aria-expanded="false" style="height: 0px;">
                                        <div class="task-content col-md-12" >
                                        <h4>Tentukan kantor penyerahan lalu klik 'Tambah Barang' : </h4>
                                        <?php echo form_open('administrator/perlengkapan/add/'.$pendaftarandetail_id, array( 'id' => 'add-form')); ?>
                                        <div class="col-md-6">
                                            <table class="table">
                                                <tr>
                                                    <td width="180">Kantor Penyerahan</td>
                                                    <td>:</td>
                                                    <td>
                                                        <select class="form-control input-sm" name="kantorcabang_id" id="kantorcabang_id" onchange="getListBarangByKantorCabang(this.value)">
                                                            <option value="">Pilih Kantor Cabang...</option>
                                                            <?php
                                                                foreach($kantor_cabang as $row){
                                                                    echo '<option value="'.encrypt($row->kantorcabang_id).'">'.$row->nama_cabang.'</option>';
                                                                }
                                                            ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Catatan</td>
                                                    <td>:</td>
                                                    <td><textarea class="form-control input-sm" name="catatan" maxlength="500" placeholder="Maksimal 500 Karakter" autocomplete="off"></textarea></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="clearfix"></div>
                                            <div id="btn-tambah-brg" class="col-md-12 display-none">
                                                <a class="btn btn-xs red " onclick="tambah_barang()"><i class="fa fa-plus"></i> Tambah Barang</a>
                                            </div>
                                            <h4 class="col-md-12 display-none" id="loading">
                                                <img src="<?php echo base_url().'assets\frontend\img\AjaxLoader.gif'?>"> 
                                                <span>Mengambil data ...</span>
                                            </h4>
                                            <div class="col-md-12">
                                                <table class="table">
                                                    <tbody id="list_barang">
                                                        
                                                    </tbody>
                                                </table>
                                                <input type="hidden" name="total_qty">
                                                <button type="submit" class="pull-right btn btn-sm green-jungle btn-simpan display-none" ><i class="fa fa-save"></i> Simpan</button>
                                            </div>
                                        <?php echo form_close();?>
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr>
                                    </div>
                                    <div class="clearfix"></div>  
                                    
                                    <div class="portlet-body">
                                        <table class="table table-condensed">
                                            <thead>
                                                <tr>
                                                    <th style="text-align: center"> No </th>
                                                    <th> No. Faktur</th>
                                                    <th> Jumlah</th>
                                                    <th style="text-align: center"> Tanggal Input </th>
                                                    <th> Aksi </th>
                                                </tr>
                                            </thead>
                                            <?php
                                                $count=0;

                                                foreach($list_faktur as $row){
                                                    $id = encrypt($row->faktur_id);
                                                    $kantorcabang_id = encrypt($row->kantorcabang_id);
                                            ?>
                                                <tr>
                                                    <td style="text-align: center"><?php echo ++$count?></td>
                                                    <td><?php echo $row->no_faktur ?></td>
                                                    <td><i class="icon-social-dropbox"></i> <?php echo $row->total_qty ?> Item</td>
                                                    <td style="text-align: center"><i class="fa fa-calendar"></i> <?php echo date('d-F-Y',strtotime($row->tgl_transaksi)) ?></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                                <i class="fa fa-angle-down"></i>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu">
                                                                <li><a href="javascript:;" onClick="detail(<?php echo $id?>)" ><i class="fa fa-search"></i>&nbsp;Detail</a></li>
                                                                <li><a href="<?php echo base_url().'administrator/perlengkapan/cetak/'.$id?>" target="_blank" ><i class="fa fa-print"></i>&nbsp;Cetak</a></li>
                                                                <li><a href="javascript:;" onClick="pembatalan(<?php echo $id?>,<?php echo $kantorcabang_id?>,<?php echo $pendaftarandetail_id?>)" ><i class="fa fa-times"></i>&nbsp;Batalkan</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                    </div>
                                </div>
                            
                            <?php } else {?>
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Managed Perlengkapan Jemaah</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#tab_1_1" data-toggle="tab"> <i class="icon-reload"></i> On Process </a>
                                        </li>
                                        <li>
                                            <a href="#tab_1_2" data-toggle="tab"> <i class="fa fa-paper-plane-o"></i> Sudah Berangkat </a>
                                        </li>
                                        <li>
                                            <a href="#tab_1_3" data-toggle="tab"> <i class="icon-ban"></i> Cancel </a>
                                        </li>
                                        <li>
                                            <a href="#tab_1_4" data-toggle="tab"> <i class="icon-star"></i> Jemaah Haji </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in" id="tab_1_1">
                                            <table class="table table-stripfed table-bordered  table-condensed table-hover" id="table_umroh">
                                                <thead>
                                                    <tr>
                                                        <th> NO </th>
                                                        <th> Nama Jemaah</th>
                                                        <th> Keberangkatan </th>
                                                        <th> Total Penyerahan </th>
                                                        <th> Aksi </th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                        <div class="tab-pane fade" id="tab_1_2">
                                            <table class="table table-striped table-bordered  table-condensed" id="table_sudah_berangkat">
                                                <thead>
                                                    <tr>
                                                        <th> NO </th>
                                                        <th> Nama Jemaah</th>
                                                        <th> Keberangkatan </th>
                                                        <th> Total Penyerahan </th>
                                                        <th> Aksi </th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>     
                                        <div class="tab-pane fade" id="tab_1_3">
                                            <table class="table table-striped table-bordered  table-condensed" id="table_cancel">
                                                <thead>
                                                    <tr>
                                                        <th> NO </th>
                                                        <th> Nama Jemaah</th>
                                                        <th> Keberangkatan </th>
                                                        <th> Total Penyerahan </th>
                                                        <th> Aksi </th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                        <div class="tab-pane fade" id="tab_1_4">
                                            <table class="table table-striped table-bordered  table-condensed" id="table_haji">
                                                <thead>
                                                    <tr>
                                                        <th> NO </th>
                                                        <th> Nama Jemaah</th>
                                                        <th> Keberangkatan </th>
                                                        <th> Total Penyerahan </th>
                                                        <th> Aksi </th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>      
                                    </div> 
                                </div>
                            
                            <?php } ?>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<div class="modal fade" id="detail_modal"  role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="portlet light bordered">
            <div class="portlet-body form" >
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="col-md-12 display-none" id="loading_modal">
                            <img src="<?php echo base_url().'assets\frontend\img\AjaxLoader.gif'?>">
                            <span>Mengambil data ...</span>
                        </h4>
                        <div class="panel-group accordion scrollable" id="accordion2">
                            <div id="list_detail"></div>
                             <div class="pull-right">
                                <button type="button" data-dismiss="modal" class="btn grey">Close</i></button>            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>