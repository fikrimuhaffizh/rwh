<?php $login_type=$this->session->userdata('login_type') ?>
<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function updateAllTable(){
        table.ajax.reload();
        table2.ajax.reload();
        table3.ajax.reload();
        table4.ajax.reload();
    }
    function tolak_file(id){
        $('#list_syarat_modal').modal('hide');
        swal({
              title: "Tolak File Upload Jemaah ?",
              type: "warning",
              text: "Email akan dikirimkan kepada jemaah terkait penolakan file upload",
              showCancelButton: true,
              confirmButtonClass: "grey",
              confirmButtonText: "Ya, dan Kirim Email Pemberitahuan",
              closeOnConfirm: false,
              showLoaderOnConfirm: true,
            },
            function(){
                pendaftarandetail_id = $('#pendaftarandetail_id').val();
                nama_jemaah          = $('#nama_jemaah_'+pendaftarandetail_id).html();
                pendaftaran_id       = $('#pendaftaran_id_'+pendaftarandetail_id).val();
                email                = $('#email_'+pendaftarandetail_id).val()
                syarat               = $('#nama_syarat_'+pendaftarandetail_id).html();

                data = 'nama_jemaah='+nama_jemaah+'&pendaftaran_id='+pendaftaran_id+'&email='+email+'&pendaftarandetail_id='+pendaftarandetail_id+'&syarat='+syarat;
                $.ajax({
                    method: 'POST',
                    beforeSend: function(){$('#loading').show();},
                    data:data,
                    url: '<?php echo base_url() ?>administrator/persyaratan/tolak_file/'+id,
                    dataType: 'json',
                    success : function(resp){
                        if(resp=='update_success')
                            updateAllTable();
                    }
                });
            }); 
    }
    function add_function(task="",id=""){
        if(task == 'show'){
            id= $('#pendaftarandetail_id').val();
            $('.add_list_syarate').remove();
            $('.header-modal').text('Ubah Persyaratan');
            $('.form-modal').addClass('form-md-floating-label');
            $.ajax({
                method: 'POST',
                url: '<?php echo base_url() ?>administrator/persyaratan/get_all_syarat/'+id,
                dataType : 'json',
                success : function(resp){
                    var pos = 0;
                    for(var i=0;i<resp.length;i++){
                            var status = "";
                            
                            if(resp[i]['status']=='Ada')
                                    status = 'checked'
                            var x ='        <div class="add_list_syarate">';
                                x+='                    <input type="checkbox" value="'+resp[i]['syaratumroh_id']+'" id="syarat_'+id+'_'+pos+'" class="md-check "'+status+'>';
                                x+='                        <label for="syarat_'+id+'_'+pos+'">';
                                x+='                            <span class="box"></span> '+resp[i]['persyaratan'];
                                x+='                        </label>';
                                x+='                </div>';
                                $('#add_list_syarat').after(x);
                                
                                pos++;
                        }
                }
            });
            $('#add_edit_modal').modal();
        }

        else if(task == 'add'){
                id = $('#pendaftarandetail_id').val();
                $('#add_edit_modal').modal('hide');
                var data = new FormData();   
                for(var i=0;;i++){
                    DOM   = $('#syarat_'+id+'_'+i);
                    val   = DOM.val();
                    check = DOM.prop('checked');

                    if(!val) break;
                    
                    data.append('syaratumroh_'+i,val+'_'+check);
                }
                $.ajax({
                    method: 'POST',
                    data:data,
                    url: '<?php echo base_url() ?>administrator/persyaratan/add_syarat_to_jamaah/'+id,
                    dataType: 'text',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success : function(resp){
                        if(resp == '"update_success"'){
                            swal({
                                  title: "Berhasil !",
                                  text : "Syarat jemaah berhasil diperbaharui",
                                  type: "success",
                                  timer: 800,
                                  showConfirmButton : false
                                });
                            updateAllTable();
                        }
                    }
                });
        }
    }

    function edit_function(task='',id=''){
        if(task == 'show_syarat'){
            $('.header-modal').text('Cek Persyaratan');
            $('#pendaftarandetail_id').val(id);
            $('.syarate').remove();
            $('#list_syarat_modal').modal();
            $.ajax({
                    method: 'POST', 
                    data:data,
                    url: '<?php echo base_url() ?>administrator/persyaratan/show_syarat/'+id,
                    dataType: 'json',
                    success : function(resp){
                        nama_jemaah = $('#nama_jemaah_'+id).html();
                        $('#nama_jemaah_modal').html(nama_jemaah);
                        var pos = 0;
                        if(resp.length > 0){
                            for(var i=0;i<resp.length;i++){
                                var status          = "";
                                var status_upload   = "";
                                var syaratumroh_id  = resp[i]['syaratumroh_id'];
                                var syaratstatus_id = resp[i]['syaratstatus_id'];
                                var syarat          = resp[i]['persyaratan'];
                                var tipe            = resp[i]['tipe'];
                                var approve         = resp[i]['approve'];
                                var tgl         = resp[i]['tgl_penyerahan'];


                                //Jemaah belum mengupload file
                                if(approve=='Tidak' || (tipe=='Upload' && !resp[i]['media_link'])){
                                    status_upload='<i style="font-size:12px;font-color:grey"> Menunggu upload jemaah...</i>';
                                    tgl_penyerahan = '';
                                    if(resp[i]['status_syarat']=='Lengkap')
                                            status = 'checked'
                                }
                                else {
                                    if(resp[i]['status_syarat']=='Lengkap')
                                            status = 'checked'

                                    //Jemaah upload file
                                    if(resp[i]['media_link']){
                                        status_upload='<a href="'+resp[i]['media_link']+'" target="_blank" style="font-size:12px;text-decoration:none"> <i class="fa fa-download"></i> Cek </a>';

                                        //Jemaah upload file namun file belum di cek admin
                                        if(resp[i]['status_syarat']=='Belum Lengkap'){
                                            status_upload +=' | <input type="checkbox" name="tolak_file[]"><a href="javascript:;" onClick="tolak_file('+syaratstatus_id+')" style="font-size:12px;text-decoration:none" class="font-red"> <i class="fa fa-times"></i> Tolak File Upload</a>';
                                        }
                                    }
                                    
                                    if(tipe=='Non-Upload'){
                                        status_upload='';
                                        tgl_penyerahan = '<small>Tanggal Penyerahan</small> : &nbsp; <input class=" date-picker pull-right" type="text" name="tgl_penyerahan" value="'+tgl+'" id="tgl_penyerahan_'+id+'_'+pos+'" style="width:100px" placeholder="dd/mm/yyyy" autocomplete="off" required>';
                                    }
                                }

                                var x ='        <div class="syarate">';
                                    x+='                <div class="md-checkbox" style="border-bottom:1px solid #eee">';
                                    x+='                    <input type="checkbox" value="'+syaratumroh_id+'" id="syarat_jemaah_'+id+'_'+pos+'" class="md-check"'+status+'>';
                                    x+='                        <label for="syarat_jemaah_'+id+'_'+pos+'">';
                                    x+='                            <span class="inc"></span>';
                                    x+='                            <span class="check"></span>';
                                    x+='                            <span class="box"></span> <div id="nama_syarat_'+id+'">'+syarat+'</div>';
                                                                    x+=tgl_penyerahan;

                                    x+='                        </label><label class="pull-right">'+status_upload+'</label>';
                                    x+='               </div>';
                                    x+='       </div>';
                                    $('#list_syarat').after(x);
                                    $('.date-picker').datepicker({format: 'dd/mm/yyyy'});
                                    pos++;
                            }
                        } else {
                            var x ='        <div class="syarate">';
                                x+='                <div class="alert alert-danger">Syarat <strong>Belum</strong> diset. Silahkan "Ubah Persyaratan" dahulu </div>';
                                x+='        </div>';
                                $('#list_syarat').after(x);
                        }  
                    }
                });
        }
        else if(task == 'update'){
            id = $('#pendaftarandetail_id').val();
            $('#list_syarat_modal').modal('hide');
            var data = new FormData();   
            for(var i=0;;i++){
                DOMSyarat   = $('#syarat_jemaah_'+id+'_'+i);
                val   = DOMSyarat.val();
                check = DOMSyarat.prop('checked');

                DOMtgl   = $('#tgl_penyerahan_'+id+'_'+i);
                val2   = DOMtgl.val();

                if(!val) break;
                
                data.append('syaratjemaah_'+i,val+'_'+check);
                data.append('tgl_penyerahan_'+i,val2);
            }
            
            swal({
                  title: "Memperbaharui Syarat Jemaah",
                  text: "Klik 'Update' untuk memulai.",
                  type: "info",
                  closeOnConfirm: false,
                  confirmButtonText: 'Update',
                  showLoaderOnConfirm: true,
                },
                function(){
                    $.ajax({
                        method: 'POST',
                        data:data,
                        url: '<?php echo base_url() ?>administrator/persyaratan/update_syarat_jemaah/'+id,
                        dataType: 'text',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success : function(resp){
                            if(resp == '"update_success"'){
                                swal({
                                  title: "Berhasil !",
                                  text : "Syarat jemaah berhasil diperbaharui",
                                  type: "success",
                                  timer: 800,
                                  showConfirmButton : false
                                });
                                updateAllTable();
                            }
                        }
                    });
                });
        }
    }
    $(document).ready(function(){
        table = $('#table_umroh').DataTable({ 
            "processing": true, "serverSide": true,"order": [],
            "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
            "ajax": {"url": "<?php echo site_url('administrator/persyaratan/pagination/umroh')?>","type": "POST"},
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                        {"targets": [0],"className": "dt-center"},
                        {"targets": [2],"className": "dt-center"},
                    ],
            "initComplete" : function (resp) {
                this.api().columns([4]).every( function () {
                    var column = this;
                    var select = $('<select class="from-control input-sm input-small"><option value="">Semua</option><option value="Sudah Lengkap">Lengkap</option><option value="Belum Lengkap">Belum Lengkap</option></select>')
                                    .appendTo( $('#filter_syarat_umroh').empty())
                                    .on( 'change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                        column.search( $(this).val(), true, false ) .draw();
                                    });
                    
                });
            }  
        }); 

       table2 = $('#table_haji').DataTable({ 
            "processing": true, "serverSide": true,"order": [],
            "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
            "ajax": {"url": "<?php echo site_url('administrator/persyaratan/pagination/haji')?>","type": "POST"},
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                        {"targets": [0],"className": "dt-center"}
                    ],
            "initComplete" : function (resp) {
                this.api().columns([4]).every( function () {
                    var column = this;
                    var select = $('<select class="from-control input-sm input-small"><option value="">Semua</option><option value="Sudah Lengkap">Lengkap</option><option value="Belum Lengkap">Belum Lengkap</option></select>')
                                    .appendTo( $('#filter_syarat_haji').empty())
                                    .on( 'change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                        column.search( $(this).val(), true, false ) .draw();
                                    });
                    
                });
            }  
        });

        table3 = $('#table_sudah_berangkat').DataTable({ 
            "processing": true, "serverSide": true,"order": [],
            "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
            "ajax": {"url": "<?php echo site_url('administrator/persyaratan/pagination/sudah_berangkat')?>","type": "POST"},
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                        {"targets": [0],"className": "dt-center"},
                        {"targets": [2],"className": "dt-center"},
                    ],
            "initComplete" : function (resp) {
                this.api().columns([4]).every( function () {
                    var column = this;
                    var select = $('<select class="from-control input-sm input-small"><option value="">Semua</option><option value="Sudah Lengkap">Lengkap</option><option value="Belum Lengkap">Belum Lengkap</option></select>')
                                    .appendTo( $('#filter_syarat_sudah_berangkat').empty())
                                    .on( 'change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                        column.search( $(this).val(), true, false ) .draw();
                                    });
                    
                });
            },
            "createdRow" : function(row,data,index){$(row).css("background-color","#F0FFF0");}
        });

        table4 = $('#table_cancel').DataTable({ 
            "processing": true, "serverSide": true,"order": [],
            "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
            "ajax": {"url": "<?php echo site_url('administrator/persyaratan/pagination/cancel')?>","type": "POST"},
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                        {"targets": [0],"className": "dt-center"},
                        {"targets": [2],"className": "dt-center"},
                    ],
            "initComplete" : function (resp) {
                this.api().columns([4]).every( function () {
                    var column = this;
                    var select = $('<select class="from-control input-sm input-small"><option value="">Semua</option><option value="Sudah Lengkap">Lengkap</option><option value="Belum Lengkap">Belum Lengkap</option></select>')
                                    .appendTo( $('#filter_syarat_cancel').empty())
                                    .on( 'change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                        column.search( $(this).val(), true, false ) .draw();
                                    });
                    
                });
            },
            "createdRow" : function(row,data,index){$(row).css("background-color","#FFF1F1");}
        });

        var x = window.location.pathname.split( '/' );
        var id = x[4];
        if(id)
            edit_function('show_syarat',id);
    });
</script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Persyaratan Jemaah
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>

        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo base_url().'administrator' ?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Persyaratan Jemaah</span>
            </li>
        </ul>
            <div class="row">
                <div class="col-md-12">

                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold uppercase"> Managed Syarat Jemaah</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_1_1" data-toggle="tab"> <i class="icon-reload"></i> On Process </a>
                                    </li>
                                    <li>
                                        <a href="#tab_1_2" data-toggle="tab"> <i class="fa fa-paper-plane-o"></i> Sudah Berangkat </a>
                                    </li>
                                    <li>
                                        <a href="#tab_1_3" data-toggle="tab"> <i class="icon-ban"></i> Cancel </a>
                                    </li>
                                     <li>
                                        <a href="#tab_1_4" data-toggle="tab"> <i class="icon-star"></i> Jemaah Haji </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab_1_1">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-inline pull-left">
                                                        <div class="form-group"><span>Filter By : </span></div>
                                                        <div class="form-group"><div id="filter_syarat_umroh"></div></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-stripfed table-bordered  table-condensed table-hover" id="table_umroh">
                                            <thead>
                                                <tr>
                                                    <th> NO </th>
                                                    <th> Nama Jemaah</th>
                                                    <th> Keberangkatan </th>
                                                    <th> Status Syarat </th>
                                                    <th> Aksi </th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="tab_1_2">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-inline pull-left">
                                                        <div class="form-group"><span>Filter By : </span></div>
                                                        <div class="form-group"><div id="filter_syarat_sudah_berangkat"></div></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered  table-condensed" id="table_sudah_berangkat">
                                            <thead>
                                                <tr>
                                                    <th> NO </th>
                                                    <th> Nama Jemaah</th>
                                                    <th> Keberangkatan </th>
                                                    <th> Status Syarat </th>
                                                    <th> Aksi </th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>     
                                    <div class="tab-pane fade" id="tab_1_3">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-inline pull-left">
                                                        <div class="form-group"><span>Filter By : </span></div>
                                                        <div class="form-group"><div id="filter_syarat_cancel"></div></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered  table-condensed" id="table_cancel">
                                            <thead>
                                                <tr>
                                                    <th> NO </th>
                                                    <th> Nama Jemaah</th>
                                                    <th> Keberangkatan </th>
                                                    <th> Status Syarat </th>
                                                    <th> Aksi </th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>    
                                    <div class="tab-pane fade" id="tab_1_4">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-inline pull-left">
                                                        <div class="form-group"><span>Filter By : </span></div>
                                                        <div class="form-group"><div id="filter_syarat_haji"></div></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered  table-condensed" id="table_haji">
                                            <thead>
                                                <tr>
                                                    <th> NO </th>
                                                    <th> Nama Jemaah</th>
                                                    <th> Keberangkatan </th>
                                                    <th> Status Syarat </th>
                                                    <th> Aksi </th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>                                                                
                                </div>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<!-- add Syarat Modal-->
<div class="modal fade" id="add_edit_modal"  role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-pencil font-red"></i>
                    <span class="header-modal caption-subject font-red sbold uppercase">Ubah Persyaratan</span>
                </div>
            </div>
            <div class="portlet-body form" >
                <div class="row">
                    <div class="col-md-12">
                        <label class="label label-danger"><b>Perhatian !</b> Mengganti syarat akan menghapus file yang diupload jemaah </label>
                        <h4 id="add_list_syarat">Silahkan menambah atau mengurangi syarat jemaah :</h4>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                
                <input type="hidden" id="pendaftarandetail_id">
                <button type="button" data-dismiss="modal" class="btn btn-sm grey">Cancel &nbsp;<i class="fa fa-times"></i></button>            
                <button type="button" onClick="add_function('add')" class="btn_update btn btn-sm red">Simpan Perubahan Syarat &nbsp;<i class="fa fa-save"></i></button>
            </div>
        </div>

    </div>
</div>

<!--List Syarat Modal-->
<div class="modal fade" id="list_syarat_modal"  role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-users font-green"></i>
                    <span class="header-modal caption-subject font-green sbold uppercase">Check Persyaratan</span>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="row">
                    <div class="col-md-12">
                        <h4 id="list_syarat">Checklist jika perysaratan <b id="nama_jemaah_modal"></b> sudah terpenuhi :</h4>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" id="pendaftarandetail_id">
                <button type="button" data-dismiss="modal" onClick="add_function('show')" class="btn btn-sm red btn_edit pull-left">Ubah Persyaratan &nbsp;<i class="fa fa-pencil"></i></button>
                <button type="button" data-dismiss="modal" class="btn grey">Cancel &nbsp;<i class="fa fa-times"></i></button>            
                <button type="button" onClick="edit_function('update')" class="btn_update btn green">Update &nbsp;<i class="fa fa-save"></i></button>            
            </div>
        </div>

    </div>
</div>

