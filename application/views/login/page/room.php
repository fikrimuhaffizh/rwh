<?php $login_type = $this->session->userdata('login_type') ?>
<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<script type="text/javascript">
    var count_quad = 1;
    var count_triple = 1;
    var count_double = 1;
    var count_status = 0;
    function addRoom(kelas){
        if(kelas=="Quad"){
            count_quad++;
            var x = '';
                x+='    <div class="portlet light bordered">';
                x+='        <div class="portlet-body">';
                x+='            <span class="caption-subject font-red sbold uppercase">Quad '+count_quad+' (Q'+count_quad+')</span>';
                x+='            <div class="clearfix"></div>';
                x+='            <div class="dd" id="Quad_'+count_quad+'" data-id="Q'+count_quad+'"></div>';
                x+='        </div>';
                x+='    </div>';
            $('#Quad_list').prepend(x);
            $('#Quad_'+count_quad).nestable({group : 1,beforeDragStop: function(l,e,p) {updateRoom(e,p);}});
        } 
        else if(kelas=='Triple'){
            count_triple++;
            var x = '';
                x+='    <div class="portlet light bordered">';
                x+='        <div class="portlet-body">';
                x+='            <span class="caption-subject font-purple sbold uppercase">Triple '+count_triple+' (T'+count_triple+')</span>';
                x+='            <div class="clearfix"></div>';
                x+='            <div class="dd" id="Triple_'+count_triple+'" data-id="T'+count_triple+'"></div>';
                x+='        </div>';
                x+='    </div>';
            $('#Triple_list').prepend(x);
            $('#Triple_'+count_triple).nestable({group : 1,beforeDragStop: function(l,e,p) {updateRoom(e,p);}});
        } 
        else if(kelas=='Double'){
            count_double++;
            var x = '';
                x+='    <div class="portlet light bordered">';
                x+='        <div class="portlet-body">';
                x+='            <span class="caption-subject font-green-jungle sbold uppercase">Double '+count_double+' (D'+count_double+')</span>';
                x+='            <div class="clearfix"></div>';
                x+='            <div class="dd" id="Double_'+count_double+'" data-id="D'+count_double+'"></div>';
                x+='        </div>';
                x+='    </div>';
            $('#Double_list').prepend(x);
            $('#Double_'+count_double).nestable({group : 1,beforeDragStop: function(l,e,p) {updateRoom(e,p);}});
        }
    }

    function updateRoom(e,p){
        var id = $(e).data('id');
        var nama =$(e).data('nama');
        var go_to = p[0]['dataset']['id'] ? p[0]['dataset']['id'] : p[0]['parentElement']['dataset']['id'];

        console.log(e);
        console.log(p);
        console.log(go_to);
        var nama_room = 'Room '+go_to;
        if(go_to=='no_room'){
            nama_room = 'Tanpa Room';
            go_to = null;
        }
        else if(!go_to)
            return false;

        $('#status_pindah_room').append('<small class="font-green-jungle">'+(++count_status)+'. <i class="fa fa-check"></i> '+nama+' dipindahkan ke  '+nama_room+'</small><br>');
        console.log('Jemaah '+id);
        console.log('Dipindahkan ke Room '+go_to);
        $.ajax({
                method: 'POST',
                url: '<?php echo base_url() ?>administrator/room/set/'+id,
                data : {room : go_to},
                dataType: 'json',
                success : function(resp){
                    if(!go_to){
                        // location.reload();
                    } 
                }
            });
    }
    $(document).ready(function() {
        
        var temp_d = <?php echo $jemaah_d ?>;
        var temp_t = <?php echo $jemaah_t ?>;
        var temp_q = <?php echo $jemaah_q ?>;


        //Generate All Room
            for(var i=1;i<'<?php echo $max_d ?>';i++){
                addRoom('Double');
            }
           for(var i=1;i<'<?php echo $max_t ?>';i++){
                addRoom('Triple');
            }
           for(var i=1;i<'<?php echo $max_q ?>';i++){
                addRoom('Quad');
            }

        //Assign Jemaah Per Room
            for(var i=1;i<='<?php echo $max_d?>';i++){
                for(var j=0;j<temp_d.length;j++){
                    
                    if(temp_d[j]['room_count'] == i){
                        $('#Double_'+i+' > .dd-empty').remove();
                        $('#Double_'+i).append(temp_d[j]['content']); 
                    }
                } 
            }
            for(var i=1;i<='<?php echo $max_t?>';i++){
                for(var j=0;j<temp_t.length;j++){
                    if(temp_t[j]['room_count'] == i){
                        $('#Triple_'+i+' > .dd-empty').remove();
                        $('#Triple_'+i).append(temp_t[j]['content']); 
                    }
                } 
            }
            for(var i=1;i<='<?php echo $max_q?>';i++){
                for(var j=0;j<temp_q.length;j++){
                    if(temp_q[j]['room_count'] == i){
                        $('#Quad_'+i+' > .dd-empty').remove();
                        $('#Quad_'+i).append(temp_q[j]['content']); 
                    }
                } 
            }
        
        
        $('#no_room').nestable({
            group: 1,
            beforeDragStop: function(l,e,p) {updateRoom(e,p);}
        });

        $('#Quad_1').nestable({
            group : 1,
            beforeDragStop: function(l,e,p) {updateRoom(e,p);}
        });
        $('#Triple_1').nestable({
            group : 1,
            beforeDragStop: function(l,e,p) {updateRoom(e,p);}
        });
        $('#Double_1').nestable({
            group : 1,
            beforeDragStop: function(l,e,p) {updateRoom(e,p);}
        });                
    });
</script>
<link href="<?php echo base_url()?>assets/backend/global/plugins/jquery-nestable/jquery.nestable.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery-nestable/jquery.nestable.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url()?>assets/backend/pages/scripts/ui-nestable.min.js" type="text/javascript"></script> -->
<style type="text/css">
    .tr-center{text-align: center}
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Room Management
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>

        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo base_url().'administrator' ?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Room Management</span>
            </li>
        </ul>
        <?php  if($show_detail) {?>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <h4>
                                <i class="fa fa-plane"></i> Keberangkatan <?php echo '<b>'.$kota_keberangkatan.'</b> tanggal <b>'.$tgl_keberangkatan.'</b>' ?> 
                                <br><small><b class="font-red">Catatan :</b> Arahkan pointer ke nama jemaah untuk melihat kelas yang dipilih saat mendaftar</small>
                            </h4>
                        </div>

                        <div class="col-md-6">
                            <div class="col-md-12">
                                <h4 class="pull-right">
                                     <b><?php echo $total_room[0]->total_d ?></b> Double |
                                     <b><?php echo $total_room[0]->total_t ?></b> Triple |
                                     <b><?php echo $total_room[0]->total_q ?></b> Quad | 
                                      <a href="<?php echo base_url().'/administrator/room/export/'.$keberangkatan_id.'/'.$kota_keberangkatan_id ?>" class="btn btn-success btn-sm" type="button" ><i class="fa fa-file"></i> Export To Excel</a>
                                </h4>
                                <b class="font-red">Status :</b>

                                <div id="status_pindah_room"></div>
                            </div>
                        <br>
                        </div>
                        <div class="clearfix"></div>
                        <br>
                        <div class="col-md-3">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-bubble font-green"></i>
                                        <span class="caption-subject font-green sbold uppercase">List Jemaah Tanpa Room</span>
                                    </div>
                                </div>
                                <div class="portlet-body" style="min-height:120px;max-height: 600px;overflow-y: scroll;padding-right: 5px">
                                    <div class="dd" id="no_room" data-id="no_room">
                                    <?php 
                                        
                                        foreach($jemaah_room as $row){ 
                                            $id = encrypt($row->pendaftarandetail_id);
                                            $sex = $row->jenis_kelamin == 'Pria' ? '<b class="font-dark">M</b>' : '<b class="font-red">F</b>';
                                            $sharing_bed = $row->sharing_bed>0 ? '<a class="tooltips" data-placement="top" title="Jemaah Sharing Bed"><i class="fa fa-bed"></i></a>|' : '';
                                            $infant = $row->infant>0 ? '<a class="tooltips font-yellow-crusta" data-placement="top" title="Jemaah Infant"><i class="fa fa-child"></i></a>|' : '';
                                            $tourled     = $row->jenis_jemaah=='Tour Leader' ? '<a class="tooltips font-yellow" data-placement="top" title="Tour Leader"><i class="fa fa-trophy"></i></a> |' : '';
                                            $atribut     = $sex.' | '.$sharing_bed.$infant.$tourled;

                                    ?>
                                        <li class="dd-nochildren dd-item tooltips" data-nama="<?php echo $row->nama_jemaah ?>"  data-placement="bottom" data-id="<?php echo $id ?>" title="<?php echo '['.$row->nama_paket.'] '.$row->nama_jemaah.' memilih kelas '.$row->kelas ?>">
                                            <div class="dd-handle"  style="text-overflow: ellipsis;overflow: hidden;white-space: nowrap;">
                                                <label><?php echo $atribut.$row->nama_jemaah?></label><label class="pull-right"></label>
                                            </div>
                                        </li>
                                    <?php } ?>
                                </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-9">

                            <div class="col-md-4">
                                <button class="btn btn-xs green-jungle" onClick="addRoom('Double')"><i class="fa fa-plus"></i> Add Double Room</button>
                                <div id="Double_list">
                                    <div class="portlet light bordered">
                                        <div class="portlet-body">
                                            <span class="caption-subject font-green-jungle sbold uppercase">Double 1 (D1)</span>
                                   
                                            <div class="dd" id="Double_1" data-id="D1"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <button class="btn btn-xs purple" onClick="addRoom('Triple')"><i class="fa fa-plus"></i> Add Triple Room</button>
                                <div id="Triple_list">
                                    <div class="portlet light bordered">
                                        <div class="portlet-body">
                                            <span class="caption-subject font-purple sbold uppercase">Triple 1 (T1)</span>
                                   
                                            <div class="dd" id="Triple_1" data-id="T1"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <button class="btn btn-xs red" onClick="addRoom('Quad')"><i class="fa fa-plus"></i> Add Quad Room</button>
                                <div id="Quad_list">
                                    <div class="portlet light bordered">
                                        <div class="portlet-body">
                                            <span class="caption-subject font-red sbold uppercase">Quad 1 (Q1)</span>
                                         
                                            <div class="dd" id="Quad_1" data-id="Q1"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        <?php } else { ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-plane font-black"></i>
                                        <span class="caption-subject font-black sbold uppercase">List Keberangkatan Jemaah</span>
                                    </div>
                                
                                </div>
                                <div class="portlet-body">
                                    
                                    <table class="table table-striped table-bordered  table-checkable order-column" id="table-custom">
                                        <thead>
                                            <tr>
                                                <th> No. </th>
                                                <th> Nama Paket</th>
                                                <th> Tanggal Berangkat</th>
                                                <th> Kota Keberangkatan</th>
                                                <th> Total Jemaah</th>
                                                <th> Aksi </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php   $count=0;
                                                foreach($list_keberangkatan as $row){
                                                    $id_keberangkatan = encrypt($row->keberangkatan_id);
                                                    $id_kota_keberangkatan = encrypt($row->kota_keberangkatan);
                                        ?>
                                            <tr>
                                                <td><?php echo ++$count; ?></td>
                                                <td><?php echo $row->nama_paket?></td>
                                                <td><i class="fa fa-calendar"></i> <?php echo date('d-F-Y',strtotime($row->tgl_keberangkatan))?></td>
                                                <td><?php echo $row->nama_kota?></td>
                                                <td><i class="fa fa-users"></i> <?php echo $row->total_jemaah?> Jemaah</td>
                                                <td>
                                                    <?php if($row->total_jemaah > 0){ ?>
                                                    <div class="actions">
                                                        <div class="btn-group">
                                                            <button type="button" onClick="window.location='<?php echo base_url().'administrator/room/show_detail/'.$id_keberangkatan.'/'.$id_kota_keberangkatan ?>'" class="btn btn-xs green">Cek Room &nbsp;<i class="fa fa-search"></i></button>
                                                        </div>
                                                    </div>
                                                <?php } else {echo '-';}?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        </div>
    </div>
</div>

