<?php 
    $DOM = [    'id',
                'judul',
                'keterangan',
                'author',
                'tgl_post',
                'urutan',
                'url_thumbnail',
                'link_slide'
            ];
?>
<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<script type="text/javascript">
    var dom = [ 'id',
                'judul',
                'keterangan',
                'author',
                'tgl_post',
                'urutan',
                'url_thumbnail',
                'link_slide'
            ];

    function cek_all(){
        a= $('#nama').val();
        // b= $('#author').val();
        c= $('#tgl_post').val();
        d= $('#keterangan').val();
        // e= $('#alt_teks').val();
        e= $('#judul').val();
        if (document.getElementById("slideshow").files.length == 0) {
            alert("Please Select Image");
            document.getElementById("slideshow").click();
            exit();
        }
        if(a==''||c==''||d==''||e==''){
            alert("Data belum lengkap");
            exit();
        }
    }

    function reset_all(){
        for(var i=0;i<dom.length;i++){
                $('#'+dom[i]).val("");
                $('#'+dom[i]).removeClass('edited');
            }
    }

    function add_function(task){
        if(task == 'show'){
            reset_all();
            //Change Modal Header Title
            $('.add_header').removeClass('hide');
            $('.edit_header').addClass('hide');
            
            $('.btn-add').removeClass('hide');
            $('.btn-update').addClass('hide');

            //Image Thumbnail
            var link = 'http://www.novelupdates.com/img/noimagefound.jpg';
            $('#slideshow_preview').attr('src',link);
            $('#add_edit_modal').modal();
        }

        if(task == 'add'){
            cek_all();
            $('#add_edit_modal').modal('hide');
            var file = document.getElementById('slideshow').files[0];
            if (file) {
                var file_data = $('#slideshow').prop('files')[0];  
                var data = new FormData();    
                console.log(data);              
                data.append('slideshow', file_data); 
                data.append('judul',$('#judul').val());
                data.append('author','Administrator');
                data.append('keterangan',$('#keterangan').val());
                data.append('tgl_post',$('#tgl_post').val());
                data.append('link_slide',$('#link_slide').val());
                data.append('urutan',$('#urutan     ').val());
            }
            
                swal({
                      title: "Meng-upload File",
                      text: "Klik 'Upload' untuk memulai.",
                      type: "info",
                      closeOnConfirm: false,
                      confirmButtonText: 'Upload',
                      showLoaderOnConfirm: true,
                    },
                    function(){
                        $.ajax({
                            method: 'POST',
                            data:data,
                            url: '<?php echo base_url() ?>administrator/slideshow/add',
                            dataType: 'text',
                            cache: false,
                            contentType: false,
                            processData: false,
                            success : function(resp){
                                if(resp == '"add_success"'){
                                    swal({
                                          title: "Tambah Slideshow  ",
                                          text: "Penambahan berhasil",
                                          type: "success",
                                          confirmButtonClass: "btn-danger",
                                          confirmButtonText: "Ok",
                                        },
                                        function(){
                                            location.reload();
                                        });
                                }
                                if(resp == '"add_fail"'){
                                    swal({
                                          title: "Penambahan Gagal !",
                                          text: "File Tidak Sesuai atau Ukuran Melebihi 2 MB",
                                          type: "warning",
                                          confirmButtonClass: "btn-danger",
                                          confirmButtonText: "Ok",
                                        });
                                }
                            }
                        });
                    });
        }
    }

    function edit_function(task='',count=''){
        if(task == 'show'){
            reset_all();
            $('#add_edit_modal').modal();

            //Change Modal Header Title
                $('.add_header').addClass('hide');
                $('.edit_header').removeClass('hide');

            $('.btn-add').addClass('hide');
            $('.btn-update').removeClass('hide');

            //Image Thumbnail
                var link = '<?php echo base_url() ?>'+$('#url_thumbnail_edit_'+count).val();
                $('#slideshow_preview').attr('src',link);

            //Memasukkan data yang hidden ke inputnya masing masing pada modal add_edit_modal
                for(var i=0;i<dom.length;i++){
                    $('#'+dom[i]).val($('#'+dom[i]+'_edit_'+count).val());
                    $('#'+dom[i]).addClass('edited');
                }
            //set count ke berapa di modal
                $('#count').val(count);
        }

        if(task == 'update'){
            count = $('#count').val();
            id = $('#id_edit_'+count).val();
            // cek_all();
            $('#add_edit_modal').modal('hide');
            var data = new FormData();    
            var file = document.getElementById('slideshow').files[0];
            if (file) {
                var file_data = $('#slideshow').prop('files')[0];  
                data.append('slideshow', file_data); 
            }
                data.append('judul',$('#judul').val());
                data.append('author',$('#author').val());
                data.append('keterangan',$('#keterangan').val());
                data.append('tgl_post',$('#tgl_post').val());
                data.append('link_slide',$('#link_slide').val());
                data.append('urutan',$('#urutan').val());
            
            
                swal({
                      title: "Memperbaharui Slideshow",
                      text: "Klik 'Update' untuk memulai.",
                      type: "info",
                      closeOnConfirm: false,
                      confirmButtonText: 'Update',
                      showLoaderOnConfirm: true,
                    },
                    function(){
                        $.ajax({
                            method: 'POST',
                            data:data,
                            url: '<?php echo base_url() ?>administrator/slideshow/update/'+id,
                            dataType: 'text',
                            cache: false,
                            contentType: false,
                            processData: false,
                            success : function(resp){
                                if(resp == '"update_success"'){
                                    swal({
                                          title: "Update Slideshow  ",
                                          text: "Pembaharuan berhasil",
                                          type: "success",
                                          confirmButtonClass: "yellow-crusta",
                                          confirmButtonText: "Ok",
                                        },
                                        function(){
                                            location.reload();
                                        });
                                }
                                if(resp == '"update_fail"'){
                                    swal({
                                          title: "Pembaharuan Gagal !",
                                          text: "File Tidak Sesuai atau Ukuran Melebihi 2 MB",
                                          type: "warning",
                                          confirmButtonClass: "btn-danger",
                                          confirmButtonText: "Ok",
                                        });
                                }
                            }
                        });
                    });
        }
        

        if(task == 'delete'){
            swal({
              title: "Hapus Data?",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "grey",
              confirmButtonText: "Yes, delete it!",
              closeOnConfirm: false
            },
            function(){
                id = $('#id_edit_'+count).val();
                $.ajax({
                    method: 'POST',
                    url: '<?php echo base_url() ?>administrator/slideshow/delete/'+id,
                    dataType: 'json',
                    success : function(resp){
                        if(resp == 'update_success')
                            $('#add_edit_modal').modal('hide');
                            swal({
                                  title: "Hapus Data",
                                  text: "Penghapusan berhasil",
                                  type: "success",
                                  confirmButtonClass: "grey",
                                  confirmButtonText: "Ok",
                                },
                                function(){
                                    location.reload();
                                });
                            
                    }
                });
            });
        }
    }

</script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Slideshow
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="<?php echo base_url().'administrator' ?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Slideshow</span>
        </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">
    <!-- List Slideshow -->
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <button type="button" onClick="add_function('show')" class="btn btn-sm red">Tambah Slideshow &nbsp;<i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-layers font-dark"></i>
                                <span class="caption-subject font-dark bold uppercase">Slideshow</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="mt-element-card mt-element-overlay">
                                <div class="row">
                                    <?php 
                                        
                                        $count=1;$div_row=0;
                                        foreach($slide as $row){
                                            $id=encrypt($row->slide_id);
                                            $judul  = $row->judul;
                                            $keterangan = $row->keterangan;
                                            $author = $row->author;
                                            $tgl_post = date('d-m-Y',strtotime($row->tgl_post));
                                            $urutan = $row->urutan;
                                            $url_thumbnail = $row->url_thumbnail;
                                            $url_slide = $row->url_slide;
                                            $link_slide = 'javascript:;';
                                            if($row->link_slide)
                                                $link_slide = $row->link_slide;

                                            $count++;

                                            
                                            $div_row++;
                                            if($div_row==1) echo '<div class="col-md-12">';
                                    ?>
                                        <div class="col-md-3">
                                            <div class="mt-card-item">
                                                <div class="mt-card-avatar mt-overlay-1">
                                                    <img src="<?php echo base_url().$url_thumbnail ?>" />
                                                    <div class="mt-overlay">
                                                        <ul class="mt-info">
                                                            <li>
                                                                <a class="btn default btn-outline" target="_blank" href="<?php echo base_url().$url_slide;?>">
                                                                    <i class="icon-magnifier"></i>
                                                                </a>
                                                            </li>
                                                            <?php if($link_slide!='javascript:;'){?>
                                                            <li>
                                                                <a class="btn default btn-outline" href="<?php echo $link_slide;?>">
                                                                    <i class="icon-link"></i>
                                                                </a>
                                                            </li>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="mt-card-content">
                                                    <h3 class="mt-card-name"><?php echo $judul; ?></h3>
                                                    <div class="mt-card-social">
                                                        <ul>
                                                            <li>
                                                                 <button type="button" onClick="edit_function('show',<?php echo $count ?>);" class="btn btn-xs yellow-crusta">Edit &nbsp;<i class="fa fa-pencil"></i></button>
                                                            </li>
                                                            <li>
                                                                <button type="button" onClick="edit_function('delete',<?php echo $count ?>);" class="btn btn-xs grey">Hapus &nbsp;<i class="fa fa-trash"></i></button>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="hide">
                                                    <!-- Mencetak Input hidden untuk data detail -->
                                                    <?php
                                                        for($i=0;$i<count($DOM);$i++){
                                                            $input_id = $DOM[$i].'_edit_'.$count; 
                                                            if($DOM[$i]=='id')
                                                                $value_id = $id;
                                                            else
                                                                $value_id = $row->$DOM[$i];
                                                      ?>
                                                        <input type="text" id="<?php echo $input_id?>" value="<?php echo $value_id ?>">
                                                    <?php } ?>
                                                        <input type="text" id="url_thumbnail_edit_<?php echo $count ?>" value="<?php echo $url_thumbnail ?>">
                                                </div>
                                            </div>
                                        </div>
                                                
                                    <?php if($div_row == 4) {echo '</div>';$div_row=0;} } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<!-- add Modal-->
<div class="modal fade" id="add_edit_modal"  role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="add_header caption">
                    <i class=" icon-plus font-red"></i>
                    <span class="caption-subject font-red sbold uppercase">Tambah Slideshow</span>
                </div>
                <div class="edit_header caption hide">
                    <i class=" icon-pencil font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">Edit Slideshow</span>
                </div>
            </div>
            <div class="portlet-body form">
                <?php echo form_open('administrator', array( 'id' => 'add-form')); ?>
                    <div class="row">
                        <div class="col-md-5">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px;">
                                    <img id="slideshow_preview" alt=""> </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>
                                <div>
                                    <span class="btn default btn-file">
                                        <span class="fileinput-new"> Select image </span>
                                        <span class="fileinput-exists"> Change </span>
                                        <input type="file" name="slideshow" id="slideshow" required> </span>
                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <label for="form_control_1">Judul</label>
                                <input type="text" class="form-control" name="judul" id="judul">
                            </div>
                            <div class="form-group form-md-line-input form-md-floating-label input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
                                <input type="text" class="form-control" name="tgl_post" id="tgl_post" readonly="" placeholder="Tanggal Posting">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                            <div class="form-group">
                                <span class="label label-danger">Note!</span> 
                                <h5><i>Gunakan Image dengan ukuran maximal 1920x760. Slideshow akan tampil sesuai dengan tanggal posting</i></h5>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="form_control_1">Keterangan</label>
                                <textarea type="text" rows="1" class="form-control" name="keterangan" id="keterangan" ></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <span class="label label-danger">Note!</span> 
                            <h5><i>Isi Link slideshow jika menginginkan slideshow mengarahkan user ke suatu halaman</i></h5>
                            <div class="form-group">
                                <label for="form_control_1">Link Slideshow (optional)</label>
                                <textarea type="text" rows="1" class="form-control" name="link_slide" id="link_slide" ></textarea>
                            </div>
                             
                        </div>
                    </div>
                    
                <?php echo form_close();?>
            </div>
            <div class="modal-footer">
                <input type="hidden" id="count">
                <button type="button" data-dismiss="modal" class="btn btn-circle grey">Cancel &nbsp;<i class="fa fa-times"></i></button>            
                <button type="button" onClick="add_function('add')" class="btn-add btn btn-circle red">Simpan &nbsp;<i class="fa fa-save"></i></button>
                <button type="button" onClick="edit_function('update');" class="btn-update btn btn-circle yellow-crusta    ">Simpan &nbsp;<i class="fa fa-pencil"></i></button>
            </div>
        </div>

    </div>
</div>