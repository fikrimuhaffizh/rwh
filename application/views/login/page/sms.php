<?php $login_type = $this->session->userdata('login_type') ?>
<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/js/sms-counter.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js" type="text/javascript"></script>
<script type="text/javascript">

    $(document).ready(function(){
        $('#message').countSms('#sms-counter');
        var rows_selected = [];
        var rowsPerjemaahId_selected = [];
        var rowsPerKeberangkatanId_selected = [];
        table = $('#table_umroh').DataTable({ 
            "processing": true, "serverSide": true,"order": [],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "ajax": {"url": "<?php echo site_url('administrator/sms/pagination/umroh')?>","type": "POST"},
            "order": [[ 0, "desc" ]],
            'columnDefs': [
                {'targets': [0],'className': 'dt-body-center','searchable': false,'orderable': false,'width': '1%'},
                {"targets": [3],"className": "dt-center","orderable" : false},
                {"targets": [4],"className": "dt-center","orderable" : false}
            ],
            'order': [[1, 'asc']],
            'rowCallback': function(row, data, dataIndex){
               // Get row ID
               var rowsPerjemaahId_selected = data[0];

               // If row ID is in the list of selected row IDs
               if($.inArray(rowsPerjemaahId_selected, rows_selected) !== -1){
                  $(row).find('input[type="checkbox"]').prop('checked', true);
                  $(row).addClass('selected');
               }
            }
        }); 

        // Handle click on checkbox per jemaah
        $('#table_umroh tbody').on('click', 'input[type="checkbox"]', function(e){
          var $row   = $(this).closest('tr');
          var data   = table.row($row).data();
          var rowId  = data[0];
          var nama   = data[1];
          var no_hp  = data[3];
          var no_hp2 = data[4];

          // Determine whether row ID is in the list of selected row IDs 
          var index = $.inArray(rowId, rows_selected);

          // If checkbox is checked and row ID is not in list of selected row IDs
          if(this.checked && index === -1){
             $row.addClass('selected');
             rows_selected.push(rowId);
              var x = '<div id="'+no_hp+'">'
                    +   '<input type="hidden" name="no_hp[]" value="'+no_hp+'">'
                    +   '<label> - '+nama+' : '+no_hp+'</label>'
                    + '</div>';
              if(no_hp2 != '-'){
                    x+= '<div id="'+no_hp2+'">'
                    +    '<input type="hidden"  name="no_hp[]" value="'+no_hp2+'">'
                    +    '<label> - '+nama+' : '+no_hp2+'</label>'
                    + '</div>';
              }
            $("#tujuan").prepend(x);

          // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
          } else if (!this.checked && index !== -1){
             rows_selected.splice(index, 1);
             $row.removeClass('selected');
             $('#'+no_hp).remove();
             $('#'+no_hp2).remove();
          }

          // Prevent click event from propagating to parent
          e.stopPropagation();
        });

       table2 = $('#table_keberangkatan').DataTable({ 
            "processing": true, "serverSide": true,"order": [],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "ajax": {"url": "<?php echo site_url('administrator/sms/paginationTglKeberangkatan')?>","type": "POST"},
            "order": [[ 3, "desc" ]],
            "columnDefs": [
                        {"targets": [0],"className": "dt-center"},
                        {"targets": [2],"visible": false},
                        {"targets": [3],"className": 'dt-center'},
                        {"targets": [4],"visible": false},
                        {"targets": [5],"className": 'dt-center'},
                        {"targets": [6],"visible": false},
                        {"targets": [7],"visible": false},
                    ],
            'rowCallback': function(row, data, dataIndex){
               // Get row ID
               var rowsPerKeberangkatanId_selected = data[0];

               // If row ID is in the list of selected row IDs
               if($.inArray(rowsPerKeberangkatanId_selected, rows_selected) !== -1){
                  $(row).find('input[type="checkbox"]').prop('checked', true);
                  $(row).addClass('selected');
               }

                if (data[5] == "0 Jemaah") {
                    $(row).find('input[type="checkbox"]').attr('disabled', true);
                    $(row).css('backgroundColor', '#eee');
             
                }
            }
        });

       // Handle click on checkbox
       $('#table_keberangkatan tbody').on('click', 'input[type="checkbox"]', function(e){
          var $row              = $(this).closest('tr');
          var data              = table2.row($row).data();
          console.log(data);
          var rowId             = data[0];
          var paket             = data[1];
          var keberangkatan_id  = data[2];
          var tgl_keberangkatan = data[3];
          var index             = $.inArray(rowId, rows_selected);

          // If checkbox is checked and row ID is not in list of selected row IDs
          if(this.checked && index === -1){
             rows_selected.push(rowId);
             $row.addClass('selected');
              var x = '<div id="'+keberangkatan_id+'">'
                    +    '<input type="hidden" id="" name="keberangkatan_id[]" value="'+keberangkatan_id+'">'
                    +    '<label> - '+paket+', keberangkatan : '+tgl_keberangkatan+'</label>'
                    + '</div>';

            $("#tujuan").prepend(x);

          // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
          } else if (!this.checked && index !== -1){
             rows_selected.splice(index, 1);
             $row.removeClass('selected');
             $('#'+keberangkatan_id).remove();
          }

          // Prevent click event from propagating to parent
          e.stopPropagation();
       });

       $('#no_custom').tagsinput({
          maxChars: 12,
          minChars:10
        });

       $('#no_custom').on('itemAdded', function(event) {
            var x = '<div id="'+event.item+'">'
                  +    '<input type="hidden" name="no_hp[]" value="'+event.item+'">'
                  +    '<label> - No Hp. '+event.item+'</label>'
                  +'</div>'
            $("#tujuan").prepend(x);
      });

      $('#no_custom').on('itemRemoved', function(event) {
        $('#'+event.item).remove();
      });

      $('#btn-kirim').on('click',function(e){
        $.ajax({
            method: 'POST',
            url: '<?php echo base_url().'administrator/sms/send' ?>',
            data : $("#form-sms").serialize(),
            beforeSend: function() {
                $('.div-gagal').hide();
                $('.div-berhasil').hide();
                $('.loading').show();
                $('.status_gagal').empty();
                $('.status_berhasil').empty();
            },
            dataType: 'json',
            success : function(resp){
              $('.loading').hide();
              if(resp['error']){
                $('.div-gagal').show();
                for(var i =0;i<resp['error'].length;i++){

                $('.status_gagal').append('- '+resp['error'][i]+'<br>');
                }
              } else {
                $('.div-berhasil').show();
                $('.status_berhasil').append(resp['success']);
              }
            }
        });
      });        
    });

</script>
<style type="text/css">
    .tr-center{text-align: center}
    .bootstrap-tagsinput{width:100%;}
    table.dataTable td.sorting_1, table.dataTable td.sorting_2, table.dataTable td.sorting_3, table.dataTable th.sorting_1, table.dataTable th.sorting_2, table.dataTable th.sorting_3{background: #ffffff00!important;}
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>SMS Blast</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>

        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo base_url().'administrator' ?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">SMS Blast</span>
            </li>
        </ul>
            
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-envelope font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">SMS Blast Jemaah</span>
                                    </div>
                                
                                </div>
                                <div class="portlet-body">
                                            

                                            <script>
                                                $('#message').countSms('#sms-counter');
                                            </script>
                                        <div class="tabbable-custom nav-justified">
                                            <ul class="nav nav-tabs nav-justified">
                                                <li class="active">
                                                    <a href="#tab_1_1_1" data-toggle="tab" aria-expanded="true"><i class="fa fa-plane"></i> Per Tanggal Keberangkatan </a>
                                                </li>
                                                <li class="">
                                                    <a href="#tab_1_1_2" data-toggle="tab" aria-expanded="false"><i class="fa fa-user"></i> Per Jemaah</a>
                                                </li>
                                                <li class="">
                                                    <a href="#tab_1_1_3" data-toggle="tab" aria-expanded="false"><i class="fa fa-gears"></i> Custom </a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="tab_1_1_1">
                                                    <table class="table table-condensed table-responsive" id="table_keberangkatan">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Paket</th>
                                                                <th style="text-align: center">Bulan Keberangkatan</th>
                                                                <th style="text-align: center">Tanggal Keberangkatan</th>
                                                                <th style="text-align: center">Kuota</th>
                                                                <th style="text-align: center">Seat Terisi</th>                                                    
                                                                <th style="text-align: center">Sisa Seat</th>
                                                                <th style="text-align: center">Detail</th>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <div class="tab-pane" id="tab_1_1_2">
                                                    <table class="table table-bordered table-hover" id="table_umroh">
                                                        <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th> Nama</th>
                                                                <th> Paket </th>
                                                                <th> No HP 1 </th>
                                                                <th> No HP 2 </th>
                                                                <th>No Pendaftaran</th>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="tab-pane" id="tab_1_1_3">
                                                    <h5>Ketik nomor tujuan lalu tekan Enter :</h5>
                                                      
                                                    <input type="text" value="" id="no_custom"> 
                                                       
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                </div>
                                <div class="portlet-body">
                                      <?php echo form_open("#",array('id'=>'form-sms')) ?>
                                        <span>Silahkan ketik Isi SMS yang akan dikirim: </span>
                                        <p><textarea class="form-control" name="isi_sms" id="message" ></textarea></p>
                                        <div id="sms-counter">
                                            <p>Length: <b class="length"></b> | Messages: <b class="messages"></b> | Remaining: <b class="remaining"></b></p>
                                        </div>
                                        <p><button class="btn btn green-jungle pull-right" id="btn-kirim" type="button"><i class="fa fa-send"></i> Kirim</button></p>
                                        <div class="loading display-none pull-left">
                                            <img src="<?php echo base_url().'assets/frontend/img/AjaxLoader.gif' ?>" alt="">
                                            <span>Mohon Tunggu...</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr>

                                      
                                          <div class="alert alert-danger div-gagal display-none"><i class="fa fa-times"></i> Ada kegagalan dalam mengirim SMS :
                                            <div class="status_gagal">
                                            </div>
                                          </div>
                                          <div class="alert bg-green-jungle font-white div-success div-berhasil display-none"><i class="fa fa-check"></i> 
                                            <span class="status_berhasil">
                                              SMS Berhasil di kirim ke 900 nomor 
                                            </span>
                                          </div>
                                    
                                        <p><strong>Dikirim ke : </strong></p>
                                        <div id="tujuan"></div>
                                      <?php echo form_close();?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>






