<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>

<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>

<?php include 'global_function.php';$login_type = $this->session->userdata('login_type'); ?>

<script type="text/javascript">

    function add_function(task){

        if(task == 'show'){

            $('#add_modal').modal();

        }

        if(task == 'add'){

            $('#add_modal').modal('hide');

            data = new FormData($('#add-form')[0]);

            url = '<?php echo base_url() ?>administrator/syarat_umroh/add';

            AjaxCRUD('add',data,url);

        }

        if(task == 'setDefaultSyarat'){

            $('#set_default_modal').modal('hide');

            data = new FormData($('#setDefaultSyarat-form')[0]);

            url = '<?php echo base_url() ?>administrator/syarat_umroh/setDefaultSyarat/';

            AjaxCRUD('update',data,url);

        }        

    }



    function edit_function(task='',id='',resp=''){

        if(task == 'edit'){

            $('.data_input'+id).removeClass('hide');

            $('.data_text'+id).addClass('hide');



            $('.btn_save'+id).removeClass('hide');

            $('.btn_cancel'+id).removeClass('hide');



            $('.btn_edit'+id).addClass('hide');

            $('.btn_hapus'+id).addClass('hide');

        }



        if(task == 'cancel'){

            $('.data_input'+id).addClass('hide');

            $('.data_text'+id).removeClass('hide');



            $('.btn_save'+id).addClass('hide');

            $('.btn_cancel'+id).addClass('hide');



            $('.btn_edit'+id).removeClass('hide');

            $('.btn_hapus'+id).removeClass('hide');

        }



        if(task == 'update'){

            data = new FormData($('#edit-form'+id)[0]);

            data.append('syarat',$('#syarat_'+id).val());

            data.append('tipe',$('#tipe_'+id).val());

            url = '<?php echo base_url() ?>administrator/syarat_umroh/update/'+id;

            AjaxCRUD('update',data,url);

        }



        if(task == 'delete'){

            url = '<?php echo base_url() ?>administrator/syarat_umroh/delete/'+id;

            AjaxCRUD('delete',"",url);

        }



        if(task == 'show_setDefault'){

            url = '<?php echo base_url() ?>administrator/syarat_umroh/get_all_syarat/';

            AjaxCRUD('edit',"",url);

        }

        if(task == 'show_setDefault_haji'){

            url = '<?php echo base_url() ?>administrator/syarat_umroh/get_all_syarat_haji/';

            AjaxCRUD('edit',"",url);

        }

        if(task == 'process_respon'){

            $('.add_list_syarate').remove();

            $('.form-modal').addClass('form-md-floating-label');

            var pos = 0;

            var jenis;

            for(var i=0;i<resp.length;i++){

                    var status = "";

                    

                    if(resp[i]['aktif']==1)

                            status = 'checked'

                    var x ='        <div class="add_list_syarate">';



                        x+='                    <input type="checkbox" name="syarat_'+resp[i]['syaratumroh_id']+'" id="syarat_'+pos+'" class="md-check "'+status+'>';

                        x+='                        <label for="syarat_'+pos+'">';

                        x+='                            <span class="box"></span> '+resp[i]['persyaratan'];

                        x+='                        </label>';

                        x+='                </div>';

                        $('#add_list_syarat').after(x);

                        $('#jenis').val(resp[i]['jenis']);

                        jenis=resp[i]['jenis'];

                        pos++;

                }



            if(jenis=='Haji'){

                $('#text-syarat').removeClass('font-green');

                $('#text-syarat').addClass('font-blue');



                $('#icon-syarat').removeClass('font-green');

                $('#icon-syarat').addClass('font-blue');



                $('#btn_update_default-syarat').removeClass('green');

                $('#btn_update_default-syarat').addClass('blue');



                $('#add_list_syarat').html('Tentukan <b class="font-blue"> Syarat Awal Jemaah</b> ketika mereka baru mendaftar <b>Haji</b>:');

            }

            else if(jenis=='Umroh'){

                $('#text-syarat').addClass('font-green');

                $('#text-syarat').removeClass('font-blue');



                $('#icon-syarat').removeClass('font-blue');

                $('#icon-syarat').addClass('font-green');



                $('#btn_update_default-syarat').removeClass('blue');

                $('#btn_update_default-syarat').addClass('green');





                $('#add_list_syarat').html('Tentukan <b class="font-green"> Syarat Awal Jemaah</b> ketika mereka baru mendaftar  <b>Umroh / Wisata Islam</b> :');

            }



            $('#set_default_modal').modal();

        }

    }



</script>

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">

    <!-- BEGIN CONTENT BODY -->

    <div class="page-content">

        <!-- BEGIN PAGE HEAD-->

        <div class="page-head">

            <!-- BEGIN PAGE TITLE -->

            <div class="page-title">

                <h1>Syarat Umroh

                </h1>

            </div>

            <!-- END PAGE TITLE -->

        </div>

        <!-- END PAGE HEAD-->

        <!-- BEGIN PAGE BREADCRUMB -->

        <ul class="page-breadcrumb breadcrumb">

            <li>

                <a href="<?php echo base_url().'administrator' ?>">Home</a>

                <i class="fa fa-circle"></i>

            </li>

            <li>

                <span class="active">Syarat</span>

            </li>

        </ul>

        <!-- END PAGE BREADCRUMB -->

        <!-- BEGIN PAGE BASE CONTENT -->



        <div class="row">

            <div class="col-md-12">

                <div class="table-toolbar">

                    <div class="row">

                        <div class="col-md-12">

                            <div class="btn-group btn-group-devided">

                                <button type="button" onClick="add_function('show')" class="btn btn-sm red">Tambah Syarat

                                    <i class="fa fa-plus"></i>

                                </button>

                                <button type="button" onClick="edit_function('show_setDefault')" class="btn btn-sm green">Set Default Syarat Umroh &nbsp;<i class="fa fa-bars"></i></button>

                                 <button type="button" onClick="edit_function('show_setDefault_haji')" class="btn btn-sm blue">Set Default Syarat Haji &nbsp;<i class="fa fa-bars"></i></button>

                            </div>

                            <br><br><button class="btn btn-xs green"><i class="fa fa-check"></i>&nbsp;Default</button> <button class="btn btn-xs blue"><i class="fa fa-check"></i>&nbsp;Default</button> : <label>Syarat yang otomatis menjadi syarat awal jemaah ketika baru mendaftar</label>

                        </div>

                    </div>

                </div>

                <!-- BEGIN EXAMPLE TABLE PORTLET-->

                <div class="portlet light bordered">

                    <div class="portlet-title">

                        <div class="caption font-dark">

                            <i class="icon-settings font-dark"></i>

                            <span class="caption-subject bold uppercase"> Managed Syarat</span>

                        </div>

                    </div>

                    <div class="portlet-body">

                            <ul class="nav nav-tabs">

                                <li class="active">

                                    <a href="#tab_1_1" data-toggle="tab"> Syarat Umroh / Wisata Islam </a>

                                </li>

                                <li>

                                    <a href="#tab_1_2" data-toggle="tab"> Syarat Haji </a>

                                </li>

                            </ul>

                            <div class="tab-content">

                                <div class="tab-pane fade active in" id="tab_1_1">

                                    <table class="table table-striped table-bordered  table-checkable order-column" id="table-custom">

                                        <thead>

                                            <tr>

                                                <th> NO</th>

                                                <th> Syarat Umroh / Wisata Islam</th>

                                                <th> Tipe </th>

                                                <th> Status </th>

                                                <th> Aksi</th>

                                            </tr>

                                        </thead>

                                        <tbody>

                                        <?php if($syarat > 0){ 

                                                $count=0;

                                                foreach($syarat as $row){

                                                    $count++;

                                                    $id=encrypt($row->syaratumroh_id);

                                        ?>

                                                <?php echo form_open('administrator', array( 'id' => 'edit-form'.$id)); ?>

                                                <tr>

                                                    <td><?php echo $count; ?></td>

                                                    <td>

                                                        <div style="margin:0px;padding:0px" class="data_input<?php echo $id ?> hide form-group form-md-line-input">

                                                            <input type="text" class="form-control" name="syarat_<?php echo $id ?>" id="syarat_<?php echo $id ?>" value="<?php echo $row->persyaratan ?>">

                                                            <label for="syarat"></label>



                                                        </div>

                                                        <span class="data_text<?php echo $id ?>"><?php echo $row->persyaratan ?></span>     

                                                    </td>

                                                    <td>

                                                        <div style="margin:0px;padding:0px" class="data_input<?php echo $id ?> hide form-group form-md-line-input">

                                                            <select class="form-control" name="tipe_<?php echo $id ?>" id="tipe_<?php echo $id ?>" >

                                                                <option value="Upload" <?php echo $row->tipe=='Upload' ? 'selected':'' ?>>Upload</option>

                                                                <option value="Non-Upload" <?php echo $row->tipe=='Non-Upload' ? 'selected':'' ?>>Non-Upload</option>

                                                            </select>



                                                        </div>

                                                        <span class="data_text<?php echo $id ?>"><?php echo $row->tipe ?></span> 

                                                    </td>

                                                    <td><?php echo $row->status_default ?></td>

                                                    <td>

     

                                                        <div class="btn-group" >

                                                            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions

                                                                <i class="fa fa-angle-down"></i>

                                                            </button>

                                                            <ul class="dropdown-menu" role="menu">

                                                                <li><a href="javascript:;" onClick="edit_function('update',<?php echo $id ?>);" class="btn_save<?php echo $id ?> hide"><i class="fa fa-save"></i>&nbsp;Save</a></li>

                                                                <li><a href="javascript:;" onClick="edit_function('cancel',<?php echo $id ?>);" class="btn_cancel<?php echo $id ?> hide"><i class="fa fa-times"></i>&nbsp;Cancel</a></li>

                                                                <li><a href="javascript:;" onClick="edit_function('edit',<?php echo $id ?>);" class="btn_edit<?php echo $id ?>"><i class="fa fa-pencil"></i>&nbsp;Edit</a></li>

                                                                <?php if($row->total==0){ ?>

                                                                <li><a href="javascript:;" onClick="edit_function('delete',<?php echo $id ?>);" class="btn_hapus<?php echo $id ?>"><i class="fa fa-trash"></i>&nbsp;Hapus</a></li>

                                                                <?php } ?>

                                                            </ul>

                                                        </div> 
                                          

                                                    </td>

                                                </tr>

                                                <?php } 

                                        } ?>

                                        <?php echo form_close();?>

                                        </tbody>

                                    </table>

                                </div>

                                <div class="tab-pane fade" id="tab_1_2">

                                    <table class="table table-striped table-bordered  table-checkable " id="table-custom">

                                        <thead>

                                            <tr>

                                                <th> NO</th>

                                                <th> Syarat Haji </th>

                                                <th> Tipe </th>

                                                <th> Status </th>

                                                <th> Aksi</th>

                                            </tr>

                                        </thead>

                                        <tbody>

                                        <?php if($syarat_haji > 0){ 

                                                $count=0;

                                                foreach($syarat_haji as $row){

                                                    $count++;

                                                    $id=encrypt($row->syaratumroh_id);

                                        ?>

                                                <?php echo form_open('administrator', array( 'id' => 'edit-form'.$id)); ?>

                                                <tr>

                                                    <td><?php echo $count; ?></td>

                                                    <td>

                                                        <div style="margin:0px;padding:0px" class="data_input<?php echo $id ?> hide form-group form-md-line-input">

                                                            <input type="text" class="form-control" name="syarat_<?php echo $id ?>" id="syarat_<?php echo $id ?>" value="<?php echo $row->persyaratan ?>">

                                                            <label for="syarat"></label>



                                                        </div>

                                                        <span class="data_text<?php echo $id ?>"><?php echo $row->persyaratan ?></span>     

                                                    </td>

                                                    <td>

                                                        <div style="margin:0px;padding:0px" class="data_input<?php echo $id ?> hide form-group form-md-line-input">

                                                            <select class="form-control" name="tipe_<?php echo $id ?>" id="tipe_<?php echo $id ?>" >

                                                                <option value="Upload" <?php echo $row->tipe=='Upload' ? 'selected':'' ?>>Upload</option>

                                                                <option value="Non-Upload" <?php echo $row->tipe=='Non-Upload' ? 'selected':'' ?>>Non-Upload</option>

                                                            </select>



                                                        </div>

                                                        <span class="data_text<?php echo $id ?>"><?php echo $row->tipe ?></span> 

                                                    </td>

                                                    <td><?php echo $row->status_default ?></td>

                                                    <td>

                                                    <?php if($login_type == 'Kantor Pusat'){ ?>

                                                        <div class="btn-group" >

                                                            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions

                                                                <i class="fa fa-angle-down"></i>

                                                            </button>

                                                            <ul class="dropdown-menu" role="menu">

                                                                <li><a href="javascript:;" onClick="edit_function('update',<?php echo $id ?>);" class="btn_save<?php echo $id ?> hide"><i class="fa fa-save"></i>&nbsp;Save</a></li>

                                                                <li><a href="javascript:;" onClick="edit_function('cancel',<?php echo $id ?>);" class="btn_cancel<?php echo $id ?> hide"><i class="fa fa-times"></i>&nbsp;Cancel</a></li>

                                                                <li><a href="javascript:;" onClick="edit_function('edit',<?php echo $id ?>);" class="btn_edit<?php echo $id ?>"><i class="fa fa-pencil"></i>&nbsp;Edit</a></li>

                                                                <?php if($row->total==0){ ?>

                                                                <li><a href="javascript:;" onClick="edit_function('delete',<?php echo $id ?>);" class="btn_hapus<?php echo $id ?>"><i class="fa fa-trash"></i>&nbsp;Hapus</a></li>

                                                                <?php } ?>

                                                            </ul>

                                                        </div> 

                                                    <?php } else { echo '-';}?>                                           

                                                    </td>

                                                </tr>

                                                <?php } 

                                        } ?>

                                        <?php echo form_close();?>

                                        </tbody>

                                    </table>

                                </div>

                            </div>



                    </div>

                </div>

                <!-- END EXAMPLE TABLE PORTLET-->

            </div>

        </div>

        <!-- END PAGE BASE CONTENT -->

    </div>

    <!-- END CONTENT BODY -->

</div>

<!-- END CONTENT -->



<!-- add Modal-->

<div class="modal fade" id="add_modal"  role="basic" aria-hidden="true" style="display: none;">

    <div class="modal-dialog">

        <div class="portlet light bordered">

            <div class="portlet-title">

                <div class="caption">

                    <i class=" icon-plus font-red"></i>

                    <span class="caption-subject font-red sbold uppercase">Tambah Syarat</span>

                </div>

            </div>

            <div class="portlet-body form">

                <?php echo form_open('administrator', array( 'id' => 'add-form')); ?>

                    <div class="row">

                        <div class="col-md-12">

                            <div class="form-group">

                                <label for="form_control_1">Syarat</label>

                                <input type="text" class="form-control" name="syarat" id="syarat" placeholder="ex. Kartu Keluarga">

                            </div>

                            <div class="col-md-5">

                            <div class="form-group form-md-radios">

                                <label>Jenis Syarat</label>

                                <div class="md-radio-inline">

                                    <div class="md-radio">

                                        <input type="radio" value="Umroh" id="jenis1" name="jenis" class="md-radiobtn" checked="">

                                        <label for="jenis1">

                                            <span class="inc"></span>

                                            <span class="check"></span>

                                            <span class="box"></span>Umroh / Wisata Islam</label>

                                    </div>

                                    <div class="md-radio">

                                        <input type="radio" value="Haji" id="jenis2" name="jenis" class="md-radiobtn">

                                        <label for="jenis2">

                                            <span></span>

                                            <span class="check"></span>

                                            <span class="box"></span>Haji</label>

                                    </div>

                                    <div class="md-radio">

                                        <input type="radio" value="all" id="jenis3" name="jenis" class="md-radiobtn">

                                        <label for="jenis3">

                                            <span></span>

                                            <span class="check"></span>

                                            <span class="box"></span>Umroh/Wisata Islam/Haji</label>

                                    </div>

                                </div>

                            </div>

                        </div>

                            <div class="col-md-5">

                                <div class="form-group form-md-radios">

                                    <label>Tipe Syarat</label>

                                    <div class="md-radio-inline">

                                        <div class="md-radio">

                                            <input type="radio" value="Upload" id="tipe1" name="tipe" class="md-radiobtn" checked="">

                                            <label for="tipe1">

                                                <span class="inc"></span>

                                                <span class="check"></span>

                                                <span class="box"></span>Upload</label>

                                        </div>

                                        <div class="md-radio">

                                            <input type="radio" value="Non-Upload" id="tipe2" name="tipe" class="md-radiobtn">

                                            <label for="tipe2">

                                                <span></span>

                                                <span class="check"></span>

                                                <span class="box"></span>Non-Upload</label>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                <?php echo form_close();?>

            </div>

            <div class="modal-footer">

                <button type="button" data-dismiss="modal" class="btn btn-circle grey">Cancel &nbsp;<i class="fa fa-times"></i></button>            

                <button type="button" onClick="add_function('add')" class="btn btn-circle red">Simpan &nbsp;<i class="fa fa-save"></i></button>

            </div>

        </div>



    </div>

</div>

<!-- add Syarat Modal-->

<div class="modal fade" id="set_default_modal"  role="basic" aria-hidden="true" style="display: none;">

    <div class="modal-dialog">

        <div class="portlet light bordered">

            <div class="portlet-title">

                <div class="caption">

                    <i id="icon-syarat" class=" fa fa-bars font-green"></i>

                    <span id="text-syarat" class="header-modal caption-subject font-green sbold uppercase">Set Default Persyaratan</span>

                </div>

            </div>

            <div class="portlet-body form" >

                <?php echo form_open('administrator', array( 'id' => 'setDefaultSyarat-form')); ?>

                    <div class="row">

                        <div class="col-md-12">

                        <h4 id="add_list_syarat"></h4>

                            

                        </div>

                    </div>

                    <input type="hidden" name="jenis" id="jenis">

                <?php echo form_close();?>

                <label class="alert alert-info"><i class="fa fa-exclamation-circle"></i>&nbsp;Perubahan syarat per jemaah tetap bisa dilakukan di menu "Syarat Jemaah"</label>

            </div>

            <div class="modal-footer">

                <input type="hidden" id="pendaftarandetail_id">

                <button type="button" data-dismiss="modal" class="btn btn-circle grey">Cancel &nbsp;<i class="fa fa-times"></i></button>            

                <button type="button" onClick="add_function('setDefaultSyarat')" id="btn_update_default-syarat" class="btn_update btn green">Set Syarat Default&nbsp;<i class="fa fa-save"></i></button>

            </div>

        </div>



    </div>

</div>