<?php 
     $img_link =base_url().'assets/frontend/img/noimagefound.jpg';
?>
<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/backend/global/plugins/ckeditor/ckeditor.js"></script>
<?php include 'global_function.php';$login_type = $this->session->userdata('login_type'); ?>
<script type="text/javascript">
    function add_function(task){
        if(task == 'show'){
            $('.caption-testimoni-text').remove();
            $('.caption-testimoni').after('<span class="caption-testimoni-text"><i class=" icon-plus font-red"></i><span class="caption-subject font-red sbold uppercase"> Tambah testimoni</span></span>');

            //Reset Form
                empty_form('add-form');
                CKEDITOR.instances.testimoni.setData("");  
                $('#testimoni_thumb').attr('src','<?php echo $img_link ?>');

            $('#edit_button').hide();    
            $('#add_button').show();                  
            $('#add_modal').modal();
        }
        
        else if(task == 'add'){
            if(check_empty()>0){
                return alert('Form Belum Lengkap');
            }

            var x = CKEDITOR.instances.testimoni.getData();
            if(x.length<200)
                return alert("Testimoni minimal 200 karakter");

            $('#add_modal').modal('hide'); 
            //Unutk CKEditor harus melalui update Elemenet agar terbaca ketika men-submit form melalui ajx
            CKEDITOR.instances.testimoni.updateElement();                
            data = new FormData($('#add-form')[0]);
            url = '<?php echo base_url() ?>administrator/testimoni/add';
            AjaxCRUD('add',data,url);
        }
    }
    function edit_function(task='',id='',resp=""){
        if(task == 'edit'){
            url = '<?php echo base_url() ?>administrator/testimoni/edit/'+id;
            AjaxCRUD('edit',"",url);
        }

        else if(task == 'update'){
            if(check_empty('update')>0){
                return alert('Form Belum Lengkap');
            }
            $('#add_modal').modal('hide');
            //Unutk CKEditor harus melalui update Elemenet agar terbaca ketika men-submit form melalui ajx
            CKEDITOR.instances.testimoni.updateElement();     
            data = new FormData($('#add-form')[0]);
            url = '<?php echo base_url() ?>administrator/testimoni/update';
            AjaxCRUD('update',data,url);
        }

        else if(task == 'delete'){
            url = '<?php echo base_url() ?>administrator/testimoni/delete/'+id;
            AjaxCRUD('delete',"",url);         
        }

        else if(task == 'process_respon'){
            $('.caption-testimoni-text').remove();
            $('.caption-testimoni').after('<span class="caption-testimoni-text"><i class=" icon-pencil font-yellow-crusta"></i><span class="caption-subject font-yellow-crusta sbold uppercase"> Update testimoni</span></span>');
            $('#nama_testimoni').addClass('edited').val(resp.dt[0]['nama_testimoni']);
            if(resp.dt[0]['media_id']){
                $('#testimoni_thumb').attr('src',resp.dt[0]['media_id']);
            }
            $('#testimoni_id').val(resp.dt[0]['testimoni_id']);
            $('#nama_lengkap').val(resp.dt[0]['nama_lengkap']);
            $('#asal_daerah').val(resp.dt[0]['asal_daerah']);
            $('#jabatan').val(resp.dt[0]['jabatan']);
            $('#tgl_post').val(resp.dt[0]['tgl_post']);
            CKEDITOR.instances.testimoni.setData(resp.dt[0]['testimoni']);

            $('#edit_button').show();    
            $('#add_button').hide();                     
            $('#add_modal').modal();
        }
    }

    function detail_function(task="",id="",resp=""){
        if(task=='process_respon'){
            $('#detail_nama').text(resp.dt[0]['nama_lengkap']);
            $('#detail_asal_daerah').text(resp.dt[0]['asal_daerah']);
            $('#detail_jabatan').text(resp.dt[0]['jabatan']);
            $('#detail_tgl_post').text(resp.dt[0]['tgl_post']);

            $('#detail_testimoni').html(resp.dt[0]['testimoni']);
            if(resp.dt[0]['media_id']){
                $('#detail_avatar').attr('src',resp.dt[0]['media_id']);
            }
            $('#detail_modal').modal();
        }

        else if(task=="detail") {
            url = '<?php echo base_url() ?>administrator/testimoni/edit/'+id;
            AjaxCRUD('detail',"",url);
        }
    }

    function check_empty(task=""){
        var empty = 0;
            $('input', '#add-form').each(function(){
                if($(this).val() == "" && $(this).attr('id') 
                    && $(this).attr('id')!="testimoni_id"
                    && $(this).attr('id')!="jabatan"){
                    empty++;
                    //Khusus ketika update, bagian testimoni_pic tidak perlu dicek
                    if(task=='update' && $(this).attr('id')=='testimoni_pic')
                        empty--;
                }
            })
        var x = CKEDITOR.instances.testimoni.getData();
        if(!x) empty++;
            
            return empty;
    }    
    $(document).ready(function(){
        CKEDITOR.replace( 'testimoni' );
    });
</script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Testimoni</h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="<?php echo base_url().'administrator' ?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Testimoni</span>
        </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">
    <!-- List testimoni -->
        <div class="col-md-12">
            <div class="table-toolbar">
                <div class="row">
                    <div class="col-md-6">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <button type="button" onClick="add_function('show')" class="btn btn-sm red">Tambah Testimoni &nbsp;<i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Managed Testimoni</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered  table-checkable order-column" id="table-custom">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>Nama</th>
                                <th>Asal Daerah</th>
                                <th>Jabatan</th>
                                <th>Tanggal Post</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if($testimoni > 0){ 
                                $count=0;
                                foreach($testimoni as $row){
                                    $count++;
                                    $id=encrypt($row->testimoni_id);
                                    $jabatan = '-';
                                    if($row->jabatan)
                                        $jabatan = $row->jabatan;
                            ?>
                            <tr>
                                <td><?php echo $count; ?></td> 
                                <td><?php echo $row->nama_lengkap ?></td>
                                <td><i class="fa fa-map-marker"></i>&nbsp;<?php echo $row->asal_daerah ?></td>
                                <td><?php echo $jabatan ?></td>
                                <td><i class="fa fa-calendar"></i>&nbsp;<?php echo date('d-M-Y',strtotime($row->tgl_post)) ?></td>
                                <td>
                                    <div class="btn-group" >
                                        <button style="width:100%" class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="javascript:;" onClick="detail_function('detail',<?php echo $id ?>);" ><i class="fa fa-search"></i>&nbsp;Detail</a></li>                                                                                   
                                            <li><a href="javascript:;" onClick="edit_function('edit',<?php echo $id ?>);"><i class="fa fa-pencil"></i>&nbsp;Edit</a></li>
                                            <li><a href="javascript:;" onClick="edit_function('delete',<?php echo $id ?>);"><i class="fa fa-trash"></i>&nbsp;Hapus</a></li>
                                        </ul>
                                    </div>

                                </td>
                            </tr>
                        <?php } 
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="portlet  bordered">     
                

            
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<!-- add Modal-->
<div class="modal fade" id="add_modal"  role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-testimoni"></span>
                </div>
            </div>
            <div class="portlet-body form">
                <?php echo form_open('administrator', array( 'id' => 'add-form')); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <label class="label label-danger" style="font-size: 13px"><i class="fa fa-info-circle"></i>&nbsp;Optimal Image Resolution : 240 x 240</label>
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" >
                                        <img id="testimoni_thumb" src="<?php echo $img_link ?>" alt=""> 
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style=" line-height: 10px;"></div>
                                    <div>
                                        <span class="btn default btn-file btn-sm">
                                            <span class="fileinput-new"> Select image </span>
                                            <span class="fileinput-exists"> Change </span>
                                            <input type="file" name="testimoni_pic" id="testimoni_pic"> </span>
                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                    </div>
                                </div>
                            </div>                    
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_control_1">Nama</label>
                                    <input type="text" class="form-control" name="nama_lengkap" id="nama_lengkap">
                                </div>
                            </div>  
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_control_1">Asal Daerah</label>
                                    <input type="text" class="form-control" name="asal_daerah" id="asal_daerah">
                                </div>
                            </div> 
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_control_1">Jabatan</label>
                                    <input type="text" class="form-control" name="jabatan" id="jabatan">
                                </div>
                            </div> 
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_control_1">Tanggal Post</label>
                                    <input type="text" class="form-control" name="tgl_post" id="tgl_post">
                                </div>
                            </div>                   
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <hr>
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <label class="backdrop-form" style="background: rgb(247, 247, 247);padding:10px;margin:0px;width:100%"><i class="fa fa-warning"></i>&nbsp;Silahkan testimoni user terkait pelayanan RWH</label>                                
                                    <textarea class="ckeditor" name="testimoni" id="testimoni"></textarea>
                                </div>
                                <label ><b class="font-red">*</b> Testimoni minimal 200 karakter</label>
                            </div>  
                        </div>
                    </div>
                    <input type="hidden" name="testimoni_id" id="testimoni_id">
                <?php echo form_close();?>
            </div>
            <br>
            <div class="modal-footer">
                <button type="button" id="add_button" onClick="add_function('add')" class="btn btn-circle red">Simpan &nbsp;<i class="fa fa-save"></i></button>
                <button type="button" id="edit_button" onClick="edit_function('update')" class="btn btn-circle yellow-crusta">Update &nbsp;<i class="fa fa-save"></i></button>
            </div>
        </div>

    </div>
</div>

<!-- detail Modal-->
<div class="modal fade" id="detail_modal" role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog"  style="width:800px">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-search"></i>
                    <span class="header-modal caption-subject sbold uppercase">Detail Testimoni</span>
                </div>
            </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-4 col-sm-3">
                            <div class="fileinput-new thumbnail">
                                <img id="detail_avatar" src="<?php echo base_url().'assets/frontend/img/no-avatar.png' ?>" alt=""> 
                            </div> 
                        </div>
                        <div class="col-md-8">
                            <h3><span id="detail_nama"></span></h3>
                            <table class="table" style="margin-top:15px">
                                <tr>
                                    <td style="width:130px"><i class="icon-home"></i>&nbsp; <span>Asal Daerah</td>
                                    <td>:</td>
                                    <td><span id="detail_asal_daerah"></span></td>
                                </tr>
                                <tr>
                                    <td><i class="icon-badge"></i>&nbsp; <span>Jabatan</td>
                                    <td>:</td>
                                    <td><span id="detail_jabatan"></span></td>
                                </tr>
                                <tr>
                                    <td><i class="icon-calendar"></i>&nbsp; <span>Tanggal Post</td>
                                    <td>:</td>
                                    <td><span id="detail_tgl_post"></span></td>
                                </tr>
                                <tr>
                                    <td><i class="icon-bubble"></i>&nbsp; <span>Testimoni</td>
                                    <td>:</td>
                                    <td><span id="detail_testimoni"></span></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-circle grey">Close &nbsp;<i class="fa fa-times"></i></button>            
            </div>
        </div>

    </div>
</div>

