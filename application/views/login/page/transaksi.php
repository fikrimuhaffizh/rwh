<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<?php include 'global_function.php';$login_type = $this->session->userdata('login_type'); ?>

<script type="text/javascript">
    var count=0;
    var total_qty =0;
    var listbarang = [];
    function tambah_barang(aksi,urutan){
        jenis_faktur = $("input[name='jenis_faktur']:checked"). val();
        if(!jenis_faktur){
            return alert('Silahakn pilih Jenis Faktur terlebih dahulu...');
        }
        if(listbarang.length==0){
            return alert('Silahkan pilih kantor terlebih dahulu...')
        }
        count++;
        var x = '';
            x+='    <tr id="barang_'+count+'">';
            x+='        <td>Data Barang : </td>';
            x+='        <td>';
            x+='            <select class="form-control input-sm" name="bkantor_id[]" required>';
            x+='                <option value="" >Pilih Barang...</option>';
                                if(listbarang.length>0){
                                    for(var i =0;i<listbarang.length;i++){
                                        var stok = listbarang[i]['stok_akhir'];
                                        var disabled ='';
                                        if(stok==0 && jenis_faktur=='Keluar')
                                            disabled = 'disabled';

                                        x+='<option '+disabled+' value='+listbarang[i]['bkantor_id']+'>'+listbarang[i]['nama_barang']+' (stok '+listbarang[i]['stok_akhir']+')</option>';
                                    }
                                }
            x+='            </select>';
            x+='        </td>';
            x+='        <td>Qty : </td>';
            x+='        <td>';
            x+='            <input type="number" class="form-control input-sm pull-left" style="width:70px" name="qty[]" required >';
            x+='            <a class="btn red btn-sm pull-right" onClick="hapus_barang('+count+')"><i class="fa fa-trash"></i></a>';
            x+='        </td>';
            x+='    </tr>';
            $('#list_barang').append(x);
        

    }
    function hapus_barang(urutan){
        $('#barang_'+urutan).hide('puff',function(){$(this).remove()})
    }

    function getListBarangByKantorCabang(id){
        $('#list_barang').empty();
        $('#btn-tambah-brg').hide();
        $.ajax({
                method: 'POST',
                data:{'kantorcabang_id' : id},
                url: '<?php echo base_url() ?>administrator/barang/getListBarangByKantorCabang',
                dataType : 'JSON',
                beforeSend : function(){$('#loading').show();},
                success : function(resp){
                    $('#loading').hide();
                    if(resp.length>0){
                        $('#btn-tambah-brg').show('hide');
                        listbarang = resp;                        
                    } else {
                        alert('Kantor Cabang tidak memiliki list barang !');
                    }
                }
            });
    }
    function add_function(task){
        if(task == 'show'){
            $('#add_modal').modal();
        }
        if(task == 'add'){
            $('#add_modal').modal('hide');
            data = new FormData($('#add-form')[0]);
            $.ajax({
                method: 'POST',
                data:data,
                url: '<?php echo base_url() ?>administrator/barang/add',
                cache:false,
                contentType: false,
                processData: false,
                success : function(resp){
                    if(resp == '"add_success"'){
                        swal({
                              title: "Berhasil !",
                              text : "Transaksi berhasil ditambahkan",
                              type: "success",
                              timer: 800,
                              showConfirmButton : false
                            },
                            function(){
                                swal.close();
                                table.ajax.reload();
                        });
                    }
                }
            });
        }
    }

    function edit_function(task='',id='',jenis_faktur=''){
        if(task == 'edit'){
            $('.data_input'+id).removeClass('hide');
            $('.data_text'+id).addClass('hide');

            $('.btn_save'+id).removeClass('hide');
            $('.btn_cancel'+id).removeClass('hide');

            $('.btn_edit'+id).addClass('hide');
            $('.btn_hapus'+id).addClass('hide');
        }

        if(task == 'cancel'){
            $('.data_input'+id).addClass('hide');
            $('.data_text'+id).removeClass('hide');

            $('.btn_save'+id).addClass('hide');
            $('.btn_cancel'+id).addClass('hide');

            $('.btn_edit'+id).removeClass('hide');
            $('.btn_hapus'+id).removeClass('hide');
        }


        if(task == 'delete'){
            url = '<?php echo base_url() ?>administrator/transaksi/delete';
            swal({
                  title: "Hapus Data?",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "grey",
                  confirmButtonText: "Yes, delete it!",
                  closeOnConfirm: false,
                  showLoaderOnConfirm : true
                },
                function(){
                    $.ajax({
                      method: 'POST',
                      url: url,
                      data: {'jenis_faktur' : jenis_faktur,'faktur_id' : id},
                      dataType: 'json',
                      success : function(resp){
                          if(resp == 'update_success'){
                            swal.close();
                            table1.ajax.reload();
                          }
                      }
                  });
                }); 
            
        }
    }
    function toggleNoFaktur(task){
        $('.form-no-faktur').empty();
        $('#list_barang').empty();
        if(task=='enable'){
            var x = '<label class="col-md-3 control-label sbold">No. Faktur : </label>';
                x+= '<div class="col-md-9">';
                x+= '     <div class="input-group">';
                x+= '          <input type="text" name="no_faktur" class="form-control input-sm input-medium">';
                x+= '     </div>';
                x+= '</div>';
            $('.form-no-faktur').append(x);
        }
    }
    $(document).ready(function(){

        table1 = $('#table_barang').DataTable({ 
            "processing": true, "serverSide": true,"order": [],
            "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
            "ajax": {"url": "<?php echo site_url('administrator/transaksi/pagination')?>","type": "POST"},
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                        {"targets": [0],"className": "dt-center"}

                    ]
        }); 

        table2 = $('#table_history').DataTable({ 
            "processing": true, "serverSide": true,"order": [],
            "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
            "ajax": {"url": "<?php echo site_url('administrator/transaksi/pagination_history')?>","type": "POST"},
            "order": [[ 7, "desc" ]],
            "columnDefs": [
                        {"targets": [0],"className": "dt-center"},
                        {"targets": [4],"className": "dt-center"},
                        {"targets": [5],"className": "dt-center"},
                        {"targets": [6],"className": "dt-center"},
                        {"targets": [7],"className": "dt-center"}
                    ],
            "initComplete" : function (resp) {
                this.api().columns([3]).every( function () {
                    var column = this;
                    var select = $('<select class="from-control input-sm input-small"><option value="">Semua Kantor</option></select>')
                                    .appendTo( $('#filter_barang').empty())
                                    .on( 'change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                        column.search( $(this).val(), true, false ) .draw();
                                    });
                    $.ajax({
                        url: '<?php echo base_url() ?>administrator/kantor_cabang/getAllKantorCabang/',                   
                        type: 'post',
                        dataType : 'json',
                        success : function(resp){
                            for(var i=0;i<resp.length;i++){
                                select.append( '<option value="'+resp[i]['nama_cabang']+'">'+resp[i]['nama_cabang']+'</option>' )
                            }
                        }
                     });
                    
                });

                this.api().columns([1]).every( function () {
                    var column = this;
                    var select = $('<select class="from-control input-sm input-small"><option value="">Semua Barang</option></select>')
                                    .appendTo( $('#filter_kepemilikan').empty())
                                    .on( 'change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                        column.search( $(this).val(), true, false ) .draw();
                                    });
                    $.ajax({
                        url: '<?php echo base_url() ?>administrator/barang/getAllNamaBarangDistinct/',                   
                        type: 'post',
                        dataType : 'json',
                        success : function(resp){
                            for(var i=0;i<resp.length;i++){
                                select.append( '<option value="'+resp[i]['nama_barang']+'">'+resp[i]['nama_barang']+'</option>' )
                            }
                        }
                     });
                    
                });
            }
        }); 
        $("#add-form").submit(function() {
            $('[name="qty[]"]').each(function(){
                total_qty += Number($(this).val());
                $('[name="total_qty"]').val(total_qty);
            })
        });
    });
</script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Stock Management
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="<?php echo base_url().'administrator' ?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Transaksi</span>
        </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                
                <?php if($show_form){ ?>
                    <div class="col-md-6">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a class="btn btn-sm yellow-crusta" href="<?php echo base_url().'administrator/transaksi' ?>"><i class="fa fa-arrow-circle-o-left"></i> Kembali</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject bold uppercase"> Tambah Transaksi</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <?php if($this->session->flashdata('err_add')){ ?>
                                    <div class="alert alert-danger"><i class="fa fa-times"></i> <?php echo $this->session->flashdata('err_add') ?></div>
                                <?php } ?>
                                <?php echo form_open('administrator/transaksi/add',array('id'=>'add-form','class'=>'form-horizontal'));?>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label sbold">Jenis Faktur : </label>
                                        <div class="col-md-9">
                                            <div class="mt-radio-inline" style="margin-bottom: -25px">
                                                <label class="mt-radio mt-radio-outline">
                                                    <input type="radio" name="jenis_faktur" id="optionsRadios25" value="Masuk"  onclick="toggleNoFaktur('enable')" required> Barang Masuk &nbsp;
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio mt-radio-outline">
                                                    <input type="radio" name="jenis_faktur" id="optionsRadios26" value="Keluar" onclick="toggleNoFaktur('disble')"> Barang Keluar
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label sbold">Kepemilikan :</label>
                                        <div class="col-md-9">
                                            <select class="form-control input-sm input-medium" name="kantorcabang_id" id="kantorcabang_id" onchange="getListBarangByKantorCabang(this.value)">
                                                    <option value="" selected>Pilih Kantor Cabang...</option>
                                                    <?php
                                                        foreach($kantor_cabang as $row){
                                                            echo '<option value="'.encrypt($row->kantorcabang_id).'">'.$row->nama_cabang.'</option>';
                                                        }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group form-no-faktur">
                                        
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label sbold">Keterangan : </label>
                                        <div class="col-md-9">
                                            <textarea class="form-control input-sm input-medium" name="keterangan" required></textarea>
                                        </div>
                                    </div>
                                    <h4 class="col-md-12 display-none" id="loading">
                                            <img src="<?php echo base_url().'assets\frontend\img\AjaxLoader.gif'?>"> 
                                            <span>Mengambil data ...</span>
                                        </h4>
                                    <div  id="btn-tambah-brg" class="col-md-12 display-none">
                                        <a class="btn btn-xs red " onclick="tambah_barang()"><i class="fa fa-plus"></i> Tambah Barang</a>
                                    </div>
                                    <div class="col-md-12">
                                        <table class="table">
                                            <tbody id="list_barang">
                                                
                                            </tbody>
                                        </table>
                                        <hr>
                                        <button class="btn btn-sm green-jungle pull-right"><i class="fa fa-save"></i> Simpan</button>
                                        <input class="form-control input-sm input-xsmall" type="hidden" name="total_qty" id="total_qty">
                                    </div>
                                    <div class="clearfix"></div>
                                <?php echo form_close()?>
                            </div>
                        </div>
                    </div>
                
                <?php } else if($show_detail){ 
                    $jenis_trans = $transaksi[0]->jenis_trans == 'Masuk' ? '<span class="font-green-jungle"><i class="fa fa-download"></i> Barang Masuk</span>' : '<span class="font-red"><i class="fa fa-upload"></i> Barang Keluar</span>';
                    ?>
                    <div class="col-md-12">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a class="btn btn-sm yellow-crusta" href="<?php echo base_url().'administrator/transaksi' ?>"><i class="fa fa-arrow-circle-o-left"></i> Kembali</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <i class="fa fa-list font-dark"></i>
                                    <span class="caption-subject"><?php echo 'Detail &nbsp;<b>'.$jenis_trans.'</b> Faktur <b>'.$transaksi[0]->no_faktur.'</b>'?> di <b><?php echo $transaksi[0]->nama_cabang ?></b></span>
                                </div>
                            </div>
                            <span><b>Catatan : </b> <?php echo $transaksi[0]->keterangan ? $transaksi[0]->keterangan : '-' ?></span>
                            <div class="clearfix"></div><br>
                            <div class="portlet-body">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th style="text-align: center"> No. </th>
                                        <th> Nama Barang</th>
                                        <th> Kode Barang </th>
                                        <th style="text-align: center"> Tanggal Transaksi </th>
                                        <th style="text-align: center"> Qty </th>
                                        <th style="text-align: center"> Stok Awal </th>
                                        <th style="text-align: center"> Stok Akhir </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $count=0;foreach($transaksi as $row){?>
                                    <tr>
                                        <td style="text-align: center"><?php echo ++$count; ?></td>
                                        <td><?php echo $row->nama_barang?></td>
                                        <td><?php echo $row->kode_barang?></td>
                                        <td style="text-align: center"><?php echo date('d-M-Y H:i',strtotime($row->tgl_transaksi))?></td>
                                        <td style="text-align: center"><?php echo $row->qty?></td>
                                        <td style="text-align: center"><?php echo $row->stok_awal?></td>
                                        <td style="text-align: center"><?php echo $row->stok_akhir?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                            </table>
                        </div>
                            
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>                
                
                <?php } else { ?>
                        <div class="col-md-12">
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="btn-group">
                                            <a class="btn btn-sm red" href="<?php echo base_url().'administrator/transaksi/form' ?>">Tambah Transaksi <i class="fa fa-plus"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Managed Transaksi</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_1_1" data-toggle="tab"> <i class="fa fa-list"></i> Riwayat Per Faktur </a>
                                    </li>
                                    <li>
                                        <a href="#tab_1_2" data-toggle="tab"> <i class="fa fa-briefcase"></i> Riwayat Per Barang </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab_1_1">
                                        <?php if($this->session->flashdata('add_success')){ echo '<label class="alert bg-green-jungle font-white"><i class="fa fa-check"></i> '.$this->session->flashdata('add_success').'</label>';}?>
                                        <table class="table table-hover table-bordered  table-condensed" id="table_barang">
                                            <thead>
                                                <tr>
                                                    <th> No. </th>
                                                    <th> No. Faktur</th>
                                                    <th> Jenis Faktur </th>
                                                    <th> Jumlah </th>
                                                    <th> Tanggal Transaksi </th>
                                                    <th> Petugas </th>
                                                    <th> Aksi </th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="tab_1_2">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-inline pull-left">
                                                        <div class="form-group"><span>Filter By : </span></div>
                                                        <div class="form-group"><div id="filter_barang"></div></div>
                                                        <div class="form-group"><div id="filter_kepemilikan"></div></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-hover table-bordered  table-condensed" id="table_history">
                                            <thead>
                                                <tr>
                                                    <th> No. </th>
                                                    <th> Nama Barang </th>
                                                    <th> Jenis Transaksi </th>
                                                    <th> Kepemilikan </th>
                                                    <th> Qty </th>
                                                    <th> Stok Awal </th>
                                                    <th> Stok Akhir </th>
                                                    <th> Tanggal Transaksi </th>
                                                    <th> No. Faktur </th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>        
                                </div> 
                                
                            </div>
                        </div>
                
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
