    <!-- Start contain wrapp -->
    <style type="text/css">
        .img_artikel{width:100%;}
        .artikel_container{margin:20px 5px 20px 5px;}
        .post-thumbnail{top:0px;}
        .recent-widget h6 a{line-height: 0px}
        h1 {font-size: 25px;margin-bottom: 0px;line-height: 30px}
    </style>

    <?php 
        if($detail_artikel){ ?>
            <div class="inner-head">
                <div class="row">
                    <div class="col-md-12 paddingright-clear">
                        <h4>Artikel <span>Riau Wisata Hati</span></h4>
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url(); ?>">Beranda</a></li>
                            <li class="active"><a href="<?php echo base_url().'artikel' ?>">Artikel</a></li>
                            <li class="active"><a href="<?php echo $_SERVER['REQUEST_URI']; ?>"><?php echo $page_title ?></a></li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="artikel_container">   
                <div class="container">
                    <?php foreach($detail_artikel as $row){
                        $isi = $row->isi;
                        //Ambil kata pertama tanpa tag html dan masukan kedalam array setiap char nya. array pertama akan menjadi dropcaps
                        // $first_char = strip_tags(str_replace('...','',word_cut($isi,1)));
                        // //Hapus char pertama
                        // $x = substr($first_char, 1);

                        // //Pisahkan ke dalam array
                        // $first_letter = explode( ' ', $isi);
                        // //Hapur array pertama
                        // array_shift($first_letter);
                        // //Gabungkan kembali dengan kata pertama yang sudah dipotong huruf pertamanya
                        // $isi = $x.' '.implode( ' ', $first_letter);

            ?>
                <div class="row">
                    <div class="col-md-9">
                        <h1 class="fa-green"><a href="#"></a><?php echo $row->judul ?></a></h1>
                        <ul class="post-meta">
                            <li><a href="#"><i class="fa fa-calendar"></i> <?php echo indo_date($row->tgl_post); ?></a></li>
                            <li><a href="#"><i class="fa fa-user"></i> Adminsitrator</a></li>
                        </ul>
                        <hr>
                        <div class="col-md-5">
                            <div class="media-wrapper ">
                                <img src="<?php echo $row->media_id ?>" alt="Artikel Detail" class="<?php echo $row->judul ?> img-responsive">
                            </div>
                            <hr>
                        </div>
                        
                        <!-- <span class="dropcaps drop-primary"><?php echo $first_char[0] ?></span> -->
                      
                        <?php echo $isi ?>
           
                        <nav>
                            <ul class="pager">
                                <?php 
                                    if($prev_news){ 
                                        foreach($prev_news as $row2){
                                            echo '<li class="previous"><a href="'.base_url().'artikel/detail/'.encrypt($row2->artikel_id).'/'.link_replace($row2->judul).'"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;Previous</a></li>'; 
                                        }
                                    }
                                    if($next_news){ 
                                        foreach($next_news as $row3){
                                            echo '<li class="next"><a href="'.base_url().'artikel/detail/'.encrypt($row3->artikel_id).'/'.link_replace($row3->judul).'" class="pull-right">Next&nbsp;<i class="fa fa-arrow-circle-o-right"></i></a></li>'; 
                                        }
                                    }
                                ?>
                            </ul>
                        </nav>
                        <div class="clearfix"></div>
                        <hr>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <aside>
                            <div class="widget">
                                <h5 class="widget-head">Artikel Lainnya</h5>
                                <div class="recent-widget">
                                <?php 
                                    $other = $this->md_artikel->getOtherArtikel(date('Y-m-d H:i:s',strtotime($row->tgl_post)));
                                    foreach($other as $row4){
                                        $other_judul = strtolower(preg_replace('/\s+/', '-', $row4->judul));
                                        $tgl_post = indo_date($row4->tgl_post);
                                        $media = $this->md_media_detail->getMediaDetailByMediaId($row4->media_id);
                                        $link = base_url().'/artikel/detail/'.encrypt($row4->artikel_id).'/'.link_replace($other_judul);
                                        echo '<span class="fa-green"><i class="fa fa-newspaper-o"></i>&nbsp;'.$tgl_post.'</span>';
                                        echo '<div class="post">';
                                        echo '<a href="'.$link.'" class="post-thumbnail"><img src="'.base_url().$media[2]->media_link.'" class="img-thumb" alt="thumb_news" style="height: auto;width:100%;transform: scale(0.8);" /></a>';
                                        echo '<h6><a href="'.$link.'">'.$row4->judul.'</a></h6>';
                                        echo '</div>';
                                        
                                    }
                                ?>
                                <a class=" btn btn-xs btn-green pull-right" href="<?php echo base_url().'artikel' ?>">Semua artikel <i class="fa fa-share"></i></a>
                                </div>
                            </div>
                            <div class="widget">
                                <h5 class="widget-head">Follow us</h5>
                                <a href="https://www.facebook.com/RiauWisataHati/?nr" class="btn btn-facebook btn-icon btn-block">Facebook <i class="fa fa-facebook"></i></a>
                                <a href="javascript:;" class="btn btn-google-plus btn-icon btn-block">Google plus <i class="fa fa-google-plus"></i></a>
                                <a href="https://www.instagram.com/riauwisatahati/" class="btn btn-instagram btn7-icon btn-block">Instagram <i class="fa fa-instagram"></i></a>
                            </div>
                        </aside>
                    </div>
                </div>
            <?php } ?>
                </div>
            </div>
    <?php } else { ?>
            <div class="inner-head">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Artikel <span>Riau Wisata Hati</span></h4>
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url(); ?>">Beranda</a></li>
                            <li class="active"><a href="<?php echo base_url().'artikel' ?>"><?php echo $page_title ?></a></li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="artikel_container">   
                <div class="container">
                    <div class="row">
                            <?php 
                                if($artikel){
                                        foreach($artikel as $row){
                                            $id = encrypt($row->artikel_id);
                                            $media = $this->md_media_detail->getMediaDetailByMediaId($row->media_id);
                                            $tgl = indo_date($row->tgl_post);
                                            $isi = word_cut($row->isi,300);
                                            $judul = link_replace($row->judul);
                                            $link = base_url().'artikel/detail/'.$id.'/'.link_replace($judul);
                            ?>
                          
                                <a href="<?php echo $link ?>">
                                    <div class="Col-md-12 col-md-offset-1">
                                        <div class="col-md-4 col-sm-12">
                                            <div class="media-wrapper">
                                                <img src="<?php echo base_url().$media[1]->media_link ?>" class="img-responsive" alt="<?php echo $row->judul ?>">
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-7">
                                            <h5><a href="<?php echo base_url().'artikel/detail/'.$id.'/'.$judul ?>"><?php echo $row->judul ?></a></h5>
                                            <ul class="post-meta">
                                                <li><a href="javascript:;"><i class="fa fa-calendar"></i> <?php echo $tgl ?></a></li>
                                                <li><a href="javascript:;"><i class="fa fa-user"></i> Administrator</a></li>
                                            </ul>
                                            <p><?php echo $isi; ?></p>
                                            <a href="#"></a>
                                            <button onClick="window.location.href='<?php echo $link ?>'" class="btn btn-xs btn-yellow">Selengkapnya</button>
                                        </div>
                                    </div>
                                </a>
                                <div class="clearfix"></div>
                                <hr>
                            
                            <?php }
                            } ?>
                        
                    </div>
                    <div class="col-md-8 col-md-offset-2">
                        <div class="title-head centered">
                            <?php echo $pagination ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>    