<script type="text/javascript">
    function cek_keberangkatan(){
        $('.hp_tidak_valid').addClass('hide');
        $('.hp_valid').addClass('hide');
        $('.lst_byr').remove();
        $('.lst_syrt').remove();
        $('.lst_dt').remove();
        val = $('#no_hp').val();
        $.ajax({
            method: 'POST',
            beforeSend: function(){$('#loading').show();},
            complete: function(){setInterval(function(){ $('#loading').hide(); }, 2000);},                
            url: '<?php echo base_url() ?>keberangkatan/'+val,
            dataType: 'json',
            success : function(resp){
                if(resp=='Tidak Valid'){
                    $('.div_detail_jemaah').addClass('hide');
                    $('.hp_tidak_valid').removeClass('hide');
                    $('.accordion_jemaah').empty();
                } else {
                    $('.hp_valid').removeClass('hide');
                    $('.div_detail_jemaah').removeClass('hide');
                    $('.div_detail_jemaah').empty();
                    for(var j =0;j<resp.length;j++){
                        var z = '<hr>';
                            z += '<a data-toggle="collapse" class="btn btn-primary btn-sm accordion_jemaah" style="width:50%" data-parent="#accordion" href="#detail-jemaah-'+j+'" >';
                            z += '    '+(j+1)+'.&nbsp;'+resp[j]['nama_jemaah']+'';
                            z += '</a>';
                            z += '<div id="detail-jemaah-'+j+'" class="panel-collapse collapse accordion_jemaah">';
                            z += '    <div class="panel-body" id="accordion_jemaah_'+j+'">';
                            z += '    </div>';
                            z += '</div>';
                            $('.div_detail_jemaah').append(z);

                             if(resp[j]['status_verifikasi'] == 'Belum Verifikasi'){
                                    var x = '<label class="fa-green hp_valid">No handphone terdaftar<br>Namun jemaah belum diverifikasi oleh Kantor Pusat</label>';
                                    $('#accordion_jemaah_'+j).append(x);
                             }
                             else if(resp[j]['status_verifikasi'] == 'Tidak Valid'){
                                    var x = '<label class="fa-red hp_valid">No handphone terdaftar<br>Namun data jemaah dianggap tidak valid.<Br>Informasi lebih lanjut hubungi Kantor Pusat</label>';
                                    $('#accordion_jemaah_'+j).append(x);
                             }
                             else {
                                    var x = '';
                                        x += '<label >Data Jemaah</label>';
                                        x += '<span id="status_detail_'+j+'"></span>';

                                        x += '<label>List Syarat Jemaah</label>';
                                        x += '<table class="table table-striped" id="status_syarat_'+j+'"></table>';

                                        x += '<label>List Pembayaran Jemaah</label>';
                                        x += '<table class="table table-striped" id="status_bayar_'+j+'"></table>';
                                    $('#accordion_jemaah_'+j).append(x);

                                    var syarat = resp[j]['list_syarat'];
                                    for(var i=0;i<syarat.length;i++){
                                        if(syarat[i]['status_syarat']=='Lengkap')
                                            syarat[i]['status_syarat'] = '<span class="fa-green">Lengkap</span>';
                                        else 
                                            syarat[i]['status_syarat'] = '<span class="fa-red">Belum Lengkap</span>';
                                        var x  = '';
                                            x += '<tr class="lst_syrt">';
                                            x += '  <td style="width:50%">'+syarat[i]['persyaratan']+'</td>';
                                            x += '  <td>'+syarat[i]['status_syarat']+'</td>';
                                            x += '</tr>';
                                        $('#status_syarat_'+j+'').append(x);
                                    }
                                    var bayar = resp[j]['list_bayar'];
                                    for(var i=0;i<bayar.length;i++){
                                        var x  = '';
                                            x += '<tr class="lst_byr">';
                                            x += '  <td class="td_center">'+bayar[i]['tgl_bayar']+'</td>';
                                            x += '  <td class="td_center">'+bayar[i]['jumlah_bayar']+'</td>';
                                            x += '  <td class="td_center">'+bayar[i]['status_pembayaran']+'</td>';
                                            x += '</tr>';
                                        $('#status_bayar_'+j+'').append(x);
                                    }
                                    var x  = '<table class="table table-striped lst_dt">';
                                        x += ' <tr><td class="td_right">Nama Jemaah</td><td>:</td><td class="td_left">'+resp[j]['nama_jemaah']+'</td></tr>';
                                        x += ' <tr><td class="td_right">Paket</td><td>:</td><td class="td_left">'+resp[j]['nama_paket']+'</td></tr>';
                                        x += ' <tr><td class="td_right">Kelas</td><td>:</td><td class="td_left">'+resp[j]['kelas']+'</td></tr>';
                                        x += ' <tr><td class="td_right">Keberangkatan</td><td>:</td><td class="td_left">'+resp[j]['berangkat']+'</td></tr>';
                                        x += ' <tr><td class="td_right">Status Syarat</td><td>:</td><td class="td_left">'+resp[j]['status_syarat']+'</td></tr>';
                                        x += ' <tr><td class="td_right">Status Pembayaran</td><td>:</td><td class="td_left">'+resp[j]['status_pembayaran']+'</td></tr>';
                                        x += ' <tr><td class="td_right">Status Jemaah</td><td>:</td><td class="td_left">'+resp[j]['status_jemaah']+'</td></tr>';
                                        x += '</table>';
                                        $('#status_detail_'+j+'').after(x); 
                            }  
                    }                    
                }
            }
        });
    }
</script>
<style type="text/css">
    h2 {color:#349b34;font-size:20px;text-transform: uppercase;}
    ul {padding-left: 0px}    
    .container a {text-decoration: none}
    .full-width{width:100%;}
    .h1-footer{font-size: 15px;padding: 0px;margin:20px 0px 20px 0px;line-height: 25px}
    .p-footer{font-size: 11px;line-height: 20px}
    .td_left{text-align: left;}
    .td_right{text-align: right;}
    .td_center{text-align: center;}
    .search-paket-box{padding: 20px 0px 20px 20px;margin:0px 10px 30px 10px;background-color: #f2f2f2; box-shadow: 2px 2px 5px #888888;}
    .icon-linner li ul li h5{color: #f0a64d;}
    .icon-linner li ul li .fa{color: #349b34;}
    .why-us {font-size:15px;color:#f0a64d;text-transform: uppercase;}
    .text-beranda-form{font-size:15px;font-weight: bold;margin-bottom:10px;}
    .partner-list{padding:40px;}
    .table-responsive {border:0px;}
</style>

    <div id="slider-wrapper">
        <div id="layerslider" style="width:100%;height:750px;">
        <?php 
            foreach($slideshow as $row){
                $link = "#";
                if($row->link_slide!="")
                    $link = $row->link_slide;
        ?>
            <div class="ls-slide" onClick="window.location='<?php echo $link ?>'" data-ls="slidedelay:8000;transition2d:1,2,3,4;">
                <img src="<?php echo base_url().$row->url_slide ?>" class="ls-bg" alt="Slide_background_<?php echo $row->judul ?>" />
            </div>
        <?php } ?>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="cta-wrapp cta-dark paddingbot-clear padding-top20">
        <div class="container">
            <div class="custom-search-wrapp custom-offset-top">
                <div class="col-md-12 col-sm-12 pull-left">
                    <?php echo form_open('paket/', array('id'=>'add-form','class'=>'row')); ?>
                        <div class="custom-tabs" data-effect-in="flipInX">
                            <ul class="nav nav-tabs"  role="tablist">
                                <li class="active"><a href="#tab1" data-toggle="tab">Umroh</a></li>
                                <li><a href="<?php echo base_url().'haji' ?>" >Haji</a></li>
                                <li><a href="#tab3" data-toggle="tab">Wisata Islam</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab1">
                                    <div class="col-md-3">
                                        <label class="text-beranda-form"><i class="fa fa-plane"></i> Kota Berangkat</label>

                                        <select class="form-control" name="keberangkatan_umroh">
                                            <option value="Semua Keberangkatan" selected>Semua Kota</option>
                                            <?php foreach($atribut_form_umroh['berangkat_awal'] as $kntrcbg){ ?>
                                                <option value="<?php echo $kntrcbg->kota_id ?>"><?php echo $kntrcbg->provinsi ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="text-beranda-form"><i class="fa fa-calendar"></i> Bulan Berangkat</label>
                                        <select class="form-control" name="bulan_umroh">
                                            <option value="Semua Bulan" selected>Semua Bulan</option>
                                            <?php foreach($atribut_form_umroh['bulan_berangkat'] as $bln){ ?>
                                                <option value="<?php echo $bln->bulan_berangkat ?>"><?php echo $bln->bulan?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="text-beranda-form"><i class="fa fa-calendar-minus-o"></i> Tahun Berangkat</label>
                                        <select class="form-control" name="tahun_umroh">
                                            <option value="Semua Tahun" selected>Semua Tahun</option>
                                            <?php foreach($atribut_form_umroh['tahun_berangkat'] as $thn){?>
                                                <option value="<?php echo $thn->tahun_berangkat ?>"><?php echo $thn->tahun_berangkat?></option>
                                            <?php } ?> 
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="text-beranda-form">&nbsp;</label>
                                        <button class="btn btn-green btn-icon full-width" name="btn-cari" value="umroh">Cari Paket Umroh <i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab3">
                                    <div class="col-md-3">
                                        <label class="text-beranda-form"><i class="fa fa-plane"></i> Kota Berangkat</label>
                                        <select class="form-control" name="keberangkatan_wisata">
                                            <option value="Semua Keberangkatan" selected>Semua Kota</option>
                                            <?php foreach($atribut_form_wisata['berangkat_awal'] as $kntrcbg){ ?>
                                                <option value="<?php echo $kntrcbg->kota_id ?>"><?php echo $kntrcbg->provinsi ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="text-beranda-form"><i class="fa fa-calendar"></i> Bulan Berangkat</label>
                                        <select class="form-control" name="bulan_wisata">
                                            <option value="Semua Bulan" selected>Semua Bulan</option>
                                            <?php foreach($atribut_form_wisata['bulan_berangkat'] as $bln){ ?>
                                                <option value="<?php echo $bln->bulan_berangkat ?>"><?php echo $bln->bulan?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="text-beranda-form"><i class="fa fa-calendar-minus-o"></i> Tahun Berangkat</label>
                                        <select class="form-control" name="tahun_wisata">
                                            <option value="Semua Tahun" selected>Semua Tahun</option>
                                            <?php foreach($atribut_form_wisata['tahun_berangkat'] as $thn){?>
                                                <option value="<?php echo $thn->tahun_berangkat ?>"><?php echo $thn->tahun_berangkat?></option>
                                            <?php } ?> 
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="text-beranda-form">&nbsp;</label>
                                        <button class="btn btn-green btn-icon full-width" name="btn-cari" value="wisata">Cari Paket Wisata Islam <i class="fa fa-search"></i></button>
                                        </div>
                                </div>
                            </div>
                        </div>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>

    <?php if($paket_promo){ ?>
        <div class="contain-wrapp gray-container paddingbot-clear padding-top20">   
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="title-head centered">
                            <h2>Paket Promo</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 owl-column-wrapp">
                        <div id="recent-column3" class="owl-carousel leftControls-right">
                            <?php foreach($paket_promo as $row){ ?>
                                <div class="item">
                                    <a href="<?php echo base_url().'paket/detail/'.encrypt($row->paketwaktu_id).'/'.link_replace($row->nama_paket)?>">
                                    <div class="wrap-team standard">
                                        <div class="img-wrapper">
                                            <img src="<?php echo base_url().$row->media_link ?>" class="img-responsive"  alt="<?php echo $row->nama_paket.' '.$row->bulan_berangkat.'-'.$row->tahun_berangkat ?>" />
                                        </div>
                                        <div class="content">
                                            <h5 class="name">
                                                <?php echo $row->nama_paket ?><br>
                                                <span><?php echo $row->harga ?></span> <button class="btn btn-xs btn-red btn-bordered">Promo !</button>
                                            </h5>
                                        </div>
                                        <div class="share">
                                            <ul>
                                                <li><a class="btn btn-sm btn-primary" href="<?php echo base_url().'paket/detail/'.encrypt($row->paketwaktu_id).'/'.link_replace($row->nama_paket)?>"><i class="fa fa-plane"></i> Detail Paket</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <div class="contain-wrapp padding-top20 paddingbot-clear">  
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="title-head centered">
                        <h2>Kenapa Memilih Kami</h2>
                    </div>
                </div>
                <div class="col-md-12 paddingbot-clear">
                    <ul class="icon-linner">
                        <li>
                            <ul>
                                <li>
                                    <i class="fa fa-institution  fa-3x"></i>
                                    <h3 class="why-us">Izin</h3>
                                    <p>Pasti Travel Berijinnya</p>
                                </li>
                                <li>
                                    <i class="fa fa-building fa-3x"></i>
                                    <h3 class="why-us">Hotel</h3>
                                    <p>Pasti Hotelnya Dekat</p>
                                </li>
                                <li>
                                    <i class="fa fa-plane fa-3x"></i>
                                    <h3 class="why-us">Jadwal</h3>
                                    <p>Pasti Jadwal Terbangnya</p>
                                </li>
                                <li>
                                    <i class="fa fa-cart-plus fa-3x"></i>
                                    <h3 class="why-us">Layanan</h3>
                                    <p>Pasti Layanannya</p>
                                </li>
                                <li>
                                    <i class="fa fa-cc-visa fa-3x"></i>
                                    <h3 class="why-us">Visa</h3>
                                    <p>Pasti Visanya</p>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="cta-wrapp cta-dark text-center padding-bot30 padding-top20">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="title-head centered">
                        <h2>Cek Status Keberangkatan</h2>
                    </div>
                    <p>Pastikan untuk selalu memantau status keberangkatan.<br>
                    Masukkan No Handphone Jemaah</p>
                </div>
        
                
                <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                    <div class="input-group">
                        <input type="text" class="form-control" id="no_hp" name="no_hp" placeholder="Ex. 0825xxxxxxx" minlength="8" maxlength="12">
                        <span class="input-group-btn">
                            <button class="btn btn-yellow" onClick="cek_keberangkatan()" type="button"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                    <label class="fa-green hide hp_valid"><i class="fa fa-check"></i> No handphone Valid </label>
                    <label class="fa-red hide hp_tidak_valid"><i class="fa fa-times"></i> No Handphone Tidak Valid </label>  
                </div>
                <div class="col-md-6 col-sm-12 col-md-offset-3 ">
                    <div class="hide div_detail_jemaah table-responsive">
          

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="cta-wrapp cta-primary text-center ">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <h2>Cari Kantor Cabang Di Daerah Kamu </h2>
                </div>
                <div class="col-md-3">
                    <a class="btn btn-green btn-icon btn-block" href="<?php echo base_url().'kantor' ?>">Selengkapnya <i class="fa fa-send"></i></a>
                </div>
            </div>
        </div>
    </div>
    <?php 
        if($top3artikel){
    ?>
        <div class="contain-wrapp paddingtop-clear padding-bot20">   
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 padding-top20">
                        <div class="title-head centered">
                            <h2>Artikel Terbaru</h2>
                        </div>
                    </div>
                    
                    
                    <?php   foreach($top3artikel as $row){
                                $id = encrypt($row->artikel_id);
                                $judul = strtolower(preg_replace('/\s+/', '-', $row->judul));
                                $media = $this->md_media_detail->getMediaDetailByMediaId($row->media_id);
                                $tgl = indo_date($row->tgl_post);
                                $isi = word_cut($row->isi,120);
                                $link = base_url().'artikel/detail/'.$id.'/'.link_replace($judul);
                    ?>
                        <div class="col-md-4">
                            <div class="thumbnail">
                                <a href ="<?php echo $link ?>">
                                <img src="<?php echo base_url().$media[1]->media_link ?>" class="img-responsive full-width" alt="alt="<?php echo $row->judul ?>"" >
                                </a>
                                <div class="caption">
                                    <i class="fa fa-newspaper-o fa-3x icon-circle fa-default"></i>
                                    <a href ="<?php echo $link ?>"><h5><?php echo $row->judul ?></h5></a>
                                    <b href="javascript:;"><?php echo $tgl ?></b>
                                    <p><?php echo $isi; ?></p>
                                    <p><a href="<?php echo $link ?>" class="btn btn-default btn-sm">Selengkapnya</a></p>
                                </div>
                            </div>
                        </div>
                      
                    <?php }
                     ?>
                    
                </div>
                <div class="col-md-8 col-md-offset-2">
                    <div class="title-head centered">
                    </div>
                </div>
                <a href="<?php echo base_url().'artikel' ?>" class="btn btn-green btn-sm pull-right ">Semua Artikel <i class="fa fa-share"></i></a>
            </div>
        </div>
    <?php } ?>

    <?php if($testimoni){ ?>
        <div class=" padding-top10 padding-bot40 " data-speed="0.5" data-size="50%">
            <div class="container">
                <div class="title-head"><h2>Testimoni Jemaah</h2></div>
                <div class="content-parallax">
                    <div class="row">
                        <div class="col-md-12  owl-column-wrapp" style="background-color: #f9f9f9" >
                            <div id="testimoni" class="owl-carousel botControls-right">
                                <?php foreach($testimoni as $row){ ?>
                                <div class="item">
                                    <div class="testimonial">
                                        <img src="<?php echo base_url().$row->media_link ?>" class="img-circle" alt="<?php echo $row->nama_lengkap ?>" />
                                        <blockquote class="quote-lg">
                                            <p><?php echo $row->testimoni ?></p>
                                        </blockquote>
                                        <p class="author"><?php echo '<b>'.$row->nama_lengkap.'</b>, '.$row->asal_daerah ?> - <a href="#"><?php echo $row->jabatan ?></a></p>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </div> 
    <?php } ?>

    <div class="clearfix"></div>
    <div class="parallax partner-list" data-speed="0.5" data-size="50%">
        <div class="container">
            <div class="title-head"><h2>Our Partner</h2></div>
            <div class="content-parallax">
                <div class="row">
                    <div class="col-md-12 owl-column-wrapp">

                        <div id="clients" class="owl-carousel leftControls-right">
                            <?php   

                                $alt = array('Bank Syariah Mandiri');
                                $img = array('bsm.png');
                                for($i=0;$i<count($img);$i++){ ?>
                                <div class="item">
                                    <div class="client-box">
                                        <a href="#" class="client-logo">
                                            <img src="<?php echo base_url().'assets/frontend/img/partner/'.$img[$i] ?>" alt="<?php echo $alt[$i] ?>" />
                                        </a>
                                    </div>
                                </div>
                            <?php }
                            foreach($maskapai as $row){ 

                                $y = $this->md_media_detail ->getMediaDetailByMediaId($row->media_id);
                            ?>
                                <div class="item">
                                    <div class="client-box">
                                        <a href="#" class="client-logo">
                                            <img src="<?php echo base_url().$y[1]->media_link; ?>" style="width:70%">
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div> 

    <div class="col-md-12">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="h1-footer">PT. Riau Wisata Hati - Travel Terpercaya  Di Indonesia</h1>
                </div>
                <div class="col-md-6">
                    <p class="p-footer">Lorem ipsum dolor sit amet, mel noster virtute evertitur cu. Nec te dicant patrioque dissentiet, natum porro te sea. At solet scriptorem per, an impetus dolores vix. Inani vituperatoribus an nam, in ipsum platonem est, ei falli commune recteque eam. Eripuit nusquam in nam.
                    </p>
                    <p class="p-footer">
                    Cum aperiri molestie at, fugit putant suscipiantur sea ei. Id graeci legendos maiestatis pro, at eam modus atqui mediocritatem, qui id assentior assueverit. Sed graecis moderatius et, per tamquam civibus oportere in. Veri accumsan accusata pro an, ut eam purto nullam. Option reprimique mei ne, suas persequeris usu an, deseruisse adversarium an ius.</p>
                </div>
                <div class="col-md-6">
                    <p class="p-footer">Graecis molestie perpetua no qui, ex his delenit omittam constituam. Tempor iuvaret aliquid cu per, qui id nibh consul. Mutat legere facete eu duo. Ea dolorum contentiones disputationi eos. Vix an propriae delectus, scaevola dissentiunt an eum.
                    </p>
                    <p class="p-footer">
                    Appetere maluisset liberavisse ad nec, minim simul aliquam ea sed, cum an tamquam virtute quaeque. Assum vocent vituperata cu vis, pri at prima antiopam. Vis copiosae forensibus ex, et quo quem verterem, has saepe erroribus te. Quando abhorreant his id. Pro scripta denique et, in graece melius constituto vis. Vitae quaestio incorrupte vis ut.</p>
                </div>
            </div>
        </div>
    </div>

    

