<script type="text/javascript">
function gantiMode(){
	cek = $('#change').is(":checked");
	if(cek)
		$('#password_daftar').attr('type','text');
	else
		$('#password_daftar').attr('type','password');
}
</script>
<style type="text/css">
	.full-width{width:100%}
</style>
<div class="container gray-container padding-bot20">
	<div class="row">
		<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
			<div class="login-wrapp">
			<?php if($add_success){ ?>
					<div class="alert alert-success" role="alert" data-out="bounceOut">
						<i class="fa fa-check"></i> 
						<h6 class="title">Success</h6>
						<p>Pendaftaran Member baru berhasil !</p>
					</div>
			<?php } else { ?>
				<div class="logreg-contain">
					<div class="custom-tabs" style="padding:30px" >
						<div class="title-head centered"><h4>Pendaftaran Member Riau Wisata Hati</h4></div>
						
							<p><strong>Selamat Datang di Riau Wisata Hati</strong> !<br> Silahkan lengkapi form dibawah ini untuk menjadi Member Riau Wisata Hati</p>
						<div class="col-md-12">
						<?php echo form_open('daftar/add', array('id'=>'add-form','class'=>'form-horizontal')); ?>
							<div class="form-group">
								<label>Nama : <span class="noempaty">*</span></label> 
								<input type="input" class="form-control" name="nama_lengkap" id="nama_lengkap" value="<?php echo $nama ?>" required/>
							</div>
							<div class="form-group">
								<label>Handphone :</label>
								<input type="input" maxlength="12" onkeypress="return event.charCode >= 45 && event.charCode <= 57" class="form-control" name="no_hp" id="no_hp" placeholder="Ex. 085212345678" />
							</div>
							<div class="form-group">
								<label>Email : <span class="noempaty">*</span></label> 
								<input type="email" class="form-control" name="email" id="email" value="<?php echo $email ?>" readonly />
							</div>
							<div class="form-group">
								<label>Password:<span class="noempaty">*</span></label>
								<input type="password" autocomplete="off" class="form-control input-sm" name="password" id="password_daftar" onkeypress="return event.charCode != 32" required/>
								<div class="checkbox pull-right">
									<label>
										<div class="checkator_holder checkbox">
											<input type="checkbox" id="change" onclick="gantiMode()" >
										</div> Show Password
									</label>
								</div>
								<span>Minimal 8 karakter</span>
							</div>

							<div class="form-group">
								<?php if($this->session->flashdata('err_daftar')){ ?>
									<div class="alert alert-danger" role="alert" data-out="fadeOutDown">
										<i class="fa fa-times"></i> 
										<h6 class="title">Submit Gagal</h6>
										<p><?php echo $this->session->flashdata('err_daftar') ?></p>
									</div>
								<?php } ?>
								<hr>
								<button type="submit" name="daftar" value="daftar" class="btn btn-primary btn-sm full-width">Daftar Member</button>
							</div>
						<?php echo form_close();?>
						</div>
					</div>  
				</div>
			<?php } ?>
			</div>
		</div>
	</div>
</div>