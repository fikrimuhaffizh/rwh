<style type="text/css">
    .font20{font-size: 25px}
</style>
<div class="inner-head">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <?php 
            if($galeri_item){
                $judul = 'Photo <span>'.$album_name.'</span>';
            } else {
                $judul = 'Album <span> Riau Wisata Hati</span>';
            }
            ?>
                <h1><?php echo $judul ?></h1>
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>">Beranda</a></li>
                    <li class="active"><?php echo $page_title ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="contain-wrapp padding-bot30 padding-top30 ">   
        <div class="container">
            <div class="row">
                <?php
                if($galeri_item){ ?>
                    <div class="col-md-8 col-md-offset-2">
                        <div class="title-head centered">
                            <h4><?php echo $album_keterangan ?></h4>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    
                    <?php $i=0;foreach($galeri_item as $row){ 
                        $alt = explode('/',$row['media_medium']); 

                        if($i == 0) 
                            echo '<div class="col-md-12">';
                        $i++;
                    ?>
                            <div class="col-md-4" >
                                <div class="recent-widget ">
                                    <div class="img-wrapper">
                                        <a href="<?php echo base_url().$row['media_large'] ?>">
                                            <img src="<?php echo base_url().$row['media_medium'] ?>" style="width:100%" alt="<?php echo $alt[3] ?>">
                                        </a>    
                                    </div>
                                </div>
                            </div>
                    <?php 
                        if($i == 3) {echo '</div>';$i=0;}; 
                        } 
                    ?>
                    <div class="clearfix"></div>
                    <hr>
                    <div class="col-md-8 col-md-offset-2">
                        <div class="title-head centered">
                            <?php echo $link ?>
                        </div>
                    </div>
                <?php } else { ?>

                    <div class="masonry gallery box">
                       <?php 
                        if($album){
                            $i=0;
                            foreach($album as $row){
                                $id = encrypt($row->galerialbum_id);
                                
                                if($i == 0) echo '<div class="col-md-12">';

                                $i++;
                        ?>
                        <div class="col-sm-4">
                            <div class="img-wrapper">
                                <a href="<?php echo base_url().'galeri/detail/'.$id ?>">
                                    <img src="<?php echo $row->media_id ?>" class="img-responsive" alt="Album Thumbnail">
                                </a>    
                            </div>
                            <div class="panel-group margintop20" id="accordion<?php echo $i; ?>">
                                <div class="panel panel-default">
                                    <div class="panel-heading" id="heading<?php echo $i; ?>">
                                        <h6 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion<?php echo $i; ?>" href="#panel<?php echo $i; ?>" class="collapsed" aria-expanded="false">
                                                <?php echo $row->nama_album ?>
                                            </a>
                                        </h6>
                                    </div>
                                    <div id="panel<?php echo $i; ?>" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                        <p><?php echo $row->keterangan ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php 
                            if($i == 3) {echo '</div>';$i=0;};
                         } ?>      
                    </div>
                    <div class="clearfix"></div>
                    <hr>
                    <div class="col-md-8 col-md-offset-2">
                        <div class="title-head centered">
                            <?php echo $link ?>
                        </div>
                    </div>
            <?php   } else {
                    echo '<div class="title-head centered"><h2 class="font20">Maaf, Album Belum Tersedia</h2></div>';
                }
            }
            ?>
               
            </div>
        </div>
    </div>
</div>