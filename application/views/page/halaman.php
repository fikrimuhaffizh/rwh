<div class="inner-head">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?php echo $judul ?></h1>
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>">Beranda</a></li>
                    <li class="active"><?php echo $page_title ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="contain-wrapp padding-bot30 padding-top30 ">   
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <article class="post">
                <?php 
                    if($halaman){ 
                        foreach($halaman as $row)
                        echo $row->isi;
                    } 
                 ?>
                </article>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>