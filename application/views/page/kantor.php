<style type="text/css">
    #td-list-jemaah{padding:5px}
    h2 {color:#349b34;font-size:20px;text-transform: uppercase;}
    .display-none {display:none;}
</style>
<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function getDetailKantor(){
        $('.kantor_keterangan').remove();
        val = $('#kantor').val();

        $.ajax({
                method: 'POST',
                beforeSend: function(){$('.loading').show();$('#list_kantor_div').hide();},
                complete: function(){$('.loading').hide();$('#list_kantor_div').show();},
                url: '<?php echo base_url() ?>kantor/getDetailKantor/'+val,
                dataType: 'json',
                success : function(resp){

                    if(resp){
                        for(var i=0;i<resp.length;i++){
                            var x =' <tr class="kantor_keterangan" id="kantor_keterangan">';
                                x +='     <td id="td-list-jemaah" style="text-align:center"><span>'+(i+1)+'</span></td>';
                                x +='     <td id="td-list-jemaah"><span>'+resp[i]['nama_cabang']+'</span></td>';
                                x +='     <td id="td-list-jemaah"><span>'+resp[i]['provinsi']+'</span></td>';
                                x +='     <td id="td-list-jemaah"><span>'+resp[i]['nama_daerah']+'</span></td>';
                                x +='     <td id="td-list-jemaah"><span>'+resp[i]['alamat']+'</span></td>';
                                x +='     <td id="td-list-jemaah"><span>'+resp[i]['telp']+'</span></td>';
                                x +=' </tr>';
                            $('#list_kantor').before(x);
                        }
                    }
                }
            });
    }
    $(document).ready(function(){
        $('#kantor').change(function(){
            getDetailKantor();
        });
    });
</script>
<div class="inner-head">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Kantor <span>Cabang</span></h1>
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>">Beranda</a></li>
                    <li class="active"><?php echo $page_title ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="contain-wrapp padding-bot30 padding-top30 ">	
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="title-head centered">
						<h2 class="hide">Riau Wisata Hati</h2>
						<p>Daftar Kantor Cabang RWH</p>
                    </div>
                    <center>RWH memiliki beberapa kantor cabang yang tersebar dibeberapa daerah untuk memudahkan jemaah mendapatkan informasi terkait paket perjalan Umroh, Haji, dan Wisata Islam yang tersedia</center>
				</div>
			</div><br>
            <div class="row">
                <div class="col-md-12">
                <div class="col-md-4 col-md-offset-4">
                    <select class="form-control" id="kantor">
                        <option value="">Pilih Daerah Kantor Cabang...</option>
                        <?php  foreach($kantor_cabang_provinsi as $row){ ?>
                            <optgroup label="<?php echo $row->provinsi ?>">
                            <?php foreach($all_kota_kantor as $row2){ 
                                if($row->kantorcabang_id == $row2->parent && $row->kota!=$row2->kota){
                        ?>
                                    <option value="<?php echo str_replace(' ','_',$row2->kota) ?>"><?php echo $row2->kota ?></option>

                        <?php } 
                            } ?>
                            </optgroup>
                        <?php 
                        } ?>

                    </select>
                    <hr>
                <div class="loading col-md-offset-4 clearfix display-none">
                    <img src="<?php echo base_url().'assets/frontend/img/AjaxLoader.gif' ?>" alt="">
                    <span>Mohon Tunggu...</span>
                    <br>
                </div>
                </div>
                <div class="col-md-8 col-md-offset-2 table-responsive display-none" id="list_kantor_div">
                    <table class="table">
                    <thead>
                        <th> No</th>
                        <th> Kantor </th>
                        <th> Provinsi </th>
                        <th> Daerah </th>
                        <th> Alamat </th>
                        <th> Kontak </th>
                    </thead>
                        <tr id="list_kantor"></tr>
                    </table>
                    <hr>
                </div> 
			</div>
		</div>
    </div>
</div>