<script type="text/javascript">
	    function tandaPemisahTitik(b){
        var _minus = false;
        if (b<0) _minus = true;
        b = b.toString();
        b=b.replace(".","");
        b=b.replace("-","");
        c = "";
        panjang = b.length;
        j = 0;
        for (i = panjang; i > 0; i--){
        j = j + 1;
        if (((j % 3) == 1) && (j != 1)){
        c = b.substr(i-1,1) + "." + c;
        } else {
        c = b.substr(i-1,1) + c;
        }
        }
        if (_minus) c = "-" + c ;
        return c;
    }

    function numbersonly(ini, e){
        if (e.keyCode>=49){
            if(e.keyCode<=57){
                a = ini.value.toString().replace(".","");
                b = a.replace(/[^\d]/g,"");
                b = (b=="0")?String.fromCharCode(e.keyCode):b + String.fromCharCode(e.keyCode);
                ini.value = tandaPemisahTitik(b);
                return false;
            }
            else if(e.keyCode<=105){
                if(e.keyCode>=96){
                    //e.keycode = e.keycode - 47;
                    a = ini.value.toString().replace(".","");
                    b = a.replace(/[^\d]/g,"");
                    b = (b=="0")?String.fromCharCode(e.keyCode-48):b + String.fromCharCode(e.keyCode-48);
                    ini.value = tandaPemisahTitik(b);
                    //alert(e.keycode);
                    return false;
                } else {
                    return false;
                }
            } else {
                return false; 
            }
        } else if (e.keyCode==48){
            a = ini.value.replace(".","") + String.fromCharCode(e.keyCode);
            b = a.replace(/[^\d]/g,"");
            if (parseFloat(b)!=0){
                ini.value = tandaPemisahTitik(b);
                return false;
            } else {
                return false;
            }
        } else if (e.keyCode==95){
            a = ini.value.replace(".","") + String.fromCharCode(e.keyCode-48);
            b = a.replace(/[^\d]/g,"");
            if (parseFloat(b)!=0){
                ini.value = tandaPemisahTitik(b);
                return false;
            } else {
                return false;
            }
        } else if (e.keyCode==8 || e.keycode==46){
            a = ini.value.replace(".","");
            b = a.replace(/[^\d]/g,"");
            b = b.substr(0,b.length -1);
            if (tandaPemisahTitik(b)!=""){
                ini.value = tandaPemisahTitik(b);
            } else {
                ini.value = "";
            }
            return false;
        } else if (e.keyCode==9){
            return true;
        } else if (e.keyCode==17){
            return true;
        } else {
            //alert (e.keyCode);
            return false;
        }
    }
</script>
<style type="text/css">
    #td-list-jemaah{padding:5px}
    h2 {color:#349b34;font-size:20px;text-transform: uppercase;}
</style>

<div class="inner-head">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Konfirmasi <span>Pembayaran</span></h1>
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>">Beranda</a></li>
                    <li class="active"><?php echo $page_title ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="contain-wrapp padding-bot30 padding-top30 ">	
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
                    <center>Mohon Konfirmasi dilakukan setelah dilakukannya pembayaran. Silahkan isi data dengan benar untuk memudahkan kantor pusat memverifikasi pembayaran</center>
				</div>
			</div><br>
            <div class="row">
                <div class="col-md-12">
                <div class="col-md-6 col-md-offset-3">
					<?php if($this->session->flashdata('info_success')){ ?>
						<div class="alert alert-success" role="alert" data-out="fadeOutDown">
							<i class="fa fa-check"></i> 
							<h6 class="title">Submit Berhasil</h6>
							<p><?php echo $this->session->flashdata('info_success') ?></p>
						</div>
					<?php } ?>
                    <?php echo form_open('konfirmasi/add', array('id'=>'add-form','class'=>'form-horizontal')); ?>
							<div class="form-group">
								<label class="col-md-4">Nama: <span class="noempaty">*</span></label> 
								<div class="col-md-8">
									<input type="input" class="form-control input-sm" name="nama" id="nama" required/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4">Handphone: <span class="noempaty">*</span></label>
								<div class="col-md-8">
								<input type="input"  maxlength="12" onkeypress="return event.charCode >= 45 && event.charCode <= 57" class="form-control input-sm" name="no_hp" id="no_hp" required/>
								<span>Pastikan nomor handphone aktif</span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4">Email: </label> 
								<div class="col-md-8">
								<input type="email" autocomplete="off" class="form-control input-sm" name="email" id="email" onkeypress="return event.charCode != 32"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4">Bank Tujuan: <span class="noempaty">*</span></label> 
								<div class="col-md-8">
									<select class="form-control input-xs" name="bank_tujuan" id="bank_tujuan">
										<?php if($dt_bank_transfer){ 
		                                    foreach($dt_bank_transfer as $dbt){
		                                ?>
                                    		<option value="<?php echo encrypt($dbt->bank_id)?>"><?php echo $dbt->nama_bank.' ('.$dbt->nomor_rekening.')' ?></option>
                                		<?php } }?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="input2" class="col-sm-4">Tanggal Pembayaran</label>
								<div class="col-sm-8">
									<div class="input-group">
										<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
										<input type="date" class="form-control input-sm" name="tgl_bayar" placeholder="dd-mm-yyyy" required >
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4">Jumlah Bayar: <span class="noempaty">*</span></label> 
								<div class="col-md-8">
									<input type="input" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" class="form-control input-sm" name="jumlah_bayar" id="jumlah_bayar" required/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4">Nama Pengirim / Pemilik rekening: <span class="noempaty">*</span></label> 
								<div class="col-md-8">
									<input type="input" class="form-control input-sm" name="nama_pemilik_rek" id="nama_pemilik_rek" required/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4">Keterangan Pembayaran: <span class="noempaty">*</span></label> 
								<div class="col-md-8">
									<textarea class="form-control" name="keterangan" rows="3" placeholder="Tulis hal penting terkait pembayaran" required></textarea>
								</div>
							</div>
							
							<div class="form-group">
								<hr>
								<div class="form-group">
								<label class="col-md-4"></label> 
								<div class="col-md-8">
								<button type="submit" name="daftar" value="daftar" class="btn btn-primary btn-sm full-width">Submit</button>
								</div>
							</div>
						<?php echo form_close();?>
                </div>
			</div>
		</div>
    </div>
</div>