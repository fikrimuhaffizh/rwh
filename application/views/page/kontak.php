<div class="inner-head">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Kontak <span>RWH</span></h1>
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>">Beranda</a></li>
                    <li class="active"><?php echo $page_title ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="contain-wrapp padding-bot30 padding-top30 ">   
        <div class="container">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-8">
                    <div class="title-head">
                        <h4><i class="fa fa-info-circle"></i>&nbsp;Punya pertanyaan terkait Riau Wisata Hati ?</h4>
                        <p>Jangan Sungkan untuk menghubungi kami ! </p>
                    </div>
                </div>
            </div>
            <div class="row">
                
                <div class="col-md-8 col-sm-7">
                    <?php echo form_open('kontak/send_email',array('id'=>"mycontactform",'novalidate'=>"novalidate")); ?>
                        <div class="clearfix"></div>
                        <?php if($this->session->flashdata('email_sent')=='success'){ ?>
                            <div class="alert alert-success" role="alert" data-out="flipOutX">
                                <i class="fa fa-check"></i>
                                <h6 class="title">Email Berhasil Dikrim !</h6>
                                <p>Pertanyaan akan segera diproses oleh Riau Wisata Hati, Terima Kasih</p>
                            </div>
                        <?php } ?>
                        <div class="row wrap-form">
                            <div class="form-group col-md-6 col-sm-6">
                                <h6>Nama</h6>
                                <input type="text" name="name" id="name" class="form-control input-sm required" placeholder="Enter your full Name...">
                                <span data-for="name" class="error"></span>
                            </div>
                            <div class="form-group col-md-6 col-sm-6">
                                <h6>Alamat Email</h6>
                                <input type="email" name="email" id="email" class="form-control input-sm required" placeholder="Enter your email address...">
                                <span data-for="email" class="error"></span>
                            </div>
                            <div class="form-group col-md-12">
                                <h6>Pesan</h6>
                                <textarea name="message" id="message" class="form-control required" placeholder="Write something for us..." rows="9"></textarea>
                                <span data-for="message" class="error"></span>
                            </div>
                            <div class="form-group col-md-12">
                                <input value="Send Message" type="submit" id="submit" class="btn btn-sm btn-primary btn-lg pull-right">
                            </div>
                        </div>
                    </form>
                </div>
                <?php 
                if($kontak){ 
                    foreach($kontak as $row){
                    ?>
                    <br>
                    <div class="col-md-4 col-sm-5">
                        <div class="contact-detail">
                            <div class="contact-sparator"></div>
                            <ul class="list-unstyled">
                                <?php if($row->telp){?>
                                    <li>
                                        <i class="fa fa-phone fa-2x fa-primary"></i>
                                        <span><b>Telephone</b></span>
                                        <p><?php echo $row->telp ?></p>
                                    </li>
                                <?php } ?>
                                <?php if($row->hp){?>
                                    <li>
                                        <i class="fa fa-phone fa-2x fa-primary"></i>
                                        <span><b>Handphone</b></span>
                                        <p><?php echo $row->hp ?></p>
                                    </li>
                                <?php } ?>
                                <?php if($row->faks){?>
                                <li>
                                    <i class="fa fa-print fa-2x fa-primary"></i>
                                    <span><b>Fax</b></span>
                                    <p><?php echo $row->faks ?></p>
                                </li>
                                <?php } ?>
                                <li>
                                    <i class="fa fa-envelope fa-2x fa-primary"></i>
                                    <span><b>Email</b></span>
                                    <p><?php echo $row->email ?></p>
                                </li>
                                <li>
                                    <i class="fa fa-home fa-2x fa-primary"></i>
                                    <span><b>Alamat</b></span>
                                    <p><?php echo $row->alamat ?></p>
                                </li>
                            </ul>
                        </div>
                    </div>  
            
                    <?php   } 
                        }
                    ?> 
                </div>
            
            <div class="clearfix"></div>
            <br>
            <div class="row">
                <div class="col-md-12">
                <div class="title-head"><h4><i class="fa fa-map-marker"></i>&nbsp;We are here !</h4></div>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7979.382926821097!2d101.41998159703593!3d0.45689807031440094!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xdcb29d5086e3975f!2sPT+RIAU+WISATA+HATI!5e0!3m2!1sen!2sid!4v1544482731250" height="450" frameborder="0" style="border:0;width:100%" allowfullscreen></iframe>
                </div>
            </div>  
            </div>
        </div>
    </div>