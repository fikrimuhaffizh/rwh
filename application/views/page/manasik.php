<style type="text/css">
    .title-head{margin-bottom: 10px;margin-top:0px;}
    .line-height18{line-height: 18px}
    .font20{font-size: 25px}
</style>
<div class="inner-head">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4>Jadwal <span>Manasik</span></h4>
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>">Beranda</a></li>
                    <li class="active"><?php echo $page_title ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="contain-wrapp padding-bot30 padding-top30 ">   
    <div class="container">
        <div class="row">
            <?php if($manasik){ ?>
            <div class="col-md-offset-1">
                <?php 
                    $i=0;
                    foreach($manasik as $row){
                        $i++;
                        $tgl = date('d-F-Y H:i',strtotime($row->tgl_manasik));
                        if($i % 2 != 0) echo '<div class="col-md-12">';
                ?>
                        <div class="col-md-6 ">
                            <div class="col-icon absolute-left">
                                <i class="fa fa-calendar  fa-2x icon-square fa-green"></i>
                                <div class="title-head"><h4 class="marginbot5 "><?php echo $row->tempat?></h4></div>
                                <b class="fa-yellow"><?php echo $tgl ?></b>
                            </div>
                            <table class="table">
                                    <tr>
                                        <td style="width:100px">Lokasi : </td>
                                        <td><?php echo $row->tempat ?></td>
                                    </tr>
                                    <tr>
                                        <td>Alamat : </td>
                                        <td><?php echo $row->alamat_tempat ?></td>
                                    </tr>
                                    <tr>
                                        <td>Pembicara : </td>
                                        <td><?php echo $row->pembicara ?></td>
                                    </tr>
                                    <tr>
                                        <td>Keterangan : </td>
                                        <td><?php echo $row->keterangan ?></td>
                                    </tr>
                                </table>
                        </div>
                <?php
                        if($i % 2 == 0) echo '</div>';
                    } ?>
            </div>
            <?php } else { echo '<div class="title-head centered"><h2 class="font20">Maaf, Jadwal Manasik Belum Tersedia</h2></div>'; } ?>
        </div>
    </div>
</div>