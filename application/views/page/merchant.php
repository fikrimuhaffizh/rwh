<style type="text/css">
    .title-head{margin-bottom: 10px;margin-top:0px;}
    .line-height18{line-height: 18px}
    .font20{font-size: 25px}
</style>
<div class="inner-head">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>List <span>Merchant</span></h1>
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>">Beranda</a></li>
                    <li class="active"><?php echo $page_title ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="contain-wrapp padding-bot30 padding-top30 ">   
    <div class="container">
        <div class="row">
            <?php if($merchant){ ?>
            <div class="col-md-offset-1">
                <?php 
                    $i=0;
                    foreach($merchant as $row){

                        if($i == 0) echo '<div class="col-md-12">';
                        $i++;

                ?>
                        <div class="col-md-3">
                            <div class="thumbnail">
                                <div class="caption" style="padding:20px;line-height: 18px;text-align: left">
                                    <a href="javascript:;"><img class="img-responsive" src="<?php echo $row->img_link ?>"></a>
                                    <hr>
                                    <span><?php echo $row->deskripsi ?></span>
                                </div>
                            </div>
                        </div>            
            <?php  if($i == 4) {echo '</div>';$i=0;}; } ?> 
            </div>

            <?php } else { echo '<div class="title-head centered"><h2 class="font20">Maaf, Merchant Belum Tersedia</h2></div>'; } ?>
        </div>
    </div>
</div>