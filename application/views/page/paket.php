<?php 
$x = explode('Paket ',$page_title);

if($page_title=='Paket Haji Plus'){
	$jenis = "haji";
	$image = base_url().'assets/frontend/img/header/haji.jpg';
	$kota = FALSE;
	$bulan = FALSE;
	$tahun = FALSE;
}
else {
	if($page_title=='Paket Wisata Islam'){
		$jenis = 'wisata';
		$image = base_url().'assets/frontend/img/header/wisata.jpg';
		$kota = $atribut_form_wisata['berangkat_awal'];
		$bulan = $atribut_form_wisata['bulan_berangkat'];
		$tahun = $atribut_form_wisata['tahun_berangkat'];
	}
	else if($page_title=='Paket Umroh'){
		$jenis = 'umroh';
		$image = base_url().'assets/frontend/img/header/umroh.jpg';
		$kota = $atribut_form_umroh['berangkat_awal'];
		$bulan = $atribut_form_umroh['bulan_berangkat'];
		$tahun = $atribut_form_umroh['tahun_berangkat'];
	}

	//Hasil Pencarian Text
	$result_dt_bulan = 'Semua Bulan';
	if($dt_bulan != 'Semua Bulan') {
		$result_dt_bulan = $list_bulan[$dt_bulan];
	}

	$result_dt_berangkat_awal = 'Semua Keberangkatan';
	if($kota){
		foreach($kota as $kt){
			if($kt->kota_id==$dt_berangkat_awal)
				$result_dt_berangkat_awal = $kt->nama_kota;
		}
	}
}

?>
<style type="text/css">
	.inner-head{
		background-repeat:no-repeat;
		background-position:center center;
		background-size:cover;
		background-image:url(<?php echo $image;?>);
	}
	.search-paket-box{padding: 15px;border:5px solid #333;}
	.font16{font-size:16px}
	.font20{font-size:20px;}
	.font21{font-size:21px;}
	.font22{font-size:22px;}
	.full-width{width:100%;}
	.bg-greenOld{background: #349b34;}
	.bg-grey{background: grey; color:white;}

	.cl-greenOld{color: #349b34;}
	.cl-grey{color: grey;}

	.list-paket{border-bottom:1px solid #ddd;margin:0px}
	.item a{text-decoration: none}
	.disable {pointer-events: none !important;
       cursor: default;
       opacity: 0.2;}
</style>
<div class="inner-head">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 style="color:white"> Paket<span style="color:white"> <?php echo $x[1] ?></span></h1>
				<ol class="breadcrumb">
					<li ><a style="color:white" href="<?php echo base_url(); ?>">Beranda</a></li>
					<li class="active"><span style="color:white"><?php echo $page_title ?></span></li>
				</ol>
			</div>
		</div>
	</div>
</div>

<div class="clearfix"></div>

<div class="contain-wrapp padding-bot30 padding-top20 ">	
	<div class="container">
	<?php 
		if($jenis=='haji'){ 
		// echo_array($paket);
			if($paket){
		?>
	        <div class="contain-wrapp paddingbot-clear padding-top20">   
	            <div class="container">	            	
	                <div class="row">
	                    <div class="col-md-12 owl-column-wrapp">
	                        <div id="recent-column3" class="owl-carousel ">
	                            <?php foreach($paket as $row){ ?>
	                                <div class="item">
	                                    <a href="<?php echo base_url().'paket/detail/haji/'.encrypt($row->paket_id).'/'.link_replace($row->nama_paket)?>">
	                                    <div class="wrap-team standard">
	                                        <div class="img-wrapper">
	                                            <img src="<?php echo base_url().$row->media_link ?>" class="img-responsive"  alt="<?php echo $row->nama_paket?>" />
	                                        </div>
	                                        <div class="content">
	                                            <h5 class="name">
	                                                <?php echo $row->nama_paket ?><br>
	                                                <span><?php echo $row->harga_terakhir ?></span><br>
	                                                <i class="fa fa-refresh"></i> Masa Tunggu <?php echo $row->masa_tunggu ?> Tahun
	                                            </h5>

	                                        </div>
	                                        <div class="share">
	                                            <ul>
	                                                <li><a class="btn btn-sm btn-primary" href="<?php echo base_url().'paket/detail/haji/'.encrypt($row->paket_id).'/'.link_replace($row->nama_paket)?>"><i class="fa fa-plane"></i> Detail Paket</a></li>
	                                            </ul>
	                                        </div>
	                                    </div>
	                                    </a>
	                                </div>
	                            <?php } ?>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	<?php } else { ?>
				<div class="col-md-6 col-md-offset-3">
					<div class="alert alert-warning" role="alert" data-out="fadeOutUp">
						<i class="fa fa-warning"></i> 
						<h6 class="title">Oopss !!!</h6>
						<p>Saat Ini Paket Tidak Tersedia</p>
					</div>
				</div>
	<?php }
		}
		else { ?>
			<div class="row">
				<div class="col-md-10  col-md-offset-1 search-paket-box">
					<?php echo form_open($jenis.'/cari', array('id'=>'add-form')); ?>
						<div class="col-md-3">
		                    <h6 class="padding-bot10 padding-top10"><i class="fa fa-plane"></i> Kota Berangkat</h6>
		                    <select class="form-control" name="keberangkatan_<?php echo $jenis ?>">
		                        <option value="Semua Keberangkatan">Semua Kota</option>
		                        <?php foreach($kota as $kntrcbg){ ?>
		                            <option value="<?php echo $kntrcbg->kota_id ?>" <?php if($kntrcbg->kota_id==encrypt($dt_berangkat_awal)){echo 'selected';} ?> ><?php echo $kntrcbg->provinsi ?></option>
		                        <?php } ?>
		                    </select>
		                </div>
		                <div class="col-md-3">
		                    <h6 class="padding-bot10 padding-top10"><i class="fa fa-calendar"></i> Bulan Berangkat</h6>
		                    <select class="form-control" name="bulan_<?php echo $jenis ?>">
		                        <option value="Semua Bulan">Semua Bulan</option>
		                        <?php foreach($bulan as $bln){ ?>
		                            <option value="<?php echo $bln->bulan_berangkat ?>" <?php if($bln->bulan_berangkat==$dt_bulan){echo 'selected';} ?> ><?php echo $bln->bulan?></option>
		                        <?php } ?>
		                    </select>
		                </div>
		                <div class="col-md-3">
		                    <h6 class="padding-bot10 padding-top10"><i class="fa fa-calendar-minus-o"></i> Tahun Berangkat</h6>
		                    <select class="form-control" name="tahun_<?php echo $jenis ?>">
		                        <option value="Semua Tahun">Semua Tahun</option>
		                        <?php foreach($tahun as $thn){?>
		                            <option value="<?php echo $thn->tahun_berangkat ?>" <?php if($thn->tahun_berangkat==$dt_tahun){echo 'selected';} ?> ><?php echo $thn->tahun_berangkat?></option>
		                        <?php } ?> 
		                    </select>
		                </div>
		                <div class="col-md-3">
		                    <h6 class="padding-bot10 padding-top10">&nbsp;</h6>
		                    <button class="btn btn-green btn-icon full-width" type="submit">Cari Paket <i class="fa fa-search"></i></button>
		                </div>
					<?php echo form_close();?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1 padding-top20 padding-bot10">
					<span class="font15">Hasil Pencarian : 
						<b class="font20"> <?php echo $page_title.' | <b>'.$result_dt_berangkat_awal.'</b> - <b>'.$result_dt_bulan.'</b> - <b>'.$dt_tahun.'</b>' ?></b>
					</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1 padding-clear">
					<ul class="list-icons link-list">
						<?php if($paket){//echo_array($paket);
							foreach($paket as $row){
								$img_maskapai = [];
								$paketwaktu_id = $row->paketwaktu_id;
								$paket_id = $row->paket_id;

								//Keberangkatan
									$berangkat_awal = '<b>'.$row->nama_kota.'</b>';
									
								//Maskapai
									$y = $this->md_paket_maskapai->getPaketMaskapaiByPaketId($paket_id);
									foreach($y as $mask){
										$temp = $this->md_maskapai->getMaskapaiById($mask->maskapai_id);
										$y = $this->md_media_detail	->getMediaDetailByMediaId($temp[0]->media_id);
										$img_maskapai[] = base_url().$y[2]->media_link;
									}

								$now = date('Y-m-d H:i:s');
								$berakhir = $row->akhir_publish;

								
						?>
							<li class="list-paket">
								<a href="<?php echo base_url().'paket/detail/'.encrypt($paketwaktu_id).'/'.link_replace($row->nama_paket)?>" class="<?php echo $now < $berakhir ? '' : 'disable' ?>">
									<div class="col-md-12 col-sm-12 col-xs-12 padding-top10">
										<div class="col-md-1 padding-top10 padding-bot5 <?php echo $now < $berakhir ? 'bg-greenOld' : 'bg-grey' ?>" style="color:white;">
											<span class="font20"><?php echo $bulan_short[$row->bulan_berangkat] ?></span> 
											<span class="font20"><?php echo $row->tahun_berangkat ?></span>
										</div>
										<div class="col-md-5 col-sm-6">
											<span class="font20"><?php echo $row->nama_paket ?></span><br>
											<span class="font16"><i class= "fa fa-plane"></i> <?php echo $berangkat_awal ?></span>&nbsp;
											<i class="fa fa-calendar-minus-o"></i>&nbsp;<?php echo $row->lama_hari ?> Hari Perjalanan
										</div>
										
										<div class="col-md-6 col-sm-6 col-xs-12 padding-clear">
											<div class="col-md-12 pull-left">
												<?php for($i=0;$i<count($img_maskapai);$i++){ ?>
														<img src="<?php echo $img_maskapai[$i] ?>" style="width:15%">
												<?php } ?>
											</div>
											<div class="col-md-12 pull-right" style="margin-top:5px">
												<?php if($row->promo=='Ya'){ ?><button class="btn btn-xs btn-bordered btn-red pull-right <?php echo $row->promo ? '' : 'hide' ?>">PROMO</button><br><?php } ?>
													<div class="clearfix"></div>
												<span class="pull-right">Mulai dari : <b class="font21 color-primary <?php echo $now < $berakhir ? 'cl-greenOld' : 'cl-grey' ?>"><?php echo rupiah_format($row->harga) ?></b></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li><div class="clearfix"></div></li>
					<?php  } 
					echo '<li>'.$link.'</li>';
						} else { ?> 
						<div class="col-md-6 col-md-offset-3">
							<div class="alert alert-warning" role="alert" data-out="fadeOutUp">
								<i class="fa fa-warning"></i> 
								<h6 class="title">Oopss !</h6>
								<p>Saat Ini Paket Tidak Tersedia</p>
							</div>
						</div>
					<?php  } ?>
					</ul>
				</div>
			</div>

	<?php } ?>
	</div>
</div>
