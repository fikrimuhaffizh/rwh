<style type="text/css">
	.caption-bulan{font-size: 20px}
	.caption-warning{font-size: 20px}
	.full-width{width: 100%}
	.quarter-width{width: 25%}
	.font20{font-size:28px;}
	.font15{font-size:15px;}
	.title-head{margin-bottom: 25px}
	.img-wrapper{text-align: center}
	.product-wrapper.product-list{border-bottom: 0px}
</style>
<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<link href="<?php echo base_url()?>assets/backend/css/star-rating.min.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url()?>assets/backend/js/star-rating.min.js" type="text/javascript"></script>
<script type="text/javascript">
	var count = 1;
	function add_jemaah(){
		jenis = $('#jenis').val();

		$('.btn-hapus').show();
		count++;
		var x  = '<div class="data_jemaah_'+count+'"><div class="clearfix"></div><div class="title-head"><h5>Data Jemaah '+count+'</h5></div>';
			x +='<div class="row">'
			x +=' <div class="col-md-6">';
			x +=' 		<div class="form-group">';
			x +=' 			<label for="input1" class="col-sm-3">Nama&nbsp;<span class="noempaty">*</span></label>';
			x +=' 			<div class="col-sm-9">';
			x +=' 				<input type="text" class="form-control" name="nama_jemaah_'+count+'" id="nama_jemaah_'+count+'" required>';
			x +=' 			</div>';
			x +=' 		</div>';
			x +=' 	</div>';
			x +='	<div class="col-md-6">';
			x +='		<div class="form-group">';
			x +='			<label class="col-sm-3">Tanggal Lahir&nbsp;<span class="noempaty">*</span></label>';
			x +='			<div class="col-sm-9">';
			x +='				<div class="input-group">';
			x +='					<div class="input-group-addon"><i class="fa fa-calendar"></i></div>';
			x +='					<input type="date" class="form-control" name="tgl_lahir_'+count+'" id="tgl_lahir__'+count+'" placeholder="dd-mm-yyyy" required>';
			x +='				</div>';
			x +='			</div>';
			x +='		</div>';
			x +='	</div>';			
			x +='</div>';
			x +='<div class="row">'
			x +=' 	<div class="col-md-6">';
			x +=' 		<div class="form-group">';
			x +=' 			<label class="col-sm-3">Jenis Kelamin&nbsp;<span class="noempaty">*</span></label>';
			x +=' 			<div class="col-sm-9">';
			x +=' 				<select required class="form-control" name="jenis_kelamin_'+count+'" id="jenis_kelamin_'+count+'">';
			x +=' 					<option value=""></option>';
			x +=' 					<option value="Laki-Laki">Laki-Laki</option>';
			x +=' 					<option value="Perempuan">Perempuan</option>';
			x +=' 				</select>';
			x +=' 			</div>';
			x +=' 		</div>';
			x +=' 	</div>';			
			x +=' 	<div class="col-md-6">';
			x +=' 		<div class="form-group">';
			x +=' 			<label for="input3" class="col-sm-3">Email&nbsp;<span class="noempaty">*</span></label>';
			x +=' 			<div class="col-sm-9">';
			x +=' 				<input type="text" class="form-control" name="email_'+count+'" id="email_'+count+'" required>';
			x +=' 			</div>';
			x +=' 		</div>';
			x +=' 	</div>';
			x +='</div>';			
			x +='<div class="row">'
			x +=' 	<div class="col-md-6">';
			x +=' 		<div class="form-group">';
			x +=' 			<label for="input2" class="col-sm-3">Handphone&nbsp;<span class="noempaty">*</span></label>';
			x +=' 			<div class="col-sm-9">';
			x +=' 				<input type="text" class="form-control" name="no_hp_'+count+'" id="no_hp_'+count+'" onkeypress="return event.charCode >= 45 && event.charCode <= 57" id="input2" required>';
			x +=' 			</div>';
			x +=' 		</div>';
			x +=' 	</div>';	
			x +=' 	<div class="col-md-6">';
			x +=' 		<div class="form-group">';
			x +=' 			<label for="input12" class="col-sm-3">Alamat&nbsp;<span class="noempaty">*</span></label>';
			x +=' 			<div class="col-sm-9">';
			x +=' 				<textarea class="form-control" rows="3" name="alamat_'+count+'" id="alamat_'+count+'" required></textarea>';
			x +=' 			</div>';
			x +=' 		</div>';
			x +=' 	</div>';		
			x +='</div>';

			if(jenis=='Reguler')
				x +='<div class="row">';
			else
				x +='<div class="row hide">';
				x +='	<div class="col-md-12">';
				x +='		<div class="form-group">';
				x +='			<label class="col-sm-2">Jenis Pendaftaran&nbsp;<span class="noempaty">*</span></label>';
				x +='			<div class="col-sm-9">';
				x +='				<div class="radio">';
				x +='					<label>';
				x +='						<input type="radio" name="jenis_pendaftaran_'+count+'" id="reguler_'+count+'" value="Reguler" checked>';
				x +='						Reguler';
				x +='					</label>&nbsp; ';
				x +='					<label>';
				x +='						<input type="radio" name="jenis_pendaftaran_'+count+'" id="1pasti_'+count+'" value="1PASTI">';
				x +='						1PASTI';
				x +='					</label>&nbsp; <button class="btn btn-xs btn-red btn-bordered" type="button" data-toggle="modal" data-target="#jnspndftr_modal">Apa itu 1PASTI&nbsp;<i class="fa fa-question-circle"></i></button>';
				x +='				</div>';
				x +='			</div>';
				x +='		</div>';
				x +='	</div>';
				x +='</div>';
			

			$('.more_jemaah').before(x);
	} 

	function remove_jemaah(){
		$('.data_jemaah_'+count).remove();
		if(count!=1)
			count--;
		if(count==1)
			$('.btn-hapus').hide();
	}

    function cancel_kode(){
    	$('#kode_promo_temp').val("");
    	$('#btn-sumbit-form-daftar').removeAttr('disabled');
    	$('.btn-kode-cancel').hide();
    	$('#kode_tidak_valid').addClass('hide');
    	$('#kode_valid').addClass('hide');
    }
    function step1(){
    	$('.step1').toggle();
    	$('.step2').toggle();
        setTimeout("goToByScroll('start')",10);
    }

    function step2(task=""){
    	if(task=='1_2'){
	    	$('.step2').toggle();
	    	$('.step1').toggle();
    	}
    	else if(task=="2_1"){
    		$('.step2').toggle();
    		$('.step3').toggle();
    	}
        setTimeout("goToByScroll('start')",10);
    }

    function step3(){
    	empty=0;
        $('input', '#add-form').each(function(){
            if($(this).val() == "" && $(this).attr('id') 
            	&& $(this).attr('id')!='kode_promo' 
            	&& $(this).attr('id')!='kode_promo_temp'
            	&& $(this).attr('id')!='keberangkatan_id') {
            	$(this).css('border-color' , '#349b34');
            	alert($(this).attr('id'));
                empty++;
            }else{
		        $(this).css('border-color' , '#c1c1c1');
		    }
        })
        $('select', '#add-form').each(function(){
            if($(this).val() == "" && $(this).attr('id')){
            	$(this).css('border-color' , '#349b34');
            	// alert($(this).attr('id'));
                empty++;
            }else{
		        $(this).css('border-color' , '#c1c1c1');
		    }
        })
        $('textarea', '#add-form').each(function(){
            if($(this).val() == "" && $(this).attr('id')){
            	$(this).css('border-color' , '#349b34');
            	// alert($(this).attr('id'));
                empty++;
            }else{
		        $(this).css('border-color' , '#c1c1c1');
		    }
        })
        if(empty>0)
        	return alert('Silahkan cek kembali Form Jemaah. Form Tidak Boleh Kosong');

        $('.step3').toggle();
    	$('.step2').toggle();
        getDetailJemaah();
        setTimeout("goToByScroll('start')",10);
    }

    function goToByScroll(id){
	    $('html,body').animate({scrollTop: $("."+id).offset().top},'slow');
	}

    function getDetailJemaah(){
    	$('.sum_jemaah').remove();
    	for(var i=1;;i++){
    		nama = $('#nama_jemaah_'+i).val();
    		jenis_kelamin = $('#jenis_kelamin_'+i).val();
    		no_hp = $('#no_hp_'+i).val();
    		email = $('#email_'+i).val();
    		alamat = $('#alamat_'+i).val();
    		jenis_pendaftaran = $('#jenis_pendaftaran_'+i).val();
    		if(nama){
    			if(i==1){
	    			$('.sum_nama_'+i).text(nama);
	    			$('.sum_jk_'+i).text(jenis_kelamin);
	    			$('.sum_no_hp_'+i).text(no_hp);
	    			$('.sum_email_'+i).text(email);
	    			$('.sum_alamat_'+i).text(alamat);
	    			$('.sum_jenis_dftr_'+i).text(jenis_pendaftaran);
    			} else {
    				var x = '<div class="sum_jemaah"><h5 class="head-title">Jemaah '+i+'</h5>';
    					x  += '<table class="table">';
						x  += '<tr><td style="width:120px">Nama</td><td>:</td><td><span class="sum_nama_"'+i+'>'+nama+'</span></td></tr>';
						x  += '<tr><td>Jenis Kelamin</td><td>:</td><td><span class="sum_jk_"'+i+'>'+jenis_kelamin+'</span></td></tr>';
						x  += '<tr><td>Handphone</td><td>:</td><td><span class="sum_no_hp_"'+i+'>'+no_hp+'</span></td></tr>';
						x  += '<tr><td>Email</td><td>:</td><td><span class="sum_email_"'+i+'>'+email+'</span></td></tr>';
						x  += '<tr><td>Alamat</td><td>:</td><td><span class="sum_alamat_"'+i+'>'+alamat+'</span></td></tr>';
						x  += '</table></div>';
					$('.more_sum_jemaah').before(x);
    			}
    		} else break;

    	}
    }

    $(document).ready(function(){
        $('.step2').hide();
        $('.step3').hide();
        $('.btn-hapus').hide();
        $('.btn-kode-cancel').hide();

    })
</script>
<?php 
	if($this->session->flashdata('booking_done')){
		$x = $this->session->flashdata('booking_done');
?>
	<div class="contain-wrapp padding-bot30 padding-top30 " >
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<aside>
						<div class="order-detail">
							<div class="widget">
								<h5 class="widget-head"><i class="fa fa-user"></i> Hai <?php echo $this->session->userdata('nama') ?></h5>
								<div class="title-head centered">
									<h2 class="">Booking Berhasil !</h2>
									<p>Terima kasih sudah menggunakan layanan Riau Wisata Hati Service.  Informasi lebih lanjut silahkan hubungi kantor pusat</p>
			                    </div>
								<a href="<?php echo base_url().'member/detail/pdf_pendaftaran/'.$x['pendaftaran_id'] ?>" class="btn btn-primary btn-sm pull-left"><i class="fa fa-print"></i> Cetak Detail Booking</a>
								<a href="<?php echo base_url().'member' ?>" class="btn btn-primary btn-sm  pull-right">Menuju Dashboard <i class="fa fa-user"></i></a>
							</div>
						</div>
					</aside>
				</div>
			</div>
		</div>
	</div>
<?php } else if($paket_detail){?>
	<div class="inner-head">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Paket <span><?php echo $paket_detail['nama_paket'] ?></span></h1>
					<ol class="breadcrumb">
	                    <li><a href="<?php echo base_url(); ?>">Beranda</a></li>
	                    <li class="active"><?php echo $page_title ?></li>
	                </ol>
				</div>
			</div>
		</div>
	</div>
	<div class="contain-wrapp padding-bot30 padding-top30 " >	
		<div class="container">
			<div class="row">
				
					<?php if($booking){ 
						$reguler=FALSE;
						$haji = FALSE;
						$harga='';
						$pwk_id='';
						$keberangkatan ='';
						if($paketwaktu_kelas){
							foreach($paketwaktu_kelas as $row2){
	 								$keberangkatan =  $bulan[$row2->bulan_berangkat].' '.$row2->tahun_berangkat; 
								$pwk_id = encrypt($row2->pwk_id);
								$paket_id = encrypt($row2->paket_id);
								$harga = $row2->harga;
								$reguler = TRUE;	
							}
						} else {
							$haji = TRUE;
							$harga = $paket_detail['harga_terakhir'];
						}
					?>
						<div class="col-md-12">
							<div class="row">
								<?php echo form_open('booking', array('id'=>'add-form','class'=>'form-horizontal')); ?>
									<div class="col-md-6 col-sm-12 col-md-offset-3">
					                    <div class="step1">
					                    	<div class="title-head"><h5>Syarat dan Ketentuan</h5></div>
											<div class="col-md-12">
												<ul>
													<li>
														<p class="head"><strong>Persyaratan <?php echo $haji ? 'Haji' : 'Umroh'; ?></strong></p>
														<ol type="a">
															<li>Sehat Jasmani dan Rohani</li>
															<li>Jemaah beresiko tinggi & usia >60 Tahun WAJIB di dampingi keluarga yang sehat</li>
															<li>Passport RI yang masih berlaku Min. 7 Bulan dengan 3 Suku Kata</li>
															<li>Pas Photo 4x6= 10 Lembar (tampak muka fokus 80%)</li>
															<li>Fotocopy KK & KTP</li>
															<li>Buku Vaksin Meningkitis</li>
															<li>Surat Nikah asli bagi suami istri</li>
															<li>Akte lahir asli + KK asli bagi anak-anak </li>
														</ol>
													</li>
													<li>
														<p class="head"><strong>Biaya TERMASUK</strong></p>
														<ol type="a">
															<li>Program Kajian Islam Intensif</li>
															<li>Tiket Peswawat Terbang - Airport Tax (PP)</li>
															<li>Visa Perjalanan Umroh</li>
															<li>Makan 3x sehari selama ditanah suci</li>
															<li>Manasik 2x</li>
															<li>Full Ziarah Madinah dan Makkah</li>
															<li>Buss Full AC</li>
															<li>Air Zam-Zam 5 liter</li>
															<li>Perlengkapan Umroh (tracel bag, tas passport, ihram/mukenah, seragam, sajadah dll</li>
														</ol>
													</li>
													<li>
														<p class="head"><strong>Biaya TIDAK TERMASUK</strong></p>
														<ol type="a">
															<li>Pembuatan pasport/perpanjang masa masa berlaku</li>
															<li>Suntik Miningkitis</li>
															<li>Kelebihan Bagasi</li>
															<li>Keperluan Pribadi Selama di Tanah Suci</li>
															<li>Biaya SR 2.000,- (dua ribu real) jika jemaah berangkat lebih dari sekali dalam satu tahun kalender hijjriah</li>
															<li>Full Ziarah Madinah dan Makkah</li>
															<li>Buss Full AC</li>
															<li>Air Zam-Zam 5 liter</li>
															<li>Perlengkapan Umroh (tracel bag, tas passport, ihram/mukenah, seragam, sajadah dll</li>
														</ol>
													</li>
												</ul>
						                    </div>
											<button class="btn btn-primary btn-sm pull-right full-width" type="button" onClick="step2('1_2');">Setuju dan Lanjut Isi data&nbsp;<i class="fa fa-arror-right"></i></button>
					                    	<hr>
					                    </div>
					                </div>

									<div class="col-md-10 col-sm-12 col-md-offset-1">
					                    <div class="step2">
					                    	<div class="alert alert-warning" role="alert" data-out="fadeOutUp"><span class="close-alert"><i class="fa fa-times-circle"></i></span>
						                        <i class="fa fa-info-circle"></i> 
						                        <p>Pastikan data yang diisi <b>BENAR</b> dan menggunakan data <b>TERBARU</b>, agar tidak terjadi kesalahan dalam pendataan jemaah kedepannya.</p>
						                    </div>
											<div class="data_jemaah_1'">
												<div class="title-head"><h5>Data Jemaah</h5></div>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label for="input1" class="col-sm-3">Nama&nbsp;<span class="noempaty">*</span></label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="nama_jemaah_1" id="nama_jemaah_1" required>
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="col-sm-3">Tanggal Lahir&nbsp;<span class="noempaty">*</span></label>
															<div class="col-sm-9">
																<div class="input-group">
																	<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
																	<input type="date" class="form-control" name="tgl_lahir_1" id="tgl_lahir_1" placeholder="dd-mm-yyyy" required>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="col-sm-3">Jenis Kelamin&nbsp;<span class="noempaty">*</span></label>
															<div class="col-sm-9">
																<select required class="form-control" name="jenis_kelamin_1" id="jenis_kelamin_1">
																	<option value=""></option>
																	<option value="Laki-Laki">Laki-Laki</option>
																	<option value="Perempuan">Perempuan</option>
																</select>
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label for="input3" class="col-sm-3">Email&nbsp;<span class="noempaty">*</span></label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="email_1" id="email_1" required>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label for="input2" class="col-sm-3">Handphone&nbsp;<span class="noempaty">*</span></label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="no_hp_1" id="no_hp_1" onkeypress='return event.charCode >= 45 && event.charCode <= 57' required>
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="col-sm-3">Alamat&nbsp;<span class="noempaty">*</span></label>
															<div class="col-sm-9">
																<textarea class="form-control" rows="3" name="alamat_1" id="alamat_1" required></textarea>
															</div>
														</div>
													</div>
												</div>
												<div class="row <?php echo $haji ? 'hide' : ''; ?>">
													<div class="col-md-12" >
														<div class="form-group">
															<label class="col-sm-2">Jenis Pendaftaran&nbsp;<span class="noempaty">*</span></label>
															<div class="col-sm-9">
																<div class="radio">
																	<label>
																		<input type="radio" name="jenis_pendaftaran_1" id="jenis_pendaftaran_1" value="Reguler" checked>
																		Reguler
																	</label>&nbsp; 
																	<label>
																		<input type="radio" name="jenis_pendaftaran_1" id="jenis_pendaftaran_1" value="1PASTI">
																		1PASTI
																	</label>&nbsp; <button class="btn btn-xs btn-red btn-bordered" type="button" data-toggle="modal" data-target="#jnspndftr_modal">Apa itu 1PASTI&nbsp;<i class="fa fa-question-circle"></i></button>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="more_jemaah"></div>
											<div class="clearfix"></div>
											<hr>
											<div class="col-md-12">
												<div class="col-md-12">
													<div class="form-group">
														<span><i class="fa fa-info-circle fa-green"></i>&nbsp;Ingin menambah jemaah ? Klik&nbsp;</span>
														<button onClick="add_jemaah()" class="btn btn-xs btn-primary" type="button">Tambah Jemaah&nbsp;<i class="fa fa-plus"></i></button>
														<button onClick="remove_jemaah()" class="btn-hapus btn btn-xs btn-red" type="button">Hapus&nbsp;<i class="fa fa-trash"></i></button>
													</div>
												</div>
											</div>
											<div class="clearfix"></div>
											<hr>
											<button class="btn btn-yellow quarter-width btn-sm" onClick="step1();" type="button">Back</button>
											<button class="btn btn-primary quarter-width btn-sm pull-right" onClick="step3();" type="button">Next</button>
										</div>
									</div>
									
									<div class="col-md-6 col-sm-12 col-md-offset-3">
										<div class="step3">

											<div class="title-head"><h5>Summary</h5></div>
											<span>Berikut paket yang akan dibooking dan data jemaah yang mendaftar. Pastikan data jemaah <b>VALID</b> dan <b>BENAR</b></span><br>
											<hr>
											<?php if($reguler){ ?>
												<table class="table">
													<tbody>
														<tr>
															<td><strong>Nama Paket</strong></td>
															<td><b class="green"><?php echo $paket_detail['nama_paket'] ?></b></td>
														</tr>
														<tr>
															<td><strong>Kelas</strong></td>
															<td><b class="green"><?php echo '<i class="fa fa-users"></i>&nbsp;'.$row2->kelas.', '.$row2->jumlah_sekamar.' orang per kamar' ?></b> </td>
														</tr>
														<tr>
															<td><strong>Lama Perjalanan</strong></td>
															<td><b class="green"><?php echo $paket_detail['lama_hari'] ?></td>
														</tr>
														<tr>
															<td><strong>Keberangkatan</strong></td>
															<td><b class="green"><?php echo '<i class="fa fa-plane"></i>&nbsp;'.$paket_detail['kota_berangkat_awal'].'&nbsp; <i class="fa fa-calendar"></i>&nbsp;'.$keberangkatan ?> </td>
														</tr>
														<tr>
															<td><strong>Harga Paket</strong></td>
															<td><b class="green"><?php echo rupiah_format($row2->harga) ?></b></td>
														</tr>
													</tbody>
												</table>
												<!-- kota_berangkat_awal menjadi kantorcabang_id dikolom pendaftaran_detail -->
												<input type="hidden" name="kantorcabang_id" id="kantorcabang_id" value="<?php echo $paket_detail['berangkat_awal']; ?>">
												<input type="hidden" name="pwk_id" id="pwk_id" value="<?php echo $pwk_id; ?>">
												<input type="hidden" name="paket_id" id="paket_id" value="<?php echo $paket_id; ?>">
												<input type="hidden" id="jenis" name="jenis" value="Reguler">
											<?php } else if ($haji){ ?>
												<table class="table">
													<tbody>
														<tr>
															<td><strong>Nama Paket</strong></td>
															<td><b class="green"><?php echo $paket_detail['nama_paket'] ?></b></td>
														</tr>
														<tr>
															<td><strong>Masa Tunggu</strong></td>
															<td><b class="green"><?php echo '<i class="fa fa-refresh"></i>&nbsp; '.$paket_detail['masa_tunggu'].' Tahun' ?></b> </td>
														</tr>
														<tr>
															<td><strong>Harga Paket</strong></td>
															<td><b class="green"><?php echo rupiah_format($paket_detail['harga_terakhir']) ?></b></td>
														</tr>
													</tbody>
												</table>
												<input type="hidden" name="paket_id" id="paket_id" value="<?php echo $paket_detail['paket_id']; ?>">
												<input type="hidden" name="masa_tunggu" id="masa_tunggu" value="<?php echo $paket_detail['masa_tunggu']; ?>">
												<input type="hidden" id="jenis" name="jenis" value="Haji">
											<?php } ?>

											<div class="form-wrapp padding-top20 padding-bot20">
												<div class="sum_jemaah_1'">
													<h5 class="head-title">Jemaah 1</h5>
													<table class="table">
														<tr><td style="width:120px">Nama</td><td>:</td><td><span class="sum_nama_1">-</span></td></tr>
														<tr><td>Jenis Kelamin</td><td>:</td><td><span class="sum_jk_1">-</span></td></tr>
														<tr><td>Handphone</td><td>:</td><td><span class="sum_no_hp_1">-</span></td></tr>
														<tr><td>Email</td><td>:</td><td><span class="sum_email_1">-</span></td></tr>
														<tr><td>Alamat</td><td>:</td><td><span class="sum_alamat_1">-</span></td></tr>
													</table>
													<span class="more_sum_jemaah"></span>
												</div>
											</div>
											<div class="clearfix"></div>
											<hr>
											<button class="btn btn-yellow quarter-width btn-sm" onClick="step2('2_1');" type="button">Back</button>
											<button id="btn-sumbit-form-daftar" class="btn btn-primary quarter-width btn-sm pull-right btn-sumbit-form-daftar">Submit</button>
										</div>
									</div>
									<input type="hidden" name="keberangkatan_id" id="keberangkatan_id" value="<?php echo $keberangkatan_id ?>">
									<input type="hidden" name="kode_promo" id="kode_promo">
									<input type="hidden" name="harga" id="harga" value="<?php echo $harga?>">
									<input type="hidden" name="nama_paket" id="nama_paket" value="<?php echo $paket_detail['nama_paket']; ?>">
								<?php echo form_close();?>
							</div>
						</div>

					<?php }  else if($paket_detail['jenis_paket']=='Haji'){ ?>
								<div class="col-md-12 col-md-offset-1">
									<div class="product-wrapper product-list padding-bot20 marginbot-clear">
										<div class="col-md-4 col-sm-12 col-xs-12" style="padding-left:15px">
											<div class="img-wrapper">
												<a target="_blank" href="<?php echo $paket_detail['paket_pic'] ?>"><img src="<?php echo $paket_detail['paket_pic'] ?>" alt="<?php echo $page_title ?>" /></a>
											</div>
											<hr>
										<a href="javascript:history.go(-1)" class="btn btn-sm btn-yellow pull-right">Kembali <i class="fa fa-arrow-circle-o-left"></i></a>
										</div>

										<div class="col-md-6 col-sm-12 col-xs-12" style="padding-right:15px">
											<div class="product-containt">
												<ul class="list-icons link-list">
													<li>
														<div class="title-head"><h6 class="caption-bulan "><i class="fa fa-map"></i>&nbsp;Detail Haji</h6></div>
														<div class="col-md-12 col-sm-12 col-xs-12 font15">
															
															<div class="col-md-6 col-sm-4 col-xs-12 padding-clear">
																<i class="fa fa-angle-double-right"></i>&nbsp;Jenis Kuota : <?php echo $paket_detail['jenis_kuota'] ?>
															</div>
															<div class="col-md-6 col-sm-4 col-xs-12 padding-clear">
																<i class="fa fa-angle-double-right"></i>&nbsp;Masa Tunggu : <?php echo $paket_detail['masa_tunggu'].' Tahun' ?>
															</div>
														</div>
													</li>
													 <li>
														<div class="title-head"><h6 class="caption-bulan"><i class="fa fa-sticky-note-o"></i>&nbsp;Info Tambahan</h6></div>
														<div class="col-md-12 col-sm-12 col-xs-12 font15">
															<?php echo $paket_detail['info_plus'] ?> 
															</div>
														</li>
														<li>
															<div class="col-md-12 col-sm-12 col-xs-12 ">
															<div class="pull-left"><i class="fa fa-angle-double-right"></i>&nbsp;Biaya Keberangkatan : <h4><?php echo $paket_detail['harga_terakhir'] ?></h4></div>
															<button style="text-align:center" onClick="window.location.href='<?php echo base_url().'detail/form_booking/'.$paket_detail['paket_id'] ?>'" class="btn btn-xs btn-yellow pull-right"><i class="fa fa-shopping-cart"></i> Booking</button>
														</div>
													</li>
												</ul>											
											</div>
										</div>
									</div>
								</div>
					
					<?php }  else { ?>
							<div class="col-md-12 col-md-offset-1">
								<div class="product-wrapper product-list padding-bot20 marginbot-clear">
									<div class="col-md-4 col-sm-12 col-xs-12" style="padding-left:15px">
										<div class="img-wrapper">
											<a target="_blank" href="<?php echo $paket_detail['paket_pic'] ?>"><img src="<?php echo $paket_detail['paket_pic'] ?>" alt="<?php echo $page_title ?>" /></a>
										</div>
										<hr>
									<a href="javascript:history.go(-1)" class="btn btn-sm btn-yellow pull-right">Kembali <i class="fa fa-arrow-circle-o-left"></i></a>
									</div>

									<div class="col-md-6 col-sm-12 col-xs-12" style="padding-left:15px;padding-right:15px;">
										<div class="product-containt">
											<ul class="list-icons link-list">
												<li>
													<div class="title-head">
														<h6 class="caption-bulan "><i class="fa fa-map"></i>&nbsp;Perjalanan</h6>
													</div>
													<div class="col-md-6 col-sm-4 col-xs-12 padding-clear">
														<i class="fa fa-angle-double-right"></i>&nbsp;Kota Berangkat : <?php echo $paket_detail['berangkat_awal'] ?>
													</div>
													<div class="col-md-6 col-sm-4 col-xs-12 padding-clear">
														<i class="fa fa-angle-double-right"></i>&nbsp;Kota Transit : <?php echo $paket_detail['berangkat_transit'] ?>
													</div>
													<div class="col-md-6 col-sm-4 col-xs-12 padding-clear">
														<i class="fa fa-angle-double-right"></i>&nbsp;Kota Landing : <?php echo $paket_detail['berangkat_landing'] ?>
													</div>
													<div class="col-md-6 col-sm-4 col-xs-12 padding-clear">
														<i class="fa fa-angle-double-right"></i>&nbsp;Lama Hari : <b class="red"><?php echo $paket_detail['lama_hari'] ?></b> 
													</div>
												</li>
												
												
												<?php 
													$temp_msk="";
													$count=0;
													foreach($paket_detail['maskapai'] as $row){
													$count++;
													if($temp_msk != 'Maskapai'){
															$list_msk = "";
															$temp_msk = 'Maskapai';
														} else {
															$list_msk = "hide";
														}
												 ?>
											 		<li>
											 			<div class="<?php echo $list_msk ?>">
															<div class="title-head"><h6 class="caption-bulan "><i class="fa fa-plane"></i>&nbsp;Maskapai</h6></div>
															<p>"Kami menggunakan maskapai berkualitas yang akan memberikan kenyamanan jemaah selama dalam perjalan"</p>
														</div>
														<a data-toggle="collapse" data-parent="#accordion" href="#panel_maskapai_<?php echo $count ?>" >
															<i class="fa fa-angle-double-right"></i>&nbsp;<?php echo $row['nama_maskapai']?> 
														</a>
														<div id="panel_maskapai_<?php echo $count ?>" class="panel-collapse collapse">
															<div class="panel-body">
																<div class="col-md-5 col-sm-4 col-xs-12">
																	<img src="<?php echo $row['logo'] ?>" style="width: 120px">
																</div>
																<div class="col-md-6 col-sm-4 col-xs-12">
																	<?php echo $row['deskripsi'] ?> 
																</div>
															</div>
														</div>
													</li>
												<?php  } ?>
													<?php 
														$temp_htl="";$count=0;
														foreach($paket_detail['paket_akomodasi'] as $row){ 
														$count++;
														if($temp_htl != 'Hotel'){
															$list_htl = "";
															$temp_htl = 'Hotel';
														} else {
															$list_htl = "hide";
														}
													?>
												 	<li>
												 		<div class="<?php echo $list_htl ?>">
															<div class="title-head"><h6 class="caption-bulan "><i class="fa fa-building"></i>&nbsp;Hotel</h6></div>
															<p>"Hotel berbintang dan lokasi yang tidak jauh dari mesjid memudahkan jemaah dalam beribadah"</p>
														</div>
														<a data-toggle="collapse" data-parent="#accordion" href="#panel_hotel_<?php echo $count ?>" >
															<i class="fa fa-angle-double-right"></i>&nbsp;<?php echo $row['nama_hotel'].', '.$row['kota']?> 
														</a>
														<div id="panel_hotel_<?php echo $count ?>" class="panel-collapse collapse">
															<div class="panel-body">
																<div class="col-md-5 col-sm-6 col-xs-12">
																	<a target="_blank" href="<?php echo $row['foto_besar'] ?>"><img src="<?php echo $row['foto'] ?>" style="width: 100%"></a>
																</div><br>
																<div class="col-md-6 col-sm-6 col-xs-12">
																	<input type="number" name="bintang" value="<?php echo $row['bintang'] ?>" data-show-clear="false" data-show-caption="false" data-size="xs" class="rating" disabled data-step=1  />
																	<?php echo $row['deskripsi']?> 
																</div>
															</div>
														</div>
													</li>
													<?php } ?>
												 <li>
													<div class="title-head"><h6 class="caption-bulan"><i class="fa fa-sticky-note-o"></i>&nbsp;Info Tambahan</h6></div>
													<div>
														<?php echo $paket_detail['info_plus'] ?> 
													</div>
												</li>
												<li>
												<?php 
													$temp_bln="";
													$list_keberangkatan_temp='';
													$count=0;
													$total_paketwaktu = count($paket_detail['paket_waktu']);
													// echo_array($paket_detail['paket_waktu']);
													foreach ($paket_detail['paket_waktu'] as $row) {
														++$count;
														$promo = FALSE;
														$list_bln = "hide";

														if($row['berangkat'] != $temp_bln)
															$list_bln = "";

														if($row['promo']=='Ya')
															$promo = TRUE;
												?>
													
															<div class="<?php echo $list_bln ?>" >
																<div class="title-head"><h6 class="caption-bulan"><i class="fa fa-calendar"></i>&nbsp;<?php echo $row['berangkat'] ?></h6></div>
																<p>"Wujudkan impian berangkat Umroh yang Mudah, Cepat dan Aman bersama Riau Wisata Hati Service"</p>
															</div>
															<?php
																$list_keberangkatan = $this->md_keberangkatan->getKeberangkatanByPaketWaktuId($row['paketwaktu_id']);
																//Jika sudah ada tanggal keberangkatan, tampilkan booking secara tanggal keberangkatan
																	if($list_keberangkatan){
																		foreach($list_keberangkatan as $per_tgl){
																			$id_keberangkatan = encrypt($per_tgl->keberangkatan_id);
																			if($list_keberangkatan_temp != $per_tgl->tgl_keberangkatan){
																				echo '<div class="col-md-12 font15">
																						<i class="fa fa-angle-double-right"></i> 
																						Berangkat <b>'. date("d-F-Y",strtotime($per_tgl->tgl_keberangkatan)).'</b>
																					</div>
																					';
																			}

																?>
																			<div class="col-md-12" style="margin-left: 20px">
																					<div class="col-md-4 col-sm-6 col-xs-12">
																						<span><?php echo $row['kelas'] ?> </span>&nbsp;(&nbsp;<i class="fa fa-users"></i>&nbsp;<?php echo $row['jumlah_sekamar'] ?> orang )
																					</div>
																					<div class="col-md-8 col-sm-6 col-xs-12">
																						<?php if($row['diskon']){?>
																							<span style="font-size:14px;text-decoration: line-through"><?php echo $row['harga_awal'] ?></span><br>
																							<b class="green" style="font-size:16px"><?php echo $row['harga'] ?></b>
																						<?php } else {?>
																							<b class="green" style="font-size:16px"><?php echo $row['harga'] ?></b>
																						<?php } 
																						if($promo){?>&nbsp;<button style="padding:3px" class="btn btn-xs btn-red btn-bordered">Promo !</button><?php } ?>
																						<button onClick="window.location.href='<?php echo base_url().'detail/form_booking/'.$paket_detail['paket_id'].'/'.$row['pwk_id'].'/'.$id_keberangkatan ?>'" class="btn btn-xs btn-green pull-right"><i class="fa fa-shopping-cart"></i> Booking</button>
																					</div>
																					<div class="clearfix"></div>
																					<?php //echo $total_paketwaktu != $count ? '<hr>' : '<br>'; ?>
																			</div>

															 <?php 		
															 				$list_keberangkatan_temp = $per_tgl->tgl_keberangkatan;
																		} 
																	}

															 	//Jika belum ada, tampilkan booking secara bulan saja
																 	else {?>
																		
																		<div class="col-md-4 col-sm-6 col-xs-12">
																			<b><i class="fa fa-angle-double-right"></i> <?php echo $row['kelas'] ?> </b>&nbsp;(&nbsp;<i class="fa fa-users"></i>&nbsp;<?php echo $row['jumlah_sekamar'] ?> orang )
																		</div>
																		<div class="col-md-8 col-sm-6 col-xs-12">
																			<?php if($row['diskon']){?>
																				<span style="font-size:14px;text-decoration: line-through"><?php echo $row['harga_awal'] ?></span><br>
																				<b class="green" style="font-size:16px"><?php echo $row['harga'] ?></b>
																			<?php } else {?>
																				<b class="green" style="font-size:16px"><?php echo $row['harga'] ?></b>
																			<?php } 
																			if($promo){?>&nbsp;<button style="padding:3px" class="btn btn-xs btn-red btn-bordered">Promo !</button><?php } ?>
																			<button onClick="window.location.href='<?php echo base_url().'detail/form_booking/'.$paket_detail['paket_id'].'/'.$row['pwk_id'] ?>'" class="btn btn-xs btn-green pull-right"><i class="fa fa-shopping-cart"></i> Booking</button>
																		</div>
																		<div class="clearfix"></div>
																		<?php echo $total_paketwaktu != $count ? '<hr>' : '<br>'; ?>
															<?php } ?>
														
													
												<?php 
														$temp_bln	= $row['berangkat'];
													}
												 ?>
												 </li>
											</ul>											
										</div>
									</div>
								</div>
							</div>
					
					<?php } ?>		
			</div>
		</div>
    </div>
<?php } ?>

<div class="modal fade" id="jnspndftr_modal" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" ><span aria-hidden="true">×</span></button>
				<h6 class="modal-title">Program Umroh 1PASTI</h6>
			</div>
			<div class="modal-body">
				<p>1PASTI merupakan program umroh dari Riau Wisata Hati yang akan membantu anda dalam merencanakan perjalan umroh anda. 
					Dana jemaah aman di rekening pribadi jemaah dan pasti berangkat. (S&K)</p>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default btn-sm" type="submit" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>