<?php if($data['sitemap']==true){ ?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xsi:schemaLocation="http://www.sitemaps.org schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
        <?php for($i=0;$i<count($data['url']);$i++){?>
        	<sitemap>
				<loc><?php echo $data['url'][$i]?></loc>
				<lastmod><?php echo $data['tgl'][$i].'+00:00'?></lastmod>			
			</sitemap>
        <?php } ?>
</sitemapindex>
<?php } else {?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xsi:schemaLocation="http://www.sitemaps.org schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
        <?php for($i=0;$i<count($data['url']);$i++){?>
        	<url>
				<loc><?php echo $data['url'][$i]?></loc>
				<lastmod><?php echo $data['tgl'][$i].'+00:00'?></lastmod>
				<changefreq>daily</changefreq>
				<priority>1.00</priority>				
			</url>
        <?php } ?>
</urlset>
<?php } ?>