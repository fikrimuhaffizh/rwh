<script src="<?php echo base_url()?>assets/backend/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/backend/global/scripts/app.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function cek_keberangkatan(){
        val = $('#no_hp').val();
        if(!val){
            return alert('Silahkan masukkan no Handphone Jemaah');
        }
        $('.div_detail_jemaah').addClass('hide');
        $('.hp_tidak_valid').addClass('hide');
        $('.hp_valid').addClass('hide');
        $('.lst_byr').remove();
        $('.lst_syrt').remove();
        $('.lst_dt').remove();
        
        $.ajax({
                method: 'POST',
                beforeSend: function(){$('#loading').show();},
                complete: function(){setInterval(function(){ $('#loading').hide(); }, 2000);},
                url: '<?php echo base_url() ?>keberangkatan/'+val,
                dataType: 'json',
                success : function(resp){
                    if(resp=='Tidak Valid'){
                        $('.div_detail_jemaah').addClass('hide');
                        $('.hp_tidak_valid').removeClass('hide');
                        $('.accordion_jemaah').empty();
                    } else {
                        $('.hp_valid').removeClass('hide');
                        $('.div_detail_jemaah').removeClass('hide');
                        $('.div_detail_jemaah').empty();
                        for(var j =0;j<resp.length;j++){
                            var z = '<hr>';
                                z += '<a data-toggle="collapse" class="btn btn-primary btn-sm accordion_jemaah" style="width:100%" data-parent="#accordion" href="#detail_jemaah_'+j+'" >';
                                z += '    '+(j+1)+'.&nbsp;'+resp[j]['nama_jemaah']+'';
                                z += '</a>';
                                z += '<div id="detail_jemaah_'+j+'" class="panel-collapse collapse accordion_jemaah">';
                                z += '    <div class="panel-body" id="accordion_jemaah_'+j+'">';
                                z += '    </div>';
                                z += '</div>';
                                $('.div_detail_jemaah').append(z);

                                 if(resp[j]['status_verifikasi'] == 'Belum Verifikasi'){
                                    var x = '<label class="fa-green hp_valid">No handphone terdaftar<br>Namun jemaah belum diverifikasi oleh Kantor Pusat</label>';
                                    $('#accordion_jemaah_'+j).append(x);
                                 } else {

                                    var x = '';
                                        x += '<label >Data Jemaah</label>';
                                        x += '<span id="status_detail_'+j+'"></span>';

                                        x += '<label>List Syarat Jemaah</label>';
                                        x += '<table class="table" id="status_syarat_'+j+'"></table>';

                                        x += '<label>List Pembayaran Jemaah</label>';
                                        x += '<table class="table" id="status_bayar_'+j+'"></table>';
                                    $('#accordion_jemaah_'+j).append(x);

                                    var syarat = resp[j]['list_syarat'];
                                    for(var i=0;i<syarat.length;i++){
                                        if(syarat[i]['status_syarat']=='Lengkap')
                                            syarat[i]['status_syarat'] = '<span class="fa-green">Lengkap</span>';
                                        else 
                                            syarat[i]['status_syarat'] = '<span class="fa-red">Belum Lengkap</span>';
                                        var x  = '';
                                            x += '<tr class="lst_syrt">';
                                            x += '  <td>'+syarat[i]['persyaratan']+'</td>';
                                            x += '  <td class="td_right">'+syarat[i]['status_syarat']+'</td>';
                                            x += '</tr>';
                                        $('#status_syarat_'+j+'').append(x);
                                    }
                                    var bayar = resp[j]['list_bayar'];
                                    for(var i=0;i<bayar.length;i++){
                                        var x  = '';
                                            x += '<tr class="lst_byr">';
                                            x += '  <td>'+bayar[i]['tgl_bayar']+'</td>';
                                            x += '  <td class="td_center">'+bayar[i]['jumlah_bayar']+'</td>';
                                            x += '  <td class="td_right">'+bayar[i]['status_pembayaran']+'</td>';
                                            x += '</tr>';
                                        $('#status_bayar_'+j+'').append(x);
                                    }
                                    var x  = '<table class="table lst_dt">';
                                        x += ' <tr><td class="td_left">Nama Jemaah</td><td>:</td><td class="td_right">'+resp[j]['nama_jemaah']+'</td></tr>';
                                        x += ' <tr><td class="td_left">Paket</td><td>:</td><td class="td_right">'+resp[j]['nama_paket']+'</td></tr>';
                                        x += ' <tr><td class="td_left">Kelas</td><td>:</td><td class="td_right">'+resp[j]['kelas']+'</td></tr>';
                                        x += ' <tr><td class="td_left">Keberangkatan</td><td>:</td><td class="td_right">'+resp[j]['berangkat']+'</td></tr>';
                                        x += ' <tr><td class="td_left">Status Syarat</td><td>:</td><td class="td_right">'+resp[j]['status_syarat']+'</td></tr>';
                                        x += ' <tr><td class="td_left">Status Pembayaran</td><td>:</td><td class="td_right">'+resp[j]['status_pembayaran']+'</td></tr>';
                                        x += ' <tr><td class="td_left">Status Jemaah</td><td>:</td><td class="td_right">'+resp[j]['status_jemaah']+'</td></tr>';
                                        x += '</table>';
                                        $('#status_detail_'+j+'').after(x);   
                                }
                                
                        }                    
                    }
                    // $('#no_hp').val("");
                }
            });
    }
</script>
<style type="text/css">
    .td_left{text-align: left;}
    .td_right{text-align: right;}
    .td_center{text-align: center;}
</style>
<div class="inner-head">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Status <span>Jemaah</span></h1>
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>">Beranda</a></li>
                    <li class="active"><?php echo $page_title ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="contain-wrapp padding-bot30 padding-top30 ">    
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="title-head centered">
                        <h4>Cek Status Keberangkatan</h4>
                        <p>Masukkan No Handphone Jemaah</p>
                    </div>
                    <center>Hindari penipuan dalam hal keberangkatan menuju Baitullah dengan selalu Cek Status Keberangkatan setiap selesai melakukan transaksi atau aktifitas yang berkaitan dengan pembayaran atau kelengkapan persyaratan jemaah</center>
                </div>
            </div><br>
            <div class="row">
                <div class="col-md-12">
                <div class="col-md-4 col-md-offset-4">
                    <div class="input-group">
                        <input type="text" class="form-control" id="no_hp" name="no_hp" placeholder="Ex. 0825xxxxxxx">
                        <span class="input-group-btn">
                            <button class="btn btn-yellow" onClick="cek_keberangkatan()" type="button"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                    <label class="fa-green hide hp_valid">No handphone Valid&nbsp;<i class="fa fa-check"></i></label>
                    <label class="fa-red hide hp_tidak_valid">No Handphone Tidak Valid&nbsp;<i class="fa fa-times"></i></label>  
                </div>
                <div class="col-md-6 col-md-offset-3">
                    <div class="hide div_detail_jemaah">

                    </div>
                    <hr>
                </div> 
            </div>
        </div>
    </div>
</div>